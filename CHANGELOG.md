# Changelog/Release Notes

On this page we provide a summary of the main API changes, new features and examples for each release of Ratel.

(main)=

## Current `main` branch

### New features

- Enabled discontinuous spaces with the command line option `-pressure_petscdualspace_lagrance_continuity 0` and similar.
- Add mixed Neo-Hookean hyperelastic models in current configuration.
- Add Kershaw mesh transformation for unit box mesh.
- Add Weierstrass mms for BPs problems.
- Add `-ts_monitor_*_interval` options to all custom `-ts_monitor_*` routines to limit output frequency.
- Add block preconditioner for mixed Neo-Hookean hyperelastic models in initial configuration.
- Add support for computing surface forces and centroids within a sub-region of a face label via `-surface_force_face_[facename]_bounding_box`.
- Add block preconditioner for mixed Neo-Hookean hyperelastic models in current configuration.
- Enabled discontinuous non-tensor space for pressure in mixed problem.
- Add mixed Neo-Hookean hyperelastic models in initial configuration with perturbed Lagrange-multiplier method.
- Add support for storing computed surface forces for each face in a separate file via `-ts_monitor_surface_force_per_face ascii:[base filename].csv`
- Add block preconditioner for mixed Neo-Hookean hyperelastic models in initial configuration with perturbed Lagrange-multiplier method.
- Add penalty method based platens
- Add block preconditioner for mixed Neo-Hookean hyperelastic models in current configuration with perturbed Lagrange-multiplier method.
- Support `PCBJacobi`, `PCPBJacobi`, and `PCVPBJacobi` for `MATCEED`.
- Add preliminary MPM implementation, with material point integration, mesh-to-point and point-to-mesh projection, and libCEED BPs at points.
- Add `elasticity-mpm-neo-hookean-current` material which allows for storing material properties and density at material points.
- Add sinker example for MPM Neo-Hookean material to compare against FEM implementation.
- Add `DMSNESSetObjective` for static/quasistatic problems with objective functional to use `snes_linesearch_type bt`.
- Add support for flexible pressure boundary conditions.
- Add support for fieldsplit preconditioner MPM and tested with mixed-linear example.
- Automatically append to CSV monitoring files from prior runs when a restart file is provided with `-continue_file`.
- Add AT1 and AT2 phase-field models of fracture for linear elasticity (monolithic scheme)
- Add viscous damage regularization
- Add explicit flag for Coulomb friction model via `-bc_platen_[platen_name]_friction_type coulomb`.
- Add Threlfall regularized friction model via `-bc_platen_[platen_name]_friction_type threlfall`.
- Add support for viscous damping friction via `-bc_platen_[platen_name]_friction_viscous [non-negative real]`.
- Extend the phase-field modeling of fracture to Neo-Hookean hyperelasticity in initial/current configurations.
- Add linear poroelastic material model.
- Add block preconditioner for linear poroelastic model.
- Add support for fully incompressible (nu=0.5) in mixed models.
- Add MPM Neo-Hookean hyperelasticity with phase-field damage model: `elasticity-mpm-neo-hookean-damage-current`
- Add basic mesh remapping for MPM
- Add support for slip, dirichlet, and MMS bounaries to be active field (displacement or pressure) specific
- Add support for computing Jacobians and Hessians from strain energy density functional in the residual and jacobian evaluations via Automatic Differentiation with ADOL-C.
- Add nonlinear hardening to linear (i.e. small-strains) plasticity
- Add large-strains plasticity based on log strain measures in initial/current configurations (includes principal directions-based implementations)
- Add finite strain Neo-Hookean poroelasticity in current configuration.

### Internal

- Add `PetscFECreateLagrangeFromOptions` for more flexible control over `PetscFE` creation.
- Refactor MMS support for maintainability and to allow runtime options.
- Rename `RatelModelType` to `RatelMethodType` as FEM and MPM will use the same `CeedQFunction` source.
- Rename `RatelComputeMaxValues` to `RatelComputeMaxSolutionValues`.
- Add `RatelViewMaxDiagnosticValuesByNameFromOptions` for viewing the max diagnostic component value by names.
- Replace `RatelCeedOperatorApplyContext` with `CeedEvaluator` that mirrors `MatCEED` for nonlinear or general purpose operators.
- Add `RatelCeedSetupSurfaceQData` for computing surface Qdata in traction boundary condition.
- Refactor common parameters for friction models into `RatelFrictionParams` and add common interface for friction models.
- Fix mass density calculations in diagnostic computations. Mass density is now correctly computed as rho / J.
- `RatelCeedAddBoundariesDirichletMMS` now supports MMS boundaries for multiple fields with varying numbers of components (ex. displacement and pressure fields).
- Refactored post-run verification into separate `RatelView*ErrorFromOptions` functions, which read `-*_rtol` and `-*_atol` values from the command-line to check expected values.
- Add `stored_values_ut` for `residual_ut` qfunction.

### Breaking changes

- Deprecate `-bc_platen_[platen_name]_f` option for enabling friction, replaced by `-bc_platen_[platen_name]_friction` sub-options.

(v0-3-0)=

## v0.3.0 (1 November 2023)

### New features

- Update strain energy function to the convex form for both Neo-Hookean and Mooney-Rivlin models.
- Add mixed linear elasticity and Neo-Hookean hyperelastic models for incompressible materials.
- Add linear plasticity with linear hardening model for small strains.
- Add pressure boundary loading which is caused by liquids or gases on the surface of the solid structure.
- Add flexible clamp, slip, traction, and platen boundary conditions with more complex time variance during a quasistatic or dynamic simulation.
- Add robust surface force and surface centroid monitoring options.
- Add command-line validation options for maximum displacement and per-face centroids and surface forces.
- Add isochoric Ogden hyperelastic model in initial configuration.
- Add mixed Ogden hyperelastic model in initial configuration.
- Add isochoric Neo-Hookean model in initial configuration.
- Add Mooney-Rivlin hyperelastic model in current configuration.
- Add Coulomb friction capabilities to platen contact boundary conditions.
- Add isochoric Mooney-Rivlin model in initial configuration.
- Add CEED benchmark problems 1, 2, 3, and 4 for convergence testing and benchmarking.
- Add Kelvin-Mandel notation pack and unpack to symmetric tensors functions.
- Setup dynamic solver for mixed linear elastic and hyperelastic models.
- Add isochoric Neo-Hookean model in current configuration.
- Add density-dependent user-specified body forces which can be specified globally or on a per-material basis.
- Add isochoric Ogden hyperelastic model in current configuration.
- Add RatelCreateSubmatrix function to create Matshell block preconditioner for mixed-FEM.
- Enable pMG preconditioning for the displacement block of mixed-FEM models.
- Add isochoric Mooney-Rivlin hyperelastic model in current configuration.

### Breaking changes

- Rename `Ratel[TS,SNES,KSP]SetupPCMG` to `Ratel[TS,SNES,KSP]SetupPC` for better accuracy.
- Rename `RatelSolverType`, `RatelMultigridType`, and some function names for consistency.
- Refactor p-multigrid support as registered PETSc `PC`; this change generated various API and command line option changes.
- Rename material parameter contexts as follows:
  - `RatelLinearElasticityParams`
  - `NeoHookeanPhysics` to `RatelNeoHookeanElasticityParams` (alias of `RatelLinearElasticityParams`)
  - `MooneyRivlinPhysics` to `MooneyRivlinElastictictyParams`
- Remove material-specific platen contexts
- Rename cell-to-face surface force functions to be suffixed with `CellToFace` and `-ts_monitor_surface_force` to `-ts_monitor_surface_force_cell_to_face` to prevent accidental usage
- Rename reaction force functions `*ReactionForce*` to `*SurfaceForce*` and `-ts_monitor_reaction_force` to`-ts_monitor_surface_force`
- Deprecate `-ts_monitor_reaction_force`
- Remove `RATEL_FORCING_CONSTANT` and associated CLI options `-forcing constant` and `-forcing_vec`

See the `Using Ratel` section for the updated command line options.

### Maintainability

- Fixes to QFunctions and documentation for clarity and consistency.
- Separate `MatCeed` logic for maintainability and future upstreaming to PETSc.

### Performance

- Updates to setup to reduce setup time.
- Updates to local vector usage to reduce memory footprint.

(v0-2-0)=

## V0.2.1 (12 January 2023)

Bugfix to make tet based tests optional in the test suite.

## V0.2.0 (10 January 2023)

Expanded features and material models.

### New features

- Add `RatelComputeSurfaceForces` for computing final forces on surfaces, identified by DMPlex face id number.
- Add support for computing Jacobians from strain energy density functional from residual evaluation via Automatic Differentiation with Enzyme-AD.
  Neo-Hookean material models provided in current and initial configuration.
- Add support for platen boundary condition based upon Nitsche's method.
- Add support for restart from saved binaries of state vectors.

### Breaking changes

- Update names and command line arguments for material models for consistency.
- Use PETSc option `options_file` over redundant `yml` option in tests and examples; removed `options_yml` argument from `RatelInit`.
- Docker images built and tested as part of CI process for latest `main`.
- Update Boolean arguments `-view_diagnostic_quantities` and `-ts_monitor_diagnostic_quantities` to use viewer type.
- Remove command line option `-output_dir`; this functionality is provided with setting the output filepath for the viewer via `-view_diagnostic_quantities` and `-ts_monitor_diagnostic_quantities`.
- Remove `RatelVectorView` in favor of base PETSc functionality.

### Maintainability

- Internal refactor around `RatelMaterial`, an object representing a region of the mesh given by a material model with consistent physical parameters.
External behavior was not affected by this refactor.

(v0-1-2)=

## v0.1.2 (15 April 2022)

Bugfix for install paths in makefile.

(v0-1-1)=

## v0.1.1 (14 April 2022)

Bugfix for Mass QFunction source file relative path.

(v0-1)=

## v0.1 (11 April 2022)

Initial Ratel API.
The Ratel solid mechanics library is based upon libCEED and PETSc.
libCEED v0.10.1 or later and PETSc v3.17 or later is required.
