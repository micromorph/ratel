/// @file
/// Ratel quasistatic example

const char help[] = "Ratel - quasistatic example\n";

#include <petsc.h>
#include <ratel.h>
#include <stddef.h>

int main(int argc, char **argv) {
  MPI_Comm    comm;
  Ratel       ratel;
  TS          ts;
  DM          dm, dm_solution;
  Vec         U;
  PetscScalar final_time = 1.0;
  PetscBool   quiet      = PETSC_FALSE;

  PetscCall(PetscInitialize(&argc, &argv, NULL, help));
  comm = PETSC_COMM_WORLD;

  // Read command line options
  PetscOptionsBegin(comm, NULL, "Ratel quasistatic example", NULL);
  PetscCall(PetscOptionsBool("-quiet", "Suppress summary outputs", NULL, quiet, &quiet, NULL));
  PetscOptionsEnd();

  // Initialize Ratel context and create DM
  PetscCall(RatelInit(comm, &ratel));
  PetscCall(RatelDMCreate(ratel, RATEL_SOLVER_QUASISTATIC, &dm));
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));

  // Create TS
  PetscCall(TSCreate(comm, &ts));

  // Avoid stepping past the final loading condition (because the solution might not be valid there)
  PetscCall(TSSetMaxTime(ts, final_time));
  PetscCall(TSSetExactFinalTime(ts, TS_EXACTFINALTIME_MATCHSTEP));

  // Set options
  PetscCall(RatelTSSetup(ratel, ts));

  // View Ratel setup
  if (!quiet) {
    // LCOV_EXCL_START
    PetscCall(PetscPrintf(comm, "----- Ratel Quasistatic Example -----\n\n"));
    PetscCall(RatelView(ratel, PETSC_VIEWER_STDOUT_WORLD));
    // LCOV_EXCL_STOP
  }

  // Solution vector
  PetscCall(DMCreateGlobalVector(dm_solution, &U));
  // Name vector so it isn't automatically named (via address) in output files
  PetscCall(PetscObjectSetName((PetscObject)U, "U"));

  // Solve
  PetscCall(PetscLogDefaultBegin());  // So we can use PetscLogStageGetPerfInfo without -log_view
  PetscPreLoadBegin(PETSC_FALSE, "Ratel Solve");
  PetscCall(RatelTSSetupInitialCondition(ratel, ts, U));
  if (PetscPreLoadingOn) {
    // LCOV_EXCL_START
    SNES      snes;
    PetscReal rtol;
    PetscCall(TSGetSNES(ts, &snes));
    PetscCall(SNESGetTolerances(snes, NULL, &rtol, NULL, NULL, NULL));
    PetscCall(SNESSetTolerances(snes, PETSC_DEFAULT, .99, PETSC_DEFAULT, PETSC_DEFAULT, PETSC_DEFAULT));
    PetscCall(TSSetSolution(ts, U));
    PetscCall(TSStep(ts));
    PetscCall(SNESSetTolerances(snes, PETSC_DEFAULT, rtol, PETSC_DEFAULT, PETSC_DEFAULT, PETSC_DEFAULT));
    // LCOV_EXCL_STOP
  } else {
    PetscCall(TSSolve(ts, U));
  }
  PetscPreLoadEnd();

  // Output solution
  PetscCall(RatelTSCheckpointFinalSolutionFromOptions(ratel, ts, U));
  PetscCall(VecViewFromOptions(U, NULL, "-view_final_solution"));

  // Post solver info
  {
    TSConvergedReason reason;
    PetscInt          num_steps;

    PetscCall(TSGetSolveTime(ts, &final_time));
    if (!quiet) PetscCall(PetscPrintf(comm, "Final time: %f\n", final_time));
    PetscCall(TSGetStepNumber(ts, &num_steps));
    if (!quiet) PetscCall(PetscPrintf(comm, "TS steps: %" PetscInt_FMT "\n", num_steps));
    PetscCall(TSGetConvergedReason(ts, &reason));
    if (!quiet || reason < TS_CONVERGED_ITERATING) PetscCall(PetscPrintf(comm, "TS converged reason: %s\n", TSConvergedReasons[reason]));
  }

  // Verify MMS
  PetscCall(RatelViewMMSL2ErrorFromOptions(ratel, final_time, U));

  // Compute and verify strain energy
  PetscCall(RatelViewStrainEnergyErrorFromOptions(ratel, final_time, U));

  // Compute and verify max displacement
  PetscCall(RatelViewMaxSolutionValuesErrorFromOptions(ratel, final_time, U));

  // Compute and verify surface forces and centroids
  PetscCall(RatelViewSurfaceForceAndCentroidErrorFromOptions(ratel, final_time, U));

  // Compute and verify maximum diagnostic quantities
  PetscCall(RatelViewMaxDiagnosticQuantitiesErrorByNameFromOptions(ratel, final_time, U));

  // Compute and view diagnostic quantities
  PetscCall(RatelViewDiagnosticQuantitiesFromOptions(ratel, final_time, U));

  // View point quantities
  if (dm != dm_solution) {
    PetscCall(RatelDMSwarmViewFromOptions(ratel, dm, "-view_swarm"));
  }

  // Report total solve time
  {
    PetscLogStage      stage_id;
    PetscEventPerfInfo stage_perf_info;

    PetscCall(PetscLogStageGetId("Ratel Solve", &stage_id));
    PetscCall(PetscLogStageGetPerfInfo(stage_id, &stage_perf_info));
    if (!quiet) PetscCall(PetscPrintf(comm, "Time taken to compute solution (sec): %g\n", stage_perf_info.time));
  }

  // Cleanup
  PetscCall(TSDestroy(&ts));
  PetscCall(DMDestroy(&dm));
  PetscCall(DMDestroy(&dm_solution));
  PetscCall(VecDestroy(&U));
  PetscCall(RatelDestroy(&ratel));

  return PetscFinalize();
}

// ---------- Test Cases ----------

// MPM tests
//TESTARGS(name="MPM at Gauss points, linear, MMS")           -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-mpm-linear-mms-gauss.yml
//TESTARGS(name="MPM, Neo-Hookean current")                   -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-mpm-neo-hookean-current.yml
//TESTARGS(name="MPM, Neo-Hookean current at points, sinker") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-mpm-neo-hookean-current-sinker.yml

//TESTARGS(name="MPM, Neo-Hookean elasticity and damage AT1")                        -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-mpm-neo-hookean-damage-current.yml -use_AT1 true -snes_max_it 5 -expected_strain_energy 1.146194436140e-02 -strain_energy_atol 1e-6
//TESTARGS(name="MPM, Neo-Hookean elasticity and damage AT2",only="serial,cgnsdiff") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-mpm-neo-hookean-damage-current.yml -expected_strain_energy 1.136153484528e-02 -strain_energy_atol 1e-6 -ts_monitor_diagnostic_quantities cgns:mpm-neo-hookean-damage_{ceed_resource}.cgns
//TESTARGS(name="MPM, Neo-Hookean elasticity and damage AT2, crack")                 -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-mpm-neo-hookean-damage-current-crack-AT2.yml -ts_max_time 0.1 -expected_strain_energy 3.172886619673e-05 -strain_energy_atol 1e-10
//TESTARGS(name="MPM, Neo-Hookean elasticity and damage AT2, remap geometry")        -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-mpm-neo-hookean-damage-current-remap.yml

// MMS tests
//TESTARGS(name="linear, MMS")                                    -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-linear-mms.yml
//TESTARGS(name="mixed linear, MMS PCJacobi")                     -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-mixed-linear-mms-pcjacobi.yml -ts_monitor_strain_energy ascii:mixed-linear-mms-pcjacobi-strain-energy_{ceed_resource}.csv
//TESTARGS(name="mixed linear, MMS PCFieldSplitPMGLU",only="cpu") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-mixed-linear-mms-pcfieldsplit-pmglu.yml
//TESTARGS(name="linear poroelasticity, MMS PCJacobi")            -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-poroelasticity-linear-mms-pcjacobi.yml
//TESTARGS(name="linear poroelasticity, MMS PCFieldSplitJacobi") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-poroelasticity-linear-mms-pcfieldsplit-pcjacobi.yml

// Material model tests
//TESTARGS(name="Neo-Hookean initial AD",only="ad-enzyme") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-neo-hookean-initial-ad-enzyme.yml
//TESTARGS(name="Neo-Hookean current AD",only="ad-enzyme") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-neo-hookean-current-ad-enzyme.yml

//TESTARGS(name="mixed Neo-Hookean current") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-mixed-neo-hookean-current-pcjacobi.yml
//TESTARGS(name="mixed Ogden initial")       -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-mixed-ogden-initial-pcjacobi.yml

//TESTARGS(name="linear plasticity")                                                    -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-plasticity-linear.yml
//TESTARGS(name="linear elasticity and damage AT1, no scaling, no vis. regularization") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-linear-damage-beambending-AT1-no-scaling.yml -ts_max_time 0.02 -expected_strain_energy 8.553919591748e-05
//TESTARGS(name="linear elasticity and damage AT1")                                     -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-linear-damage-beambending-AT1.yml -ts_max_time 0.02 -expected_strain_energy 8.553925496938e-05
//TESTARGS(name="linear elasticity and damage AT2, quasinewton")                        -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-linear-damage-beambending-AT2-no-scaling-quasinewton.yml -ts_max_time 0.02 -expected_strain_energy 8.646720666601e-05

//TESTARGS(name="Neo-Hookean initial and damage AT1") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-neo-hookean-damage-initial-beambending-AT1.yml -ts_max_time 0.02 -expected_strain_energy 8.553931529999e-05
//TESTARGS(name="Neo-Hookean current and damage AT1") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-neo-hookean-damage-current-beambending-AT1.yml -ts_max_time 0.02 -expected_strain_energy 8.553931530533e-05

//TESTARGS(name="Elasticity log strains, initial, principal")                           -ceed {ceed_resource} -quiet -options_file ./examples/ymls/ex02-quasistatic-elasticity-hencky-initial-principal.yml
//TESTARGS(name="Elasticity log strains, current, principal",only="serial,cgnsdiff")    -ceed {ceed_resource} -quiet -options_file ./examples/ymls/ex02-quasistatic-elasticity-hencky-current-principal.yml -ts_monitor_diagnostic_quantities cgns:elasticity-hencky-current-principal_{ceed_resource}.cgns
//TESTARGS(name="Elasticity log strains, initial, principal, damage AT1")               -ceed {ceed_resource} -quiet -options_file ./examples/ymls/ex02-quasistatic-elasticity-hencky-damage-initial-principal-beambending-AT1.yml -ts_max_time 0.02 -expected_strain_energy 8.553925456877e-05
//TESTARGS(name="Elasticity log strains, current, principal, damage AT1")               -ceed {ceed_resource} -quiet -options_file ./examples/ymls/ex02-quasistatic-elasticity-hencky-damage-current-principal-beambending-AT1.yml -ts_max_time 0.02 -expected_strain_energy 8.553925456877e-05
//TESTARGS(name="Elasticity log strains, initial, principal, damage AT2, quasinewton")  -ceed {ceed_resource} -quiet -options_file ./examples/ymls/ex02-quasistatic-elasticity-hencky-damage-initial-principal-beambending-AT2-no-scaling-quasinewton.yml -ts_max_time 0.02 -expected_strain_energy 8.646726482756e-05
//TESTARGS(name="Plasticity log strains, initial")                                      -ceed {ceed_resource} -quiet -options_file ./examples/ymls/ex02-quasistatic-plasticity-hencky-initial.yml -ts_max_time 0.2 -expected_strain_energy 1.234751682262e-03
//TESTARGS(name="Plasticity log strains, current")                                      -ceed {ceed_resource} -quiet -options_file ./examples/ymls/ex02-quasistatic-plasticity-hencky-current.yml -ts_max_time 0.2 -expected_strain_energy 1.234751682262e-03
//TESTARGS(name="Plasticity log strains, initial, principal")                           -ceed {ceed_resource} -quiet -options_file ./examples/ymls/ex02-quasistatic-plasticity-hencky-initial-principal.yml -ts_max_time 0.2 -expected_strain_energy 1.234751682262e-03
//TESTARGS(name="Plasticity log strains, current, principal",only="serial,cgnsdiff")    -ceed {ceed_resource} -quiet -options_file ./examples/ymls/ex02-quasistatic-plasticity-hencky-current-principal.yml -ts_max_time 0.2 -expected_strain_energy 1.234751682262e-03 -ts_monitor_diagnostic_quantities cgns:plasticity-hencky-current-principal_{ceed_resource}.cgns
//TESTARGS(name="Plasticity log strains, current, nonlinear hardening")                 -ceed {ceed_resource} -quiet -options_file ./examples/ymls/ex02-quasistatic-plasticity-hencky-current-nonlinearhardening.yml -ts_max_time 0.2 -expected_strain_energy 1.287757226861e-03
//TESTARGS(name="Plasticity log strains, current, principal, nonlinear hardening")      -ceed {ceed_resource} -quiet -options_file ./examples/ymls/ex02-quasistatic-plasticity-hencky-current-principal-nonlinearhardening.yml -ts_max_time 0.2 -expected_strain_energy 1.287757226861e-03

// Boundary condition tests
//TESTARGS(name="Mooney-Rivlin initial, slip") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-mooney-rivlin-initial-slip.yml

//TESTARGS(name="Neo-Hookean current, flexible slip")     -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-neo-hookean-current-flexible-slip.yml
//TESTARGS(name="Neo-Hookean initial, flexible clamp")    -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-neo-hookean-initial-flexible-clamp.yml
//TESTARGS(name="Neo-Hookean initial, flexible traction") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-neo-hookean-initial-flexible-traction.yml
//TESTARGS(name="Neo-Hookean initial, flexible pressure") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-neo-hookean-initial-flexible-pressure.yml

//TESTARGS(name="linear, platen")                                     -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-linear-platen.yml
//TESTARGS(name="mixed linear, platen")                               -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-mixed-linear-platen-pcjacobi.yml
//TESTARGS(name="Mooney-Rivlin current, platen")                      -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-mooney-rivlin-current-platen.yml
//TESTARGS(name="mixed Neo-Hookean initial, platen")                  -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-mixed-neo-hookean-initial-platen-pcjacobi.yml
//TESTARGS(name="mixed Neo-Hookean current, platen")                  -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-mixed-neo-hookean-current-platen-pcjacobi.yml
//TESTARGS(name="mixed Neo-Hookean PL initial, platen")               -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-mixed-neo-hookean-PL-initial-platen-pcjacobi.yml
//TESTARGS(name="mixed Neo-Hookean PL current, platen")               -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-mixed-neo-hookean-PL-current-platen-pcjacobi.yml
//TESTARGS(name="mixed Ogden initial, platen")                        -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-mixed-ogden-initial-platen-pcjacobi.yml
//TESTARGS(name="linear elasticity and damage AT2, platen")           -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-linear-damage-beambending-AT2-platen.yml -ts_max_time 0.14 -expected_strain_energy 6.706756064060e-05
//TESTARGS(name="linear plasticity, platen")                          -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-plasticity-linear-platen.yml

//TESTARGS(name="Neo-Hookean current, platen half-cylinder")   -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-neo-hookean-current-platen-half-cylinder.yml
//TESTARGS(name="Mooney-Rivlin initial, platen half-cylinder") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-mooney-rivlin-initial-platen-half-cylinder.yml
//TESTARGS(name="Neo-Hookean current, multi-platen cylinder")  -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-multi-platen.yml

//TESTARGS(name="Neo-Hookean initial, platen, penalty")                -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-neo-hookean-initial-platen-penalty.yml
//TESTARGS(name="Neo-Hookean current, multi-platen cylinder, penalty") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-multi-platen-penalty.yml

//TESTARGS(name="Neo-Hookean initial, damage AT2, platen") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-neo-hookean-damage-initial-beambending-AT2-platen.yml -ts_max_time 0.14 -expected_strain_energy 6.703778644951e-05
//TESTARGS(name="Neo-Hookean current, damage AT2, platen") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-neo-hookean-damage-current-beambending-AT2-platen.yml -ts_max_time 0.14 -expected_strain_energy 6.703778641465e-05

//TESTARGS(name="Elasticity log strains, initial, principal, platen")                                    -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-hencky-initial-principal-platen.yml -ts_max_time 0.2 -expected_strain_energy 2.495345066088e-03
//TESTARGS(name="Elasticity log strains, initial, principal, damage AT2, platen",only="serial,cgnsdiff") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-hencky-damage-initial-principal-beambending-AT2-platen.yml -ts_max_time 0.14 -expected_strain_energy 1.460161251106e-04 -ts_monitor_diagnostic_quantities cgns:elasticity-hencky-initial-damage-principal-beambending-AT2-platen_{ceed_resource}.cgns
//TESTARGS(name="Plasticity log strains, current, platen")                                               -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-plasticity-hencky-current-platen.yml -ts_max_time 0.2 -expected_strain_energy 1.719128439527e-03
//TESTARGS(name="Plasticity log strains, current, principal, platen")                                    -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-plasticity-hencky-current-principal-platen.yml -ts_max_time 0.2 -expected_strain_energy 1.719128439527e-03

//TESTARGS(name="linear poroelasticity, pressure clamp") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-poroelasticity-linear-Terzaghi.yml
//TESTARGS(name="Neo-Hookean poroelasticity current")    -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-poroelasticity-neo-hookean-current.yml

// Face forces tests
//TESTARGS(name="Neo-Hookean, face forces")             -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-neo-hookean-current-face-forces.yml -ts_monitor_surface_force_cell_to_face ascii:neo-hookean-face-forces_{ceed_resource}.csv
//TESTARGS(name="isochoric Neo-Hookean, face forces")   -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-isochoric-neo-hookean-current-face-forces.yml  -ts_monitor_surface_force ascii:isochoric-neo-hookean-face-forces_{ceed_resource}.csv
//TESTARGS(name="isochoric Mooney-Rivlin, face forces") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-isochoric-mooney-rivlin-current-face-forces.yml  -ts_monitor_surface_force ascii:isochoric-mooney-rivlin-face-forces_{ceed_resource}.csv
//TESTARGS(name="isochoric Ogden, face forces")         -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-isochoric-ogden-current-face-forces.yml -ts_monitor_surface_force ascii:isochoric-ogden-face-forces_{ceed_resource}.csv

//TESTARGS(name="mixed linear, face forces")                 -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-mixed-linear-face-forces-pcjacobi.yml
//TESTARGS(name="mixed Neo-Hookean initial, face forces")    -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-mixed-neo-hookean-initial-face-forces-pcjacobi.yml -ts_monitor_surface_force ascii:mixed-neo-hookean-face-forces_initial_{ceed_resource}.csv
//TESTARGS(name="mixed Neo-Hookean current, face forces")    -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-mixed-neo-hookean-current-face-forces-pcjacobi.yml -ts_monitor_surface_force ascii:mixed-neo-hookean-face-forces_current_{ceed_resource}.csv
//TESTARGS(name="mixed Neo-Hookean PL initial, face forces") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-mixed-neo-hookean-PL-initial-face-forces-pcjacobi.yml -ts_monitor_surface_force ascii:mixed-neo-hookean-PL-face-forces_initial_{ceed_resource}.csv
//TESTARGS(name="mixed Neo-Hookean PL current, face forces") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-mixed-neo-hookean-PL-current-face-forces-pcjacobi.yml -ts_monitor_surface_force ascii:mixed-neo-hookean-PL-face-forces_current_{ceed_resource}.csv
//TESTARGS(name="mixed Ogden, face forces")                  -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-mixed-ogden-initial-face-forces-pcjacobi.yml -ts_monitor_surface_force ascii:mixed-ogden-face-forces_{ceed_resource}.csv

//TESTARGS(name="Neo-Hookean, multi-platen face forces, simplex")                  -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-multi-platen-face-forces-simplex.yml -ts_monitor_surface_force ascii:multi-platen-face-forces-simplex_{ceed_resource}.csv

// Multi-material tests
//TESTARGS(name="multi-material")                                 -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-multi-material.yml
//TESTARGS(name="multi-material, simplex")                        -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-multi-material-simplex.yml
//TESTARGS(name="multi-material, monitor",only="serial,cgnsdiff") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-multi-material.yml -ts_monitor_diagnostic_quantities cgns:multi-material_{ceed_resource}.cgns
//TESTARGS(name="multi-material, face forces")                    -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-multi-material.yml -surface_force_faces 1,2 -ts_monitor_surface_force ascii:multi-material-forces_{ceed_resource}.csv
//TESTARGS(name="multi-material, multi-density body force")       -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-multi-density-force.yml
//TESTARGS(name="multi-material, hex, dense sinker")              -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-neo-hookean-current-sinker.yml

// Other tests
//TESTARGS(name="Neo-Hookean, face forces continue",only="serial") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-neo-hookean-current-face-forces-continue.yml

//TESTARGS(name="Schwarz pendulum")                 -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-schwarz-pendulum.yml -ts_max_time 0.25 -ts_dt 0.05 -dm_plex_tps_extent 2,1,1  -dm_plex_tps_refine 0 -dm_plex_tps_layers 1 -expected_strain_energy 5.387890807640e-04 -expected_strain_energy 2.493188175680e-04
//TESTARGS(name="mixed Neo-Hookean, Schwarz press") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-mixed-schwarz-press.yml -dm_plex_tps_refine 0 -expected_strain_energy 7.515779959501e-11

//TESTARGS(name="Mooney-Rivlin current, body forces")            -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-mooney-rivlin-current-body-forces.yml

//TESTARGS(name="linear elasticity and damage AT1, tensileshear, face forces")     -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-linear-damage-tensileshear-AT1-face-forces.yml -ts_max_time 0.1 -ts_monitor_surface_force ascii:linear-elasticity-damage-tensileshear-AT1-face-forces_{ceed_resource}.csv
//TESTARGS(name="linear elasticity and damage AT2, compressiveshear, face forces") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-linear-damage-compressiveshear-AT2-face-forces.yml -ts_max_time 0.04 -ts_monitor_surface_force ascii:linear-elasticity-damage-compressiveshear-AT2-face-forces_{ceed_resource}.csv

//TESTARGS(name="mixed Neo-Hookean, cook membrane test")     -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-mixed-neo-hookean-cook-membrane.yml
//TESTARGS(name="isochoric Neo-Hookean, cook membrane test") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-isochoric-neo-hookean-cook-membrane.yml

//TESTARGS(name="mixed Neo-Hookean, punch test")                     -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-mixed-neo-hookean-punch-test.yml
//TESTARGS(name="mixed Neo-Hookean PL, punch test")                  -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-mixed-neo-hookean-PL-punch-test.yml
//TESTARGS(name="isochoric Neo-Hookean, punch test, max diagnostic") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-isochoric-neo-hookean-punch-test-max-diagnostic.yml
//TESTARGS(name="mixed Ogden, punch test, fully-incompressible")           -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-mixed-ogden-punch-test-fully-incompressible.yml
//TESTARGS(name="mixed Neo-Hookean, punch test, fully-incompressible")     -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-mixed-neo-hookean-punch-test-fully-incompressible.yml
//TESTARGS(name="mixed Neo-Hookean PL, punch test, fully-incompressible")  -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-mixed-neo-hookean-PL-punch-test-fully-incompressible.yml
//TESTARGS(name="Neo-Hookean, cylinder buckling")                    -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-neo-hookean-cylinder-buckling.yml
//TESTARGS(name="Mixed Neo-Hookean PL, cylinder buckling")           -ceed {ceed_resource} -quiet -options_file examples/ymls/ex02-quasistatic-elasticity-mixed-neo-hookean-PL-cylinder-buckling.yml
