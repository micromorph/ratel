(examples)=
# Example Applications

Ratel provides several examples for direct use as simulation drivers and as starters for more complete applications and workflows.
See {ref}`using` for common options to specify the mesh, materials, algebraic solvers, etc.

(example-static)=
## Static Example

This code solves the steady-state static momentum balance equations using unstructured high-order finite/spectral element spatial discretizations.
We consider linear and three different hyperelastic materials in solid mechanics applications:

* Linear elasticity
* Neo-Hookean hyperelasticity at finite strain
* Mooney-Rivlin hyperelasticity at finite strain
* Ogden hyperelasticity at finite strain

formulated for compressible and incompressible materials.

In the top level directory, build by using:

```console
$ make examples
```

and run with:

```console
$ ./build/ex01-static -options_file [.yml file] -view_diagnostic_quantities
```

(example-quasistatic)=
## Quasistatic Example

This code solves the same steady-state formulation as the static solver example, but with an additional time stepping parameter for boundary conditions and forcing terms.

In the top level directory, build by using:

```console
$ make examples
```

and run with:

```console
$ ./build/ex02-quasistatic -options_file [.yml file] -view_diagnostic_quantities
```

The quasistatic formulation is built off of the [PETSc Time Stepper (TS) object](https://petsc.org/release/docs/manualpages/TS/index.html).

(example-dynamic)=
## Dynamic Solver

The dynamic solver example solves the momentum balance equations with the same constitutive models as the static and quasistatic examples.

In the top level directory, build by using:

```console
$ make examples
```

and run with:

```console
$ ./build/ex03-dynamic -options_file [.yml file] -view_diagnostic_quantities
```

The dynamic formulation uses the TSAlpha2 [PETSc Time Stepper (TS) object](https://petsc.org/release/docs/manualpages/TS/index.html), which implements the implicit Generalized-Alpha method for second-order systems.
