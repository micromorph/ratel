#!/usr/bin/env python3

# Sample Python runner for Ratel examples

# autopep8: off
#TESTARGS(name="CEED BP1")                                   --ceed {ceed_resource} -n {nproc} -q -o examples/ymls/ex01-ceed-bp1.yml -r 4 5 6 7 -p 3
#TESTARGS(name="CEED BP2")                                   --ceed {ceed_resource} -n {nproc} -q -o examples/ymls/ex01-ceed-bp2.yml -r 4 5 6 7 -p 3
#TESTARGS(name="CEED BP2, AtPoints")                         --ceed {ceed_resource} -n {nproc} -q -o examples/ymls/ex01-ceed-bp2.yml -r 4 5 6 7 -p 2 --method mpm --num-points 27
#TESTARGS(name="CEED BP3")                                   --ceed {ceed_resource} -n {nproc} -q -o examples/ymls/ex01-ceed-bp3.yml -r 4 5 6 7
#TESTARGS(name="CEED BP4")                                   --ceed {ceed_resource} -n {nproc} -q -o examples/ymls/ex01-ceed-bp4.yml -r 4 5 6 7
#TESTARGS(name="CEED BP4, AtPoints")                         --ceed {ceed_resource} -n {nproc} -q -o examples/ymls/ex01-ceed-bp4.yml -r 5 6 7 8 -p 2 --method mpm --num-points 8 --point-location gauss
#TESTARGS(name="linear, MMS")                                --ceed {ceed_resource} -n {nproc} -q -o examples/ymls/ex01-static-elasticity-linear-mms.yml -r 4 5 6 7
#TESTARGS(name="mixed linear, MMS")                          --ceed {ceed_resource} -n {nproc} -q -o examples/ymls/ex01-static-elasticity-mixed-linear-mms-pcjacobi.yml -r 3 4 5 6
#TESTARGS(name="mixed linear discontinuous, MMS")            --ceed {ceed_resource} -n {nproc} -q -o examples/ymls/ex01-static-elasticity-mixed-linear-discontinuous-mms-pcjacobi.yml -r 5 6 7 8
#TESTARGS(name="mixed linear discontinuous non-tensor, MMS") --ceed {ceed_resource} -n {nproc} -q -o examples/ymls/ex01-static-elasticity-mixed-linear-discontinuous-non-tensor-mms-pcjacobi.yml -r 5 6 7 8
#TESTARGS(name="linear poroelasticity, MMS")                 --ceed {ceed_resource} -n {nproc} -q -o examples/ymls/ex02-quasistatic-poroelasticity-linear-mms-pcjacobi.yml -r 4 5 6 7 -e quasistatic
# autopep8: on

import argparse
import math
import os
from pathlib import Path
import subprocess
from time import time


# create parser for command line arguments
def create_argparser() -> argparse.ArgumentParser:
    parser = argparse.ArgumentParser(description='Base batch arguments')
    parser.add_argument('-b', '--build', action='store_true', help='build examples')
    parser.add_argument('-c', '--ceed', type=str, default='/cpu/self',
                        help='libCEED backend to use with convergence tests')
    parser.add_argument(
        '-e',
        '--example',
        type=str,
        default='static',
        choices=[
            'static',
            'quasistatic',
            'dynamic'],
        help='example to run')
    parser.add_argument('-n', '--nproc', type=int, default=1, help='number of MPI processes')
    parser.add_argument('-r', '--resolutions', nargs='*', type=int, default=[3, 4, 5], help='list of resolutions')
    parser.add_argument('-o', '--options-file',
                        type=Path,
                        default=Path('examples/ymls') / 'ex01-static-elasticity-linear-mms.yml',
                        choices=[
                            Path('examples/ymls') / 'ex01-ceed-bp1.yml',
                            Path('examples/ymls') / 'ex01-ceed-bp2.yml',
                            Path('examples/ymls') / 'ex01-ceed-bp3.yml',
                            Path('examples/ymls') / 'ex01-ceed-bp4.yml',
                            Path('examples/ymls') / 'ex01-static-elasticity-linear-mms.yml',
                            Path('examples/ymls') / 'ex01-static-elasticity-mixed-linear-mms-pcjacobi.yml',
                            Path('examples/ymls') / 'ex01-static-elasticity-mixed-linear-discontinuous-mms-pcjacobi.yml',
                            Path('examples/ymls') /
                            'ex01-static-elasticity-mixed-linear-discontinuous-non-tensor-mms-pcjacobi.yml',
                            Path('examples/ymls') / 'ex02-quasistatic-poroelasticity-linear-mms-pcjacobi.yml',
                        ],
                        help='yml file with base options')
    parser.add_argument('-p', '--orders', type=int, nargs='*', help='list of polynomial orders')
    parser.add_argument('-q', '--quiet', action='store_true', help='quiet mode')
    parser.add_argument('-s', '--save-results', type=Path, default=None, help='save results to CSV file')

    parser.add_argument(
        '--method',
        type=str,
        default='fem',
        choices=[
            'fem',
            'mpm'],
        help='FEM or MPM discretization')

    parser.add_argument('--num-points', type=int, default=27, help='number of material points per element')
    parser.add_argument(
        '--point-location-type',
        type=str,
        default='uniform',
        choices=[
            'uniform',
            'gauss',
            'cell_random'],
        help='initial material point location type')

    return parser


# convert options dict to array for command line
def option_dict_to_array(options: dict) -> list[str]:
    return [value for k in options for value in (f'-{k}', f'{options[k]}')]


# run with dictionary of command line options
def run_ratel_with_options(exe_name: str, options: dict, nproc: int, quiet: bool = True) -> tuple[str, str]:
    # executable path
    exe_path: Path = Path('bin') / (f'ratel-{exe_name}')

    # run configuration
    options_array: list[str] = option_dict_to_array(options)
    command: str = f'{exe_path} ' + ' '.join(options_array)
    if nproc > 1:
        command = f'mpiexec -n {nproc} {command}'
    if not quiet:
        print(f'  $ {command}')
    proc = subprocess.run(command,
                          shell=True,
                          stdout=subprocess.PIPE,
                          stderr=subprocess.PIPE)

    # return output
    return proc.stdout.decode('utf-8'), proc.stderr.decode('utf-8')


# transpose of 2D python list
def transpose(array: list[list[float]]) -> list[list[float]]:
    return list(map(list, zip(*array)))


# compute convergence order
def compute_convergence_order(error_current: float, error_previous: float,
                              h_current: float, h_previous: float) -> float:
    return math.log10(error_current / error_previous) / math.log10(h_current / h_previous)


# read error for a given field from ratel output
def get_field_error(output: str, field: int) -> float:
    l2_error_text: str = f'L2 Error, field {field}: '
    index: int = output.find(l2_error_text)
    index_newline: int = output.find('\n', index)
    return float(output[index + len(l2_error_text):index_newline])


# run a single trial with resolution num_faces
def run_trial(options_file: Path, order: str, num_faces: int, example: str, has_p: bool, nproc: int, ceed: str,
              method: str = 'fem', num_points: int = 27, point_location_type: str = 'uniform', quiet: bool = True) -> list[float]:
    if not quiet:
        print()
        print('-----------------------------------------')
        print(f'Running {example} example')
        print(f'  Number of faces in 1D: {num_faces}')

    # run example with current options
    options: dict = {
        'ceed': ceed,
        'options_file': options_file,
        'dm_plex_box_faces': f'{num_faces},{num_faces},{num_faces}',
        f'order{"s" if has_p else ""}': order,
    }
    if method == 'mpm':
        options['method'] = 'mpm'
        options['mpm_num_points_per_cell'] = num_points
        options['mpm_point_location_type'] = point_location_type
        options['pc_type'] = 'none'
    (stdout, stderr) = run_ratel_with_options(example, options, nproc, quiet)

    if stderr:
        print(f'stderr:\n{stderr}')

    if not quiet:
        print('-----------------------------------------')

    # parse output
    error_u: float = get_field_error(stdout, 0)
    h: float = 1 / num_faces
    if has_p:
        error_p = get_field_error(stdout, 1)
        return [num_faces, h, error_u, error_p]
    else:
        return [num_faces, h, error_u]


# main
if __name__ == '__main__':
    # command line arguments
    args = create_argparser().parse_args()

    if (args.resolutions):
        args.resolutions.sort()
    has_p: bool = 'mixed' in args.options_file.name or 'poroelasticity' in args.options_file.name
    if has_p:
        if not args.orders:
            is_discontinuous: bool = 'discontinuous' in args.options_file.name
            is_non_tensor: bool = 'non-tensor' in args.options_file.name
            if is_discontinuous:
                if is_non_tensor:
                    args.orders = [2, 1]
                else:
                    args.orders = [3, 1]
            else:
                args.orders = [2, 1]
        args.orders.sort(reverse=True)
        if len(args.orders) != 2:
            raise Exception('Two orders required for mixed linear problem')
        orders: str = '{},{}'.format(*args.orders)
    else:
        if not args.orders:
            args.orders = [2]
        if len(args.orders) != 1:
            raise Exception('Only one order required for problem')
        orders = f'{args.orders[0]}'

    # Log run info
    if not args.quiet:
        print('-----------------------------------------')
        print('Ratel Convergence Testing Script')
        print('-----------------------------------------')
        print()
        print('-----------------------------------------')
        print()
        print('Script Options:')
        print(f'  (-b) build: {args.build}')
        print(f'  (-c) ceed: {args.ceed}')
        print(f'  (-e) example: {args.example}')
        print(f'  (-n) nproc: {args.nproc}')
        print(f'  (-o) yml: {args.options_file}')
        print(f'  (-p) orders: {orders}')
        print(f'  (-r) resolutions: {args.resolutions}')
        print()
        print('-----------------------------------------')
        print()
        print('-----------------------------------------')
        print('Ratel build configuration:')
        print('-----------------------------------------')
        subprocess.check_call('make info', shell=True)

    # build examples
    if args.build:
        print('-----------------------------------------')
        print('Building Ratel examples:')
        print('-----------------------------------------')
        subprocess.check_call('make bin -j', shell=True)

    # loop over configurations
    if not args.quiet:
        print()
        print('-----------------------------------------')
        print('Running examples')
        print('-----------------------------------------')
        t0 = time()

    data: list[list[float]] = transpose([run_trial(args.options_file, orders, num_faces, args.example, has_p,
                                        args.nproc, args.ceed, args.method, args.num_points, args.point_location_type, args.quiet) for num_faces in args.resolutions])

    # compute convergence order
    data.insert(3, [0] * len(data[0]))
    data[3][1:] = list(map(compute_convergence_order, data[2][1:], data[2][:-1], data[1][1:], data[1][:-1]))
    if has_p:
        data.insert(5, [0] * len(data[0]))
        data[5][1:] = list(map(compute_convergence_order, data[4][1:], data[4][:-1], data[1][1:], data[1][:-1]))

    # check convergence
    average_u: float = sum(data[3]) / (len(data[3]) - 1)
    if has_p:
        average_p: float = sum(data[5]) / (len(data[5]) - 1)
    results: list[list[float]] = transpose(data)
    index_len: int = math.ceil(math.log10(len(results)))
    if not args.quiet:
        print()
        print('-----------------------------------------')
        print()
        print('Results:')
        if has_p:
            labels: list[str] = [
                'Number of element/direction',
                'Element size',
                'L2 Error u',
                'Convergence order of u',
                'L2 Error p',
                'Convergence order of p']
            formats: list[str] = ['{:d}', '{:0.5f}', '{:0.5g}', '{:0.5f}', '{:0.5g}', '{:0.5f}']
        else:
            labels = ['Number of element/direction', 'Element size', 'L2 Error u', 'Convergence order of u']
            formats = ['{:d}', '{:0.5f}', '{:0.5g}', '{:0.5f}']

        print(' ' * index_len + '  ' + '  '.join(labels))
        for i, row in enumerate(results):
            print(f'{i}'.rjust(index_len), end='  ')
            for value, label, fmt in zip(row, labels, formats):
                print(fmt.format(value).rjust(len(label)), end='  ')
            print()
    if args.save_results:
        labels = ['num_elements', 'element_size', 'error_u', 'order_u']
        formats: list[str] = ['{:d}', '{:.12g}', '{:.12g}', '{:.12g}']
        if has_p:
            labels.extend(['error_p', 'order_p'])
            formats.extend(['{:.12g}', '{:.12g}'])
        with args.save_results.open('w') as out_file:
            out_file.write(','.join(labels) + '\n')
            for row in results:
                out_file.write(','.join([fmt.format(value) for value, fmt in zip(row, formats)]) + '\n')

    is_continuous: bool = 'discontinuous' not in args.options_file.name
    is_non_tensor: bool = 'non-tensor' in args.options_file.name
    tol_u = 3e-1 if (args.method == "mpm" or 'poroelasticity' in args.options_file.name) else 2e-1
    if average_u - (args.orders[0] + (1.0 if (is_continuous or is_non_tensor) else 0.0)) < -tol_u or not args.quiet:
        print(f'  Average convergence order of u: {average_u}')
    tol_p = 5e-1 if 'poroelasticity' in args.options_file.name else 2e-1
    if has_p and (average_p - (args.orders[1] + 1.0) < -tol_p or not args.quiet):
        print(f'  Average convergence order of p: {average_p}')

    if not args.quiet:
        print()
        print(f'Elapsed wall time: {time() - t0}s')
        print()
        print('-----------------------------------------')
