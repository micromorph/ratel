SetFactory("OpenCASCADE");

DefineConstant[
  Nx = {2, Min 2, Max 10, Step 2,
    Name "Parameters/Element in x"}
  Ny = {2, Min 2, Max 10, Step 2,
    Name "Parameters/Element in y"}
  Nz = {1, Min 1, Max 10, Step 1,
    Name "Parameters/Element in z"}
  t = {0.05/Nx, Min 0.001, Max 0.01, Step 0.005,
    Name "Parameters/Thickness"}
];

Point(1) = {0,0,0};
Point(2) = {0.048,0.044,0};
Point(3) = {0.048,0.060,0};
Point(4) = {0,0.044,0};
Line(1) = {1,2};
Line(2) = {2,3};
Line(3) = {3,4};
Line(4) = {4,1};
Curve Loop(1) = {1,2,3,4};

Plane Surface(1) = {1};
Recombine Surface{1};

// All the straight edges should have N elements
Transfinite Curve {1} = Nx+1;
Transfinite Curve {3} = Nx+1;
Transfinite Curve {2} = Ny+1;
Transfinite Curve {4} = Ny+1;

Transfinite Surface {1};

Extrude {0, 0, t} { Surface{1}; Layers{Nz}; Recombine; }
Physical Surface("back", 1) = {1};
Physical Surface("front", 2) = {6};
Physical Surface("left", 3) = {5};
Physical Surface("right", 4) = {3};
Physical Volume("cook-membrane", 5) = {1};
