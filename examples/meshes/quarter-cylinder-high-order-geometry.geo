r = 0.6;
R = 1;
h = 0.5;
nbox = 2;
nouter = 1;
ninner = 1;
nheight = 2;

// 7 -------\
// |        / 10
// 6 --\  9    \
// |     / \    \
// 5 -- 8   \    \
// |    |    |    |
// 1 -- 2 -- 3 -- 4

Point(1) = {0, 0, 0};
Point(2) = {r/2, 0, 0};
Point(3) = {r, 0, 0};
Point(4) = {R, 0, 0};
Point(5) = {0, r/2, 0};
Point(6) = {0, r, 0};
Point(7) = {0, R, 0};
Point(8) = {r/2, r/2, 0};
Point(9) = {r*Sqrt(2)/2, r*Sqrt(2)/2, 0};
Point(10) = {R*Sqrt(2)/2, R*Sqrt(2)/2, 0};

Line(1) = {1, 2};
Line(2) = {2, 3};
Line(3) = {3, 4};
Line(4) = {1, 5};
Line(5) = {5, 6};
Line(6) = {6, 7};
Line(7) = {8, 9};
Line(8) = {9, 10};
Line(9) = {2, 8};
Line(10) = {8, 5};
Circle(11) = {3, 1, 9};
Circle(12) = {9, 1, 6};
Circle(13) = {4, 1, 10};
Circle(14) = {10, 1, 7};
Curve Loop(1) = {1, 9, 10, -4};
Curve Loop(2) = {2, 11, -7, -9};
Curve Loop(3) = {-10, 7, 12, -5};
Curve Loop(4) = {3, 13, -8, -11};
Curve Loop(5) = {-12, 8, 14, -6};
Transfinite Curve {1, 4, 9, 10, 11, 12, 13, 14} = nbox + 1;
Transfinite Curve {2, 5, 7} = ninner + 1;
Transfinite Curve {3, 6, 8} = nouter + 1;

For i In {1:5}
    Plane Surface(i) = {i};
    Transfinite Surface {i};
EndFor
Recombine Surface {:};

e = 1e-4;
bottom_half[] = Extrude {0,0,h}
{
    Surface{1:5};
    Layers{nheight/2};
    Recombine;
};
xy_plane() = Surface In BoundingBox {-R-e, -R-e, h-e, R+e, R+e, R+e};
top_half[] = Extrude {0,0,h}
{
    Surface{xy_plane()};
    Layers{nheight/2};
    Recombine;
};
Coherence;

// set up physical group tags
inner_top() = Surface In BoundingBox {-r-e,-r-e,2*h-e,r+e,r+e,2*h+e};
inner_bottom() = Surface In BoundingBox {-r-e,-r-e,-e,r+e,r+e,e};
outer_cylinder() = Surface In BoundingBox {R-e,-e,-e,-e,2*R,2*h+e};
xz_plane() = Surface In BoundingBox {-R-e, -e, -e, R+e, e, 2*h+e};
yz_plane() = Surface In BoundingBox {-e, -R-e, -e, e, R+e, 2*h+e};

Physical Surface("inner-bottom", 1) = {inner_bottom()};
Physical Surface("inner-top", 2) = {inner_top()};
Physical Surface("sym-xz", 3) = {xz_plane()};
Physical Surface("sym-yz", 4) = {yz_plane()};
Physical Surface("sym-xy", 5) = {xy_plane()};
Physical Surface("outer-bottom", 6) = {4,5};
Physical Surface("outer-top", 7) = {212,234};
Physical Surface("outer-cylinder", 8) = {93,119,203,229};
Physical Volume("volume", 3) = Volume{:};
