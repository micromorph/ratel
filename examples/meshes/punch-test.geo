SetFactory("OpenCASCADE");

DefineConstant[
  nx = {2, Min 1, Max 30, Step 1, Name "Parameters/nx"}
  ny = {2, Min 1, Max 30, Step 1, Name "Parameters/ny"}
  extrude_length = {0.001, Min .001, Max 1, Step .002, Name "Parameters/extrusion length"}
  extrude_layers = {8, Min 1, Max 100, Step 1, Name "Parameters/extrusion layers"}
];
N = nx * ny;
Rectangle(1) = {0, 0, 0, 0.0005, 0.0005, 0};

For i In {0:nx-1}
  For j In {0:ny-1}
    If (i + j > 0)
       Translate {i*0.0005, j*0.0005, 0} { Duplicata { Surface{1}; } }
    EndIf
  EndFor
EndFor

Coherence;

// All the straight edges should have 8 elements
Transfinite Curve {:} = 2+1;

Recombine Surface {1:1+N-1};
Mesh.Algorithm = 8;
Mesh.RecombinationAlgorithm = 3;

Extrude {0, 0, extrude_length} { Surface{1:1+N-1}; Layers{extrude_layers}; Recombine; }

// start is x-y plane, z=0
Physical Surface("start", 1) = {1:4};
// left is y-z plane, x=0
Physical Surface("left", 2) = {8,12};
Physical Surface("right", 3) = {15,18};
// bottom is x-z plane, y=0
Physical Surface("bottom", 4) = {5,14};
Physical Surface("top", 5) = {11,19};
Physical Surface("punch", 6) = {9};
Physical Surface("front", 7) = {13,17,20};

Physical Volume("block") = {1:N};
