SetFactory("OpenCASCADE");

DefineConstant[
  radius = {0.5, Min 0.1, Max 3, Step .1, Name "Parameters/radius"}
  extrude_length = {2, Min .1, Max 2, Step .1, Name "Parameters/extrusion length"}
];
Rectangle(1) = {0, radius, 0, -radius, -2*radius, 0};
Disk(10) = {0, 0, 0, radius};
BooleanDifference(100) = {Surface{10}; Delete;}{Surface{1}; Delete;};

Extrude {0, 0, extrude_length} { Surface{100}; }

Physical Surface("cylinder") = {101,103}; // Half cylinder
Physical Surface("bottom") = {102}; // Bottom flat part
Physical Volume(1) = {1};

