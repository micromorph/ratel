SetFactory("OpenCASCADE");

radius = 0.1;
DefineConstant[
  clmin = {0.05, Min 0.001, Max 0.5, Step 0.001, Name "Parameters/mesh size min"}
  clmax = {0.3, Min 0.001, Max 0.5, Step 0.001, Name "Parameters/mesh size max"}
];
Box(1) = {0, 0, 0, 1, 1, 1};
Sphere(10) = {0.5, 0.5, 0.5, radius};
BooleanFragments{Volume{1}; Delete;}{Volume{10};} // Creates 10 (rod) and 11 (binder)
Coherence;

// adaptive refinement around sinker region
Field[1] = Distance;
Field[1].SurfacesList = {7};
Field[1].Sampling = 24;

Field[2] = Threshold;
Field[2].InField = 1;
Field[2].SizeMin = clmin;
Field[2].SizeMax = clmax;
Field[2].DistMin = radius / 4;
Field[2].DistMax = radius * 2;

Background Field = 2;

Mesh.MeshSizeExtendFromBoundary = 0;
Mesh.MeshSizeFromPoints = 0;
Mesh.MeshSizeFromCurvature = 0;

// Faces labeled 1=z- 2=z+ 3=y- 4=y+ 5=x+ 6=x-
e = 1e-4;
zm() = Surface In BoundingBox {-e, -e, -e, 1+e, 1+e, e};
zp() = Surface In BoundingBox {-e, -e, 1-e, 1+e, 1+e, 1+e};
ym() = Surface In BoundingBox {-e, -e, -e, 1+e, e, 1+e};
yp() = Surface In BoundingBox {-e, 1-e, -e, 1+e, 1+e, 1+e};
xp() = Surface In BoundingBox {1-e, -e, -e, 1+e, 1+e, 1+e};
xm() = Surface In BoundingBox {-e, -e, -e, e, 1+e, 1+e};

Physical Surface("z-", 1) = {zm()};
Physical Surface("z+", 2) = {zp()};
Physical Surface("y-", 3) = {ym()};
Physical Surface("y+", 4) = {yp()};
Physical Surface("x+", 5) = {xp()};
Physical Surface("x-", 6) = {xm()};
Physical Volume("rock", 1) = {10};
Physical Volume ("sand", 2) = {11};
