DefineConstant[
  nelem = {10, Min 2, Max 20, Step 1, Name "Parameters/Mesh/# Elements"}
  nlayers = {1, Min 1, Max 20, Step 1, Name "Parameters/Mesh/# Layers"}
  crack_width = {0.005, Min 0.001, Max 0.1, Step 0.001, Name "Parameters/Geometry/Crack Width"}
  height = {0.05, Min 0.01, Max 0.5, Step 0.01, Name "Parameters/Geometry/Extrude Height"}
];

Mesh.Algorithm = 8;
Mesh.RecombinationAlgorithm = 3;

Point(1) = {0,0,0};
Point(11) = {0,0.5,0};
Point(2) = {0,1,0};
Point(3) = {0.5-crack_width/2,1,0};
Point(4) = {0.5,0.5,0};
Point(5) = {0.5+crack_width/2,1,0};
Point(6) = {1,1,0};
Point(16) = {1,0.5,0};
Point(7) = {1,0,0};
Point(17) = {0.5,0,0};

// Make each square
Line(1) = {1,11};
Line(2) = {11,4};
Line(3) = {4,17};
Line(4) = {17,1};

Curve Loop(1) = {1,2,3,4};

Line(5) = {11,2};
Line(6) = {2,3};
Line(7) = {3,4};
// and -Line 2

Curve Loop(2) = {5,6,7,-2};

Line(8) = {4,5};
Line(9) = {5,6};
Line(10) = {6,16};
Line(11) = {16,4};

Curve Loop(3) = {8,9,10,11};

// -Line 11
Line(12) = {16,7};
Line(13) = {7,17};
// -Line 3

Curve Loop(4) = {-11,12,13,-3};

Transfinite Curve{-2,3,-7,8,-11} = nelem+1 Using Progression 1.2;
Transfinite Curve{4,-1,5,-6,9,-10,12,-13} = nelem+1 Using Progression 1.2;

Plane Surface(1) = {1};
Plane Surface(2) = {2};
Plane Surface(3) = {3};
Plane Surface(4) = {4};

Transfinite Surface{:};

Recombine Surface{:};
Surface Loop(10) = {1,2,3,4};

Extrude {0, 0, height} {
  Surface{1:4}; Layers{nlayers}; Recombine;
}

// Faces labeled 1=z- 2=z+ 3=y- 4=y+ (left) 5=y+ (right) 6=x+ 7=x-
R = 1;
H = height;
e = 1e-4;
zm() = Surface In BoundingBox      {    -e,  -e,  -e,    R+e, R+e,   e};
zp() = Surface In BoundingBox      {    -e,  -e, H-e,    R+e, R+e, H+e};
ym() = Surface In BoundingBox      {    -e,  -e,  -e,    R+e,   e, H+e};
ypleft() = Surface In BoundingBox  {    -e, R-e,  -e,  R/2+e, R+e, H+e};
ypright() = Surface In BoundingBox { R/2-e, R-e,  -e,    R+e, R+e, H+e};
xp() = Surface In BoundingBox      {   R-e,  -e,  -e,    R+e, R+e, H+e};
xm() = Surface In BoundingBox      {    -e,  -e,  -e,      e, R+e, H+e};

Physical Surface("z-", 1) = {zm()};
Physical Surface("z+", 2) = {zp()};
Physical Surface("y-", 3) = {ym()};
Physical Surface("y+left", 4) = {ypleft()};
Physical Surface("y+right", 5) = {ypright()};
Physical Surface("x+", 6) = {xp()};
Physical Surface("x-", 7) = {xm()};

Physical Volume("volume", 1) = {1:4};
