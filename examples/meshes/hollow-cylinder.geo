DefineConstant[
  r_i = {0.75, Min 0.1, Max 0.9, Step 0.1, Name "Parameters/r_i"}
  r_o = {1, Min 0.1, Max 2, Step 0.1, Name "Parameters/r_o"}
  extrude_length = {4, Min .1, Max 10, Step .1, Name "Parameters/extrusion length"}
  extrude_layers = {10, Min 1, Max 100, Step 1, Name "Parameters/extrusion layers"}
  radial_elements = {2, Min 1, Max 100, Step 1, Name "Parameters/radial elements"}
  quadrant_elements = {4, Min 1, Max 100, Step 1, Name "Parameters/quadrant elements"}
];

Point(1) = {0, 0, 0};

Point(2) = {r_i, 0, 0};
Point(3) = {0, r_i, 0};
Point(4) = {-r_i, 0, 0};
Point(5) = {0, -r_i, 0};

Point(6) = {r_o, 0, 0};
Point(7) = {0, r_o, 0};
Point(8) = {-r_o, 0, 0};
Point(9) = {0, -r_o, 0};

Circle(12) = {2, 1, 3};
Circle(13) = {3, 1, 4};
Circle(14) = {4, 1, 5};
Circle(15) = {5, 1, 2};
Circle(16) = {6, 1, 7};
Circle(17) = {7, 1, 8};
Circle(18) = {8, 1, 9};
Circle(19) = {9, 1, 6};

Line(21) = {2, 6};
Line(22) = {3, 7};
Line(23) = {4, 8};
Line(24) = {5, 9};

Curve Loop(1) = {21, 16, -22, -12};
Curve Loop(2) = {22, 17, -23, -13};
Curve Loop(3) = {23, 18, -24, -14};
Curve Loop(4) = {24, 19, -21, -15};
Plane Surface(1) = {1};
Plane Surface(2) = {2};
Plane Surface(3) = {3};
Plane Surface(4) = {4};

Recombine Surface {1:4};
Transfinite Curve {21:24} = radial_elements + 1;
Transfinite Curve {12:19} = quadrant_elements + 1;
Transfinite Surface {1:4};

vol[] = Extrude {0, 0, extrude_length} { Surface{1:4}; Layers{extrude_layers}; Recombine; };
extrude_end() = Surface In BoundingBox {-r_o, -r_o, extrude_length, r_o, r_o, extrude_length};

Printf("vol[] = %d %d %d %d", vol[0], vol[1], vol[2], vol[3]);

Physical Surface("start", 1) = {1:4};
Physical Surface("end", 4) = {extrude_end()};
Physical Volume("cylinder", 1) = {1:4};
