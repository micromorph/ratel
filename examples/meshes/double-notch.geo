// Gmsh project created on Tue Jan 30 11:54:41 2024
//+
Point(1) = {0, 0, 0, 0.5};
//+
Point(2) = {1, 0, 0, 0.5};
//+
Point(3) = {0.7, 0.8, 0, 0.02};
//+
Point(4) = {1, 2, 0, 0.5};
//+
Point(5) = {0, 2, 0, 0.5};
//+
Point(6) = {0.3, 1.2, 0, 0.02};
//+
Line(1) = {1, 2};
//+
Line(2) = {2, 3};
//+
Line(3) = {3, 4};
//+
Line(4) = {4, 5};
//+
Line(5) = {5, 6};
//+
Line(6) = {6, 1};
//+
Curve Loop(1) = {6, 1, 2, 3, 4, 5};
//+
Plane Surface(1) = {1};
//+
Extrude {0, 0, 0.04} {
  Surface{1}; 
}
//+
Physical Volume("volume", 1) = {1};
//+
Physical Surface("front", 6) = {38};
//+
Physical Surface("rear", 5) = {1};
//+
Physical Surface("bottom", 3) = {21};
//+
Physical Surface("top", 4) = {33};
