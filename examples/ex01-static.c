/// @file
/// Ratel static example

const char help[] = "Ratel - static example\n";

#include <petsc.h>
#include <ratel.h>
#include <stddef.h>

int main(int argc, char **argv) {
  MPI_Comm      comm;
  PetscLogStage stage_snes_solve;
  Ratel         ratel;
  SNES          snes;
  DM            dm, dm_solution;
  Vec           U;
  PetscBool     quiet = PETSC_FALSE;

  PetscCall(PetscInitialize(&argc, &argv, NULL, help));
  comm = PETSC_COMM_WORLD;

  // Read command line options
  PetscOptionsBegin(comm, NULL, "Ratel static example", NULL);
  PetscCall(PetscOptionsBool("-quiet", "Suppress summary outputs", NULL, quiet, &quiet, NULL));
  PetscOptionsEnd();

  // Initialize Ratel context and create DM
  PetscCall(RatelInit(comm, &ratel));
  PetscCall(RatelDMCreate(ratel, RATEL_SOLVER_STATIC, &dm));
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));

  // Create SNES
  PetscCall(SNESCreate(comm, &snes));
  PetscCall(RatelSNESSetup(ratel, snes));

  // View Ratel setup
  if (!quiet) {
    // LCOV_EXCL_START
    PetscCall(PetscPrintf(comm, "----- Ratel Static Example -----\n\n"));
    PetscCall(RatelView(ratel, PETSC_VIEWER_STDOUT_WORLD));
    // LCOV_EXCL_STOP
  }

  // Solution vector
  PetscCall(DMCreateGlobalVector(dm_solution, &U));

  // Solve
  PetscCall(RatelSNESSetupInitialCondition(ratel, snes, U));
  PetscCall(PetscLogDefaultBegin());  // So we can use PetscLogStageGetPerfInfo without -log_view
  {
    PetscCall(PetscLogStageRegister("Ratel Solve", &stage_snes_solve));
    PetscCall(PetscLogStagePush(stage_snes_solve));
    PetscCall(SNESSolve(snes, NULL, U));
    PetscCall(PetscLogStagePop());
  }

  // Output solution
  PetscCall(VecViewFromOptions(U, NULL, "-view_final_solution"));
  PetscCall(RatelSNESCheckpointFinalSolutionFromOptions(ratel, snes, U));

  // Report solver info
  {
    SNESConvergedReason reason;
    PetscInt            num_iterations;

    PetscCall(SNESGetIterationNumber(snes, &num_iterations));
    if (!quiet) PetscCall(PetscPrintf(comm, "SNES iterations: %" PetscInt_FMT "\n", num_iterations));
    PetscCall(SNESGetConvergedReason(snes, &reason));
    if (!quiet || reason < SNES_CONVERGED_ITERATING) PetscCall(PetscPrintf(comm, "SNES converged reason: %s\n", SNESConvergedReasons[reason]));
  }

  // Verify MMS
  PetscCall(RatelViewMMSL2ErrorFromOptions(ratel, 1.0, U));

  // Compute and verify strain energy
  PetscCall(RatelViewStrainEnergyErrorFromOptions(ratel, 1.0, U));

  // Compute and verify max displacement
  PetscCall(RatelViewMaxSolutionValuesErrorFromOptions(ratel, 1.0, U));

  // Compute and verify surface forces and centroids
  PetscCall(RatelViewSurfaceForceAndCentroidErrorFromOptions(ratel, 1.0, U));

  // Compute and verify maximum diagnostic quantities
  PetscCall(RatelViewMaxDiagnosticQuantitiesErrorByNameFromOptions(ratel, 1.0, U));

  // Compute and view diagnostic quantities
  PetscCall(RatelViewDiagnosticQuantitiesFromOptions(ratel, 1.0, U));

  // Compute point quantities
  if (dm != dm_solution) {
    PetscBool is_ceed_bp = PETSC_FALSE;

    PetscCall(RatelIsCeedBP(ratel, &is_ceed_bp));
    PetscCall(RatelMPMMeshToSwarm(ratel, U, 1.0, is_ceed_bp));
    PetscCall(RatelDMSwarmViewFromOptions(ratel, dm, "-view_swarm"));
  }

  // Report total solve time
  {
    PetscLogStage      stage_id;
    PetscEventPerfInfo stage_perf_info;

    PetscCall(PetscLogStageGetId("Ratel Solve", &stage_id));
    PetscCall(PetscLogStageGetPerfInfo(stage_id, &stage_perf_info));
    if (!quiet) PetscCall(PetscPrintf(comm, "Time taken to compute solution (sec): %g\n", stage_perf_info.time));
  }

  // Cleanup
  PetscCall(SNESDestroy(&snes));
  PetscCall(DMDestroy(&dm));
  PetscCall(DMDestroy(&dm_solution));
  PetscCall(VecDestroy(&U));
  PetscCall(RatelDestroy(&ratel));

  return PetscFinalize();
}

// ---------- Test Cases ----------

// MPM Tests
//TESTARGS(name="MPM, linear, MMS")                           -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-mpm-linear-mms.yml
//TESTARGS(name="MPM, Neo-Hookean current")                   -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-mpm-neo-hookean-current.yml
//TESTARGS(name="MPM, mixed linear, MMS")                     -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-mpm-mixed-linear-mms.yml
//TESTARGS(name="MPM, mixed linear, MMS PCFieldSplitJacobi")  -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-mpm-mixed-linear-mms-pcfieldsplit-jacobi.yml

// MMS tests
//TESTARGS(name="CEED BP1")               -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-ceed-bp1.yml
//TESTARGS(name="CEED BP2")               -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-ceed-bp2.yml
//TESTARGS(name="CEED BP3")               -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-ceed-bp3.yml
//TESTARGS(name="CEED BP4")               -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-ceed-bp4.yml
//TESTARGS(name="CEED BP4, Kershaw mesh") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-ceed-bp4-kershaw.yml

//TESTARGS(name="linear, MMS") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-linear-mms.yml

//TESTARGS(name="mixed linear, MMS PCJacobi")                        -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-mixed-linear-mms-pcjacobi.yml
//TESTARGS(name="mixed linear, MMS PCFieldSplitJacobi")              -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-mixed-linear-mms-pcfieldsplit-jacobi.yml
//TESTARGS(name="mixed linear, MMS PCFieldSplitSchur")               -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-mixed-linear-mms-pcfieldsplit-schur.yml
//TESTARGS(name="mixed linear, MMS PCFieldSplitPMG")                 -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-mixed-linear-mms-pcfieldsplit-pmg.yml
//TESTARGS(name="mixed linear, MMS PCFieldSplitCholesky",only="cpu") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-mixed-linear-mms-pcfieldsplit-cholesky.yml
//TESTARGS(name="mixed linear, PCFieldSplitPMG")                     -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-mixed-linear-pcfieldsplit-pmg.yml
//TESTARGS(name="mixed linear, PCFieldSplitCholesky",only="cpu")     -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-mixed-linear-pcfieldsplit-cholesky.yml

//TESTARGS(name="mixed discontinuous linear, MMS PCJacobi")            -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-mixed-linear-discontinuous-mms-pcjacobi.yml
//TESTARGS(name="mixed discontinuous non-tensor linear, MMS PCJacobi") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-mixed-linear-discontinuous-non-tensor-mms-pcjacobi.yml

// Material model tests
//TESTARGS(name="Neo-Hookean initial")                      -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-neo-hookean-initial.yml
//TESTARGS(name="Neo-Hookean initial, Kershaw mesh")        -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-neo-hookean-initial-kershaw.yml
//TESTARGS(name="Neo-Hookean current")                      -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-neo-hookean-current.yml
//TESTARGS(name="Neo-Hookean initial AD",only="ad-enzyme")  -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-neo-hookean-initial-ad-enzyme.yml
//TESTARGS(name="Neo-Hookean current AD",only="ad-enzyme")  -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-neo-hookean-current-ad-enzyme.yml

//TESTARGS(name="Mooney-Rivlin initial") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-mooney-rivlin-initial.yml
//TESTARGS(name="Mooney-Rivlin current") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-mooney-rivlin-current.yml

//TESTARGS(name="isochoric Neo-Hookean initial") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-isochoric-neo-hookean-initial.yml
//TESTARGS(name="isochoric Neo-Hookean current") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-isochoric-neo-hookean-current.yml

//TESTARGS(name="isochoric Mooney-Rivlin initial") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-isochoric-mooney-rivlin-initial.yml
//TESTARGS(name="isochoric Mooney-Rivlin current") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-isochoric-mooney-rivlin-current.yml

//TESTARGS(name="isochoric Ogden initial") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-isochoric-ogden-initial.yml
//TESTARGS(name="isochoric Ogden current") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-isochoric-ogden-current.yml

//TESTARGS(name="mixed Neo-Hookean initial") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-mixed-neo-hookean-initial-pcjacobi.yml
//TESTARGS(name="mixed Neo-Hookean current") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-mixed-neo-hookean-current-pcjacobi.yml

//TESTARGS(name="mixed Ogden initial") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-mixed-ogden-initial-pcjacobi.yml

//TESTARGS(name="mixed Neo-Hookean initial, PCFieldSplitCholesky",only="cpu") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-mixed-neo-hookean-initial-pcfieldsplit-cholesky.yml
//TESTARGS(name="mixed Neo-Hookean initial, PCFieldSplitPBJacobi")            -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-mixed-neo-hookean-initial-pcfieldsplit-pbjacobi.yml -fieldsplit_displacement_pc_type pbjacobi
//TESTARGS(name="mixed Neo-Hookean initial, PCFieldSplitVPBJacobi")           -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-mixed-neo-hookean-initial-pcfieldsplit-vpbjacobi.yml

//TESTARGS(name="mixed Neo-Hookean PL initial")                                  -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-mixed-neo-hookean-PL-initial-pcjacobi.yml
//TESTARGS(name="mixed Neo-Hookean PL current")                                  -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-mixed-neo-hookean-PL-current-pcjacobi.yml
//TESTARGS(name="mixed Neo-Hookean PL initial, PCFieldSplitCholesky",only="cpu") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-mixed-neo-hookean-PL-initial-pcfieldsplit-cholesky.yml

// Boundary condition tests
//TESTARGS(name="Neo-Hookean initial, platen")             -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-neo-hookean-initial-platen.yml
//TESTARGS(name="isochoric Neo-Hookean initial, platen")   -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-isochoric-neo-hookean-initial-platen.yml
//TESTARGS(name="isochoric Neo-Hookean current, platen")   -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-isochoric-neo-hookean-current-platen.yml
//TESTARGS(name="isochoric Mooney-Rivlin initial, platen") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-isochoric-mooney-rivlin-initial-platen.yml
//TESTARGS(name="isochoric Mooney-Rivlin current, platen") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-isochoric-mooney-rivlin-current-platen.yml
//TESTARGS(name="isochoric Ogden initial, platen")         -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-isochoric-ogden-initial-platen.yml
//TESTARGS(name="isochoric Ogden current, platen")         -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-isochoric-ogden-current-platen.yml

// Other tests
//TESTARGS(name="Neo-Hookean, face forces continue",only="serial") -ceed {ceed_resource} -quiet -options_file examples/ymls/ex01-static-elasticity-neo-hookean-current-face-forces-continue.yml
