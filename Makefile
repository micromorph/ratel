# ------------------------------------------------------------
# Configuration
# ------------------------------------------------------------

CONFIG ?= config.mk
-include $(CONFIG)
COMMON ?= common.mk
-include $(COMMON)

# Get number of processors of the machine
NPROCS := $(shell getconf _NPROCESSORS_ONLN)
# prepare make options to run in parallel
MFLAGS := -j $(NPROCS) --warn-undefined-variables \
                       --no-print-directory --no-keep-going

# Quiet, color output
quiet ?= $($(1))

# Cancel built-in and old-fashioned implicit rules which we don't use
.SUFFIXES:

.SECONDEXPANSION: # to expand $$(@D)/.DIR

%/.DIR :
	@mkdir -p $(@D)
	@touch $@

.PRECIOUS: %/.DIR


# ------------------------------------------------------------
# Default target
# ------------------------------------------------------------

all : bin


# ------------------------------------------------------------
# Phony targets
# ------------------------------------------------------------

# These targets are not files but rather commands to run
.PHONY : all bin clean configure doxygen doc examples info info-basic install iwyu junit par test prove prove-all junit style tidy


# ------------------------------------------------------------
# Compiler flags
# ------------------------------------------------------------

ifeq (,$(filter-out undefined default,$(origin LINK)))
  LINK = $(CC)
endif
ifeq (,$(filter-out undefined default,$(origin AR)))
  AR = ar
endif
ifeq (,$(filter-out undefined default,$(origin ARFLAGS)))
  ARFLAGS = crD
endif

pkgconf-path = $(if $(wildcard $(1)/$(2).pc),$(1)/$(2).pc,$(2))

# Library dependencies
# Note: PETSC_ARCH can be undefined or empty for installations which do not use
#       PETSC_ARCH - for example when using PETSc installed through Spack.
ifneq ($(wildcard ../petsc/lib/libpetsc.*),)
  PETSC_DIR ?= ../petsc
endif
petsc.pc := $(call pkgconf-path,$(PETSC_DIR)/$(PETSC_ARCH)/lib/pkgconfig,petsc)

CEED_DIR ?= $(if $(wildcard $(PETSC_DIR)/$(PETSC_ARCH)/lib/pkgconfig/ceed.pc),$(PETSC_DIR)/$(PETSC_ARCH),../libCEED)
ceed.pc  := $(call pkgconf-path,$(CEED_DIR)/lib/pkgconfig,ceed)

pkgconf   = $(shell pkg-config $1 | $(SED) -e 's/^"//g' -e 's/"$$//g')

# Error checking flags
PEDANTIC      ?=
PEDANTICFLAGS ?= -Werror -pedantic

# Options from library dependencies
CC       = $(call pkgconf, --variable=ccompiler $(petsc.pc) $(ceed.pc))
CXX      = $(call pkgconf, --variable=cxxcompiler $(petsc.pc) $(ceed.pc))
CFLAGS   = \
  $(filter-out -fvisibility=hidden, $(call pkgconf, --variable=cflags_extra $(petsc.pc))) \
  $(filter-out -fvisibility=hidden, $(call pkgconf, --cflags-only-other $(petsc.pc))) \
  $(if $(PEDANTIC),$(PEDANTICFLAGS))
CPPFLAGS = $(call pkgconf, --cflags-only-I $(petsc.pc) $(ceed.pc)) \
  $(call pkgconf, --variable=cflags_dep $(petsc.pc)) \
  $(if $(PEDANTIC),$(PEDANTICFLAGS))
CXXFLAGS = \
  $(filter-out -fvisibility=hidden, $(call pkgconf, --variable=cxxflags_extra $(petsc.pc))) \
  $(filter-out -fvisibility=hidden, $(call pkgconf, --cflags-only-other $(petsc.pc))) \
  $(if $(PEDANTIC),$(PEDANTICFLAGS))

LDFLAGS    ?=
UNDERSCORE ?= 1

# ASAN must be left empty if you don't want to use it
ASAN    ?=

AFLAGS  ?= -fsanitize=address
CFLAGS  += $(if $(ASAN),$(AFLAGS))
FFLAGS  += $(if $(ASAN),$(AFLAGS))
LDFLAGS += $(if $(ASAN),$(AFLAGS))

CFLAGS   += $(OPT)
CXXFLAGS += $(OPT)

# Library options
BUILDDIR := $(if $(BUILD_ARCH),$(PETSC_ARCH)/,)
OBJDIR   := $(BUILDDIR)build
BINDIR   := $(BUILDDIR)$(if $(for_install),$(OBJDIR)/bin,bin)
for_install := $(filter install,$(MAKECMDGOALS))
LIBDIR   := $(if $(for_install),$(OBJDIR),$(BUILDDIR)lib)
SRCDIR   := src
DARWIN   := $(filter Darwin,$(shell uname -s))
SO_EXT   := $(if $(DARWIN),dylib,so)

CPPFLAGS += -I./include
LDLIBS    = -lm
PYTHON   ?= python3
SED      ?= sed

# Installation variables
prefix      ?= /usr/local
bindir       = $(prefix)/bin
libdir      := $(prefix)/lib
includedir   = $(prefix)/include
pkgconfigdir = $(libdir)/pkgconfig
INSTALL         = install
INSTALL_PROGRAM = $(INSTALL)
INSTALL_DATA    = $(INSTALL) -m644
INSTALL_LIB     = $(INSTALL) $(if $(STATIC),-m644)


# ------------------------------------------------------------
# Optional dependencies
# ------------------------------------------------------------

# Enzyme-AD
ENZYME_LIB ?=
ENZYMEFLAGS = -fpass-plugin=$(ENZYME_LIB)

# ADOL-C
WITH_ADOLC ?=
ifneq ($(WITH_ADOLC),)
    # Attempt to fetch paths using pkg-config
    ADOLC_INCLUDE := $(shell pkg-config --cflags-only-I adolc 2>/dev/null | sed 's/-I//')
    ADOLC_LIB := $(shell pkg-config --libs-only-L adolc 2>/dev/null | sed 's/-L//')

    # Fallback to searching standard system paths if pkg-config fails
    ifeq ($(ADOLC_INCLUDE),)
        ADOLC_INCLUDE := $(shell find /usr/include /usr/local/include -type d -name "adolc" 2>/dev/null | head -n 1)
    endif
    ifeq ($(ADOLC_LIB),)
        ADOLC_LIB := $(shell find /usr/lib /usr/local/lib -name "libadolc.*" -printf "%h\n" 2>/dev/null | head -n 1)
    endif

    ifeq ($(ADOLC_INCLUDE),)
        $(error Could not find ADOL-C include directory.)
    endif
    ifeq ($(ADOLC_LIB),)
        $(error Could not find ADOL-C library directory.)
    endif
endif
ifneq ($(ADOLC_DIR),)
	ADOLC_INCLUDE ?= $(ADOLC_DIR)/include
	ADOLC_LIB ?= $(ADOLC_DIR)/lib
endif


# ------------------------------------------------------------
# View configuration options
# ------------------------------------------------------------

# Diagnostic information
info-basic:
	$(info -----------------------------------------)
	$(info |      ____            __           __  |)
	$(info |     / __ \  ____ _  / /_  ___    / /  |)
	$(info |    / /_/ / / __ `/ / __/ / _ \  / /   |)
	$(info |   / _, _/ / /_/ / / /_  /  __/ / /    |)
	$(info |  /_/ |_|  \__,_/  \__/  \___/ /_/     |)
	$(info -----------------------------------------)
	$(info )
	$(info -----------------------------------------)
	$(info )
	$(info Dependencies:)
	$(info CEED_DIR      = $(CEED_DIR))
	$(info PETSC_DIR     = $(PETSC_DIR))
	$(info PETSC_ARCH    = $(PETSC_ARCH))
	$(info )
	$(info Optional Dependencies:)
	$(info ENZYME_LIB    = $(or $(ENZYME_LIB),(not found)))
	$(info ADOLC_LIB     = $(or $(ADOLC_LIB),(not found)))
	$(info ADOLC_INCLUDE = $(or $(ADOLC_INCLUDE),(not found)))
	$(info )
	$(info -----------------------------------------)
	$(info )
	@true

info:
	$(info -----------------------------------------)
	$(info |      ____            __           __  |)
	$(info |     / __ \  ____ _  / /_  ___    / /  |)
	$(info |    / /_/ / / __ `/ / __/ / _ \  / /   |)
	$(info |   / _, _/ / /_/ / / /_  /  __/ / /    |)
	$(info |  /_/ |_|  \__,_/  \__/  \___/ /_/     |)
	$(info -----------------------------------------)
	$(info )
	$(info -----------------------------------------)
	$(info )
	$(info Dependencies:)
	$(info CEED_DIR       = $(CEED_DIR))
	$(info PETSC_DIR      = $(PETSC_DIR))
	$(info PETSC_ARCH     = $(PETSC_ARCH))
	$(info )
	$(info Optional Dependencies:)
	$(info ENZYME_LIB     = $(or $(ENZYME_LIB),(not found)))
	$(info ADOLC_LIB      = $(or $(ADOLC_LIB),(not found)))
	$(info ADOLC_INCLUDE  = $(or $(ADOLC_INCLUDE),(not found)))
	$(info )
	$(info -----------------------------------------)
	$(info )
	$(info Build Options:)
	$(info CC             = $(CC))
	$(info CFLAGS         = $(CFLAGS))
	$(info CPPFLAGS       = $(CPPFLAGS))
	$(info LDFLAGS        = $(LDFLAGS))
	$(info LDLIBS         = $(LDLIBS))
	$(info AR             = $(AR))
	$(info ARFLAGS        = $(ARFLAGS))
	$(info OPT            = $(OPT))
	$(info AFLAGS         = $(AFLAGS))
	$(info ASAN           = $(or $(ASAN),(empty)))
	$(info VERBOSE        = $(or $(V),(empty)) [verbose=$(if $(V),on,off)])
	$(info )
	$(info -----------------------------------------)
	$(info )
	$(info Install Options:)
	$(info prefix         = $(prefix))
	$(info includedir     = $(includedir))
	$(info libdir         = $(libdir))
	$(info pkgconfigdir   = $(pkgconfigdir))
	$(info )
	$(info -----------------------------------------)
	$(info )
	$(info Format and Style Options:)
	$(info CLANG_FORMAT   = $(CLANG_FORMAT))
	$(info FORMAT_OPTS    = $(FORMAT_OPTS))
	$(info CLANG_TIDY     = $(CLANG_TIDY))
	$(info TIDY_OPTS      = $(TIDY_OPTS))
	$(info TIDY_FILE_OPTS = $(TIDY_FILE_OPTS))
	$(info )
	$(info -----------------------------------------)
	$(info )
	@true


# ------------------------------------------------------------
# Source files
# ------------------------------------------------------------

libratel.c  := $(filter-out src/internal/ratel-jit-source-root-$(if $(for_install),default,install).c %-ad-enzyme.c %-ad-adolc.c, $(sort $(wildcard $(SRCDIR)/*.c)) $(sort $(wildcard $(SRCDIR)/**/*.c)))

# Enzyme-AD
ifneq ($(ENZYME_LIB),)
	CFLAGS += $(ENZYMEFLAGS)
	libratel.c += $(sort $(wildcard $(SRCDIR)/**/*-ad-enzyme.c))
endif

# ADOL-C
ifneq ($(and $(ADOLC_LIB),$(ADOLC_INCLUDE)),)
    WITH_ADOLC := 1
    $(libratel.so):	LDFLAGS  += -L$(ADOLC_LIB) $(RPATH_FLAG)$(ADOLC_LIB)
    $(libratel.so): CPPFLAGS += -I$(ADOLC_INCLUDE)
endif
ifneq ($(WITH_ADOLC),)
    $(libratel.so): LDLIBS   += -ladolc
    $(libratel.so): CXXFLAGS += -fPIC
	libratel.c += $(sort $(wildcard $(SRCDIR)/**/*-ad-adolc.c))
	libratel.cpp += $(sort $(wildcard $(SRCDIR)/**/*-ad-adolc-cpp.cpp))
endif

# Tests
tests.c := $(sort $(wildcard tests/t[0-9][0-9][0-9]-*.c))
tests   := $(tests.c:tests/%.c=$(OBJDIR)/%)

# Examples
examples.c := $(sort $(wildcard examples/*.c))
examples   := $(examples.c:examples/%.c=$(OBJDIR)/%)
examples   :  $(examples)


# ------------------------------------------------------------
# Building core library components
# ------------------------------------------------------------

# Ratel
ratel.pc    := $(LIBDIR)/pkgconfig/ratel.pc
libratel.so := $(LIBDIR)/libratel.$(SO_EXT)
libratel.a  := $(LIBDIR)/libratel.a
libratel    := $(if $(STATIC),$(libratel.a),$(libratel.so))
RATEL_LIB    = -lratel

# Library
lib: $(libratel) $(ratel.pc) $(examples) | $(petsc.pc) $(ceed.pc)
# run 'lib' target in parallel
par:;@$(MAKE) $(MFLAGS) V=$(V) lib

SYMLINK = ln -sf

$(libratel.so) : LDFLAGS += $(if $(DARWIN), -install_name @rpath/$(notdir $(libratel.so)))

# Collect list of libraries and paths for use in linking and pkg-config
RPATH_FLAG   := $(call pkgconf, --variable=ldflag_rpath $(petsc.pc))
PKG.pc       := $(petsc.pc) $(ceed.pc)
PKG_LDLIBS    = $(call pkgconf, --libs-only-l --libs-only-other $(PKG.pc))
PKG_L         = $(call pkgconf, --libs-only-L $(PKG.pc))
PKG_LDFLAGS   = $(PKG_L) $(patsubst -L%,$(RPATH_FLAG)%,$(PKG_L))
RATEL_LDFLAGS = -L$(abspath $(LIBDIR)) $(if $(STATIC),,$(RPATH_FLAG)$(if $(for_install),"$(libdir)",$(abspath $(LIBDIR))))

_pkg_ldflags = $(filter -L%,$(PKG_LDFLAGS))
_pkg_ldlibs  = $(filter-out -L%,$(PKG_LDLIBS))
$(libratel) : LDFLAGS += $(_pkg_ldflags) $(_pkg_ldflags:-L%=$(RPATH_FLAG)%)
$(libratel) : LDLIBS  += $(_pkg_ldlibs)
pkgconfig-libs-private = $(PKG_LDLIBS)

# File names *-weak.c contain weak symbol definitions, which must be listed last
# when creating shared or static libraries.
weak_last = $(filter-out %-weak.o,$(1)) $(filter %-weak.o,$(1))

libratel.o = $(libratel.c:%.c=$(OBJDIR)/%.o) $(libratel.cpp:%.cpp=$(OBJDIR)/%.o)
$(libratel.o)  : CPPFLAGS += $(if $(filter 1,$(UNDERSCORE)),-DUNDERSCORE)
$(libratel.o)  : | info-basic
$(libratel.so) : $(call weak_last,$(libratel.o)) | $$(@D)/.DIR
	$(call quiet,LINK) $(LDFLAGS) -shared -o $@ $^ $(LDLIBS)

$(libratel.a)  : $(libratel.o) | $$(@D)/.DIR
	$(call quiet,AR) $(ARFLAGS) $@ $^

$(OBJDIR)/%.o : $(CURDIR)/%.c | $$(@D)/.DIR
	$(call quiet,CC) $(CPPFLAGS) $(CFLAGS) -c -o $@ $(abspath $<)

$(OBJDIR)/%.o : $(CURDIR)/%*.cpp | $$(@D)/.DIR
	$(call quiet,CXX) $(CPPFLAGS) $(CXXFLAGS) -c -o $@ $(abspath $<)

$(ratel.pc)   : pkgconfig-prefix = $(abspath .)
$(OBJDIR)/ratel.pc : pkgconfig-prefix = $(prefix)
.INTERMEDIATE : $(OBJDIR)/ratel.pc
%/ratel.pc    : ratel.pc.template | $$(@D)/.DIR
	@$(SED) \
	    -e "s:%prefix%:$(pkgconfig-prefix):" \
	    -e "s:%libs_private%:$(pkgconfig-libs-private):" $< > $@

$(OBJDIR)/src/internal/ratel-jit-source-root-default.o : CPPFLAGS += -DRATEL_JIT_SOURCE_ROOT_DEFAULT="\"$(abspath ./include)/\""
$(OBJDIR)/src/internal/ratel-jit-source-root-install.o : CPPFLAGS += -DRATEL_JIT_SOURCE_ROOT_DEFAULT="\"$(abspath $(includedir))/\""


# ------------------------------------------------------------
# Building example binaries
# ------------------------------------------------------------

$(BINDIR)/ratel-static : $(OBJDIR)/ex01-static | $(BINDIR)/.DIR
	$(call quiet,SYMLINK) $(abspath $<) $@

$(BINDIR)/ratel-quasistatic : $(OBJDIR)/ex02-quasistatic | $(BINDIR)/.DIR
	$(call quiet,SYMLINK) $(abspath $<) $@

$(BINDIR)/ratel-dynamic : $(OBJDIR)/ex03-dynamic | $(BINDIR)/.DIR
	$(call quiet,SYMLINK) $(abspath $<) $@

# Example binaries
BINARIES = \
  $(BINDIR)/ratel-static \
  $(BINDIR)/ratel-quasistatic \
  $(BINDIR)/ratel-dynamic

bin: $(BINARIES)


# ------------------------------------------------------------
# Building tests and examples
# ------------------------------------------------------------

$(OBJDIR)/% : tests/%.c | $$(@D)/.DIR
	$(call quiet,LINK.c) -o $@ $(abspath $<) $(RATEL_LIB) $(LDLIBS)

$(OBJDIR)/% : examples/%.c | $$(@D)/.DIR
	$(call quiet,LINK.c) -o $@ $(abspath $<) $(RATEL_LIB) $(LDLIBS)

$(examples) : $(libratel)
$(tests)    : $(libratel)
$(tests) $(examples) : LDFLAGS += $(RATEL_LDFLAGS) $(PKG_LDFLAGS)
$(tests) $(examples) : LDLIBS  += $(PKG_LDLIBS)


# ------------------------------------------------------------
# Testing
# ------------------------------------------------------------

# Flag for code coverage
ifeq ($(COVERAGE), 1)
  CFLAGS  += --coverage
  LDFLAGS += --coverage
endif

# Set libCEED backends for testing
CEED_BACKENDS ?= /cpu/self
export CEED_BACKENDS

# Set number processes for testing
NPROC_TEST ?= 1
export NPROC_TEST

# Set pool size for testing
NPROC_POOL ?= 1
export NPROC_POOL

run-% : $(OBJDIR)/%
	@$(PYTHON) tests/junit.py --petsc-arch $(or $(PETSC_ARCH),$(PETSC_DIR)) --ceed-backends $(CEED_BACKENDS) $(if $(ENZYME_LIB), --enzyme-lib $(ENZYME_LIB)) $(if $(WITH_ADOLC), --with-adolc $(WITH_ADOLC) )--mode tap --nproc $(NPROC_TEST) --pool-size $(NPROC_POOL) $(<:$(OBJDIR)/%=%)

# The test and prove targets can be controlled via pattern searches. The default
# is to run all tests and examples. Examples of finer grained control:
#
#   make prove search='t3'    # t3xx series tests
#   make junit search='t ex'  # core tests and examples
search    ?= t ex
realsearch = $(search:%=%%)
matched    = $(foreach pattern,$(realsearch),$(filter $(OBJDIR)/$(pattern),$(tests) $(examples)))

# Test Ratel
test    : $(matched:$(OBJDIR)/%=run-%)

tst     : ;@$(MAKE) $(MFLAGS) V=$(V) test

# Test with JUNIT output
JUNIT_BATCH ?= ''

junit-% : $(OBJDIR)/%
	@printf "  %10s %s\n" TEST $(<:$(OBJDIR)/%=%); $(PYTHON) tests/junit.py --junit-batch $(JUNIT_BATCH) --petsc-arch $(or $(PETSC_ARCH),$(PETSC_DIR)) --ceed-backends $(CEED_BACKENDS) $(if $(ENZYME_LIB), --enzyme-lib $(ENZYME_LIB)) $(if $(WITH_ADOLC), --with-adolc $(WITH_ADOLC) )--nproc $(NPROC_TEST) --pool-size $(NPROC_POOL) $(<:$(OBJDIR)/%=%)

junit   : $(matched:$(OBJDIR)/%=junit-%)

junit-convergence: bin
	$(info Running convergence tests)
	$(PYTHON) tests/junit.py --junit-batch $(JUNIT_BATCH) --petsc-arch $(or $(PETSC_ARCH),$(PETSC_DIR)) --ceed-backends $(CEED_BACKENDS) $(if $(ENZYME_LIB), --enzyme-lib $(ENZYME_LIB)) $(if $(WITH_ADOLC), --with-adolc $(WITH_ADOLC) )--nproc $(NPROC_TEST) ratel-convergence-test.py

# Testing with TAP format
# https://testanything.org/tap-specification.html
PROVE      ?= prove
PROVE_OPTS ?= -j $(NPROCS)

prove   : $(matched)
	$(info Running unit tests)
	$(info - Testing with libCEED backends: $(CEED_BACKENDS))
	$(info - Testing on $(NPROC_TEST) processes)
	$(PROVE) $(PROVE_OPTS) --exec '$(PYTHON) tests/junit.py --petsc-arch $(or $(PETSC_ARCH),$(PETSC_DIR)) --ceed-backends $(CEED_BACKENDS) $(if $(ENZYME_LIB), --enzyme-lib $(ENZYME_LIB)) $(if $(WITH_ADOLC), --with-adolc $(WITH_ADOLC) )--mode tap --nproc $(NPROC_TEST) --pool-size $(NPROC_POOL)' $(matched:$(OBJDIR)/%=%)

prv     : ;@$(MAKE) $(MFLAGS) V=$(V) prove

prove-all :
	+$(MAKE) prove realsearch=%

prove-convergence: bin
	$(info Runnig convergence tests)
	$(info - Testing with libCEED backends: $(CEED_BACKENDS))
	$(info - Testing on $(NPROC_TEST) processes)
	$(PROVE) $(PROVE_OPTS) --exec '$(PYTHON) tests/junit.py --petsc-arch $(or $(PETSC_ARCH),$(PETSC_DIR)) --ceed-backends $(CEED_BACKENDS) $(if $(ENZYME_LIB), --enzyme-lib $(ENZYME_LIB)) $(if $(WITH_ADOLC), --with-adolc $(WITH_ADOLC) )--mode tap --nproc $(NPROC_TEST)' ratel-convergence-test.py


# ------------------------------------------------------------
# Installation
# ------------------------------------------------------------

install : $(libratel) $(OBJDIR)/ratel.pc $(BINARIES)
	$(INSTALL) -d $(addprefix "$(DESTDIR)","$(includedir)" \
      "$(bindir)" "$(libdir)" "$(pkgconfigdir)" "$(includedir)/ratel/" \
	  "$(includedir)/ratel/models/" \
	  "$(includedir)/ratel/qfunctions/" \
	  "$(includedir)/ratel/qfunctions/boundaries/" \
	  "$(includedir)/ratel/qfunctions/forcing/" \
	  "$(includedir)/ratel/qfunctions/models/")
	$(INSTALL_DATA) include/ratel.h "$(DESTDIR)$(includedir)/"
	$(INSTALL_DATA) include/ratel/ratel.h "$(DESTDIR)$(includedir)/ratel/"
	$(INSTALL_DATA) include/ratel/models/*.h "$(DESTDIR)$(includedir)/ratel/models/"
	$(INSTALL_DATA) include/ratel/qfunctions/*.h "$(DESTDIR)$(includedir)/ratel/qfunctions/"
	$(INSTALL_DATA) include/ratel/qfunctions/boundaries/*.h "$(DESTDIR)$(includedir)/ratel/qfunctions/boundaries"
	$(INSTALL_DATA) include/ratel/qfunctions/forcing/*.h "$(DESTDIR)$(includedir)/ratel/qfunctions/forcing"
	$(INSTALL_DATA) include/ratel/qfunctions/models/*.h "$(DESTDIR)$(includedir)/ratel/qfunctions/models"
	$(INSTALL_LIB) $(libratel) "$(DESTDIR)$(libdir)/"
	$(INSTALL_DATA) $(OBJDIR)/ratel.pc "$(DESTDIR)$(pkgconfigdir)/"
	$(INSTALL_PROGRAM) $(BINDIR)/* "$(DESTDIR)$(bindir)/"


# ------------------------------------------------------------
# Cleaning
# ------------------------------------------------------------

cln clean :
	$(RM) -r $(OBJDIR) $(LIBDIR) bin dist xml *.vtk *.vtu *.csv *.cgns *.out *.err

distclean : clean
	$(RM) -r doc/html doc/build $(CONFIG)


# ------------------------------------------------------------
# Documentation
# ------------------------------------------------------------

DOXYGEN ?= doxygen
DOXYGENOPTS ?= -q
doxygen :
	$(DOXYGEN) $(DOXYGENOPTS) Doxyfile

SPHINXOPTS      =
SPHINXBUILD     = sphinx-build
SPHINXAUTOBUILD = sphinx-autobuild
SPHINXPROJ      = Ratel
SPHINXBUILDDIR  = doc/build

doc-html doc-dirhtml doc-latexpdf doc-epub doc-help : doc-% : doxygen
	@$(SPHINXBUILD) -M $* . "$(SPHINXBUILDDIR)" $(SPHINXOPTS)

doc-livehtml : doxygen
	@$(SPHINXAUTOBUILD) . "$(SPHINXBUILDDIR)" $(SPHINXOPTS)

doc : doc-html


# ------------------------------------------------------------
# Linting utilities
# ------------------------------------------------------------

# Style
CLANG_FORMAT  ?= clang-format
FORMAT_OPTS   += -style=file -i
AUTOPEP8      ?= autopep8
AUTOPEP8_OPTS += --in-place --aggressive --max-line-length 120
SED_FMT_OPTS  += -r 's/\s+$$//' -i

%.format : %
	$(call quiet,CLANG_FORMAT) $(FORMAT_OPTS) $^

format.ch := $(shell git ls-files '*.[ch]pp' '*.[ch]' 2> /dev/null)
format.py := $(filter-out tests/junit-xml/junit_xml/__init__.py, $(shell git ls-files '*.py' 2> /dev/null))
format.ot := $(shell git ls-files '*.md' 2> /dev/null)

format-c  :
	$(call quiet,CLANG_FORMAT) $(FORMAT_OPTS) $(format.ch)

format-py :
	$(call quiet,AUTOPEP8) $(AUTOPEP8_OPTS) $(format.py)

format-ot :
	$(call quiet,SED) $(SED_FMT_OPTS) $(format.ot)

format    : format-c format-py format-ot

# Tidy
CLANG_TIDY     ?= clang-tidy
TIDY_OPTS      ?= --quiet
TIDY_FILE_OPTS += $(CPPFLAGS) --std=c99 -DRATEL_JIT_SOURCE_ROOT_DEFAULT="\"$(abspath ./include)/\""

%.c.tidy : %.c
	$(call quiet,CLANG_TIDY) $(TIDY_OPTS) $^ -- $(TIDY_FILE_OPTS)

tidy-c   : $(libratel.c:%=%.tidy)
tidy-cpp : $(libratel.cpp:%=%.tidy)

tidy     : tidy-c tidy-cpp

# Include-What-You-Use
ifneq ($(wildcard ../iwyu/*),)
  IWYU_DIR ?= ../iwyu
  IWYU_CC  ?= $(IWYU_DIR)/build/bin/include-what-you-use
endif
iwyu :
	 $(MAKE) -B CC=$(IWYU_CC)


# ------------------------------------------------------------
# Configuration caching
# ------------------------------------------------------------

# "make configure" detects any variables passed on the command line or
# previously set in config.mk, caching them in config.mk as simple
# (:=) variables.  Variables set in config.mk or on the command line
# take precedence over the defaults provided in the file.  Typical
# usage:
#
#   make configure CC=/path/to/my/cc CUDA_DIR=/opt/cuda
#   make
#   make prove
#
# The values in the file can be updated by passing them on the command
# line, e.g.,
#
#   make configure CC=/path/to/other/clang

# All variables to consider for caching
CONFIG_VARS = CEED_DIR PETSC_DIR PETSC_ARCH OPT CFLAGS CPPFLAGS AR ARFLAGS LDFLAGS LDLIBS SED

# $(call needs_save,CFLAGS) returns true (a nonempty string) if CFLAGS
# was set on the command line or in config.mk (where it will appear as
# a simple variable).
needs_save = $(or $(filter command line,$(origin $(1))),$(filter simple,$(flavor $(1))))

configure :
	$(file > $(CONFIG))
	$(foreach v,$(CONFIG_VARS),$(if $(call needs_save,$(v)),$(file >> $(CONFIG),$(v) := $($(v)))))
	@echo "Configuration cached in $(CONFIG):"
	@cat $(CONFIG)

print-% :
	$(info [ variable name]: $*)
	$(info [        origin]: $(origin $*))
	$(info [        flavor]: $(flavor $*))
	$(info [         value]: $(value $*))
	$(info [expanded value]: $($*))
	$(info )
	@true

# Dependencies
# Include *.d deps when not -B = --always-make: useful if the paths are wonky in a container
-include $(if $(filter B,$(MAKEFLAGS)),,$(libratel.c:%.c=$(OBJDIR)/%.d) $(tests.c:tests/%.c=$(OBJDIR)/%.d))
