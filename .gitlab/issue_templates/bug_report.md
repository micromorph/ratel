Summary
---

Please add a brief description here.



Steps to reproduce
---

Please provide the command line options and `yml` needed to reproduce the issue.

```console
$ mpiexec -n 2 ratel-quasistatic -options_file the_bad_test.yml
```

```yml
# Ratel problem specification
# Buggy configuration

```
