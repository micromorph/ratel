The Ratel Singularity def files use the Docker images generated in Ratel CI/CD to build a Singularity container.

Alternatively, users can directly use Docker images for a Singularity container from the command line.

```console
$ singularity run docker://registry.gitlab.com/micromorph/ratel
```
