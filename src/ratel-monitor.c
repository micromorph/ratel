/// @file
/// Implementation of Ratel solver monitor interfaces

#include <ceed-evaluator.h>
#include <ceed.h>
#include <petscdm.h>
#include <petscdmswarm.h>
#include <petscts.h>
#include <ratel-diagnostic.h>
#include <ratel-impl.h>
#include <ratel-material.h>
#include <ratel-monitor.h>
#include <ratel-mpm.h>
#include <ratel.h>
#include <stddef.h>
#include <stdio.h>

/// @addtogroup RatelCore
/// @{

/**
  @brief Write data and series at a given timestep for VTK series file.

  Collective across MPI processes.

  @param[in]  ratel_viewer  `RatelViewer` storing `PetscViewerASCII` object as its `aux_viewer`
  @param[in]  steps         Timestep number
  @param[in]  time          Timestep time
  @param[in]  U             Solution vector

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelViewerWriteTimestep_VTKSeries(RatelViewer ratel_viewer, PetscInt steps, PetscReal time, Vec U) {
  Ratel       ratel  = ratel_viewer->ratel;
  PetscViewer viewer = ratel_viewer->viewer, series_viewer = ratel_viewer->aux_viewer;
  char        vtk_filename[PETSC_MAX_PATH_LEN];

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel PetscViewer VTK Series Write Timestep"));

  // Write vtk file for timestep
  PetscCall(RatelViewerGetSequenceFilename(ratel_viewer, steps, sizeof(vtk_filename), vtk_filename));
  PetscCall(PetscViewerFileSetName(viewer, vtk_filename));
  PetscCall(VecView(U, viewer));
  PetscCall(PetscViewerFlush(viewer));

  // Write line of series file
  PetscCall(PetscViewerASCIIPrintf(series_viewer, "{\n"));
  PetscCall(PetscViewerASCIIPushTab(series_viewer));
  PetscCall(PetscViewerASCIIPrintf(series_viewer, "\"name\": \"%s\",\n", vtk_filename));
  // 17 significant digits is sufficient to exactly represent a double in base 10
  // doi:10.1109/IEEESTD.2019.8766229
  PetscCall(PetscViewerASCIIPrintf(series_viewer, "\"time\": %0.17g\n", time));
  PetscCall(PetscViewerASCIIPopTab(series_viewer));
  // Note, no trailing comma or newline, handled in next iteration
  PetscCall(PetscViewerASCIIPrintf(series_viewer, "},\n"));
  PetscCall(PetscViewerFlush(series_viewer));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel PetscViewer VTK Series Write Timestep Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief `TSMonitor` function for strain energy.

  Collective across MPI processes.

  @param[in]  ts     `TS` object to monitor
  @param[in]  steps  Iteration number
  @param[in]  time   Current time
  @param[in]  U      Current state vector
  @param[in]  ctx    Monitor context holding `Ratel` context object

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSMonitorStrainEnergy(TS ts, PetscInt steps, PetscReal time, Vec U, void *ctx) {
  RatelViewer strain_energy_viewer = (RatelViewer)ctx;
  Ratel       ratel                = strain_energy_viewer->ratel;
  PetscScalar strain_energy        = 0;
  PetscBool   should_write;

  PetscFunctionBeginUser;
  PetscCall(RatelViewerShouldWrite(strain_energy_viewer, ts, steps, &should_write));
  if (!should_write) PetscFunctionReturn(PETSC_SUCCESS);
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel TS Monitor Strain Energy"));

  // Write header, if needed
  PetscCall(RatelViewerWriteHeader(strain_energy_viewer, (void *)"step,time,strain_energy"));

  // Compute strain energy
  PetscCall(RatelComputeStrainEnergy(ratel, U, time, &strain_energy));

  // Write strain energy to viewer
  if (strain_energy_viewer->viewer_format == PETSC_VIEWER_ASCII_CSV) {
    PetscCall(PetscViewerASCIIPrintf(strain_energy_viewer->viewer, "%" PetscInt_FMT ",%0.17g,%0.12e\n", steps, time, strain_energy));
  } else {
    PetscCall(
        PetscViewerASCIIPrintf(strain_energy_viewer->viewer, "%" PetscInt_FMT " TS time %g strain energy %0.12e\n", steps, time, strain_energy));
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel TS Monitor Strain Energy Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Write surface force for a face to a `RatelViewer`.

  Collective across MPI processes.

  @param[in]  viewer    `RatelViewer` context
  @param[in]  steps     Current iteration number
  @param[in]  time      Current time
  @param[in]  name      Name of the face
  @param[in]  centroid  Centroid of the face
  @param[in]  force     Computed surface force on the face

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelViewerWriteSurfaceForce(RatelViewer viewer, PetscInt steps, PetscReal time, const char *name,
                                                   const PetscScalar centroid[3], const PetscScalar force[3]) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(viewer->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Viewer Write Surface Force for Face %s", name));
  PetscCall(RatelViewerWriteHeader(viewer, (void *)"step,time,face_id,centroid_x,centroid_y,centroid_z,force_x,force_y,force_z"));
  if (viewer->viewer_format == PETSC_VIEWER_ASCII_CSV) {
    PetscCall(PetscViewerASCIIPrintf(viewer->viewer, "%" PetscInt_FMT ",%0.17g,%s,%0.12e,%0.12e,%0.12e,%0.12e,%0.12e,%0.12e\n", steps, time, name,
                                     centroid[0], centroid[1], centroid[2], force[0], force[1], force[2]));

  } else {
    PetscCall(PetscViewerASCIIPrintf(viewer->viewer, "%" PetscInt_FMT " TS time %g Surface %s:\n", steps, time, name));
    PetscCall(PetscViewerASCIIPushTab(viewer->viewer));
    PetscCall(PetscViewerASCIIPrintf(viewer->viewer, "Centroid:        [%0.12e, %0.12e, %0.12e]\n", centroid[0], centroid[1], centroid[2]));
    PetscCall(PetscViewerASCIIPrintf(viewer->viewer, "Surface Force:   [%0.12e, %0.12e, %0.12e]\n", force[0], force[1], force[2]));
    PetscCall(PetscViewerASCIIPopTab(viewer->viewer));
  }
  PetscCall(RatelDebug256(viewer->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Viewer Write Surface Force for Face %s Success!", name));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief `TSMonitor` function for surface forces.

  Collective across MPI processes.

  @param[in]  ts     `TS` object to monitor
  @param[in]  steps  Iteration number
  @param[in]  time   Current time
  @param[in]  U      Current state vector
  @param[in]  ctx    Monitor context holding `Ratel` context object

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSMonitorSurfaceForceCellToFace(TS ts, PetscInt steps, PetscReal time, Vec U, void *ctx) {
  RatelViewer surface_forces_viewer = (RatelViewer)ctx;
  Ratel       ratel                 = surface_forces_viewer->ratel;
  PetscInt    num_comp_u;
  PetscBool   should_write;
  PetscReal   dt;

  PetscFunctionBeginUser;
  PetscCall(RatelViewerShouldWrite(surface_forces_viewer, ts, steps, &should_write));
  if (!should_write) PetscFunctionReturn(PETSC_SUCCESS);
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel TS Monitor Surface Forces"));

  // Update time and timestep contexts
  PetscCall(TSGetTimeStep(ts, &dt));
  PetscCall(CeedEvaluatorUpdateTimeDtAndBoundaryValues(ratel->evaluator_residual_u, time, dt));

  // Compute centroids
  PetscScalar *surface_centroids;
  PetscCall(RatelComputeSurfaceCentroids(ratel, U, time, &num_comp_u, &surface_centroids));  // May initialize context objects
  // Compute surface forces
  PetscScalar *surface_forces;
  PetscCall(RatelComputeSurfaceForcesCellToFace(ratel, U, time, &num_comp_u, &surface_forces));  // May initialize context objects

  // View
  for (PetscInt i = 0; i < ratel->surface_force_face_count; i++) {
    PetscInt offset = i * num_comp_u;

    PetscCall(RatelViewerWriteSurfaceForce(surface_forces_viewer, steps, time, ratel->surface_force_face_names[i], &surface_centroids[offset],
                                           &surface_forces[offset]));
  }

  // Cleanup
  PetscCall(PetscFree(surface_forces));
  PetscCall(PetscFree(surface_centroids));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel TS Monitor Surface Forces Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief `TSMonitor` function for face surface forces.

  Collective across MPI processes.

  @param[in]  ts     `TS` object to monitor
  @param[in]  steps  Iteration number
  @param[in]  time   Current time
  @param[in]  U      Current state vector
  @param[in]  ctx    Monitor context holding `Ratel` context object

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSMonitorSurfaceForce(TS ts, PetscInt steps, PetscReal time, Vec U, void *ctx) {
  RatelViewer surface_forces_viewer = (RatelViewer)ctx;
  Ratel       ratel                 = surface_forces_viewer->ratel;
  PetscInt    num_comp_u;
  PetscBool   should_write;
  PetscReal   dt;

  PetscFunctionBeginUser;
  PetscCall(RatelViewerShouldWrite(surface_forces_viewer, ts, steps, &should_write));
  if (!should_write) PetscFunctionReturn(PETSC_SUCCESS);
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel TS Monitor Surface Forces"));

  // Update time and timestep contexts
  PetscCall(TSGetTimeStep(ts, &dt));
  PetscCall(CeedEvaluatorUpdateTimeDtAndBoundaryValues(ratel->evaluator_residual_u, time, dt));

  // Compute centroids
  PetscScalar *surface_centroids;
  PetscCall(RatelComputeSurfaceCentroids(ratel, U, time, &num_comp_u, &surface_centroids));  // May initialize context objects
  // Compute surface forces
  PetscScalar *surface_forces;
  PetscCall(RatelComputeSurfaceForces(ratel, U, time, &num_comp_u, &surface_forces));  // May initialize context objects

  // View
  for (PetscInt i = 0; i < ratel->surface_force_face_count; i++) {
    PetscInt offset = i * num_comp_u;

    PetscCall(RatelViewerWriteSurfaceForce(surface_forces_viewer, steps, time, ratel->surface_force_face_names[i], &surface_centroids[offset],
                                           &surface_forces[offset]));
  }

  // Cleanup
  PetscCall(PetscFree(surface_forces));
  PetscCall(PetscFree(surface_centroids));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel TS Monitor Surface Forces Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief `TSMonitor` function for face surface forces.

  Collective across MPI processes.

  @param[in]  ts     `TS` object to monitor
  @param[in]  steps  Iteration number
  @param[in]  time   Current time
  @param[in]  U      Current state vector
  @param[in]  ctx    Monitor context holding `RatelViewers` context object

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSMonitorSurfaceForcePerFace(TS ts, PetscInt steps, PetscReal time, Vec U, void *ctx) {
  RatelViewers viewer_list = (RatelViewers)ctx;
  Ratel        ratel       = viewer_list->ratel;
  PetscInt     num_comp_u;
  PetscBool    should_write;
  PetscReal    dt;

  PetscFunctionBeginUser;
  if (viewer_list->num_viewers <= 0) PetscFunctionReturn(PETSC_SUCCESS);
  PetscCall(RatelViewerShouldWrite(viewer_list->viewers[0], ts, steps, &should_write));
  if (!should_write) PetscFunctionReturn(PETSC_SUCCESS);
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel TS Monitor Surface Forces"));

  // Update time and timestep contexts
  PetscCall(TSGetTimeStep(ts, &dt));
  PetscCall(CeedEvaluatorUpdateTimeDtAndBoundaryValues(ratel->evaluator_residual_u, time, dt));

  // Compute centroids
  PetscScalar *surface_centroids;
  PetscCall(RatelComputeSurfaceCentroids(ratel, U, time, &num_comp_u, &surface_centroids));  // May initialize context objects
  // Compute surface forces
  PetscScalar *surface_forces;
  PetscCall(RatelComputeSurfaceForces(ratel, U, time, &num_comp_u, &surface_forces));  // May initialize context objects

  // View
  for (PetscInt i = 0; i < viewer_list->num_viewers; i++) {
    PetscInt    offset = i * num_comp_u;
    RatelViewer viewer = viewer_list->viewers[i];

    PetscCall(
        RatelViewerWriteSurfaceForce(viewer, steps, time, ratel->surface_force_face_names[i], &surface_centroids[offset], &surface_forces[offset]));
  }

  // Cleanup
  PetscCall(PetscFree(surface_forces));
  PetscCall(PetscFree(surface_centroids));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel TS Monitor Surface Forces Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief `TSMonitor` function for diagnostic quantities.

  Collective across MPI processes.

  @param[in]  ts     `TS` object to monitor
  @param[in]  steps  Iteration number
  @param[in]  time   Current time
  @param[in]  U      Current state vector
  @param[in]  ctx    Monitor context holding `Ratel` context object

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSMonitorDiagnosticQuantities(TS ts, PetscInt steps, PetscReal time, Vec U, void *ctx) {
  RatelViewer diagnostic_viewer = (RatelViewer)ctx;
  Ratel       ratel             = diagnostic_viewer->ratel;
  PetscBool   should_write;
  Vec         D;
  DM          dm_diagnostic;
  PetscScalar dt;

  PetscFunctionBeginUser;
  PetscCall(RatelViewerShouldWrite(diagnostic_viewer, ts, steps, &should_write));
  if (!should_write) PetscFunctionReturn(PETSC_SUCCESS);
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel TS Monitor Diagnostic Quantities"));

  // Write header, if needed
  PetscCall(RatelViewerWriteHeader(diagnostic_viewer, NULL));

  // Update time and timestep contexts
  PetscCall(TSGetTimeStep(ts, &dt));
  PetscCall(CeedEvaluatorUpdateTimeDtAndBoundaryValues(ratel->evaluator_residual_u, time, dt));

  // Borrow global vector
  PetscCall(RatelGetDiagnosticQuantities(ratel, U, time, &D));

  // Compute and store diagnostic quantities
  PetscCall(VecGetDM(D, &dm_diagnostic));
  PetscCall(DMSetOutputSequenceNumber(dm_diagnostic, steps, time));

  // Write diagnostic quantities to viewer
  if (diagnostic_viewer->is_monitor_vtk) {
    // Write entry in VTK series file too
    PetscCall(RatelViewerWriteTimestep_VTKSeries(diagnostic_viewer, steps, time, D));
  } else {
    PetscCall(VecView(D, diagnostic_viewer->viewer));
  }

  // Cleanup
  PetscCall(RatelRestoreDiagnosticQuantities(ratel, &D));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel TS Monitor Diagnostic Quantities Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Write time, topology, and geometry information for `DMSwarm` to XDMF file and store binary coordinate data.

  Not collective across MPI processes.

  @param[in]      dm             `DMSwarm` object
  @param[inout]   ratel_viewer   Ratel viewer storing `PetscViewerASCII` object
  @param[inout]   binary_viewer  `PetscViewerBinary` object to store topology and coordinate data
  @param[out]     offset         Current offset in binary file

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelDMSwarmView_WriteDMInfo(DM dm, RatelViewer ratel_viewer, PetscViewer binary_viewer, PetscInt *offset) {
  Ratel       ratel = ratel_viewer->ratel;
  PetscInt    num_points_global, dim;
  PetscViewer viewer = ratel_viewer->viewer;
  const char *binary_filename;
  PetscBool   is_swarm;
  PetscSizeT  int_size, real_size;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMSwarm View Write DM Info"));

  // Ensure that we have a DMSwarm
  PetscCall(PetscObjectTypeCompare((PetscObject)dm, DMSWARM, &is_swarm));
  PetscCheck(is_swarm, PetscObjectComm((PetscObject)viewer), PETSC_ERR_SUP, "Only valid for DMSwarm");

  // Get binary filename
  PetscCall(PetscViewerFileGetName(binary_viewer, &binary_filename));
  PetscCheck(binary_filename, ratel->comm, PETSC_ERR_SUP, "Swarm viewer requires a file name");

  PetscCall(DMSwarmGetSize(dm, &num_points_global));

  // Write Grid for particles
  {
    const char *dm_name;

    PetscCall(PetscViewerASCIIPushTab(viewer));
    PetscCall(PetscObjectGetName((PetscObject)dm, &dm_name));
    if (!dm_name) PetscCall(DMGetOptionsPrefix(dm, &dm_name));
    if (!dm_name) {
      PetscCall(PetscViewerASCIIPrintf(viewer, "<Grid Name=\"DMSwarm\" GridType=\"Uniform\">\n"));
    } else {
      PetscCall(PetscViewerASCIIPrintf(viewer, "<Grid Name=\"DMSwarm[%s]\" GridType=\"Uniform\">\n", dm_name));
    }
  }

  // Write time header
  {
    PetscReal time;

    PetscCall(DMGetOutputSequenceNumber(dm, NULL, &time));
    PetscCall(PetscViewerASCIIPushTab(viewer));
    PetscCall(PetscViewerASCIIPrintf(viewer, "<Time Value=\"%0.17g\"/>\n", time));
    PetscCall(PetscViewerASCIIPopTab(viewer));
  }

  // Get sizes
  PetscCall(PetscDataTypeGetSize(PETSC_INT, &int_size));
  PetscCall(PetscDataTypeGetSize(PETSC_REAL, &real_size));

  // Write topology header
  PetscCall(PetscViewerASCIIPushTab(viewer));
  PetscCall(PetscViewerASCIIPrintf(viewer, "<Topology Dimensions=\"%" PetscInt_FMT "\" TopologyType=\"Mixed\">\n", num_points_global));
  PetscCall(PetscViewerASCIIPushTab(viewer));

  PetscCall(PetscViewerASCIIPrintf(viewer,
                                   "<DataItem Format=\"Binary\" Endian=\"Big\" DataType=\"Int\" Precision=\"%" PetscInt_FMT
                                   "\" Dimensions=\"%" PetscInt_FMT "\" Seek=\"%" PetscInt_FMT "\">\n",
                                   (PetscInt)int_size, num_points_global * 3, *offset));
  PetscCall(PetscViewerASCIIPushTab(viewer));
  PetscCall(PetscViewerASCIIPrintf(viewer, "%s\n", binary_filename));
  PetscCall(PetscViewerASCIIPopTab(viewer));
  PetscCall(PetscViewerASCIIPrintf(viewer, "</DataItem>\n"));
  PetscCall(PetscViewerASCIIPopTab(viewer));
  PetscCall(PetscViewerASCIIPrintf(viewer, "</Topology>\n"));
  PetscCall(PetscViewerASCIIPopTab(viewer));

  // Write topology data
  for (PetscInt k = 0; k < num_points_global; k++) {
    PetscInt point_vertex[3];

    point_vertex[0] = 1;
    point_vertex[1] = 1;
    point_vertex[2] = k;
    PetscCall(PetscViewerBinaryWrite(binary_viewer, point_vertex, 3, PETSC_INT));
  }
  *offset += sizeof(PetscInt) * num_points_global * 3;

  // Write geometry header
  PetscCall(PetscViewerASCIIPushTab(viewer));
  PetscCall(DMGetDimension(dm, &dim));
  switch (dim) {
    case 1:
      SETERRQ(PETSC_COMM_SELF, PETSC_ERR_SUP, "No support for 1D");
    case 2:
      PetscCall(PetscViewerASCIIPrintf(viewer, "<Geometry Type=\"XY\">\n"));
      break;
    case 3:
      PetscCall(PetscViewerASCIIPrintf(viewer, "<Geometry Type=\"XYZ\">\n"));
      break;
  }
  PetscCall(PetscViewerASCIIPushTab(viewer));
  PetscCall(PetscViewerASCIIPrintf(viewer,
                                   "<DataItem Format=\"Binary\" Endian=\"Big\" DataType=\"Float\" Precision=\"%" PetscInt_FMT
                                   "\" Dimensions=\"%" PetscInt_FMT " %" PetscInt_FMT "\" Seek=\"%" PetscInt_FMT "\">\n",
                                   (PetscInt)real_size, num_points_global, dim, *offset));
  PetscCall(PetscViewerASCIIPushTab(viewer));
  PetscCall(PetscViewerASCIIPrintf(viewer, "%s\n", binary_filename));
  PetscCall(PetscViewerASCIIPopTab(viewer));
  PetscCall(PetscViewerASCIIPrintf(viewer, "</DataItem>\n"));
  PetscCall(PetscViewerASCIIPopTab(viewer));
  PetscCall(PetscViewerASCIIPrintf(viewer, "</Geometry>\n"));
  PetscCall(PetscViewerASCIIPopTab(viewer));

  // Write geometry data
  {
    const char  **field_coords;
    Vec           X;
    DMSwarmCellDM dm_cell;

    PetscCall(DMSwarmGetCellDMActive(dm, &dm_cell));
    PetscCall(DMSwarmCellDMGetCoordinateFields(dm_cell, NULL, &field_coords));
    PetscCall(DMSwarmCreateGlobalVectorFromField(dm, field_coords[0], &X));
    PetscCall(VecView(X, binary_viewer));
    PetscCall(DMSwarmDestroyGlobalVectorFromField(dm, field_coords[0], &X));
  }
  *offset += sizeof(PetscReal) * num_points_global * dim;

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMSwarm View Write DM Info Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Write field data for `DMSwarm` to XDMF file and store binary field data.

  Not collective across MPI processes.

  @param[in]      dm             `DMSwarm` object
  @param[in]      field          Field name to write
  @param[inout]   ratel_viewer   Ratel viewer storing `PetscViewerASCII` object
  @param[inout]   binary_viewer  `PetscViewerBinary` object to store field data
  @param[out]     offset         Current offset in binary file

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelDMSwarmView_WriteField(DM dm, const char *field, RatelViewer ratel_viewer, PetscViewer binary_viewer, PetscInt *offset) {
  Ratel         ratel  = ratel_viewer->ratel;
  PetscViewer   viewer = ratel_viewer->viewer;
  PetscInt      num_points, num_comp;
  Vec           U;
  const char   *binary_filename;
  PetscDataType datatype;
  char          datatype_str[8] = "";
  PetscSizeT    size;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Vec View Swarm"));

  // Get binary filename
  PetscCall(PetscViewerFileGetName(binary_viewer, &binary_filename));
  PetscCheck(binary_filename, ratel->comm, PETSC_ERR_SUP, "Swarm viewer requires a file name");

  // Get field data
  PetscCall(DMSwarmGetFieldInfo(dm, field, &num_comp, &datatype));

  PetscCall(PetscDataTypeGetSize(datatype, &size));

  switch (datatype) {
    case PETSC_FLOAT:
    case PETSC_DOUBLE: {
      PetscCall(PetscSNPrintf(datatype_str, sizeof(datatype_str) - 1, "Float"));
      PetscCall(DMSwarmCreateGlobalVectorFromField(dm, field, &U));
      PetscCall(VecGetSize(U, &num_points));
      num_points /= num_comp;
      // Write data
      PetscCall(VecView(U, binary_viewer));
      PetscCall(DMSwarmDestroyGlobalVectorFromField(dm, field, &U));
      break;
    }
    case PETSC_INT32:
    case PETSC_INT64:
    case PETSC_INT: {
      IS              is;
      const PetscInt *idx;
      void           *data;
      PetscInt        num_points_local;

      PetscCall(PetscSNPrintf(datatype_str, sizeof(datatype_str) - 1, "Int"));
      PetscCall(DMSwarmGetLocalSize(dm, &num_points_local));

      // Write data
      PetscCall(DMSwarmGetField(dm, field, NULL, NULL, &data));
      idx = (const PetscInt *)data;

      PetscCall(ISCreateGeneral(PetscObjectComm((PetscObject)dm), num_points_local, idx, PETSC_USE_POINTER, &is));
      PetscCall(ISView(is, binary_viewer));
      PetscCall(ISGetSize(is, &num_points));
      PetscCall(ISDestroy(&is));
      PetscCall(DMSwarmRestoreField(dm, field, NULL, NULL, &data));
      break;
    }
    default:
      SETERRQ(PETSC_COMM_SELF, PETSC_ERR_SUP, "Unsupported datatype %s", PetscDataTypes[datatype]);
  }

  // Write data header
  PetscCall(PetscViewerASCIIPushTab(viewer));
  PetscCall(PetscViewerASCIIPrintf(viewer, "<Attribute Center=\"Node\" Name=\"%s\" Type=\"None\">\n", field));
  PetscCall(PetscViewerASCIIPushTab(viewer));
  if (num_comp == 1) {
    PetscCall(PetscViewerASCIIPrintf(viewer,
                                     "<DataItem Format=\"Binary\" Endian=\"Big\" DataType=\"%s\" Precision=\"%" PetscInt_FMT
                                     "\" Dimensions=\"%" PetscInt_FMT "\" Seek=\"%" PetscInt_FMT "\">\n",
                                     datatype_str, (PetscInt)size, num_points, *offset));
  } else {
    PetscCall(PetscViewerASCIIPrintf(viewer,
                                     "<DataItem Format=\"Binary\" Endian=\"Big\" DataType=\"%s\" Precision=\"%" PetscInt_FMT
                                     "\" Dimensions=\"%" PetscInt_FMT " %" PetscInt_FMT "\" Seek=\"%" PetscInt_FMT "\">\n",
                                     datatype_str, (PetscInt)size, num_points, num_comp, *offset));
  }
  PetscCall(PetscViewerASCIIPushTab(viewer));
  PetscCall(PetscViewerASCIIPrintf(viewer, "%s\n", binary_filename));
  PetscCall(PetscViewerASCIIPopTab(viewer));
  PetscCall(PetscViewerASCIIPrintf(viewer, "</DataItem>\n"));
  PetscCall(PetscViewerASCIIPopTab(viewer));
  PetscCall(PetscViewerASCIIPrintf(viewer, "</Attribute>\n"));
  PetscCall(PetscViewerASCIIPopTab(viewer));

  // Update offset
  *offset += size * num_points * num_comp;

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Vec View Swarm Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Create a sub-viewer for `DMSwarm` to store binary data.

  Collective across MPI processes.

  @param[in]   dm             `DMSwarm` object
  @param[in]   ratel_viewer   Ratel viewer storing `PetscViewerASCII` object
  @param[out]  binary_viewer  Created `PetscViewerBinary` object for the current timestep

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelDMSwarmSubViewerCreate(DM dm, RatelViewer ratel_viewer, PetscViewer *binary_viewer) {
  Ratel ratel = ratel_viewer->ratel;
  PetscFunctionBeginUser;
  PetscCall(PetscViewerCreate(ratel->comm, binary_viewer));
  PetscCall(PetscViewerSetType(*binary_viewer, PETSCVIEWERBINARY));
  PetscCall(PetscViewerBinarySetSkipHeader(*binary_viewer, PETSC_TRUE));
  PetscCall(PetscViewerBinarySetSkipInfo(*binary_viewer, PETSC_TRUE));
  PetscCall(PetscViewerFileSetMode(*binary_viewer, FILE_MODE_WRITE));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief `TSMonitor` function for swarm data.

  Collective across MPI processes.

  @param[in]  ts     `TS` object to monitor
  @param[in]  steps  Iteration number
  @param[in]  time   Current time
  @param[in]  U      Current state vector
  @param[in]  ctx    Monitor context holding `Ratel` context object

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSMonitorSwarm(TS ts, PetscInt steps, PetscReal time, Vec U, void *ctx) {
  RatelViewer  swarm_viewer = (RatelViewer)ctx;
  char         binary_filename[PETSC_MAX_PATH_LEN];
  Ratel        ratel    = swarm_viewer->ratel;
  DM           dm_swarm = ratel->dm_solution;
  PetscBool    should_write;
  const char **field_names, **point_field_names;
  PetscInt     offset = 0;
  CeedInt      num_fields, num_point_fields;
  PetscReal    dt;

  PetscFunctionBeginUser;
  PetscCall(RatelViewerShouldWrite(swarm_viewer, ts, steps, &should_write));
  if (!should_write) PetscFunctionReturn(PETSC_SUCCESS);
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel TS Monitor Swarm"));

  // Update time and timestep contexts
  PetscCall(TSGetTimeStep(ts, &dt));
  PetscCall(CeedEvaluatorUpdateTimeDtAndBoundaryValues(ratel->evaluator_residual_u, time, dt));

  // Write header
  PetscCall(RatelViewerWriteHeader(swarm_viewer, NULL));

  // Set timestep and create binary viewer
  PetscCall(DMSetOutputSequenceNumber(dm_swarm, steps, time));

  // Create binary viewer
  if (!swarm_viewer->aux_viewer) {
    PetscCall(RatelDMSwarmSubViewerCreate(dm_swarm, swarm_viewer, &swarm_viewer->aux_viewer));
  }

  // File name is viewer.name + "_" + seq_num + ".pbin"
  PetscCall(RatelViewerGetSequenceFilename(swarm_viewer, steps, sizeof(binary_filename), binary_filename));
  PetscCall(PetscViewerFileSetName(swarm_viewer->aux_viewer, binary_filename));

  // Get field name
  PetscCall(RatelMaterialGetActiveFieldNames(ratel->materials[0], &field_names, NULL));
  PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[0], &num_fields, NULL));

  // View DM
  PetscCall(RatelDMSwarmView_WriteDMInfo(dm_swarm, swarm_viewer, swarm_viewer->aux_viewer, &offset));

  // View material active fields
  for (PetscInt i = 0; i < num_fields; i++) {
    PetscCall(RatelDMSwarmView_WriteField(dm_swarm, field_names[i], swarm_viewer, swarm_viewer->aux_viewer, &offset));
  }
  // View material property fields
  PetscCall(RatelMaterialGetPointFields(ratel->materials[0], &num_point_fields, NULL, &point_field_names));
  for (PetscInt i = 0; i < num_point_fields; i++) {
    PetscCall(RatelDMSwarmView_WriteField(dm_swarm, point_field_names[i], swarm_viewer, swarm_viewer->aux_viewer, &offset));
  }

  // View additional fields
  PetscCall(RatelDMSwarmView_WriteField(dm_swarm, DMSwarmPICField_volume, swarm_viewer, swarm_viewer->aux_viewer, &offset));
  {
    const char   *cell_id;
    DMSwarmCellDM dm_cell = NULL;

    PetscCall(DMSwarmGetCellDMActive(dm_swarm, &dm_cell));
    PetscCall(DMSwarmCellDMGetCellID(dm_cell, &cell_id));
    PetscCall(RatelDMSwarmView_WriteField(dm_swarm, cell_id, swarm_viewer, swarm_viewer->aux_viewer, &offset));
  }
  PetscCall(RatelDMSwarmView_WriteField(dm_swarm, DMSwarmField_rank, swarm_viewer, swarm_viewer->aux_viewer, &offset));
  if (ratel->materials[0]->model_data->num_comp_state > 0) {
    PetscCall(RatelDMSwarmView_WriteField(dm_swarm, "model state", swarm_viewer, swarm_viewer->aux_viewer, &offset));
  }

  // Write timestep footer
  PetscCall(PetscViewerASCIIPrintf(swarm_viewer->viewer, "</Grid>\n"));
  PetscCall(PetscViewerASCIIPopTab(swarm_viewer->viewer));

  PetscCall(PetscViewerFlush(swarm_viewer->viewer));
  PetscCall(PetscViewerFlush(swarm_viewer->aux_viewer));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel TS Monitor Swarm Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief `TSMonitor` function for swarm solution (with minimal additional data).

  Collective across MPI processes.

  @param[in]  ts     `TS` object to monitor
  @param[in]  steps  Iteration number
  @param[in]  time   Current time
  @param[in]  U      Current state vector
  @param[in]  ctx    Monitor context holding `Ratel` context object

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSMonitorSwarmSolution(TS ts, PetscInt steps, PetscReal time, Vec U, void *ctx) {
  RatelViewer  swarm_viewer = (RatelViewer)ctx;
  char         binary_filename[PETSC_MAX_PATH_LEN];
  Ratel        ratel    = swarm_viewer->ratel;
  DM           dm_swarm = ratel->dm_solution;
  PetscBool    should_write;
  const char **field_names;
  PetscInt     offset = 0;
  CeedInt      num_fields;
  PetscReal    dt;

  PetscFunctionBeginUser;
  PetscCall(RatelViewerShouldWrite(swarm_viewer, ts, steps, &should_write));
  if (!should_write) PetscFunctionReturn(PETSC_SUCCESS);
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel TS Monitor Swarm"));

  // Update time and timestep contexts
  PetscCall(TSGetTimeStep(ts, &dt));
  PetscCall(CeedEvaluatorUpdateTimeDtAndBoundaryValues(ratel->evaluator_residual_u, time, dt));

  // Write header
  PetscCall(RatelViewerWriteHeader(swarm_viewer, NULL));

  // Set timestep and create binary viewer
  PetscCall(DMSetOutputSequenceNumber(dm_swarm, steps, time));

  // Create binary viewer
  if (!swarm_viewer->aux_viewer) {
    PetscCall(RatelDMSwarmSubViewerCreate(dm_swarm, swarm_viewer, &swarm_viewer->aux_viewer));
  }

  // File name is viewer.name + "_" + seq_num + ".pbin"
  PetscCall(RatelViewerGetSequenceFilename(swarm_viewer, steps, sizeof(binary_filename), binary_filename));
  PetscCall(PetscViewerFileSetName(swarm_viewer->aux_viewer, binary_filename));

  // Get field name
  PetscCall(RatelMaterialGetActiveFieldNames(ratel->materials[0], &field_names, NULL));
  PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[0], &num_fields, NULL));

  // View DM
  PetscCall(RatelDMSwarmView_WriteDMInfo(dm_swarm, swarm_viewer, swarm_viewer->aux_viewer, &offset));

  // View material active fields
  for (PetscInt i = 0; i < num_fields; i++) {
    PetscCall(RatelDMSwarmView_WriteField(dm_swarm, field_names[i], swarm_viewer, swarm_viewer->aux_viewer, &offset));
  }

  // Write timestep footer
  PetscCall(PetscViewerASCIIPrintf(swarm_viewer->viewer, "</Grid>\n"));
  PetscCall(PetscViewerASCIIPopTab(swarm_viewer->viewer));

  PetscCall(PetscViewerFlush(swarm_viewer->viewer));
  PetscCall(PetscViewerFlush(swarm_viewer->aux_viewer));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel TS Monitor Swarm Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief `TSMonitor` function for binary checkpoints.

  Collective across MPI processes.

  @param[in]  ts     `TS` object to monitor
  @param[in]  steps  Iteration number
  @param[in]  time   Current time
  @param[in]  U      Current state vector
  @param[in]  ctx    Monitor context holding `Ratel` context object

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSMonitorCheckpoint(TS ts, PetscInt steps, PetscReal time, Vec U, void *ctx) {
  RatelCheckpointCtx checkpoint_ctx     = (RatelCheckpointCtx)ctx;
  const PetscInt32   checkpoint_version = PetscDefined(USE_64BIT_INDICES) ? RATEL_CHECKPOINT_VERSION_INT64 : RATEL_CHECKPOINT_VERSION_INT32;
  char               file_path[PETSC_MAX_PATH_LEN];
  PetscViewer        viewer;
  TSConvergedReason  reason = TS_CONVERGED_TIME;

  PetscFunctionBeginUser;
  // Check if converged, if so write final checkpoint
  if (ts) PetscCall(TSGetConvergedReason(ts, &reason));
  if (reason < 0) PetscFunctionReturn(PETSC_SUCCESS);

  if (steps % checkpoint_ctx->interval == 0 || reason > 0) {
    MPI_Comm comm;

    // Setup viewer
    PetscCall(PetscObjectGetComm((PetscObject)U, &comm));
    PetscCall(PetscSNPrintf(file_path, sizeof(file_path), "%s-%" PetscInt_FMT ".bin", checkpoint_ctx->file_name,
                            checkpoint_ctx->write_toggle ? checkpoint_ctx->toggle_state : steps));
    checkpoint_ctx->toggle_state = !checkpoint_ctx->toggle_state;
    PetscCall(PetscViewerBinaryOpen(comm, file_path, FILE_MODE_WRITE, &viewer));

    // Write checkpoint
    PetscCall(PetscViewerBinaryWrite(viewer, &checkpoint_version, 1, PETSC_INT32));
    PetscCall(PetscViewerBinaryWrite(viewer, &steps, 1, PETSC_INT));
    time /= checkpoint_ctx->units->second;  // Dimensionalize time back
    PetscCall(PetscViewerBinaryWrite(viewer, &time, 1, PETSC_REAL));
    PetscCall(VecView(U, viewer));

    // Cleanup
    PetscCall(PetscViewerDestroy(&viewer));
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

// -----------------------------------------------------------------------------
// Ratel Checkpoint Data
// -----------------------------------------------------------------------------
/**
  @brief Read `RatelCheckpointData` from a binary file.

  Collective across MPI processes.

  @param[in]   ratel     `Ratel` context
  @param[in]   filename  Continue file name
  @param[out]  data      Pointer to store created `RatelCheckpointData` object

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelCheckpointDataRead(Ratel ratel, const char filename[], RatelCheckpointData *data) {
  PetscDataType file_int_type = PETSC_INT32;
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Checkpoint Data Read"));

  // Create checkpoint data
  PetscCall(PetscNew(data));

  // Open checkpoint file
  PetscCall(PetscViewerBinaryOpen(ratel->comm, filename, FILE_MODE_READ, &(*data)->viewer));
  PetscCheck((*data)->viewer != NULL, ratel->comm, PETSC_ERR_FILE_OPEN, "Could read checkpoint from file %s", ratel->continue_file_name);

  // Read version
  PetscCall(PetscViewerBinaryRead((*data)->viewer, &(*data)->version, 1, NULL, PETSC_INT32));
  PetscCheck((*data)->version == RATEL_CHECKPOINT_VERSION_INT32 || (*data)->version == RATEL_CHECKPOINT_VERSION_INT64, ratel->comm,
             PETSC_ERR_FILE_UNEXPECTED, "Binary file was not generated by Ratel");

  // Read restart data
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "---- Reading file written with PetscInt == %s",
                          (*data)->version == RATEL_CHECKPOINT_VERSION_INT32 ? "Int32" : "Int64"));
  if ((*data)->version == RATEL_CHECKPOINT_VERSION_INT32) file_int_type = PETSC_INT32;
  else if ((*data)->version == RATEL_CHECKPOINT_VERSION_INT64) file_int_type = PETSC_INT64;
  PetscCall(PetscViewerBinaryRead((*data)->viewer, &(*data)->step, 1, NULL, file_int_type));
  PetscCall(PetscViewerBinaryRead((*data)->viewer, &(*data)->time, 1, NULL, PETSC_REAL));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Checkpoint Data Read Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Destroy `RatelCheckpointData`.

  Collective across MPI processes.

  @param[out]  data  `RatelCheckpointData` object to destroy

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelCheckpointDataDestroy(RatelCheckpointData *data) {
  PetscFunctionBeginUser;
  if (!*data) PetscFunctionReturn(PETSC_SUCCESS);

  PetscCall(PetscViewerDestroy(&(*data)->viewer));
  PetscCall(PetscFree(*data));
  PetscFunctionReturn(PETSC_SUCCESS);
}

// -----------------------------------------------------------------------------
// Ratel Checkpoint Context
// -----------------------------------------------------------------------------
/**
  @brief Create a `RatelCheckpointCtx` object from `PetscOptions` command line handling.

  Collective across MPI processes.

  @param[in]   ratel  `Ratel` context
  @param[in]   opt    `TS` monitor flag
  @param[in]   text   Description of the flag
  @param[out]  ctx    `RatelCheckpointCtx` object to create
  @param[out]  set    `PetscBool` indicating if the flag was set

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelCheckpointCtxCreateFromOptions(Ratel ratel, const char opt[], const char text[], RatelCheckpointCtx *ctx, PetscBool *set) {
  PetscBool checkpoint_toggle   = PETSC_FALSE;
  PetscInt  checkpoint_interval = 1;
  char     *file_name           = NULL;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Create RatelCheckpointCtx From Options"));

  // Get command line options
  {
    PetscInt   max_strings = 1;
    PetscSizeT opt_len;
    char      *interval_opt = NULL, *toggle_opt = NULL;

    PetscCall(PetscStrlen(opt, &opt_len));
    PetscCall(PetscCalloc1(opt_len + 11, &interval_opt));
    PetscCall(PetscSNPrintf(interval_opt, opt_len + 11, "%s_interval", opt));
    PetscCall(PetscCalloc1(opt_len + 9, &toggle_opt));
    PetscCall(PetscSNPrintf(toggle_opt, opt_len + 9, "%s_toggle", opt));

    PetscOptionsBegin(ratel->comm, NULL, "Ratel TS options", NULL);
    PetscCall(PetscOptionsStringArray(opt, text, NULL, &file_name, &max_strings, set));
    // -- Check for different checkpoint interval
    PetscCall(PetscOptionsInt(interval_opt, "Set TS Monitor checkpoint interval", NULL, checkpoint_interval, &checkpoint_interval, NULL));
    PetscCall(PetscOptionsBool(toggle_opt, "Set TS Monitor checkpoint toggle", NULL, checkpoint_toggle, &checkpoint_toggle, NULL));
    PetscOptionsEnd();

    PetscCall(PetscFree(interval_opt));
    PetscCall(PetscFree(toggle_opt));
  }

  // Create the RatelViewer
  if (*set) PetscCall(RatelCheckpointCtxCreate(ratel, checkpoint_interval, checkpoint_toggle, file_name, ctx));
  PetscCall(PetscFree(file_name));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Create RatelCheckpointCtx From Options Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Create a `RatelCheckpointCtx` object.

  Not collective across MPI processes.

  @param[in]   ratel      `Ratel` context
  @param[in]   interval   Checkpoint interval
  @param[in]   toggle     Flag to toggle between two files
  @param[in]   file_name  Base file name for checkpoint files
  @param[out]  ctx        `RatelCheckpointCtx` object to create

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelCheckpointCtxCreate(Ratel ratel, PetscInt interval, PetscBool toggle, const char file_name[], RatelCheckpointCtx *ctx) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Create RatelCheckpointCtx"));

  // Allocate
  PetscCall(PetscNew(ctx));

  // Set fields
  (*ctx)->interval     = interval;
  (*ctx)->write_toggle = toggle;
  (*ctx)->toggle_state = PETSC_FALSE;
  (*ctx)->units        = ratel->material_units;
  PetscCall(PetscStrallocpy(file_name, &(*ctx)->file_name));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Create RatelCheckpointCtx Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Destroy `RatelCheckpointCtx`.

  Not collective across MPI processes.

  @param[in,out]  ctx  `RatelCheckpointCtx` object to destroy

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelCheckpointCtxDestroy(RatelCheckpointCtx *ctx) {
  PetscFunctionBeginUser;
  if (!*ctx) PetscFunctionReturn(PETSC_SUCCESS);

  // Filename info
  PetscCall(PetscFree((*ctx)->file_name));

  // Deallocate
  PetscCall(PetscFree(*ctx));
  PetscFunctionReturn(PETSC_SUCCESS);
}

// -----------------------------------------------------------------------------
// RatelViewer (for TS Monitors)
// -----------------------------------------------------------------------------

/**
  @brief Write header for CSV file.

  Collective across MPI processes.

  @param[in]   ratel_viewer  `RatelViewer` storing `PetscViewerASCII` object
  @param[out]  ctx           Header string

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelViewerWriteHeader_CSV(RatelViewer ratel_viewer, void *ctx) {
  Ratel       ratel  = ratel_viewer->ratel;
  PetscViewer viewer = ratel_viewer->viewer;
  char       *header = (char *)ctx;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel PetscViewer CSV Write Header"));
  PetscCall(PetscViewerASCIIPrintf(viewer, "%s\n", header));
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel PetscViewer CSV Write Header Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Write header for VTK series file.

  Collective across MPI processes.

  @param[in]   ratel_viewer  `RatelViewer` storing `PetscViewerASCII` object as its `aux_viewer`
  @param[out]  ctx           Context, unused

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelViewerWriteHeader_VTKSeries(RatelViewer ratel_viewer, void *ctx) {
  Ratel       ratel         = ratel_viewer->ratel;
  PetscViewer series_viewer = ratel_viewer->aux_viewer;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel PetscViewer VTK Series Write Header"));

  PetscCall(PetscViewerASCIIUseTabs(series_viewer, PETSC_TRUE));

  // Write root object
  PetscCall(PetscViewerASCIIPrintf(series_viewer, "{\n"));
  PetscCall(PetscViewerASCIIPushTab(series_viewer));

  // Write file series version and open files array
  PetscCall(PetscViewerASCIIPrintf(series_viewer, "\"file-series-version\": \"1.0\",\n"));
  PetscCall(PetscViewerASCIIPrintf(series_viewer, "\"files\": [\n"));
  PetscCall(PetscViewerASCIIPushTab(series_viewer));

  // Flush viewer
  PetscCall(PetscViewerFlush(series_viewer));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel PetscViewer VTK Series Write Header Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Write footer for VTK series file.

  Collective across MPI processes.

  @param[in]  ratel_viewer  `RatelViewer` storing `PetscViewerASCII` object as its `aux_viewer`

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelViewerWriteFooter_VTKSeries(RatelViewer ratel_viewer) {
  Ratel       ratel         = ratel_viewer->ratel;
  PetscViewer series_viewer = ratel_viewer->aux_viewer;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel PetscViewer VTK Series Write Header"));

  // Close files array
  PetscCall(PetscViewerASCIIPopTab(series_viewer));
  PetscCall(PetscViewerASCIIPrintf(series_viewer, "]\n"));

  // Close root object
  PetscCall(PetscViewerASCIIPopTab(series_viewer));
  PetscCall(PetscViewerASCIIPrintf(series_viewer, "}\n"));

  // Flush viewer
  PetscCall(PetscViewerFlush(series_viewer));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel PetscViewer VTK Series Write Header Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Write header for XDMF file.

  Collective across MPI processes.

  @param[in,out]  ratel_viewer  Ratel viewer storing `PetscViewerASCII` object
  @param[in]      ctx           Context, unused

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelViewerWriteHeader_XDMF(RatelViewer ratel_viewer, void *ctx) {
  Ratel       ratel  = ratel_viewer->ratel;
  PetscViewer viewer = ratel_viewer->viewer;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel PetscViewer XDMF Write Header"));

  // Write XDMF header
  PetscCall(PetscViewerASCIIPrintf(viewer, "<?xml version=\"1.0\" encoding=\"utf-8\"?>\n"));
  PetscCall(PetscViewerASCIIPrintf(viewer, "<Xdmf xmlns:xi=\"http://www.w3.org/2001/XInclude/\" Version=\"2.99\">\n"));

  // Write XDMF domain
  PetscCall(PetscViewerASCIIPushTab(viewer));
  PetscCall(PetscViewerASCIIPrintf(viewer, "<Domain>\n"));

  // Write Grid for time
  PetscCall(PetscViewerASCIIPushTab(viewer));
  PetscCall(PetscViewerASCIIPrintf(viewer, "<Grid Name=\"CellTime\" GridType=\"Collection\" CollectionType=\"Temporal\">\n"));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel PetscViewer XDMF Write Header Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Write footer for XDMF file.

  Collective across MPI processes.

  @param[in,out]  ratel_viewer  Ratel viewer storing `PetscViewerASCII` object

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelViewerWriteFooter_XDMF(RatelViewer ratel_viewer) {
  Ratel       ratel  = ratel_viewer->ratel;
  PetscViewer viewer = ratel_viewer->viewer;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel PetscViewer XDMF Write Footer"));

  // Write footer
  PetscCall(PetscViewerASCIIPrintf(viewer, "</Grid>\n"));
  PetscCall(PetscViewerASCIIPopTab(viewer));
  PetscCall(PetscViewerASCIIPrintf(viewer, "</Domain>\n"));
  PetscCall(PetscViewerASCIIPopTab(viewer));
  PetscCall(PetscViewerASCIIPrintf(viewer, "</Xdmf>\n"));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel PetscViewer XDMF Write Footer Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Split a viewer specification string into its components, type, filename, format, and file mode.

  Not collective across MPI processes.

  @note Adapted from `PetscOptionsCreateViewers_Single()` in `petsc/src/sys/classes/viewer/interface/viewreg.c`.

  @param[in]   ratel      `Ratel` context
  @param[in]   value      Viewer specification string
  @param[out]  type       Pointer to `PetscViewerType` string
  @param[out]  filename   Pointer to filename string
  @param[out]  format     `PetscViewerFormat` enum, if specified
  @param[out]  file_mode  `PetscFileMode` enum, if specified

  @return An error code: 0 - success, otherwise - failure

  @post `type` and `filename` point to NULL or allocated strings, and must be freed by the caller.
**/
static PetscErrorCode RatelGetViewerInfo(Ratel ratel, const char value[], char **type, char **filename, PetscViewerFormat *format,
                                         PetscFileMode *file_mode) {
  char  *loc1_filename = NULL, *loc2_fmt = NULL, *loc3_fmode = NULL;
  size_t viewer_string_length;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Get Viewer Info"));

  PetscCheck(type != NULL, PETSC_COMM_SELF, PETSC_ERR_ARG_NULL, "type must be a valid pointer to a (possibly NULL) char*");
  PetscCheck(type != NULL, PETSC_COMM_SELF, PETSC_ERR_ARG_NULL, "filename must be a valid pointer to a (possibly NULL) char*");
  *type     = NULL;
  *filename = NULL;
  if (format) *format = PETSC_VIEWER_DEFAULT;
  if (file_mode) *file_mode = FILE_MODE_WRITE;
  PetscCall(PetscStrlen(value, &viewer_string_length));
  if (!viewer_string_length) {
    PetscFunctionReturn(PETSC_SUCCESS);
  }

  PetscCall(PetscStrallocpy(value, type));
  PetscCall(PetscStrchr(*type, ':', &loc1_filename));
  if (loc1_filename) {
    PetscBool is_daos;
    // Replace ':' with NULL terminator
    *loc1_filename++ = 0;
    // When using DAOS, the filename will have the form "daos:/path/to/file.h5", so capture the rest of it.
    PetscCall(PetscStrncmp(loc1_filename, "daos:", 5, &is_daos));
    PetscCall(PetscStrchr(loc1_filename + (is_daos == PETSC_TRUE ? 5 : 0), ':', &loc2_fmt));
  }

  if (loc2_fmt) {
    // Replace ':' with NULL terminator
    *loc2_fmt++ = 0;
    PetscCall(PetscStrchr(loc2_fmt, ':', &loc3_fmode));
  }
  // Replace ':' with NULL terminator
  if (loc3_fmode) *loc3_fmode++ = 0;

  // Copy the filename
  PetscCall(PetscStrallocpy(loc1_filename, filename));

  // Convert the format and file mode strings to enums
  if (loc2_fmt && *loc2_fmt) {
    PetscViewerFormat tfmt;
    PetscBool         flag;

    PetscCall(PetscEnumFind(PetscViewerFormats, loc2_fmt, (PetscEnum *)&tfmt, &flag));
    PetscCheck(flag, PETSC_COMM_SELF, PETSC_ERR_SUP, "Unknown viewer format %s", loc2_fmt);
    if (format) *format = tfmt;
  }
  if (loc3_fmode && *loc3_fmode) { /* Has non-empty file mode ("write" or "append") */
    PetscFileMode fmode;
    PetscBool     flag = PETSC_FALSE;

    PetscCall(PetscEnumFind(PetscFileModes, loc3_fmode, (PetscEnum *)&fmode, &flag));
    PetscCheck(flag, PETSC_COMM_SELF, PETSC_ERR_ARG_UNKNOWN_TYPE, "Unknown file mode: %s", loc3_fmode);
    if (file_mode) *file_mode = fmode;
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Get Viewer Info Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Check if a viewer of type @a type with filename @a filename is a CSV viewer.

  Not collective across MPI processes.

  @param[in]   ratel     `Ratel` context
  @param[in]   type      `PetscViewerType` string
  @param[in]   filename  File name string
  @param[out]  is_csv    Pointer to `PetscBool` to store whether the resulting viewer should be a CSV viewer

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelViewerStringIsCSV(Ratel ratel, const char type[], const char filename[], PetscBool *is_csv) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Viewer String Is CSV"));
  PetscCheck(is_csv != NULL, ratel->comm, PETSC_ERR_ARG_NULL, "is_csv cannot be NULL");

  if (type && !strcmp(type, PETSCVIEWERASCII) && filename) {
    PetscCall(PetscStrendswith(filename, ".csv", is_csv));
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Viewer String Is CSV Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Load an existing CSV file into a `PetscViewer` and move the file pointer to start writing at the checkpointed step.

  Collective across MPI processes.

  @param[in]   ratel            `Ratel` context
  @param[in]   filename         File name of the CSV file
  @param[in]   checkpoint_data  `RatelCheckpointData` object
  @param[out]  viewer           Created `PetscViewer`, or NULL if the old file is empty.

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelLoadViewerFromCheckpoint_CSV(Ratel ratel, const char filename[], RatelCheckpointData checkpoint_data,
                                                        PetscViewer *viewer) {
  FILE *file;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Create RatelViewer CSV From Checkpoint"));

  PetscCall(PetscViewerCreate(ratel->comm, viewer));
  PetscCall(PetscViewerSetType(*viewer, PETSCVIEWERASCII));
  // Note: FILE_MODE_APPEND_UPDATE is not actually 'a+' but 'r+' followed by fseek to end
  // This is to allow for writing in the middle of the file
  PetscCall(PetscViewerFileSetMode(*viewer, FILE_MODE_APPEND_UPDATE));
  PetscCall(PetscViewerFileSetName(*viewer, filename));
  PetscCall(PetscViewerSetFromOptions(*viewer));
  PetscCall(PetscViewerSetUp(*viewer));

  // Note: file is only valid on rank 0 for PETSCVIEWERASCII
  PetscCall(PetscViewerASCIIGetPointer(*viewer, &file));

  // Compute the size of the file
  PetscInt size = 0;
  if (ratel->comm_rank == 0) {
    long end = ftell(file);
    rewind(file);
    long start = ftell(file);
    size       = (PetscInt)(end - start);
  }

  // Broadcast the size of the file to all ranks
  PetscCallMPI(MPI_Bcast(&size, 1, MPIU_INT, 0, ratel->comm));

  // If the file is not empty, then (1) the header is written and (2) we need to truncate it to the last checkpoint
  // Otherwise, the user likely deleted the old file, so we will do nothing and the calling function will create a new one
  if (size > 0) {
    // Non-collective file operations, since only rank 0 has the file open
    if (ratel->comm_rank == 0) {
      // Truncate file to data up to the last checkpoint
      // Note: c is an int to handle EOF
      int c;

      // Skip header line
      while ((c = fgetc(file)) != EOF) {
        if (c == '\n') break;
      }
      // Skip to the last checkpoint
      // Note: we always write at step 0, so we need to copy num_rows+1 lines
      while (c != EOF) {
        long     file_pos = ftell(file);
        PetscInt current_step;

        // Get the step number, which is the first entry in the line
        int num_read = fscanf(file, "%" PetscInt_FMT, &current_step);
        PetscCheck(num_read == 1 || num_read == EOF, PETSC_COMM_SELF, PETSC_ERR_FILE_UNEXPECTED, "Could not read step number from checkpoint file");
        if (num_read == EOF) break;
        // If we are beyond the last checkpoint, break
        if (current_step >= checkpoint_data->step) {
          // Go back to the beginning of the line to start writing
          fseek(file, file_pos, SEEK_SET);
          break;
        }
        // Otherwise, skip to the next line
        while ((c = fgetc(file)) != EOF) {
          if (c == '\n') break;
        }
        // Ensure ending newline
        if (c == EOF) fputc('\n', file);
      }
    }
  } else {
    // Destroy the viewer, we will create a new one
    PetscCall(PetscViewerDestroy(viewer));
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Create RatelViewer CSV From Checkpoint Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Create a `RatelViewer` object from `PetscOptions` command line handling.

  Collective across MPI processes.

  @param[in]   ratel           `Ratel` context
  @param[in]   opt             `TS` monitor flag
  @param[in]   text            Description of the flag
  @param[out]  monitor_viewer  `RatelViewer` object to create
  @param[out]  set             `PetscBool` indicating if the flag was set

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelViewerCreateFromOptions(Ratel ratel, const char opt[], const char text[], RatelViewer *monitor_viewer, PetscBool *set) {
  char              interval_key[PETSC_MAX_OPTION_NAME];
  PetscInt          interval = 1;
  PetscViewer       viewer   = NULL;
  PetscViewerFormat viewer_format;
  PetscBool         header_written = PETSC_FALSE;
  PetscBool         is_restart     = ratel->initial_condition_type == RATEL_INITIAL_CONDITION_CONTINUE;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Create RatelViewer From Options"));

  // Special handling for restart
  if (is_restart) {
    char      option_string[PETSC_MAX_PATH_LEN] = {0};
    char     *type = NULL, *filename = NULL;
    PetscBool is_csv = PETSC_FALSE;

    // For some monitors (ascii), we want to copy over from checkpointed data
    PetscOptionsBegin(ratel->comm, NULL, "RatelViewer options", NULL);
    PetscCall(PetscOptionsString(opt, text, NULL, option_string, option_string, sizeof(option_string), NULL));
    PetscOptionsEnd();

    // Get viewer info
    PetscCall(RatelGetViewerInfo(ratel, option_string, &type, &filename, NULL, NULL));
    PetscCall(RatelViewerStringIsCSV(ratel, type, filename, &is_csv));

    if (is_csv) {
      RatelCheckpointData checkpoint_data;

      PetscCall(RatelCheckpointDataRead(ratel, ratel->continue_file_name, &checkpoint_data));
      PetscCall(RatelLoadViewerFromCheckpoint_CSV(ratel, filename, checkpoint_data, &viewer));
      header_written = viewer != NULL;
      *set           = viewer != NULL;
      viewer_format  = PETSC_VIEWER_ASCII_CSV;
      PetscCall(RatelCheckpointDataDestroy(&checkpoint_data));
    }
    // TODO: Add support for PETSC_VIEWER_ASCII_XML -- Needed for MPM restart and should be pretty easy to implement

    // Cleanup
    PetscFree(type);
    PetscFree(filename);
  }

  // Read viewer options
  PetscOptionsBegin(ratel->comm, NULL, "RatelViewer options", NULL);
  PetscCall(PetscSNPrintf(interval_key, sizeof(interval_key), "%s_interval", opt));
  PetscCall(PetscOptionsInt(interval_key, "Monitor every N timesteps", NULL, interval, &interval, NULL));
  if (!viewer) {
    // Get command line options
    PetscCall(PetscOptionsViewer(opt, text, NULL, &viewer, &viewer_format, set));
  }
  PetscOptionsEnd();

  // Create the RatelViewer
  if (*set) {
    const char *filename = NULL;

    PetscCall(PetscViewerFileGetName(viewer, &filename));
    PetscCall(RatelViewerCreate(ratel, viewer, viewer_format, interval, monitor_viewer));
    (*monitor_viewer)->is_header_written = header_written;
  }
  PetscCall(PetscViewerDestroy(&viewer));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Create RatelViewer From Options Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

PetscErrorCode RatelViewerListCreateFromOptions(Ratel ratel, const char opt[], const char text[], RatelViewer monitor_viewers[],
                                                PetscInt *num_viewers, PetscBool *set) {
  const char    *option_string;
  const PetscInt n_max     = *num_viewers;
  PetscBool      set_local = PETSC_FALSE;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Create RatelViewer List From Options"));
  *num_viewers = 0;
  PetscCall(PetscOptionsFindPair(NULL, NULL, opt, &option_string, &set_local));

  if (set) *set = set_local;
  if (!set_local) {
    PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Create RatelViewer List From Options Success!"));
    PetscFunctionReturn(PETSC_SUCCESS);
  }

  // Create viewers for each comma separated option
  do {
    char     *next_viewer_string             = NULL;
    char     *comma_separator                = NULL;
    char      tmp_opt[PETSC_MAX_OPTION_NAME] = {0};
    PetscBool set_sub                        = PETSC_FALSE;

    PetscCheck(*num_viewers < n_max, ratel->comm, PETSC_ERR_USER_INPUT, "More viewers than max available (%" PetscInt_FMT ")", n_max);

    PetscCall(PetscStrchr(option_string, ',', &comma_separator));
    if (comma_separator) {
      *comma_separator   = 0;
      next_viewer_string = comma_separator + 1;
    }

    PetscCall(PetscSNPrintf(tmp_opt, sizeof(tmp_opt), "%s__sub%" PetscInt_FMT "", opt, *num_viewers));
    PetscCall(PetscOptionsSetValue(NULL, tmp_opt, option_string));

    PetscCall(RatelViewerCreateFromOptions(ratel, tmp_opt, text, &monitor_viewers[*num_viewers], &set_sub));

    if (set_sub) {
      char     interval_key[PETSC_MAX_OPTION_NAME];
      PetscInt interval = 1;

      PetscOptionsBegin(ratel->comm, NULL, "RatelViewer options", NULL);
      PetscCall(PetscSNPrintf(interval_key, sizeof(interval_key), "%s_interval", opt));
      PetscCall(PetscOptionsInt(interval_key, "Monitor every N timesteps", NULL, interval, &interval, NULL));
      PetscOptionsEnd();

      monitor_viewers[*num_viewers]->interval = interval;
    }

    option_string = next_viewer_string;
    (*num_viewers)++;
  } while (option_string);

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Create RatelViewer List From Options Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Create a `RatelViewer` object.

  Not collective across MPI processes.

  @param[in]   ratel           `Ratel` context
  @param[in]   viewer          `PetscViewer` object to use
  @param[in]   viewer_format   `PetscViewerFormat` object to use
  @param[in]   interval        Monitor interval
  @param[out]  monitor_viewer  `RatelViewer` object to create

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelViewerCreate(Ratel ratel, PetscViewer viewer, PetscViewerFormat viewer_format, PetscInt interval, RatelViewer *monitor_viewer) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Create RatelViewer"));

  // Allocate
  PetscCall(PetscNew(monitor_viewer));
  (*monitor_viewer)->ratel = ratel;

  // Set viewer and format
  (*monitor_viewer)->viewer = viewer;
  PetscCall(PetscObjectReference((PetscObject)viewer));
  PetscCall(PetscViewerGetType(viewer, &(*monitor_viewer)->viewer_type));
  if (viewer_format) (*monitor_viewer)->viewer_format = viewer_format;

  // Set interval
  (*monitor_viewer)->interval = interval;

  // Set header default
  (*monitor_viewer)->is_header_written = PETSC_FALSE;

  // Set filename if needed
  const char *file_name;
  PetscCall(PetscViewerFileGetName(viewer, &file_name));

  if (file_name) {
    char *file_extension;

    PetscCall(PetscStrrchr(file_name, '.', &file_extension));
    {
      PetscBool is_vtu = PETSC_FALSE, is_vtk = PETSC_FALSE;

      PetscCall(PetscStrendswith(file_name, ".vtu", &is_vtu));
      PetscCall(PetscStrendswith(file_name, ".vtk", &is_vtk));
      (*monitor_viewer)->is_monitor_vtk = is_vtu || is_vtk;
    }
    {
      PetscBool is_xml = PETSC_FALSE;

      PetscCall(PetscStrendswith(file_name, ".xmf", &is_xml));
      if (is_xml) {
        (*monitor_viewer)->viewer_format = PETSC_VIEWER_ASCII_XML;
        (*monitor_viewer)->WriteHeader   = RatelViewerWriteHeader_XDMF;
        (*monitor_viewer)->WriteFooter   = RatelViewerWriteFooter_XDMF;
      }
    }
    if ((*monitor_viewer)->is_monitor_vtk || (*monitor_viewer)->viewer_format == PETSC_VIEWER_ASCII_XML) {
      PetscSizeT file_name_len      = file_extension - file_name;
      PetscSizeT file_extension_len = 0;

      PetscCall(PetscStrlen(file_extension, &file_extension_len));
      file_extension_len += 1;
      PetscCall(PetscCalloc1(file_name_len + 1, &(*monitor_viewer)->monitor_file_name));
      PetscCall(PetscSNPrintf((*monitor_viewer)->monitor_file_name, file_name_len, "%s", file_name));

      if ((*monitor_viewer)->is_monitor_vtk) {
        char series_filename[PETSC_MAX_PATH_LEN];

        (*monitor_viewer)->WriteHeader = RatelViewerWriteHeader_VTKSeries;
        (*monitor_viewer)->WriteFooter = RatelViewerWriteFooter_VTKSeries;
        PetscCall(PetscCalloc1(file_extension_len + 1, &(*monitor_viewer)->monitor_file_extension));
        PetscCall(PetscSNPrintf((*monitor_viewer)->monitor_file_extension, file_extension_len, "%s", file_extension));

        // Create auxiliary viewer for .vtu.series file
        PetscCall(PetscSNPrintf(series_filename, sizeof(series_filename), "%s.%s.series", (*monitor_viewer)->monitor_file_name, file_extension));
        PetscCall(PetscViewerASCIIOpen(ratel->comm, series_filename, &(*monitor_viewer)->aux_viewer));
      } else if ((*monitor_viewer)->viewer_format == PETSC_VIEWER_ASCII_XML) {
        const char file_extension[] = "pbin";

        (*monitor_viewer)->WriteHeader = RatelViewerWriteHeader_XDMF;
        (*monitor_viewer)->WriteFooter = RatelViewerWriteFooter_XDMF;
        PetscCall(PetscCalloc1(sizeof(file_extension), &(*monitor_viewer)->monitor_file_extension));
        PetscCall(PetscMemcpy((*monitor_viewer)->monitor_file_extension, file_extension, sizeof(file_extension)));
      }
    }
    {
      PetscBool is_csv = PETSC_FALSE;

      PetscCall(PetscStrendswith(file_name, ".csv", &is_csv));
      if (is_csv) {
        (*monitor_viewer)->viewer_format = PETSC_VIEWER_ASCII_CSV;
        (*monitor_viewer)->WriteHeader   = RatelViewerWriteHeader_CSV;
      }
    }
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Create RatelViewer Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Destroy viewer data for `RatelViewer`.

  Not collective across MPI processes.

  @param[in,out]  monitor_viewer  `RatelViewer` object to destroy

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelViewerDestroy(RatelViewer *monitor_viewer) {
  Ratel ratel;

  PetscFunctionBeginUser;
  if (!monitor_viewer || !*monitor_viewer) PetscFunctionReturn(PETSC_SUCCESS);

  ratel = (*monitor_viewer)->ratel;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Destroy RatelViewer"));

  // Write footer if needed
  PetscCall(RatelViewerWriteFooter(*monitor_viewer));

  // Filename info
  PetscCall(PetscFree((*monitor_viewer)->monitor_file_name));
  PetscCall(PetscFree((*monitor_viewer)->monitor_file_extension));

  // PETSc objects
  PetscCall(PetscViewerDestroy(&(*monitor_viewer)->viewer));
  PetscCall(PetscViewerDestroy(&(*monitor_viewer)->aux_viewer));

  // Deallocate
  PetscCall(PetscFree(*monitor_viewer));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Destroy RatelViewer Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Write header for a `RatelViewer`.

  Collective across MPI processes.

  @param[in]   ratel_viewer  `RatelViewer` context
  @param[out]  ctx           Header context, optional

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelViewerWriteHeader(RatelViewer ratel_viewer, void *ctx) {
  PetscFunctionBeginUser;
  if (!ratel_viewer->is_header_written && ratel_viewer->WriteHeader) PetscCall(ratel_viewer->WriteHeader(ratel_viewer, ctx));
  ratel_viewer->is_header_written = PETSC_TRUE;
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Write footer for a `RatelViewer`.

  Collective across MPI processes.

  @param[in]  ratel_viewer  `RatelViewer` context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelViewerWriteFooter(RatelViewer ratel_viewer) {
  PetscBool should_write = ratel_viewer->is_header_written && !ratel_viewer->is_footer_written && ratel_viewer->WriteFooter;

  PetscFunctionBeginUser;
  if (should_write) PetscCall(ratel_viewer->WriteFooter(ratel_viewer));
  ratel_viewer->is_footer_written = PETSC_TRUE;
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get the filename for an entry in a sequence of files.

  The filename is `{ratel_viewer->monitor_file_name}_{steps}.{ratel_viewer->monitor_file_extension}`.

  Collective across MPI processes.

  @param[in]   ratel_viewer   `RatelViewer` context
  @param[in]   steps          Timestep number
  @param[in]   filename_size  Size of `filename`
  @param[out]  filename       String to store the filename, must be preallocated

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelViewerGetSequenceFilename(RatelViewer ratel_viewer, PetscInt steps, PetscSizeT filename_size, char filename[]) {
  Ratel ratel = ratel_viewer->ratel;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel PetscViewer Get Sequence Filename"));

  PetscCall(PetscSNPrintf(filename, filename_size, "%s_%06" PetscInt_FMT ".%s", ratel_viewer->monitor_file_name, steps,
                          ratel_viewer->monitor_file_extension));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel PetscViewer Get Sequence Filename Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Determine whether the `RatelViewer` should write at the current time step.

  Not collective across MPI processes.

  @param[in]   viewer        `RatelViewer` object
  @param[in]   ts            `TS` object
  @param[in]   steps         Current number of time steps
  @param[out]  should_write  True if the viewer should write at the current time step

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelViewerShouldWrite(RatelViewer viewer, TS ts, PetscInt steps, PetscBool *should_write) {
  TSConvergedReason reason = TS_CONVERGED_ITERATING;

  PetscFunctionBeginUser;

  *should_write = PETSC_FALSE;
  PetscCall(TSGetConvergedReason(ts, &reason));
  // Do not write if diverged
  if (reason < 0) PetscFunctionReturn(PETSC_SUCCESS);
  // Write every viewer->interval steps or if converged
  *should_write = steps % viewer->interval == 0 || reason > 0;
  PetscFunctionReturn(PETSC_SUCCESS);
}

// -----------------------------------------------------------------------------
// RatelViewers (for TS Monitors)
// -----------------------------------------------------------------------------
/**
  @brief Create a `RatelViewers` object from `PetscOptions` command line handling.

  Collective across MPI processes.

  @param[in]   ratel        `Ratel` context
  @param[in]   opt          `TS` monitor flag
  @param[in]   text         Description of the flag
  @param[in]   num_viewers  Number of viewers to create
  @param[in]   names        Names for each viewer
  @param[out]  viewer_list  `RatelViewer` object to create
  @param[out]  set          `PetscBool` indicating if the flag was set

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelViewersCreateFromOptions(Ratel ratel, const char opt[], const char text[], PetscInt num_viewers, const char **names,
                                             RatelViewers *viewer_list, PetscBool *set) {
  char              interval_key[PETSC_MAX_OPTION_NAME];
  PetscInt          interval = 1;
  PetscViewer       viewer;
  PetscViewerFormat viewer_format;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Create RatelViewers From Options"));

  // Get command line options
  PetscOptionsBegin(ratel->comm, NULL, "RatelViewer options", NULL);
  PetscCall(PetscOptionsViewer(opt, text, NULL, &viewer, &viewer_format, set));
  PetscCall(PetscSNPrintf(interval_key, sizeof interval_key, "%s_interval", opt));
  PetscCall(PetscOptionsInt(interval_key, "Monitor every N timesteps", NULL, interval, &interval, NULL));
  PetscOptionsEnd();

  // Create the RatelViewer
  if (*set) {
    const char *filename                          = NULL;
    char        filename_copy[PETSC_MAX_PATH_LEN] = {0};
    PetscBool   is_stdout                         = PETSC_FALSE;

    PetscCall(PetscViewerFileGetName(viewer, &filename));
    PetscCall(PetscStrncpy(filename_copy, filename, sizeof(filename_copy)));
    if (filename) PetscCall(PetscStrncmp(filename, "stdout", 7, &is_stdout));
    PetscCall(RatelViewersCreate(ratel, viewer, viewer_format, interval, num_viewers, names, viewer_list));
    if (!is_stdout) {
      // Delete empty file
      PetscCall(MPI_Barrier(ratel->comm));
      if (ratel->comm_rank == 0) (void)remove(filename_copy);
    }
  }
  PetscCall(PetscViewerDestroy(&viewer));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Create RatelViewers From Options Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Create a `RatelViewers` object.

  Collective across MPI processes.

  @param[in]   ratel          `Ratel` context
  @param[in]   viewer         `PetscViewer` object to use
  @param[in]   viewer_format  `PetscViewerFormat` object to use
  @param[in]   interval       Monitor interval
  @param[in]   num_viewers    Number of viewers to create
  @param[in]   names          Names for each viewer
  @param[out]  viewer_list    `RatelViewers` object to create

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelViewersCreate(Ratel ratel, PetscViewer viewer, PetscViewerFormat viewer_format, PetscInt interval, PetscInt num_viewers,
                                  const char **names, RatelViewers *viewer_list) {
  const char *filename  = NULL;
  PetscBool   is_stdout = PETSC_FALSE;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Create RatelViewers"));

  // Allocate
  PetscCall(PetscNew(viewer_list));
  (*viewer_list)->ratel       = ratel;
  (*viewer_list)->num_viewers = num_viewers;
  PetscCall(PetscCalloc1(num_viewers, &(*viewer_list)->viewers));

  // Set viewer and format
  PetscCall(PetscViewerFileGetName(viewer, &filename));
  if (filename) PetscCall(PetscStrncmp(filename, "stdout", 7, &is_stdout));
  if (!filename || is_stdout) {
    // Using stdout
    for (PetscInt i = 0; i < num_viewers; i++) {
      PetscCall(RatelViewerCreate(ratel, viewer, viewer_format, interval, &(*viewer_list)->viewers[i]));
    }
  } else {
    size_t          root_len = 0;
    char            filename_no_ext[PETSC_MAX_PATH_LEN];
    char           *extension = NULL;
    PetscViewerType viewer_type;
    PetscBool       is_restart = ratel->initial_condition_type == RATEL_INITIAL_CONDITION_CONTINUE;
    PetscBool       is_csv     = PETSC_FALSE;

    PetscCall(PetscStrrchr(filename, '.', &extension));
    {
      size_t filename_len = 0, extension_len = 0;

      PetscCall(PetscStrlen(filename, &filename_len));
      PetscCall(PetscStrlen(extension, &extension_len));
      root_len = filename_len - extension_len;
    }
    PetscCall(PetscSNPrintf(filename_no_ext, root_len, "%s", filename));
    PetscCall(PetscViewerGetType(viewer, &viewer_type));
    PetscCall(RatelViewerStringIsCSV(ratel, viewer_type, filename, &is_csv));

    for (PetscInt i = 0; i < num_viewers; i++) {
      char        full_filename[PETSC_MAX_PATH_LEN];
      PetscViewer viewer_i          = NULL;
      PetscBool   is_header_written = PETSC_FALSE;

      if (names != NULL) {
        // Use names to create viewers
        PetscCall(PetscSNPrintf(full_filename, sizeof(full_filename), "%s_%s.%s", filename_no_ext, names[i], extension));
        // Use names to create viewers
      } else {
        // Use index to create viewers
        PetscCall(PetscSNPrintf(full_filename, sizeof(full_filename), "%s_%" PetscInt_FMT ".%s", filename_no_ext, i, extension));
      }

      // Create PetscViewer
      // If restarting, we want to append to the file in some cases
      if (is_restart && is_csv) {
        RatelCheckpointData checkpoint_data;

        PetscCall(RatelCheckpointDataRead(ratel, ratel->continue_file_name, &checkpoint_data));
        PetscCall(RatelLoadViewerFromCheckpoint_CSV(ratel, full_filename, checkpoint_data, &viewer_i));
        PetscCall(RatelCheckpointDataDestroy(&checkpoint_data));
        is_header_written = viewer_i != NULL;
      }
      if (!viewer_i) {
        PetscCall(PetscViewerCreate(ratel->comm, &viewer_i));
        PetscCall(PetscViewerSetType(viewer_i, viewer_type));
        PetscCall(PetscViewerFileSetName(viewer_i, full_filename));
        PetscCall(PetscViewerFileSetMode(viewer_i, FILE_MODE_WRITE));
        PetscCall(PetscViewerSetUp(viewer_i));
      }

      // Create RatelViewer
      PetscCall(RatelViewerCreate(ratel, viewer_i, viewer_format, interval, &(*viewer_list)->viewers[i]));
      (*viewer_list)->viewers[i]->is_header_written = is_header_written;

      // Cleanup
      PetscCall(PetscViewerDestroy(&viewer_i));
    }
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Create RatelViewers Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Destroy viewer list data for `RatelViewers`.

  Collective across MPI processes.

  @param[in,out]  viewer_list  `RatelViewers` object to destroy

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelViewersDestroy(RatelViewers *viewer_list) {
  Ratel ratel;

  PetscFunctionBeginUser;
  if (!*viewer_list) PetscFunctionReturn(PETSC_SUCCESS);

  ratel = (*viewer_list)->ratel;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Destroy RatelViewers"));

  // Viewers
  for (PetscInt i = 0; i < (*viewer_list)->num_viewers; i++) {
    PetscCall(RatelViewerDestroy(&(*viewer_list)->viewers[i]));
  }
  PetscCall(PetscFree((*viewer_list)->viewers));

  // Deallocate
  PetscCall(PetscFree(*viewer_list));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Destroy RatelViewers Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}
