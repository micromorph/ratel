/// @file
/// Implementation of Core Ratel interfaces

#include <ceed-boundary-evaluator.h>
#include <ceed-evaluator.h>
#include <ceed.h>
#include <petsc-ceed-utils.h>
#include <petscdmplex.h>
#include <petscdmswarm.h>
#include <petscksp.h>
#include <ratel-boundary.h>
#include <ratel-cl-options.h>
#include <ratel-impl.h>
#include <ratel-material.h>
#include <ratel-model.h>
#include <ratel-monitor.h>
#include <ratel-pmg.h>
#include <ratel-solver.h>
#include <ratel-utils.h>
#include <ratel.h>
#include <ratel/models/platen.h>
#include <stdarg.h>
#include <stdbool.h>
#include <stdio.h>
#include <stdlib.h>

/// @addtogroup RatelInternal
/// @{

PetscClassId  RATEL_CLASSID;
PetscLogStage RATEL_Setup;
PetscLogEvent RATEL_DMSetupByOrder, RATEL_DMSetupSolver, RATEL_Diagnostics, RATEL_Diagnostics_CeedOp, RATEL_Residual, RATEL_Residual_CeedOp,
    RATEL_Jacobian, RATEL_Jacobian_CeedOp;

/**
  @brief Print `Ratel` debugging information in color.

  @param[in]  comm       MPI communicator for debugging
  @param[in]  is_debug   Boolean flag for debugging
  @param[in]  all_ranks  Boolean flag to force printing on all ranks
  @param[in]  comm_rank  MPI communicator rank
  @param[in]  color      Color to print
  @param[in]  format     Printing format
**/
// LCOV_EXCL_START
PetscErrorCode RatelDebugImpl256(MPI_Comm comm, PetscBool is_debug, PetscBool all_ranks, PetscInt comm_rank, const unsigned char color,
                                 const char *format, ...) {
  va_list args;

  PetscFunctionBeginUser;
  if (!is_debug && !all_ranks) PetscFunctionReturn(PETSC_SUCCESS);
  if (all_ranks) {
    if (color != RATEL_DEBUG_COLOR_NONE) PetscCall(PetscSynchronizedFPrintf(comm, PETSC_STDOUT, "\033[38;5;%dm", color));
    if (comm_rank == -1) {
      PetscMPIInt mpi_rank = -1;

      PetscCallMPI(MPI_Comm_rank(PETSC_COMM_WORLD, &mpi_rank));
      comm_rank = mpi_rank;
    }
    if (all_ranks) PetscCall(PetscSynchronizedFPrintf(comm, PETSC_STDOUT, "Printing from rank %" PetscInt_FMT "\n", comm_rank));

    va_start(args, format);
    PetscCall(PetscSynchronizedFPrintf(comm, PETSC_STDOUT, format, args));
    va_end(args);

    if (color != RATEL_DEBUG_COLOR_NONE) PetscCall(PetscSynchronizedFPrintf(comm, PETSC_STDOUT, "\033[m"));
    PetscCall(PetscSynchronizedFPrintf(comm, PETSC_STDOUT, "\n"));
    PetscCall(PetscSynchronizedFlush(comm, PETSC_STDOUT));
  } else if (comm_rank == 0) {
    if (color != RATEL_DEBUG_COLOR_NONE) PetscCall(PetscFPrintf(comm, PETSC_STDOUT, "\033[38;5;%dm", color));

    va_start(args, format);
    PetscCall(PetscVFPrintf(PETSC_STDOUT, format, args));
    va_end(args);

    if (color != RATEL_DEBUG_COLOR_NONE) PetscCall(PetscFPrintf(comm, PETSC_STDOUT, "\033[m"));
    PetscCall(PetscPrintf(comm, "\n"));
    fflush(PETSC_STDOUT);
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}
// LCOV_EXCL_STOP

/// @}

/// @addtogroup RatelCore
/// @{

/**
  @brief Get `Ratel` library version info.

  `Ratel` version numbers have the form major.minor.patch.
  Non-release versions may contain unstable interfaces..

  Not collective across MPI processes.

  @param[out]  major    Major version of the library
  @param[out]  minor    Minor version of the library
  @param[out]  patch    Patch (subminor) version of the library
  @param[out]  release  True for releases; false for development branches.

  The caller may pass NULL for any arguments that are not needed.

  @sa RATEL_VERSION_GE()

  @return An error code: 0 - success, otherwise - failure
**/
// LCOV_EXCL_START
PetscErrorCode RatelGetVersion(int *major, int *minor, int *patch, PetscBool *release) {
  if (major) *major = RATEL_VERSION_MAJOR;
  if (minor) *minor = RATEL_VERSION_MINOR;
  if (patch) *patch = RATEL_VERSION_PATCH;
  if (release) *release = RATEL_VERSION_RELEASE;
  return PETSC_SUCCESS;
}
// LCOV_EXCL_STOP

/**
  @brief Register core Ratel log events.

  Not collective across MPI processes.

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelRegisterLogEvents() {
  static bool registered = false;

  PetscFunctionBeginUser;
  if (registered) PetscFunctionReturn(PETSC_SUCCESS);
  PetscCall(PetscClassIdRegister("Ratel", &RATEL_CLASSID));

  PetscCall(PetscLogStageRegister("Ratel Setup", &RATEL_Setup));
  PetscCall(PetscLogEventRegister("RatelSetupFEM", RATEL_CLASSID, &RATEL_DMSetupByOrder));
  PetscCall(PetscLogEventRegister("RatelSetupSolver", RATEL_CLASSID, &RATEL_DMSetupSolver));
  PetscCall(PetscLogEventRegister("RatelDgnstic", RATEL_CLASSID, &RATEL_Diagnostics));
  PetscCall(PetscLogEventRegister("RatelDgnsticCeed", RATEL_CLASSID, &RATEL_Diagnostics_CeedOp));
  PetscCall(PetscLogEventRegister("RatelResApp", RATEL_CLASSID, &RATEL_Residual));
  PetscCall(PetscLogEventRegister("RatelResAppCeed", RATEL_CLASSID, &RATEL_Residual_CeedOp));
  PetscCall(PetscLogEventRegister("RatelJacApp", RATEL_CLASSID, &RATEL_Jacobian));
  PetscCall(PetscLogEventRegister("RatelJacAppCeed", RATEL_CLASSID, &RATEL_Jacobian_CeedOp));
  registered = true;
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup `Ratel` context object.
         Note: This function call initializes the `libCEED` context.

  Collective across MPI processes.

  @param[in]   comm   MPI communication object
  @param[out]  ratel  `Ratel` context object

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelInit(MPI_Comm comm, Ratel *ratel) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebugEnv256(RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Init"));

  PetscCall(RatelRegisterLogEvents());
  PetscCall(PetscLogStagePush(RATEL_Setup));

  PetscCall(PetscNew(ratel));
  (*ratel)->comm = comm;

  // Record env variables RATEL_DEBUG or DBG
  {
    PetscMPIInt comm_rank = -1;

    PetscCallMPI(MPI_Comm_rank(comm, &comm_rank));
    (*ratel)->comm_rank       = comm_rank;
    (*ratel)->is_debug        = (!!getenv("RATEL_DEBUG") || !!getenv("DEBUG") || !!getenv("DBG"));
    (*ratel)->is_rank_0_debug = ((*ratel)->is_debug && comm_rank == 0);
    PetscCall(RatelDebug256(*ratel, RATEL_DEBUG_COLOR_SUCCESS, "---------- Ratel debugging mode active ----------"));
  }

  // Register models
  PetscCall(RatelDebug(*ratel, "---- Registering models"));
  (*ratel)->material_create_functions = NULL;
  PetscCall(RatelRegisterModels(*ratel, &(*ratel)->material_create_functions));

  // Set MAT_SPD default to true
  (*ratel)->is_spd = PETSC_TRUE;

  // Read basic command line options
  PetscCall(RatelDebug(*ratel, "---- Processing command line options"));
  PetscCall(RatelProcessCommandLineOptions(*ratel));

  // Check for MMS support
  (*ratel)->has_mms = PETSC_TRUE;
  {
    PetscInt bc_mms_count = 0;

    // -- Material model and forcing support
    for (PetscInt i = 0; i < (*ratel)->num_materials; i++) {
      PetscBool        has_material_mms;
      RatelForcingType forcing_type;

      PetscCall(RatelMaterialHasMMS((*ratel)->materials[i], &has_material_mms));
      (*ratel)->has_mms &= has_material_mms;
      PetscCall(RatelMaterialGetForcingType((*ratel)->materials[i], &forcing_type));
      (*ratel)->has_mms &= forcing_type == RATEL_FORCING_MMS;
    }
    // -- Boundary condition support
    for (PetscInt i = 0; i < (*ratel)->num_active_fields; i++) bc_mms_count += (*ratel)->bc_mms_count[i];
    (*ratel)->has_mms &= (bc_mms_count > 0);
  }

  // Register PMG PC
  PetscCall(RatelPCRegisterPMG(*ratel));

  PetscCall(PetscLogStagePop());

  PetscCall(RatelDebug256(*ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Init Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief View a `Ratel` context.
         Note: This function can be called with the command line option `-ratel_view`.

  Collective across MPI processes.

  @param[in]  ratel   `Ratel` context to view
  @param[in]  viewer  Optional visualization context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelView(Ratel ratel, PetscViewer viewer) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel View"));

  if (!viewer) PetscCall(PetscViewerASCIIGetStdout(ratel->comm, &viewer));

  PetscCall(PetscViewerASCIIPrintf(viewer, "Ratel Context:\n"));
  PetscCall(PetscViewerASCIIPushTab(viewer));
  {
    PetscMPIInt comm_size;
    char        hostname[PETSC_MAX_PATH_LEN];

    PetscCall(MPI_Comm_size(ratel->comm, &comm_size));
    PetscCall(PetscGetHostName(hostname, sizeof(hostname)));
    PetscCall(PetscViewerASCIIPrintf(viewer, "MPI:\n"));
    PetscCall(PetscViewerASCIIPushTab(viewer));
    PetscCall(PetscViewerASCIIPrintf(viewer, "Hostname: %s\n", hostname));
    PetscCall(PetscViewerASCIIPrintf(viewer, "Total ranks: %d\n", comm_size));
    PetscCall(PetscViewerASCIIPopTab(viewer));
  }
  {
    const char *ceed_resource_used;
    CeedMemType mem_type_backend;

    RatelCallCeed(ratel, CeedGetResource(ratel->ceed, &ceed_resource_used));
    RatelCallCeed(ratel, CeedGetPreferredMemType(ratel->ceed, &mem_type_backend));

    PetscCall(PetscViewerASCIIPrintf(viewer, "libCEED:\n"));
    PetscCall(PetscViewerASCIIPushTab(viewer));
    PetscCall(PetscViewerASCIIPrintf(viewer, "Backend resource: %s\n", ceed_resource_used));
    PetscCall(PetscViewerASCIIPrintf(viewer, "Backend MemType: %s\n", CeedMemTypes[mem_type_backend]));
    PetscCall(PetscViewerASCIIPopTab(viewer));
  }
  if (ratel->dm_solution) {
    // LCOV_EXCL_START
    PetscInt num_global_dofs, num_local_dofs;
    VecType  vec_type;
    MatType  mat_type;
    DM       dm_solution;

    PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));
    PetscCall(DMGetVecType(dm_solution, &vec_type));
    PetscCall(DMGetMatType(dm_solution, &mat_type));
    PetscCall(PetscViewerASCIIPrintf(viewer, "PETSc:\n"));
    PetscCall(PetscViewerASCIIPushTab(viewer));
    PetscCall(PetscViewerASCIIPrintf(viewer, "VecType: %s\n", vec_type));
    PetscCall(PetscViewerASCIIPrintf(viewer, "MatType: %s\n", mat_type));
    PetscCall(PetscViewerASCIIPopTab(viewer));

    if (ratel->method_type == RATEL_METHOD_MPM) {
      PetscInt num_global_points = 0, num_local_points = 0;

      PetscCall(DMSwarmGetLocalSize(ratel->dm_solution, &num_local_points));

      PetscInt num_local_points_minmax[] = {num_local_points, -num_local_points};
      PetscCall(MPIU_Allreduce(MPI_IN_PLACE, num_local_points_minmax, 2, MPIU_INT, MPIU_MIN, ratel->comm));
      PetscCall(MPIU_Allreduce(&num_local_points, &num_global_points, 1, MPIU_INT, MPIU_SUM, ratel->comm));

      PetscCall(PetscViewerASCIIPrintf(viewer, "Swarm:\n"));
      PetscCall(PetscViewerASCIIPushTab(viewer));
      PetscCall(PetscViewerASCIIPrintf(viewer, "Global Points: %" PetscInt_FMT "\n", num_global_points));
      PetscCall(PetscViewerASCIIPrintf(viewer, "Local Points (min/max): [%" PetscInt_FMT ", %" PetscInt_FMT "]\n", num_local_points_minmax[0],
                                       -num_local_points_minmax[1]));
      PetscCall(PetscViewerASCIIPopTab(viewer));
    }
    {
      Vec U;

      PetscCall(DMGetGlobalVector(dm_solution, &U));
      PetscCall(VecGetSize(U, &num_global_dofs));
      PetscCall(VecGetLocalSize(U, &num_local_dofs));
      PetscCall(DMRestoreGlobalVector(dm_solution, &U));
    }
    PetscInt num_local_dofs_minmax[] = {num_local_dofs, -num_local_dofs};
    PetscCallMPI(MPIU_Allreduce(MPI_IN_PLACE, num_local_dofs_minmax, 2, MPIU_INT, MPI_MIN, ratel->comm));
    PetscCall(PetscViewerASCIIPrintf(viewer, "Mesh:\n"));
    PetscCall(PetscViewerASCIIPushTab(viewer));
    PetscCall(PetscViewerASCIIPrintf(viewer, "Global DoFs: %" PetscInt_FMT "\n", num_global_dofs));
    PetscCall(PetscViewerASCIIPrintf(viewer, "Local DoFs (min/max): [%" PetscInt_FMT ", %" PetscInt_FMT "]\n", num_local_dofs_minmax[0],
                                     -num_local_dofs_minmax[1]));
    PetscCall(PetscViewerASCIIPopTab(viewer));
    PetscCall(DMDestroy(&dm_solution));
    // LCOV_EXCL_STOP
  } else {
    PetscCall(PetscViewerASCIIPrintf(viewer, "PETSc: DM not yet setup\n"));
  }
  PetscCall(PetscViewerASCIIPrintf(viewer, "Method: %s\n", RatelMethodTypes[ratel->method_type]));
  PetscCall(PetscViewerASCIIPrintf(viewer, "Solver: %s\n", RatelSolverTypes[ratel->solver_type]));
  PetscCall(PetscViewerASCIIPrintf(viewer, "Materials:\n"));
  PetscCall(PetscViewerASCIIPushTab(viewer));
  for (PetscInt material_index = 0; material_index < ratel->num_materials; material_index++) {
    PetscCall(RatelMaterialView(ratel->materials[material_index], viewer));
  }
  PetscCall(PetscViewerASCIIPopTab(viewer));
  PetscCall(PetscViewerASCIIPrintf(viewer, "Ceed Evaluators:\n"));
  PetscCall(PetscViewerASCIIPushTab(viewer));
  if (ratel->evaluator_residual_u) PetscCall(CeedEvaluatorView(ratel->evaluator_residual_u, viewer));
  if (ratel->evaluator_residual_ut) PetscCall(CeedEvaluatorView(ratel->evaluator_residual_ut, viewer));
  if (ratel->evaluator_residual_utt) PetscCall(CeedEvaluatorView(ratel->evaluator_residual_utt, viewer));
  if (ratel->mat_jacobian) PetscCall(MatView(ratel->mat_jacobian, viewer));
  PetscCall(PetscViewerASCIIPopTab(viewer));
  PetscCall(PetscViewerASCIIPrintf(viewer, "Boundary Conditions:\n"));
  PetscCall(PetscViewerASCIIPushTab(viewer));
  for (PetscInt i = 0; i < ratel->num_active_fields; i++) {
    if (ratel->bc_mms_count[i]) {
      PetscCall(PetscViewerASCIIPrintf(viewer, "Dirichlet MMS boundaries, field %" PetscInt_FMT ":\n", i));
      PetscCall(PetscViewerASCIIPushTab(viewer));
      for (PetscInt j = 0; j < ratel->bc_mms_count[i]; j++) {
        PetscCall(PetscViewerASCIIPrintf(viewer, "Face %" PetscInt_FMT ":\n", ratel->bc_mms_faces[i][j]));
      }
      PetscCall(PetscViewerASCIIPopTab(viewer));
    }
  }
  for (PetscInt i = 0; i < ratel->num_active_fields; i++) {
    if (ratel->bc_clamp_count[i]) {
      PetscCall(PetscViewerASCIIPrintf(viewer, "Dirichlet (clamp) boundaries, field %" PetscInt_FMT ":\n", i));
      PetscCall(PetscViewerASCIIPushTab(viewer));
      for (PetscInt j = 0; j < ratel->bc_clamp_count[i]; j++) {
        RatelBCClampParams params_clamp;

        PetscCall(RatelBoundaryClampDataFromOptions(ratel, i, j, &params_clamp));
        PetscCall(PetscViewerASCIIPrintf(viewer, "Face %" PetscInt_FMT ":  %d translation(s)/rotation(s) with %s\n", ratel->bc_clamp_faces[i][j],
                                         params_clamp.num_times, RatelBCInterpolationTypes[params_clamp.interpolation_type]));
        PetscCall(PetscViewerASCIIPushTab(viewer));
        for (PetscInt t = 0; t < params_clamp.num_times; t++) {
          PetscCall(PetscViewerASCIIPrintf(viewer, "t = %.3f:\n", params_clamp.times[t]));
          PetscCall(PetscViewerASCIIPushTab(viewer));
          PetscCall(PetscViewerASCIIPrintf(viewer, "Translation:         [%f", params_clamp.translation[params_clamp.num_comp * t]));
          PetscCall(PetscViewerASCIIUseTabs(viewer, PETSC_FALSE));
          for (PetscInt k = 1; k <= params_clamp.num_comp; k++) {
            PetscCall(PetscViewerASCIIPrintf(viewer, ", %f", params_clamp.translation[params_clamp.num_comp * t + k]));
          }
          PetscCall(PetscViewerASCIIPrintf(viewer, "]\n"));
          PetscCall(PetscViewerASCIIUseTabs(viewer, PETSC_TRUE));
          PetscCall(PetscViewerASCIIPrintf(viewer, "Rotation axis:       [%f", params_clamp.rotation_axis[params_clamp.num_comp * t]));
          PetscCall(PetscViewerASCIIUseTabs(viewer, PETSC_FALSE));
          for (PetscInt k = 1; k <= params_clamp.num_comp; k++) {
            PetscCall(PetscViewerASCIIPrintf(viewer, ", %f", params_clamp.rotation_axis[params_clamp.num_comp * t + k]));
          }
          PetscCall(PetscViewerASCIIPrintf(viewer, "]\n"));
          PetscCall(PetscViewerASCIIUseTabs(viewer, PETSC_TRUE));
          PetscCall(PetscViewerASCIIPrintf(viewer, "Rotation polynomial: [%f, %f]\n", params_clamp.rotation_polynomial[2 * t],
                                           params_clamp.rotation_polynomial[2 * t + 1]));
          PetscCall(PetscViewerASCIIPopTab(viewer));
        }
        PetscCall(PetscViewerASCIIPopTab(viewer));
        if (ratel->method_type == RATEL_METHOD_MPM) {
          PetscCall(RatelIncrementalize(ratel, PETSC_FALSE, params_clamp.num_comp, &params_clamp.num_times, params_clamp.translation,
                                        params_clamp.times, &params_clamp.interpolation_type));
          PetscCall(PetscViewerASCIIPrintf(viewer, "Face %" PetscInt_FMT ":  Incrementalized to %d velocity(s)/angular velocities(s) with %s\n",
                                           ratel->bc_clamp_faces[i][j], params_clamp.num_times,
                                           RatelBCInterpolationTypes[params_clamp.interpolation_type]));
          PetscCall(PetscViewerASCIIPushTab(viewer));
          for (PetscInt t = 0; t < params_clamp.num_times; t++) {
            PetscCall(PetscViewerASCIIPrintf(viewer, "t = %.3f:\n", params_clamp.times[t]));
            PetscCall(PetscViewerASCIIPushTab(viewer));
            PetscCall(PetscViewerASCIIPrintf(viewer, "Translation:         [%f", params_clamp.translation[params_clamp.num_comp * t]));
            PetscCall(PetscViewerASCIIUseTabs(viewer, PETSC_FALSE));
            for (PetscInt k = 1; k <= params_clamp.num_comp; k++) {
              PetscCall(PetscViewerASCIIPrintf(viewer, ", %f", params_clamp.translation[params_clamp.num_comp * t + k]));
            }
            PetscCall(PetscViewerASCIIPrintf(viewer, "]\n"));
            PetscCall(PetscViewerASCIIUseTabs(viewer, PETSC_TRUE));
            PetscCall(PetscViewerASCIIPrintf(viewer, "Rotation axis:       [%f", params_clamp.rotation_axis[params_clamp.num_comp * t]));
            PetscCall(PetscViewerASCIIUseTabs(viewer, PETSC_FALSE));
            for (PetscInt k = 1; k <= params_clamp.num_comp; k++) {
              PetscCall(PetscViewerASCIIPrintf(viewer, ", %f", params_clamp.rotation_axis[params_clamp.num_comp * t + k]));
            }
            PetscCall(PetscViewerASCIIPrintf(viewer, "]\n"));
            PetscCall(PetscViewerASCIIUseTabs(viewer, PETSC_TRUE));
            PetscCall(PetscViewerASCIIPrintf(viewer, "Rotation polynomial: [%f, %f]\n", params_clamp.rotation_polynomial[2 * t],
                                             params_clamp.rotation_polynomial[2 * t + 1]));
            PetscCall(PetscViewerASCIIPopTab(viewer));
          }
          PetscCall(PetscViewerASCIIPopTab(viewer));
        }
      }
      PetscCall(PetscViewerASCIIPopTab(viewer));
    }
  }
  for (PetscInt i = 0; i < ratel->num_active_fields; i++) {
    if (ratel->bc_slip_count[i]) {
      PetscCall(PetscViewerASCIIPrintf(viewer, "Slip boundaries, field %" PetscInt_FMT ":\n", i));
      PetscCall(PetscViewerASCIIPushTab(viewer));
      for (PetscInt j = 0; j < ratel->bc_slip_count[i]; j++) {
        RatelBCSlipParams params_slip;

        PetscCall(RatelBoundarySlipDataFromOptions(ratel, i, j, &params_slip));
        PetscCall(PetscViewerASCIIPrintf(viewer, "Face %" PetscInt_FMT ": %d translation(s) with %s\n", ratel->bc_slip_faces[i][j],
                                         params_slip.num_times, RatelBCInterpolationTypes[params_slip.interpolation_type]));
        PetscCall(PetscViewerASCIIPushTab(viewer));
        for (PetscInt t = 0; t < params_slip.num_times; t++) {
          PetscCall(PetscViewerASCIIPrintf(viewer, "t = %.3f: [t%c: %.3f", params_slip.times[t], 'x' + params_slip.components[0],
                                           params_slip.translation[params_slip.num_comp_slip * t + 0]));
          PetscCall(PetscViewerASCIIUseTabs(viewer, PETSC_FALSE));
          for (PetscInt k = 1; k < params_slip.num_comp_slip; k++) {
            PetscCall(PetscViewerASCIIPrintf(viewer, ", t%c: %.3f", 'x' + params_slip.components[k],
                                             params_slip.translation[params_slip.num_comp_slip * t + k]));
          }
          PetscCall(PetscViewerASCIIPrintf(viewer, "]\n"));
          PetscCall(PetscViewerASCIIUseTabs(viewer, PETSC_TRUE));
        }
        PetscCall(PetscViewerASCIIPopTab(viewer));
        if (ratel->method_type == RATEL_METHOD_MPM) {
          PetscCall(RatelIncrementalize(ratel, PETSC_FALSE, params_slip.num_comp, &params_slip.num_times, params_slip.translation, params_slip.times,
                                        &params_slip.interpolation_type));
          PetscCall(PetscViewerASCIIPrintf(viewer, "Face %" PetscInt_FMT ": Incrementalized to %d velocities(s) with %s\n",
                                           ratel->bc_slip_faces[i][j], params_slip.num_times,
                                           RatelBCInterpolationTypes[params_slip.interpolation_type]));
          PetscCall(PetscViewerASCIIPushTab(viewer));
          for (PetscInt t = 0; t < params_slip.num_times; t++) {
            PetscCall(PetscViewerASCIIPrintf(viewer, "t = %.3f: [t%c: %.3f", params_slip.times[t], 'x' + params_slip.components[0],
                                             params_slip.translation[params_slip.num_comp_slip * t + 0]));
            PetscCall(PetscViewerASCIIUseTabs(viewer, PETSC_FALSE));
            for (PetscInt k = 1; k < params_slip.num_comp_slip; k++) {
              PetscCall(PetscViewerASCIIPrintf(viewer, ", t%c: %.3f", 'x' + params_slip.components[k],
                                               params_slip.translation[params_slip.num_comp_slip * t + k]));
            }
            PetscCall(PetscViewerASCIIPrintf(viewer, "]\n"));
            PetscCall(PetscViewerASCIIUseTabs(viewer, PETSC_TRUE));
          }
          PetscCall(PetscViewerASCIIPopTab(viewer));
        }
      }
      PetscCall(PetscViewerASCIIPopTab(viewer));
    }
  }
  if (ratel->bc_traction_count) {
    PetscCall(PetscViewerASCIIPrintf(viewer, "Neumann (traction) boundaries:\n"));
    PetscCall(PetscViewerASCIIPushTab(viewer));
    for (PetscInt i = 0; i < ratel->bc_traction_count; i++) {
      RatelBCTractionParams params_traction;

      PetscCall(RatelBoundaryTractionDataFromOptions(ratel, i, &params_traction));
      PetscCall(PetscViewerASCIIPrintf(viewer, "Face %" PetscInt_FMT ": %d traction vector(s) with %s\n", ratel->bc_traction_faces[i],
                                       params_traction.num_times, RatelBCInterpolationTypes[params_traction.interpolation_type]));
      PetscCall(PetscViewerASCIIPushTab(viewer));
      for (PetscInt t = 0; t < params_traction.num_times; t++) {
        PetscCall(PetscViewerASCIIPrintf(viewer, "t = %.3f: [%f, %f, %f]\n", params_traction.times[t], params_traction.direction[3 * t],
                                         params_traction.direction[3 * t + 1], params_traction.direction[3 * t + 2]));
      }
      PetscCall(PetscViewerASCIIPopTab(viewer));
    }
    PetscCall(PetscViewerASCIIPopTab(viewer));
  }
  if (ratel->bc_pressure_count) {
    PetscCall(PetscViewerASCIIPrintf(viewer, "Pressure boundaries:\n"));
    PetscCall(PetscViewerASCIIPushTab(viewer));
    for (PetscInt i = 0; i < ratel->bc_pressure_count; i++) {
      RatelBCPressureParams params_pressure;

      PetscCall(RatelBoundaryPressureDataFromOptions(ratel, i, &params_pressure));
      PetscCall(PetscViewerASCIIPrintf(viewer, "Face %" PetscInt_FMT ": %d pressure values with %s\n", ratel->bc_pressure_faces[i],
                                       params_pressure.num_times, RatelBCInterpolationTypes[params_pressure.interpolation_type]));
      PetscCall(PetscViewerASCIIPushTab(viewer));
      for (PetscInt t = 0; t < params_pressure.num_times; t++) {
        PetscCall(PetscViewerASCIIPrintf(viewer, "t = %.3f: %f\n", params_pressure.times[t], params_pressure.pressure[t]));
      }
      PetscCall(PetscViewerASCIIPopTab(viewer));
    }
    PetscCall(PetscViewerASCIIPopTab(viewer));
  }
  if (ratel->bc_platen_count) {
    PetscCall(PetscViewerASCIIPushTab(viewer));
    PetscCall(PetscViewerASCIIPrintf(viewer, "Platen (contact) boundaries:\n"));
    for (PetscInt i = 0; i < ratel->bc_platen_count; i++) {
      char                      cl_prefix[PETSC_MAX_OPTION_NAME];
      const char               *face_name = ratel->bc_platen_names[i];
      RatelBCPlatenParamsCommon params_platen;

      PetscCall(PetscSNPrintf(cl_prefix, sizeof(cl_prefix), "bc_platen_%s_", face_name));
      PetscCall(RatelBoundaryPlatenParamsCommonFromOptions(ratel, cl_prefix, &params_platen));
      params_platen.face_id    = ratel->bc_platen_label_values[i];
      params_platen.name_index = i;

      PetscCall(PetscViewerASCIIPushTab(viewer));
      PetscCall(RatelBoundaryPlatenParamsCommonView(ratel, &params_platen, viewer));
      PetscCall(PetscViewerASCIIPopTab(viewer));
    }
    PetscCall(PetscViewerASCIIPopTab(viewer));
  }
  PetscCall(PetscViewerASCIIPopTab(viewer));  // Boundary Conditions

  PetscCall(PetscViewerASCIIPrintf(viewer, "Initial condition:\n"));
  PetscCall(PetscViewerASCIIPushTab(viewer));
  PetscCall(PetscViewerASCIIPrintf(viewer, "Type: %s\n", RatelInitialConditionTypes[ratel->initial_condition_type]));
  switch (ratel->initial_condition_type) {
    case RATEL_INITIAL_CONDITION_ZERO:
      break;
    case RATEL_INITIAL_CONDITION_CONTINUE: {
      RatelCheckpointData checkpoint_data;
      PetscCall(RatelCheckpointDataRead(ratel, ratel->continue_file_name, &checkpoint_data));
      PetscCall(PetscViewerASCIIPrintf(viewer, "Filename: %s\n", ratel->continue_file_name));
      PetscCall(PetscViewerASCIIPrintf(viewer, "Checkpoint version: %X\n", (uint32_t)checkpoint_data->version));
      PetscCall(PetscViewerASCIIPrintf(viewer, "Timestep: %" PetscInt_FMT "\n", checkpoint_data->step));
      PetscCall(PetscViewerASCIIPrintf(viewer, "Time: %g\n", checkpoint_data->time));
      PetscCall(RatelCheckpointDataDestroy(&checkpoint_data));
    }
  }
  PetscCall(PetscViewerASCIIPopTab(viewer));  // Initial condition

  {
    for (PetscInt i = 0; i < ratel->num_active_fields; i++) {
      PetscCall(PetscViewerASCIIPrintf(viewer, "Finite elements, field %" PetscInt_FMT ":\n", i));
      PetscCall(PetscViewerASCIIPushTab(viewer));
      PetscCall(PetscViewerASCIIPrintf(viewer, "Polynomial order: %" PetscInt_FMT "\n", ratel->fine_grid_orders[i]));
      PetscCall(PetscViewerASCIIPrintf(viewer, "Additional quadrature points: %" PetscInt_FMT "\n", ratel->q_extra));
      if (ratel->dm_solution) {
        // LCOV_EXCL_START
        PetscBool      is_tensor = PETSC_FALSE;
        PetscInt       depth = 0, first_point = 0;
        PetscInt       ids[1] = {0};
        DMLabel        depth_label;
        DMPolytopeType cell_type;
        DM             dm_solution;
        PetscFE        fe;
        PetscDualSpace dual_space;

        PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));
        PetscCall(DMGetField(dm_solution, i, NULL, (PetscObject *)&fe));
        PetscCall(PetscFEGetDualSpace(fe, &dual_space));
        PetscCall(PetscDualSpaceLagrangeGetTensor(dual_space, &is_tensor));
        PetscCall(PetscViewerASCIIPrintf(viewer, "Basis application: %s\n", is_tensor ? "tensor product" : "non-tensor product"));

        PetscCall(DMPlexGetDepth(dm_solution, &depth));
        PetscCall(DMPlexGetDepthLabel(dm_solution, &depth_label));
        ids[0] = depth;
        PetscCall(DMGetFirstLabeledPoint(dm_solution, dm_solution, depth_label, 1, ids, 0, &first_point, NULL));
        if (first_point >= 0) {
          PetscCall(DMPlexGetCellType(dm_solution, first_point, &cell_type));
          PetscCall(PetscViewerASCIIPrintf(viewer, "Element topology: %s\n", CeedElemTopologies[PolytopeTypePetscToCeed(cell_type)]));
        }
        PetscCall(DMDestroy(&dm_solution));
        // LCOV_EXCL_STOP
      }
      PetscCall(PetscViewerASCIIPopTab(viewer));
    }
  }
  {
    PetscBool is_diagnostic_geometry_ratel_decide = ratel->diagnostic_geometry_order == RATEL_DECIDE;
    char      diagnostic_geometry_order_string[PETSC_MAX_PATH_LEN];

    if (is_diagnostic_geometry_ratel_decide) {
      PetscCall(PetscSNPrintf(diagnostic_geometry_order_string, sizeof(diagnostic_geometry_order_string), "%s", "use solution mesh geometric order"));
    } else {
      PetscCall(PetscSNPrintf(diagnostic_geometry_order_string, sizeof(diagnostic_geometry_order_string), "%" PetscInt_FMT,
                              ratel->diagnostic_geometry_order));
    }
    PetscCall(PetscViewerASCIIPrintf(viewer, "Geometry order for diagnostic values mesh: %s\n", diagnostic_geometry_order_string));
  }
  PetscCall(PetscViewerASCIIPrintf(viewer, "Has manufactured solution: %s\n", ratel->has_mms ? "yes" : "no"));
  if (ratel->expected_strain_energy == 0.0) {
    // LCOV_EXCL_START
    PetscCall(PetscViewerASCIIPrintf(viewer, "Expected strain energy: not set\n"));
    // LCOV_EXCL_STOP
  } else {
    PetscCall(PetscViewerASCIIPrintf(viewer, "Expected strain energy: %0.12e\n", ratel->expected_strain_energy));
  }
  PetscCall(PetscViewerASCIIPopTab(viewer));  // Ratel Context

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel View Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Destroy a `Ratel` context.

  Collective across MPI processes.

  @param[in,out]  ratel  `Ratel` context object to destroy

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDestroy(Ratel *ratel) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(*ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Destroy"));

  // View before destroying, if requested
  {
    PetscViewer viewer     = NULL;
    PetscBool   view_ratel = PETSC_FALSE;

    PetscCall(PetscOptionsCreateViewer((*ratel)->comm, NULL, NULL, "-ratel_view", &viewer, NULL, &view_ratel));
    if (view_ratel) PetscCall(RatelView(*ratel, viewer));
    if (viewer != PETSC_VIEWER_STDOUT_((*ratel)->comm)) PetscCall(PetscViewerDestroy(&viewer));
  }

  // DMs
  if ((*ratel)->method_type == RATEL_METHOD_MPM && (*ratel)->dm_solution) {
    DM dm_mesh;

    PetscCall(DMSwarmGetCellDM((*ratel)->dm_solution, &dm_mesh));
    PetscCall(DMDestroy(&dm_mesh));
  }
  PetscCall(DMDestroy(&(*ratel)->dm_solution));
  PetscCall(DMDestroy(&(*ratel)->dm_diagnostic));
  for (PetscInt i = 0; i < (*ratel)->num_sub_dms_diagnostic; i++) {
    PetscCall(ISDestroy(&(*ratel)->is_sub_dms_diagnostic[i]));
    PetscCall(DMDestroy(&(*ratel)->sub_dms_diagnostic[i]));
  }
  PetscCall(PetscFree((*ratel)->is_sub_dms_diagnostic));
  PetscCall(PetscFree((*ratel)->sub_dms_diagnostic));

  // Point field vectors
  if ((*ratel)->point_fields) {
    for (PetscInt i = 0; i < (*ratel)->num_point_fields; i++) {
      RatelCallCeed((*ratel), CeedVectorDestroy(&(*ratel)->point_fields[i]));
    }
    PetscCall(PetscFree((*ratel)->point_fields));
  }

  // KSP
  PetscCall(KSPDestroy(&(*ratel)->ksp_diagnostic_projection));

  // Mats
  PetscCall(MatDestroy(&(*ratel)->mat_jacobian));
  PetscCall(MatDestroy(&(*ratel)->mat_jacobian_pre));

  // Evaluators
  PetscCall(CeedBoundaryEvaluatorDestroy(&(*ratel)->boundary_evaluator_u));
  PetscCall(CeedBoundaryEvaluatorDestroy(&(*ratel)->boundary_evaluator_visualization));
  PetscCall(CeedEvaluatorDestroy(&(*ratel)->evaluator_residual_u));
  PetscCall(CeedEvaluatorDestroy(&(*ratel)->evaluator_residual_ut));
  PetscCall(CeedEvaluatorDestroy(&(*ratel)->evaluator_residual_utt));
  PetscCall(CeedEvaluatorDestroy(&(*ratel)->evaluator_strain_energy));
  PetscCall(CeedEvaluatorDestroy(&(*ratel)->evaluator_external_energy));
  PetscCall(CeedEvaluatorDestroy(&(*ratel)->evaluator_projected_diagnostic));
  PetscCall(CeedEvaluatorDestroy(&(*ratel)->evaluator_dual_diagnostic));
  PetscCall(CeedEvaluatorDestroy(&(*ratel)->evaluator_dual_nodal_scale));
  PetscCall(CeedEvaluatorDestroy(&(*ratel)->evaluator_surface_force));
  PetscCall(CeedEvaluatorDestroy(&(*ratel)->evaluator_mms_error));
  for (PetscInt i = 0; i < (*ratel)->surface_force_face_count; i++) {
    PetscCall(CeedEvaluatorDestroy(&(*ratel)->evaluators_surface_displacement[i]));
    PetscCall(CeedEvaluatorDestroy(&(*ratel)->evaluators_surface_force_face[i]));
    PetscCall(CeedEvaluatorDestroy(&(*ratel)->evaluators_surface_force_cell_to_face[i]));
  }

  // Materials
  PetscCall(PetscFunctionListDestroy(&(*ratel)->material_create_functions));
  for (PetscInt i = 0; i < ((*ratel)->num_materials); i++) PetscCall(RatelMaterialDestroy(&(*ratel)->materials[i]));
  PetscCall(PetscFree((*ratel)->materials));
  PetscCall(PetscFree((*ratel)->material_volume_label_name));
  PetscCall(PetscFree((*ratel)->material_units));
  PetscCall(PetscFree((*ratel)->jacobian_multiplicity_skip_indices));

  // Platen BCs
  for (PetscInt i = 0; i < (*ratel)->bc_platen_count; i++) PetscCall(PetscFree((*ratel)->bc_platen_names[i]));

  // Surface force names
  for (PetscInt i = 0; i < (*ratel)->surface_force_face_count; i++) PetscCall(PetscFree((*ratel)->surface_force_face_names[i]));

  // Output options
  PetscCall(PetscFree((*ratel)->continue_file_name));

  // Ceed
  PetscCall(PetscFree((*ratel)->ceed_resource));
  PetscCheck(CeedDestroy(&(*ratel)->ceed) == CEED_ERROR_SUCCESS, (*ratel)->comm, PETSC_ERR_LIB, "Destroying Ceed object failed");

  // Ratel itself
  PetscCall(PetscFree(*ratel));

  PetscCall(RatelDebugEnv256(RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Destroy Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Create `DM` with `SNES` or `TS` hooks from `Ratel` context.

  Collective across MPI processes.

  @param[in]   ratel        `Ratel` context
  @param[in]   solver_type  Solver type to use
  @param[out]  dm           `DMPlex` object with `SNES` or `TS` hooks

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDMCreate(Ratel ratel, RatelSolverType solver_type, DM *dm) {
  PetscLogStage stage_dm_setup;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DM Create"));

  PetscCall(PetscLogStageGetId("Ratel DM Setup", &stage_dm_setup));
  if (stage_dm_setup == -1) PetscCall(PetscLogStageRegister("Ratel DM Setup", &stage_dm_setup));
  PetscCall(PetscLogStagePush(stage_dm_setup));

  // Numerical method specific setup
  switch (ratel->method_type) {
    case RATEL_METHOD_FEM:
      PetscCall(RatelDebug(ratel, "---- Method type: FEM"));
      PetscCall(RatelGetSolutionMeshDM(ratel, dm));
      break;
    case RATEL_METHOD_MPM:
      PetscCheck(solver_type != RATEL_SOLVER_DYNAMIC, ratel->comm, PETSC_ERR_SUP, "Dynamic MPM not supported");
      PetscCall(RatelDebug(ratel, "---- Model type: MPM"));
      PetscCall(RatelGetSolutionMeshDM(ratel, dm));
      PetscCall(DMDestroy(dm));
      *dm = ratel->dm_solution;
      PetscCall(PetscObjectReference((PetscObject)(*dm)));
      break;
  }
  // Solver specific setup
  ratel->solver_type = solver_type;
  PetscCall(RatelDMSetupSolver(ratel));

  PetscCall(PetscLogStagePop());
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DM Create Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Set the `Ratel` context to not assume an SPD Jacobian.

  Not collective across MPI processes.

  @param[in]  ratel  `Ratel` context object

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSetNonSPD(Ratel ratel) {
  PetscFunctionBeginUser;
  ratel->is_spd = PETSC_FALSE;
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}
