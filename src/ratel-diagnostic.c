/// @file
/// Implementation of Ratel diagnostic data interfaces

#include <ceed-evaluator.h>
#include <ceed.h>
#include <mat-ceed.h>
#include <math.h>
#include <petsc-ceed-utils.h>
#include <petscdmplex.h>
#include <petscksp.h>
#include <ratel-boundary.h>
#include <ratel-diagnostic.h>
#include <ratel-dm.h>
#include <ratel-fem.h>
#include <ratel-impl.h>
#include <ratel-material.h>
#include <ratel.h>
#include <ratel/qfunctions/boundaries/dirichlet.h>
#include <ratel/qfunctions/geometry/bounding-box.h>
#include <stddef.h>
#include <stdio.h>

/// @addtogroup RatelInternal
/// @{

/**
  @brief Setup `CeedEvaluator` computing strain energy.

  Collective across MPI processes.

  @param[in]   ratel                    `Ratel` context
  @param[out]  evaluator_strain_energy  Computed strain energy evaluator

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSetupStrainEnergyEvaluator(Ratel ratel, CeedEvaluator *evaluator_strain_energy) {
  DM           dm_energy, dm_solution;
  CeedOperator op_strain_energy;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Setup Strain Energy Operator"));

  // DM
  PetscCall(RatelDebug(ratel, "---- Energy DM"));
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));
  PetscCall(RatelGetEnergyDM(ratel, &dm_energy));

  // libCEED operators
  PetscCall(RatelDebug(ratel, "---- Energy operator"));
  RatelCallCeed(ratel, CeedCompositeOperatorCreate(ratel->ceed, &op_strain_energy));
  RatelCallCeed(ratel, CeedOperatorSetName(op_strain_energy, "strain energy"));
  for (PetscInt i = 0; i < ratel->num_materials; i++) {
    PetscCall(RatelMaterialSetupStrainEnergySuboperator(ratel->materials[i], dm_energy, op_strain_energy));
  }

  // Evaluator
  PetscCall(CeedEvaluatorCreateWithSameInputs(ratel->evaluator_residual_u, dm_energy, op_strain_energy, evaluator_strain_energy));
  PetscCall(CeedEvaluatorSetLogEvent(*evaluator_strain_energy, RATEL_Diagnostics));
  PetscCall(CeedEvaluatorSetCeedOperatorLogEvent(*evaluator_strain_energy, RATEL_Diagnostics_CeedOp));

  // Debugging
  PetscCall(RatelDebug(ratel, "---- Final strain energy evaluator"));
  if (ratel->is_debug) PetscCall(CeedEvaluatorView(*evaluator_strain_energy, PETSC_VIEWER_STDOUT_WORLD));

  // Cleanup
  PetscCall(DMDestroy(&dm_solution));
  PetscCall(DMDestroy(&dm_energy));
  RatelCallCeed(ratel, CeedOperatorDestroy(&op_strain_energy));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Setup Strain Energy Operator Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup `CeedEvaluator` computing external energy.

  Collective across MPI processes.

  @param[in]   ratel                      `Ratel` context
  @param[out]  evaluator_external_energy  Computed external energy evaluator

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSetupExternalEnergyEvaluator(Ratel ratel, CeedEvaluator *evaluator_external_energy) {
  DM           dm_energy, dm_solution;
  CeedOperator op_external_energy;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Setup External Energy Operator"));

  // DM
  PetscCall(RatelDebug(ratel, "---- Energy DM"));
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));
  PetscCall(RatelGetEnergyDM(ratel, &dm_energy));

  // libCEED operators
  PetscCall(RatelDebug(ratel, "---- Energy operator"));
  RatelCallCeed(ratel, CeedCompositeOperatorCreate(ratel->ceed, &op_external_energy));
  RatelCallCeed(ratel, CeedOperatorSetName(op_external_energy, "external energy"));
  // -- Energy of body force
  for (PetscInt i = 0; i < ratel->num_materials; i++) {
    PetscCall(RatelMaterialSetupForcingEnergySuboperator(ratel->materials[i], dm_energy, op_external_energy));
  }
  // -- Energy of traction boundary
  PetscCall(RatelSetupTractionEnergySuboperator(ratel, dm_energy, op_external_energy));

  // Evaluator
  PetscCall(CeedEvaluatorCreateWithSameInputs(ratel->evaluator_residual_u, dm_energy, op_external_energy, evaluator_external_energy));
  PetscCall(CeedEvaluatorSetLogEvent(*evaluator_external_energy, RATEL_Diagnostics));

  // Debugging information
  PetscCall(RatelDebug(ratel, "---- Final external energy operator"));
  if (ratel->is_debug) PetscCall(CeedEvaluatorView(*evaluator_external_energy, PETSC_VIEWER_STDOUT_WORLD));

  // Cleanup
  PetscCall(DMDestroy(&dm_solution));
  PetscCall(DMDestroy(&dm_energy));
  RatelCallCeed(ratel, CeedOperatorDestroy(&op_external_energy));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Setup External Energy Operator Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

#define RATEL_DIAGNOSTIC_PROJECTION_SCALAR_FIELD_NAME "scalar_field"
#define RATEL_DIAGNOSTIC_PROJECTION_SCALAR_LABEL_NAME "scalar_field_label"
#define RATEL_DIAGNOSTIC_PROJECTION_SCALAR_LABEL_VALUE 42
#define RATEL_DIAGNOSTIC_PROJECTION_SCALAR_LABEL_VALUE_STRING "42"
#define RATEL_DIAGNOSTIC_PROJECTION_SCALAR_OPTION_NAME "diagnostic_projection_scalar_mass_matrix"
#define RATEL_DIAGNOSTIC_PROJECTION_FULL_DIAGONAL_NAME "diagnostic_projection_full_diagonal"
/**
  @brief Setup `CeedEvaluator` computing diagnostic values.

  Collective across MPI processes.

  @param[in]   ratel                           `Ratel` context
  @param[out]  ksp_diagnostic_projection       `KSP` for solving projection
  @param[out]  evaluator_projected_diagnostic  Diagnostic quantity projected values evaluator
  @param[out]  evaluator_dual_diagnostic       Diagnostic quantity dual space values evaluator
  @param[out]  evaluator_dual_nodal_scale      Diagnostic quantity dual space nodal scaling evaluator

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSetupDiagnosticEvaluators(Ratel ratel, KSP *ksp_diagnostic_projection, CeedEvaluator *evaluator_projected_diagnostic,
                                              CeedEvaluator *evaluator_dual_diagnostic, CeedEvaluator *evaluator_dual_nodal_scale) {
  DM           dm_solution;
  Mat          mat_diagnostic_projection;
  CeedOperator op_mass_diagnostic, op_projected_diagnostic, op_dual_diagnostic, op_dual_nodal_scale;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Setup Diagnostic Operators"));

  // DM
  PetscCall(RatelDebug(ratel, "---- Diagnostic D"));
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));
  if (!ratel->dm_diagnostic) {
    switch (ratel->method_type) {
      case RATEL_METHOD_FEM:
      case RATEL_METHOD_MPM:  // Use FEM DM for MPM
        PetscCall(RatelDiagnosticDMsSetup_FEM(ratel, &ratel->dm_diagnostic, &ratel->num_sub_dms_diagnostic, &ratel->is_sub_dms_diagnostic,
                                              &ratel->sub_dms_diagnostic));
        break;
    }
  }

  // libCEED operators
  RatelCallCeed(ratel, CeedCompositeOperatorCreate(ratel->ceed, &op_projected_diagnostic));
  RatelCallCeed(ratel, CeedOperatorSetName(op_projected_diagnostic, "projected diagnostic values"));
  RatelCallCeed(ratel, CeedCompositeOperatorCreate(ratel->ceed, &op_dual_diagnostic));
  RatelCallCeed(ratel, CeedOperatorSetName(op_dual_diagnostic, "dual diagnostic values"));
  RatelCallCeed(ratel, CeedCompositeOperatorCreate(ratel->ceed, &op_mass_diagnostic));
  RatelCallCeed(ratel, CeedOperatorSetName(op_mass_diagnostic, "mass operator for diagnostic value projection"));
  RatelCallCeed(ratel, CeedCompositeOperatorCreate(ratel->ceed, &op_dual_nodal_scale));
  RatelCallCeed(ratel, CeedOperatorSetName(op_dual_nodal_scale, "nodal scaling operator operator for dual diagnostic values"));

  PetscCall(RatelDebug(ratel, "---- Diagnostic operators"));
  for (PetscInt i = 0; i < ratel->num_materials; i++) {
    PetscCall(RatelMaterialSetupDiagnosticSuboperators(ratel->materials[i], ratel->sub_dms_diagnostic[RATEL_DIAGNOSTIC_PROJECTED],
                                                       ratel->sub_dms_diagnostic[RATEL_DIAGNOSTIC_DUAL], op_mass_diagnostic, op_projected_diagnostic,
                                                       op_dual_diagnostic, op_dual_nodal_scale));
  }

  {
    CeedInt       num_sub;
    CeedOperator *sub_ops_dual_scale;

    RatelCallCeed(ratel, CeedCompositeOperatorGetNumSub(op_dual_nodal_scale, &num_sub));
    RatelCallCeed(ratel, CeedCompositeOperatorGetSubList(op_dual_nodal_scale, &sub_ops_dual_scale));

    PetscCall(RatelDebug(ratel, "---- Set nodal multiplicity vector"));
    for (PetscInt i = 0; i < num_sub; i++) {
      CeedOperatorField field_nodal_multiplicity;
      CeedVector        multiplicity_vector;

      RatelCallCeed(ratel, CeedOperatorGetFieldByName(sub_ops_dual_scale[i], "nodal_multiplicity", &field_nodal_multiplicity));
      RatelCallCeed(ratel, CeedOperatorFieldGetVector(field_nodal_multiplicity, &multiplicity_vector));
      RatelCallCeed(ratel, CeedCompositeOperatorGetMultiplicity(op_dual_nodal_scale, 0, NULL, multiplicity_vector));
      RatelCallCeed(ratel, CeedVectorDestroy(&multiplicity_vector));
    }
  }

  // Evaluators
  PetscCall(CeedEvaluatorCreateWithSameInputs(ratel->evaluator_residual_u, ratel->sub_dms_diagnostic[RATEL_DIAGNOSTIC_PROJECTED],
                                              op_projected_diagnostic, evaluator_projected_diagnostic));
  PetscCall(CeedEvaluatorSetLogEvent(*evaluator_projected_diagnostic, RATEL_Diagnostics));
  PetscCall(CeedEvaluatorSetCeedOperatorLogEvent(*evaluator_projected_diagnostic, RATEL_Diagnostics_CeedOp));
  PetscCall(CeedEvaluatorCreateWithSameInputs(ratel->evaluator_residual_u, ratel->sub_dms_diagnostic[RATEL_DIAGNOSTIC_DUAL], op_dual_diagnostic,
                                              evaluator_dual_diagnostic));
  PetscCall(CeedEvaluatorSetLogEvent(*evaluator_dual_diagnostic, RATEL_Diagnostics));
  PetscCall(CeedEvaluatorSetCeedOperatorLogEvent(*evaluator_dual_diagnostic, RATEL_Diagnostics_CeedOp));
  PetscCall(CeedEvaluatorCreate(ratel->sub_dms_diagnostic[RATEL_DIAGNOSTIC_DUAL], ratel->sub_dms_diagnostic[RATEL_DIAGNOSTIC_DUAL],
                                op_dual_nodal_scale, evaluator_dual_nodal_scale));
  PetscCall(CeedEvaluatorSetLogEvent(*evaluator_dual_nodal_scale, RATEL_Diagnostics));
  PetscCall(CeedEvaluatorSetCeedOperatorLogEvent(*evaluator_dual_nodal_scale, RATEL_Diagnostics_CeedOp));

  // Diagnostic projection MatCEED
  PetscCall(RatelDebug(ratel, "---- Setting up diagnostic projection context"));
  PetscCall(MatCreateCeed(ratel->sub_dms_diagnostic[RATEL_DIAGNOSTIC_PROJECTED], ratel->sub_dms_diagnostic[RATEL_DIAGNOSTIC_PROJECTED],
                          op_mass_diagnostic, NULL, &mat_diagnostic_projection));
  PetscCall(MatCeedSetLogEvents(mat_diagnostic_projection, RATEL_Diagnostics, RATEL_Diagnostics));
  PetscCall(MatCeedSetCeedOperatorLogEvents(mat_diagnostic_projection, RATEL_Diagnostics_CeedOp, RATEL_Diagnostics_CeedOp));
  PetscCall(PetscObjectSetName((PetscObject)mat_diagnostic_projection, "diagnostic projection"));

  // Diagnostic projection KSP
  PetscCall(RatelDebug(ratel, "---- Setting up diagnostic projection KSP"));
  {
    KSP ksp;
    Mat mat_pc;
    PC  pc;

    // -- Projection PC Mat
    {
      CeedInt num_components_projected;
      Vec     full_diagonal, scalar_diagonal, scalar_diagonal_local;
      DM      dm_diagnostic_scalar;

      PetscCall(DMClone(ratel->sub_dms_diagnostic[RATEL_DIAGNOSTIC_PROJECTED], &dm_diagnostic_scalar));
      PetscCall(DMSetMatrixPreallocateSkip(dm_diagnostic_scalar, PETSC_TRUE));
      {
        const char    *field_name     = RATEL_DIAGNOSTIC_PROJECTION_SCALAR_FIELD_NAME;
        const char    *component_name = RATEL_DIAGNOSTIC_PROJECTION_SCALAR_FIELD_NAME;
        const char    *label_name     = RATEL_DIAGNOSTIC_PROJECTION_SCALAR_LABEL_NAME;
        const PetscInt label_value    = RATEL_DIAGNOSTIC_PROJECTION_SCALAR_LABEL_VALUE;
        PetscInt       start = 0, stop = 0;
        CeedInt        num_comp = 1;
        DMLabel        scalar_label;

        PetscCall(RatelDMSetupByOrder_FEM(ratel, PETSC_FALSE, PETSC_FALSE, &ratel->diagnostic_order, ratel->diagnostic_geometry_order, PETSC_TRUE,
                                          RATEL_DECIDE, 1, &num_comp, &field_name, &component_name, dm_diagnostic_scalar));
        PetscCall(DMLabelCreate(ratel->comm, label_name, &scalar_label));
        PetscCall(DMPlexGetHeightStratum(dm_diagnostic_scalar, 0, &start, &stop));
        for (PetscInt i = start; i < stop; i++) PetscCall(DMLabelSetValue(scalar_label, i, label_value));
        PetscCall(DMLabelDestroy(&scalar_label));
      }

      // ---- Work vectors
      PetscCall(DMGetNamedGlobalVector(ratel->sub_dms_diagnostic[RATEL_DIAGNOSTIC_PROJECTED], RATEL_DIAGNOSTIC_PROJECTION_FULL_DIAGONAL_NAME,
                                       &full_diagonal));
      PetscCall(VecZeroEntries(full_diagonal));
      PetscCall(RatelMaterialGetNumDiagnosticComponents(ratel->materials[0], &num_components_projected, NULL));
      PetscCall(VecSetBlockSize(full_diagonal, num_components_projected));
      PetscCall(DMGetGlobalVector(dm_diagnostic_scalar, &scalar_diagonal));
      PetscCall(DMGetLocalVector(dm_diagnostic_scalar, &scalar_diagonal_local));

      // ---- CeedOperator
      {
        CeedOperator op_scalar_residual, op_scalar_jacobian;

        RatelCallCeed(ratel, CeedCompositeOperatorCreate(ratel->ceed, &op_scalar_residual));
        RatelCallCeed(ratel, CeedCompositeOperatorCreate(ratel->ceed, &op_scalar_jacobian));
        // ------ Scalar mass matrix dummy operator on full mesh
        {
          const char *material_volume_label_name = ratel->material_volume_label_name;
          const char *scalar_name                = RATEL_DIAGNOSTIC_PROJECTION_SCALAR_OPTION_NAME;
          // clang-format off
          const char *scalar_args =
              " -" RATEL_DIAGNOSTIC_PROJECTION_SCALAR_OPTION_NAME "_model         ceed-bp1"
              " -" RATEL_DIAGNOSTIC_PROJECTION_SCALAR_OPTION_NAME "_label_name  " RATEL_DIAGNOSTIC_PROJECTION_SCALAR_LABEL_NAME
              " -" RATEL_DIAGNOSTIC_PROJECTION_SCALAR_OPTION_NAME "_label_value " RATEL_DIAGNOSTIC_PROJECTION_SCALAR_LABEL_VALUE_STRING;
          // clang-format on
          RatelMaterial   scalar_material;
          RatelMethodType method_type = ratel->method_type;
          RatelSolverType solver_type = ratel->solver_type;
          DM              dm_solution = ratel->dm_solution;

          // ------ Hack will be immediately reset
          ratel->material_volume_label_name = (char *)RATEL_DIAGNOSTIC_PROJECTION_SCALAR_LABEL_NAME;
          ratel->method_type                = RATEL_METHOD_FEM;
          ratel->solver_type                = RATEL_SOLVER_STATIC;
          ratel->dm_solution                = dm_diagnostic_scalar;

          // ------ Setup global dummy material
          PetscCall(PetscOptionsInsertString(NULL, scalar_args));
          PetscCall(RatelMaterialCreate(ratel, scalar_name, &scalar_material));

          // ------ Setup global dummy operator
          PetscCall(RatelMaterialSetupResidualSuboperators(scalar_material, op_scalar_residual, NULL, NULL));
          // Note: want Jacobian because residual may have forcing term
          PetscCall(RatelMaterialSetupJacobianSuboperator(scalar_material, op_scalar_residual, NULL, op_scalar_jacobian));

          // ------ Reset hack
          ratel->material_volume_label_name = (char *)material_volume_label_name;
          ratel->method_type                = method_type;
          ratel->solver_type                = solver_type;
          ratel->dm_solution                = dm_solution;

          // ------ Cleanup
          PetscCall(RatelMaterialDestroy(&scalar_material));
        }
        // ------ Get scalar mass matrix diagonal
        {
          PetscMemType mem_type;
          PetscInt     len;
          CeedVector   diagonal_local_ceed;

          PetscCall(VecGetSize(scalar_diagonal_local, &len));
          RatelCallCeed(ratel, CeedVectorCreate(ratel->ceed, len, &diagonal_local_ceed));
          PetscCall(VecPetscToCeed(scalar_diagonal_local, &mem_type, diagonal_local_ceed));
          RatelCallCeed(ratel, CeedOperatorLinearAssembleDiagonal(op_scalar_jacobian, diagonal_local_ceed, CEED_REQUEST_IMMEDIATE));
          PetscCall(VecCeedToPetsc(diagonal_local_ceed, mem_type, scalar_diagonal_local));
          RatelCallCeed(ratel, CeedVectorDestroy(&diagonal_local_ceed));
        }
        // ------ Cleanup
        RatelCallCeed(ratel, CeedOperatorDestroy(&op_scalar_residual));
        RatelCallCeed(ratel, CeedOperatorDestroy(&op_scalar_jacobian));
      }

      // ---- Build the global diagonal
      PetscCall(VecZeroEntries(scalar_diagonal));
      PetscCall(DMLocalToGlobal(dm_diagnostic_scalar, scalar_diagonal_local, ADD_VALUES, scalar_diagonal));
      for (CeedInt i = 0; i < num_components_projected; i++) {
        PetscCall(VecStrideScatter(scalar_diagonal, i, full_diagonal, INSERT_VALUES));
      }

      // ---- Finally the mat
      PetscCall(MatCreateDiagonal(full_diagonal, &mat_pc));

      // ---- Cleanup
      PetscCall(DMRestoreNamedGlobalVector(ratel->sub_dms_diagnostic[RATEL_DIAGNOSTIC_PROJECTED], RATEL_DIAGNOSTIC_PROJECTION_FULL_DIAGONAL_NAME,
                                           &full_diagonal));
      PetscCall(DMRestoreGlobalVector(dm_diagnostic_scalar, &scalar_diagonal));
      PetscCall(DMRestoreLocalVector(dm_diagnostic_scalar, &scalar_diagonal_local));
      PetscCall(DMDestroy(&dm_diagnostic_scalar));
    }

    // -- Projection KSP
    PetscCall(KSPCreate(ratel->comm, &ksp));
    PetscCall(KSPSetOptionsPrefix(ksp, "diagnostic_projection_"));
    PetscCall(KSPSetType(ksp, KSPCG));
    PetscCall(KSPSetOperators(ksp, mat_diagnostic_projection, mat_pc));
    PetscCall(KSPSetTolerances(ksp, 1e-04, PETSC_DEFAULT, PETSC_DEFAULT, PETSC_DEFAULT));
    PetscCall(KSPGetPC(ksp, &pc));
    PetscCall(PCSetType(pc, PCJACOBI));
    PetscCall(KSPSetFromOptions(ksp));
    *ksp_diagnostic_projection = ksp;

    // -- Cleanup
    PetscCall(MatDestroy(&mat_pc));
    PetscCall(MatDestroy(&mat_diagnostic_projection));
  }

  // Debugging
  PetscCall(RatelDebug(ratel, "---- Final projected diagnostic evaluator"));
  if (ratel->is_debug) PetscCall(CeedEvaluatorView(*evaluator_projected_diagnostic, PETSC_VIEWER_STDOUT_WORLD));
  PetscCall(RatelDebug(ratel, "---- Final dual diagnostic evaluator"));
  if (ratel->is_debug) PetscCall(CeedEvaluatorView(*evaluator_dual_diagnostic, PETSC_VIEWER_STDOUT_WORLD));
  PetscCall(RatelDebug(ratel, "---- Final diagnostic projection KSP"));
  if (ratel->is_debug) PetscCall(KSPView(*ksp_diagnostic_projection, PETSC_VIEWER_STDOUT_WORLD));

  // Cleanup
  PetscCall(DMDestroy(&dm_solution));
  RatelCallCeed(ratel, CeedOperatorDestroy(&op_projected_diagnostic));
  RatelCallCeed(ratel, CeedOperatorDestroy(&op_dual_diagnostic));
  RatelCallCeed(ratel, CeedOperatorDestroy(&op_mass_diagnostic));
  RatelCallCeed(ratel, CeedOperatorDestroy(&op_dual_nodal_scale));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Setup Diagnostic Operators Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}
#undef RATEL_DIAGNOSTIC_PROJECTION_SCALAR_FIELD_NAME
#undef RATEL_DIAGNOSTIC_PROJECTION_SCALAR_LABEL_NAME
#undef RATEL_DIAGNOSTIC_PROJECTION_SCALAR_LABEL_VALUE
#undef RATEL_DIAGNOSTIC_PROJECTION_SCALAR_OPTION_NAME

/**
  @brief Setup context computing MMS error.

  Collective across MPI processes.

  @param[in]   ratel                `Ratel` context
  @param[out]  evaluator_mms_error  MMS error computation evaluator

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSetupMMSErrorEvaluator(Ratel ratel, CeedEvaluator *evaluator_mms_error) {
  DM           dm_solution;
  CeedOperator op_mms_error;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Setup MMS Error Operator"));

  // libCEED operators
  RatelCallCeed(ratel, CeedCompositeOperatorCreate(ratel->ceed, &op_mms_error));
  RatelCallCeed(ratel, CeedOperatorSetName(op_mms_error, "MMS error"));

  PetscCall(RatelDebug(ratel, "---- MMS Error operator"));
  for (PetscInt i = 0; i < ratel->num_materials; i++) {
    PetscCall(RatelMaterialSetupMMSErrorSuboperator(ratel->materials[i], op_mms_error));
  }

  // DM
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));

  // Evaluator
  PetscCall(CeedEvaluatorCreateWithSameInputs(ratel->evaluator_residual_u, dm_solution, op_mms_error, evaluator_mms_error));
  PetscCall(CeedEvaluatorSetLogEvent(*evaluator_mms_error, RATEL_Diagnostics));
  PetscCall(CeedEvaluatorSetCeedOperatorLogEvent(*evaluator_mms_error, RATEL_Diagnostics_CeedOp));

  // Debugging
  if (ratel->is_debug) {
    // LCOV_EXCL_START
    PetscCall(RatelDebug(ratel, "---- Final MMS error evaluator"));
    PetscCall(CeedEvaluatorView(*evaluator_mms_error, PETSC_VIEWER_STDOUT_WORLD));
    // LCOV_EXCL_STOP
  }

  // Cleanup
  PetscCall(DMDestroy(&dm_solution));
  RatelCallCeed(ratel, CeedOperatorDestroy(&op_mms_error));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Setup MMS Error Operator Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup `CeedEvaluator` computing average surface displacement.

  Collective across MPI processes.

  @param[in]   ratel                            `Ratel` context
  @param[out]  evaluators_surface_displacement  Average surface displacement computation evaluators

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSetupSurfaceDisplacementEvaluators(Ratel ratel, CeedEvaluator evaluators_surface_displacement[]) {
  DM           dm_solution, dm_coord;
  CeedOperator ops_surface_displacement[RATEL_MAX_BOUNDARY_FACES] = {NULL};

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Setup Average Surface Displacement Operators"));

  for (PetscInt i = 0; i < ratel->surface_force_face_count; i++) {
    char operator_name[1024];

    // Create operator
    RatelCallCeed(ratel, CeedCompositeOperatorCreate(ratel->ceed, &ops_surface_displacement[i]));
    PetscCall(PetscSNPrintf(operator_name, sizeof(operator_name), "average surface displacement on face %s", ratel->surface_force_face_names[i]));
    RatelCallCeed(ratel, CeedOperatorSetName(ops_surface_displacement[i], operator_name));
  }

  // DMs
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));
  PetscCall(DMGetCoordinateDM(dm_solution, &dm_coord));

  // Surface force centroids
  PetscCall(RatelDebug(ratel, "---- Initial surface centroids"));
  PetscCall(RatelSetupSurfaceForceCentroids(ratel, dm_solution));

  PetscCall(RatelDebug(ratel, "---- Average surface displacement operators"));
  PetscCall(RatelBoundarySetupSurfaceDisplacementSuboperators(ratel, dm_solution, ops_surface_displacement));

  // Evaluators
  for (PetscInt i = 0; i < ratel->surface_force_face_count; i++) {
    PetscCall(
        CeedEvaluatorCreateWithSameInputs(ratel->evaluator_residual_u, dm_coord, ops_surface_displacement[i], &evaluators_surface_displacement[i]));
    PetscCall(CeedEvaluatorSetLogEvent(evaluators_surface_displacement[i], RATEL_Diagnostics));
    PetscCall(CeedEvaluatorSetCeedOperatorLogEvent(evaluators_surface_displacement[i], RATEL_Diagnostics_CeedOp));
  }

  // Cleanup
  PetscCall(DMDestroy(&dm_solution));
  for (PetscInt i = 0; i < ratel->surface_force_face_count; i++) RatelCallCeed(ratel, CeedOperatorDestroy(&ops_surface_displacement[i]));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Setup Surface Centroid Operators Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup `CeedOperator` restricting surface forces to specific face.

  Collective across MPI processes.

  @param[in]   ratel                `Ratel` context
  @param[in]   dm                   `DMPlex` containing face
  @param[in]   face_label_value     Face label value on `dm`
  @param[in]   face_name            Name of face
  @param[out]  op_face_restriction  `CeedOperator` restricting values to face

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSetupFaceRestrictionNodalOperator(Ratel ratel, DM dm, PetscInt face_label_value, const char face_name[],
                                                      CeedOperator *op_face_restriction) {
  CeedVector           x_loc, x_stored, scale_stored, operator_multiplicity, multiplicity;
  CeedQFunction        qf_bounding_box, qf_setup;
  CeedOperator         op_setup;
  CeedQFunctionContext ctx_qf_bounding_box;
  CeedElemRestriction  restriction_x, restriction_u, restriction_x_stored, restriction_scale;
  CeedBasis            basis_x_to_u;
  DMLabel              face_label;
  PetscInt             height_face = 1, dim = 3, num_comp_u = 3;
  PetscMemType         x_mem_type;
  Vec                  X_loc;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Setup Bounding Box Context"));

  // DM
  PetscCall(DMGetLabel(dm, "Face Sets", &face_label));
  PetscCall(DMGetDimension(dm, &dim));
  {
    const CeedInt *field_sizes;

    PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[0], NULL, &field_sizes));
    num_comp_u = field_sizes[0];  // First field should be displacement
  }

  // Coordinates vector
  {
    PetscInt x_size;

    PetscCall(DMGetCoordinatesLocal(dm, &X_loc));
    PetscCall(VecGetLocalSize(X_loc, &x_size));
    RatelCallCeed(ratel, CeedVectorCreate(ratel->ceed, x_size, &x_loc));
    PetscCall(VecReadPetscToCeed(X_loc, &x_mem_type, x_loc));
  }

  // Multiplicity operator
  // -- Restrictions and multiplicity
  {
    CeedVector multiplicity_lvec, multiplicity_evec;

    PetscCall(RatelDMPlexCeedElemRestrictionCreate(ratel, dm, face_label, face_label_value, height_face, 0, &restriction_u));

    RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_u, &operator_multiplicity, NULL));
    RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_u, &multiplicity_lvec, &multiplicity_evec));
    RatelCallCeed(ratel, CeedVectorSetValue(operator_multiplicity, 0.0));

    RatelCallCeed(ratel, CeedVectorSetValue(multiplicity_evec, 1.0));
    RatelCallCeed(ratel, CeedVectorSetValue(multiplicity_lvec, 0.0));
    RatelCallCeed(ratel, CeedElemRestrictionApply(restriction_u, CEED_TRANSPOSE, multiplicity_evec, multiplicity_lvec, CEED_REQUEST_IMMEDIATE));
    {
      CeedSize          lvec_len;
      CeedScalar       *multiplicity_array;
      const CeedScalar *lvec_array;

      RatelCallCeed(ratel, CeedVectorGetLength(multiplicity_lvec, &lvec_len));
      RatelCallCeed(ratel, CeedVectorGetArrayRead(multiplicity_lvec, CEED_MEM_HOST, &lvec_array));
      RatelCallCeed(ratel, CeedVectorGetArray(operator_multiplicity, CEED_MEM_HOST, &multiplicity_array));
      for (PetscInt i = 0; i < lvec_len; i++) {
        if (lvec_array[i] > 0.0) multiplicity_array[i] += 1.0;
      }
      RatelCallCeed(ratel, CeedVectorRestoreArrayRead(multiplicity_lvec, &lvec_array));
      RatelCallCeed(ratel, CeedVectorRestoreArray(operator_multiplicity, &multiplicity_array));
    }
    RatelCallCeed(ratel, CeedVectorDestroy(&multiplicity_evec));
    RatelCallCeed(ratel, CeedVectorDestroy(&multiplicity_lvec));
  }

  // -- Restrictions
  PetscCall(RatelDebug(ratel, "---- Setting up libCEED restrictions"));
  PetscCall(RatelDMPlexCeedElemRestrictionCoordinateCreate(ratel, dm, face_label, face_label_value, height_face, &restriction_x));
  RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_u, &multiplicity, NULL));
  {
    CeedVector multiplicity_evec;

    RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_u, NULL, &multiplicity_evec));
    RatelCallCeed(ratel, CeedElemRestrictionApply(restriction_u, CEED_NOTRANSPOSE, operator_multiplicity, multiplicity_evec, CEED_REQUEST_IMMEDIATE));
    RatelCallCeed(ratel, CeedVectorSetValue(multiplicity, 0.0));
    RatelCallCeed(ratel, CeedElemRestrictionApply(restriction_u, CEED_TRANSPOSE, multiplicity_evec, multiplicity, CEED_REQUEST_IMMEDIATE));
    RatelCallCeed(ratel, CeedVectorDestroy(&multiplicity_evec));
  }
  PetscCall(RatelDMPlexCeedElemRestrictionCollocatedCreate(ratel, dm, face_label, face_label_value, height_face, 0, dim, &restriction_x_stored));
  RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_x_stored, &x_stored, NULL));
  PetscCall(RatelDMPlexCeedElemRestrictionCollocatedCreate(ratel, dm, face_label, face_label_value, height_face, 0, 1, &restriction_scale));
  RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_scale, &scale_stored, NULL));

  // -- Bases
  {
    CeedBasis basis_x, basis_u;

    PetscCall(RatelDMPlexCeedBasisCoordinateCreate(ratel, dm, face_label, face_label_value, height_face, &basis_x));
    PetscCall(RatelDMPlexCeedBasisCreate(ratel, dm, face_label, face_label_value, height_face, 0, &basis_u));
    PetscCall(CeedBasisCreateProjection(basis_x, basis_u, &basis_x_to_u));

    // ---- Cleanup
    RatelCallCeed(ratel, CeedBasisDestroy(&basis_x));
    RatelCallCeed(ratel, CeedBasisDestroy(&basis_u));
  }

  // -- Setup QFunction
  RatelCallCeed(ratel, CeedQFunctionCreateInterior(ratel->ceed, 1, SetupDirichletBCs, RatelQFunctionRelativePath(SetupDirichletBCs_loc), &qf_setup));
  RatelCallCeed(ratel, CeedQFunctionAddInput(qf_setup, "x", dim, CEED_EVAL_INTERP));
  RatelCallCeed(ratel, CeedQFunctionAddInput(qf_setup, "multiplicity", num_comp_u, CEED_EVAL_NONE));
  RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_setup, "x stored", dim, CEED_EVAL_NONE));
  RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_setup, "scale", 1, CEED_EVAL_NONE));

  // -- Setup Operator
  RatelCallCeed(ratel, CeedOperatorCreate(ratel->ceed, qf_setup, NULL, NULL, &op_setup));
  RatelCallCeed(ratel, CeedOperatorSetName(op_setup, "surface geometric data"));
  RatelCallCeed(ratel, CeedOperatorSetField(op_setup, "x", restriction_x, basis_x_to_u, CEED_VECTOR_ACTIVE));
  RatelCallCeed(ratel, CeedOperatorSetField(op_setup, "multiplicity", restriction_u, CEED_BASIS_NONE, multiplicity));
  RatelCallCeed(ratel, CeedOperatorSetField(op_setup, "x stored", restriction_x_stored, CEED_BASIS_NONE, x_stored));
  RatelCallCeed(ratel, CeedOperatorSetField(op_setup, "scale", restriction_scale, CEED_BASIS_NONE, scale_stored));

  // -- Compute geometric factors
  PetscCall(RatelDebug(ratel, "---- Projecting coordinates and computing multiplicity scaling factor"));
  if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(op_setup, stdout));
  RatelCallCeed(ratel, CeedOperatorApply(op_setup, x_loc, CEED_VECTOR_NONE, CEED_REQUEST_IMMEDIATE));

  // CEED Objects
  // -- QFunction
  PetscCall(RatelDebug(ratel, "---- Bounding box QFunction"));
  RatelCallCeed(
      ratel, CeedQFunctionCreateInterior(ratel->ceed, 1, RestrictBoundingBox, RatelQFunctionRelativePath(RestrictBoundingBox_loc), &qf_bounding_box));
  RatelCallCeed(ratel, CeedQFunctionAddInput(qf_bounding_box, "x stored", dim, CEED_EVAL_NONE));
  RatelCallCeed(ratel, CeedQFunctionAddInput(qf_bounding_box, "scale", 1, CEED_EVAL_NONE));
  RatelCallCeed(ratel, CeedQFunctionAddInput(qf_bounding_box, "r", num_comp_u, CEED_EVAL_NONE));
  RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_bounding_box, "v", num_comp_u, CEED_EVAL_NONE));

  // -- QFunctionContext
  PetscCall(RatelDebug(ratel, "---- Bounding box QFunctionContext"));
  {
    RatelBoundingBoxParams params_bounding_box;

    PetscCall(RatelBoundingBoxParamsFromOptions(ratel, "surface_force_face_", face_name, &params_bounding_box));
    RatelBoundingBoxParamsRestrictionContext ctx = {
        .box      = params_bounding_box,
        .num_comp = num_comp_u,
    };
    RatelCallCeed(ratel, CeedQFunctionContextCreate(ratel->ceed, &ctx_qf_bounding_box));
    RatelCallCeed(ratel, CeedQFunctionContextSetData(ctx_qf_bounding_box, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(ctx), &ctx));
  }
  RatelCallCeed(ratel, CeedQFunctionSetContext(qf_bounding_box, ctx_qf_bounding_box));
  RatelCallCeed(ratel, CeedQFunctionSetContextWritable(qf_bounding_box, PETSC_FALSE));

  // -- Operator
  PetscCall(RatelDebug(ratel, "---- Bounding box operator"));
  RatelCallCeed(ratel, CeedOperatorCreate(ratel->ceed, qf_bounding_box, NULL, NULL, op_face_restriction));
  RatelCallCeed(ratel, CeedOperatorSetField(*op_face_restriction, "x stored", restriction_x_stored, CEED_BASIS_NONE, x_stored));
  RatelCallCeed(ratel, CeedOperatorSetField(*op_face_restriction, "scale", restriction_scale, CEED_BASIS_NONE, scale_stored));
  RatelCallCeed(ratel, CeedOperatorSetField(*op_face_restriction, "r", restriction_u, CEED_BASIS_NONE, CEED_VECTOR_ACTIVE));
  RatelCallCeed(ratel, CeedOperatorSetField(*op_face_restriction, "v", restriction_u, CEED_BASIS_NONE, CEED_VECTOR_ACTIVE));
  {
    char operator_name[1024];

    PetscCall(PetscSNPrintf(operator_name, sizeof(operator_name), "surface force on face %s", face_name));
    RatelCallCeed(ratel, CeedOperatorSetName(*op_face_restriction, operator_name));
  }

  PetscCall(RatelDebug(ratel, "---- Final bounding box operator"));
  if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(*op_face_restriction, stdout));

  // Cleanup
  PetscCall(RatelDebug(ratel, "---- Cleanup"));
  PetscCall(VecReadCeedToPetsc(x_loc, x_mem_type, X_loc));
  RatelCallCeed(ratel, CeedVectorDestroy(&x_loc));
  RatelCallCeed(ratel, CeedVectorDestroy(&x_stored));
  RatelCallCeed(ratel, CeedVectorDestroy(&scale_stored));
  RatelCallCeed(ratel, CeedVectorDestroy(&operator_multiplicity));
  RatelCallCeed(ratel, CeedVectorDestroy(&multiplicity));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_x));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_u));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_x_stored));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_scale));
  RatelCallCeed(ratel, CeedBasisDestroy(&basis_x_to_u));
  RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_bounding_box));
  RatelCallCeed(ratel, CeedQFunctionContextDestroy(&ctx_qf_bounding_box));
  RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_setup));
  RatelCallCeed(ratel, CeedOperatorDestroy(&op_setup));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Setup Bounding Box Context Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup `CeedEvaluator` for computing surface forces.

  Collective across MPI processes.

  @param[in]   ratel                          `Ratel` context
  @param[out]  evaluator_surface_force        Surface force computation evaluator
  @param[out]  evaluators_surface_force_face  Surface force face restriction evaluator

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSetupSurfaceForceEvaluator(Ratel ratel, CeedEvaluator *evaluator_surface_force, CeedEvaluator evaluators_surface_force_face[]) {
  DM           dm_solution;
  CeedOperator op_residual, ops_surface_force_face[RATEL_MAX_BOUNDARY_FACES];

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Setup Surface Force Operator"));

  // Surface force
  RatelCallCeed(ratel, CeedCompositeOperatorCreate(ratel->ceed, &op_residual));
  RatelCallCeed(ratel, CeedOperatorSetName(op_residual, "surface force"));
  // -- Get volume residual operators
  {
    CeedOperator op_residual_u, *sub_operators;

    PetscCall(CeedEvaluatorGetCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
    RatelCallCeed(ratel, CeedCompositeOperatorGetSubList(op_residual_u, &sub_operators));
    for (PetscInt i = 0; i < ratel->num_materials; i++) {
      CeedOperator op_sub_residual = NULL;

      RatelCallCeed(ratel, CeedOperatorReferenceCopy(sub_operators[ratel->materials[i]->residual_indices[0]], &op_sub_residual));
      if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(op_sub_residual, stdout));
      RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_residual, op_sub_residual));
      RatelCallCeed(ratel, CeedOperatorDestroy(&op_sub_residual));
    }
    PetscCall(CeedEvaluatorRestoreCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
  }

  // Evaluator
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));
  PetscCall(CeedEvaluatorCreateWithSameInputs(ratel->evaluator_residual_u, dm_solution, op_residual, evaluator_surface_force));
  PetscCall(CeedEvaluatorSetLogEvent(*evaluator_surface_force, RATEL_Diagnostics));
  PetscCall(CeedEvaluatorSetCeedOperatorLogEvent(*evaluator_surface_force, RATEL_Diagnostics_CeedOp));

  // Setup surface force faces
  PetscCall(RatelDebug(ratel, "---- Surface force faces"));
  for (PetscInt i = 0; i < ratel->surface_force_face_count; i++) {
    PetscCall(RatelSetupFaceRestrictionNodalOperator(ratel, dm_solution, ratel->surface_force_face_label_values[i],
                                                     ratel->surface_force_face_names[i], &ops_surface_force_face[i]));
  }
  for (PetscInt i = 0; i < ratel->surface_force_face_count; i++) {
    PetscCall(
        CeedEvaluatorCreateWithSameInputs(ratel->evaluator_residual_u, dm_solution, ops_surface_force_face[i], &evaluators_surface_force_face[i]));
    PetscCall(CeedEvaluatorSetLogEvent(evaluators_surface_force_face[i], RATEL_Diagnostics));
    PetscCall(CeedEvaluatorSetCeedOperatorLogEvent(evaluators_surface_force_face[i], RATEL_Diagnostics_CeedOp));
  }

  // Cleanup
  PetscCall(DMDestroy(&dm_solution));
  RatelCallCeed(ratel, CeedOperatorDestroy(&op_residual));
  for (PetscInt i = 0; i < ratel->surface_force_face_count; i++) RatelCallCeed(ratel, CeedOperatorDestroy(&ops_surface_force_face[i]));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Setup Surface Force Operator Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup `CeedEvaluator` computing surface forces with cell-to-face bases.

  Collective across MPI processes.

  @param[in]   ratel                                  `Ratel` context
  @param[out]  evaluators_surface_force_cell_to_face  Surface force computation evaluators

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSetupSurfaceForceCellToFaceEvaluators(Ratel ratel, CeedEvaluator evaluators_surface_force_cell_to_face[]) {
  DM           dm_solution, dm_surface_force;
  CeedOperator ops_surface_force[RATEL_MAX_BOUNDARY_FACES];

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Setup Surface Force Cell-to-Face Operators"));

  // DM
  PetscCall(RatelDebug(ratel, "---- Surface Force Cell-to-Face DMs"));
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));
  switch (ratel->method_type) {
    case RATEL_METHOD_FEM:
    case RATEL_METHOD_MPM:  // Use FEM DM for MPM
      PetscCall(RatelSurfaceForceCellToFaceDMSetup_FEM(ratel, &dm_surface_force));
      break;
  }

  // Surface force
  PetscCheck(ratel->surface_force_face_count != 0, ratel->comm, PETSC_ERR_SUP, "At least one surface must be specified to compute  surface  forces");

  // -- libCEED operators
  for (PetscInt i = 0; i < ratel->surface_force_face_count; i++) {
    char operator_name[PETSC_MAX_OPTION_NAME];

    RatelCallCeed(ratel, CeedCompositeOperatorCreate(ratel->ceed, &ops_surface_force[i]));
    PetscCall(PetscSNPrintf(operator_name, sizeof(operator_name), "surface force cell-to-face on face %s", ratel->surface_force_face_names[i]));
    RatelCallCeed(ratel, CeedOperatorSetName(ops_surface_force[i], operator_name));
  }

  PetscCall(RatelDebug(ratel, "---- Surface force cell-to-face operator"));
  for (PetscInt i = 0; i < ratel->num_materials; i++) {
    PetscCall(RatelMaterialSetupSurfaceForceCellToFaceSuboperators(ratel->materials[i], dm_surface_force, ops_surface_force));
  }

  // Evaluators
  for (PetscInt i = 0; i < ratel->surface_force_face_count; i++) {
    PetscCall(CeedEvaluatorCreateWithSameInputs(ratel->evaluator_residual_u, dm_surface_force, ops_surface_force[i],
                                                &evaluators_surface_force_cell_to_face[i]));
    PetscCall(CeedEvaluatorSetLogEvent(evaluators_surface_force_cell_to_face[i], RATEL_Diagnostics));
    PetscCall(CeedEvaluatorSetCeedOperatorLogEvent(evaluators_surface_force_cell_to_face[i], RATEL_Diagnostics_CeedOp));
  }

  // Debugging
  if (ratel->is_debug) {
    // LCOV_EXCL_START
    PetscCall(RatelDebug(ratel, "---- Final surface force cell-to-face evaluators"));
    for (PetscInt i = 0; i < ratel->surface_force_face_count; i++) {
      PetscCall(CeedEvaluatorView(evaluators_surface_force_cell_to_face[i], PETSC_VIEWER_STDOUT_WORLD));
    }
    // LCOV_EXCL_STOP
  }

  // Cleanup
  PetscCall(DMDestroy(&dm_solution));
  PetscCall(DMDestroy(&dm_surface_force));
  for (PetscInt i = 0; i < ratel->surface_force_face_count; i++) RatelCallCeed(ratel, CeedOperatorDestroy(&ops_surface_force[i]));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Setup Surface Force Cell-to-Face Operators Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Internal code for computing diagnostic quantities.

  Collective across MPI processes.

  @param[in]   ratel  `Ratel` context
  @param[in]   U      Computed solution vector
  @param[in]   time   Final time value, or `1.0` for `SNES` solution
  @param[out]  D      Computed diagnostic quantities vector

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelComputeDiagnosticQuantities_Internal(Ratel ratel, Vec U, PetscReal time, Vec D) {
  PetscFunctionBeginUser;
  // Update time and BCs
  PetscCall(CeedEvaluatorUpdateTimeAndBoundaryValues(ratel->evaluator_residual_u, time));

  // Projected quantities
  PetscCall(RatelDebug(ratel, "---- Computing projected diagnostic quantities"));
  {
    Vec D_projection;

    PetscCall(VecGetSubVector(D, ratel->is_sub_dms_diagnostic[RATEL_DIAGNOSTIC_PROJECTED], &D_projection));
    PetscCall(CeedEvaluatorApplyGlobalToGlobal(ratel->evaluator_projected_diagnostic, U, D_projection));
    PetscCall(KSPSolve(ratel->ksp_diagnostic_projection, D_projection, D_projection));
    PetscCall(VecRestoreSubVector(D, ratel->is_sub_dms_diagnostic[RATEL_DIAGNOSTIC_PROJECTED], &D_projection));
  }

  // Nodal quantities
  PetscCall(RatelDebug(ratel, "---- Computing dual space diagnostic quantities"));
  {
    Vec D_dual;

    PetscCall(VecGetSubVector(D, ratel->is_sub_dms_diagnostic[RATEL_DIAGNOSTIC_DUAL], &D_dual));
    PetscCall(CeedEvaluatorApplyGlobalToGlobal(ratel->evaluator_dual_diagnostic, U, D_dual));
    PetscCall(VecRestoreSubVector(D, ratel->is_sub_dms_diagnostic[RATEL_DIAGNOSTIC_DUAL], &D_dual));
  }

  // Scale dual quantities by 1/nodal volume
  PetscCall(RatelDebug(ratel, "---- Fixing projected diagnostic quantities"));
  {
    Vec D_dual_loc, D_dual_in, D_dual, D_copy;

    // Get sub vector
    PetscCall(VecGetSubVector(D, ratel->is_sub_dms_diagnostic[RATEL_DIAGNOSTIC_DUAL], &D_dual));

    // Copy to input vector
    PetscCall(DMGetGlobalVector(ratel->dm_diagnostic, &D_copy));
    PetscCall(VecCopy(D, D_copy));
    PetscCall(VecGetSubVector(D_copy, ratel->is_sub_dms_diagnostic[RATEL_DIAGNOSTIC_DUAL], &D_dual_in));

    // Get output vector
    PetscCall(DMGetLocalVector(ratel->sub_dms_diagnostic[RATEL_DIAGNOSTIC_DUAL], &D_dual_loc));

    // Apply scaling
    PetscCall(CeedEvaluatorApplyGlobalToLocal(ratel->evaluator_dual_nodal_scale, D_dual_in, D_dual_loc));

    // Restore vectors
    PetscCall(VecRestoreSubVector(D_copy, ratel->is_sub_dms_diagnostic[RATEL_DIAGNOSTIC_DUAL], &D_dual_in));
    PetscCall(DMRestoreGlobalVector(ratel->dm_diagnostic, &D_copy));
    PetscCall(DMLocalToGlobal(ratel->sub_dms_diagnostic[RATEL_DIAGNOSTIC_DUAL], D_dual_loc, INSERT_VALUES, D_dual));
    PetscCall(DMRestoreLocalVector(ratel->sub_dms_diagnostic[RATEL_DIAGNOSTIC_DUAL], &D_dual_loc));
    PetscCall(VecRestoreSubVector(D, ratel->is_sub_dms_diagnostic[RATEL_DIAGNOSTIC_DUAL], &D_dual));

    // Cleanup
    PetscCall(VecDestroy(&D_dual_loc));
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Internal routine for computing diagnostic quantities on mesh faces.

  Collective across MPI processes.

  @param[in]   ratel           `Ratel` context
  @param[in]   U               Computed solution vector
  @param[in]   time            Current time value, or `1.0` for `SNES `solution
  @param[out]  surface_forces  Computed face forces

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelComputeSurfaceForcesCellToFace_Internal(Ratel ratel, Vec U, PetscReal time, PetscScalar *surface_forces) {
  PetscInt dim = 3, num_comp_u = 3;
  IS       is_u_loc;
  Vec      X_loc, F_loc;
  DM       dm_solution, dm_force;

  PetscFunctionBeginUser;
  if (ratel->surface_force_face_count == 0) PetscFunctionReturn(PETSC_SUCCESS);

  // Setup evaluators if needed
  if (!ratel->evaluators_surface_force_cell_to_face[0]) {
    PetscCall(RatelSetupSurfaceForceCellToFaceEvaluators(ratel, ratel->evaluators_surface_force_cell_to_face));
  }

  // Get number of solution components
  PetscCall(CeedEvaluatorGetDMs(ratel->evaluators_surface_force_cell_to_face[0], &dm_solution, &dm_force));
  PetscCall(DMGetDimension(dm_force, &dim));

  // Get local vectors and field sub-indices
  PetscCall(CeedEvaluatorGetLocalVectors(ratel->evaluators_surface_force_cell_to_face[0], &X_loc, NULL, NULL));
  PetscCall(DMGetLocalVector(dm_force, &F_loc));
  PetscCall(RatelDMGetFieldISLocal(ratel, dm_force, 0, &num_comp_u, &is_u_loc));

  // Compute quantities
  PetscCall(RatelDebug(ratel, "---- Computing surface forces"));
  PetscCall(CeedEvaluatorUpdateTimeAndBoundaryValues(ratel->evaluator_residual_u, time));
  PetscCall(DMGlobalToLocal(dm_solution, U, INSERT_VALUES, X_loc));
  for (PetscInt i = 0; i < ratel->surface_force_face_count; i++) {
    Vec         F_loc_u;
    PetscScalar local_surface_forces[num_comp_u];

    PetscCall(RatelDebug(ratel, "------ Face %s", ratel->surface_force_face_names[i]));
    for (PetscInt j = 0; j < num_comp_u; j++) local_surface_forces[j] = 0.0;

    // -- Apply surface force operator
    PetscCall(CeedEvaluatorApplyLocalToLocal(ratel->evaluators_surface_force_cell_to_face[i], X_loc, F_loc));

    // -- Compute surface forces
    PetscCall(VecGetSubVector(F_loc, is_u_loc, &F_loc_u));
    PetscCall(VecSetBlockSize(F_loc_u, num_comp_u));
    PetscCall(VecStrideSumAll(F_loc_u, local_surface_forces));

    PetscCall(MPIU_Allreduce(local_surface_forces, &surface_forces[dim * i], dim, MPIU_REAL, MPI_SUM, ratel->comm));

    PetscCall(RatelDebug(ratel, "------   Forces: [%f, %f, %f]", surface_forces[dim * i + 0], surface_forces[dim * i + 1],
                         dim >= 3 ? surface_forces[dim * i + 2] : 0.0));

    // -- Cleanup
    PetscCall(VecRestoreSubVector(F_loc, is_u_loc, &F_loc_u));
  }

  // Cleanup
  PetscCall(ISDestroy(&is_u_loc));
  PetscCall(CeedEvaluatorRestoreLocalVectors(ratel->evaluators_surface_force_cell_to_face[0], &X_loc, NULL, NULL));
  PetscCall(DMRestoreLocalVector(dm_force, &F_loc));
  PetscCall(CeedEvaluatorRestoreDMs(ratel->evaluators_surface_force_cell_to_face[0], &dm_solution, &dm_force));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Internal routine for computing surface forces on mesh faces.

  Collective across MPI processes.

  @param[in]   ratel           `Ratel` context
  @param[in]   U               Computed solution vector
  @param[in]   time            Current time value, or `1.0` for `SNES` solution
  @param[out]  surface_forces  Computed face forces

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelComputeSurfaceForces_Internal(Ratel ratel, Vec U, PetscReal time, PetscScalar *surface_forces) {
  PetscInt dim, num_comp_u;
  DMLabel  face_label;
  IS       is_u_loc;
  Vec      Y_loc, F_loc;
  DM       dm;

  PetscFunctionBeginUser;
  if (ratel->surface_force_face_count == 0) PetscFunctionReturn(PETSC_SUCCESS);

  // Setup evaluator if needed
  if (!ratel->evaluator_surface_force) {
    PetscCall(RatelSetupSurfaceForceEvaluator(ratel, &ratel->evaluator_surface_force, ratel->evaluators_surface_force_face));
  }
  PetscCall(CeedEvaluatorGetDMs(ratel->evaluator_surface_force, NULL, &dm));

  // Get dimension & solution components
  {
    const CeedInt *field_sizes;

    PetscCall(DMGetDimension(dm, &dim));
    PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[0], NULL, &field_sizes));
    num_comp_u = field_sizes[0];  // First field should be displacement
  }

  PetscCall(DMGetLocalVector(dm, &F_loc));
  PetscCall(DMGetLocalVector(dm, &Y_loc));

  // Compute quantities
  PetscCall(RatelDebug(ratel, "---- Computing surface forces"));
  PetscCall(CeedEvaluatorUpdateTimeAndBoundaryValues(ratel->evaluator_residual_u, time));
  PetscCall(CeedEvaluatorApplyGlobalToLocal(ratel->evaluator_surface_force, U, Y_loc));

  PetscCall(DMGetLabel(dm, "Face Sets", &face_label));

  PetscCall(RatelDMGetFieldISLocal(ratel, dm, 0, &num_comp_u, &is_u_loc));

  for (PetscInt f = 0; f < ratel->surface_force_face_count; f++) {
    const PetscScalar *y;
    Vec                F_loc_u;
    PetscScalar        local_surface_forces[num_comp_u];

    // -- Apply surface force face restriction operator
    PetscCall(VecZeroEntries(F_loc));
    PetscCall(CeedEvaluatorApplyLocalToLocal(ratel->evaluators_surface_force_face[f], Y_loc, F_loc));

    // -- Read local vector
    PetscCall(VecGetArrayRead(F_loc, &y));
    for (PetscInt j = 0; j < num_comp_u; j++) local_surface_forces[j] = 0;
    PetscCall(RatelDebug(ratel, "------ Face %s", ratel->surface_force_face_names[f]));

    // -- Compute surface forces
    PetscCall(VecGetSubVector(F_loc, is_u_loc, &F_loc_u));
    PetscCall(VecSetBlockSize(F_loc_u, num_comp_u));
    PetscCall(VecStrideSumAll(F_loc_u, local_surface_forces));
    for (PetscInt j = 0; j < num_comp_u; j++) local_surface_forces[j] *= -1.0;

    PetscCall(MPIU_Allreduce(local_surface_forces, &surface_forces[dim * f], dim, MPIU_REAL, MPI_SUM, ratel->comm));

    PetscCall(RatelDebug(ratel, "------   Forces: [%f, %f, %f]", surface_forces[dim * f + 0], surface_forces[dim * f + 1],
                         dim >= 3 ? surface_forces[dim * f + 2] : 0.0));

    // -- Cleanup
    PetscCall(VecRestoreSubVector(F_loc, is_u_loc, &F_loc_u));
  }

  // Restore local vectors
  PetscCall(DMRestoreLocalVector(dm, &F_loc));
  PetscCall(DMRestoreLocalVector(dm, &Y_loc));
  PetscCall(CeedEvaluatorRestoreDMs(ratel->evaluator_surface_force, NULL, &dm));
  PetscCall(ISDestroy(&is_u_loc));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Internal routine for computing centroids of mesh faces

  @param[in]   ratel              `Ratel` context
  @param[in]   U                  Computed solution vector
  @param[in]   time               Current time value, or `1.0` for `SNES` solution
  @param[out]  surface_centroids  Computed centroids

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelComputeSurfaceCentroids_Internal(Ratel ratel, Vec U, PetscReal time, PetscScalar *surface_centroids) {
  PetscInt dim, num_comp_u;
  IS       is_u_loc;
  Vec      X_loc, F_loc;
  DM       dm_solution, dm_force;

  PetscFunctionBeginUser;
  if (ratel->surface_force_face_count == 0) PetscFunctionReturn(PETSC_SUCCESS);

  // Setup evaluators if needed
  if (!ratel->evaluators_surface_displacement[0]) {
    PetscCall(RatelSetupSurfaceDisplacementEvaluators(ratel, ratel->evaluators_surface_displacement));
  }

  // Get number of solution components
  PetscCall(CeedEvaluatorGetDMs(ratel->evaluators_surface_displacement[0], &dm_solution, &dm_force));
  PetscCall(DMGetDimension(dm_force, &dim));

  // Get local vector and field sub-indices
  PetscCall(CeedEvaluatorGetLocalVectors(ratel->evaluators_surface_displacement[0], &X_loc, NULL, NULL));
  PetscCall(DMGetLocalVector(dm_force, &F_loc));
  PetscCall(RatelDMGetFieldISLocal(ratel, dm_force, 0, &num_comp_u, &is_u_loc));

  // Compute quantities
  PetscCall(RatelDebug(ratel, "---- Computing surface centroids"));
  PetscCall(CeedEvaluatorUpdateTimeAndBoundaryValues(ratel->evaluator_residual_u, time));
  PetscCall(DMGlobalToLocal(dm_solution, U, INSERT_VALUES, X_loc));
  for (PetscInt i = 0; i < ratel->surface_force_face_count; i++) {
    PetscScalar local_centroids[num_comp_u];
    Vec         F_loc_u;

    PetscCall(RatelDebug(ratel, "------ Face %s", ratel->surface_force_face_names[i]));

    for (PetscInt j = 0; j < num_comp_u; j++) local_centroids[j] = 0.0;

    // -- Apply surface displacement operator
    PetscCall(CeedEvaluatorApplyLocalToLocal(ratel->evaluators_surface_displacement[i], X_loc, F_loc));

    // -- Compute average surface displacement
    PetscCall(VecGetSubVector(F_loc, is_u_loc, &F_loc_u));
    PetscCall(VecSetBlockSize(F_loc_u, num_comp_u));
    PetscCall(VecStrideSumAll(F_loc_u, local_centroids));

    // -- Divide by surface area
    for (PetscInt j = 0; j < dim; j++) local_centroids[j] /= ratel->initial_surface_area[i];
    PetscCall(MPIU_Allreduce(local_centroids, &surface_centroids[dim * i], dim, MPIU_REAL, MPI_SUM, ratel->comm));
    for (PetscInt j = 0; j < dim; j++) surface_centroids[dim * i + j] += ratel->initial_surface_centroid[i][j];

    // -- Debugging
    PetscCall(RatelDebug(ratel, "------   Centroid: [%f, %f, %f]", surface_centroids[num_comp_u * i + 0], surface_centroids[num_comp_u * i + 1],
                         dim >= 3 ? surface_centroids[num_comp_u * i + 2] : 0.0));

    // -- Cleanup
    PetscCall(VecRestoreSubVector(F_loc, is_u_loc, &F_loc_u));
  }

  // Cleanup
  PetscCall(ISDestroy(&is_u_loc));
  PetscCall(CeedEvaluatorRestoreLocalVectors(ratel->evaluators_surface_displacement[0], &X_loc, NULL, NULL));
  PetscCall(DMRestoreLocalVector(dm_force, &F_loc));
  PetscCall(CeedEvaluatorRestoreDMs(ratel->evaluators_surface_displacement[0], &dm_solution, &dm_force));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}

/// @addtogroup RatelCore
/// @{

/**
  @brief Determine if `Ratel` context has MMS solution.

  Not collective across MPI processes.

  @param[in]   ratel    `Ratel` context
  @param[out]  has_mms  Boolean flag indicating if `Ratel` context has MMS solution

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelHasMMS(Ratel ratel, PetscBool *has_mms) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Checking MMS Status"));
  *has_mms = ratel->has_mms;
  PetscCall(RatelDebug(ratel, "---- MMS status: %s", *has_mms ? "has mms" : "no mms"));
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Checking MMS status success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Determine if `Ratel` context is using a single material  with a Ceed BP material model.

  Not collective across MPI processes.

  @param[in]   ratel       `Ratel` context
  @param[out]  is_ceed_bp  Boolean flag indicating if `Ratel` context is using single Ceed BP material model

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelIsCeedBP(Ratel ratel, PetscBool *is_ceed_bp) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Checking Ceed BP Status"));
  if (ratel->num_materials > 1) {
    *is_ceed_bp = PETSC_FALSE;
  } else {
    const char *name;

    PetscCall(RatelMaterialGetModelName(ratel->materials[0], &name));
    PetscCall(PetscStrncmp(name, "CEED BP", 7, is_ceed_bp));
  }
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Checking Ceed BP status success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Compute the L2 error from the manufactured solution.
    Caller is responsible for freeing `l2_error`.

  Collective across MPI processes.

  @param[in]   ratel       `Ratel` context
  @param[in]   U           Computed solution vector
  @param[in]   time        Final time value, or `1.0` for `SNES` solution
  @param[out]  num_fields  Number of L2 error values
  @param[out]  l2_error    Computed L2 error for solution compared to MMS

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelComputeMMSL2Error(Ratel ratel, Vec U, PetscScalar time, PetscInt *num_fields, PetscScalar **l2_error) {
  DM        dm_solution;
  PetscBool has_mms;
  Vec       E;
  IS       *is_field;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Compute MMS L2 Error"));

  PetscCall(RatelHasMMS(ratel, &has_mms));
  PetscCheck(has_mms, ratel->comm, PETSC_ERR_SUP, "No manufactured solution available");

  // Setup evaluator if needed
  if (!ratel->evaluator_mms_error) PetscCall(RatelSetupMMSErrorEvaluator(ratel, &ratel->evaluator_mms_error));

  // DM
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));

  // Get global vector
  PetscCall(DMGetGlobalVector(dm_solution, &E));

  // Field ISs
  PetscCall(DMCreateFieldDecomposition(dm_solution, num_fields, NULL, &is_field, NULL));

  // Allocate results array
  PetscCall(PetscCalloc1(*num_fields, l2_error));

  // Integrate L2 error
  PetscCall(RatelDebug(ratel, "---- Computing MMS error"));
  PetscCall(CeedEvaluatorUpdateTimeAndBoundaryValues(ratel->evaluator_residual_u, time));
  PetscCall(CeedEvaluatorApplyGlobalToGlobal(ratel->evaluator_mms_error, U, E));

  // Compute L2 error per field
  for (PetscInt i = 0; i < *num_fields; i++) {
    PetscScalar l2_error_field = 1.0;
    Vec         E_field;

    if (*num_fields > 1) PetscCall(VecGetSubVector(E, is_field[i], &E_field));
    else E_field = E;
    PetscCall(VecSum(E_field, &l2_error_field));
    if (*num_fields > 1) PetscCall(VecDestroy(&E_field));
    (*l2_error)[i] = sqrt(l2_error_field);
    PetscCall(RatelDebug(ratel, "---- L2 Error, field %" PetscInt_FMT ": %f", i, (*l2_error)[i]));
  }

  // Cleanup
  PetscCall(DMRestoreGlobalVector(dm_solution, &E));
  PetscCall(DMDestroy(&dm_solution));
  for (PetscInt i = 0; i < *num_fields; i++) PetscCall(ISDestroy(&is_field[i]));
  PetscCall(PetscFree(is_field));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Compute MMS L2 Error Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief View MMS L2 error

  Collective across MPI processes.

  @param[in]  ratel  `Ratel` context
  @param[in]  time   Final time value, or `1.0` for `SNES` solution
  @param[in]  U      Final solution vector
**/
PetscErrorCode RatelViewMMSL2ErrorFromOptions(Ratel ratel, PetscScalar time, Vec U) {
  PetscScalar atol  = 5e-7;
  PetscBool   quiet = PETSC_FALSE;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel View MMS L2 Error From Options"));

  PetscOptionsBegin(ratel->comm, NULL, "Ratel MMS L2 Error", NULL);
  PetscCall(PetscOptionsBool("-quiet", "Suppress summary outputs", NULL, quiet, &quiet, NULL));
  PetscCall(PetscOptionsScalar("-mms_l2_atol", "L2 error tolerance", NULL, atol, &atol, NULL));
  PetscOptionsEnd();

  PetscBool has_mms = PETSC_FALSE;
  PetscCall(RatelHasMMS(ratel, &has_mms));
  if (has_mms) {
    PetscInt     num_fields;
    PetscScalar *l2_error;

    PetscCall(RatelComputeMMSL2Error(ratel, U, time, &num_fields, &l2_error));
    for (PetscInt i = 0; i < num_fields; i++) {
      if (!quiet || l2_error[i] > atol) PetscCall(PetscPrintf(ratel->comm, "L2 Error, field %" PetscInt_FMT ": %0.12e\n", i, l2_error[i]));
    }
    PetscCall(PetscFree(l2_error));
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel View MMS L2 Error From Options Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Retrieve expected strain energy.

  Not collective across MPI processes.

  @param[in]   ratel                   `Ratel` context
  @param[out]  has_expected            `PETSC_TRUE` if expected strain energy was set
  @param[out]  expected_strain_energy  Expected strain energy

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelGetExpectedStrainEnergy(Ratel ratel, PetscBool *has_expected, PetscScalar *expected_strain_energy) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Expected Strain Energy"));

  *has_expected           = ratel->has_expected_strain_energy;
  *expected_strain_energy = ratel->expected_strain_energy;
  PetscCall(RatelDebug(ratel, "---- expected strain energy: %f", *expected_strain_energy));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Expected Strain Energy Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Retrieve expected max displacements.

  Not collective across MPI processes.

  @param[in]      ratel                      `Ratel` context
  @param[in,out]  num_components             Size of input arrays / number of output components
  @param[out]     has_expected               Boolean array, `PETSC_TRUE` at index if expected value set for component
  @param[out]     expected_max_displacement  Expected max displacement for component

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelGetExpectedMaxDisplacement(Ratel ratel, PetscInt *num_components, PetscBool has_expected[],
                                               PetscScalar expected_max_displacement[]) {
  PetscInt num_comp_u;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Expected Max Displacement"));

  // Get number of solution components
  {
    const CeedInt *field_sizes;

    PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[0], NULL, &field_sizes));
    num_comp_u = field_sizes[0];  // First field should be displacement
  }
  if (num_comp_u < *num_components) *num_components = num_comp_u;

  // Get expected displacements
  for (PetscInt i = 0; i < *num_components; i++) {
    has_expected[i]              = ratel->has_expected_max_displacement[i];
    expected_max_displacement[i] = ratel->expected_max_displacement[i];
  }
  PetscCall(RatelDebug(ratel, "---- expected max displacements: [%f, %f, %f]", expected_max_displacement[0], expected_max_displacement[1],
                       expected_max_displacement[2]));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Expected Max Displacement Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Retrieve expected surface forces on face.

  Not collective across MPI processes.

  @param[in]      ratel                   `Ratel` context
  @param[in]      face                    Face name
  @param[in,out]  num_components          Size of input arrays / number of output components
  @param[out]     has_expected            Boolean array, `PETSC_TRUE` at index if expected value set for component
  @param[out]     expected_surface_force  Expected surface forces

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelGetExpectedFaceSurfaceForce(Ratel ratel, const char *face, PetscInt *num_components, PetscBool has_expected[],
                                                PetscScalar expected_surface_force[]) {
  PetscInt num_comp_u;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Expected Surface Force"));

  // Get number of solution components
  {
    const CeedInt *field_sizes;

    PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[0], NULL, &field_sizes));
    num_comp_u = field_sizes[0] > 3 ? 3 : field_sizes[0];  // First field should be displacement
  }
  PetscCheck(*num_components >= num_comp_u, ratel->comm, PETSC_ERR_ARG_OUTOFRANGE, "Output array too small (%" PetscInt_FMT " < %" PetscInt_FMT ")",
             *num_components, num_comp_u);
  *num_components = num_comp_u;

  // find index of face
  PetscInt face_index = -1;
  for (PetscInt i = 0; i < ratel->surface_force_face_count; i++) {
    PetscBool is_match = PETSC_FALSE;

    PetscCall(PetscStrcmp(face, ratel->surface_force_face_names[i], &is_match));
    if (is_match) {
      face_index = i;
      break;
    }
  }
  PetscCheck(face_index >= 0, ratel->comm, PETSC_ERR_ARG_OUTOFRANGE, "Surface forces not computed for face %s", face);

  // Get expected surface forces for face
  for (PetscInt i = 0; i < *num_components; i++) {
    has_expected[i]           = ratel->has_expected_surface_force[face_index][i];
    expected_surface_force[i] = ratel->expected_surface_force[face_index][i];
  }

  PetscCall(RatelDebug(ratel, "---- expected surface force on face %s: [%f, %f, %f]", face, expected_surface_force[0], expected_surface_force[1],
                       expected_surface_force[2]));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Expected Surface Force Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Retrieve expected centroid of face.

  Not collective across MPI processes.

  @param[in]      ratel              `Ratel` context
  @param[in]      face               Face name
  @param[in,out]  num_components     Size of input arrays / number of output components
  @param[out]     has_expected       Boolean array, `PETSC_TRUE` at index if expected value set for component
  @param[out]     expected_centroid  Expected centroid values

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelGetExpectedFaceCentroid(Ratel ratel, const char *face, PetscInt *num_components, PetscBool has_expected[],
                                            PetscScalar expected_centroid[]) {
  PetscInt num_comp_u;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Expected Centroid"));

  // Get number of solution components
  {
    const CeedInt *field_sizes;

    PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[0], NULL, &field_sizes));
    num_comp_u = field_sizes[0] > 3 ? 3 : field_sizes[0];  // First field should be displacement
  }
  PetscCheck(*num_components >= num_comp_u, ratel->comm, PETSC_ERR_ARG_OUTOFRANGE, "Output array too small (%" PetscInt_FMT " < %" PetscInt_FMT ")",
             *num_components, num_comp_u);
  *num_components = num_comp_u;

  // find index of face
  PetscInt face_index = -1;
  for (PetscInt i = 0; i < ratel->surface_force_face_count; i++) {
    PetscBool is_match = PETSC_FALSE;

    PetscCall(PetscStrcmp(face, ratel->surface_force_face_names[i], &is_match));
    if (is_match) {
      face_index = i;
      break;
    }
  }
  PetscCheck(face_index >= 0, ratel->comm, PETSC_ERR_ARG_OUTOFRANGE, "Centroid not computed for face %s", face);

  // Get expected centroid for face
  for (PetscInt i = 0; i < *num_components; i++) {
    has_expected[i]      = ratel->has_expected_centroid[face_index][i];
    expected_centroid[i] = ratel->expected_centroid[face_index][i];
  }
  PetscCall(
      RatelDebug(ratel, "---- expected centroid on face %s: [%f, %f, %f]", face, expected_centroid[0], expected_centroid[1], expected_centroid[2]));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Expected Centroid Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Compute the error between the expected and final strain energy.

  Not collective across MPI processes.

  @param[in]    ratel                `Ratel` Context
  @param[in]    strain_energy        Computed final strain energy
  @param[out]   has_expected         `PETSC_TRUE` if expected strain energy provided
  @param[out]   strain_energy_error  Computed error in the final strain energy
*/
PetscErrorCode RatelComputeStrainEnergyError(Ratel ratel, PetscScalar strain_energy, PetscBool *has_expected, PetscScalar *strain_energy_error) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Compute Strain Energy Error"));

  PetscCall(RatelGetExpectedStrainEnergy(ratel, has_expected, strain_energy_error));
  *strain_energy_error = *has_expected ? PetscAbsScalar(*strain_energy_error - strain_energy) : 0.0;

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Compute Strain Energy Error Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Compute the error between the expected and final maximum displacements.

  Not collective across MPI processes.

  @param[in]    ratel                   `Ratel` Context
  @param[in]    num_components          Number of components of the input and output arrays
  @param[in]    max_displacement        Computed final maximum displacements
  @param[out]   has_any_expected        `PETSC_TRUE` if expected maximum displacement provided for any component
  @param[out]   max_displacement_error  Computed error in the final maximum displacements
*/
PetscErrorCode RatelComputeMaxDisplacementError(Ratel ratel, PetscInt num_components, const PetscScalar max_displacement[],
                                                PetscBool *has_any_expected, PetscScalar max_displacement_error[]) {
  PetscBool has_expected[3] = {PETSC_FALSE};
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Compute Max Displacement Error"));

  PetscCall(RatelGetExpectedMaxDisplacement(ratel, &num_components, has_expected, max_displacement_error));

  *has_any_expected = PETSC_FALSE;
  for (PetscInt i = 0; i < num_components; i++) {
    if (has_expected[i]) *has_any_expected = PETSC_TRUE;
    max_displacement_error[i] = has_expected[i] ? PetscAbsScalar(max_displacement_error[i] - max_displacement[i]) : 0.0;
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Compute Max Displacement Error Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Compute max solution error.

  Collective across MPI processes.

  @param[in]  ratel  `Ratel` context
  @param[in]  time   Final time value, or `1.0` for `SNES` solution
  @param[in]  U      Final solution vector

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelViewMaxSolutionValuesErrorFromOptions(Ratel ratel, PetscScalar time, Vec U) {
  PetscScalar  atol = 1e-4, rtol = 1e-4;
  PetscInt    *num_components, num_fields, offset = 0, num_component_u;
  PetscScalar  max_values[RATEL_MAX_FIELDS] = {0.0}, error[3] = {0.0}, expected[3] = {0.0};
  PetscBool    has_expected[3] = {PETSC_FALSE}, any_expected = PETSC_FALSE, wrong_displacement = PETSC_FALSE, quiet = PETSC_FALSE, view = PETSC_FALSE;
  const char **field_names, **component_names;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel View Max Solution Values Error From Options"));

  PetscOptionsBegin(ratel->comm, NULL, "Ratel max displacement error", NULL);
  PetscCall(PetscOptionsScalar("-max_displacement_atol", "Max displacement error absolute tolerance", NULL, atol, &atol, NULL));
  PetscCall(PetscOptionsScalar("-max_displacement_rtol", "Max displacement error relative tolerance", NULL, rtol, &rtol, NULL));
  PetscCall(PetscOptionsBool("-quiet", "Suppress summary outputs", NULL, quiet, &quiet, NULL));
  PetscCall(PetscOptionsBool("-view_max_solution", "View maximum final solution values", NULL, view, &view, NULL));
  PetscOptionsEnd();

  // Compute max values, including boundary
  PetscCall(RatelMaterialGetActiveFieldNames(ratel->materials[0], &field_names, &component_names));
  PetscCall(RatelComputeMaxSolutionValues(ratel, U, time, &num_fields, NULL, &num_components, max_values));
  num_component_u = (num_components[0] > 3) ? 3 : num_components[0];
  if (!quiet || view) {
    // LCOV_EXCL_START
    // First field is handled specially since damage may be a component
    PetscCall(PetscPrintf(ratel->comm, "Max displacement: [%0.12e, %0.12e, %0.12e]\n", max_values[0], max_values[1], max_values[2]));
    for (PetscInt i = num_component_u; i < num_components[0]; i++) {
      PetscCall(PetscPrintf(ratel->comm, "Max %s: %0.12e\n", component_names[i], max_values[i]));
    }
    offset = num_components[0];
    for (PetscInt i = 1; i < num_fields; i++) {
      PetscCall(PetscPrintf(ratel->comm, "Max %s: %s%0.12e\n", field_names[i], num_components[i] > 1 ? "[" : "", max_values[offset]));
      for (PetscInt j = 1; j < num_components[i]; j++, offset++) {
        PetscCall(PetscPrintf(ratel->comm, ", %0.12e", max_values[offset]));
      }
    }
    // LCOV_EXCL_STOP
  }

  // Verify max displacement
  PetscCall(RatelComputeMaxDisplacementError(ratel, num_component_u, max_values, &any_expected, error));
  PetscCall(RatelGetExpectedMaxDisplacement(ratel, &num_component_u, has_expected, expected));
  for (PetscInt i = 0; i < num_component_u; i++) {
    PetscScalar tolerance = PetscMin(atol, rtol * (PetscAbs(expected[i]) > PETSC_MACHINE_EPSILON ? PetscAbs(expected[i]) : 1.0));
    if (error[i] > tolerance) wrong_displacement = PETSC_TRUE;
  }
  if ((!quiet && any_expected) || wrong_displacement) {
    // LCOV_EXCL_START
    PetscCall(PetscPrintf(ratel->comm, "Max displacement errors: [%0.12e, %0.12e, %0.12e]\n", error[0], error[1], error[2]));
    // LCOV_EXCL_STOP
  }

  // Clean-up
  PetscCall(PetscFree(num_components));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel View Max Solution Values Error From Options Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Compute the error between the expected and final face forces and centroids.

  Not collective across MPI processes.

  @param[in]    ratel                           `Ratel` Context
  @param[in]    face                            Face name
  @param[in]    num_components                  Number of components of the input and output arrays
  @param[in]    centroid                        Computed final centroid for face
  @param[in]    surface_force                   Computed final surface forces for face
  @param[out]   has_any_expected_centroid       `PETSC_TRUE` if expected centroid provided for face
  @param[out]   has_any_expected_surface_force  `PETSC_TRUE` if expected surface force provided for face
  @param[out]   centroid_error                  Computed error in the final centroid for face
  @param[out]   surface_force_error             Computed error in the final surface force for face
*/
PetscErrorCode RatelComputeFaceForceErrors(Ratel ratel, const char *face, PetscInt num_components, const PetscScalar centroid[],
                                           const PetscScalar surface_force[], PetscBool *has_any_expected_centroid,
                                           PetscBool *has_any_expected_surface_force, PetscScalar centroid_error[],
                                           PetscScalar surface_force_error[]) {
  PetscBool has_expected_surface_force[3] = {PETSC_FALSE};
  PetscBool has_expected_centroid[3]      = {PETSC_FALSE};

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Compute Face Force Errors"));

  PetscCall(RatelGetExpectedFaceCentroid(ratel, face, &num_components, has_expected_centroid, centroid_error));
  PetscCall(RatelGetExpectedFaceSurfaceForce(ratel, face, &num_components, has_expected_surface_force, surface_force_error));

  *has_any_expected_centroid      = PETSC_FALSE;
  *has_any_expected_surface_force = PETSC_FALSE;
  for (PetscInt i = 0; i < num_components; i++) {
    if (centroid_error[i]) *has_any_expected_centroid = PETSC_TRUE;
    if (surface_force_error[i]) *has_any_expected_surface_force = PETSC_TRUE;
    centroid_error[i]      = has_expected_centroid[i] ? PetscAbsScalar(centroid_error[i] - centroid[i]) : 0.0;
    surface_force_error[i] = has_expected_surface_force[i] ? PetscAbsScalar(surface_force_error[i] - surface_force[i]) : 0.0;
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Compute Face Force Errors Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Compute surface force and centroid errors.

  Collective across MPI processes.

  @param[in]  ratel  `Ratel` context
  @param[in]  time   Final time value, or `1.0` for `SNES` solution
  @param[in]  U      Final solution vector

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelViewSurfaceForceAndCentroidErrorFromOptions(Ratel ratel, PetscScalar time, Vec U) {
  PetscScalar force_atol = 1e-4, force_rtol = 1e-4, centroid_atol = 1e-4, centroid_rtol = 1e-4;
  PetscBool   quiet = PETSC_FALSE, view = PETSC_FALSE;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel View Surface Force and Centroid Error From Options"));

  PetscOptionsBegin(ratel->comm, NULL, "Ratel surface force and centroid error", NULL);
  PetscCall(PetscOptionsScalar("-surface_force_atol", "Surface force error absolute tolerance", NULL, force_atol, &force_atol, NULL));
  PetscCall(PetscOptionsScalar("-surface_force_rtol", "Surface force error relative tolerance", NULL, force_rtol, &force_rtol, NULL));
  PetscCall(PetscOptionsScalar("-centroid_force_atol", "Surface centroid error absolute tolerance", NULL, centroid_atol, &centroid_atol, NULL));
  PetscCall(PetscOptionsScalar("-centroid_force_rtol", "Surface centroid error relative tolerance", NULL, centroid_rtol, &centroid_rtol, NULL));
  PetscCall(PetscOptionsBool("-quiet", "Suppress summary outputs", NULL, quiet, &quiet, NULL));
  PetscCall(PetscOptionsBool("-view_surface_force", "View final surface force and centroid values", NULL, view, &view, NULL));
  PetscOptionsEnd();

  // Compute max values, including boundary
  PetscInt     num_components = 3;
  PetscReal   *surface_forces = NULL, *surface_centroids = NULL;
  PetscInt     num_force_faces;
  const char **force_faces = NULL;

  // Compute surface forces and centroids
  PetscCall(RatelComputeSurfaceForces(ratel, U, time, &num_components, &surface_forces));
  PetscCall(RatelComputeSurfaceCentroids(ratel, U, time, &num_components, &surface_centroids));
  PetscCall(RatelGetSurfaceForceFaces(ratel, &num_force_faces, &force_faces));

  // Verify surface forces and centroids
  for (PetscInt f = 0; f < num_force_faces; f++) {
    PetscBool   wrong_centroid = PETSC_FALSE, wrong_force = PETSC_FALSE, has_any_expected_centroid, has_any_expected_surface_force;
    PetscScalar force_error[3] = {0.0}, centroid_error[3] = {0.0}, expected_force[3] = {0.0}, expected_centroid[3] = {0.0};
    PetscBool   has_expected_force[3] = {0.0}, has_expected_centroid[3] = {0.0};
    PetscInt    offset = f * num_components;

    PetscCall(RatelComputeFaceForceErrors(ratel, force_faces[f], num_components, &surface_centroids[offset], &surface_forces[offset],
                                          &has_any_expected_centroid, &has_any_expected_surface_force, centroid_error, force_error));

    PetscCall(RatelGetExpectedFaceCentroid(ratel, force_faces[f], &num_components, has_expected_centroid, expected_centroid));
    PetscCall(RatelGetExpectedFaceSurfaceForce(ratel, force_faces[f], &num_components, has_expected_force, expected_force));
    for (PetscInt i = 0; i < num_components; i++) {
      PetscScalar scaled_expected    = PetscAbs(expected_centroid[i]) > PETSC_MACHINE_EPSILON ? PetscAbs(expected_centroid[i]) : 1.0;
      PetscScalar centroid_tolerance = PetscMin(centroid_atol, centroid_rtol * scaled_expected);
      scaled_expected                = PetscAbs(expected_force[i]) > PETSC_MACHINE_EPSILON ? PetscAbs(expected_force[i]) : 1.0;
      PetscScalar force_tolerance    = PetscMin(force_atol, force_rtol * scaled_expected);

      if (centroid_error[i] > centroid_tolerance) wrong_centroid = PETSC_TRUE;
      if (force_error[i] > force_tolerance) wrong_force = PETSC_TRUE;
    }

    // LCOV_EXCL_START
    if (!quiet || wrong_centroid || wrong_force || view) PetscCall(PetscPrintf(ratel->comm, "Surface %s:\n", force_faces[f]));
    if (!quiet || view) {
      PetscCall(PetscPrintf(ratel->comm, "  Centroid:             [%0.12e, %0.12e, %0.12e]\n", surface_centroids[offset + 0],
                            surface_centroids[offset + 1], surface_centroids[offset + 2]));
    }
    if (((!quiet || view) && has_any_expected_centroid) || wrong_centroid) {
      PetscCall(
          PetscPrintf(ratel->comm, "  Centroid Errors:      [%0.12e, %0.12e, %0.12e]\n", centroid_error[0], centroid_error[1], centroid_error[2]));
    }
    if (!quiet || view) {
      PetscCall(PetscPrintf(ratel->comm, "  Surface Force:        [%0.12e, %0.12e, %0.12e]\n", surface_forces[offset + 0], surface_forces[offset + 1],
                            surface_forces[offset + 2]));
    }
    if (((!quiet || view) && has_any_expected_surface_force) || wrong_force) {
      PetscCall(PetscPrintf(ratel->comm, "  Surface Force Errors: [%0.12e, %0.12e, %0.12e]\n", force_error[0], force_error[1], force_error[2]));
    }
    // LCOV_EXCL_STOP
  }

  // Cleanup
  PetscCall(PetscFree(surface_forces));
  PetscCall(PetscFree(surface_centroids));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel View Surface Force and Centroid Error From Options Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Compute the final strain energy in the computed solution.

  Collective across MPI processes.

  @param[in]   ratel          `Ratel` context
  @param[in]   U              Computed solution vector
  @param[in]   time           Final time value, or `1.0` for `SNES` solution
  @param[out]  strain_energy  Computed strain energy

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelComputeStrainEnergy(Ratel ratel, Vec U, PetscReal time, PetscScalar *strain_energy) {
  PetscScalar local_strain_energy = 0;
  Vec         E_loc;
  DM          dm_energy;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Compute Strain Energy"));

  // Setup context if needed
  if (!ratel->evaluator_strain_energy) PetscCall(RatelSetupStrainEnergyEvaluator(ratel, &ratel->evaluator_strain_energy));

  // Get local vector
  PetscCall(CeedEvaluatorGetDMs(ratel->evaluator_strain_energy, NULL, &dm_energy));
  PetscCall(DMGetLocalVector(dm_energy, &E_loc));

  // Compute strain energy
  PetscCall(RatelDebug(ratel, "---- Computing Strain Energy"));
  PetscCall(CeedEvaluatorUpdateTimeAndBoundaryValues(ratel->evaluator_residual_u, time));
  PetscCall(CeedEvaluatorApplyGlobalToLocal(ratel->evaluator_strain_energy, U, E_loc));

  // Reduce max error
  PetscCall(VecSum(E_loc, &local_strain_energy));
  PetscCall(MPIU_Allreduce(&local_strain_energy, strain_energy, 1, MPIU_REAL, MPI_SUM, ratel->comm));
  PetscCall(RatelDebug(ratel, "---- strain energy: %f", *strain_energy));

  // Restore local vector
  PetscCall(DMRestoreLocalVector(dm_energy, &E_loc));
  PetscCall(CeedEvaluatorRestoreDMs(ratel->evaluator_strain_energy, NULL, &dm_energy));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Compute Strain Energy Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Compute strain energy error.

  Collective across MPI processes.

  @param[in]  ratel  `Ratel` context
  @param[in]  time   Final time
  @param[in]  U      Final solution vector

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelViewStrainEnergyErrorFromOptions(Ratel ratel, PetscScalar time, Vec U) {
  PetscScalar computed = 0.0, expected = 0.0, error = 0.0, atol = 1e-4, rtol = 1e-4, tolerance = 0.0;
  PetscBool   has_expected = PETSC_FALSE, quiet = PETSC_FALSE, view = PETSC_FALSE;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel View Strain Energy Error From Options"));

  PetscOptionsBegin(ratel->comm, NULL, "Ratel strain energy error", NULL);
  PetscCall(PetscOptionsScalar("-strain_energy_atol", "Strain energy error absolute tolerance", NULL, atol, &atol, NULL));
  PetscCall(PetscOptionsScalar("-strain_energy_rtol", "Strain energy error relative tolerance", NULL, rtol, &rtol, NULL));
  PetscCall(PetscOptionsBool("-quiet", "Suppress summary outputs", NULL, quiet, &quiet, NULL));
  PetscCall(PetscOptionsBool("-view_strain_energy", "View final strain energy", NULL, view, &view, NULL));
  PetscOptionsEnd();

  // Get expected strain energy
  PetscCall(RatelGetExpectedStrainEnergy(ratel, &has_expected, &expected));
  if (quiet && !view && !has_expected) PetscFunctionReturn(PETSC_SUCCESS);

  // Compute strain energy
  PetscCall(RatelComputeStrainEnergy(ratel, U, time, &computed));
  if (!quiet || view) PetscCall(PetscPrintf(ratel->comm, "Computed strain energy: %0.12e\n", computed));

  // Compute strain energy error
  PetscCall(RatelComputeStrainEnergyError(ratel, computed, &has_expected, &error));

  // Set tolerance to smaller of absolute and relative tolerances
  tolerance = PetscMin(atol, rtol * (PetscAbs(expected) > PETSC_MACHINE_EPSILON ? PetscAbs(expected) : 1.0));
  if ((!quiet && has_expected) || error > tolerance) {
    // LCOV_EXCL_START
    PetscCall(PetscPrintf(ratel->comm, "Strain energy error: %0.12e\n", error));
    // LCOV_EXCL_STOP
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel View Strain Energy Error From Options Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Compute maximum values of global vector U.
    Caller is responsible for freeing `field_names` and `num_components`.

  Collective across MPI processes.

  @param[in]   ratel           `Ratel` context
  @param[in]   dm              `DM` for `U`
  @param[in]   U               Global vector
  @param[in]   time            Current solution time
  @param[out]  num_fields      Number of field
  @param[out]  field_names     Name of the fields
  @param[out]  num_components  Size of output buffer/number of components
  @param[out]  max_values      Computed max values of all fields

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelComputeMaxVectorValues(Ratel ratel, DM dm, Vec U, PetscReal time, PetscInt *num_fields, char ***field_names,
                                           PetscInt **num_components, PetscScalar max_values[]) {
  PetscInt offset = 0;
  Vec      U_loc;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Compute Max Values"));

  // Get displacement global vector
  PetscCall(DMGetLocalVector(dm, &U_loc));
  PetscCall(VecZeroEntries(U_loc));

  // Insert Dirichlet BCs
  PetscCall(DMPlexInsertBoundaryValues(dm, PETSC_TRUE, U_loc, time, NULL, NULL, NULL));
  PetscCall(DMGlobalToLocal(dm, U, INSERT_VALUES, U_loc));
  PetscCall(DMCreateFieldDecomposition(dm, num_fields, field_names, NULL, NULL));
  PetscCall(PetscCalloc1(*num_fields, num_components));
  for (PetscInt f = 0; f < *num_fields; f++) {
    PetscInt     num_comp  = 0;
    PetscScalar *max_local = NULL, *max_global = NULL;
    Vec          U_loc_field;
    IS           is_field;

    PetscCall(RatelDMGetFieldISLocal(ratel, dm, f, &num_comp, &is_field));
    (*num_components)[f] = num_comp;
    // Temporary vectors
    PetscCall(PetscCalloc1(num_comp, &max_local));
    PetscCall(PetscCalloc1(num_comp, &max_global));

    // Get displacement field subvector
    PetscCall(VecGetSubVector(U_loc, is_field, &U_loc_field));
    PetscCall(VecSetBlockSize(U_loc_field, num_comp));

    // Compute max displacements
    PetscCall(VecStrideNormAll(U_loc_field, NORM_MAX, max_local));
    PetscCall(MPIU_Allreduce(max_local, max_global, num_comp, MPIU_REAL, MPI_MAX, ratel->comm));
    // Copy over requested number of components
    for (PetscInt i = 0; i < (*num_components)[f]; i++) max_values[i + offset] = max_global[i];
    offset += (*num_components)[f];

    // Cleanup
    PetscCall(PetscFree(max_local));
    PetscCall(PetscFree(max_global));
    PetscCall(VecRestoreSubVector(U_loc, is_field, &U_loc_field));
    PetscCall(ISDestroy(&is_field));
  }

  // Cleanup
  PetscCall(DMRestoreLocalVector(dm, &U_loc));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Compute Max Values Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Compute maximum values of solution.
    Caller is responsible for freeing `field_names` and `num_components`.

  Collective across MPI processes.

  @param[in]   ratel           `Ratel` context
  @param[in]   U               Computed solution vector
  @param[in]   time            Current solution time
  @param[out]  num_fields      Number of field
  @param[out]  field_names     Name of the fields
  @param[out]  num_components  Size of output buffer/number of components
  @param[out]  max_values      Computed max values of all fields

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelComputeMaxSolutionValues(Ratel ratel, Vec U, PetscReal time, PetscInt *num_fields, char ***field_names, PetscInt **num_components,
                                             PetscScalar max_values[]) {
  DM             dm_solution;
  CeedInt        num_active_fields = 0;
  const CeedInt *active_field_sizes;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Compute Max Solution Values"));

  // DM
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));

  // Compute Max value of solution vector U
  PetscCall(RatelComputeMaxVectorValues(ratel, dm_solution, U, time, num_fields, field_names, num_components, max_values));

  // Check num_fields and components
  PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[0], &num_active_fields, &active_field_sizes));
  PetscCheck((PetscInt)num_active_fields == *num_fields, ratel->comm, PETSC_ERR_SUP,
             "Number of fields mismatched %" PetscInt_FMT " != %" PetscInt_FMT, (PetscInt)num_active_fields, *num_fields);
  for (PetscInt f = 0; f < *num_fields; f++) {
    PetscCheck((PetscInt)active_field_sizes[f] == (*num_components)[f], ratel->comm, PETSC_ERR_SUP,
               "Number of component mismatched %" PetscInt_FMT " != %" PetscInt_FMT, (PetscInt)active_field_sizes[f], (*num_components)[f]);
  }

  // Cleanup
  PetscCall(DMDestroy(&dm_solution));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Compute Max Solution Values Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Compute requested maximum diagnostic quantities from options.

  Collective across MPI processes.

  @param[in]   ratel                       `Ratel` context
  @param[in]   U                           Final solution quantities vector
  @param[in]   time                        Final time value, or `1.0` for `SNES` solution
  @param[out]  num_diagnostic_components   Number of diagnostic components
  @param[out]  diagnostic_component_names  Names of diagnostic components, shared reference, do not free
  @param[out]  max_diagnostic_quantities   Maximum diagnostic quantities

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelComputeMaxDiagnosticQuantities(Ratel ratel, Vec U, PetscScalar time, PetscInt *num_diagnostic_components,
                                                   const char *diagnostic_component_names[], PetscScalar max_diagnostic_quantities[]) {
  char        *diagnostic_component_names_arg[*num_diagnostic_components];
  PetscInt     diagnostic_component_indices[*num_diagnostic_components];
  CeedInt      num_comp_projected_diagnostic, num_comp_dual_diagnostic;
  const char **component_names_projected, **component_names_dual;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Get Max Diagnostic Quantities To View"));

  PetscOptionsBegin(ratel->comm, NULL, "Ratel diagnostic component indices", NULL);
  PetscCall(PetscOptionsStringArray("-diagnostic_components", "Names of diagnostic components for viewing max value", NULL,
                                    diagnostic_component_names_arg, num_diagnostic_components, NULL));
  PetscOptionsEnd();

  // Get diagnostic component indices
  PetscCall(RatelMaterialGetNumDiagnosticComponents(ratel->materials[0], &num_comp_projected_diagnostic, &num_comp_dual_diagnostic));
  PetscCall(RatelMaterialGetDiagnosticComponentNames(ratel->materials[0], &component_names_projected, &component_names_dual));

  for (PetscInt i = 0; i < *num_diagnostic_components; i++) {
    PetscBool is_match = PETSC_FALSE;

    for (PetscInt j = 0; j < num_comp_projected_diagnostic && !is_match; j++) {
      PetscCall(PetscStrcmp(diagnostic_component_names_arg[i], component_names_projected[j], &is_match));
      if (is_match) {
        diagnostic_component_indices[i] = j;
        diagnostic_component_names[i]   = component_names_projected[j];
      }
    }
    for (PetscInt j = 0; j < num_comp_dual_diagnostic && !is_match; j++) {
      PetscCall(PetscStrcmp(diagnostic_component_names_arg[i], component_names_dual[j], &is_match));
      if (is_match) {
        diagnostic_component_indices[i] = j + num_comp_projected_diagnostic;
        diagnostic_component_names[i]   = component_names_dual[j];
      }
    }
    PetscCheck(is_match, ratel->comm, PETSC_ERR_ARG_OUTOFRANGE, "Diagnostic component %s not found", diagnostic_component_names_arg[i]);
  }

  // Compute max diagnostics if any requested
  if (*num_diagnostic_components > 0) {
    Vec         D;
    PetscInt   *num_components, num_fields;
    PetscScalar max_diagnostic_quantities_full[128] = {0.0};

    PetscCall(RatelGetDiagnosticQuantities(ratel, U, time, &D));
    PetscCall(RatelComputeMaxVectorValues(ratel, ratel->dm_diagnostic, D, time, &num_fields, NULL, &num_components, max_diagnostic_quantities_full));

    // Check
    PetscCheck((PetscInt)num_fields == 2, ratel->comm, PETSC_ERR_SUP, "Number of fields mismatched %" PetscInt_FMT " != %" PetscInt_FMT, (PetscInt)2,
               num_fields);
    PetscCheck((PetscInt)num_comp_projected_diagnostic == num_components[0], ratel->comm, PETSC_ERR_SUP,
               "Number of projected component mismatched %" CeedInt_FMT " != %" PetscInt_FMT, num_comp_projected_diagnostic, num_components[0]);
    PetscCheck((PetscInt)num_comp_dual_diagnostic == num_components[1], ratel->comm, PETSC_ERR_SUP,
               "Number of dual component mismatched %" CeedInt_FMT " != %" PetscInt_FMT, num_comp_dual_diagnostic, num_components[1]);

    // Copy over requested components
    for (PetscInt i = 0; i < *num_diagnostic_components; i++) {
      max_diagnostic_quantities[i] = max_diagnostic_quantities_full[diagnostic_component_indices[i]];
    }

    // Cleanup
    PetscCall(RatelRestoreDiagnosticQuantities(ratel, &D));
    PetscFree(num_components);
    for (PetscInt i = 0; i < *num_diagnostic_components; i++) PetscFree(diagnostic_component_names_arg[i]);
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Get Max Diagnostic Quantities To View Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get expected maximum diagnostic quantities from options.

  Collective across MPI processes.

  @param[in]   ratel                    `Ratel` context
  @param[out]  num_expected             Number of expected diagnostic components
  @param[out]  expected_max_quantities  Expected maximum diagnostic quantities

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelGetExpectedMaxDiagnosticQuantities(Ratel ratel, PetscInt *num_expected, PetscScalar expected_max_quantities[]) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Get Expected Max Diagnostic Quantities"));

  PetscOptionsBegin(ratel->comm, NULL, "Ratel expected max diagnostic quantities", NULL);
  PetscCall(PetscOptionsRealArray("-expected_max_diagnostic", "Expected final max diagnostic", NULL, expected_max_quantities, num_expected, NULL));
  PetscOptionsEnd();

  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Compute the error between the expected and final maximum diagnostic quantities.

  Collective across MPI processes.

  @param[in]   ratel                      `Ratel` Context
  @param[in]   num_components             Number of components of the input and output arrays
  @param[in]   max_diagnostic_quantities  Computed final maximum diagnostic quantities
  @param[out]  has_any_expected           `PETSC_TRUE` if expected maximum diagnostic provided for any component
  @param[out]  max_diagnostic_error       Computed error in the final maximum diagnostic quantities

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelComputeMaxDiagnosticQuantitiesError(Ratel ratel, PetscInt num_components, const PetscScalar max_diagnostic_quantities[],
                                                        PetscBool *has_any_expected, PetscScalar max_diagnostic_error[]) {
  PetscScalar expected_max_diagnostic[num_components];
  PetscInt    num_expected = num_components;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Compute Max Diagnostic Quantities Error"));

  PetscCall(RatelGetExpectedMaxDiagnosticQuantities(ratel, &num_expected, expected_max_diagnostic));
  PetscCheck(num_expected == 0 || num_components == num_expected, ratel->comm, PETSC_ERR_SUP,
             "Number of expected diagnostic components mismatched %" PetscInt_FMT " != %" PetscInt_FMT, num_expected, num_components);
  *has_any_expected = num_expected > 0;
  for (PetscInt i = 0; i < num_components; i++) {
    max_diagnostic_error[i] = has_any_expected ? PetscAbsScalar(expected_max_diagnostic[i] - max_diagnostic_quantities[i]) : 0.0;
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Compute Max Diagnostic Quantities Error Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Compute maximum values of diagnostic by name from options.

  Collective across MPI processes.

  @note This function may compute the diagnostic quantities if they are not already computed for this timestep.

  @param[in]  ratel  `Ratel` context
  @param[in]  time   Final time
  @param[in]  U      Final solution vector

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelViewMaxDiagnosticQuantitiesErrorByNameFromOptions(Ratel ratel, PetscReal time, Vec U) {
  PetscInt    num_diagnostic_components = 128, num_expected = 128;
  const char *diagnostic_component_names[num_diagnostic_components];
  PetscBool   has_expected_max_diagnostics = PETSC_FALSE, quiet = PETSC_FALSE;
  PetscScalar expected_max_diagnostic[128] = {0.0}, max_diagnostic_error[128] = {0.0}, max_diagnostic_quantities[128] = {0.0};
  PetscScalar rtol = 1e-4, atol = 1e-4;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Compute Max Diagnostic Value"));

  PetscOptionsBegin(ratel->comm, NULL, "Ratel diagnostic component names and expected values", NULL);
  PetscCall(PetscOptionsScalar("-diagnostic_components_atol", "Max diagnostic components error absolute tolerance", NULL, atol, &atol, NULL));
  PetscCall(PetscOptionsScalar("-diagnostic_components_rtol", "Max diagnostic components error relative tolerance", NULL, rtol, &rtol, NULL));
  PetscCall(PetscOptionsBool("-quiet", "Suppress summary outputs", NULL, quiet, &quiet, NULL));
  PetscOptionsEnd();

  // Get diagnostic component indices
  PetscCall(RatelComputeMaxDiagnosticQuantities(ratel, U, time, &num_diagnostic_components, diagnostic_component_names, max_diagnostic_quantities));

  if (num_diagnostic_components <= 0) PetscFunctionReturn(PETSC_SUCCESS);

  // Compute errors
  PetscCall(RatelComputeMaxDiagnosticQuantitiesError(ratel, num_diagnostic_components, max_diagnostic_quantities, &has_expected_max_diagnostics,
                                                     max_diagnostic_error));

  // Get expected max diagnostic quantities, if any
  PetscCall(RatelGetExpectedMaxDiagnosticQuantities(ratel, &num_expected, expected_max_diagnostic));

  // View max diagnostics
  for (PetscInt i = 0; i < num_diagnostic_components; i++) {
    const char *name = diagnostic_component_names[i];

    if (!quiet) PetscCall(PetscPrintf(ratel->comm, "Max diagnostic %s: %0.12g\n", name, max_diagnostic_quantities[i]));
    if (has_expected_max_diagnostics) {
      PetscScalar scaled_expected = PetscAbs(expected_max_diagnostic[i]) > PETSC_MACHINE_EPSILON ? PetscAbs(expected_max_diagnostic[i]) : 1.0;
      PetscScalar tolerance       = PetscMin(atol, rtol * scaled_expected);
      if (!quiet || max_diagnostic_error[i] > tolerance) {
        // LCOV_EXCL_START
        PetscCall(PetscPrintf(ratel->comm, "Max diagnostic %s error: %0.12g\n", name, max_diagnostic_error[i]));
        // LCOV_EXCL_STOP
      }
    }
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Compute Max Diagnostic Value Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief View the computed volume and error from options.

  Collective across MPI processes.

  @param[in]  ratel  `Ratel` context
  @param[in]  time   Current solution time
  @param[in]  U      Current solution vector

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelViewVolumeErrorFromOptions(Ratel ratel, PetscReal time, Vec U) {
  PetscScalar volume = 0.0, expected_volume = 1.0, rtol = 1e-4, atol = 1e-4;
  PetscBool   quiet = PETSC_FALSE, view = PETSC_FALSE, set = PETSC_FALSE;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel View Volume Error From Options"));

  PetscOptionsBegin(ratel->comm, NULL, "Ratel volume error", NULL);
  PetscCall(PetscOptionsScalar("-volume_atol", "Volume error absolute tolerance", NULL, atol, &atol, NULL));
  PetscCall(PetscOptionsScalar("-volume_rtol", "Volume error relative tolerance", NULL, rtol, &rtol, NULL));
  PetscCall(PetscOptionsScalar("-expected_volume", "Expected final volume", NULL, expected_volume, &expected_volume, &set));
  PetscCall(PetscOptionsBool("-quiet", "Suppress summary outputs", NULL, quiet, &quiet, NULL));
  PetscCall(PetscOptionsBool("-view_volume", "View final volume", NULL, view, &view, NULL));
  PetscOptionsEnd();

  // Compute volume error
  PetscCall(RatelComputeVolume(ratel, U, time, &volume));
  PetscScalar volume_error = set ? PetscAbs(expected_volume - volume) : 0.0;
  if (!quiet || view) PetscCall(PetscPrintf(ratel->comm, "Computed volume: %0.12g\n", volume));

  // Check volume error
  PetscScalar tolerance = PetscMax(atol, rtol * PetscAbs(expected_volume));
  if ((!quiet && view) || volume_error > tolerance) PetscCall(PetscPrintf(ratel->comm, "Volume error:    %0.12g\n", volume_error));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel View Volume Error From Options Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief View the diagnostic quantities vector from options.

  Collective across MPI processes.

  @note This function may compute the diagnostic quantities if they are not already computed for this timestep.

  @param[in]  ratel  `Ratel` context
  @param[in]  time   Current time
  @param[in]  U      Current solution vector

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelViewDiagnosticQuantitiesFromOptions(Ratel ratel, PetscReal time, Vec U) {
  PetscBool view = PETSC_FALSE;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel View Diagnostic Quantities From Options"));

  PetscOptionsBegin(ratel->comm, NULL, "Ratel diagnostic quantities", NULL);
  PetscCall(PetscOptionsHasName(NULL, NULL, "-view_diagnostic_quantities", &view));
  PetscOptionsEnd();

  if (view) {
    Vec D;

    PetscCall(RatelGetDiagnosticQuantities(ratel, U, time, &D));
    PetscCall(VecViewFromOptions(D, NULL, "-view_diagnostic_quantities"));
    PetscCall(RatelRestoreDiagnosticQuantities(ratel, &D));
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel View Diagnostic Quantities From Options Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief List mesh face numbers for diagnostic force computation.

  Not collective across MPI processes.

  @param[in]   ratel      `Ratel` context
  @param[out]  num_faces  Number of faces where forces are computed
  @param[out]  faces      `DMPlex` mesh face numbers

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelGetSurfaceForceFaces(Ratel ratel, PetscInt *num_faces, const char **faces[]) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Get Surface Force Faces"));

  *num_faces = ratel->surface_force_face_count;
  *faces     = (const char **)ratel->surface_force_face_names;

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Get Surface Force Faces Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Compute surface forces on mesh faces.

  Note: The component `i` of the force for face `f` is at index `surface_forces[num_comp * f + i]`.

  Collective across MPI processes.

  @param[in]   ratel           `Ratel` context
  @param[in]   U               Computed solution vector
  @param[in]   time            Final time value, or `1.0` for `SNES` solution
  @param[out]  num_components  Number of force components in output buffer
  @param[out]  surface_forces  Computed face forces - caller is responsible for freeing

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelComputeSurfaceForcesCellToFace(Ratel ratel, Vec U, PetscReal time, PetscInt *num_components, PetscScalar **surface_forces) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Compute Surface Forces"));

  // Get number of solution components
  {
    const CeedInt *field_sizes;

    PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[0], NULL, &field_sizes));
    *num_components = field_sizes[0];  // First field should be displacement
  }

  // Work array
  PetscCall(RatelDebug(ratel, "---- Creating surface force array"));
  PetscCall(PetscCalloc1(ratel->surface_force_face_count * (*num_components), surface_forces));

  // Internal routine
  PetscCall(RatelComputeSurfaceForcesCellToFace_Internal(ratel, U, time, *surface_forces));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Compute Surface Forces Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Compute surface forces on mesh faces.

  Collective across MPI processes.

  @param[in]   ratel           `Ratel` context
  @param[in]   U               Computed solution vector
  @param[in]   time            Final time value, or `1.0` for `SNES` solution
  @param[out]  num_components  Number of force components in output buffer
  @param[out]  surface_forces  Computed face surface forces - caller is responsible for freeing

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelComputeSurfaceForces(Ratel ratel, Vec U, PetscReal time, PetscInt *num_components, PetscScalar **surface_forces) {
  DM dm_solution;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Compute Surface Forces"));

  // Get number of solution components
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));
  PetscCall(DMGetDimension(dm_solution, num_components));

  // Work array
  PetscCall(RatelDebug(ratel, "---- Creating surface force array"));
  PetscCall(PetscCalloc1(ratel->surface_force_face_count * (*num_components), surface_forces));

  // Internal routine
  PetscCall(RatelComputeSurfaceForces_Internal(ratel, U, time, *surface_forces));

  // Cleanup
  PetscCall(DMDestroy(&dm_solution));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Compute Surface Forces Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Compute centroids of mesh faces

  Collective across MPI processes.

  @param[in]   ratel              Ratel context
  @param[in]   U                  Computed solution vector
  @param[in]   time               Final time value, or `1.0` for `SNES` solution
  @param[out]  num_components     Number of centroid components in output buffer
  @param[out]  surface_centroids  Computed surface centroids - caller is responsible for freeing

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelComputeSurfaceCentroids(Ratel ratel, Vec U, PetscReal time, PetscInt *num_components, PetscScalar **surface_centroids) {
  DM dm_solution;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Compute Surface Centroids"));

  // Get number of solution components
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));
  PetscCall(DMGetDimension(dm_solution, num_components));

  // Work array
  PetscCall(RatelDebug(ratel, "---- Creating centroid array"));
  PetscCall(PetscCalloc1(ratel->surface_force_face_count * (*num_components), surface_centroids));

  // Internal routine
  PetscCall(RatelComputeSurfaceCentroids_Internal(ratel, U, time, *surface_centroids));

  // Cleanup
  PetscCall(DMDestroy(&dm_solution));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Compute Surface Centroids Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Compute the total volume of the mesh using nodal diagnostics.

  Collective across MPI processes.

  @param[in]   ratel   `Ratel` context
  @param[in]   U       Current solution vector
  @param[in]   time    Current time value
  @param[out]  volume  Computed volume

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelComputeVolume(Ratel ratel, Vec U, PetscReal time, PetscScalar *volume) {
  Vec            D, D_dual;
  const PetscInt nodal_volume_index = 0;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Compute Volume"));

  // Get diagnostic quantities vector
  PetscCall(RatelGetDiagnosticQuantities(ratel, U, time, &D));

  // Get dual diagnostics
  PetscCall(VecGetSubVector(D, ratel->is_sub_dms_diagnostic[RATEL_DIAGNOSTIC_DUAL], &D_dual));

  // Compute nodal volume
  PetscCall(RatelDebug(ratel, "---- Computing Volume"));
  PetscCall(VecStrideSum(D_dual, nodal_volume_index, volume));

  // Cleanup
  PetscCall(VecRestoreSubVector(D, ratel->is_sub_dms_diagnostic[RATEL_DIAGNOSTIC_DUAL], &D_dual));
  PetscCall(RatelRestoreDiagnosticQuantities(ratel, &D));
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Compute Volume Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get the diagnostic quantities vector.

  Collective across MPI processes.

  @note The user is responsible for restoring the vector with `RatelRestoreDiagnosticQuantities()`.

  @param[in]   ratel  `Ratel` context
  @param[in]   U      Computed solution vector
  @param[in]   time   Current time value
  @param[out]  D      Diagnostic quantities vector

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelGetDiagnosticQuantities(Ratel ratel, Vec U, PetscReal time, Vec *D) {
  PetscReal old_time;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Get Diagnostic Quantities"));

  // Setup contexts and DMs if needed
  // Zach: I don't like that the setup of ratel->dm_diagnostic is implicit here
  if (!ratel->ksp_diagnostic_projection) {
    PetscCall(RatelSetupDiagnosticEvaluators(ratel, &ratel->ksp_diagnostic_projection, &ratel->evaluator_projected_diagnostic,
                                             &ratel->evaluator_dual_diagnostic, &ratel->evaluator_dual_nodal_scale));
  }

  // Get or create diagnostic quantities vector
  PetscCall(DMGetNamedGlobalVector(ratel->dm_diagnostic, "diagnostic_quantities", D));
  PetscCall(PetscObjectSetName((PetscObject)*D, "diagnostic_quantities"));

  // Only recompute if time has changed
  PetscCall(CeedEvaluatorGetTime(ratel->evaluator_projected_diagnostic, &old_time));
  if (!PetscEqualReal(old_time, time)) PetscCall(RatelComputeDiagnosticQuantities_Internal(ratel, U, time, *D));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Get Diagnostic Quantities Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Restore the diagnostic quantities vector.

  Collective across MPI processes.

  @param[in]   ratel  `Ratel` context
  @param[out]  D      Diagnostic quantities vector

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelRestoreDiagnosticQuantities(Ratel ratel, Vec *D) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Restore Diagnostic Quantities"));
  PetscCall(DMRestoreNamedGlobalVector(ratel->dm_diagnostic, "diagnostic_quantities", D));
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Restore Diagnostic Quantities Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}
