/// @file
/// Hyperelastic material at finite strain using Neo-Hookean model in initial configuration

#include <ceed.h>
#include <petscsys.h>
#include <ratel-impl.h>
#include <ratel-model.h>
#include <ratel.h>
#include <ratel/qfunctions/geometry/surface-force.h>
#include <ratel/qfunctions/geometry/surface.h>
#include <ratel/qfunctions/geometry/volumetric.h>
#include <ratel/qfunctions/models/elasticity-common-utt.h>
#include <ratel/qfunctions/models/elasticity-dual-diagnostic.h>
#include <ratel/qfunctions/models/elasticity-neo-hookean-initial-ad-enzyme.h>

static const CeedInt      active_field_sizes[]          = {3};
static const char *const  active_field_names[]          = {"displacement"};
static const char *const  active_component_names[]      = {"u_x", "u_y", "u_z"};
static const CeedInt      active_field_num_eval_modes[] = {NUM_ACTIVE_FIELD_EVAL_MODES_NeoHookeanInitial_AD_Enzyme};
static const CeedEvalMode active_field_eval_modes[]     = {CEED_EVAL_GRAD};
static const CeedInt      utt_field_num_eval_modes[]    = {1};
static const CeedEvalMode utt_field_eval_modes[]        = {CEED_EVAL_INTERP};

static const char *const projected_diagnostic_component_names[] = {"displacement_x",
                                                                   "displacement_y",
                                                                   "displacement_z",
                                                                   "Cauchy_stress_xx",
                                                                   "Cauchy_stress_xy",
                                                                   "Cauchy_stress_xz",
                                                                   "Cauchy_stress_yy",
                                                                   "Cauchy_stress_yz",
                                                                   "Cauchy_stress_zz",
                                                                   "pressure",
                                                                   "volumetric_strain",
                                                                   "trace_E2",
                                                                   "J",
                                                                   "strain_energy_density",
                                                                   "von_Mises_stress",
                                                                   "mass_density"};

struct RatelModelData_private elasticity_Neo_Hookean_initial_ad_data_private = {
    .name                                 = "Neo-Hookean hyperelasticity at finite strain, in initial configuration using Enzyme-AD",
    .command_line_option                  = "elasticity-neo-hookean-initial-ad",
    .setup_q_data_volume                  = SetupVolumeGeometry,
    .setup_q_data_volume_loc              = SetupVolumeGeometry_loc,
    .setup_q_data_surface                 = SetupSurfaceGeometry,
    .setup_q_data_surface_loc             = SetupSurfaceGeometry_loc,
    .setup_q_data_surface_grad            = SetupSurfaceForceGeometry,
    .setup_q_data_surface_grad_loc        = SetupSurfaceForceGeometry_loc,
    .q_data_volume_size                   = Q_DATA_VOLUMETRIC_GEOMETRY_SIZE,
    .q_data_surface_size                  = Q_DATA_SURFACE_GEOMETRY_SIZE,
    .q_data_surface_grad_size             = Q_DATA_SURFACE_FORCE_GEOMETRY_SIZE,
    .num_forcing                          = 1,
    .num_active_fields                    = sizeof(active_field_sizes) / sizeof(*active_field_sizes),
    .active_field_sizes                   = active_field_sizes,
    .active_field_names                   = active_field_names,
    .active_component_names               = active_component_names,
    .active_field_num_eval_modes          = active_field_num_eval_modes,
    .active_field_eval_modes              = active_field_eval_modes,
    .utt_field_num_eval_modes             = utt_field_num_eval_modes,
    .utt_field_eval_modes                 = utt_field_eval_modes,
    .num_comp_stored_u                    = NUM_COMPONENTS_STORED_NeoHookeanInitial_AD_Enzyme,
    .num_comp_projected_diagnostic        = NUM_COMPONENTS_DIAGNOSTIC_Elasticity,
    .projected_diagnostic_component_names = projected_diagnostic_component_names,
    .num_comp_dual_diagnostic             = NUM_COMPONENTS_ELASTICITY_DIAGNOSTIC_Dual,
    .dual_diagnostic_component_names      = RatelElasticityDualDiagnosticComponentNames,
    .quadrature_mode                      = CEED_GAUSS,
    .residual_u                           = ElasticityResidual_NeoHookeanInitial_AD_Enzyme,
    .residual_u_loc                       = ElasticityResidual_NeoHookeanInitial_AD_Enzyme_loc,
    .residual_utt                         = ElasticityResidual_utt,
    .residual_utt_loc                     = ElasticityResidual_utt_loc,
    .jacobian                             = ElasticityJacobian_NeoHookeanInitial_AD_Enzyme,
    .jacobian_loc                         = ElasticityJacobian_NeoHookeanInitial_AD_Enzyme_loc,
    .strain_energy                        = StrainEnergy_NeoHookean,
    .strain_energy_loc                    = StrainEnergy_NeoHookean_loc,
    .projected_diagnostic                 = Diagnostic_NeoHookean,
    .projected_diagnostic_loc             = Diagnostic_NeoHookean_loc,
    .dual_diagnostic                      = ElasticityDualDiagnostic,
    .dual_diagnostic_loc                  = ElasticityDualDiagnostic_loc,
    .flops_qf_jacobian_u                  = 802,
    .flops_qf_jacobian_utt                = 9,
};
RatelModelData elasticity_Neo_Hookean_initial_ad_data = &elasticity_Neo_Hookean_initial_ad_data_private;

/// @addtogroup RatelMaterials
/// @{

/**
@brief Helper function to check allocated Enzyme tape size

@param[in] ratel Ratel context

@return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelCheckTapeSize_ElasticityNeoHookeanInitialAD_Enzyme(Ratel ratel) {
  PetscInt tape_scalars_allocated, tape_scalars_needed;

  PetscFunctionBeginUser;
  tape_scalars_needed =
      __enzyme_augmentsize((void *)SecondPiolaKirchhoffStress_NeoHookean_AD, enzyme_dup, enzyme_dup, enzyme_const, enzyme_const) / sizeof(CeedScalar);
  tape_scalars_allocated = RATEL_ELASTICITY_NEO_HOOKEAN_INITIAL_AD_TAPE_SIZE;

  if (tape_scalars_allocated > tape_scalars_needed) {
    PetscCall(PetscPrintf(ratel->comm, "Warning: allocated %" PetscInt_FMT " CeedScalars for Enzyme tape when %" PetscInt_FMT " needed\n",
                          tape_scalars_allocated, tape_scalars_needed));
  }
  PetscCheck(tape_scalars_allocated >= tape_scalars_needed, ratel->comm, PETSC_ERR_SUP,
             "Error: allocated %" PetscInt_FMT " CeedScalars for Enzyme tape when %" PetscInt_FMT " needed\n", tape_scalars_allocated,
             tape_scalars_needed);
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Create `RatelMaterial` model data for Neo-Hookean hyperelasticity at finite strain in initial configuration.

  Not collective across MPI processes.

  @param[in]   ratel     `Ratel` context
  @param[out]  material  `RatelMaterial` context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialCreate_ElasticityNeoHookeanInitialAD_Enzyme(Ratel ratel, RatelMaterial material) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Create Elasticity Neo-Hookean Initial AD"));

  // Tape size check
  PetscCall(RatelCheckTapeSize_ElasticityNeoHookeanInitialAD_Enzyme(ratel));

  // Model data
  PetscCall(RatelModelDataVerifyRelativePath(ratel, elasticity_Neo_Hookean_initial_ad_data));
  material->model_data = elasticity_Neo_Hookean_initial_ad_data;

  // QFunction contexts
  PetscCall(RatelMaterialParamsContextCreate_NeoHookean(material, &material->ctx_params));
  PetscCall(RatelMaterialParamsSmootherDataSetup_NeoHookean(material));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Create Elasticity Neo-Hookean Initial AD Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
@brief Register Neo-Hookean hyperelasticity at finite strain in initial configuration using Enzyme-AD model.

  Not collective across MPI processes.

  @param[in]   ratel                      `Ratel` context
  @param[in]   cl_argument                Command line argument to use for model
  @param[out]  material_create_functions  PETSc function list for `RatelMaterial`

@return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelRegisterModel_ElasticityNeoHookeanInitialAD_Enzyme(Ratel ratel, const char *cl_argument,
                                                                       PetscFunctionList *material_create_functions) {
  PetscFunctionBeginUser;
  RATEL_MODEL_REGISTER(material_create_functions, cl_argument, ElasticityNeoHookeanInitialAD_Enzyme);
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}
