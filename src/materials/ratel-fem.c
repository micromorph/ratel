/// @file
/// FEM material DM and operator setup

#include <ceed-evaluator.h>
#include <ceed.h>
#include <ceed/backend.h>
#include <mat-ceed.h>
#include <petsc-ceed-utils.h>
#include <petscdmplex.h>
#include <petscds.h>
#include <petscsys.h>
#include <ratel-boundary.h>
#include <ratel-dm.h>
#include <ratel-fem.h>
#include <ratel-impl.h>
#include <ratel-material.h>
#include <ratel.h>
#include <ratel/models/scaled-mass.h>
#include <ratel/qfunctions/forcing/body.h>
#include <ratel/qfunctions/mass.h>
#include <ratel/qfunctions/scale-lumped-dual-diagnostic-terms.h>
#include <stddef.h>
#include <stdio.h>
#include <string.h>

/// @addtogroup RatelMaterials
/// @{

/**
  @brief Create a `RatelMaterial` object for a finite element model

  @param[in]   ratel     `Ratel` context
  @param[out]  material  `RatelMaterial` context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialCreate_FEM(Ratel ratel, RatelMaterial material) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material FEM Create"));

  // -- Geometry
  material->SetupVolumeQData          = RatelMaterialSetupVolumeQData_FEM;
  material->SetupSurfaceGradientQData = RatelMaterialSetupSurfaceGradientQData_FEM;

  // -- Residual and Jacobian operators
  material->SetupResidualSuboperators     = RatelMaterialSetupResidualSuboperators_FEM;
  material->SetupJacobianSuboperator      = RatelMaterialSetupJacobianSuboperator_FEM;
  material->SetupJacobianBlockSuboperator = RatelMaterialSetupJacobianBlockSuboperator_FEM;

  // -- Output operators
  material->SetupStrainEnergySuboperator            = RatelMaterialSetupStrainEnergySuboperator_FEM;
  material->SetupDiagnosticSuboperators             = RatelMaterialSetupDiagnosticSuboperators_FEM;
  material->SetupSurfaceForceCellToFaceSuboperators = RatelMaterialSetupSurfaceForceCellToFaceSuboperators_FEM;
  material->SetupMMSErrorSuboperator                = RatelMaterialSetupMMSErrorSuboperator_FEM;

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Create FEM Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup base and fine grid `DM`

  @param[in,out]  ratel        `Ratel` context
  @param[out]     dm_solution  Fine grid FEM `DM`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSolutionDMSetup_FEM(Ratel ratel, DM *dm_solution) {
  VecType        vec_type;
  CeedMemType    mem_type_backend;
  CeedInt        num_active_fields = 0;
  const CeedInt *active_field_sizes;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Solution DM Setup FEM"));

  // Get VecType
  RatelCallCeed(ratel, CeedGetPreferredMemType(ratel->ceed, &mem_type_backend));
  PetscCall(RatelDebug(ratel, "---- Memory type: %s", CeedMemTypes[mem_type_backend]));
  switch (mem_type_backend) {
    case CEED_MEM_HOST:
      vec_type = VECSTANDARD;
      break;
    case CEED_MEM_DEVICE: {
      const char *ceed_resource;

      RatelCallCeed(ratel, CeedGetResource(ratel->ceed, &ceed_resource));
      if (strstr(ceed_resource, "/gpu/cuda")) vec_type = VECCUDA;
      else if (strstr(ceed_resource, "/gpu/hip")) vec_type = VECKOKKOS;
      else vec_type = VECSTANDARD;
    }
  }
  ratel->is_ceed_backend_gpu = mem_type_backend == CEED_MEM_DEVICE;

  // Determine active fields
  {
    PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[0], &num_active_fields, &active_field_sizes));
    for (PetscInt i = 1; i < ratel->num_materials; i++) {
      CeedInt        num_active_fields_current = 0;
      const CeedInt *active_field_sizes_current;

      PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[i], &num_active_fields_current, &active_field_sizes_current));
      PetscCheck(num_active_fields == num_active_fields_current, ratel->comm, PETSC_ERR_LIB, "Mixed number of active fields not supported");
      for (PetscInt j = 0; j < num_active_fields_current; j++) {
        PetscCheck(active_field_sizes[j] == active_field_sizes_current[j], ratel->comm, PETSC_ERR_LIB, "Mixed active field sizes not supported");
      }
    }
  }
  PetscCheck(num_active_fields == ratel->num_active_fields, ratel->comm, PETSC_ERR_LIB, "Insufficient field orders provided");

  // Setup DM
  // -- Parallel distributed DM with CL options
  PetscCall(RatelDMPlexCreateFromOptions(ratel, vec_type, dm_solution));
  {
    PetscInt cStart, cEnd, count;

    PetscCall(DMPlexGetHeightStratum(*dm_solution, 0, &cStart, &cEnd));
    count = cEnd - cStart;
    PetscCall(MPI_Allreduce(MPI_IN_PLACE, &count, 1, MPIU_INT, MPI_SUM, ratel->comm));
    PetscCheck(count >= 1, ratel->comm, PETSC_ERR_USER, "Cannot use DM with %" PetscInt_FMT " cells; check -dm_plex_filename or -dm_plex_box_faces",
               count);
  }
  PetscCall(DMGetVecType(*dm_solution, &vec_type));

  // -- Volumetric domain labels
  {
    PetscBool has_label = PETSC_FALSE;

    // ---- Check if label exists
    PetscCall(DMHasLabel(*dm_solution, ratel->material_volume_label_name, &has_label));
    if (!has_label && ratel->num_materials == 1) {
      // ---- Create label with all elements in one material
      PetscInt c_start, c_end, *label_values;

      // ------ Set "Cell Sets" value to 1 if the label doesn't exist and the user didn't request a value
      PetscCall(RatelMaterialGetVolumeLabelValues(ratel->materials[0], NULL, &label_values));
      if (label_values[0] == -1) PetscCall(RatelMaterialSetVolumeLabelValue(ratel->materials[0], 1));

      // ------ Add all elements to this new label
      PetscCall(DMCreateLabel(*dm_solution, ratel->material_volume_label_name));
      PetscCall(DMPlexGetHeightStratum(*dm_solution, 0, &c_start, &c_end));
      for (PetscInt c = c_start; c < c_end; c++) PetscCall(DMSetLabelValue(*dm_solution, ratel->material_volume_label_name, c, label_values[0]));
      has_label = PETSC_TRUE;
    } else if (ratel->num_materials == 1) {
      // ---- Check label value for single material
      PetscInt *label_values;

      PetscCall(RatelMaterialGetVolumeLabelValues(ratel->materials[0], NULL, &label_values));
      // ------ Get the mesh label value if user did not provide one
      if (label_values[0] == -1) {
        PetscInt num_values_known;
        IS       id_is;

        PetscCall(DMGetLabelIdIS(*dm_solution, ratel->material_volume_label_name, &id_is));
        PetscCall(ISGetSize(id_is, &num_values_known));
        if (num_values_known == 1) {
          const PetscInt *ids;

          PetscCall(ISGetIndices(id_is, &ids));
          PetscCall(RatelMaterialSetVolumeLabelValue(ratel->materials[0], ids[0]));
          PetscCall(ISRestoreIndices(id_is, &ids));
        }
        PetscCall(ISDestroy(&id_is));
      }
    }
    PetscCheck(has_label || ratel->num_materials == 1, ratel->comm, PETSC_ERR_SUP, "Domain label doesn't exist for multiple materials!");
  }

  // -- Boundary domain labels
  // ---- Add labels on DM faces for FE face numbers for surface force computation
  for (PetscInt i = 0; i < ratel->num_materials; i++) {
    RatelMaterial material = ratel->materials[i];

    material->num_surface_grad_diagnostic_dm_face_ids = ratel->surface_force_face_count;
    PetscCall(PetscCalloc1(material->num_surface_grad_diagnostic_dm_face_ids, &material->surface_grad_diagnostic_dm_face_ids));
    PetscCall(PetscCalloc1(material->num_surface_grad_diagnostic_dm_face_ids, &material->surface_grad_diagnostic_label_names));
    PetscCall(PetscCalloc1(material->num_surface_grad_diagnostic_dm_face_ids, &material->q_data_surface_grad_diagnostic));
    PetscCall(PetscCalloc1(material->num_surface_grad_diagnostic_dm_face_ids, &material->restrictions_q_data_surface_grad_diagnostic));
    for (PetscInt j = 0; j < ratel->surface_force_face_count; j++) {
      PetscInt num_face_orientations;

      material->surface_grad_diagnostic_dm_face_ids[j] = ratel->surface_force_face_label_values[j];
      PetscCall(RatelDMPlexCreateFaceLabel(ratel, *dm_solution, material, material->surface_grad_diagnostic_dm_face_ids[j], &num_face_orientations,
                                           &material->surface_grad_diagnostic_label_names[j]));
      if (num_face_orientations != 0 && material->num_face_orientations == -1) {
        material->num_face_orientations = num_face_orientations;
      }
      PetscCheck(num_face_orientations == 0 || material->num_face_orientations == num_face_orientations, ratel->comm, PETSC_ERR_SUP,
                 "Conflicting numbers of face orientations for material %s", material->name);
    }
    for (PetscInt j = 0; j < ratel->surface_force_face_count; j++) {
      PetscCall(PetscCalloc1(material->num_face_orientations, &material->q_data_surface_grad_diagnostic[j]));
      PetscCall(PetscCalloc1(material->num_face_orientations, &material->restrictions_q_data_surface_grad_diagnostic[j]));
    }
    // ---- Add labels on DM faces for FE face numbers for platen boundary condition
    material->num_surface_grad_dm_face_ids = ratel->bc_platen_count;
    PetscCall(PetscCalloc1(material->num_surface_grad_dm_face_ids, &material->surface_grad_dm_face_ids));
    PetscCall(PetscCalloc1(material->num_surface_grad_dm_face_ids, &material->surface_grad_label_names));
    PetscCall(PetscCalloc1(material->num_surface_grad_dm_face_ids, &material->q_data_surface_grad));
    PetscCall(PetscCalloc1(material->num_surface_grad_dm_face_ids, &material->restrictions_q_data_surface_grad));
    for (PetscInt j = 0; j < ratel->bc_platen_count; j++) {
      PetscInt num_face_orientations;

      material->surface_grad_dm_face_ids[j] = ratel->bc_platen_label_values[j];
      PetscCall(RatelDMPlexCreateFaceLabel(ratel, *dm_solution, material, material->surface_grad_dm_face_ids[j], &num_face_orientations,
                                           &material->surface_grad_label_names[j]));
      if (num_face_orientations != 0 && material->num_face_orientations == -1) {
        material->num_face_orientations = num_face_orientations;
      }
      PetscCheck(num_face_orientations == 0 || material->num_face_orientations == num_face_orientations, ratel->comm, PETSC_ERR_SUP,
                 "Conflicting numbers of face orientations for material %s", material->name);
    }
    for (PetscInt j = 0; j < ratel->bc_platen_count; j++) {
      PetscCall(PetscCalloc1(material->num_face_orientations, &material->q_data_surface_grad[j]));
      PetscCall(PetscCalloc1(material->num_face_orientations, &material->restrictions_q_data_surface_grad[j]));
    }
  }

  // -- Default MatType
  {
    char      mat_type_cl[PETSC_MAX_PATH_LEN], pc_type_cl[PETSC_MAX_PATH_LEN] = "";
    PetscBool is_mat_type_cl = PETSC_FALSE;
    MatType   mat_type       = MATSHELL;

    PetscOptionsBegin(ratel->comm, NULL, "", NULL);
    PetscCall(PetscOptionsString("-pc_type", "pc type", NULL, pc_type_cl, pc_type_cl, sizeof(pc_type_cl), NULL));
    PetscCall(PetscOptionsString("-dm_mat_type", "fine grid DM MatType", NULL, mat_type_cl, mat_type_cl, sizeof(mat_type_cl), &is_mat_type_cl));
    PetscOptionsEnd();

    // Default to assembled matrix for some PC types
    if (strstr(pc_type_cl, PCGAMG) || strstr(pc_type_cl, PCHYPRE) || strstr(pc_type_cl, PCLU) || strstr(pc_type_cl, PCQR) ||
        strstr(pc_type_cl, PCSVD)) {
      if (strstr(vec_type, VECCUDA)) mat_type = MATAIJCUSPARSE;
      else if (strstr(vec_type, VECKOKKOS)) mat_type = MATAIJKOKKOS;
      else mat_type = MATAIJ;
    }

    mat_type = is_mat_type_cl ? mat_type_cl : mat_type;
    PetscCall(DMSetMatType(*dm_solution, mat_type));
    if (strstr(mat_type, MATSHELL)) PetscCall(DMSetMatrixPreallocateSkip(*dm_solution, PETSC_TRUE));
  }

  // -- FE spaces for residual and Jacobian DM
  {
    const char **active_field_names, **active_component_names;
    PetscInt     field_orders[RATEL_MAX_FIELDS] = {0};

    PetscCall(RatelMaterialGetActiveFieldNames(ratel->materials[0], &active_field_names, &active_component_names));
    for (PetscInt i = 0; i < ratel->num_active_fields; i++) field_orders[i] = ratel->fine_grid_orders[i];
    PetscCall(RatelDMSetupByOrder_FEM(ratel, PETSC_TRUE, PETSC_FALSE, field_orders, RATEL_DECIDE, PETSC_TRUE, ratel->q_extra, num_active_fields,
                                      active_field_sizes, active_field_names, active_component_names, *dm_solution));
  }

  // -- Apply 3D Kershaw mesh transformation
  {
    PetscScalar kershaw_eps  = 1.0;  // 1.0 recovers uniform mesh
    PetscInt    box_faces[3] = {1, 1, 1}, num_face_dims = 3;
    PetscBool   set_kershaw_eps = PETSC_FALSE, set_box_mesh = PETSC_FALSE;
    PetscReal   box_faces_lower[3] = {0., 0., 0.}, box_faces_upper[3] = {1., 1., 1.};

    PetscOptionsBegin(ratel->comm, NULL, "Setup Kershaw mesh transformation", NULL);
    PetscCall(
        PetscOptionsScalar("-kershaw_eps", "Epsilon parameter for Kershaw mesh transformation", NULL, kershaw_eps, &kershaw_eps, &set_kershaw_eps));
    PetscCall(PetscOptionsIntArray("-dm_plex_box_faces", "DMPLex box mesh", NULL, box_faces, &num_face_dims, &set_box_mesh));
    PetscCall(PetscOptionsRealArray("-dm_plex_box_lower", "Lower-left-bottom coordinates for the box", NULL, box_faces_lower, &num_face_dims, NULL));
    num_face_dims = 3;
    PetscCall(PetscOptionsRealArray("-dm_plex_box_upper", "Upper-right-top coordinates for the box", NULL, box_faces_upper, &num_face_dims, NULL));
    num_face_dims = 3;
    PetscOptionsEnd();

    PetscCheck(kershaw_eps > 0 && kershaw_eps <= 1.0, ratel->comm, PETSC_ERR_SUP, "kershaw_eps must be in (0, 1.0]");
    PetscCheck(!set_kershaw_eps || set_box_mesh, ratel->comm, PETSC_ERR_SUP, "Kershaw mesh transformation only supported with DMPlex box mesh");
    if (set_kershaw_eps) {
      for (PetscInt i = 0; i < num_face_dims; i++) {
        PetscCheck(box_faces_lower[i] == 0.0 && box_faces_upper[i] == 1.0, ratel->comm, PETSC_ERR_SUP,
                   "DMPlex box mesh must be [0, 1]^3 to use Kershaw transformation");
      }
      PetscCall(RatelKershaw(*dm_solution, kershaw_eps));
    }
  }

  // -- Copy labels to coordinate DM
  {
    DM dm_coord;

    PetscCall(DMGetCoordinateDM(*dm_solution, &dm_coord));
    PetscCall(DMCopyLabels(*dm_solution, dm_coord, PETSC_OWN_POINTER, PETSC_FALSE, DM_COPY_LABELS_KEEP));
  }

  // View final result
  PetscCall(PetscObjectSetName((PetscObject)*dm_solution, "Solution"));
  PetscCall(DMViewFromOptions(*dm_solution, NULL, "-dm_view"));
  {
    DM dm_coord;

    PetscCall(DMGetCoordinateDM(*dm_solution, &dm_coord));
    PetscCall(DMViewFromOptions(dm_coord, NULL, "-view"));                                // Option -cdm_view
    PetscCall(DMViewFromOptions(dm_coord, (PetscObject)*dm_solution, "-coord_dm_view"));  // Option -coord_dm_view
  }

  // Set Ratel as DM application context
  PetscCall(DMSetApplicationContext(*dm_solution, ratel));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Solution DM Setup FEM Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup strain energy computation `DM`

  @param[in,out]  ratel      `Ratel` context
  @param[out]     dm_energy  Strain energy `DM`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelEnergyDMSetup_FEM(Ratel ratel, DM *dm_energy) {
  DM          dm_solution;
  VecType     vec_type;
  CeedInt     num_comp_energy   = 1;
  const char *field_names[]     = {"energy"};
  const char *component_names[] = {"1"};

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Energy DM Setup FEM"));

  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));
  PetscCall(DMClone(dm_solution, dm_energy));
  PetscCall(DMSetMatrixPreallocateSkip(*dm_energy, PETSC_TRUE));
  PetscCall(RatelDMSetupByOrder_FEM(ratel, PETSC_FALSE, PETSC_FALSE, &ratel->highest_fine_order, RATEL_DECIDE, PETSC_FALSE, RATEL_DECIDE, 1,
                                    &num_comp_energy, field_names, component_names, *dm_energy));
  PetscCall(DMGetVecType(dm_solution, &vec_type));
  PetscCall(DMSetVecType(*dm_energy, vec_type));
  PetscCall(PetscObjectSetName((PetscObject)*dm_energy, "Energy"));
  PetscCall(DMViewFromOptions(*dm_energy, NULL, "-energy_dm_view"));
  PetscCall(DMDestroy(&dm_solution));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Energy DM Setup FEM Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup diagnostic values `DM` objects

  @param[in,out]  ratel                   `Ratel` context
  @param[out]     dm_diagnostic           Diagnostic value super `DM`
  @param[out]     num_sub_dms_diagnostic  Number of diagnostic value sub `DM`
  @param[out]     is_sub_dms_diagnostic   Index sets for diagnostic value sub `DM`
  @param[out]     sub_dms_diagnostic      Diagnostic value sub `DM`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDiagnosticDMsSetup_FEM(Ratel ratel, DM *dm_diagnostic, PetscInt *num_sub_dms_diagnostic, IS **is_sub_dms_diagnostic,
                                           DM **sub_dms_diagnostic) {
  DM      dm_solution;
  VecType vec_type;
  CeedInt num_comp_projected_diagnostic, num_comp_dual_diagnostic;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Diagnostic DMs Setup FEM"));

  // Get solution DM
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));

  // Determine num_comp_*_diagnostic
  PetscCall(RatelMaterialGetNumDiagnosticComponents(ratel->materials[0], &num_comp_projected_diagnostic, &num_comp_dual_diagnostic));
  for (PetscInt i = 1; i < ratel->num_materials; i++) {
    CeedInt num_comp_projected_diagnostic_current, num_comp_dual_diagnostic_current;

    PetscCall(
        RatelMaterialGetNumDiagnosticComponents(ratel->materials[i], &num_comp_projected_diagnostic_current, &num_comp_dual_diagnostic_current));
    PetscCheck(num_comp_projected_diagnostic == num_comp_projected_diagnostic_current && num_comp_dual_diagnostic == num_comp_dual_diagnostic_current,
               ratel->comm, PETSC_ERR_LIB, "Mixed number of diagnostic components not supported");
  }

  // Clone DM
  {
    const PetscInt num_diagnostic_fields    = 2;
    const char    *diagnostic_field_names[] = {"projected", "dual"};
    const char   **component_names_projected, **component_names_dual,
        *diagnostic_component_names[num_comp_projected_diagnostic + num_comp_dual_diagnostic];
    PetscInt diagnostic_field_orders[num_diagnostic_fields];
    CeedInt  diagnostic_field_sizes[num_diagnostic_fields];

    for (PetscInt i = 0; i < num_diagnostic_fields; i++) diagnostic_field_orders[i] = ratel->diagnostic_order;
    diagnostic_field_sizes[0] = num_comp_projected_diagnostic;
    diagnostic_field_sizes[1] = num_comp_dual_diagnostic;
    PetscCall(RatelMaterialGetDiagnosticComponentNames(ratel->materials[0], &component_names_projected, &component_names_dual));
    for (PetscInt i = 0; i < num_comp_projected_diagnostic; i++) diagnostic_component_names[i] = component_names_projected[i];
    for (PetscInt i = 0; i < num_comp_dual_diagnostic; i++) diagnostic_component_names[i + num_comp_projected_diagnostic] = component_names_dual[i];
    PetscCall(DMClone(dm_solution, dm_diagnostic));
    PetscCall(DMSetMatrixPreallocateSkip(*dm_diagnostic, PETSC_TRUE));
    PetscCall(RatelDMSetupByOrder_FEM(ratel, PETSC_FALSE, PETSC_FALSE, diagnostic_field_orders, ratel->diagnostic_geometry_order, PETSC_TRUE,
                                      RATEL_DECIDE, num_diagnostic_fields, diagnostic_field_sizes, diagnostic_field_names, diagnostic_component_names,
                                      *dm_diagnostic));
    PetscCall(DMGetVecType(dm_solution, &vec_type));
    PetscCall(DMSetVecType(*dm_diagnostic, vec_type));
  }

  // -- Projected vs dual subproblems
  PetscCall(DMCreateFieldDecomposition(*dm_diagnostic, num_sub_dms_diagnostic, NULL, is_sub_dms_diagnostic, sub_dms_diagnostic));
  {
    PetscBool is_simplex = PETSC_FALSE;

    PetscCall(DMPlexIsSimplex(*dm_diagnostic, &is_simplex));
    if (!is_simplex) {
      PetscCall(DMPlexSetClosurePermutationTensor(*dm_diagnostic, PETSC_DETERMINE, NULL));
      for (PetscInt i = 0; i < *num_sub_dms_diagnostic; i++) {
        PetscCall(DMPlexSetClosurePermutationTensor((*sub_dms_diagnostic)[i], PETSC_DETERMINE, NULL));
      }
    }
  }
  for (PetscInt i = 0; i < *num_sub_dms_diagnostic; i++) PetscCall(DMSetVecType((*sub_dms_diagnostic)[i], vec_type));

  PetscCall(PetscObjectSetName((PetscObject)*dm_diagnostic, "Diagnostic"));
  PetscCall(DMViewFromOptions(*dm_diagnostic, NULL, "-diagnostic_dm_view"));
  PetscCall(PetscObjectSetName((PetscObject)(*sub_dms_diagnostic)[0], "Diagnostic Projected"));
  PetscCall(DMViewFromOptions((*sub_dms_diagnostic)[0], NULL, "-diagnostic_projected_dm_view"));
  PetscCall(PetscObjectSetName((PetscObject)(*sub_dms_diagnostic)[1], "Diagnostic Dual"));
  PetscCall(DMViewFromOptions((*sub_dms_diagnostic)[1], NULL, "-diagnostic_dual_dm_view"));
  PetscCall(DMDestroy(&dm_solution));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Diagnostic DMs Setup FEM Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup surface force `DM`

  Collective across MPI processes.

  @param[in,out]  ratel             `Ratel` context
  @param[out]     dm_surface_force  Surface force `DM`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSurfaceForceCellToFaceDMSetup_FEM(Ratel ratel, DM *dm_surface_force) {
  DM      dm_solution;
  VecType vec_type;
  CeedInt num_active_fields = ratel->num_active_fields;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Surface Force DMs Setup FEM"));

  // Surface force DM
  PetscCall(RatelDebug(ratel, "---- Surface force DM"));
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));
  PetscCall(DMClone(dm_solution, dm_surface_force));
  PetscCall(DMSetMatrixPreallocateSkip(*dm_surface_force, PETSC_TRUE));
  PetscCall(DMGetVecType(dm_solution, &vec_type));
  {
    PetscInt       field_orders[RATEL_MAX_FIELDS] = {0};
    const CeedInt *active_field_sizes;
    const char   **active_field_names, **active_component_names;

    PetscCall(RatelMaterialGetActiveFieldNames(ratel->materials[0], &active_field_names, &active_component_names));
    for (PetscInt i = 0; i < num_active_fields; i++) field_orders[i] = ratel->fine_grid_orders[i];
    PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[0], NULL, &active_field_sizes));
    PetscCall(RatelDMSetupByOrder_FEM(ratel, PETSC_TRUE, PETSC_TRUE, field_orders, RATEL_DECIDE, PETSC_TRUE, ratel->q_extra_surface_force,
                                      num_active_fields, active_field_sizes, active_field_names, active_component_names, *dm_surface_force));
  }
  PetscCall(DMSetVecType(*dm_surface_force, vec_type));

  // -- Copy labels to surface force DMs
  {
    DM dm_coord;

    PetscCall(DMGetCoordinateDM(*dm_surface_force, &dm_coord));
    PetscCall(DMCopyLabels(dm_solution, *dm_surface_force, PETSC_OWN_POINTER, PETSC_FALSE, DM_COPY_LABELS_KEEP));
    PetscCall(DMCopyLabels(dm_solution, dm_coord, PETSC_OWN_POINTER, PETSC_FALSE, DM_COPY_LABELS_KEEP));
  }
  PetscCall(DMViewFromOptions(*dm_surface_force, NULL, "-surface_force_dm_view"));
  PetscCall(DMDestroy(&dm_solution));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Surface Force DMs Setup FEM Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup `DM` with FE space of appropriate degree

  @param[in]   ratel            `Ratel` context
  @param[in]   setup_boundary   Flag to add Dirichlet boundary
  @param[in]   setup_faces      Flag to setup face geometry
  @param[in]   orders           Polynomial orders of field
  @param[in]   coord_order      Polynomial order of coordinate basis, or `RATEL_DECIDE` for default
  @param[in]   setup_coords     Flag to setup coordinate spaces
  @param[in]   q_extra          Additional quadrature order, or `RATEL_DECIDE` for default
  @param[in]   num_fields       Number of fields in solution vector
  @param[in]   field_sizes      Array of number of components for each field
  @param[in]   field_names      Names of fields in `RatelMaterial`
  @param[in]   component_names  Names of components in `RatelMaterial`
  @param[out]  dm               `DM` to setup

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDMSetupByOrder_FEM(Ratel ratel, PetscBool setup_boundary, PetscBool setup_faces, PetscInt *orders, PetscInt coord_order,
                                       PetscBool setup_coords, PetscInt q_extra, CeedInt num_fields, const CeedInt *field_sizes,
                                       const char **field_names, const char **component_names, DM dm) {
  PetscBool is_simplex = PETSC_TRUE;
  PetscInt  dim, q_order = ratel->highest_fine_order + (q_extra == RATEL_DECIDE ? ratel->q_extra : q_extra);
  PetscFE   fe;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMPlex Setup by Degree FEM"));
  PetscCall(PetscLogEventBegin(RATEL_DMSetupByOrder, dm, 0, 0, 0));
  // Check if simplex or tensor-product mesh
  PetscCall(DMPlexIsSimplex(dm, &is_simplex));

  // Setup DM
  PetscCall(DMGetDimension(dm, &dim));
  for (PetscInt i = 0; i < num_fields; i++) {
    char    prefix[PETSC_MAX_PATH_LEN];
    PetscFE fe_face;

    PetscCall(PetscSNPrintf(prefix, PETSC_MAX_PATH_LEN, "%s_", field_names[i]));
    PetscCall(PetscFECreateLagrangeFromOptions(ratel->comm, dim, field_sizes[i], is_simplex, orders[i], q_order, prefix, &fe));
    if (setup_faces) PetscCall(PetscFEGetHeightSubspace(fe, 1, &fe_face));
    PetscCall(DMAddField(dm, NULL, (PetscObject)fe));
    PetscCall(PetscFEDestroy(&fe));
  }
  PetscCall(DMCreateDS(dm));

  // Dirichlet like boundary conditions
  if (setup_boundary) {
    // Dirichlet boundaries
    PetscCall(RatelDebug(ratel, "---- Dirichlet boundaries"));
    PetscCall(RatelDMAddBoundariesDirichlet(ratel, dm));
    // Slip boundaries
    PetscCall(RatelDebug(ratel, "---- Slip boundaries"));
    PetscCall(RatelDMAddBoundariesSlip(ratel, dm));
  }

  // Project coordinates to enrich quadrature space
  if (setup_coords) {
    PetscInt       fe_coord_order, num_comp_coord;
    DM             dm_coord;
    PetscDS        ds_coord;
    PetscFE        fe_coord_current, fe_coord_new, fe_coord_face_new;
    PetscDualSpace fe_coord_dual_space;

    PetscCall(DMGetCoordinateDM(dm, &dm_coord));
    PetscCall(DMGetCoordinateDim(dm, &num_comp_coord));
    PetscCall(DMGetRegionDS(dm_coord, NULL, NULL, &ds_coord, NULL));
    PetscCall(PetscDSGetDiscretization(ds_coord, 0, (PetscObject *)&fe_coord_current));
    PetscCall(PetscFEGetDualSpace(fe_coord_current, &fe_coord_dual_space));
    PetscCall(PetscDualSpaceGetOrder(fe_coord_dual_space, &fe_coord_order));

    // Create FE for coordinates
    if (coord_order != RATEL_DECIDE) fe_coord_order = coord_order;
    PetscCall(PetscFECreateLagrangeFromOptions(ratel->comm, dim, num_comp_coord, is_simplex, fe_coord_order, q_order, NULL, &fe_coord_new));
    if (setup_faces) PetscCall(PetscFEGetHeightSubspace(fe_coord_new, 1, &fe_coord_face_new));
    PetscCall(DMSetCoordinateDisc(dm, fe_coord_new, PETSC_TRUE));
    PetscCall(DMLocalizeCoordinates(dm));  // Update CellCoordinateDM with projected coordinates
    PetscCall(PetscFEDestroy(&fe_coord_new));
  }

  // Set tensor permutation if needed
  if (!is_simplex) {
    PetscCall(DMPlexSetClosurePermutationTensor(dm, PETSC_DETERMINE, NULL));
    if (setup_coords) {
      DM dm_coord;

      PetscCall(DMGetCoordinateDM(dm, &dm_coord));
      PetscCall(DMPlexSetClosurePermutationTensor(dm_coord, PETSC_DETERMINE, NULL));
      PetscCall(DMGetCellCoordinateDM(dm, &dm_coord));
      if (dm_coord) PetscCall(DMPlexSetClosurePermutationTensor(dm_coord, PETSC_DETERMINE, NULL));
    }
  }

  // -- Label field components for viewing
  {
    PetscSection section;

    PetscCall(DMGetLocalSection(dm, &section));
    for (PetscInt i = 0, j = 0; j < num_fields; j++) {
      PetscCall(PetscSectionSetFieldName(section, j, field_names[j]));
      for (PetscInt k = 0; k < field_sizes[j]; k++) {
        PetscCall(PetscSectionSetComponentName(section, j, k, component_names[i]));
        i++;
      }
    }
  }

  PetscCall(PetscLogEventEnd(RATEL_DMSetupByOrder, dm, 0, 0, 0));
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMPlex Setup by Degree FEM Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Compute `CeedOperator` volumetric QData for `RatelMaterial`.

  Collective across MPI processes.

  @param[in]   material     `RatelMaterial` context
  @param[in]   label_name   `DMPlex` label name for volume
  @param[in]   label_value  `DMPlex` label value for volume
  @param[out]  restriction  `CeedElemRestriction` for QData
  @param[out]  q_data       `CeedVector` holding QData

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupVolumeQData_FEM(RatelMaterial material, const char *label_name, PetscInt label_value,
                                                 CeedElemRestriction *restriction, CeedVector *q_data) {
  Ratel               ratel = material->ratel;
  DM                  dm_solution;
  DMLabel             domain_label;
  PetscInt            dim;
  const PetscInt      height_cell = 0;
  PetscMemType        x_mem_type;
  Vec                 X_loc;
  Ceed                ceed = ratel->ceed;
  CeedInt             num_comp_x, q_data_size = material->model_data->q_data_volume_size;
  CeedVector          x_loc = NULL;
  CeedElemRestriction restriction_x;
  CeedBasis           basis_x;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Volume QData FEM"));

  // DM information
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));
  PetscCall(DMGetDimension(dm_solution, &dim));
  num_comp_x = dim;
  PetscCall(DMGetLabel(dm_solution, label_name, &domain_label));

  // libCEED objects
  // -- Coordinate basis
  PetscCall(RatelDebug(ratel, "---- Coordinate basis"));
  PetscCall(RatelDMPlexCeedBasisCoordinateCreate(ratel, dm_solution, domain_label, label_value, height_cell, &basis_x));
  // -- Coordinate restriction
  PetscCall(RatelDebug(ratel, "---- Coordinate restriction"));
  PetscCall(RatelDMPlexCeedElemRestrictionCoordinateCreate(ratel, dm_solution, domain_label, label_value, height_cell, &restriction_x));
  // ---- QData restriction
  PetscCall(RatelDebug(ratel, "---- QData restriction"));
  PetscCall(RatelDMPlexCeedElemRestrictionQDataCreate(ratel, dm_solution, domain_label, label_value, height_cell, q_data_size, restriction));

  // Element coordinates
  PetscCall(RatelDebug(ratel, "---- Retrieving element coordinates"));
  PetscCall(DMGetCoordinatesLocal(dm_solution, &X_loc));
  RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_x, &x_loc, NULL));
  PetscCall(VecReadPetscToCeed(X_loc, &x_mem_type, x_loc));

  // libCEED vectors
  PetscCall(RatelDebug(ratel, "---- QData vector"));
  // -- Geometric data vector
  RatelCallCeed(ratel, CeedElemRestrictionCreateVector(*restriction, q_data, NULL));

  // Geometric factor computation
  {
    CeedQFunction qf_setup_q_data_volume;
    CeedOperator  op_setup_q_data_volume;

    PetscCall(RatelDebug(ratel, "---- Computing volumetric qdata"));

    // -- QFunction
    RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, material->model_data->setup_q_data_volume,
                                                     material->model_data->setup_q_data_volume_loc, &qf_setup_q_data_volume));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_setup_q_data_volume, "dx/dX", num_comp_x * dim, CEED_EVAL_GRAD));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_setup_q_data_volume, "weight", 1, CEED_EVAL_WEIGHT));
    RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_setup_q_data_volume, "q data", q_data_size, CEED_EVAL_NONE));

    // -- Operator
    RatelCallCeed(ratel, CeedOperatorCreate(ceed, qf_setup_q_data_volume, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &op_setup_q_data_volume));
    PetscCall(RatelMaterialSetOperatorName(material, "volumetric geometric data", op_setup_q_data_volume));
    RatelCallCeed(ratel, CeedOperatorSetField(op_setup_q_data_volume, "dx/dX", restriction_x, basis_x, CEED_VECTOR_ACTIVE));
    RatelCallCeed(ratel, CeedOperatorSetField(op_setup_q_data_volume, "weight", CEED_ELEMRESTRICTION_NONE, basis_x, CEED_VECTOR_NONE));
    RatelCallCeed(ratel, CeedOperatorSetField(op_setup_q_data_volume, "q data", *restriction, CEED_BASIS_NONE, CEED_VECTOR_ACTIVE));

    // -- Compute the quadrature data
    if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(op_setup_q_data_volume, stdout));
    RatelCallCeed(ratel, CeedOperatorApply(op_setup_q_data_volume, x_loc, *q_data, CEED_REQUEST_IMMEDIATE));

    // -- Cleanup
    RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_setup_q_data_volume));
    RatelCallCeed(ratel, CeedOperatorDestroy(&op_setup_q_data_volume));
  }
  // Cleanup
  PetscCall(DMDestroy(&dm_solution));
  PetscCall(VecReadCeedToPetsc(x_loc, x_mem_type, X_loc));
  RatelCallCeed(ratel, CeedVectorDestroy(&x_loc));
  RatelCallCeed(ratel, CeedBasisDestroy(&basis_x));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_x));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Volume QData FEM Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Compute `CeedOperator` surface gradient QData for `RatelMaterial`.

  Collective across MPI processes.

  @param[in]   material     `RatelMaterial` context
  @param[in]   dm           `DMPlex` grid
  @param[in]   label_name   `DMPlex` label name for surface
  @param[in]   label_value  `DMPlex` label value for surface
  @param[out]  restriction  `CeedElemRestriction` for QData
  @param[out]  q_data       `CeedVector` holding QData

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupSurfaceGradientQData_FEM(RatelMaterial material, DM dm, const char *label_name, PetscInt label_value,
                                                          CeedElemRestriction *restriction, CeedVector *q_data) {
  Ratel               ratel = material->ratel;
  DMLabel             domain_label;
  PetscInt            dim;
  const PetscInt      height_cell = 0, height_face = 1;
  PetscMemType        x_mem_type;
  Vec                 X_loc;
  Ceed                ceed = ratel->ceed;
  CeedInt             num_comp_x, q_data_size = material->model_data->q_data_surface_grad_size;
  CeedVector          x_loc = NULL;
  CeedElemRestriction restriction_x_cell, restriction_x_face;
  CeedBasis           basis_x_cell_to_face, basis_x_face;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Surface Gradient QData FEM"));

  // DM information
  PetscCall(DMGetDimension(dm, &dim));
  num_comp_x = dim;
  PetscCall(DMGetLabel(dm, label_name, &domain_label));

  // libCEED objects
  // -- Coordinate bases
  PetscCall(RatelDebug(ratel, "---- Coordinate bases"));
  PetscCall(RatelDMPlexCeedBasisCoordinateCreate(ratel, dm, domain_label, label_value, height_face, &basis_x_face));
  PetscCall(RatelDMPlexCeedBasisCellToFaceCoordinateCreate(ratel, dm, domain_label, label_value, label_value, &basis_x_cell_to_face));
  // -- Coordinate restrictions
  PetscCall(RatelDebug(ratel, "---- Coordinate restrictions"));
  PetscCall(RatelDMPlexCeedElemRestrictionCoordinateCreate(ratel, dm, domain_label, label_value, height_cell, &restriction_x_cell));
  PetscCall(RatelDMPlexCeedElemRestrictionCoordinateCreate(ratel, dm, domain_label, label_value, height_face, &restriction_x_face));
  // ---- QData restriction
  PetscCall(RatelDebug(ratel, "---- QData restriction"));
  PetscCall(RatelDMPlexCeedElemRestrictionQDataCreate(ratel, dm, domain_label, label_value, height_face, q_data_size, restriction));

  // Element coordinates
  PetscCall(RatelDebug(ratel, "---- Retrieving element coordinates"));
  PetscCall(DMGetCoordinatesLocal(dm, &X_loc));
  RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_x_cell, &x_loc, NULL));
  PetscCall(VecReadPetscToCeed(X_loc, &x_mem_type, x_loc));

  // libCEED vectors
  PetscCall(RatelDebug(ratel, "---- QData vector"));
  // -- Geometric data vector
  RatelCallCeed(ratel, CeedElemRestrictionCreateVector(*restriction, q_data, NULL));

  // Geometric factor computation
  {
    CeedQFunction qf_setup_q_data_surface_grad;
    CeedOperator  op_setup_q_data_surface_grad;

    PetscCall(RatelDebug(ratel, "---- Computing surface gradient qdata"));

    // -- QFunction
    RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, material->model_data->setup_q_data_surface_grad,
                                                     material->model_data->setup_q_data_surface_grad_loc, &qf_setup_q_data_surface_grad));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_setup_q_data_surface_grad, "dx/dX cell", num_comp_x * (dim - height_cell), CEED_EVAL_GRAD));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_setup_q_data_surface_grad, "dx/dX face", num_comp_x * (dim - height_face), CEED_EVAL_GRAD));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_setup_q_data_surface_grad, "weight", 1, CEED_EVAL_WEIGHT));
    RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_setup_q_data_surface_grad, "q data", q_data_size, CEED_EVAL_NONE));

    // -- Operator
    RatelCallCeed(ratel,
                  CeedOperatorCreate(ceed, qf_setup_q_data_surface_grad, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &op_setup_q_data_surface_grad));
    PetscCall(RatelMaterialSetOperatorName(material, "surface gradient geometric data", op_setup_q_data_surface_grad));
    RatelCallCeed(ratel,
                  CeedOperatorSetField(op_setup_q_data_surface_grad, "dx/dX cell", restriction_x_cell, basis_x_cell_to_face, CEED_VECTOR_ACTIVE));
    RatelCallCeed(ratel, CeedOperatorSetField(op_setup_q_data_surface_grad, "dx/dX face", restriction_x_face, basis_x_face, CEED_VECTOR_ACTIVE));
    RatelCallCeed(ratel, CeedOperatorSetField(op_setup_q_data_surface_grad, "weight", CEED_ELEMRESTRICTION_NONE, basis_x_face, CEED_VECTOR_NONE));
    RatelCallCeed(ratel, CeedOperatorSetField(op_setup_q_data_surface_grad, "q data", *restriction, CEED_BASIS_NONE, CEED_VECTOR_ACTIVE));

    // -- Compute the quadrature data
    if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(op_setup_q_data_surface_grad, stdout));
    RatelCallCeed(ratel, CeedOperatorApply(op_setup_q_data_surface_grad, x_loc, *q_data, CEED_REQUEST_IMMEDIATE));

    // -- Cleanup
    RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_setup_q_data_surface_grad));
    RatelCallCeed(ratel, CeedOperatorDestroy(&op_setup_q_data_surface_grad));
  }
  // Cleanup
  PetscCall(VecCeedToPetsc(x_loc, x_mem_type, X_loc));
  RatelCallCeed(ratel, CeedVectorDestroy(&x_loc));
  RatelCallCeed(ratel, CeedBasisDestroy(&basis_x_face));
  RatelCallCeed(ratel, CeedBasisDestroy(&basis_x_cell_to_face));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_x_face));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_x_cell));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Surface Gradient QData FEM Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup Residual `CeedOperator` for `RatelMaterial`.

  Collective across MPI processes.

  @param[in]   material         `RatelMaterial` context
  @param[out]  op_residual_u    Composite residual u term `CeedOperator` to add `RatelMaterial` suboperator
  @param[out]  op_residual_ut   Composite residual u_t term `CeedOperator` to add `RatelMaterial` suboperator
  @param[out]  op_residual_utt  Composite residual u_tt term `CeedOperator` to add `RatelMaterial` suboperator

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupResidualSuboperators_FEM(RatelMaterial material, CeedOperator op_residual_u, CeedOperator op_residual_ut,
                                                          CeedOperator op_residual_utt) {
  Ratel          ratel      = material->ratel;
  RatelModelData model_data = material->model_data;
  DM             dm_solution;
  const char    *volume_label_name;
  PetscInt       dim, *domain_values;
  const PetscInt height_cell = 0;
  DMLabel        domain_label;
  Ceed           ceed = ratel->ceed;
  const CeedInt *active_field_sizes, *active_field_num_eval_modes = model_data->active_field_num_eval_modes,
                                     *ut_field_num_eval_modes  = model_data->ut_field_num_eval_modes,
                                     *utt_field_num_eval_modes = model_data->utt_field_num_eval_modes;
  CeedInt             num_active_fields, q_data_size = model_data->q_data_volume_size;
  const CeedEvalMode *active_field_eval_modes = model_data->active_field_eval_modes, *ut_field_eval_modes = model_data->ut_field_eval_modes,
                     *utt_field_eval_modes = model_data->utt_field_eval_modes;
  CeedVector           q_data = NULL, stored_values_u = NULL, state_values = NULL, state_values_current = NULL;
  CeedElemRestriction *restrictions_u = NULL, restriction_q_data = NULL, restriction_stored_values_u = NULL, restriction_state_values = NULL;
  CeedBasis           *bases_u = NULL;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Residual Suboperators FEM"));

  // DM information
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));
  PetscCall(DMGetDimension(dm_solution, &dim));
  PetscCall(RatelMaterialGetVolumeLabelName(material, &volume_label_name));
  PetscCall(DMGetLabel(dm_solution, volume_label_name, &domain_label));

  // Active fields
  PetscCall(RatelMaterialGetActiveFieldSizes(material, &num_active_fields, &active_field_sizes));

  // libCEED objects
  // -- Solution basis
  PetscCall(RatelDebug(ratel, "---- Solution bases"));
  PetscCall(RatelMaterialGetVolumeLabelValues(material, NULL, &domain_values));
  PetscCall(PetscCalloc1(num_active_fields, &bases_u));
  for (PetscInt i = 0; i < num_active_fields; i++) {
    PetscCall(RatelDMPlexCeedBasisCreate(ratel, dm_solution, domain_label, domain_values[0], height_cell, i, &bases_u[i]));
  }
  // -- Solution restriction
  PetscCall(RatelDebug(ratel, "---- Solution restrictions"));
  PetscCall(PetscCalloc1(num_active_fields, &restrictions_u));
  for (PetscInt i = 0; i < num_active_fields; i++) {
    PetscCall(RatelDMPlexCeedElemRestrictionCreate(ratel, dm_solution, domain_label, domain_values[0], height_cell, i, &restrictions_u[i]));
  }
  // -- Stored data in residual_u at quadrature points
  if (model_data->num_comp_stored_u > 0) {
    PetscCall(RatelDMPlexCeedElemRestrictionQDataCreate(ratel, dm_solution, domain_label, domain_values[0], height_cell,
                                                        model_data->num_comp_stored_u, &restriction_stored_values_u));
    RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_stored_values_u, &stored_values_u, NULL));
    // Need to initialize to 0.0
    RatelCallCeed(ratel, CeedVectorSetValue(stored_values_u, 0.0));
  }
  // -- State data at quadrature points
  if (model_data->num_comp_state > 0) {
    PetscCall(RatelDMPlexCeedElemRestrictionQDataCreate(ratel, dm_solution, domain_label, domain_values[0], height_cell, model_data->num_comp_state,
                                                        &restriction_state_values));
    RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_state_values, &state_values, NULL));
    RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_state_values, &state_values_current, NULL));
    // Need to initialize to 0.0
    RatelCallCeed(ratel, CeedVectorSetValue(state_values, 0.0));
    RatelCallCeed(ratel, CeedVectorSetValue(state_values_current, 0.0));
  }
  // -- QData
  PetscCall(RatelMaterialGetVolumeQData(material, &restriction_q_data, &q_data));

  // Local residual evaluator
  {
    CeedQFunction qf_residual_u;
    CeedOperator  sub_op_residual_u;

    PetscCall(RatelDebug(ratel, "---- Setting up local residual evaluator, u term"));

    // -- QFunction
    RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, model_data->residual_u, model_data->residual_u_loc, &qf_residual_u));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_residual_u, "q data", q_data_size, CEED_EVAL_NONE));
    if (model_data->num_comp_state > 0) {
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_residual_u, "model state", model_data->num_comp_state, CEED_EVAL_NONE));
      RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_residual_u, "current model state", model_data->num_comp_state, CEED_EVAL_NONE));
    }
    if (model_data->num_comp_stored_u > 0) {
      RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_residual_u, "stored values u", model_data->num_comp_stored_u, CEED_EVAL_NONE));
    }
    {
      CeedInt eval_mode_index = 0;

      for (PetscInt i = 0; i < num_active_fields; i++) {
        for (PetscInt j = 0; j < active_field_num_eval_modes[i]; j++) {
          char         field_name[PETSC_MAX_PATH_LEN];
          CeedEvalMode eval_mode       = active_field_eval_modes[eval_mode_index++];
          CeedInt      quadrature_size = GetCeedQuadratureSize(eval_mode, dim, active_field_sizes[i]);

          PetscCheck(quadrature_size > 0, ratel->comm, PETSC_ERR_SUP, "Eval mode %s not supported", CeedEvalModes[eval_mode]);
          PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "du/dX" : "u", i));
          RatelCallCeed(ratel, CeedQFunctionAddInput(qf_residual_u, field_name, quadrature_size, eval_mode));
          PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "dv/dX" : "v", i));
          RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_residual_u, field_name, quadrature_size, eval_mode));
        }
      }
    }
    RatelCallCeed(ratel, CeedQFunctionSetContext(qf_residual_u, material->ctx_params));
    RatelCallCeed(ratel, CeedQFunctionSetContextWritable(qf_residual_u, PETSC_FALSE));
    // -- Operator
    RatelCallCeed(ratel, CeedOperatorCreate(ceed, qf_residual_u, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &sub_op_residual_u));
    PetscCall(RatelMaterialSetOperatorName(material, "volumetric term", sub_op_residual_u));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_residual_u, "q data", restriction_q_data, CEED_BASIS_NONE, q_data));
    if (model_data->num_comp_state > 0) {
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_residual_u, "model state", restriction_state_values, CEED_BASIS_NONE, state_values));
      RatelCallCeed(ratel,
                    CeedOperatorSetField(sub_op_residual_u, "current model state", restriction_state_values, CEED_BASIS_NONE, state_values_current));
    }
    if (model_data->num_comp_stored_u > 0) {
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_residual_u, "stored values u", restriction_stored_values_u, CEED_BASIS_NONE, stored_values_u));
    }
    {
      CeedInt eval_mode_index = 0;

      for (PetscInt i = 0; i < num_active_fields; i++) {
        for (PetscInt j = 0; j < active_field_num_eval_modes[i]; j++) {
          char         field_name[PETSC_MAX_PATH_LEN];
          CeedEvalMode eval_mode = active_field_eval_modes[eval_mode_index++];

          PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "du/dX" : "u", i));
          RatelCallCeed(ratel, CeedOperatorSetField(sub_op_residual_u, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
          PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "dv/dX" : "v", i));
          RatelCallCeed(ratel, CeedOperatorSetField(sub_op_residual_u, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
        }
      }
    }
    // -- Add to composite operator
    RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_residual_u, sub_op_residual_u));
    if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_residual_u, stdout));
    // -- Cleanup
    RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_residual_u));
    RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_residual_u));
  }

  if (material->model_data->has_ut_term) {
    ratel->has_ut_term                               = PETSC_TRUE;
    CeedVector          stored_values_ut             = NULL;
    CeedElemRestriction restriction_stored_values_ut = NULL;
    CeedQFunction       qf_residual_ut;
    CeedOperator        sub_op_residual_ut;

    PetscCall(RatelDebug(ratel, "---- Setting up local residual evaluator, u_t term"));

    // -- Stored data in residual_ut at quadrature points
    if (model_data->num_comp_stored_ut > 0) {
      PetscCall(RatelDMPlexCeedElemRestrictionQDataCreate(ratel, dm_solution, domain_label, domain_values[0], height_cell,
                                                          model_data->num_comp_stored_ut, &restriction_stored_values_ut));
      RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_stored_values_ut, &stored_values_ut, NULL));
      // Need to initialize to 0.0
      RatelCallCeed(ratel, CeedVectorSetValue(stored_values_ut, 0.0));
    }

    // -- QFunction
    RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, model_data->residual_ut, model_data->residual_ut_loc, &qf_residual_ut));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_residual_ut, "q data", q_data_size, CEED_EVAL_NONE));
    if (model_data->pass_stored_u_to_ut) {
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_residual_ut, "stored values u", model_data->num_comp_stored_u, CEED_EVAL_NONE));
    }
    if (model_data->num_comp_stored_ut > 0) {
      RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_residual_ut, "stored values ut", model_data->num_comp_stored_ut, CEED_EVAL_NONE));
    }
    {
      CeedInt eval_mode_index = 0;

      for (PetscInt i = 0; i < num_active_fields; i++) {
        for (PetscInt j = 0; j < ut_field_num_eval_modes[i]; j++) {
          char         field_name[PETSC_MAX_PATH_LEN];
          CeedEvalMode eval_mode       = ut_field_eval_modes[eval_mode_index++];
          CeedInt      quadrature_size = GetCeedQuadratureSize(eval_mode, dim, active_field_sizes[i]);

          PetscCheck(quadrature_size > 0, ratel->comm, PETSC_ERR_SUP, "Eval mode %s not supported", CeedEvalModes[eval_mode]);
          PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "du_t/dX" : "u_t", i));
          RatelCallCeed(ratel, CeedQFunctionAddInput(qf_residual_ut, field_name, quadrature_size, eval_mode));
          PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "dv_t/dX" : "v_t", i));
          RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_residual_ut, field_name, quadrature_size, eval_mode));
        }
      }
    }
    RatelCallCeed(ratel, CeedQFunctionSetContext(qf_residual_ut, material->ctx_params));
    RatelCallCeed(ratel, CeedQFunctionSetContextWritable(qf_residual_ut, PETSC_FALSE));
    // -- Operator
    RatelCallCeed(ratel, CeedOperatorCreate(ceed, qf_residual_ut, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &sub_op_residual_ut));
    PetscCall(RatelMaterialSetOperatorName(material, "volumetric term", sub_op_residual_ut));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_residual_ut, "q data", restriction_q_data, CEED_BASIS_NONE, q_data));
    if (model_data->pass_stored_u_to_ut) {
      RatelCallCeed(ratel,
                    CeedOperatorSetField(sub_op_residual_ut, "stored values u", restriction_stored_values_u, CEED_BASIS_NONE, stored_values_u));
    }
    if (model_data->num_comp_stored_ut > 0) {
      RatelCallCeed(ratel,
                    CeedOperatorSetField(sub_op_residual_ut, "stored values ut", restriction_stored_values_ut, CEED_BASIS_NONE, stored_values_ut));
    }
    {
      CeedInt eval_mode_index = 0;

      for (PetscInt i = 0; i < num_active_fields; i++) {
        for (PetscInt j = 0; j < ut_field_num_eval_modes[i]; j++) {
          char         field_name[PETSC_MAX_PATH_LEN];
          CeedEvalMode eval_mode = ut_field_eval_modes[eval_mode_index++];

          PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "du_t/dX" : "u_t", i));
          RatelCallCeed(ratel, CeedOperatorSetField(sub_op_residual_ut, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
          PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "dv_t/dX" : "v_t", i));
          RatelCallCeed(ratel, CeedOperatorSetField(sub_op_residual_ut, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
        }
      }
    }
    // -- Add to composite operator
    RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_residual_ut, sub_op_residual_ut));
    if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_residual_ut, stdout));
    // -- Cleanup
    RatelCallCeed(ratel, CeedVectorDestroy(&stored_values_ut));
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_stored_values_ut));
    RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_residual_ut));
    RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_residual_ut));
  }

  // Scaled mass matrix for local residual evaluator:
  if (ratel->solver_type == RATEL_SOLVER_DYNAMIC) {
    CeedQFunction qf_residual_utt;
    CeedOperator  sub_op_residual_utt;

    PetscCall(RatelDebug(ratel, "---- Setting up local residual evaluator, u_tt term"));

    // -- Check for support
    PetscCheck(model_data->residual_utt, ratel->comm, PETSC_ERR_SUP, "Material %s does not support u_tt term", material->name);

    // -- QFunction
    RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, model_data->residual_utt, model_data->residual_utt_loc, &qf_residual_utt));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_residual_utt, "q data", q_data_size, CEED_EVAL_NONE));
    {
      CeedInt eval_mode_index = 0;

      for (PetscInt i = 0; i < num_active_fields; i++) {
        for (PetscInt j = 0; j < utt_field_num_eval_modes[i]; j++) {
          char         field_name[PETSC_MAX_PATH_LEN];
          CeedEvalMode eval_mode       = utt_field_eval_modes[eval_mode_index++];
          CeedInt      quadrature_size = GetCeedQuadratureSize(eval_mode, dim, active_field_sizes[i]);

          PetscCheck(quadrature_size > 0, ratel->comm, PETSC_ERR_SUP, "Eval mode %s not supported", CeedEvalModes[eval_mode]);
          PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "du_tt/dX" : "u_tt", i));
          RatelCallCeed(ratel, CeedQFunctionAddInput(qf_residual_utt, field_name, quadrature_size, eval_mode));
          PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "dv_tt/dX" : "v_tt", i));
          RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_residual_utt, field_name, quadrature_size, eval_mode));
        }
      }
    }
    RatelCallCeed(ratel, CeedQFunctionSetContext(qf_residual_utt, material->ctx_params));
    RatelCallCeed(ratel, CeedQFunctionSetContextWritable(qf_residual_utt, PETSC_FALSE));
    // -- Operator
    RatelCallCeed(ratel, CeedOperatorCreate(ceed, qf_residual_utt, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &sub_op_residual_utt));
    PetscCall(RatelMaterialSetOperatorName(material, "volumetric term", sub_op_residual_utt));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_residual_utt, "q data", restriction_q_data, CEED_BASIS_NONE, q_data));
    {
      CeedInt eval_mode_index = 0;

      for (PetscInt i = 0; i < num_active_fields; i++) {
        for (PetscInt j = 0; j < utt_field_num_eval_modes[i]; j++) {
          char         field_name[PETSC_MAX_PATH_LEN];
          CeedEvalMode eval_mode = utt_field_eval_modes[eval_mode_index++];

          PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "du_tt/dX" : "u_tt", i));
          RatelCallCeed(ratel, CeedOperatorSetField(sub_op_residual_utt, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
          PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "dv_tt/dX" : "v_tt", i));
          RatelCallCeed(ratel, CeedOperatorSetField(sub_op_residual_utt, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
        }
      }
    }
    // -- Add to composite operator
    RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_residual_utt, sub_op_residual_utt));
    if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_residual_utt, stdout));
    // -- Cleanup
    RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_residual_utt));
    RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_residual_utt));
  }

  // Cleanup
  PetscCall(DMDestroy(&dm_solution));
  PetscCall(RatelDebug(ratel, "---- Cleaning up libCEED objects"));
  RatelCallCeed(ratel, CeedVectorDestroy(&q_data));
  RatelCallCeed(ratel, CeedVectorDestroy(&stored_values_u));
  RatelCallCeed(ratel, CeedVectorDestroy(&state_values));
  RatelCallCeed(ratel, CeedVectorDestroy(&state_values_current));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_q_data));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_stored_values_u));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_state_values));
  for (PetscInt i = 0; i < num_active_fields; i++) {
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restrictions_u[i]));
    RatelCallCeed(ratel, CeedBasisDestroy(&bases_u[i]));
  }
  PetscCall(PetscFree(restrictions_u));
  PetscCall(PetscFree(bases_u));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Residual and Jacobian Suboperators FEM Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup Jacobian `CeedOperator` for `RatelMaterial`.

  Collective across MPI processes.

  @param[in]   material        `RatelMaterial` context
  @param[out]  op_residual_u   Composite residual u term `CeedOperator`
  @param[out]  op_residual_ut  Composite residual ut term `CeedOperator`
  @param[out]  op_jacobian     Composite Jacobian `CeedOperator` to add `RatelMaterial` suboperator

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupJacobianSuboperator_FEM(RatelMaterial material, CeedOperator op_residual_u, CeedOperator op_residual_ut,
                                                         CeedOperator op_jacobian) {
  Ratel          ratel      = material->ratel;
  RatelModelData model_data = material->model_data;
  DM             dm_solution;
  const char    *volume_label_name;
  PetscInt       dim;
  DMLabel        domain_label;
  Ceed           ceed = ratel->ceed;
  const CeedInt *active_field_sizes, *active_field_num_eval_modes = model_data->active_field_num_eval_modes,
                                     *utt_field_num_eval_modes = model_data->utt_field_num_eval_modes;
  CeedInt              num_active_fields, q_data_size = model_data->q_data_volume_size;
  const CeedEvalMode  *active_field_eval_modes = model_data->active_field_eval_modes;
  CeedVector           q_data = NULL, stored_values_u = NULL, stored_values_ut = NULL, state_values = NULL;
  CeedElemRestriction *restrictions_u = NULL, restriction_q_data = NULL, restriction_stored_values_u = NULL, restriction_stored_values_ut = NULL,
                      restriction_state_values = NULL;
  CeedBasis *bases_u                           = NULL;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Residual and Jacobian Suboperators FEM"));

  // DM information
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));
  PetscCall(DMGetDimension(dm_solution, &dim));
  PetscCall(RatelMaterialGetVolumeLabelName(material, &volume_label_name));
  PetscCall(DMGetLabel(dm_solution, volume_label_name, &domain_label));

  // Active fields
  PetscCall(RatelMaterialGetActiveFieldSizes(material, &num_active_fields, &active_field_sizes));

  // libCEED objects
  // -- Solution objects from suboperator
  PetscCall(RatelMaterialGetSolutionData(material, op_residual_u, &num_active_fields, &restrictions_u, &bases_u));
  // -- QData
  PetscCall(RatelMaterialGetVolumeQData(material, &restriction_q_data, &q_data));
  // -- Stored data in residual_u at quadrature points
  PetscCall(RatelMaterialGetStoredDataU(material, op_residual_u, &restriction_stored_values_u, &stored_values_u));
  // -- Stored data in residual_ut at quadrature points
  if (material->model_data->has_ut_term) {
    PetscCall(RatelMaterialGetStoredDataUt(material, op_residual_ut, &restriction_stored_values_ut, &stored_values_ut));
  }
  // -- State data at quadrature points
  PetscCall(RatelMaterialGetStateData(material, op_residual_u, &restriction_state_values, &state_values));

  // Jacobian evaluator
  {
    CeedQFunction qf_jacobian;
    CeedOperator  sub_op_jacobian;

    PetscCall(RatelDebug(ratel, "---- Setting up Jacobian evaluator"));

    // -- QFunction
    RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, model_data->jacobian, model_data->jacobian_loc, &qf_jacobian));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_jacobian, "q data", q_data_size, CEED_EVAL_NONE));
    if (model_data->num_comp_state > 0) {
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_jacobian, "model state", model_data->num_comp_state, CEED_EVAL_NONE));
    }
    if (model_data->num_comp_stored_u > 0) {
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_jacobian, "stored values u", model_data->num_comp_stored_u, CEED_EVAL_NONE));
    }
    if (model_data->num_comp_stored_ut > 0) {
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_jacobian, "stored values ut", model_data->num_comp_stored_ut, CEED_EVAL_NONE));
    }
    {
      CeedInt eval_mode_index = 0;

      for (PetscInt i = 0; i < num_active_fields; i++) {
        for (PetscInt j = 0; j < active_field_num_eval_modes[i]; j++) {
          char         field_name[PETSC_MAX_PATH_LEN];
          CeedEvalMode eval_mode       = active_field_eval_modes[eval_mode_index++];
          CeedInt      quadrature_size = GetCeedQuadratureSize(eval_mode, dim, active_field_sizes[i]);

          PetscCheck(quadrature_size > 0, ratel->comm, PETSC_ERR_SUP, "Eval mode %s not supported", CeedEvalModes[eval_mode]);
          PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "d(du)/dX" : "du", i));
          RatelCallCeed(ratel, CeedQFunctionAddInput(qf_jacobian, field_name, quadrature_size, eval_mode));
          PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "d(dv)/dX" : "dv", i));
          RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_jacobian, field_name, quadrature_size, eval_mode));
        }
      }
    }
    if (ratel->solver_type == RATEL_SOLVER_DYNAMIC) {
      CeedInt eval_mode_index = 0;

      for (PetscInt i = 0; i < num_active_fields; i++) {
        PetscBool has_interp = PETSC_FALSE;

        for (PetscInt j = 0; j < active_field_num_eval_modes[i]; j++) {
          has_interp = has_interp || (active_field_eval_modes[eval_mode_index++] == CEED_EVAL_INTERP);
        }
        if (!has_interp && utt_field_num_eval_modes[i] > 0) {
          char field_name[PETSC_MAX_PATH_LEN];

          PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "du, field %" PetscInt_FMT, i));
          RatelCallCeed(ratel, CeedQFunctionAddInput(qf_jacobian, field_name, active_field_sizes[i], CEED_EVAL_INTERP));
          PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "dv, field %" PetscInt_FMT, i));
          RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_jacobian, field_name, active_field_sizes[i], CEED_EVAL_INTERP));
        }
      }
    }
    RatelCallCeed(ratel, CeedQFunctionSetContext(qf_jacobian, material->ctx_params));
    RatelCallCeed(ratel, CeedQFunctionSetContextWritable(qf_jacobian, PETSC_FALSE));
    // -- Operator
    RatelCallCeed(ratel, CeedOperatorCreate(ceed, qf_jacobian, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &sub_op_jacobian));
    PetscCall(RatelMaterialSetOperatorName(material, "volumetric term", sub_op_jacobian));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_jacobian, "q data", restriction_q_data, CEED_BASIS_NONE, q_data));
    if (model_data->num_comp_state > 0) {
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_jacobian, "model state", restriction_state_values, CEED_BASIS_NONE, state_values));
    }
    if (model_data->num_comp_stored_u > 0) {
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_jacobian, "stored values u", restriction_stored_values_u, CEED_BASIS_NONE, stored_values_u));
    }
    if (model_data->num_comp_stored_ut > 0) {
      RatelCallCeed(ratel,
                    CeedOperatorSetField(sub_op_jacobian, "stored values ut", restriction_stored_values_ut, CEED_BASIS_NONE, stored_values_ut));
    }
    {
      CeedInt eval_mode_index = 0;

      for (PetscInt i = 0; i < num_active_fields; i++) {
        for (PetscInt j = 0; j < active_field_num_eval_modes[i]; j++) {
          char         field_name[PETSC_MAX_PATH_LEN];
          CeedEvalMode eval_mode = active_field_eval_modes[eval_mode_index++];

          PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "d(du)/dX" : "du", i));
          RatelCallCeed(ratel, CeedOperatorSetField(sub_op_jacobian, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
          PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "d(dv)/dX" : "dv", i));
          RatelCallCeed(ratel, CeedOperatorSetField(sub_op_jacobian, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
        }
      }
    }
    if (ratel->solver_type == RATEL_SOLVER_DYNAMIC) {
      CeedInt eval_mode_index = 0;

      for (PetscInt i = 0; i < num_active_fields; i++) {
        PetscBool has_interp = PETSC_FALSE;

        for (PetscInt j = 0; j < active_field_num_eval_modes[i]; j++) {
          has_interp = has_interp || (active_field_eval_modes[eval_mode_index++] == CEED_EVAL_INTERP);
        }
        if (!has_interp && utt_field_num_eval_modes[i] > 0) {
          char field_name[PETSC_MAX_PATH_LEN];

          PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "du, field %" PetscInt_FMT, i));
          RatelCallCeed(ratel, CeedOperatorSetField(sub_op_jacobian, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
          PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "dv, field %" PetscInt_FMT, i));
          RatelCallCeed(ratel, CeedOperatorSetField(sub_op_jacobian, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
        }
      }
    }
    // -- FLOPs counting
    CeedSize flops = model_data->flops_qf_jacobian_u + model_data->flops_qf_jacobian_ut;
    if (ratel->solver_type == RATEL_SOLVER_DYNAMIC) flops += model_data->flops_qf_jacobian_utt;
    RatelCallCeed(ratel, CeedQFunctionSetUserFlopsEstimate(qf_jacobian, flops));
    // -- Add to composite operator
    RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_jacobian, sub_op_jacobian));
    if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_jacobian, stdout));
    // -- Cleanup
    RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_jacobian));
    RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_jacobian));
  }

  // Cleanup
  PetscCall(DMDestroy(&dm_solution));
  RatelCallCeed(ratel, CeedVectorDestroy(&q_data));
  RatelCallCeed(ratel, CeedVectorDestroy(&stored_values_u));
  RatelCallCeed(ratel, CeedVectorDestroy(&stored_values_ut));
  RatelCallCeed(ratel, CeedVectorDestroy(&state_values));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_q_data));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_stored_values_u));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_stored_values_ut));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_state_values));
  for (PetscInt i = 0; i < num_active_fields; i++) {
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restrictions_u[i]));
    RatelCallCeed(ratel, CeedBasisDestroy(&bases_u[i]));
  }
  PetscCall(PetscFree(restrictions_u));
  PetscCall(PetscFree(bases_u));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Residual and Jacobian Suboperators FEM Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup multigrid level `CeedOperator` for `RatelMaterial`.

  Collective across MPI processes.

  @param[in]      material              `RatelMaterial` context
  @param[in]      dm_level              `DMPlex` for multigrid level to setup
  @param[in]      m_loc                 `CeedVector` holding multiplicity
  @param[in]      sub_op_jacobian_fine  Fine grid Jacobian `CeedOperator`
  @param[in,out]  op_jacobian_coarse    Composite Jacobian `CeedOperator` to add `RatelMaterial` suboperators
  @param[in,out]  op_prolong            Composite prolongation `CeedOperator` to add `RatelMaterial` suboperators
  @param[in,out]  op_restrict           Composite restriction `CeedOperator` to add `RatelMaterial` suboperators

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupJacobianMultigridLevel_FEM(RatelMaterial material, DM dm_level, CeedVector m_loc, CeedOperator sub_op_jacobian_fine,
                                                            CeedOperator op_jacobian_coarse, CeedOperator op_prolong, CeedOperator op_restrict) {
  Ratel               ratel = material->ratel;
  DM                  dm_solution;
  const char         *volume_label_name;
  PetscInt            dim, *label_values;
  const PetscInt      height_cell = 0;
  DMLabel             domain_label;
  CeedElemRestriction restriction_u;
  CeedBasis           basis_u;
  CeedOperator        sub_op_jacobian_coarse, sub_op_prolong, sub_op_restrict;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Multigrid Level"));

  // -- Domain information
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));
  PetscCall(DMGetDimension(dm_solution, &dim));
  PetscCall(RatelMaterialGetVolumeLabelValues(material, NULL, &label_values));
  PetscCall(RatelMaterialGetVolumeLabelName(material, &volume_label_name));
  PetscCall(DMGetLabel(dm_solution, volume_label_name, &domain_label));

  // --  libCEED objects
  PetscCall(RatelDebug(ratel, "---- Setting up libCEED restriction for level"));
  PetscCall(RatelDMPlexCeedElemRestrictionCreate(ratel, dm_level, domain_label, label_values[0], height_cell, 0, &restriction_u));
  PetscCall(RatelDebug(ratel, "---- Setting up libCEED basis for level"));
  PetscCall(RatelDMPlexCeedBasisCreate(ratel, dm_level, domain_label, label_values[0], height_cell, 0, &basis_u));

  // -- Create coarse grid, prolongation, and restriction operators
  PetscCall(RatelDebug(ratel, "---- Setting up level Jacobian, prolongation, and restriction operators"));
  RatelCallCeed(ratel, CeedOperatorMultigridLevelCreate(sub_op_jacobian_fine, m_loc, restriction_u, basis_u, &sub_op_jacobian_coarse, &sub_op_prolong,
                                                        &sub_op_restrict));

  // -- Add to composite operators
  RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_jacobian_coarse, sub_op_jacobian_coarse));
  RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_prolong, sub_op_prolong));
  RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_restrict, sub_op_restrict));

  // -- Cleanup
  PetscCall(DMDestroy(&dm_solution));
  RatelCallCeed(ratel, CeedBasisDestroy(&basis_u));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_u));
  RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_jacobian_coarse));
  RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_prolong));
  RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_restrict));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Multigrid Level Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup Block Preconditioner CeedOperator for RatelMaterial

  @param[in]   material           RatelMaterial context
  @param[in]   dm                 DM for the given field
  @param[in]   field              Field that we want to apply pc
  @param[out]  op_jacobian_block  CeedOperator created for the given field

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupJacobianBlockSuboperator_FEM(RatelMaterial material, DM dm, PetscInt field, CeedOperator op_jacobian_block) {
  Ratel               ratel      = material->ratel;
  RatelModelData      model_data = material->model_data;
  DM                  dm_solution;
  DMLabel             domain_label;
  PetscInt            dim, *domain_values;
  const CeedInt      *active_field_sizes;
  CeedInt             q_data_size                 = model_data->q_data_volume_size;
  Ceed                ceed                        = ratel->ceed;
  const CeedInt      *active_field_num_eval_modes = model_data->active_field_num_eval_modes;
  const CeedEvalMode *active_field_eval_modes     = model_data->active_field_eval_modes;
  CeedVector          q_data                      = NULL;
  CeedBasis           basis;
  CeedElemRestriction restriction, restriction_q_data = NULL;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Jacobian Block Suboperator FEM"));

  // DM information
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));
  PetscCall(DMGetDimension(dm_solution, &dim));
  PetscCall(DMGetLabel(dm_solution, material->volume_label_name, &domain_label));

  // Active fields
  PetscCall(RatelMaterialGetActiveFieldSizes(material, NULL, &active_field_sizes));

  // libCEED objects
  // -- Solution basis
  PetscCall(RatelDebug(ratel, "---- Solution bases"));
  PetscCall(RatelMaterialGetVolumeLabelValues(material, NULL, &domain_values));
  PetscCall(RatelDMPlexCeedBasisCreate(ratel, dm_solution, domain_label, domain_values[0], 0, field, &basis));

  // -- Solution restriction
  PetscCall(RatelDebug(ratel, "---- Solution restrictions"));
  PetscCall(RatelDMPlexCeedElemRestrictionCreate(ratel, dm, domain_label, domain_values[0], 0, 0, &restriction));

  // -- QData
  PetscCall(RatelMaterialGetVolumeQData(material, &restriction_q_data, &q_data));

  {
    CeedQFunction qf_jacobian_block;
    CeedOperator  sub_op_jacobian_block, op_jacobian_pre, jacobian_pre_sub_op;

    // -- Get suboperators
    PetscCall(MatCeedGetCeedOperators(ratel->mat_jacobian_pre, &op_jacobian_pre, NULL));
    {
      PetscInt      sub_op_index = material->jacobian_indices[0];
      CeedOperator *sub_ops;

      RatelCallCeed(ratel, CeedCompositeOperatorGetSubList(op_jacobian_pre, &sub_ops));
      jacobian_pre_sub_op = sub_ops[sub_op_index];
    }

    // -- QFunction
    RatelCallCeed(ratel,
                  CeedQFunctionCreateInterior(ceed, 1, model_data->jacobian_block[field], model_data->jacobian_block_loc[field], &qf_jacobian_block));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_jacobian_block, "q data", q_data_size, CEED_EVAL_NONE));
    if (model_data->num_comp_state > 0) {
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_jacobian_block, "model state", model_data->num_comp_state, CEED_EVAL_NONE));
    }
    if (model_data->num_comp_stored_u > 0) {
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_jacobian_block, "stored values u", model_data->num_comp_stored_u, CEED_EVAL_NONE));
    }
    if (model_data->num_comp_stored_ut > 0) {
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_jacobian_block, "stored values ut", model_data->num_comp_stored_ut, CEED_EVAL_NONE));
    }

    // Get the correct starting index for eval modes
    {
      CeedInt eval_mode_index = 0;
      if (field > 0) {
        for (CeedInt i = 0; i < field; i++) eval_mode_index += active_field_num_eval_modes[i];
      }
      for (PetscInt j = 0; j < active_field_num_eval_modes[field]; j++) {
        char         field_name[PETSC_MAX_PATH_LEN];
        CeedEvalMode eval_mode       = active_field_eval_modes[eval_mode_index + j];
        CeedInt      quadrature_size = GetCeedQuadratureSize(eval_mode, dim, active_field_sizes[field]);

        PetscCheck(quadrature_size > 0, ratel->comm, PETSC_ERR_SUP, "Eval mode %s not supported", CeedEvalModes[eval_mode]);
        PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "d(du)/dX" : "du", field));
        RatelCallCeed(ratel, CeedQFunctionAddInput(qf_jacobian_block, field_name, quadrature_size, eval_mode));
        PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "d(dv)/dX" : "dv", field));
        RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_jacobian_block, field_name, quadrature_size, eval_mode));
      }
    }
    // -- Add context
    {
      CeedQFunctionContext ctx_jacobian_pre = NULL;

      RatelCallCeed(ratel, CeedOperatorGetContext(jacobian_pre_sub_op, &ctx_jacobian_pre));
      RatelCallCeed(ratel, CeedQFunctionSetContext(qf_jacobian_block, ctx_jacobian_pre));
      RatelCallCeed(ratel, CeedQFunctionSetContextWritable(qf_jacobian_block, PETSC_FALSE));
      RatelCallCeed(ratel, CeedQFunctionContextDestroy(&ctx_jacobian_pre));
    }

    // -- Operator
    RatelCallCeed(ratel, CeedOperatorCreate(ceed, qf_jacobian_block, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &sub_op_jacobian_block));
    PetscCall(RatelMaterialSetOperatorName(material, "volumetric term", sub_op_jacobian_block));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_jacobian_block, "q data", restriction_q_data, CEED_BASIS_NONE, q_data));
    if (model_data->num_comp_state > 0) {
      const char         *name = "model state";
      CeedVector          state_values;
      CeedElemRestriction restriction_state_values;
      CeedOperator        op_residual_u;

      PetscCall(CeedEvaluatorGetCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
      PetscCall(RatelMaterialGetStateData(material, op_residual_u, &restriction_state_values, &state_values));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_jacobian_block, name, restriction_state_values, CEED_BASIS_NONE, state_values));
      RatelCallCeed(ratel, CeedVectorDestroy(&state_values));
      RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_state_values));
      PetscCall(CeedEvaluatorRestoreCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
    }
    if (model_data->num_comp_stored_u > 0) {
      const char         *name = "stored values u";
      CeedVector          stored_values_u;
      CeedElemRestriction restriction_stored_values_u;
      CeedOperator        op_residual_u;

      PetscCall(CeedEvaluatorGetCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
      PetscCall(RatelMaterialGetStoredDataU(material, op_residual_u, &restriction_stored_values_u, &stored_values_u));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_jacobian_block, name, restriction_stored_values_u, CEED_BASIS_NONE, stored_values_u));
      RatelCallCeed(ratel, CeedVectorDestroy(&stored_values_u));
      RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_stored_values_u));
      PetscCall(CeedEvaluatorRestoreCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
    }
    if (model_data->num_comp_stored_ut > 0) {
      const char         *name = "stored values ut";
      CeedVector          stored_values_ut;
      CeedElemRestriction restriction_stored_values_ut;
      CeedOperator        op_residual_ut;

      PetscCall(CeedEvaluatorGetCeedOperator(ratel->evaluator_residual_ut, &op_residual_ut));
      PetscCall(RatelMaterialGetStoredDataUt(material, op_residual_ut, &restriction_stored_values_ut, &stored_values_ut));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_jacobian_block, name, restriction_stored_values_ut, CEED_BASIS_NONE, stored_values_ut));
      RatelCallCeed(ratel, CeedVectorDestroy(&stored_values_ut));
      RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_stored_values_ut));
      PetscCall(CeedEvaluatorRestoreCeedOperator(ratel->evaluator_residual_ut, &op_residual_ut));
    }
    {
      CeedInt eval_mode_index = 0;
      if (field > 0) {
        for (CeedInt i = 0; i < field; i++) eval_mode_index += active_field_num_eval_modes[i];
      }
      for (PetscInt j = 0; j < active_field_num_eval_modes[field]; j++) {
        char         field_name[PETSC_MAX_PATH_LEN];
        CeedEvalMode eval_mode = active_field_eval_modes[eval_mode_index + j];

        PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "d(du)/dX" : "du", field));
        RatelCallCeed(ratel, CeedOperatorSetField(sub_op_jacobian_block, field_name, restriction, basis, CEED_VECTOR_ACTIVE));
        PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "d(dv)/dX" : "dv", field));
        RatelCallCeed(ratel, CeedOperatorSetField(sub_op_jacobian_block, field_name, restriction, basis, CEED_VECTOR_ACTIVE));
      }
    }
    // -- FLOPs counting
    CeedSize flops = model_data->flops_qf_jacobian_block[field];
    RatelCallCeed(ratel, CeedQFunctionSetUserFlopsEstimate(qf_jacobian_block, flops));
    // -- Add to composite operators
    RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_jacobian_block, sub_op_jacobian_block));
    if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_jacobian_block, stdout));
    // -- Cleanup
    PetscCall(MatCeedRestoreCeedOperators(ratel->mat_jacobian_pre, &op_jacobian_pre, NULL));
    RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_jacobian_block));
    RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_jacobian_block));
  }
  // Cleanup
  PetscCall(DMDestroy(&dm_solution));
  RatelCallCeed(ratel, CeedVectorDestroy(&q_data));
  RatelCallCeed(ratel, CeedBasisDestroy(&basis));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_q_data));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Jacobian Block Suboperator FEM Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup strain energy `CeedOperator` for `RatelMaterial`.

  Collective across MPI processes.

  @param[in]   material          `RatelMaterial` context
  @param[in]   dm_energy         `DM` for strain energy computation
  @param[out]  op_strain_energy  Composite strain energy `CeedOperator` to add `RatelMaterial` suboperator

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupStrainEnergySuboperator_FEM(RatelMaterial material, DM dm_energy, CeedOperator op_strain_energy) {
  Ratel                ratel      = material->ratel;
  RatelModelData       model_data = material->model_data;
  DM                   dm_solution;
  const char          *volume_label_name;
  DMLabel              domain_label;
  PetscInt             dim, *domain_values;
  const PetscInt       height_cell = 0;
  Ceed                 ceed        = ratel->ceed;
  CeedInt              num_active_fields, num_comp_energy = 1, q_data_size = model_data->q_data_volume_size;
  const CeedInt       *active_field_sizes;
  const CeedInt       *active_field_num_eval_modes = model_data->active_field_num_eval_modes;
  const CeedEvalMode  *active_field_eval_modes     = model_data->active_field_eval_modes;
  CeedVector           q_data                      = NULL;
  CeedElemRestriction *restrictions_u = NULL, restriction_q_data = NULL, restriction_energy;
  CeedBasis           *bases_u = NULL, basis_energy;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Strain Energy Suboperator FEM"));

  // DM information
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));
  PetscCall(DMGetDimension(dm_solution, &dim));
  PetscCall(RatelMaterialGetVolumeLabelValues(material, NULL, &domain_values));
  PetscCall(RatelMaterialGetVolumeLabelName(material, &volume_label_name));
  PetscCall(DMGetLabel(dm_solution, volume_label_name, &domain_label));

  // Active fields
  PetscCall(RatelMaterialGetActiveFieldSizes(material, &num_active_fields, &active_field_sizes));

  // libCEED objects
  // -- Energy basis
  PetscCall(RatelDebug(ratel, "------ Energy basis"));
  PetscCall(RatelDMPlexCeedBasisCreate(ratel, dm_energy, domain_label, domain_values[0], height_cell, 0, &basis_energy));
  // -- Energy restriction
  PetscCall(RatelDebug(ratel, "------ Energy restriction"));
  PetscCall(RatelDMPlexCeedElemRestrictionCreate(ratel, dm_energy, domain_label, domain_values[0], height_cell, 0, &restriction_energy));
  // -- Solution objects from suboperator
  {
    CeedOperator op_residual_u;

    PetscCall(CeedEvaluatorGetCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
    PetscCall(RatelMaterialGetSolutionData(material, op_residual_u, &num_active_fields, &restrictions_u, &bases_u));
    PetscCall(CeedEvaluatorRestoreCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
  }
  // -- QData
  PetscCall(RatelMaterialGetMeshVolumeQData(material, &restriction_q_data, &q_data));

  // Local energy computation
  {
    CeedQFunction qf_energy;
    CeedOperator  sub_op_strain_energy;

    PetscCall(RatelDebug(ratel, "---- Setting up local energy computation"));

    // -- QFunction
    RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, model_data->strain_energy, model_data->strain_energy_loc, &qf_energy));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_energy, "q data", q_data_size, CEED_EVAL_NONE));
    if (model_data->num_comp_state > 0) {
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_energy, "model state", model_data->num_comp_state, CEED_EVAL_NONE));
    }
    {
      CeedInt eval_mode_index = 0;

      for (PetscInt i = 0; i < num_active_fields; i++) {
        for (PetscInt j = 0; j < active_field_num_eval_modes[i]; j++) {
          char         field_name[PETSC_MAX_PATH_LEN];
          CeedEvalMode eval_mode       = active_field_eval_modes[eval_mode_index++];
          CeedInt      quadrature_size = GetCeedQuadratureSize(eval_mode, dim, active_field_sizes[i]);

          PetscCheck(quadrature_size > 0, ratel->comm, PETSC_ERR_SUP, "Eval mode %s not supported", CeedEvalModes[eval_mode]);
          PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%su, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "d" : "", i));
          RatelCallCeed(ratel, CeedQFunctionAddInput(qf_energy, field_name, quadrature_size, eval_mode));
        }
      }
    }
    RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_energy, "strain energy", num_comp_energy, CEED_EVAL_INTERP));
    RatelCallCeed(ratel, CeedQFunctionSetContext(qf_energy, material->ctx_params));
    RatelCallCeed(ratel, CeedQFunctionSetContextWritable(qf_energy, PETSC_FALSE));
    // -- Operator
    RatelCallCeed(ratel, CeedOperatorCreate(ceed, qf_energy, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &sub_op_strain_energy));
    PetscCall(RatelMaterialSetOperatorName(material, "volumetric term", sub_op_strain_energy));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_strain_energy, "q data", restriction_q_data, CEED_BASIS_NONE, q_data));
    {
      CeedInt eval_mode_index = 0;

      for (PetscInt i = 0; i < num_active_fields; i++) {
        for (PetscInt j = 0; j < active_field_num_eval_modes[i]; j++) {
          char         field_name[PETSC_MAX_PATH_LEN];
          CeedEvalMode eval_mode = active_field_eval_modes[eval_mode_index++];

          PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%su, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "d" : "", i));
          RatelCallCeed(ratel, CeedOperatorSetField(sub_op_strain_energy, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
        }
      }
    }
    // Add state field components to the energy QFunction
    if (model_data->num_comp_state > 0) {
      const char         *name = "model state";
      CeedVector          state_values;
      CeedElemRestriction restriction_state_values;
      CeedOperator        op_residual_u;

      PetscCall(CeedEvaluatorGetCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
      PetscCall(RatelMaterialGetStateData(material, op_residual_u, &restriction_state_values, &state_values));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_strain_energy, name, restriction_state_values, CEED_BASIS_NONE, state_values));
      RatelCallCeed(ratel, CeedVectorDestroy(&state_values));
      RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_state_values));
      PetscCall(CeedEvaluatorRestoreCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
    }
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_strain_energy, "strain energy", restriction_energy, basis_energy, CEED_VECTOR_ACTIVE));
    // -- Add to composite operator
    RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_strain_energy, sub_op_strain_energy));
    if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_strain_energy, stdout));
    // -- Cleanup
    RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_energy));
    RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_strain_energy));
  }

  // Cleanup
  PetscCall(DMDestroy(&dm_solution));
  RatelCallCeed(ratel, CeedVectorDestroy(&q_data));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_energy));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_q_data));
  for (PetscInt i = 0; i < num_active_fields; i++) {
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restrictions_u[i]));
    RatelCallCeed(ratel, CeedBasisDestroy(&bases_u[i]));
  }

  RatelCallCeed(ratel, CeedBasisDestroy(&basis_energy));
  PetscCall(PetscFree(restrictions_u));
  PetscCall(PetscFree(bases_u));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Energy Suboperator FEM Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup diagnostic value `CeedOperator` for `RatelMaterial`.

  Collective across MPI processes.

  @param[in]   material                 `RatelMaterial` context
  @param[in]   dm_projected_diagnostic  `DM` for projected diagnostic value computation
  @param[in]   dm_dual_diagnostic       `DM` for dual space diagnostic value computation
  @param[out]  op_mass_diagnostic       Composite diagnostic value projection `CeedOperator` to add `RatelMaterial` suboperator
  @param[out]  op_projected_diagnostic  Composite projected diagnostic value `CeedOperator` to add `RatelMaterial` suboperator
  @param[out]  op_dual_diagnostic       Composite dual space diagnostic value `CeedOperator` to add `RatelMaterial` suboperator
  @param[out]  op_dual_nodal_scale      Composite dual space nodal scale `CeedOperator` to add `RatelMaterial` suboperator

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupDiagnosticSuboperators_FEM(RatelMaterial material, DM dm_projected_diagnostic, DM dm_dual_diagnostic,
                                                            CeedOperator op_mass_diagnostic, CeedOperator op_projected_diagnostic,
                                                            CeedOperator op_dual_diagnostic, CeedOperator op_dual_nodal_scale) {
  Ratel          ratel      = material->ratel;
  RatelModelData model_data = material->model_data;
  DM             dm_solution;
  const char    *volume_label_name;
  DMLabel        domain_label;
  PetscInt       dim, *domain_values;
  const PetscInt height_cell = 0;
  Ceed           ceed        = ratel->ceed;
  CeedInt        num_active_fields, num_comp_projected_diagnostic = model_data->num_comp_projected_diagnostic,
                             num_comp_dual_diagnostic = model_data->num_comp_dual_diagnostic, q_data_size = model_data->q_data_volume_size;
  const CeedInt       *active_field_sizes;
  CeedVector           q_data         = NULL;
  CeedElemRestriction *restrictions_u = NULL, restriction_q_data = NULL;
  CeedBasis           *bases_u = NULL;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Diagnostic Suboperator FEM"));

  // DM information
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));
  PetscCall(DMGetDimension(dm_solution, &dim));
  PetscCall(RatelMaterialGetVolumeLabelValues(material, NULL, &domain_values));
  PetscCall(RatelMaterialGetVolumeLabelName(material, &volume_label_name));
  PetscCall(DMGetLabel(dm_solution, volume_label_name, &domain_label));

  // Active fields
  PetscCall(RatelMaterialGetActiveFieldSizes(material, &num_active_fields, &active_field_sizes));

  // libCEED objects
  // -- Solution objects from suboperator
  {
    CeedOperator op_residual_u;

    PetscCall(CeedEvaluatorGetCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
    PetscCall(RatelMaterialGetSolutionData(material, op_residual_u, &num_active_fields, &restrictions_u, &bases_u));
    PetscCall(CeedEvaluatorRestoreCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
  }
  // -- QData
  PetscCall(RatelMaterialGetMeshVolumeQData(material, &restriction_q_data, &q_data));

  // Diagnostic value computation, projected
  PetscCall(RatelDebug(ratel, "---- Setting up projected diagnostic value computation"));
  {
    CeedElemRestriction  restriction_diagnostic;
    CeedBasis            basis_diagnostic;
    CeedQFunctionContext ctx_mass_diagnostic;
    CeedQFunction        qf_mass_diagnostic, qf_diagnostic;
    CeedOperator         sub_op_mass_diagnostic, sub_op_diagnostic;

    // -- Diagnostic basis
    PetscCall(RatelDebug(ratel, "------ Diagnostics basis"));
    PetscCall(RatelDMPlexCeedBasisCreate(ratel, dm_projected_diagnostic, domain_label, domain_values[0], height_cell, 0, &basis_diagnostic));
    // -- Diagnostic data restriction
    PetscCall(RatelDebug(ratel, "------ Diagnostics restriction"));
    PetscCall(RatelDMPlexCeedElemRestrictionCreate(ratel, dm_projected_diagnostic, domain_label, domain_values[0], height_cell, 0,
                                                   &restriction_diagnostic));

    // Mass matrix
    // -- QFunction
    RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, Mass, RatelQFunctionRelativePath(Mass_loc), &qf_mass_diagnostic));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_mass_diagnostic, "q data", q_data_size, CEED_EVAL_NONE));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_mass_diagnostic, "u", num_comp_projected_diagnostic, CEED_EVAL_INTERP));
    RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_mass_diagnostic, "v", num_comp_projected_diagnostic, CEED_EVAL_INTERP));
    RatelCallCeed(ratel, CeedQFunctionContextCreate(ceed, &ctx_mass_diagnostic));
    RatelCallCeed(ratel, CeedQFunctionContextSetData(ctx_mass_diagnostic, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(num_comp_projected_diagnostic),
                                                     &num_comp_projected_diagnostic));
    RatelCallCeed(ratel, CeedQFunctionSetContext(qf_mass_diagnostic, ctx_mass_diagnostic));
    RatelCallCeed(ratel, CeedQFunctionSetContextWritable(qf_mass_diagnostic, PETSC_FALSE));
    RatelCallCeed(ratel, CeedQFunctionSetUserFlopsEstimate(qf_mass_diagnostic, num_comp_projected_diagnostic));
    // -- Operator
    RatelCallCeed(ratel, CeedOperatorCreate(ceed, qf_mass_diagnostic, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &sub_op_mass_diagnostic));
    PetscCall(RatelMaterialSetOperatorName(material, "volumetric term", sub_op_mass_diagnostic));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_mass_diagnostic, "q data", restriction_q_data, CEED_BASIS_NONE, q_data));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_mass_diagnostic, "u", restriction_diagnostic, basis_diagnostic, CEED_VECTOR_ACTIVE));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_mass_diagnostic, "v", restriction_diagnostic, basis_diagnostic, CEED_VECTOR_ACTIVE));
    // -- Add to composite operator
    RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_mass_diagnostic, sub_op_mass_diagnostic));
    if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_mass_diagnostic, stdout));
    // -- Cleanup
    RatelCallCeed(ratel, CeedQFunctionContextDestroy(&ctx_mass_diagnostic));
    RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_mass_diagnostic));
    RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_mass_diagnostic));

    // Diagnostic quantities, projected
    // -- QFunction
    RatelCallCeed(ratel,
                  CeedQFunctionCreateInterior(ceed, 1, model_data->projected_diagnostic, model_data->projected_diagnostic_loc, &qf_diagnostic));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_diagnostic, "q data", q_data_size, CEED_EVAL_NONE));
    if (model_data->num_comp_state > 0) {
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_diagnostic, "model state", model_data->num_comp_state, CEED_EVAL_NONE));
    }
    for (PetscInt i = 0; i < num_active_fields; i++) {
      char field_name[PETSC_MAX_PATH_LEN];

      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "u, field %" PetscInt_FMT, i));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_diagnostic, field_name, active_field_sizes[i], CEED_EVAL_INTERP));
      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "du/dX, field %" PetscInt_FMT, i));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_diagnostic, field_name, active_field_sizes[i] * dim, CEED_EVAL_GRAD));
    }
    RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_diagnostic, "diagnostic", num_comp_projected_diagnostic, CEED_EVAL_INTERP));
    RatelCallCeed(ratel, CeedQFunctionSetContext(qf_diagnostic, material->ctx_params));
    RatelCallCeed(ratel, CeedQFunctionSetContextWritable(qf_diagnostic, PETSC_FALSE));
    // -- Operator
    RatelCallCeed(ratel, CeedOperatorCreate(ceed, qf_diagnostic, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &sub_op_diagnostic));
    PetscCall(RatelMaterialSetOperatorName(material, "volumetric term", sub_op_diagnostic));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_diagnostic, "q data", restriction_q_data, CEED_BASIS_NONE, q_data));
    for (PetscInt i = 0; i < num_active_fields; i++) {
      char field_name[PETSC_MAX_PATH_LEN];

      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "u, field %" PetscInt_FMT, i));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_diagnostic, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "du/dX, field %" PetscInt_FMT, i));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_diagnostic, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
    }

    // Add state field components to the projected diagnostics QFunction
    if (model_data->num_comp_state > 0) {
      const char         *name = "model state";
      CeedVector          state_values;
      CeedElemRestriction restriction_state_values;
      CeedOperator        op_residual_u;

      PetscCall(CeedEvaluatorGetCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
      PetscCall(RatelMaterialGetStateData(material, op_residual_u, &restriction_state_values, &state_values));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_diagnostic, name, restriction_state_values, CEED_BASIS_NONE, state_values));
      RatelCallCeed(ratel, CeedVectorDestroy(&state_values));
      RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_state_values));
      PetscCall(CeedEvaluatorRestoreCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
    }

    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_diagnostic, "diagnostic", restriction_diagnostic, basis_diagnostic, CEED_VECTOR_ACTIVE));
    // -- Add to composite operator
    RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_projected_diagnostic, sub_op_diagnostic));
    if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_diagnostic, stdout));
    // -- Cleanup
    RatelCallCeed(ratel, CeedBasisDestroy(&basis_diagnostic));
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_diagnostic));
    RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_diagnostic));
    RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_diagnostic));
  }

  // Diagnostic value computation, dual space
  PetscCall(RatelDebug(ratel, "---- Setting up dual space diagnostic value computation"));
  {
    CeedElemRestriction restriction_diagnostic;
    CeedBasis           basis_diagnostic;
    CeedQFunction       qf_diagnostic;
    CeedOperator        sub_op_diagnostic;

    // -- Diagnostic basis
    PetscCall(RatelDebug(ratel, "------ Diagnostics basis"));
    PetscCall(RatelDMPlexCeedBasisCreate(ratel, dm_dual_diagnostic, domain_label, domain_values[0], height_cell, 0, &basis_diagnostic));
    // -- Diagnostic data restriction
    PetscCall(RatelDebug(ratel, "------ Diagnostics restriction"));
    PetscCall(
        RatelDMPlexCeedElemRestrictionCreate(ratel, dm_dual_diagnostic, domain_label, domain_values[0], height_cell, 0, &restriction_diagnostic));

    // -- QFunction
    RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, model_data->dual_diagnostic, model_data->dual_diagnostic_loc, &qf_diagnostic));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_diagnostic, "q data", q_data_size, CEED_EVAL_NONE));
    for (PetscInt i = 0; i < num_active_fields; i++) {
      char field_name[PETSC_MAX_PATH_LEN];

      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "u, field %" PetscInt_FMT, i));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_diagnostic, field_name, active_field_sizes[i], CEED_EVAL_INTERP));
      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "du/dX, field %" PetscInt_FMT, i));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_diagnostic, field_name, active_field_sizes[i] * dim, CEED_EVAL_GRAD));
    }
    RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_diagnostic, "diagnostic", num_comp_dual_diagnostic, CEED_EVAL_INTERP));
    RatelCallCeed(ratel, CeedQFunctionSetContext(qf_diagnostic, material->ctx_params));
    RatelCallCeed(ratel, CeedQFunctionSetContextWritable(qf_diagnostic, PETSC_FALSE));
    // -- Operator
    RatelCallCeed(ratel, CeedOperatorCreate(ceed, qf_diagnostic, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &sub_op_diagnostic));
    PetscCall(RatelMaterialSetOperatorName(material, "volumetric term", sub_op_diagnostic));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_diagnostic, "q data", restriction_q_data, CEED_BASIS_NONE, q_data));
    for (PetscInt i = 0; i < num_active_fields; i++) {
      char field_name[PETSC_MAX_PATH_LEN];

      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "u, field %" PetscInt_FMT, i));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_diagnostic, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "du/dX, field %" PetscInt_FMT, i));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_diagnostic, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
    }
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_diagnostic, "diagnostic", restriction_diagnostic, basis_diagnostic, CEED_VECTOR_ACTIVE));
    // -- Add to composite operator
    RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_dual_diagnostic, sub_op_diagnostic));
    if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_diagnostic, stdout));

    // Dual space nodal scale
    {
      CeedVector           multiplicity_stored, nodal_multiplicity_stored;
      CeedQFunction        qf_dual_nodal_scale;
      CeedOperator         sub_op_dual_nodal_scale;
      CeedQFunctionContext ctx_nodal_scale;

      RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_diagnostic, &multiplicity_stored, NULL));
      RatelCallCeed(ratel, CeedElemRestrictionGetMultiplicity(restriction_diagnostic, multiplicity_stored));

      // -- Create nodal multiplicity vector
      //    This will be filled using CeedCompositeOperatorGetMultiplicity after setup is complete
      RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_diagnostic, &nodal_multiplicity_stored, NULL));

      // -- QFunction
      RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, ScaleLumpedDualDiagnosticTerms,
                                                       RatelQFunctionRelativePath(ScaleLumpedDualDiagnosticTerms_loc), &qf_dual_nodal_scale));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_dual_nodal_scale, "multiplicity", num_comp_dual_diagnostic, CEED_EVAL_NONE));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_dual_nodal_scale, "nodal_multiplicity", num_comp_dual_diagnostic, CEED_EVAL_NONE));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_dual_nodal_scale, "u", num_comp_dual_diagnostic, CEED_EVAL_NONE));
      RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_dual_nodal_scale, "v", num_comp_dual_diagnostic, CEED_EVAL_NONE));
      RatelCallCeed(ratel, CeedQFunctionContextCreate(ceed, &ctx_nodal_scale));
      RatelCallCeed(ratel, CeedQFunctionContextSetData(ctx_nodal_scale, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(num_comp_dual_diagnostic),
                                                       &num_comp_dual_diagnostic));
      RatelCallCeed(ratel, CeedQFunctionSetContext(qf_dual_nodal_scale, ctx_nodal_scale));
      RatelCallCeed(ratel, CeedQFunctionSetContextWritable(qf_dual_nodal_scale, PETSC_FALSE));

      // -- Operator
      RatelCallCeed(ratel, CeedOperatorCreate(ceed, qf_dual_nodal_scale, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &sub_op_dual_nodal_scale));
      PetscCall(RatelMaterialSetOperatorName(material, "volumetric term", sub_op_dual_nodal_scale));
      RatelCallCeed(ratel,
                    CeedOperatorSetField(sub_op_dual_nodal_scale, "multiplicity", restriction_diagnostic, CEED_BASIS_NONE, multiplicity_stored));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_dual_nodal_scale, "nodal_multiplicity", restriction_diagnostic, CEED_BASIS_NONE,
                                                nodal_multiplicity_stored));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_dual_nodal_scale, "u", restriction_diagnostic, CEED_BASIS_NONE, CEED_VECTOR_ACTIVE));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_dual_nodal_scale, "v", restriction_diagnostic, CEED_BASIS_NONE, CEED_VECTOR_ACTIVE));
      // -- Add to composite operator
      RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_dual_nodal_scale, sub_op_dual_nodal_scale));
      if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_dual_nodal_scale, stdout));

      // -- Cleanup
      RatelCallCeed(ratel, CeedQFunctionContextDestroy(&ctx_nodal_scale));
      RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_dual_nodal_scale));
      RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_dual_nodal_scale));
      RatelCallCeed(ratel, CeedVectorDestroy(&multiplicity_stored));
      RatelCallCeed(ratel, CeedVectorDestroy(&nodal_multiplicity_stored));
    }

    // -- Cleanup
    RatelCallCeed(ratel, CeedBasisDestroy(&basis_diagnostic));
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_diagnostic));
    RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_diagnostic));
    RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_diagnostic));
  }

  // Cleanup
  PetscCall(DMDestroy(&dm_solution));
  RatelCallCeed(ratel, CeedVectorDestroy(&q_data));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_q_data));
  for (PetscInt i = 0; i < num_active_fields; i++) {
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restrictions_u[i]));
    RatelCallCeed(ratel, CeedBasisDestroy(&bases_u[i]));
  }
  PetscCall(PetscFree(restrictions_u));
  PetscCall(PetscFree(bases_u));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Diagnostic Suboperator FEM Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup surface force `CeedOperator` for `RatelMaterial`.

  Collective across MPI processes.

  @param[in]   material           `RatelMaterial` context
  @param[in]   dm_surface_force   `DM` for surface force computation
  @param[out]  ops_surface_force  Array of composite surface force `CeedOperator` to add `RatelMaterial` suboperator for each face

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupSurfaceForceCellToFaceSuboperators_FEM(RatelMaterial material, DM dm_surface_force,
                                                                        CeedOperator ops_surface_force[]) {
  Ratel               ratel      = material->ratel;
  RatelModelData      model_data = material->model_data;
  PetscInt            dim;
  const PetscInt      height_face = 1, height_cell = 0;
  Ceed                ceed                        = ratel->ceed;
  CeedInt             num_active_fields           = model_data->num_active_fields;
  const CeedInt      *active_field_sizes          = model_data->active_field_sizes;
  const CeedInt      *active_field_num_eval_modes = model_data->active_field_num_eval_modes;
  const CeedEvalMode *active_field_eval_modes     = model_data->active_field_eval_modes;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Surface Force Suboperator FEM"));

  // DM information
  PetscCall(DMGetDimension(dm_surface_force, &dim));

  // Surface force computation
  for (PetscInt i = 0; i < ratel->surface_force_face_count; i++) {
    PetscInt dm_face = ratel->surface_force_face_label_values[i];

    PetscCall(RatelDebug(ratel, "---- Setting up surface %" PetscInt_FMT " force computation", dm_face));

    // Domain label for Face Sets
    const char *face_domain_label_name = NULL;
    DMLabel     face_domain_label      = NULL;

    PetscCall(RatelMaterialGetSurfaceGradientDiagnosticLabelName(material, dm_face, &face_domain_label_name));
    PetscCall(DMGetLabel(dm_surface_force, face_domain_label_name, &face_domain_label));
    PetscCheck(face_domain_label, ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "No DM label corresponding to name %s found", face_domain_label_name);

    // Loop over all face orientations
    for (PetscInt orientation = 0; orientation < material->num_face_orientations; orientation++) {
      CeedInt             q_data_size = model_data->q_data_surface_grad_size, num_comp_u = active_field_sizes[0];
      CeedVector          q_data                                = NULL;
      CeedElemRestriction restrictions_u_face[RATEL_MAX_FIELDS] = {NULL}, restrictions_u_cell_to_face[RATEL_MAX_FIELDS], restriction_q_data = NULL;
      CeedBasis           bases_u_face[RATEL_MAX_FIELDS] = {NULL}, bases_u_cell_to_face[RATEL_MAX_FIELDS];
      CeedQFunction       qf_surface_force;
      CeedOperator        sub_op_surface_force;

      PetscCall(RatelDebug(ratel, "------ Face orientation %" PetscInt_FMT, orientation));

      // Check for face on process
      {
        PetscInt first_point_local = -1, first_point_global = -1, ids[1] = {orientation};

        PetscCall(DMGetFirstLabeledPoint(dm_surface_force, dm_surface_force, face_domain_label, 1, ids, height_cell, &first_point_local, NULL));
        PetscCall(MPIU_Allreduce(&first_point_local, &first_point_global, 1, MPIU_INT, MPI_MAX, ratel->comm));
        if (first_point_global == -1) {
          PetscCall(RatelDebug(ratel, "-------- Skipping, no process uses this face orientation"));
          continue;
        }
      }
      // -- QData
      PetscCall(RatelMaterialGetSurfaceGradientDiagnosticQData(material, dm_surface_force, dm_face, orientation, &restriction_q_data, &q_data));

      // Diagnostic quantities
      PetscCall(RatelDebug(ratel, "-------- Setting up surface force operator"));
      // -- Bases
      for (CeedInt j = 0; j < num_active_fields; j++) {
        PetscCall(RatelDMPlexCeedBasisCreate(ratel, dm_surface_force, face_domain_label, orientation, height_face, j, &bases_u_face[j]));
        PetscCall(
            RatelDMPlexCeedBasisCellToFaceCreate(ratel, dm_surface_force, face_domain_label, orientation, orientation, j, &bases_u_cell_to_face[j]));
      }
      // -- ElemRestriction
      for (CeedInt j = 0; j < num_active_fields; j++) {
        PetscCall(
            RatelDMPlexCeedElemRestrictionCreate(ratel, dm_surface_force, face_domain_label, orientation, height_face, j, &restrictions_u_face[j]));
        PetscCall(RatelDMPlexCeedElemRestrictionCreate(ratel, dm_surface_force, face_domain_label, orientation, height_cell, j,
                                                       &restrictions_u_cell_to_face[j]));
      }
      // -- QFunction
      RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, model_data->surface_force_cell_to_face, model_data->surface_force_cell_to_face_loc,
                                                       &qf_surface_force));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_surface_force, "q data", q_data_size, CEED_EVAL_NONE));
      {
        CeedInt eval_mode_index = 0;

        for (PetscInt i = 0; i < num_active_fields; i++) {
          for (PetscInt j = 0; j < active_field_num_eval_modes[i]; j++) {
            char         field_name[PETSC_MAX_PATH_LEN];
            CeedEvalMode eval_mode       = active_field_eval_modes[eval_mode_index++];
            CeedInt      quadrature_size = GetCeedQuadratureSize(eval_mode, dim, active_field_sizes[i]);

            PetscCheck(quadrature_size > 0, ratel->comm, PETSC_ERR_SUP, "Eval mode %s not supported", CeedEvalModes[eval_mode]);
            PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "du/dX" : "u", i));
            RatelCallCeed(ratel, CeedQFunctionAddInput(qf_surface_force, field_name, quadrature_size, eval_mode));
          }
        }
        RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_surface_force, "surface force", num_comp_u, CEED_EVAL_INTERP));
      }
      RatelCallCeed(ratel, CeedQFunctionSetContext(qf_surface_force, material->ctx_params));
      RatelCallCeed(ratel, CeedQFunctionSetContextWritable(qf_surface_force, PETSC_FALSE));
      // -- Operator
      RatelCallCeed(ratel, CeedOperatorCreate(ceed, qf_surface_force, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &sub_op_surface_force));
      {
        char operator_name[PETSC_MAX_PATH_LEN];

        PetscCall(PetscSNPrintf(operator_name, sizeof(operator_name), "surface %" PetscInt_FMT " force computation", dm_face));
        PetscCall(RatelMaterialSetOperatorName(material, operator_name, sub_op_surface_force));
      }
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_surface_force, "q data", restriction_q_data, CEED_BASIS_NONE, q_data));
      {
        CeedInt eval_mode_index = 0;

        for (PetscInt i = 0; i < num_active_fields; i++) {
          for (PetscInt j = 0; j < active_field_num_eval_modes[i]; j++) {
            char         field_name[PETSC_MAX_PATH_LEN];
            CeedEvalMode eval_mode = active_field_eval_modes[eval_mode_index++];

            PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "du/dX" : "u", i));
            if (eval_mode == CEED_EVAL_INTERP) {
              RatelCallCeed(ratel,
                            CeedOperatorSetField(sub_op_surface_force, field_name, restrictions_u_face[i], bases_u_face[i], CEED_VECTOR_ACTIVE));
            } else {
              RatelCallCeed(ratel, CeedOperatorSetField(sub_op_surface_force, field_name, restrictions_u_cell_to_face[i], bases_u_cell_to_face[i],
                                                        CEED_VECTOR_ACTIVE));
            }
          }
        }
      }
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_surface_force, "surface force", restrictions_u_face[0], bases_u_face[0], CEED_VECTOR_ACTIVE));
      // -- Add to composite operator
      RatelCallCeed(ratel, CeedCompositeOperatorAddSub(ops_surface_force[i], sub_op_surface_force));
      if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_surface_force, stdout));

      // -- Cleanup
      RatelCallCeed(ratel, CeedVectorDestroy(&q_data));
      for (CeedInt j = 0; j < num_active_fields; j++) {
        RatelCallCeed(ratel, CeedBasisDestroy(&bases_u_face[j]));
        RatelCallCeed(ratel, CeedBasisDestroy(&bases_u_cell_to_face[j]));
      }
      RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_q_data));
      for (CeedInt j = 0; j < num_active_fields; j++) {
        RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restrictions_u_face[j]));
        RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restrictions_u_cell_to_face[j]));
      }
      RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_surface_force));
      RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_surface_force));
    }
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Surface Force Suboperator FEM Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup MMS error `CeedOperator` for `RatelMaterial`

  @param[in]   material      `RatelMaterial` context
  @param[out]  op_mms_error  Composite MMS error `CeedOperator` to add `RatelMaterial` suboperator

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupMMSErrorSuboperator_FEM(RatelMaterial material, CeedOperator op_mms_error) {
  Ratel                ratel      = material->ratel;
  RatelModelData       model_data = material->model_data;
  DM                   dm_solution;
  const char          *volume_label_name;
  DMLabel              domain_label;
  PetscInt             dim, *domain_values;
  const PetscInt       height_cell = 0;
  const PetscScalar   *x_array;
  Vec                  X;
  Ceed                 ceed = ratel->ceed;
  CeedInt              num_active_fields, num_comp_x, q_data_size = model_data->q_data_volume_size;
  const CeedInt       *active_field_sizes;
  CeedVector           q_data         = NULL, x;
  CeedElemRestriction *restrictions_u = NULL, restriction_q_data = NULL, restriction_x;
  CeedBasis           *bases_u = NULL, basis_x;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup MMS Error Suboperator FEM"));

  // DM information
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));
  PetscCall(DMGetDimension(dm_solution, &dim));
  num_comp_x = dim;
  PetscCall(RatelMaterialGetVolumeLabelValues(material, NULL, &domain_values));
  PetscCall(RatelMaterialGetVolumeLabelName(material, &volume_label_name));
  PetscCall(DMGetLabel(dm_solution, volume_label_name, &domain_label));

  // Active fields
  PetscCall(RatelMaterialGetActiveFieldSizes(material, &num_active_fields, &active_field_sizes));

  // libCEED objects
  // -- Coordinate basis
  PetscCall(RatelDebug(ratel, "---- Coordinate basis"));
  PetscCall(RatelDMPlexCeedBasisCoordinateCreate(ratel, dm_solution, domain_label, domain_values[0], height_cell, &basis_x));
  // -- Coordinate restriction
  PetscCall(RatelDebug(ratel, "---- Coordinate restriction"));
  PetscCall(RatelDMPlexCeedElemRestrictionCoordinateCreate(ratel, dm_solution, domain_label, domain_values[0], height_cell, &restriction_x));
  // -- Solution objects from suboperator
  {
    CeedOperator op_residual_u;

    PetscCall(CeedEvaluatorGetCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
    PetscCall(RatelMaterialGetSolutionData(material, op_residual_u, &num_active_fields, &restrictions_u, &bases_u));
    PetscCall(CeedEvaluatorRestoreCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
  }
  // -- QData
  PetscCall(RatelMaterialGetVolumeQData(material, &restriction_q_data, &q_data));

  // Element coordinates
  PetscCall(RatelDebug(ratel, "---- Retrieving element coordinates"));
  PetscCall(DMGetCoordinatesLocal(dm_solution, &X));
  PetscCall(VecGetArrayRead(X, &x_array));
  RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_x, &x, NULL));
  RatelCallCeed(ratel, CeedVectorSetArray(x, CEED_MEM_HOST, CEED_COPY_VALUES, (CeedScalar *)x_array));
  PetscCall(VecRestoreArrayRead(X, &x_array));

  // Error calculation, for MMS
  if (model_data->mms_error) {
    CeedQFunction qf_mms_error;
    CeedOperator  sub_op_mms_error;

    PetscCall(RatelDebug(ratel, "---- Setting up error computation for MMS"));

    // -- QFunction
    RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, model_data->mms_error, model_data->mms_error_loc, &qf_mms_error));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_mms_error, "q data", q_data_size, CEED_EVAL_NONE));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_mms_error, "x", num_comp_x, CEED_EVAL_INTERP));
    for (PetscInt i = 0; i < num_active_fields; i++) {
      char field_name[PETSC_MAX_PATH_LEN];

      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "u, field %" PetscInt_FMT, i));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_mms_error, field_name, active_field_sizes[i], CEED_EVAL_INTERP));
      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "l2 error, field %" PetscInt_FMT, i));
      RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_mms_error, field_name, active_field_sizes[i], CEED_EVAL_INTERP));
    }
    // -- QFunctionContext
    if (material->ctx_mms_params) {
      RatelCallCeed(ratel, CeedQFunctionSetContext(qf_mms_error, material->ctx_mms_params));
      RatelCallCeed(ratel, CeedQFunctionSetContextWritable(qf_mms_error, PETSC_FALSE));
    }

    // -- Operator
    RatelCallCeed(ratel, CeedOperatorCreate(ceed, qf_mms_error, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &sub_op_mms_error));
    PetscCall(RatelMaterialSetOperatorName(material, "volumetric term", sub_op_mms_error));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_mms_error, "q data", restriction_q_data, CEED_BASIS_NONE, q_data));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_mms_error, "x", restriction_x, basis_x, x));
    for (PetscInt i = 0; i < num_active_fields; i++) {
      char field_name[PETSC_MAX_PATH_LEN];

      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "u, field %" PetscInt_FMT, i));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_mms_error, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "l2 error, field %" PetscInt_FMT, i));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_mms_error, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
    }
    // -- Add to composite operator
    RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_mms_error, sub_op_mms_error));
    if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_mms_error, stdout));
    // -- Cleanup
    RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_mms_error));
    RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_mms_error));
  }

  // Cleanup
  PetscCall(DMDestroy(&dm_solution));
  RatelCallCeed(ratel, CeedVectorDestroy(&x));
  RatelCallCeed(ratel, CeedVectorDestroy(&q_data));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_q_data));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_x));
  for (PetscInt i = 0; i < num_active_fields; i++) {
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restrictions_u[i]));
    RatelCallCeed(ratel, CeedBasisDestroy(&bases_u[i]));
  }
  RatelCallCeed(ratel, CeedBasisDestroy(&basis_x));
  PetscCall(PetscFree(restrictions_u));
  PetscCall(PetscFree(bases_u));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup MMS Error Suboperator FEM Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Add forcing term to residual u term operator.

  Collective across MPI processes.

  @param[in]   material     `RatelMaterial` context
  @param[out]  op_residual  Composite residual u term `CeedOperator` to add `RatelMaterial` sub-operator

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupForcingSuboperator_FEM(RatelMaterial material, CeedOperator op_residual) {
  Ratel                ratel = material->ratel;
  DM                   dm_solution;
  RatelModelData       model_data = material->model_data;
  DMLabel              domain_label;
  PetscInt             dim, *domain_values;
  CeedInt              num_active_fields, q_data_size = model_data->q_data_volume_size, num_forcing = model_data->num_forcing;
  const CeedInt       *active_field_sizes = model_data->active_field_sizes;
  CeedVector           q_data             = NULL;
  CeedBasis           *bases_u            = NULL;
  CeedElemRestriction *restrictions_u = NULL, restriction_q_data = NULL;
  CeedQFunctionContext ctx_forcing = NULL;
  CeedQFunction        qf_forcing  = NULL;
  CeedOperator         sub_op_forcing;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Forcing Sub-operator"));

  // Create the QFunction and Operator that computes the forcing term for the residual evaluator
  // DM information
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));
  PetscCall(DMGetDimension(dm_solution, &dim));
  PetscCall(RatelMaterialGetVolumeLabelValues(material, NULL, &domain_values));
  PetscCall(DMGetLabel(dm_solution, material->volume_label_name, &domain_label));

  // libCEED objects
  // -- Solution objects from sub-operator
  PetscCall(RatelMaterialGetSolutionData(material, op_residual, &num_active_fields, &restrictions_u, &bases_u));
  // -- QData
  PetscCall(RatelMaterialGetVolumeQData(material, &restriction_q_data, &q_data));

  // Forcing choice specific options
  switch (material->forcing_type) {
    // LCOV_EXCL_START
    case RATEL_FORCING_NONE:
      break;  // Excluded above
    // LCOV_EXCL_STOP
    case RATEL_FORCING_BODY: {
      RatelForcingBodyParams params_forcing;

      // -- Read in forcing vector(s)
      PetscCall(RatelMaterialForcingBodyDataFromOptions(material, &params_forcing));

      // -- QFunctionContext
      RatelCallCeed(ratel, CeedQFunctionContextCreate(ratel->ceed, &ctx_forcing));
      RatelCallCeed(ratel, CeedQFunctionContextSetData(ctx_forcing, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(params_forcing), &params_forcing));
      RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(ctx_forcing, "acceleration vectors", offsetof(RatelForcingBodyParams, acceleration),
                                                              3 * params_forcing.num_times, "vector describing traction in (x, y, z) direction"));
      RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(ctx_forcing, "times", offsetof(RatelForcingBodyParams, times), params_forcing.num_times,
                                                              "transition time values"));
      RatelCallCeed(ratel, CeedQFunctionContextRegisterInt32(ctx_forcing, "num times", offsetof(RatelForcingBodyParams, num_times), 1,
                                                             "number of time values and direction vectors"));
      RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(ctx_forcing, "time", offsetof(RatelForcingBodyParams, time), 1, "current solver time"));

      // -- QFunction
      RatelCallCeed(ratel, CeedQFunctionCreateInterior(ratel->ceed, 1, BodyForce, RatelQFunctionRelativePath(BodyForce_loc), &qf_forcing));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_forcing, "q data", q_data_size, CEED_EVAL_NONE));
      break;
    }
    case RATEL_FORCING_MMS: {
      // -- Check support
      PetscCheck(model_data->mms_forcing, ratel->comm, PETSC_ERR_SUP, "MMS forcing function for material '%s' not found", material->name);

      // -- QFunctionContext
      if (material->ctx_mms_params) {
        RatelCallCeed(ratel, CeedQFunctionContextReference(material->ctx_mms_params));
        ctx_forcing = material->ctx_mms_params;
      }

      // -- QFunction
      RatelCallCeed(ratel, CeedQFunctionCreateInterior(ratel->ceed, 1, model_data->mms_forcing, model_data->mms_forcing_loc, &qf_forcing));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_forcing, "q data", q_data_size, CEED_EVAL_NONE));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_forcing, "x", dim, CEED_EVAL_INTERP));
      break;
    }
  }
  // -- Common QFunction output
  for (PetscInt i = 0; i < num_forcing; i++) {
    char field_name[PETSC_MAX_PATH_LEN];

    PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "force, field %" PetscInt_FMT, i));
    RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_forcing, field_name, active_field_sizes[i], CEED_EVAL_INTERP));
  }

  // -- QFunctionContext
  RatelCallCeed(ratel, CeedQFunctionSetContext(qf_forcing, ctx_forcing));
  RatelCallCeed(ratel, CeedQFunctionSetContextWritable(qf_forcing, PETSC_FALSE));
  if (ctx_forcing && ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedQFunctionContextView(ctx_forcing, stdout));
  RatelCallCeed(ratel, CeedQFunctionContextDestroy(&ctx_forcing));

  // -- Operator
  RatelCallCeed(ratel, CeedOperatorCreate(ratel->ceed, qf_forcing, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &sub_op_forcing));
  PetscCall(RatelMaterialSetOperatorName(material, "forcing", sub_op_forcing));
  RatelCallCeed(ratel, CeedOperatorSetField(sub_op_forcing, "q data", restriction_q_data, CEED_BASIS_NONE, q_data));
  for (PetscInt i = 0; i < num_forcing; i++) {
    char field_name[PETSC_MAX_PATH_LEN];

    PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "force, field %" PetscInt_FMT, i));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_forcing, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
  }
  if (material->forcing_type == RATEL_FORCING_MMS) {
    const PetscInt      height_cell = 0;
    CeedVector          x_loc;
    CeedElemRestriction restriction_x;
    CeedBasis           basis_x;

    // libCEED objects
    // -- Coordinate basis
    PetscCall(RatelDebug(ratel, "---- Coordinate basis"));
    PetscCall(RatelDMPlexCeedBasisCoordinateCreate(ratel, dm_solution, domain_label, domain_values[0], height_cell, &basis_x));
    // -- Coordinate restriction
    PetscCall(RatelDebug(ratel, "---- Coordinate restriction"));
    PetscCall(RatelDMPlexCeedElemRestrictionCoordinateCreate(ratel, dm_solution, domain_label, domain_values[0], height_cell, &restriction_x));

    // Element coordinates
    {
      const PetscScalar *x_array;
      Vec                X_loc;

      PetscCall(RatelDebug(ratel, "---- Retrieving element coordinates"));
      PetscCall(DMGetCoordinatesLocal(dm_solution, &X_loc));
      PetscCall(VecGetArrayRead(X_loc, &x_array));
      RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_x, &x_loc, NULL));
      RatelCallCeed(ratel, CeedVectorSetArray(x_loc, CEED_MEM_HOST, CEED_COPY_VALUES, (CeedScalar *)x_array));
      PetscCall(VecRestoreArrayRead(X_loc, &x_array));
    }

    // -- Input field
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_forcing, "x", restriction_x, basis_x, x_loc));

    // -- Cleanup
    RatelCallCeed(ratel, CeedVectorDestroy(&x_loc));
    RatelCallCeed(ratel, CeedBasisDestroy(&basis_x));
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_x));
  }
  // Add to composite operator
  RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_residual, sub_op_forcing));
  if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_forcing, stdout));

  // Cleanup
  PetscCall(DMDestroy(&dm_solution));
  RatelCallCeed(ratel, CeedVectorDestroy(&q_data));
  for (PetscInt i = 0; i < num_active_fields; i++) {
    RatelCallCeed(ratel, CeedBasisDestroy(&bases_u[i]));
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restrictions_u[i]));
  }
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_q_data));
  RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_forcing));
  RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_forcing));
  PetscCall(PetscFree(restrictions_u));
  PetscCall(PetscFree(bases_u));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Forcing Sub-operator Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Add forcing energy to external energy operator.

  Collective across MPI processes.

  @param[in]   material            `RatelMaterial` context
  @param[in]   dm_energy           `DM` for external energy computation
  @param[out]  op_external_energy  Composite external energy `CeedOperator` to add energy of body force sub-operators

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupForcingEnergySuboperator_FEM(RatelMaterial material, DM dm_energy, CeedOperator op_external_energy) {
  Ratel                ratel = material->ratel;
  DM                   dm_solution;
  RatelModelData       model_data = material->model_data;
  DMLabel              domain_label;
  PetscInt             dim, *domain_values;
  const PetscInt       height_cell = 0;
  CeedInt              num_active_fields, num_comp_energy = 1, q_data_size = model_data->q_data_volume_size, num_forcing = model_data->num_forcing;
  const CeedInt       *active_field_sizes = model_data->active_field_sizes;
  CeedVector           q_data             = NULL;
  CeedBasis           *bases_u            = NULL, basis_energy;
  CeedElemRestriction *restrictions_u = NULL, restriction_q_data = NULL, restriction_energy;
  CeedQFunctionContext ctx_forcing       = NULL;
  CeedQFunction        qf_forcing_energy = NULL;
  CeedOperator         sub_op_forcing_energy;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Forcing Energy Sub-operator"));

  // Create the QFunction and Operator that computes the forcing term for the external energy evaluator
  // DM information
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));
  PetscCall(DMGetDimension(dm_solution, &dim));
  PetscCall(RatelMaterialGetVolumeLabelValues(material, NULL, &domain_values));
  PetscCall(DMGetLabel(dm_solution, material->volume_label_name, &domain_label));

  // libCEED objects
  // -- Energy basis
  PetscCall(RatelDebug(ratel, "------ Energy basis"));
  PetscCall(RatelDMPlexCeedBasisCreate(ratel, dm_energy, domain_label, domain_values[0], height_cell, 0, &basis_energy));
  // -- Energy restriction
  PetscCall(RatelDebug(ratel, "------ Energy restriction"));
  PetscCall(RatelDMPlexCeedElemRestrictionCreate(ratel, dm_energy, domain_label, domain_values[0], height_cell, 0, &restriction_energy));
  // -- Solution objects from sub-operator
  {
    CeedOperator op_residual_u;

    PetscCall(CeedEvaluatorGetCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
    PetscCall(RatelMaterialGetSolutionData(material, op_residual_u, &num_active_fields, &restrictions_u, &bases_u));
    PetscCall(CeedEvaluatorRestoreCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
  }
  // -- QData
  PetscCall(RatelMaterialGetVolumeQData(material, &restriction_q_data, &q_data));

  // Forcing choice specific options
  switch (material->forcing_type) {
    // LCOV_EXCL_START
    case RATEL_FORCING_NONE:
      break;  // Excluded above
    // LCOV_EXCL_STOP
    case RATEL_FORCING_BODY: {
      RatelForcingBodyParams params_forcing;

      // -- Read in forcing vector(s)
      PetscCall(RatelMaterialForcingBodyDataFromOptions(material, &params_forcing));

      // -- QFunctionContext
      RatelCallCeed(ratel, CeedQFunctionContextCreate(ratel->ceed, &ctx_forcing));
      RatelCallCeed(ratel, CeedQFunctionContextSetData(ctx_forcing, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(params_forcing), &params_forcing));
      RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(ctx_forcing, "acceleration vectors", offsetof(RatelForcingBodyParams, acceleration),
                                                              3 * params_forcing.num_times, "vector describing traction in (x, y, z) direction"));
      RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(ctx_forcing, "times", offsetof(RatelForcingBodyParams, times), params_forcing.num_times,
                                                              "transition time values"));
      RatelCallCeed(ratel, CeedQFunctionContextRegisterInt32(ctx_forcing, "num times", offsetof(RatelForcingBodyParams, num_times), 1,
                                                             "number of time values and direction vectors"));
      RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(ctx_forcing, "time", offsetof(RatelForcingBodyParams, time), 1, "current solver time"));

      // -- QFunction
      RatelCallCeed(
          ratel, CeedQFunctionCreateInterior(ratel->ceed, 1, BodyForceEnergy, RatelQFunctionRelativePath(BodyForceEnergy_loc), &qf_forcing_energy));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_forcing_energy, "q data", q_data_size, CEED_EVAL_NONE));
      break;
    }
    case RATEL_FORCING_MMS: {
      // -- Check support
      PetscCheck(model_data->mms_forcing_energy, ratel->comm, PETSC_ERR_SUP, "MMS forcing function for material '%s' not found", material->name);

      // -- QFunctionContext
      if (material->ctx_mms_params) {
        RatelCallCeed(ratel, CeedQFunctionContextReference(material->ctx_mms_params));
        ctx_forcing = material->ctx_mms_params;
      }

      // -- QFunction
      RatelCallCeed(
          ratel, CeedQFunctionCreateInterior(ratel->ceed, 1, model_data->mms_forcing_energy, model_data->mms_forcing_energy_loc, &qf_forcing_energy));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_forcing_energy, "q data", q_data_size, CEED_EVAL_NONE));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_forcing_energy, "x", dim, CEED_EVAL_INTERP));
      break;
    }
  }
  // -- Common QFunction input/output
  for (PetscInt i = 0; i < num_forcing; i++) {
    char field_name[PETSC_MAX_PATH_LEN];

    PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "force energy, field %" PetscInt_FMT, i));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_forcing_energy, field_name, active_field_sizes[i], CEED_EVAL_INTERP));
  }
  RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_forcing_energy, "external energy", num_comp_energy, CEED_EVAL_INTERP));

  // -- QFunctionContext
  RatelCallCeed(ratel, CeedQFunctionSetContext(qf_forcing_energy, ctx_forcing));
  RatelCallCeed(ratel, CeedQFunctionSetContextWritable(qf_forcing_energy, PETSC_FALSE));
  if (ctx_forcing && ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedQFunctionContextView(ctx_forcing, stdout));
  RatelCallCeed(ratel, CeedQFunctionContextDestroy(&ctx_forcing));

  // -- Operator
  RatelCallCeed(ratel, CeedOperatorCreate(ratel->ceed, qf_forcing_energy, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &sub_op_forcing_energy));
  PetscCall(RatelMaterialSetOperatorName(material, "forcing energy", sub_op_forcing_energy));
  RatelCallCeed(ratel, CeedOperatorSetField(sub_op_forcing_energy, "q data", restriction_q_data, CEED_BASIS_NONE, q_data));
  for (PetscInt i = 0; i < num_forcing; i++) {
    char field_name[PETSC_MAX_PATH_LEN];

    PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "force energy, field %" PetscInt_FMT, i));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_forcing_energy, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
  }
  if (material->forcing_type == RATEL_FORCING_MMS) {
    const PetscInt      height_cell = 0;
    CeedVector          x_loc;
    CeedElemRestriction restriction_x;
    CeedBasis           basis_x;

    // libCEED objects
    // -- Coordinate basis
    PetscCall(RatelDebug(ratel, "---- Coordinate basis"));
    PetscCall(RatelDMPlexCeedBasisCoordinateCreate(ratel, dm_solution, domain_label, domain_values[0], height_cell, &basis_x));
    // -- Coordinate restriction
    PetscCall(RatelDebug(ratel, "---- Coordinate restriction"));
    PetscCall(RatelDMPlexCeedElemRestrictionCoordinateCreate(ratel, dm_solution, domain_label, domain_values[0], height_cell, &restriction_x));

    // Element coordinates
    {
      const PetscScalar *x_array;
      Vec                X_loc;

      PetscCall(RatelDebug(ratel, "---- Retrieving element coordinates"));
      PetscCall(DMGetCoordinatesLocal(dm_solution, &X_loc));
      PetscCall(VecGetArrayRead(X_loc, &x_array));
      RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_x, &x_loc, NULL));
      RatelCallCeed(ratel, CeedVectorSetArray(x_loc, CEED_MEM_HOST, CEED_COPY_VALUES, (CeedScalar *)x_array));
      PetscCall(VecRestoreArrayRead(X_loc, &x_array));
    }

    // -- Input field
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_forcing_energy, "x", restriction_x, basis_x, x_loc));

    // -- Cleanup
    RatelCallCeed(ratel, CeedVectorDestroy(&x_loc));
    RatelCallCeed(ratel, CeedBasisDestroy(&basis_x));
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_x));
  }
  RatelCallCeed(ratel, CeedOperatorSetField(sub_op_forcing_energy, "external energy", restriction_energy, basis_energy, CEED_VECTOR_ACTIVE));
  // Add to composite operator
  RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_external_energy, sub_op_forcing_energy));
  if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_forcing_energy, stdout));

  // Cleanup
  PetscCall(DMDestroy(&dm_solution));
  RatelCallCeed(ratel, CeedVectorDestroy(&q_data));
  for (PetscInt i = 0; i < num_active_fields; i++) {
    RatelCallCeed(ratel, CeedBasisDestroy(&bases_u[i]));
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restrictions_u[i]));
  }
  RatelCallCeed(ratel, CeedBasisDestroy(&basis_energy));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_energy));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_q_data));
  RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_forcing_energy));
  RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_forcing_energy));
  PetscCall(PetscFree(restrictions_u));
  PetscCall(PetscFree(bases_u));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Forcing Energy Sub-operator Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}
