/// @file
/// NeoHookean material (initial configuration) with damage phase field model - four components

#include <ceed.h>
#include <petscsys.h>
#include <ratel-impl.h>
#include <ratel-model.h>
#include <ratel.h>
#include <ratel/qfunctions/geometry/surface-force.h>
#include <ratel/qfunctions/geometry/surface.h>
#include <ratel/qfunctions/geometry/volumetric.h>
#include <ratel/qfunctions/models/elasticity-damage-common-utt.h>
#include <ratel/qfunctions/models/elasticity-neo-hookean-damage-initial.h>

static const CeedInt      active_field_sizes[]          = {4};
static const char *const  active_field_names[]          = {"solution"};
static const char *const  active_component_names[]      = {"displacement_x", "displacement_y", "displacement_z", "damage"};
static const CeedInt      active_field_num_eval_modes[] = {NUM_ACTIVE_FIELD_EVAL_MODES_NeoHookean_Damage};
static const CeedEvalMode active_field_eval_modes[]     = {CEED_EVAL_INTERP, CEED_EVAL_GRAD};
static const CeedInt      ut_field_num_eval_modes[]     = {NUM_U_t_FIELD_EVAL_MODES_NeoHookean_Damage};
static const CeedEvalMode ut_field_eval_modes[]         = {CEED_EVAL_INTERP};
static const CeedInt      utt_field_num_eval_modes[]    = {1};
static const CeedEvalMode utt_field_eval_modes[]        = {CEED_EVAL_INTERP};

static const char *const projected_diagnostic_component_names[] = {"displacement_x",
                                                                   "displacement_y",
                                                                   "displacement_z",
                                                                   "Cauchy_stress_xx",
                                                                   "Cauchy_stress_yy",
                                                                   "Cauchy_stress_zz",
                                                                   "Cauchy_stress_xy",
                                                                   "Cauchy_stress_yz",
                                                                   "Cauchy_stress_xz",
                                                                   "pressure",
                                                                   "trace_e",
                                                                   "trace_e2",
                                                                   "J",
                                                                   "degraded_strain_energy_density",
                                                                   "von_Mises_stress",
                                                                   "mass_density",
                                                                   "damage",
                                                                   "undegraded_vol_strain_energy",
                                                                   "undegraded_dev_strain_energy"};

struct RatelModelData_private neo_hookean_initial_damage_data_private = {
    .name                                 = "Neo-Hookean hyperelasticity with damage phase-field model in initial configuration",
    .command_line_option                  = "elasticity-neo-hookean-damage-initial",
    .setup_q_data_volume                  = SetupVolumeGeometry,
    .setup_q_data_volume_loc              = SetupVolumeGeometry_loc,
    .setup_q_data_surface                 = SetupSurfaceGeometry,
    .setup_q_data_surface_loc             = SetupSurfaceGeometry_loc,
    .setup_q_data_surface_grad            = SetupSurfaceForceGeometry,
    .setup_q_data_surface_grad_loc        = SetupSurfaceForceGeometry_loc,
    .q_data_volume_size                   = Q_DATA_VOLUMETRIC_GEOMETRY_SIZE,
    .q_data_surface_size                  = Q_DATA_SURFACE_GEOMETRY_SIZE,
    .q_data_surface_grad_size             = Q_DATA_SURFACE_FORCE_GEOMETRY_SIZE,
    .num_forcing                          = 1,
    .num_active_fields                    = sizeof(active_field_sizes) / sizeof(*active_field_sizes),
    .active_field_sizes                   = active_field_sizes,
    .active_field_names                   = active_field_names,
    .active_component_names               = active_component_names,
    .active_field_num_eval_modes          = active_field_num_eval_modes,
    .active_field_eval_modes              = active_field_eval_modes,
    .ut_field_num_eval_modes              = ut_field_num_eval_modes,
    .ut_field_eval_modes                  = ut_field_eval_modes,
    .utt_field_num_eval_modes             = utt_field_num_eval_modes,
    .utt_field_eval_modes                 = utt_field_eval_modes,
    .num_comp_projected_diagnostic        = NUM_COMPONENTS_DIAGNOSTIC_NeoHookean_Damage,
    .projected_diagnostic_component_names = projected_diagnostic_component_names,
    .num_comp_dual_diagnostic             = NUM_COMPONENTS_DIAGNOSTIC_NeoHookean_Damage_Dual,
    .dual_diagnostic_component_names      = RatelElasticityDualDiagnosticComponentNames,
    .quadrature_mode                      = CEED_GAUSS,
    .residual_u                           = ElasticityDamageResidual_NeoHookeanInitial,
    .residual_u_loc                       = ElasticityDamageResidual_NeoHookeanInitial_loc,
    .residual_ut                          = ElasticityDamageResidual_ut_NeoHookeanInitial,
    .residual_ut_loc                      = ElasticityDamageResidual_ut_NeoHookeanInitial_loc,
    .residual_utt                         = ElasticityDamageResidual_utt,
    .residual_utt_loc                     = ElasticityDamageResidual_utt_loc,
    .has_ut_term                          = PETSC_TRUE,
    .num_comp_stored_u                    = NUM_COMPONENTS_STORED_NeoHookeanInitial_Damage,
    .jacobian                             = ElasticityDamageJacobian_NeoHookeanInitial,
    .jacobian_loc                         = ElasticityDamageJacobian_NeoHookeanInitial_loc,
    .num_comp_state                       = NUM_COMPONENTS_STATE_NeoHookean_Damage,
    .projected_diagnostic                 = ElasticityDamageDiagnostic_NeoHookean,
    .projected_diagnostic_loc             = ElasticityDamageDiagnostic_NeoHookean_loc,
    .dual_diagnostic                      = ElasticityDamageDualDiagnostic_NeoHookean,
    .dual_diagnostic_loc                  = ElasticityDamageDualDiagnostic_NeoHookean_loc,
    .strain_energy                        = ElasticityDamageStrainEnergy_NeoHookean,
    .strain_energy_loc                    = ElasticityDamageStrainEnergy_NeoHookean_loc,
    .platen_residual_u                    = PlatenBCsResidualElasticityDamage_NeoHookeanInitial,
    .platen_residual_u_loc                = PlatenBCsResidualElasticityDamage_NeoHookeanInitial_loc,
    .platen_jacobian                      = PlatenBCsJacobianElasticityDamage_NeoHookeanInitial,
    .platen_jacobian_loc                  = PlatenBCsJacobianElasticityDamage_NeoHookeanInitial_loc,
    .flops_qf_jacobian_u                  = FLOPS_JACOBIAN_NeoHookeanInitial_Damage,
    .flops_qf_jacobian_platen             = FLOPS_Platen_without_df1 + FLOPS_JACOBIAN_NeoHookeanInitial_Damage_Platen,
};
RatelModelData neo_hookean_initial_damage_data = &neo_hookean_initial_damage_data_private;

/// @addtogroup RatelMaterials
/// @{

/**
  @brief Create RatelMaterial for Damage Phase Field

  @param[in]   ratel     Ratel context
  @param[out]  material  RatelMaterial context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialCreate_ElasticityNeoHookeanDamageInitial(Ratel ratel, RatelMaterial material) {
  PetscFunctionBegin;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Create Linear Elasticity with Damage"));

  // Non SPD in general
  PetscCall(RatelSetNonSPD(ratel));

  // Model data
  PetscCall(RatelModelDataVerifyRelativePath(ratel, neo_hookean_initial_damage_data));
  material->model_data = neo_hookean_initial_damage_data;

  // QFunction contexts
  PetscCall(RatelMaterialParamsContextCreate_ElasticityDamage(material, &material->ctx_params));
  PetscCall(RatelMaterialParamsSmootherDataSetup_ElasticityDamage(material));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Create Linear Elasticity with Damage Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Register damage model

  @param[in]   ratel                      Ratel context
  @param[in]   cl_argument                Command line argument to use for model
  @param[out]  material_create_functions  PETSc function list for RatelMaterials

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelRegisterModel_ElasticityNeoHookeanDamageInitial(Ratel ratel, const char *cl_argument,
                                                                    PetscFunctionList *material_create_functions) {
  PetscFunctionBegin;
  RATEL_MODEL_REGISTER(material_create_functions, cl_argument, ElasticityNeoHookeanDamageInitial);
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}
