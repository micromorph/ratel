/// @file
/// Linear poroelasticity

#include <ceed.h>
#include <petscsys.h>
#include <ratel-impl.h>
#include <ratel-model.h>
#include <ratel.h>
#include <ratel/qfunctions/boundaries/poroelastic-linear-manufactured.h>
#include <ratel/qfunctions/error/poroelastic-linear-manufactured.h>
#include <ratel/qfunctions/forcing/poroelastic-linear-manufactured.h>
#include <ratel/qfunctions/geometry/surface-force.h>
#include <ratel/qfunctions/geometry/surface.h>
#include <ratel/qfunctions/geometry/volumetric.h>
#include <ratel/qfunctions/models/dual-diagnostic.h>
#include <ratel/qfunctions/models/poroelasticity-linear.h>

static const CeedInt      active_field_sizes[]          = {3, 1};
static const char *const  active_field_names[]          = {"displacement", "pressure"};
static const char *const  active_component_names[]      = {"u_x", "u_y", "u_z", "p"};
static const CeedInt      active_field_num_eval_modes[] = {NUM_ACTIVE_FIELD_EVAL_MODES_PoroElasticityLinear_u,
                                                           NUM_ACTIVE_FIELD_EVAL_MODES_PoroElasticityLinear_p};
static const CeedEvalMode active_field_eval_modes[]     = {CEED_EVAL_GRAD, CEED_EVAL_INTERP, CEED_EVAL_GRAD};
static const CeedInt ut_field_num_eval_modes[]  = {NUM_U_t_FIELD_EVAL_MODES_PoroElasticityLinear_u, NUM_U_t_FIELD_EVAL_MODES_PoroElasticityLinear_p};
static const CeedEvalMode ut_field_eval_modes[] = {CEED_EVAL_GRAD, CEED_EVAL_INTERP};

static const char *const projected_diagnostic_component_names[] = {"displacement_x",
                                                                   "displacement_y",
                                                                   "displacement_z",
                                                                   "Cauchy_stress_xx",
                                                                   "Cauchy_stress_xy",
                                                                   "Cauchy_stress_xz",
                                                                   "Cauchy_stress_yy",
                                                                   "Cauchy_stress_yz",
                                                                   "Cauchy_stress_zz",
                                                                   "pressure",
                                                                   "volumetric_strain",
                                                                   "trace_E2",
                                                                   "J",
                                                                   "strain_energy_density",
                                                                   "von_Mises_stress",
                                                                   "mass_density",
                                                                   "porosity"};
static const char *const dual_diagnostic_component_names[]      = {"nodal_volume"};

struct RatelModelData_private poroelasticity_linear_data_private = {
    .name                                 = "linear poroelasticity at small strain",
    .command_line_option                  = "poroelasticity-linear",
    .setup_q_data_volume                  = SetupVolumeGeometry,
    .setup_q_data_volume_loc              = SetupVolumeGeometry_loc,
    .setup_q_data_surface                 = SetupSurfaceGeometry,
    .setup_q_data_surface_loc             = SetupSurfaceGeometry_loc,
    .setup_q_data_surface_grad            = SetupSurfaceForceGeometry,
    .setup_q_data_surface_grad_loc        = SetupSurfaceForceGeometry_loc,
    .q_data_volume_size                   = Q_DATA_VOLUMETRIC_GEOMETRY_SIZE,
    .q_data_surface_size                  = Q_DATA_SURFACE_GEOMETRY_SIZE,
    .q_data_surface_grad_size             = Q_DATA_SURFACE_FORCE_GEOMETRY_SIZE,
    .num_forcing                          = 2,
    .num_active_fields                    = sizeof(active_field_sizes) / sizeof(*active_field_sizes),
    .active_field_sizes                   = active_field_sizes,
    .active_field_names                   = active_field_names,
    .active_component_names               = active_component_names,
    .active_field_num_eval_modes          = active_field_num_eval_modes,
    .active_field_eval_modes              = active_field_eval_modes,
    .num_comp_projected_diagnostic        = NUM_COMPONENTS_DIAGNOSTIC_PoroElasticityLinear,
    .projected_diagnostic_component_names = projected_diagnostic_component_names,
    .num_comp_dual_diagnostic             = NUM_COMPONENTS_DIAGNOSTIC_Dual,
    .dual_diagnostic_component_names      = dual_diagnostic_component_names,
    .quadrature_mode                      = CEED_GAUSS,
    .residual_u                           = PoroElasticityResidual_Linear,
    .residual_u_loc                       = PoroElasticityResidual_Linear_loc,
    .residual_ut                          = PoroElasticityResidual_Linear_ut,
    .residual_ut_loc                      = PoroElasticityResidual_Linear_ut_loc,
    .has_ut_term                          = PETSC_TRUE,
    .ut_field_num_eval_modes              = ut_field_num_eval_modes,
    .ut_field_eval_modes                  = ut_field_eval_modes,
    .jacobian                             = PoroElasticityJacobian_Linear,
    .jacobian_loc                         = PoroElasticityJacobian_Linear_loc,
    .jacobian_block                       = {PoroElasticityPC_uu_Linear,                   PoroElasticityPC_pp_Linear                  },
    .jacobian_block_loc                   = {PoroElasticityPC_uu_Linear_loc,               PoroElasticityPC_pp_Linear_loc              },
    .platen_residual_u                    = PlatenBCsResidual_PoroElasticityLinear,
    .platen_residual_u_loc                = PlatenBCsResidual_PoroElasticityLinear_loc,
    .platen_jacobian                      = PlatenBCsJacobian_PoroElasticityLinear,
    .platen_jacobian_loc                  = PlatenBCsJacobian_PoroElasticityLinear_loc,
    .strain_energy                        = StrainEnergy_PoroElasticityLinear,
    .strain_energy_loc                    = StrainEnergy_PoroElasticityLinear_loc,
    .projected_diagnostic                 = Diagnostic_PoroElasticityLinear,
    .projected_diagnostic_loc             = Diagnostic_PoroElasticityLinear_loc,
    .dual_diagnostic                      = DualDiagnostic,
    .dual_diagnostic_loc                  = DualDiagnostic_loc,
    .mms_boundary                         = {MMSBCs_PoroElasticityLinear_u,                MMSBCs_PoroElasticityLinear_p               },
    .mms_boundary_loc                     = {MMSBCs_PoroElasticityLinear_u_loc,            MMSBCs_PoroElasticityLinear_p_loc           },
    .mms_error                            = MMSError_PoroElasticityLinear,
    .mms_error_loc                        = MMSError_PoroElasticityLinear_loc,
    .mms_forcing                          = MMSForce_PoroElasticityLinear,
    .mms_forcing_loc                      = MMSForce_PoroElasticityLinear_loc,
    .flops_qf_jacobian_u                  = FLOPS_JACOBIAN_PoroElasticityLinear,
    .flops_qf_jacobian_utt                = FLOPS_ScaledMass,
    .flops_qf_jacobian_block              = {FLOPS_JACOBIAN_Block_uu_PoroElasticityLinear, FLOPS_JACOBIAN_Block_pp_PoroElasticityLinear},
};
RatelModelData poroelasticity_linear_data = &poroelasticity_linear_data_private;

/// @addtogroup RatelMaterials
/// @{

/**
  @brief Create `RatelMaterial` model data for linear elasticity.

  Not collective across MPI processes.

  @param[in]   ratel     `Ratel` context
  @param[out]  material  `RatelMaterial` context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialCreate_PoroElasticityLinear(Ratel ratel, RatelMaterial material) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Create Poroelasticity Linear"));

  // Model data
  PetscCall(RatelModelDataVerifyRelativePath(ratel, poroelasticity_linear_data));
  material->model_data = poroelasticity_linear_data;

  // QFunction contexts
  PetscCall(RatelMaterialParamsContextCreate_PoroElasticityLinear(material, &material->ctx_params));
  PetscCall(RatelMaterialParamsSmootherDataSetup_PoroElasticityLinear(material));
  PetscCall(RatelMMSParamsContextCreate_PoroElasticityLinear(material, &material->ctx_mms_params));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Create Mixed Elasticity Linear Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Register mixed linear elastic model.

  Not collective across MPI processes.

  @param[in]   ratel                      `Ratel` context
  @param[in]   cl_argument                Command line argument to use for model
  @param[out]  material_create_functions  PETSc function list for `RatelMaterial`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelRegisterModel_PoroElasticityLinear(Ratel ratel, const char *cl_argument, PetscFunctionList *material_create_functions) {
  PetscFunctionBeginUser;
  RATEL_MODEL_REGISTER(material_create_functions, cl_argument, PoroElasticityLinear);
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}
