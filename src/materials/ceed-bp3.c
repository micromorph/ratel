/// @file
/// CEED BP 3 - scalar Poisson

#include <ceed.h>
#include <petscsys.h>
#include <ratel-impl.h>
#include <ratel-model.h>
#include <ratel.h>
#include <ratel/qfunctions/boundaries/ceed-scalar-bps-manufactured.h>
#include <ratel/qfunctions/error/ceed-scalar-bps-manufactured.h>
#include <ratel/qfunctions/forcing/ceed-bp3-manufactured.h>
#include <ratel/qfunctions/geometry/volumetric-symmetric.h>
#include <ratel/qfunctions/models/ceed-bp3.h>
#include <ratel/qfunctions/models/dual-diagnostic.h>

static const CeedInt      active_field_sizes[]          = {1};
static const char *const  active_field_names[]          = {"solution"};
static const char *const  active_component_names[]      = {"1"};
static const CeedInt      active_field_num_eval_modes[] = {1};
static const CeedEvalMode active_field_eval_modes[]     = {CEED_EVAL_GRAD};

static const char *const dual_diagnostic_component_names[] = {"nodal_volume"};

struct RatelModelData_private ceed_bp3_data_private = {
    .name                                 = "CEED BP3",
    .command_line_option                  = "ceed-bp3",
    .setup_q_data_volume                  = SetupVolumeGeometrySymmetric,
    .setup_q_data_volume_loc              = SetupVolumeGeometrySymmetric_loc,
    .q_data_volume_size                   = Q_DATA_VOLUMETRIC_SYMMETRIC_GEOMETRY_SIZE,
    .num_forcing                          = 1,
    .num_active_fields                    = sizeof(active_field_sizes) / sizeof(*active_field_sizes),
    .active_field_sizes                   = active_field_sizes,
    .active_field_names                   = active_field_names,
    .active_component_names               = active_component_names,
    .active_field_num_eval_modes          = active_field_num_eval_modes,
    .active_field_eval_modes              = active_field_eval_modes,
    .num_comp_projected_diagnostic        = 1,
    .projected_diagnostic_component_names = active_component_names,
    .num_comp_dual_diagnostic             = NUM_COMPONENTS_DIAGNOSTIC_Dual,
    .dual_diagnostic_component_names      = dual_diagnostic_component_names,
    .quadrature_mode                      = CEED_GAUSS,
    .residual_u                           = Residual_CEED_BP3,
    .residual_u_loc                       = Residual_CEED_BP3_loc,
    .jacobian                             = Jacobian_CEED_BP3,
    .jacobian_loc                         = Jacobian_CEED_BP3_loc,
    .projected_diagnostic                 = Diagnostic_CEED_BP3,
    .projected_diagnostic_loc             = Diagnostic_CEED_BP3_loc,
    .dual_diagnostic                      = DualDiagnostic,
    .dual_diagnostic_loc                  = DualDiagnostic_loc,
    .mms_boundary                         = {MMSBCs_CEED_ScalarBPs},
    .mms_boundary_loc                     = {MMSBCs_CEED_ScalarBPs_loc},
    .mms_error                            = MMSError_CEED_ScalarBPs,
    .mms_error_loc                        = MMSError_CEED_ScalarBPs_loc,
    .mms_forcing                          = MMSForce_CEED_BP3,
    .mms_forcing_loc                      = MMSForce_CEED_BP3_loc,
    .flops_qf_jacobian_u                  = 15,
};
RatelModelData ceed_bp3_data = &ceed_bp3_data_private;

/// @addtogroup RatelMaterials
/// @{

/**
  @brief Create `RatelMaterial` model data for CEED BP3.

  Not collective across MPI processes.

  @param[in]   ratel     `Ratel` context
  @param[out]  material  `RatelMaterial` context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialCreate_CEED_BP3(Ratel ratel, RatelMaterial material) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Create CEED BP3"));

  // Model data
  PetscCall(RatelModelDataVerifyRelativePath(ratel, ceed_bp3_data));
  material->model_data = ceed_bp3_data;

  // QFunction contexts
  PetscCall(RatelMMSParamsContextCreate_CEED_BPs(material, &material->ctx_mms_params));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Create CEED BP3 Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Register CEED BP3 model.

  Not collective across MPI processes.

  @param[in]   ratel                      `Ratel` context
  @param[in]   cl_argument                Command line argument to use for model
  @param[out]  material_create_functions  PETSc function list for `RatelMaterial`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelRegisterModel_CEED_BP3(Ratel ratel, const char *cl_argument, PetscFunctionList *material_create_functions) {
  PetscFunctionBeginUser;
  RATEL_MODEL_REGISTER(material_create_functions, cl_argument, CEED_BP3);
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}
