/// @file
/// Setup Mooney-Rivlin model parameters context objects

#include <ceed.h>
#include <math.h>
#include <petscsys.h>
#include <ratel-impl.h>
#include <ratel-material.h>
#include <ratel-model.h>
#include <ratel.h>
#include <ratel/models/mooney-rivlin.h>
#include <stddef.h>
#include <stdio.h>

/// @addtogroup RatelMaterials
/// @{

/// @brief Mooney-Rivlin model parameters for `CeedQFunctionContext` and viewing
struct RatelModelParameterData_private mooney_rivlin_param_data_private = {
    .parameters     = {{
                           .name        = "shift v",
                           .description = "shift for U_t",
                           .is_hidden   = PETSC_TRUE,
                           .offset      = offsetof(RatelMooneyRivlinElasticityParams, common_parameters[RATEL_COMMON_PARAMETER_SHIFT_V]),
                           .type        = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name        = "shift a",
                           .description = "shift for U_tt",
                           .is_hidden   = PETSC_TRUE,
                           .offset      = offsetof(RatelMooneyRivlinElasticityParams, common_parameters[RATEL_COMMON_PARAMETER_SHIFT_A]),
                           .type        = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name            = "rho",
                           .description     = "Density",
                           .units           = "kg/m^3",
                           .restrictions    = "rho >= 0",
                           .default_value.s = 1,
                           .is_required     = PETSC_FALSE,
                           .offset          = offsetof(RatelMooneyRivlinElasticityParams, common_parameters[RATEL_COMMON_PARAMETER_RHO]),
                           .type            = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "nu",
                           .description  = "Poisson's ratio",
                           .restrictions = "0 <= nu < 0.5",
                           .is_required  = PETSC_TRUE,
                           .offset       = offsetof(RatelMooneyRivlinElasticityParams, nu),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "mu_1",
                           .description  = "First shear modulus",
                           .units        = "Pa",
                           .restrictions = "mu_1 >= 0",
                           .is_required  = PETSC_TRUE,
                           .offset       = offsetof(RatelMooneyRivlinElasticityParams, mu_1),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "mu_2",
                           .description  = "Second shear modulus",
                           .units        = "Pa",
                           .restrictions = "mu_2 >= 0",
                           .is_required  = PETSC_TRUE,
                           .offset       = offsetof(RatelMooneyRivlinElasticityParams, mu_2),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "mu",
                           .description  = "Shear Modulus: mu_1 + mu_2",
                           .units        = "Pa",
                           .restrictions = "mu >= 0",
                           .is_required  = PETSC_FALSE,
                           .offset       = offsetof(RatelMooneyRivlinElasticityParams, mu),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "lambda",
                           .description  = "First Lame parameter: 2 mu nu / (1 - 2 nu)",
                           .units        = "Pa",
                           .restrictions = "lambda >= 0",
                           .is_required  = PETSC_FALSE,
                           .offset       = offsetof(RatelMooneyRivlinElasticityParams, lambda),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "bulk",
                           .description  = "Bulk Modulus: 2 mu (1 + nu) / (3 (1 - 2 nu))",
                           .units        = "Pa",
                           .restrictions = "bulk >= 0",
                           .is_required  = PETSC_FALSE,
                           .offset       = offsetof(RatelMooneyRivlinElasticityParams, bulk),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "two_mu_2",
                           .description  = "Second shear modulus multiplied by 2",
                           .units        = "Pa",
                           .restrictions = "mu >= 0",
                           .is_required  = PETSC_FALSE,
                           .offset       = offsetof(RatelMooneyRivlinElasticityParams, two_mu_2),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }},
    .num_parameters = 10,
    .reference      = "ISBN: 0-471-82319-8"
};
RatelModelParameterData mooney_rivlin_param_data = &mooney_rivlin_param_data_private;

/**
  @brief Set `CeedQFunctionContext` data and register fields for Mooney-Rivlin parameters.

  Not collective across MPI processes.

  @param[in]   material  `RatelMaterial` to setup model parameters context
  @param[in]   nu        Poisson's ratio
  @param[in]   mu_1      Material Property mu_1
  @param[in]   mu_2      Material Property mu_2
  @param[in]   rho       Density for scaled mass matrix
  @param[out]  ctx       `CeedQFunctionContext` for Mooney-Rivlin parameters

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelCeedParamsContextCreate_MooneyRivlin(RatelMaterial material, CeedScalar nu, CeedScalar mu_1, CeedScalar mu_2,
                                                                CeedScalar rho, CeedQFunctionContext *ctx) {
  Ratel                              ratel      = material->ratel;
  RatelModelParameterData            param_data = material->model_data->param_data;
  RatelMooneyRivlinElasticityParams *params;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Create Parameters Context Mooney-Rivlin"));

  // Create context data with correct scaling
  PetscCall(PetscNew(&params));
  params->nu                                            = nu;
  params->mu_1                                          = mu_1 * ratel->material_units->Pascal;
  params->mu_2                                          = mu_2 * ratel->material_units->Pascal;
  params->common_parameters[RATEL_COMMON_PARAMETER_RHO] = rho * (ratel->material_units->kilogram) / pow(ratel->material_units->meter, 3);
  params->mu                                            = params->mu_1 + params->mu_2;
  params->two_mu_2                                      = 2 * params->mu_2;
  params->lambda                                        = 2 * params->mu * params->nu / (1 - 2 * params->nu);
  params->bulk                                          = 2 * params->mu * (1 + params->nu) / (3. * (1 - 2 * params->nu));

  // Create context object
  RatelCallCeed(ratel, CeedQFunctionContextCreate(ratel->ceed, ctx));
  RatelCallCeed(ratel, CeedQFunctionContextSetData(*ctx, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(*params), params));
  PetscCall(PetscFree(params));

  // Register parameters
  PetscCall(RatelModelParameterDataRegisterContextFields(ratel, param_data, *ctx));

  // Debugging output
  if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedQFunctionContextView(*ctx, stdout));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Create Parameters Context Mooney-Rivlin Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Build `CeedQFunctionContext` for Mooney-Rivlin parameters.

  Collective across MPI processes.

  @param[in]   material  `RatelMaterial` to setup model parameters context
  @param[out]  ctx       `CeedQFunctionContext` for Mooney-Rivlin parameters

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialParamsContextCreate_MooneyRivlin(RatelMaterial material, CeedQFunctionContext *ctx) {
  Ratel      ratel    = material->ratel;
  PetscBool  set_nu   = PETSC_FALSE;
  PetscBool  set_mu_1 = PETSC_FALSE;
  PetscBool  set_mu_2 = PETSC_FALSE;
  CeedScalar nu = -1.0, mu_1 = -1.0, mu_2 = -1.0, rho;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup Model Parameters Context Mooney-Rivlin"));

  // Set model parameter data
  material->model_data->param_data = mooney_rivlin_param_data;

  // Get default values;
  PetscCall(RatelModelParameterDataGetDefaultValue_Scalar(ratel, mooney_rivlin_param_data, "rho", &rho));

  // Get parameter values
  {
    char *cl_prefix;

    PetscCall(RatelMaterialGetCLPrefix(material, &cl_prefix));
    PetscOptionsBegin(ratel->comm, cl_prefix, "Mooney-Rivlin model parameters", NULL);

    PetscCall(PetscOptionsScalar("-rho", "Density for scaled mass matrix", NULL, rho, &rho, NULL));
    PetscCheck(rho >= 0.0, ratel->comm, PETSC_ERR_SUP, "Density must be a positive value");
    PetscCall(RatelDebug(ratel, "---- rho: %f", rho));

    PetscCall(PetscOptionsScalar("-nu", "Poisson ratio", NULL, nu, &nu, &set_nu));
    PetscCheck(nu >= 0 && nu < 0.5, ratel->comm, PETSC_ERR_SUP, "Mooney-Rivlin model requires Poisson ratio -nu option in [0, 0.5)");
    PetscCall(RatelDebug(ratel, "---- nu: %f", nu));

    PetscCall(PetscOptionsScalar("-mu_1", "Material Property mu_1", NULL, mu_1, &mu_1, &set_mu_1));
    PetscCheck(mu_1 >= 0, ratel->comm, PETSC_ERR_SUP, "Mooney-Rivlin model requires non-negative -mu_1 option (Pa)");
    PetscCall(RatelDebug(ratel, "---- mu_1: %f", mu_1));

    PetscCall(PetscOptionsScalar("-mu_2", "Material Property mu_2", NULL, mu_2, &mu_2, &set_mu_2));
    PetscCheck(mu_2 >= 0, ratel->comm, PETSC_ERR_SUP, "Mooney-Rivlin model requires non-negative -mu_2 option (Pa)");
    PetscCall(RatelDebug(ratel, "---- mu_2: %f", mu_2));

    PetscOptionsEnd();  // End of setting model parameters
    PetscCall(PetscFree(cl_prefix));
  }

  // Check for all required options to be set
  PetscCheck(set_nu, ratel->comm, PETSC_ERR_SUP, "-nu option needed");
  PetscCheck(set_mu_1, ratel->comm, PETSC_ERR_SUP, "-mu_1 option needed");
  PetscCheck(set_mu_2, ratel->comm, PETSC_ERR_SUP, "-mu_2 option needed");

  // Create context
  PetscCall(RatelCeedParamsContextCreate_MooneyRivlin(material, nu, mu_1, mu_2, rho, ctx));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup Model Parameters Context Mooney-Rivlin Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup data for `CeedQFunctionContext` for smoother with Mooney-Rivlin parameters.

  Collective across MPI processes.

  @param[in, out]  material  `RatelMaterial` to setup model parameters context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialParamsSmootherDataSetup_MooneyRivlin(RatelMaterial material) {
  Ratel      ratel           = material->ratel;
  PetscBool  set_nu_smoother = PETSC_FALSE;
  CeedScalar nu = -1.0, nu_smoother = -1.0;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup Model Parameters Smoother Context Mooney-Rivlin"));

  // Get parameter values
  if (material->num_params_smoother_values == 0) {
    char *cl_prefix;

    PetscCall(RatelMaterialGetCLPrefix(material, &cl_prefix));
    PetscOptionsBegin(ratel->comm, cl_prefix, "Mooney-Rivlin model parameters", NULL);

    PetscCall(PetscOptionsScalar("-nu", "Poisson ratio", NULL, nu, &nu, NULL));
    PetscCheck(nu >= 0 && nu < 0.5, ratel->comm, PETSC_ERR_SUP, "Mooney-Rivlin model requires Poisson ratio -nu option in [0, 0.5)");
    PetscCall(RatelDebug(ratel, "---- nu: %f", nu));

    PetscCall(PetscOptionsScalar("-nu_smoother", "Poisson's ratio for smoother", NULL, nu_smoother, &nu_smoother, &set_nu_smoother));
    if (set_nu_smoother) PetscCall(RatelDebug(ratel, "---- nu_smoother: %f", nu_smoother));
    else PetscCall(RatelDebug(ratel, "---- No Nu for smoother set"));

    PetscOptionsEnd();  // End of setting model parameters
    PetscCall(PetscFree(cl_prefix));

    // Store smoother values, if needed
    if (set_nu_smoother) {
      const char *label_name     = "nu";
      PetscSizeT  label_name_len = 0;

      material->num_params_smoother_values = 1;
      PetscCall(PetscNew(&material->ctx_params_values));
      PetscCall(PetscNew(&material->ctx_params_smoother_values));
      material->ctx_params_values[0]          = nu;
      material->ctx_params_smoother_values[0] = nu_smoother;
      PetscCall(PetscNew(&material->ctx_params_label_names));
      PetscCall(PetscStrlen(label_name, &label_name_len));
      PetscCall(PetscCalloc1(label_name_len + 1, &material->ctx_params_label_names[0]));
      PetscCall(PetscStrncpy(material->ctx_params_label_names[0], label_name, label_name_len + 1));
    }
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup Model Parameters Smoother Context Mooney-Rivlin Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}
