/// @file
/// Linear elastic material using Neo-Hookean model

#include <ceed.h>
#include <petscsys.h>
#include <ratel-impl.h>
#include <ratel-model.h>
#include <ratel.h>
#include <ratel/qfunctions/boundaries/linear-manufactured.h>
#include <ratel/qfunctions/error/linear-manufactured.h>
#include <ratel/qfunctions/forcing/linear-manufactured.h>
#include <ratel/qfunctions/geometry/surface-force.h>
#include <ratel/qfunctions/geometry/surface.h>
#include <ratel/qfunctions/geometry/volumetric.h>
#include <ratel/qfunctions/models/elasticity-common-utt.h>
#include <ratel/qfunctions/models/elasticity-linear-dual-diagnostic.h>
#include <ratel/qfunctions/models/elasticity-linear.h>

static const CeedInt      active_field_sizes[]          = {3};
static const char *const  active_field_names[]          = {"displacement"};
static const char *const  active_component_names[]      = {"u_x", "u_y", "u_z"};
static const CeedInt      active_field_num_eval_modes[] = {NUM_ACTIVE_FIELD_EVAL_MODES_Linear};
static const CeedEvalMode active_field_eval_modes[]     = {CEED_EVAL_GRAD};
static const CeedInt      utt_field_num_eval_modes[]    = {1};
static const CeedEvalMode utt_field_eval_modes[]        = {CEED_EVAL_INTERP};

static const char *const projected_diagnostic_component_names[] = {"displacement_x",
                                                                   "displacement_y",
                                                                   "displacement_z",
                                                                   "Cauchy_stress_xx",
                                                                   "Cauchy_stress_xy",
                                                                   "Cauchy_stress_xz",
                                                                   "Cauchy_stress_yy",
                                                                   "Cauchy_stress_yz",
                                                                   "Cauchy_stress_zz",
                                                                   "pressure",
                                                                   "volumetric_strain",
                                                                   "trace_E2",
                                                                   "J",
                                                                   "strain_energy_density",
                                                                   "von_Mises_stress",
                                                                   "mass_density"};

struct RatelModelData_private elasticity_linear_data_private = {
    .name                                 = "Linear elasticity",
    .command_line_option                  = "elasticity-linear",
    .setup_q_data_volume                  = SetupVolumeGeometry,
    .setup_q_data_volume_loc              = SetupVolumeGeometry_loc,
    .setup_q_data_surface                 = SetupSurfaceGeometry,
    .setup_q_data_surface_loc             = SetupSurfaceGeometry_loc,
    .setup_q_data_surface_grad            = SetupSurfaceForceGeometry,
    .setup_q_data_surface_grad_loc        = SetupSurfaceForceGeometry_loc,
    .q_data_volume_size                   = Q_DATA_VOLUMETRIC_GEOMETRY_SIZE,
    .q_data_surface_size                  = Q_DATA_SURFACE_GEOMETRY_SIZE,
    .q_data_surface_grad_size             = Q_DATA_SURFACE_FORCE_GEOMETRY_SIZE,
    .num_forcing                          = 1,
    .num_active_fields                    = sizeof(active_field_sizes) / sizeof(*active_field_sizes),
    .active_field_sizes                   = active_field_sizes,
    .active_field_names                   = active_field_names,
    .active_component_names               = active_component_names,
    .active_field_num_eval_modes          = active_field_num_eval_modes,
    .active_field_eval_modes              = active_field_eval_modes,
    .utt_field_num_eval_modes             = utt_field_num_eval_modes,
    .utt_field_eval_modes                 = utt_field_eval_modes,
    .num_comp_projected_diagnostic        = NUM_COMPONENTS_DIAGNOSTIC_Linear,
    .projected_diagnostic_component_names = projected_diagnostic_component_names,
    .num_comp_dual_diagnostic             = NUM_COMPONENTS_ELASTICITY_LINEAR_DIAGNOSTIC_Dual,
    .dual_diagnostic_component_names      = RatelElasticityDualDiagnosticComponentNames,
    .quadrature_mode                      = CEED_GAUSS,
    .residual_u                           = ElasticityResidual_Linear,
    .residual_u_loc                       = ElasticityResidual_Linear_loc,
    .residual_utt                         = ElasticityResidual_utt,
    .residual_utt_loc                     = ElasticityResidual_utt_loc,
    .jacobian                             = ElasticityJacobian_Linear,
    .jacobian_loc                         = ElasticityJacobian_Linear_loc,
    .strain_energy                        = StrainEnergy_Linear,
    .strain_energy_loc                    = StrainEnergy_Linear_loc,
    .projected_diagnostic                 = Diagnostic_Linear,
    .projected_diagnostic_loc             = Diagnostic_Linear_loc,
    .dual_diagnostic                      = ElasticityDualDiagnostic_Linear,
    .dual_diagnostic_loc                  = ElasticityDualDiagnostic_Linear_loc,
    .surface_force_cell_to_face           = SurfaceForceCellToFace_Linear,
    .surface_force_cell_to_face_loc       = SurfaceForceCellToFace_Linear_loc,
    .platen_residual_u                    = PlatenBCsResidual_Linear,
    .platen_residual_u_loc                = PlatenBCsResidual_Linear_loc,
    .platen_jacobian                      = PlatenBCsJacobian_Linear,
    .platen_jacobian_loc                  = PlatenBCsJacobian_Linear_loc,
    .mms_boundary                         = {MMSBCs_Linear},
    .mms_boundary_loc                     = {MMSBCs_Linear_loc},
    .mms_error                            = MMSError_Linear,
    .mms_error_loc                        = MMSError_Linear_loc,
    .mms_forcing                          = MMSForce_Linear,
    .mms_forcing_loc                      = MMSForce_Linear_loc,
    .mms_forcing_energy                   = MMSForceEnergy_Linear,
    .mms_forcing_energy_loc               = MMSForceEnergy_Linear_loc,
    .flops_qf_jacobian_u                  = FLOPS_JACOBIAN_Linear,
    .flops_qf_jacobian_utt                = FLOPS_ScaledMass,
    .flops_qf_jacobian_platen             = FLOPS_Platen_without_df1 + FLOPS_df1_Linear,
};
RatelModelData elasticity_linear_data = &elasticity_linear_data_private;

/// @addtogroup RatelMaterials
/// @{

/**
  @brief Create `RatelMaterial` model data for linear elasticity.

  Not collective across MPI processes.

  @param[in]   ratel     `Ratel` context
  @param[out]  material  `RatelMaterial` context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialCreate_ElasticityLinear(Ratel ratel, RatelMaterial material) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Create Elasticity Linear"));

  // Model data
  PetscCall(RatelModelDataVerifyRelativePath(ratel, elasticity_linear_data));
  material->model_data = elasticity_linear_data;

  // QFunction contexts
  PetscCall(RatelMaterialParamsContextCreate_NeoHookean(material, &material->ctx_params));
  PetscCall(RatelMaterialParamsSmootherDataSetup_NeoHookean(material));
  PetscCall(RatelMMSParamsContextCreate_Elasticity_Linear(material, &material->ctx_mms_params));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Create Elasticity Linear Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Register linear elastic model.

  Not collective across MPI processes.

  @param[in]   ratel                      `Ratel` context
  @param[in]   cl_argument                Command line argument to use for model
  @param[out]  material_create_functions  PETSc function list for `RatelMaterial`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelRegisterModel_ElasticityLinear(Ratel ratel, const char *cl_argument, PetscFunctionList *material_create_functions) {
  PetscFunctionBeginUser;
  RATEL_MODEL_REGISTER(material_create_functions, cl_argument, ElasticityLinear);
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}
