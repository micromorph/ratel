/// @file
/// Hyperelastic material at finite strain using Mixed Neo-Hookean model in current configuration

#include <ceed.h>
#include <petscsys.h>
#include <ratel-impl.h>
#include <ratel-model.h>
#include <ratel.h>
#include <ratel/qfunctions/geometry/surface-force.h>
#include <ratel/qfunctions/geometry/surface.h>
#include <ratel/qfunctions/geometry/volumetric.h>
#include <ratel/qfunctions/models/elasticity-common-utt.h>
#include <ratel/qfunctions/models/elasticity-dual-diagnostic.h>
#include <ratel/qfunctions/models/elasticity-mixed-neo-hookean-current.h>

static const CeedInt      active_field_sizes[]          = {3, 1};
static const char *const  active_field_names[]          = {"displacement", "pressure"};
static const char *const  active_component_names[]      = {"u_x", "u_y", "u_z", "p"};
static const CeedInt      active_field_num_eval_modes[] = {NUM_ACTIVE_FIELD_EVAL_MODES_MixedNeoHookeanCurrent_u,
                                                           NUM_ACTIVE_FIELD_EVAL_MODES_MixedNeoHookeanCurrent_p};
static const CeedEvalMode active_field_eval_modes[]     = {CEED_EVAL_GRAD, CEED_EVAL_INTERP};
static const CeedInt      utt_field_num_eval_modes[]    = {1, 0};
static const CeedEvalMode utt_field_eval_modes[]        = {CEED_EVAL_INTERP};

static const char *const projected_diagnostic_component_names[] = {"displacement_x",
                                                                   "displacement_y",
                                                                   "displacement_z",
                                                                   "Cauchy_stress_xx",
                                                                   "Cauchy_stress_xy",
                                                                   "Cauchy_stress_xz",
                                                                   "Cauchy_stress_yy",
                                                                   "Cauchy_stress_yz",
                                                                   "Cauchy_stress_zz",
                                                                   "pressure",
                                                                   "volumetric_strain",
                                                                   "trace_E2",
                                                                   "J",
                                                                   "strain_energy_density",
                                                                   "von_Mises_stress",
                                                                   "mass_density"};

struct RatelModelData_private elasticity_mixed_Neo_Hookean_current_data_private = {
    .name                                 = "Mixed Neo-Hookean hyperelasticity at finite strain, in current configuration",
    .command_line_option                  = "elasticity-mixed-neo-hookean-current",
    .setup_q_data_volume                  = SetupVolumeGeometry,
    .setup_q_data_volume_loc              = SetupVolumeGeometry_loc,
    .setup_q_data_surface                 = SetupSurfaceGeometry,
    .setup_q_data_surface_loc             = SetupSurfaceGeometry_loc,
    .setup_q_data_surface_grad            = SetupSurfaceForceGeometry,
    .setup_q_data_surface_grad_loc        = SetupSurfaceForceGeometry_loc,
    .q_data_volume_size                   = Q_DATA_VOLUMETRIC_GEOMETRY_SIZE,
    .q_data_surface_size                  = Q_DATA_SURFACE_GEOMETRY_SIZE,
    .q_data_surface_grad_size             = Q_DATA_SURFACE_FORCE_GEOMETRY_SIZE,
    .num_forcing                          = 1,
    .num_active_fields                    = sizeof(active_field_sizes) / sizeof(*active_field_sizes),
    .active_field_sizes                   = active_field_sizes,
    .active_field_names                   = active_field_names,
    .active_component_names               = active_component_names,
    .active_field_num_eval_modes          = active_field_num_eval_modes,
    .active_field_eval_modes              = active_field_eval_modes,
    .utt_field_num_eval_modes             = utt_field_num_eval_modes,
    .utt_field_eval_modes                 = utt_field_eval_modes,
    .num_comp_stored_u                    = NUM_COMPONENTS_STORED_MixedNeoHookeanCurrent,
    .num_comp_stored_platen               = NUM_COMPONENTS_STORED_MixedNeoHookeanInitial,
    .num_comp_projected_diagnostic        = NUM_COMPONENTS_DIAGNOSTIC_MixedElasticity,
    .projected_diagnostic_component_names = projected_diagnostic_component_names,
    .num_comp_dual_diagnostic             = NUM_COMPONENTS_ELASTICITY_DIAGNOSTIC_Dual,
    .dual_diagnostic_component_names      = RatelElasticityDualDiagnosticComponentNames,
    .quadrature_mode                      = CEED_GAUSS,
    .residual_u                           = ElasticityResidual_MixedNeoHookeanCurrent,
    .residual_u_loc                       = ElasticityResidual_MixedNeoHookeanCurrent_loc,
    .residual_utt                         = ElasticityResidual_utt,
    .residual_utt_loc                     = ElasticityResidual_utt_loc,
    .jacobian                             = ElasticityJacobian_MixedNeoHookeanCurrent,
    .jacobian_loc                         = ElasticityJacobian_MixedNeoHookeanCurrent_loc,
    .jacobian_block                       = {ElasticityPC_uu_MixedNeoHookeanCurrent,         ElasticityPC_pp_MixedNeoHookeanCurrent        },
    .jacobian_block_loc                   = {ElasticityPC_uu_MixedNeoHookeanCurrent_loc,     ElasticityPC_pp_MixedNeoHookeanCurrent_loc    },
    .platen_residual_u                    = PlatenBCsResidual_MixedNeoHookeanInitial,
    .platen_residual_u_loc                = PlatenBCsResidual_MixedNeoHookeanInitial_loc,
    .platen_jacobian                      = PlatenBCsJacobian_MixedNeoHookeanInitial,
    .platen_jacobian_loc                  = PlatenBCsJacobian_MixedNeoHookeanInitial_loc,
    .strain_energy                        = StrainEnergy_MixedNeoHookean,
    .strain_energy_loc                    = StrainEnergy_MixedNeoHookean_loc,
    .projected_diagnostic                 = Diagnostic_MixedNeoHookean,
    .projected_diagnostic_loc             = Diagnostic_MixedNeoHookean_loc,
    .dual_diagnostic                      = ElasticityDualDiagnostic,
    .dual_diagnostic_loc                  = ElasticityDualDiagnostic_loc,
    .flops_qf_jacobian_u                  = FLOPS_JACOBIAN_MixedNeoHookeanCurrent,
    .flops_qf_jacobian_utt                = FLOPS_ScaledMass,
    .flops_qf_jacobian_platen             = FLOPS_Platen_without_df1 + FLOPS_JACOBIAN_MixedNeoHookeanCurrent,
    .flops_qf_jacobian_block              = {FLOPS_JACOBIAN_Block_uu_MixedNeoHookeanCurrent, FLOPS_JACOBIAN_Block_pp_MixedNeoHookeanCurrent},
};
RatelModelData elasticity_mixed_Neo_Hookean_current_data = &elasticity_mixed_Neo_Hookean_current_data_private;

/// @addtogroup RatelMaterials
/// @{

/**
  @brief Create `RatelMaterial` model data for mixed Neo-Hookean hyperelasticity at finite strain in current configuration.

  Not collective across MPI processes.

  @param[in]   ratel     `Ratel` context
  @param[out]  material  `RatelMaterial` context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialCreate_ElasticityMixedNeoHookeanCurrent(Ratel ratel, RatelMaterial material) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Create Elasticity Mixed Neo-Hookean Current"));

  // Model data
  PetscCall(RatelModelDataVerifyRelativePath(ratel, elasticity_mixed_Neo_Hookean_current_data));
  material->model_data = elasticity_mixed_Neo_Hookean_current_data;

  // QFunction contexts
  PetscCall(RatelMaterialParamsContextCreate_MixedNeoHookean(material, &material->ctx_params));
  PetscCall(RatelMaterialParamsSmootherDataSetup_MixedNeoHookean(material));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Create Elasticity Mixed Neo-Hookean Current Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Register Mixed Neo-Hookean hyperelasticity at finite strain in current configuration model.

  Not collective across MPI processes.

  @param[in]   ratel                      `Ratel` context
  @param[in]   cl_argument                Command line argument to use for model
  @param[out]  material_create_functions  PETSc function list for `RatelMaterial`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelRegisterModel_ElasticityMixedNeoHookeanCurrent(Ratel ratel, const char *cl_argument,
                                                                   PetscFunctionList *material_create_functions) {
  PetscFunctionBeginUser;
  RATEL_MODEL_REGISTER(material_create_functions, cl_argument, ElasticityMixedNeoHookeanCurrent);
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}
