/// @file
/// Provide weak stubs for Ratel models that could be missing

#include <petscsys.h>
#include <ratel-impl.h>
#include <ratel.h>

#define RATEL_MODEL(model_cl_argument, model_postfix)                                                                                                  \
  RATEL_INTERN PetscErrorCode RatelMaterialCreate_##model_postfix(Ratel, RatelMaterial) __attribute__((weak));                                         \
  PetscErrorCode              RatelMaterialCreate_##model_postfix(Ratel ratel, RatelMaterial material) {                                               \
    PetscFunctionBeginUser;                                                                                                               \
    SETERRQ(ratel->comm, PETSC_ERR_SUP, "Model data for " model_cl_argument " not compiled");                                             \
  }                                                                                                                                                    \
                                                                                                                                                       \
  RATEL_INTERN PetscErrorCode RatelRegisterModel_##model_postfix(Ratel, const char *, PetscFunctionList *) __attribute__((weak));                      \
  PetscErrorCode              RatelRegisterModel_##model_postfix(Ratel ratel, const char *cl_argument, PetscFunctionList *material_create_functions) { \
    PetscFunctionBeginUser;                                                                                                               \
    PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_WARNING, "---- Weak Registration of model \"%s\"", cl_argument));                    \
    PetscCall(PetscFunctionListAdd(material_create_functions, model_cl_argument, RatelMaterialCreate_##model_postfix));                   \
    PetscFunctionReturn(PETSC_SUCCESS);                                                                                                   \
  }

#include "ratel-model-list.h"
#undef RATEL_MODEL
