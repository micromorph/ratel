/// @file
/// Setup linear poroelasticity MMS parameters context objects

#include <ceed.h>
#include <petscsys.h>
#include <ratel-impl.h>
#include <ratel-material.h>
#include <ratel-model.h>
#include <ratel.h>
#include <ratel/models/mms-poroelasticity-linear.h>
#include <ratel/qfunctions/utils.h>
#include <stddef.h>
#include <stdio.h>

/// @addtogroup RatelMaterials
/// @{

/// @brief Linear poroelasticity MMS model parameters for `CeedQFunctionContext` and viewing
struct RatelModelParameterData_private mms_linear_poroelasticity_param_data_private = {
    .parameters     = {{
                           .name            = "A0",
                           .description     = "Shift parameter",
                           .default_value.s = 1e-3,
                           .is_required     = PETSC_FALSE,
                           .offset          = offsetof(RatelMMSPoroElasticityLinearParams, A0),
                           .type            = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name            = "shift",
                           .description     = "Sin shift parameter",
                           .default_value.s = RATEL_PI_DOUBLE / 2,
                           .is_required     = PETSC_FALSE,
                           .offset          = offsetof(RatelMMSPoroElasticityLinearParams, shift),
                           .type            = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name            = "scale",
                           .description     = "Sin scale parameter",
                           .default_value.s = RATEL_PI_DOUBLE * 2,
                           .is_required     = PETSC_FALSE,
                           .offset          = offsetof(RatelMMSPoroElasticityLinearParams, scale),
                           .type            = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name        = "time",
                           .description = "Current solver time",
                           .is_hidden   = PETSC_TRUE,
                           .offset      = offsetof(RatelMMSPoroElasticityLinearParams, time),
                           .type        = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name        = "material",
                           .description = "Material model parameters",
                           .is_hidden   = PETSC_TRUE,
                           .offset      = offsetof(RatelMMSPoroElasticityLinearParams, material),
                   }},
    .num_parameters = 5
};
RatelModelParameterData mms_linear_poroelasticity_param_data = &mms_linear_poroelasticity_param_data_private;

/**
  @brief Set `CeedQFunctionContext` data and register fields for linear poroelasticity MMS parameters.

  Not collective across MPI processes.

  @param[in]   material  `RatelMaterial` to setup model parameters context
  @param[in]   A0         Shift parameter
  @param[in]   shift      Sin shift parameter
  @param[in]   scale      Sin scale parameter
  @param[out]  ctx       `CeedQFunctionContext` for linear poroelasticity MMS parameters

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelCeedParamsContextCreate_MMS_PoroElasticityLinear(RatelMaterial material, CeedScalar A0, CeedScalar shift, CeedScalar scale,
                                                                            CeedQFunctionContext *ctx) {
  Ratel                               ratel      = material->ratel;
  RatelModelParameterData             param_data = material->model_data->mms_param_data;
  RatelMMSPoroElasticityLinearParams *params;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Create Parameters Context MMS Poroelasticity Linear"));

  // Create context data with correct scaling
  PetscCall(PetscNew(&params));
  params->A0    = A0;
  params->shift = shift;
  params->scale = scale;
  params->time  = RATEL_UNSET_INITIAL_TIME;
  {
    size_t            material_size;
    const CeedScalar *material_ctx;

    RatelCallCeed(ratel, CeedQFunctionContextGetContextSize(material->ctx_params, &material_size));
    RatelCallCeed(ratel, CeedQFunctionContextGetDataRead(material->ctx_params, CEED_MEM_HOST, &material_ctx));
    PetscCall(PetscMemcpy(params->material, material_ctx, material_size));
    RatelCallCeed(ratel, CeedQFunctionContextRestoreDataRead(material->ctx_params, &material_ctx));
  }

  // Create context object
  RatelCallCeed(ratel, CeedQFunctionContextCreate(ratel->ceed, ctx));
  RatelCallCeed(ratel, CeedQFunctionContextSetData(*ctx, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(*params), params));
  PetscCall(PetscFree(params));

  // Register parameters
  PetscCall(RatelModelParameterDataRegisterContextFields(ratel, param_data, *ctx));

  // Debugging output
  if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedQFunctionContextView(*ctx, stdout));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Create Parameters Context MMS Poroelasticity Linear Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Build `CeedQFunctionContext` for linear poroelasticity MMS parameters.

  Collective across MPI processes.

  @param[in]   material  `RatelMaterial` to setup model parameters context
  @param[out]  ctx       `CeedQFunctionContext` for linear poroelasticity MMS parameters

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMMSParamsContextCreate_PoroElasticityLinear(RatelMaterial material, CeedQFunctionContext *ctx) {
  Ratel      ratel = material->ratel;
  CeedScalar A0, shift, scale;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup MMS Parameters Context Poroelasticity Linear"));

  // Set model parameter data
  material->model_data->mms_param_data = mms_linear_poroelasticity_param_data;

  // Get default values;
  PetscCall(RatelModelParameterDataGetDefaultValue_Scalar(ratel, mms_linear_poroelasticity_param_data, "A0", &A0));
  PetscCall(RatelModelParameterDataGetDefaultValue_Scalar(ratel, mms_linear_poroelasticity_param_data, "shift", &shift));
  PetscCall(RatelModelParameterDataGetDefaultValue_Scalar(ratel, mms_linear_poroelasticity_param_data, "scale", &scale));

  // Get parameter values
  {
    char *cl_prefix;

    PetscCall(RatelMaterialGetCLPrefix(material, &cl_prefix));
    PetscOptionsBegin(ratel->comm, cl_prefix, "Linear poroelasticity MMS model parameters", NULL);

    PetscCall(PetscOptionsScalar("-a0", "MMS shift parameter", NULL, A0, &A0, NULL));
    PetscCall(RatelDebug(ratel, "---- A0: %f", A0));

    PetscCall(PetscOptionsScalar("-shift", "MMS sin shift parameter", NULL, shift, &shift, NULL));
    PetscCall(RatelDebug(ratel, "---- shift: %f", shift));

    PetscCall(PetscOptionsScalar("-scale", "MMS sin scale parameter", NULL, scale, &scale, NULL));
    PetscCall(RatelDebug(ratel, "---- scale: %f", scale));

    PetscOptionsEnd();  // End of setting model parameters
    PetscCall(PetscFree(cl_prefix));
  }

  // Create context
  PetscCall(RatelCeedParamsContextCreate_MMS_PoroElasticityLinear(material, A0, shift, scale, ctx));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup MMS Parameters Context Poroelasticity Linear Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}
