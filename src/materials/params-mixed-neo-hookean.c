/// @file
/// Setup mixed Neo-Hookean model params context objects

#include <ceed.h>
#include <math.h>
#include <petscsys.h>
#include <ratel-impl.h>
#include <ratel-material.h>
#include <ratel-model.h>
#include <ratel.h>
#include <ratel/models/mixed-neo-hookean.h>
#include <stddef.h>
#include <stdio.h>

/// @addtogroup RatelMaterials
/// @{

/// @brief Mixed Neo-Hookean model parameters for `CeedQFunctionContext` and viewing
struct RatelModelParameterData_private mixed_neo_hookean_param_data_private = {
    .parameters     = {{
                           .name        = "shift v",
                           .description = "shift for U_t",
                           .is_hidden   = PETSC_TRUE,
                           .offset      = offsetof(RatelMixedNeoHookeanElasticityParams, common_parameters[RATEL_COMMON_PARAMETER_SHIFT_V]),
                           .type        = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name        = "shift a",
                           .description = "shift for U_tt",
                           .is_hidden   = PETSC_TRUE,
                           .offset      = offsetof(RatelMixedNeoHookeanElasticityParams, common_parameters[RATEL_COMMON_PARAMETER_SHIFT_A]),
                           .type        = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name            = "rho",
                           .description     = "Density",
                           .units           = "kg/m^3",
                           .restrictions    = "rho >= 0",
                           .default_value.s = 1,
                           .is_required     = PETSC_FALSE,
                           .offset          = offsetof(RatelMixedNeoHookeanElasticityParams, common_parameters[RATEL_COMMON_PARAMETER_RHO]),
                           .type            = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "nu",
                           .description  = "Poisson's ratio",
                           .restrictions = "0 <= nu <= 0.5",
                           .is_required  = PETSC_TRUE,
                           .offset       = offsetof(RatelMixedNeoHookeanElasticityParams, nu),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "E",
                           .description  = "Young's Modulus",
                           .units        = "Pa",
                           .restrictions = "E >= 0",
                           .is_required  = PETSC_TRUE,
                           .offset       = offsetof(RatelMixedNeoHookeanElasticityParams, E),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name            = "nu_primal",
                           .description     = "To change full strain to deviatoric in mixed formulation",
                           .restrictions    = "-1. <= nu_primal < nu",
                           .default_value.s = -1.,
                           .is_required     = PETSC_FALSE,
                           .offset          = offsetof(RatelMixedNeoHookeanElasticityParams, nu_primal),
                           .type            = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name            = "nu_primal_pc",
                           .description     = "To change full strain to deviatoric in uu block pc",
                           .restrictions    = "-1. <= nu_primal_pc < nu",
                           .default_value.s = -1.,
                           .is_required     = PETSC_FALSE,
                           .offset          = offsetof(RatelMixedNeoHookeanElasticityParams, nu_primal_pc),
                           .type            = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name            = "sign_pp",
                           .description     = "To change the sign in pp block pc",
                           .restrictions    = "-1 <= sign_pp <= 1",
                           .default_value.i = -1,
                           .is_required     = PETSC_FALSE,
                           .offset          = offsetof(RatelMixedNeoHookeanElasticityParams, sign_pp),
                           .type            = CEED_CONTEXT_FIELD_INT32,
                   }, {
                           .name         = "mu",
                           .description  = "Shear Modulus: E / (2 (1 + nu))",
                           .units        = "Pa",
                           .restrictions = "mu >= 0",
                           .is_required  = PETSC_FALSE,
                           .offset       = offsetof(RatelMixedNeoHookeanElasticityParams, mu),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "lambda",
                           .description  = "First Lame parameter: 2 mu nu / (1 - 2 nu)",
                           .units        = "Pa",
                           .restrictions = "lambda >= 0",
                           .is_required  = PETSC_FALSE,
                           .offset       = offsetof(RatelMixedNeoHookeanElasticityParams, lambda),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "bulk",
                           .description  = "Bulk Modulus: 2 mu (1 + nu) / (3 (1 - 2 nu))",
                           .units        = "Pa",
                           .restrictions = "bulk >= 0",
                           .is_required  = PETSC_FALSE,
                           .offset       = offsetof(RatelMixedNeoHookeanElasticityParams, bulk),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "bulk_primal",
                           .description  = "Primal bulk Modulus:  2 mu (1 + nu_primal) / (3 (1 - 2 nu_primal))",
                           .units        = "Pa",
                           .restrictions = "bulk_primal >= 0",
                           .is_required  = PETSC_FALSE,
                           .offset       = offsetof(RatelMixedNeoHookeanElasticityParams, bulk_primal),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "bulk_primal_pc",
                           .description  = "Primal bulk Modulus for block pc: 2 mu (1 + nu_primal_pc) / (3 (1 - 2 nu_primal_pc))",
                           .units        = "Pa",
                           .restrictions = "bulk_primal_pc >= 0",
                           .is_required  = PETSC_FALSE,
                           .offset       = offsetof(RatelMixedNeoHookeanElasticityParams, bulk_primal_pc),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "two_mu",
                           .description  = "Shear Modulus multiplied by 2",
                           .units        = "Pa",
                           .restrictions = "mu >= 0",
                           .is_required  = PETSC_FALSE,
                           .offset       = offsetof(RatelMixedNeoHookeanElasticityParams, two_mu),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }},
    .num_parameters = 14,
    .reference      = "ISBN: 0-471-82319-8"
};
RatelModelParameterData mixed_neo_hookean_param_data = &mixed_neo_hookean_param_data_private;

/**
  @brief Set `CeedQFunctionContext` data and register fields for mixed Neo-Hookean parameters.

  Collective across MPI processes.

  @param[in]   material      `RatelMaterial` to setup model params context
  @param[in]   nu            Poisson's ratio
  @param[in]   E             Young's Modulus
  @param[in]   rho           Density for scaled mass matrix
  @param[in]   nu_primal     To change full strain to deviatoric in mixed formulation
  @param[in]   nu_primal_pc  To change full strain to deviatoric in uu block pc
  @param[in]   sign_pp       To change the sign in pp block pc
  @param[out]  ctx           `CeedQFunctionContext` for mixed Neo-Hookean parameters

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelCeedParamsContextCreate_MixedNeoHookean(RatelMaterial material, CeedScalar nu, CeedScalar E, CeedScalar rho,
                                                                   CeedScalar nu_primal, CeedScalar nu_primal_pc, CeedInt sign_pp,
                                                                   CeedQFunctionContext *ctx) {
  Ratel                                 ratel      = material->ratel;
  RatelModelParameterData               param_data = material->model_data->param_data;
  RatelMixedNeoHookeanElasticityParams *params;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Create Parameters Context mixed Neo-Hookean"));

  // Create context data with correct scaling
  PetscCall(PetscNew(&params));
  params->nu                                            = nu;
  params->E                                             = E * ratel->material_units->Pascal;
  params->common_parameters[RATEL_COMMON_PARAMETER_RHO] = rho * (ratel->material_units->kilogram) / pow(ratel->material_units->meter, 3);
  params->nu_primal                                     = nu_primal;
  params->nu_primal_pc                                  = nu_primal_pc;
  params->sign_pp                                       = sign_pp;
  params->mu                                            = params->E / (2 * (1 + params->nu));
  params->two_mu                                        = 2 * params->mu;
  params->bulk_primal                                   = params->two_mu * (1 + params->nu_primal) / (3. * (1 - 2 * params->nu_primal));
  params->bulk_primal_pc                                = params->two_mu * (1 + params->nu_primal_pc) / (3. * (1 - 2 * params->nu_primal_pc));
  if (params->nu == 0.5) {
    params->lambda = 0.;
    params->bulk   = 0.;
  } else {
    params->lambda = params->two_mu * params->nu / (1 - 2 * params->nu);
    params->bulk   = params->two_mu * (1 + params->nu) / (3. * (1 - 2 * params->nu));
  }

  // Create context object
  RatelCallCeed(ratel, CeedQFunctionContextCreate(ratel->ceed, ctx));
  RatelCallCeed(ratel, CeedQFunctionContextSetData(*ctx, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(*params), params));
  PetscCall(PetscFree(params));

  // Register parameters
  PetscCall(RatelModelParameterDataRegisterContextFields(ratel, param_data, *ctx));

  // Debugging output
  if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedQFunctionContextView(*ctx, stdout));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Create Parameters Context mixed Neo-Hookean Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Build `CeedQFunctionContext` with mixed Neo-Hookean parameters.

  Collective across MPI processes.

  @param[in]   material  `RatelMaterial` to setup model params context
  @param[out]  ctx       `CeedQFunctionContext` for Neo-Hookean parameters

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialParamsContextCreate_MixedNeoHookean(RatelMaterial material, CeedQFunctionContext *ctx) {
  Ratel      ratel      = material->ratel;
  PetscBool  set_nu     = PETSC_FALSE;
  PetscBool  set_Youngs = PETSC_FALSE;
  CeedScalar nu = 0.0, E = 0.0, rho, nu_primal, nu_primal_pc;
  PetscInt   sign_pp;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup Model Parameters Context mixed Neo-Hookean"));

  // Set model parameter data
  material->model_data->param_data = mixed_neo_hookean_param_data;

  // Get default values;
  PetscCall(RatelModelParameterDataGetDefaultValue_Scalar(ratel, mixed_neo_hookean_param_data, "rho", &rho));
  PetscCall(RatelModelParameterDataGetDefaultValue_Scalar(ratel, mixed_neo_hookean_param_data, "nu_primal", &nu_primal));
  PetscCall(RatelModelParameterDataGetDefaultValue_Scalar(ratel, mixed_neo_hookean_param_data, "nu_primal_pc", &nu_primal_pc));
  {
    CeedInt default_sign_pp;

    PetscCall(RatelModelParameterDataGetDefaultValue_Int(ratel, mixed_neo_hookean_param_data, "sign_pp", &default_sign_pp));
    sign_pp = default_sign_pp;
  }

  // Get parameter values
  {
    char *cl_prefix;

    PetscCall(RatelMaterialGetCLPrefix(material, &cl_prefix));
    PetscOptionsBegin(ratel->comm, cl_prefix, "mixed Neo-Hookean model parameters", NULL);

    PetscCall(PetscOptionsScalar("-nu", "Poisson's ratio", NULL, nu, &nu, &set_nu));
    PetscCheck(nu >= 0 && nu <= 0.5, ratel->comm, PETSC_ERR_SUP, "mixed Neo-Hookean model requires Poisson ratio -nu option in [0, 0.5]");
    PetscCall(RatelDebug(ratel, "---- nu: %f", nu));

    PetscCall(PetscOptionsScalar("-E", "Young's Modulus", NULL, E, &E, &set_Youngs));
    PetscCheck(E >= 0, ratel->comm, PETSC_ERR_SUP, "mixed Neo-Hookean model requires non-negative Young's Modulus -E option (Pa)");
    PetscCall(RatelDebug(ratel, "---- E: %f", E));

    PetscCall(PetscOptionsScalar("-rho", "Density for scaled mass matrix", NULL, rho, &rho, NULL));
    PetscCheck(rho >= 0.0, ratel->comm, PETSC_ERR_SUP, "Density must be a positive value");
    PetscCall(RatelDebug(ratel, "---- rho: %f", rho));

    PetscCall(PetscOptionsScalar("-nu_primal", "To change full strain to deviatoric in mixed jacobian", NULL, nu_primal, &nu_primal, NULL));
    PetscCheck(nu_primal >= -1. && nu_primal < nu, ratel->comm, PETSC_ERR_SUP,
               "To change full strain to deviatoric in mixed jacobian -nu_primal option in [-1., nu)");
    PetscCall(RatelDebug(ratel, "---- nu_primal: %f", nu_primal));

    PetscCall(PetscOptionsScalar("-nu_primal_pc", "To change full strain to deviatoric in uu block pc", NULL, nu_primal_pc, &nu_primal_pc, NULL));
    PetscCheck(nu_primal_pc >= -1. && nu_primal_pc < nu, ratel->comm, PETSC_ERR_SUP,
               "To change full strain to deviatoric in uu block pc -nu_primal_pc option in [-1., nu)");
    PetscCall(RatelDebug(ratel, "---- nu_primal_pc: %f", nu_primal_pc));

    PetscCall(PetscOptionsInt("-sign_pp", "To change the sign in pp block pc", NULL, sign_pp, &sign_pp, NULL));
    PetscCheck(sign_pp >= -1 && sign_pp <= 1, ratel->comm, PETSC_ERR_SUP, "To change the sign in pp block pc -sign_pp option in [-1, 1]");
    PetscCall(RatelDebug(ratel, "---- sign_pp: %d", sign_pp));

    PetscOptionsEnd();  // End of setting model parameters
    PetscCall(PetscFree(cl_prefix));
  }

  // Check for all required options to be set
  PetscCheck(set_nu, ratel->comm, PETSC_ERR_SUP, "-nu option needed");
  PetscCheck(set_Youngs, ratel->comm, PETSC_ERR_SUP, "-E option needed");

  // Create context
  PetscCall(RatelCeedParamsContextCreate_MixedNeoHookean(material, nu, E, rho, nu_primal, nu_primal_pc, sign_pp, ctx));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup Model Parameters Context mixed Neo-Hookean Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup data for `CeedQFunctionContext` for smoother with mixed Neo-Hookean parameters.

  Collective across MPI processes.

  @param[in,out]  material  `RatelMaterial` to setup model params context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialParamsSmootherDataSetup_MixedNeoHookean(RatelMaterial material) {
  Ratel      ratel           = material->ratel;
  PetscBool  set_nu_smoother = PETSC_FALSE;
  CeedScalar nu = 0.0, nu_smoother = 0.0;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup Model Parameters Smoother Context mixed Neo-Hookean"));

  // Get parameter values
  if (material->num_params_smoother_values == 0) {
    char *cl_prefix;

    PetscCall(RatelMaterialGetCLPrefix(material, &cl_prefix));
    PetscOptionsBegin(ratel->comm, cl_prefix, "mixed Neo-Hookean model parameters for smoother", NULL);

    PetscCall(PetscOptionsScalar("-nu", "Poisson's ratio", NULL, nu, &nu, NULL));
    PetscCheck(nu >= 0 && nu <= 0.5, ratel->comm, PETSC_ERR_SUP, "Mixed Neo-Hookean model requires Poisson ratio -nu option in [0, 0.5]");
    PetscCall(RatelDebug(ratel, "---- nu: %f", nu));

    PetscCall(PetscOptionsScalar("-nu_smoother", "Poisson's ratio for smoother", NULL, nu_smoother, &nu_smoother, &set_nu_smoother));
    if (set_nu_smoother) PetscCall(RatelDebug(ratel, "---- nu_smoother: %f", nu_smoother));
    else PetscCall(RatelDebug(ratel, "---- No Nu for smoother set"));

    PetscOptionsEnd();  // End of setting model parameters
    PetscCall(PetscFree(cl_prefix));

    // Store smoother values, if needed
    if (set_nu_smoother) {
      const char *label_name     = "nu";
      PetscSizeT  label_name_len = 0;

      material->num_params_smoother_values = 1;
      PetscCall(PetscNew(&material->ctx_params_values));
      PetscCall(PetscNew(&material->ctx_params_smoother_values));
      material->ctx_params_values[0]          = nu;
      material->ctx_params_smoother_values[0] = nu_smoother;
      PetscCall(PetscNew(&material->ctx_params_label_names));
      PetscCall(PetscStrlen(label_name, &label_name_len));
      PetscCall(PetscCalloc1(label_name_len + 1, &material->ctx_params_label_names[0]));
      PetscCall(PetscStrncpy(material->ctx_params_label_names[0], label_name, label_name_len + 1));
    }
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup Model Parameters Smoother Context mixed Neo-Hookean Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}
