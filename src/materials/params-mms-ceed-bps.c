/// @file
/// Setup CEED BPs MMS parameters context objects

#include <ceed.h>
#include <petscsys.h>
#include <ratel-impl.h>
#include <ratel-material.h>
#include <ratel-model.h>
#include <ratel.h>
#include <ratel/models/mms-ceed-bps.h>
#include <stddef.h>
#include <stdio.h>

/// @addtogroup RatelMaterials
/// @{

/// @brief CEED BPs MMS model parameters for `CeedQFunctionContext` and viewing
struct RatelModelParameterData_private mms_ceed_bps_param_data_private = {
    .parameters     = {{
                           .name            = "weierstrass_a",
                           .description     = "First Weierstrass parameter",
                           .default_value.s = 0.5,
                           .is_required     = PETSC_FALSE,
                           .is_hidden       = PETSC_FALSE,
                           .offset          = offsetof(RatelMMSCEEDBPsParams, weierstrass_a),
                           .type            = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name            = "weierstrass_b",
                           .default_value.s = 1.5,
                           .is_required     = PETSC_FALSE,
                           .is_hidden       = PETSC_FALSE,
                           .description     = "Second Weierstrass parameter",
                           .offset          = offsetof(RatelMMSCEEDBPsParams, weierstrass_b),
                           .type            = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name            = "weierstrass_n",
                           .default_value.i = 2,
                           .is_required     = PETSC_FALSE,
                           .is_hidden       = PETSC_FALSE,
                           .description     = "Number of terms in Weierstrass series",
                           .offset          = offsetof(RatelMMSCEEDBPsParams, weierstrass_n),
                           .type            = CEED_CONTEXT_FIELD_INT32,
                   }, {
                           .name        = "time",
                           .description = "Current solver time",
                           .is_hidden   = PETSC_TRUE,
                           .offset      = offsetof(RatelMMSCEEDBPsParams, time),
                   }},
    .num_parameters = 4,
    .reference      = "ISSN: 1802-114X"
};
RatelModelParameterData mms_ceed_bps_param_data = &mms_ceed_bps_param_data_private;

/**
  @brief Set `CeedQFunctionContext` data and register fields for CEED BPs MMS parameters.

  Not collective across MPI processes.

  @param[in]   material       `RatelMaterial` to setup model parameters context
  @param[in]   weierstrass_a  First Weierstrass parameter
  @param[in]   weierstrass_b  Second Weierstrass parameter
  @param[in]   weierstrass_n  Number of terms in Weierstrass series
  @param[out]  ctx            `CeedQFunctionContext` for CEED BPs MMS parameters

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelCeedParamsContextCreate_MMS_CEED_BPs(RatelMaterial material, CeedScalar weierstrass_a, CeedScalar weierstrass_b,
                                                                CeedInt weierstrass_n, CeedQFunctionContext *ctx) {
  Ratel                   ratel      = material->ratel;
  RatelModelParameterData param_data = material->model_data->mms_param_data;
  RatelMMSCEEDBPsParams  *params;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Create Parameters Context MMS CEED BPs"));

  // Create context data with correct scaling
  PetscCall(PetscNew(&params));
  params->weierstrass_a = weierstrass_a;
  params->weierstrass_b = weierstrass_b;
  params->weierstrass_n = weierstrass_n;
  params->time          = RATEL_UNSET_INITIAL_TIME;

  // Create context object
  RatelCallCeed(ratel, CeedQFunctionContextCreate(ratel->ceed, ctx));
  RatelCallCeed(ratel, CeedQFunctionContextSetData(*ctx, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(*params), params));
  PetscCall(PetscFree(params));

  // Register parameters
  PetscCall(RatelModelParameterDataRegisterContextFields(ratel, param_data, *ctx));

  // Debugging output
  if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedQFunctionContextView(*ctx, stdout));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Create Parameters Context MMS CEED BPs Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Build `CeedQFunctionContext` for CEED BPs MMS parameters.

  Collective across MPI processes.

  @param[in]   material  `RatelMaterial` to setup model parameters context
  @param[out]  ctx       `CeedQFunctionContext` for CEED BPs MMS parameters

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMMSParamsContextCreate_CEED_BPs(RatelMaterial material, CeedQFunctionContext *ctx) {
  Ratel      ratel = material->ratel;
  PetscInt   weierstrass_n;
  CeedScalar weierstrass_a, weierstrass_b;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup MMS Parameters Context CEED BPs"));

  // Set model parameter data
  material->model_data->mms_param_data = mms_ceed_bps_param_data;

  // Get default values;
  PetscCall(RatelModelParameterDataGetDefaultValue_Scalar(ratel, mms_ceed_bps_param_data, "weierstrass_a", &weierstrass_a));
  PetscCall(RatelModelParameterDataGetDefaultValue_Scalar(ratel, mms_ceed_bps_param_data, "weierstrass_b", &weierstrass_b));
  {
    CeedInt default_n;

    PetscCall(RatelModelParameterDataGetDefaultValue_Int(ratel, mms_ceed_bps_param_data, "weierstrass_n", &default_n));
    weierstrass_n = default_n;
  }

  // Get parameter values
  {
    char *cl_prefix;

    PetscCall(RatelMaterialGetCLPrefix(material, &cl_prefix));
    PetscOptionsBegin(ratel->comm, cl_prefix, "CEED BPs MMS model parameters", NULL);

    PetscCall(PetscOptionsScalar("-weierstrass_a", "First Weierstrass parameter", NULL, weierstrass_a, &weierstrass_a, NULL));
    PetscCall(RatelDebug(ratel, "---- weierstrass_a: %f", weierstrass_a));

    PetscCall(PetscOptionsScalar("-weierstrass_b", "Second Weierstrass parameter", NULL, weierstrass_b, &weierstrass_b, NULL));
    PetscCall(RatelDebug(ratel, "---- weierstrass_b: %f", weierstrass_b));

    PetscCall(PetscOptionsInt("-weierstrass_n", "Number of terms in Weierstrass series", NULL, weierstrass_n, &weierstrass_n, NULL));
    PetscCall(RatelDebug(ratel, "---- weierstrass_n: %d", weierstrass_n));

    PetscOptionsEnd();  // End of setting model parameters
    PetscCall(PetscFree(cl_prefix));
  }

  // Create context
  PetscCall(RatelCeedParamsContextCreate_MMS_CEED_BPs(material, weierstrass_a, weierstrass_b, weierstrass_n, ctx));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup MMS Parameters Context CEED BPs Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}
