/// @file
/// Elasticity Hencky, initial configuration, principal directions

#include <ceed.h>
#include <petscsys.h>
#include <ratel-impl.h>
#include <ratel-model.h>
#include <ratel.h>
#include <ratel/qfunctions/geometry/surface-force.h>
#include <ratel/qfunctions/geometry/surface.h>
#include <ratel/qfunctions/geometry/volumetric.h>
#include <ratel/qfunctions/models/elasticity-dual-diagnostic.h>
#include <ratel/qfunctions/models/elasticity-hencky-initial-principal.h>

static const CeedInt      active_field_sizes[]          = {3};
static const char *const  active_field_names[]          = {"displacement"};
static const char *const  active_component_names[]      = {"displacement_x", "displacement_y", "displacement_z"};
static const CeedInt      active_field_num_eval_modes[] = {1};
static const CeedEvalMode active_field_eval_modes[]     = {CEED_EVAL_GRAD};

static const char *const diagnostic_field_names[] = {"displacement_x",
                                                     "displacement_y",
                                                     "displacement_z",
                                                     "Cauchy_stress_xx",
                                                     "Cauchy_stress_xy",
                                                     "Cauchy_stress_xz",
                                                     "Cauchy_stress_yy",
                                                     "Cauchy_stress_yz",
                                                     "Cauchy_stress_zz",
                                                     "pressure",
                                                     "volumetric_strain",
                                                     "trace_E2",
                                                     "J",
                                                     "strain_energy_density",
                                                     "von_Mises_stress",
                                                     "mass_density"};

struct RatelModelData_private elasticity_hencky_initial_data_private = {
    .name                                 = "elasticity in logarithmic strains initial configuration",
    .command_line_option                  = "elasticity-hencky-initial-principal",
    .setup_q_data_volume                  = SetupVolumeGeometry,
    .setup_q_data_volume_loc              = SetupVolumeGeometry_loc,
    .setup_q_data_surface                 = SetupSurfaceGeometry,
    .setup_q_data_surface_loc             = SetupSurfaceGeometry_loc,
    .setup_q_data_surface_grad            = SetupSurfaceForceGeometry,
    .setup_q_data_surface_grad_loc        = SetupSurfaceForceGeometry_loc,
    .q_data_volume_size                   = Q_DATA_VOLUMETRIC_GEOMETRY_SIZE,
    .q_data_surface_size                  = Q_DATA_SURFACE_GEOMETRY_SIZE,
    .q_data_surface_grad_size             = Q_DATA_SURFACE_FORCE_GEOMETRY_SIZE,
    .num_active_fields                    = sizeof(active_field_sizes) / sizeof(*active_field_sizes),
    .active_field_sizes                   = active_field_sizes,
    .active_field_names                   = active_field_names,
    .active_component_names               = active_component_names,
    .active_field_num_eval_modes          = active_field_num_eval_modes,
    .active_field_eval_modes              = active_field_eval_modes,
    .num_comp_projected_diagnostic        = NUM_COMPONENTS_DIAGNOSTIC_Elasticity,
    .projected_diagnostic_component_names = diagnostic_field_names,
    .num_comp_dual_diagnostic             = NUM_COMPONENTS_ELASTICITY_DIAGNOSTIC_Dual,
    .dual_diagnostic_component_names      = RatelElasticityDualDiagnosticComponentNames,
    .quadrature_mode                      = CEED_GAUSS,
    .residual_u                           = ElasticityHenckyInitialPrincipal_Residual,
    .residual_u_loc                       = ElasticityHenckyInitialPrincipal_Residual_loc,
    .num_comp_stored_u                    = NUM_COMPONENTS_STORED_ElasticityHenckyInitialPrincipal,
    .jacobian                             = ElasticityHenckyInitialPrincipal_Jacobian,
    .jacobian_loc                         = ElasticityHenckyInitialPrincipal_Jacobian_loc,
    .num_comp_state                       = NUM_COMPONENTS_STATE_ElasticityHenckyInitialPrincipal,
    .projected_diagnostic                 = Diagnostic_ElasticityHencky,
    .projected_diagnostic_loc             = Diagnostic_ElasticityHencky_loc,
    .dual_diagnostic                      = ElasticityDualDiagnostic,
    .dual_diagnostic_loc                  = ElasticityDualDiagnostic_loc,
    .strain_energy                        = StrainEnergy_ElasticityHencky,
    .strain_energy_loc                    = StrainEnergy_ElasticityHencky_loc,
    .platen_residual_u                    = ElasticityHenckyInitialPrincipalPlaten_Residual,
    .platen_residual_u_loc                = ElasticityHenckyInitialPrincipalPlaten_Residual_loc,
    .platen_jacobian                      = ElasticityHenckyInitialPrincipalPlaten_Jacobian,
    .platen_jacobian_loc                  = ElasticityHenckyInitialPrincipalPlaten_Jacobian_loc,
    .flops_qf_jacobian_u                  = FLOPS_JACOBIAN_ElasticityHenckyInitialPrincipal,
    .flops_qf_jacobian_platen             = FLOPS_Platen_without_df1 + FLOPS_JACOBIAN_ElasticityHenckyInitialPrincipal,
};
RatelModelData elasticity_hencky_initial_data = &elasticity_hencky_initial_data_private;

/// @addtogroup RatelMaterials
/// @{

/**
  @brief Create RatelMaterial for Elasticity Hencky initial conf.

  @param[in]   ratel     Ratel context
  @param[out]  material  RatelMaterial context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialCreate_ElasticityHenckyInitialPrincipal(Ratel ratel, RatelMaterial material) {
  PetscFunctionBegin;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Create ElasticityHenckyInitialPrincipal"));

  // Model data
  PetscCall(RatelModelDataVerifyRelativePath(ratel, elasticity_hencky_initial_data));
  material->model_data = elasticity_hencky_initial_data;

  // QFunction contexts
  PetscCall(RatelMaterialParamsContextCreate_NeoHookean(material, &material->ctx_params));
  PetscCall(RatelMaterialParamsSmootherDataSetup_NeoHookean(material));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Create ElasticityHenckyInitialPrincipal Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Register elasticity model

  @param[in]   ratel                      Ratel context
  @param[in]   cl_argument                Command line argument to use for model
  @param[out]  material_create_functions  PETSc function list for RatelMaterials

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelRegisterModel_ElasticityHenckyInitialPrincipal(Ratel ratel, const char *cl_argument,
                                                                   PetscFunctionList *material_create_functions) {
  PetscFunctionBegin;
  RATEL_MODEL_REGISTER(material_create_functions, cl_argument, ElasticityHenckyInitialPrincipal);
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}
