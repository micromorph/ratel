/// @file
/// Setup Neo-Hookean PoroElasticity model params context objects

#include <ceed.h>
#include <math.h>
#include <petscsys.h>
#include <ratel-impl.h>
#include <ratel-material.h>
#include <ratel-model.h>
#include <ratel.h>
#include <ratel/models/poroelasticity-neo-hookean.h>
#include <stddef.h>
#include <stdio.h>

/// @addtogroup RatelMaterials
/// @{

/// @brief Neo-Hookean poroelasticity model parameters for `CeedQFunctionContext` and viewing
struct RatelModelParameterData_private poroelasticity_neo_hookean_param_data_private = {
    .parameters     = {{
                           .name        = "shift v",
                           .description = "shift for U_t",
                           .offset      = offsetof(RatelNeoHookeanPoroElasticityParams, common_parameters[RATEL_COMMON_PARAMETER_SHIFT_V]),
                           .is_hidden   = PETSC_TRUE,
                           .type        = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name        = "shift a",
                           .description = "shift for U_tt",
                           .offset      = offsetof(RatelNeoHookeanPoroElasticityParams, common_parameters[RATEL_COMMON_PARAMETER_SHIFT_A]),
                           .is_hidden   = PETSC_TRUE,
                           .type        = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name            = "rho",
                           .description     = "Density",
                           .units           = "kg/m^3",
                           .restrictions    = "rho >= 0",
                           .default_value.s = 1,
                           .is_required     = PETSC_FALSE,
                           .offset          = offsetof(RatelNeoHookeanPoroElasticityParams, common_parameters[RATEL_COMMON_PARAMETER_RHO]),
                           .type            = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name            = "sign_pp",
                           .description     = "To change the sign in pp block pc",
                           .restrictions    = "-1 <= sign_pp <= 1",
                           .default_value.i = -1,
                           .is_required     = PETSC_FALSE,
                           .offset          = offsetof(RatelNeoHookeanPoroElasticityParams, sign_pp),
                           .type            = CEED_CONTEXT_FIELD_INT32,
                   }, {
                           .name         = "lambda_d",
                           .description  = "First Lame parameter for drained",
                           .units        = "Pa",
                           .restrictions = "lambda_d >= 0",
                           .is_required  = PETSC_TRUE,
                           .offset       = offsetof(RatelNeoHookeanPoroElasticityParams, lambda_d),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "mu_d",
                           .description  = "Shear Modulus for drained",
                           .units        = "Pa",
                           .restrictions = "mu_d >= 0",
                           .is_required  = PETSC_TRUE,
                           .offset       = offsetof(RatelNeoHookeanPoroElasticityParams, mu_d),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "bulk_d",
                           .description  = "Bulk Modulus for drained",
                           .units        = "Pa",
                           .restrictions = "bulk_d >= 0",
                           .is_required  = PETSC_TRUE,
                           .offset       = offsetof(RatelNeoHookeanPoroElasticityParams, bulk_d),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "bulk_s",
                           .description  = "Bulk Modulus for solid",
                           .units        = "Pa",
                           .restrictions = "bulk_s >= 0",
                           .is_required  = PETSC_TRUE,
                           .offset       = offsetof(RatelNeoHookeanPoroElasticityParams, bulk_s),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "bulk_f",
                           .description  = "Bulk Modulus for fluid",
                           .units        = "Pa",
                           .restrictions = "bulk_f >= 0",
                           .is_required  = PETSC_TRUE,
                           .offset       = offsetof(RatelNeoHookeanPoroElasticityParams, bulk_f),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "phi_0",
                           .description  = "Porosity at t=0",
                           .restrictions = "0 < phi_0 < 1",
                           .is_required  = PETSC_TRUE,
                           .offset       = offsetof(RatelNeoHookeanPoroElasticityParams, phi_0),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "eta_f",
                           .description  = "Fluid viscosity",
                           .units        = "Pa * s",
                           .restrictions = "eta_f >= 0",
                           .is_required  = PETSC_TRUE,
                           .offset       = offsetof(RatelNeoHookeanPoroElasticityParams, eta_f),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "varkappa_0",
                           .description  = "Intrinsic Permeability",
                           .units        = "m^2",
                           .restrictions = "varkappa_0 >= 0",
                           .is_required  = PETSC_TRUE,
                           .offset       = offsetof(RatelNeoHookeanPoroElasticityParams, varkappa_0),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "B",
                           .description  = "Biot's coefficient: 1 - k_d/k_s",
                           .restrictions = "0 <= B <= 1",
                           .is_required  = PETSC_FALSE,
                           .is_hidden    = PETSC_FALSE,
                           .offset       = offsetof(RatelNeoHookeanPoroElasticityParams, B),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "M",
                           .description  = "Biot's modulus: 1 / M = phi_0/k_f + (B - phi_0)/k_s",
                           .units        = "Pa",
                           .restrictions = "M >= 0",
                           .is_required  = PETSC_FALSE,
                           .is_hidden    = PETSC_FALSE,
                           .offset       = offsetof(RatelNeoHookeanPoroElasticityParams, M),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "two_mu_d",
                           .description  = "Shear Modulus multiplied by 2",
                           .units        = "Pa",
                           .restrictions = "mu_d >= 0",
                           .is_required  = PETSC_FALSE,
                           .offset       = offsetof(RatelNeoHookeanPoroElasticityParams, two_mu_d),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }},
    .num_parameters = 15,
    .reference      = "doi:10.1115/1.4023692"
};
RatelModelParameterData poroelasticity_neo_hookean_param_data = &poroelasticity_neo_hookean_param_data_private;

/**
  @brief Set `CeedQFunctionContext` data and register fields for Neo-Hookean Poroelasticity parameters.

  Collective across MPI processes.

  @param[in]   material      `RatelMaterial` to setup model params context
  @param[in]   rho           Density for scaled mass matrix
  @param[in]   lambda_d      First Lame parameter for drained state
  @param[in]   mu_d          Shear Modulus for drained state
  @param[in]   bulk_d        Bulk Modulus for drained state
  @param[in]   bulk_s        Bulk Modulus for solid
  @param[in]   bulk_f        Bulk Modulus for fluid
  @param[in]   phi_0         Porosity at t = 0
  @param[in]   eta_f         Fluid viscosity
  @param[in]   varkappa_0    Intrinsic Permeability
  @param[in]   sign_pp       Sign to use in pp block
  @param[out]  ctx           `CeedQFunctionContext` for Neo-Hookean Poroelasticity parameters

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelCeedParamsContextCreate_PoroElasticityNeoHookean(RatelMaterial material, CeedScalar rho, CeedScalar lambda_d,
                                                                            CeedScalar mu_d, CeedScalar bulk_d, CeedScalar bulk_s, CeedScalar bulk_f,
                                                                            CeedScalar phi_0, CeedScalar eta_f, CeedScalar varkappa_0,
                                                                            CeedInt sign_pp, CeedQFunctionContext *ctx) {
  Ratel                                ratel      = material->ratel;
  RatelModelParameterData              param_data = material->model_data->param_data;
  RatelNeoHookeanPoroElasticityParams *params;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Create Parameters Context Neo-Hookean Poroelasticity"));

  // Create context data with correct scaling
  PetscCall(PetscNew(&params));
  params->common_parameters[RATEL_COMMON_PARAMETER_RHO] = rho * (ratel->material_units->kilogram) / pow(ratel->material_units->meter, 3);
  params->lambda_d                                      = lambda_d;
  params->mu_d                                          = mu_d;
  params->bulk_d                                        = bulk_d;
  params->bulk_s                                        = bulk_s;
  params->bulk_f                                        = bulk_f;
  params->phi_0                                         = phi_0;
  params->eta_f                                         = eta_f;
  params->varkappa_0                                    = varkappa_0;
  params->sign_pp                                       = sign_pp;
  params->B                                             = 1 - (params->bulk_d / params->bulk_s);
  params->M        = (params->bulk_s * params->bulk_f) / (params->bulk_s * params->phi_0 + params->bulk_f * (params->B - params->phi_0));
  params->two_mu_d = 2 * params->mu_d;

  // Create context object
  RatelCallCeed(ratel, CeedQFunctionContextCreate(ratel->ceed, ctx));
  RatelCallCeed(ratel, CeedQFunctionContextSetData(*ctx, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(*params), params));
  PetscCall(PetscFree(params));

  // Register parameters
  PetscCall(RatelModelParameterDataRegisterContextFields(ratel, param_data, *ctx));

  // Debugging output
  if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedQFunctionContextView(*ctx, stdout));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Create Parameters Context Neo-Hookean Poroelasticity Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Build `CeedQFunctionContext` with Neo-Hookean Poroelasticity parameters.

  Collective across MPI processes.

  @param[in]   material  `RatelMaterial` to setup model params context
  @param[out]  ctx       `CeedQFunctionContext` for Neo-Hookean Poroelasticity parameters

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialParamsContextCreate_PoroElasticityNeoHookean(RatelMaterial material, CeedQFunctionContext *ctx) {
  Ratel      ratel          = material->ratel;
  PetscBool  set_lambda_d   = PETSC_FALSE;
  PetscBool  set_mu_d       = PETSC_FALSE;
  PetscBool  set_bulk_d     = PETSC_FALSE;
  PetscBool  set_bulk_s     = PETSC_FALSE;
  PetscBool  set_bulk_f     = PETSC_FALSE;
  PetscBool  set_phi_0      = PETSC_FALSE;
  PetscBool  set_eta_f      = PETSC_FALSE;
  PetscBool  set_varkappa_0 = PETSC_FALSE;
  CeedScalar lambda_d = 0.0, mu_d = 0.0, bulk_d = 0.0, bulk_s = 0.0, bulk_f = 0.0, phi_0 = 0.0, eta_f = 0.0, varkappa_0 = 0.0, rho;
  PetscInt   sign_pp;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup Model Parameters Context Neo-Hookean Poroelasticity"));

  // Set model parameter data
  material->model_data->param_data = poroelasticity_neo_hookean_param_data;

  // Get default values;
  PetscCall(RatelModelParameterDataGetDefaultValue_Scalar(ratel, poroelasticity_neo_hookean_param_data, "rho", &rho));
  {
    CeedInt default_sign_pp;

    PetscCall(RatelModelParameterDataGetDefaultValue_Int(ratel, poroelasticity_neo_hookean_param_data, "sign_pp", &default_sign_pp));
    sign_pp = default_sign_pp;
  }

  // Get parameter values
  {
    char *cl_prefix;

    PetscCall(RatelMaterialGetCLPrefix(material, &cl_prefix));
    PetscOptionsBegin(ratel->comm, cl_prefix, "Neo-Hookean Poroelasticity model parameters", NULL);

    PetscCall(PetscOptionsScalar("-rho", "Density for scaled mass matrix", NULL, rho, &rho, NULL));
    PetscCheck(rho >= 0.0, ratel->comm, PETSC_ERR_SUP, "Density must be a positive value");
    PetscCall(RatelDebug(ratel, "---- rho: %f", rho));

    PetscCall(PetscOptionsScalar("-lambda_d", "First Lame parameter for drained state", NULL, lambda_d, &lambda_d, &set_lambda_d));
    PetscCheck(lambda_d >= 0, ratel->comm, PETSC_ERR_SUP,
               "Neo-Hookean Poroelasticity model requires non-negative first Lame parameter -lambda_d option (Pa)");
    PetscCall(RatelDebug(ratel, "---- lambda_d: %f", lambda_d));

    // mu_d
    PetscCall(PetscOptionsScalar("-mu_d", "Shear Modulus for drained state", NULL, mu_d, &mu_d, &set_mu_d));
    PetscCheck(mu_d >= 0, ratel->comm, PETSC_ERR_SUP, "Neo-Hookean Poroelasticity model requires non-negative -mu_d option (Pa)");
    PetscCall(RatelDebug(ratel, "---- mu_d: %f", mu_d));

    // bulk_d
    PetscCall(PetscOptionsScalar("-bulk_d", "Bulk Modulus for drained state", NULL, bulk_d, &bulk_d, &set_bulk_d));
    PetscCheck(bulk_d >= 0, ratel->comm, PETSC_ERR_SUP, "Neo-Hookean Poroelasticity model requires non-negative -bulk_d option (Pa) ");
    PetscCall(RatelDebug(ratel, "---- bulk_d: %f", bulk_d));

    // bulk_s
    PetscCall(PetscOptionsScalar("-bulk_s", "Bulk Modulus for solid", NULL, bulk_s, &bulk_s, &set_bulk_s));
    PetscCheck(bulk_s >= 0, ratel->comm, PETSC_ERR_SUP, "Neo-Hookean Poroelasticity model requires non-negative -bulk_s option (Pa)");
    PetscCall(RatelDebug(ratel, "---- bulk_s: %f", bulk_s));

    // bulk_f
    PetscCall(PetscOptionsScalar("-bulk_f", "Bulk Modulus for fluid", NULL, bulk_f, &bulk_f, &set_bulk_f));
    PetscCheck(bulk_f >= 0, ratel->comm, PETSC_ERR_SUP, "Neo-Hookean Poroelasticity model requires non-negative -bulk_f option (Pa)");
    PetscCall(RatelDebug(ratel, "---- bulk_f: %f", bulk_f));

    // phi_0
    PetscCall(PetscOptionsScalar("-phi_0", "Porosity at t=0", NULL, phi_0, &phi_0, &set_phi_0));
    PetscCheck((0 < phi_0) && (phi_0 < 1), ratel->comm, PETSC_ERR_SUP, "Neo-Hookean Poroelasticity model requires -phi_0 option in (0, 1)");
    PetscCall(RatelDebug(ratel, "---- phi_0: %f", phi_0));

    // eta_f
    PetscCall(PetscOptionsScalar("-eta_f", "Fluid viscosity", NULL, eta_f, &eta_f, &set_eta_f));
    PetscCheck(eta_f >= 0, ratel->comm, PETSC_ERR_SUP, "Neo-Hookean Poroelasticity model requires non-negative -eta_f option (Pa * s)");
    PetscCall(RatelDebug(ratel, "---- eta_f: %f", eta_f));

    // varkappa_0
    PetscCall(PetscOptionsScalar("-varkappa_0", "Intrinsic Permeability", NULL, varkappa_0, &varkappa_0, &set_varkappa_0));
    PetscCheck(varkappa_0 >= 0, ratel->comm, PETSC_ERR_SUP, "Neo-Hookean Poroelasticity model requires non-negative -varkappa_0 option (m^2)");
    PetscCall(RatelDebug(ratel, "---- varkappa_0: %f", varkappa_0));

    PetscCall(PetscOptionsInt("-sign_pp", "To change the sign in pp block pc", NULL, sign_pp, &sign_pp, NULL));
    PetscCheck(sign_pp >= -1 && sign_pp <= 1, ratel->comm, PETSC_ERR_SUP, "To change the sign in pp block pc -sign_pp option in [-1, 1]");
    PetscCall(RatelDebug(ratel, "---- sign_pp: %d", sign_pp));

    PetscOptionsEnd();  // End of setting model parameters
    PetscCall(PetscFree(cl_prefix));
  }

  // Check for all required options to be set
  PetscCheck(set_lambda_d, ratel->comm, PETSC_ERR_SUP, "-lambda_d option needed");
  PetscCheck(set_mu_d, ratel->comm, PETSC_ERR_SUP, "-mu_d option needed");
  PetscCheck(set_bulk_d, ratel->comm, PETSC_ERR_SUP, "-bulk_d option needed");
  PetscCheck(set_bulk_s, ratel->comm, PETSC_ERR_SUP, "-bulk_s option needed");
  PetscCheck(set_bulk_f, ratel->comm, PETSC_ERR_SUP, "-bulk_f option needed");
  PetscCheck(set_phi_0, ratel->comm, PETSC_ERR_SUP, "-phi_0 option needed");
  PetscCheck(set_eta_f, ratel->comm, PETSC_ERR_SUP, "-eta_f option needed");
  PetscCheck(set_varkappa_0, ratel->comm, PETSC_ERR_SUP, "-varkappa_0 option needed");

  // Create context
  PetscCall(RatelCeedParamsContextCreate_PoroElasticityNeoHookean(material, rho, lambda_d, mu_d, bulk_d, bulk_s, bulk_f, phi_0, eta_f, varkappa_0,
                                                                  sign_pp, ctx));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup Model Parameters Context Neo-Hookean Poroelasticity Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup data for `CeedQFunctionContext` for smoother with Neo-Hookean Poroelasticity parameters.

  Collective across MPI processes.

  @param[in,out]  material  `RatelMaterial` to setup model params context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialParamsSmootherDataSetup_PoroElasticityNeoHookean(RatelMaterial material) {
  Ratel      ratel           = material->ratel;
  PetscBool  set_nu_smoother = PETSC_FALSE;
  PetscBool  set_mu_d        = PETSC_FALSE;
  PetscBool  set_bulk_d      = PETSC_FALSE;
  CeedScalar mu_d = 0.0, bulk_d = 0.0, nu = 0.0, nu_smoother = 0.0;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup Model Parameters Smoother Context Neo-Hookean Poroelasticity"));

  // Get parameter values
  if (material->num_params_smoother_values == 0) {
    char *cl_prefix;

    PetscCall(RatelMaterialGetCLPrefix(material, &cl_prefix));
    PetscOptionsBegin(ratel->comm, cl_prefix, "Neo-Hookean Poroelasticity model parameters for smoother", NULL);

    // mu_d
    PetscCall(PetscOptionsScalar("-mu_d", "Shear Modulus for drained state", NULL, mu_d, &mu_d, &set_mu_d));
    PetscCheck(mu_d >= 0, ratel->comm, PETSC_ERR_SUP, "Neo-Hookean Poroelasticity model requires non-negative -mu_d option (Pa)");
    PetscCall(RatelDebug(ratel, "---- mu_d: %f", mu_d));

    // bulk_d
    PetscCall(PetscOptionsScalar("-bulk_d", "Bulk Modulus for drained state", NULL, bulk_d, &bulk_d, &set_bulk_d));
    PetscCheck(bulk_d >= 0, ratel->comm, PETSC_ERR_SUP, "Neo-Hookean Poroelasticity model requires non-negative -bulk_d option (Pa) ");
    PetscCall(RatelDebug(ratel, "---- bulk_d: %f", bulk_d));

    // Compute nu value
    nu = (3 * bulk_d - 2 * mu_d) / (2 * (3 * bulk_d + mu_d));
    PetscCheck(nu >= 0 && nu < 0.5, ratel->comm, PETSC_ERR_SUP, "Neo-Hookean Poroelasticity model requires Poisson ratio -nu option in [0, 0.5)");
    PetscCall(RatelDebug(ratel, "---- nu: %f", nu));

    PetscCall(PetscOptionsScalar("-nu_smoother", "Poisson's ratio for smoother", NULL, nu_smoother, &nu_smoother, &set_nu_smoother));
    if (set_nu_smoother) PetscCall(RatelDebug(ratel, "---- nu_smoother: %f", nu_smoother));
    else PetscCall(RatelDebug(ratel, "---- No Nu for smoother set"));

    PetscOptionsEnd();  // End of setting model parameters
    PetscCall(PetscFree(cl_prefix));

    // Store smoother values, if needed
    if (set_nu_smoother) {
      const char *label_name     = "nu";
      PetscSizeT  label_name_len = 0;

      material->num_params_smoother_values = 1;
      PetscCall(PetscNew(&material->ctx_params_values));
      PetscCall(PetscNew(&material->ctx_params_smoother_values));
      material->ctx_params_values[0]          = nu;
      material->ctx_params_smoother_values[0] = nu_smoother;
      PetscCall(PetscNew(&material->ctx_params_label_names));
      PetscCall(PetscStrlen(label_name, &label_name_len));
      PetscCall(PetscCalloc1(label_name_len + 1, &material->ctx_params_label_names[0]));
      PetscCall(PetscStrncpy(material->ctx_params_label_names[0], label_name, label_name_len + 1));
    }
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup Model Parameters Smoother Context Neo-Hookean Poroelasticity Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}
