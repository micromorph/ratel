/// @file
/// Setup Neo-Hookean model params context objects

#include <ceed.h>
#include <math.h>
#include <petscsys.h>
#include <ratel-impl.h>
#include <ratel-material.h>
#include <ratel-model.h>
#include <ratel.h>
#include <ratel/models/neo-hookean.h>
#include <stddef.h>
#include <stdio.h>

/// @addtogroup RatelMaterials
/// @{

/// @brief Neo-Hookean model parameters for `CeedQFunctionContext` and viewing
struct RatelModelParameterData_private neo_hookean_param_data_private = {
    .parameters     = {{
                           .name        = "shift v",
                           .description = "shift for U_t",
                           .is_hidden   = PETSC_TRUE,
                           .offset      = offsetof(RatelNeoHookeanElasticityParams, common_parameters[RATEL_COMMON_PARAMETER_SHIFT_V]),
                           .type        = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name        = "shift a",
                           .description = "shift for U_tt",
                           .is_hidden   = PETSC_TRUE,
                           .offset      = offsetof(RatelNeoHookeanElasticityParams, common_parameters[RATEL_COMMON_PARAMETER_SHIFT_A]),
                           .type        = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name            = "rho",
                           .description     = "Density",
                           .units           = "kg/m^3",
                           .restrictions    = "rho >= 0",
                           .default_value.s = 1,
                           .is_required     = PETSC_FALSE,
                           .offset          = offsetof(RatelNeoHookeanElasticityParams, common_parameters[RATEL_COMMON_PARAMETER_RHO]),
                           .type            = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "nu",
                           .description  = "Poisson's ratio",
                           .restrictions = "0 <= nu < 0.5",
                           .is_required  = PETSC_TRUE,
                           .offset       = offsetof(RatelNeoHookeanElasticityParams, nu),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "E",
                           .description  = "Young's Modulus",
                           .units        = "Pa",
                           .restrictions = "E >= 0",
                           .is_required  = PETSC_TRUE,
                           .offset       = offsetof(RatelNeoHookeanElasticityParams, E),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "mu",
                           .description  = "Shear Modulus: E / (2 (1 + nu))",
                           .units        = "Pa",
                           .restrictions = "mu >= 0",
                           .is_required  = PETSC_FALSE,
                           .offset       = offsetof(RatelNeoHookeanElasticityParams, mu),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "lambda",
                           .description  = "First Lame parameter: 2 mu nu / (1 - 2 nu)",
                           .units        = "Pa",
                           .restrictions = "lambda >= 0",
                           .is_required  = PETSC_FALSE,
                           .offset       = offsetof(RatelNeoHookeanElasticityParams, lambda),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "bulk",
                           .description  = "Bulk Modulus: 2 mu (1 + nu) / (3 (1 - 2 nu))",
                           .units        = "Pa",
                           .restrictions = "bulk >= 0",
                           .is_required  = PETSC_FALSE,
                           .offset       = offsetof(RatelNeoHookeanElasticityParams, bulk),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "two_mu",
                           .description  = "Shear Modulus multiplied by 2",
                           .units        = "Pa",
                           .restrictions = "mu >= 0",
                           .is_required  = PETSC_FALSE,
                           .offset       = offsetof(RatelNeoHookeanElasticityParams, two_mu),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }},
    .num_parameters = 9,
    .reference      = "ISBN: 0-471-82319-8"
};
RatelModelParameterData neo_hookean_param_data = &neo_hookean_param_data_private;

/**
  @brief Set `CeedQFunctionContext` data and register fields for Neo-Hookean parameters.

  Collective across MPI processes.

  @param[in]   material      `RatelMaterial` to setup model params context
  @param[in]   nu            Poisson's ratio
  @param[in]   E             Young's Modulus
  @param[in]   rho           Density for scaled mass matrix
  @param[out]  ctx           `CeedQFunctionContext` for Neo-Hookean parameters

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelCeedParamsContextCreate_NeoHookean(RatelMaterial material, CeedScalar nu, CeedScalar E, CeedScalar rho,
                                                              CeedQFunctionContext *ctx) {
  Ratel                            ratel      = material->ratel;
  RatelModelParameterData          param_data = material->model_data->param_data;
  RatelNeoHookeanElasticityParams *params;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Create Parameters Context Neo-Hookean"));

  // Create context data with correct scaling
  PetscCall(PetscNew(&params));
  params->nu                                            = nu;
  params->E                                             = E * ratel->material_units->Pascal;
  params->common_parameters[RATEL_COMMON_PARAMETER_RHO] = rho * (ratel->material_units->kilogram) / pow(ratel->material_units->meter, 3);
  params->mu                                            = params->E / (2 * (1 + params->nu));
  params->two_mu                                        = 2 * params->mu;
  params->lambda                                        = params->two_mu * params->nu / (1 - 2 * params->nu);
  params->bulk                                          = params->two_mu * (1 + params->nu) / (3. * (1 - 2 * params->nu));

  // Create context object
  RatelCallCeed(ratel, CeedQFunctionContextCreate(ratel->ceed, ctx));
  RatelCallCeed(ratel, CeedQFunctionContextSetData(*ctx, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(*params), params));
  PetscCall(PetscFree(params));

  // Register parameters
  PetscCall(RatelModelParameterDataRegisterContextFields(ratel, param_data, *ctx));

  // Debugging output
  if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedQFunctionContextView(*ctx, stdout));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Create Parameters Context Neo-Hookean Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Build `CeedQFunctionContext` with Neo-Hookean parameters.

  Collective across MPI processes.

  @param[in]   material  `RatelMaterial` to setup model params context
  @param[out]  ctx       `CeedQFunctionContext` for Neo-Hookean parameters

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialParamsContextCreate_NeoHookean(RatelMaterial material, CeedQFunctionContext *ctx) {
  Ratel      ratel      = material->ratel;
  PetscBool  set_nu     = PETSC_FALSE;
  PetscBool  set_Youngs = PETSC_FALSE;
  CeedScalar nu = 0.0, E = 0.0, rho;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup Model Parameters Context Neo-Hookean"));

  // Set model parameter data
  material->model_data->param_data = neo_hookean_param_data;

  // Get default values;
  PetscCall(RatelModelParameterDataGetDefaultValue_Scalar(ratel, neo_hookean_param_data, "rho", &rho));

  // Get parameter values
  {
    char *cl_prefix;

    PetscCall(RatelMaterialGetCLPrefix(material, &cl_prefix));
    PetscOptionsBegin(ratel->comm, cl_prefix, "Neo-Hookean model parameters", NULL);

    PetscCall(PetscOptionsScalar("-nu", "Poisson's ratio", NULL, nu, &nu, &set_nu));
    PetscCheck(nu >= 0 && nu < 0.5, ratel->comm, PETSC_ERR_SUP, "Neo-Hookean model requires Poisson ratio -nu option in [0, 0.5)");
    PetscCall(RatelDebug(ratel, "---- nu: %f", nu));

    PetscCall(PetscOptionsScalar("-E", "Young's Modulus", NULL, E, &E, &set_Youngs));
    PetscCheck(E >= 0, ratel->comm, PETSC_ERR_SUP, "Neo-Hookean model requires non-negative Young's Modulus -E option (Pa)");
    PetscCall(RatelDebug(ratel, "---- E: %f", E));

    PetscCall(PetscOptionsScalar("-rho", "Density for scaled mass matrix", NULL, rho, &rho, NULL));
    PetscCheck(rho >= 0.0, ratel->comm, PETSC_ERR_SUP, "Density must be a positive value");
    PetscCall(RatelDebug(ratel, "---- rho: %f", rho));

    PetscOptionsEnd();  // End of setting model parameters
    PetscCall(PetscFree(cl_prefix));
  }

  // Check for all required options to be set
  PetscCheck(set_nu, ratel->comm, PETSC_ERR_SUP, "-nu option needed");
  PetscCheck(set_Youngs, ratel->comm, PETSC_ERR_SUP, "-E option needed");

  // Create context
  PetscCall(RatelCeedParamsContextCreate_NeoHookean(material, nu, E, rho, ctx));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup Model Parameters Context Neo-Hookean Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup data for `CeedQFunctionContext` for smoother with Neo-Hookean parameters.

  Collective across MPI processes.

  @param[in,out]  material  `RatelMaterial` to setup model params context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialParamsSmootherDataSetup_NeoHookean(RatelMaterial material) {
  Ratel      ratel           = material->ratel;
  PetscBool  set_nu_smoother = PETSC_FALSE;
  CeedScalar nu = 0.0, nu_smoother = 0.0;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup Model Parameters Smoother Context Neo-Hookean"));

  // Get parameter values
  if (material->num_params_smoother_values == 0) {
    char *cl_prefix;

    PetscCall(RatelMaterialGetCLPrefix(material, &cl_prefix));
    PetscOptionsBegin(ratel->comm, cl_prefix, "Neo-Hookean model parameters for smoother", NULL);

    PetscCall(PetscOptionsScalar("-nu", "Poisson's ratio", NULL, nu, &nu, NULL));
    PetscCheck(nu >= 0 && nu < 0.5, ratel->comm, PETSC_ERR_SUP, "Neo-Hookean model requires Poisson ratio -nu option in [0, 0.5)");
    PetscCall(RatelDebug(ratel, "---- nu: %f", nu));

    PetscCall(PetscOptionsScalar("-nu_smoother", "Poisson's ratio for smoother", NULL, nu_smoother, &nu_smoother, &set_nu_smoother));
    if (set_nu_smoother) PetscCall(RatelDebug(ratel, "---- nu_smoother: %f", nu_smoother));
    else PetscCall(RatelDebug(ratel, "---- No Nu for smoother set"));

    PetscOptionsEnd();  // End of setting model parameters
    PetscCall(PetscFree(cl_prefix));

    // Store smoother values, if needed
    if (set_nu_smoother) {
      const char *label_name     = "nu";
      PetscSizeT  label_name_len = 0;

      material->num_params_smoother_values = 1;
      PetscCall(PetscNew(&material->ctx_params_values));
      PetscCall(PetscNew(&material->ctx_params_smoother_values));
      material->ctx_params_values[0]          = nu;
      material->ctx_params_smoother_values[0] = nu_smoother;
      PetscCall(PetscNew(&material->ctx_params_label_names));
      PetscCall(PetscStrlen(label_name, &label_name_len));
      PetscCall(PetscCalloc1(label_name_len + 1, &material->ctx_params_label_names[0]));
      PetscCall(PetscStrncpy(material->ctx_params_label_names[0], label_name, label_name_len + 1));
    }
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup Model Parameters Smoother Context Neo-Hookean Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}
