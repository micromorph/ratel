/// @file
//// Ratel MPM material model DM and operator setup

#include <ceed-evaluator.h>
#include <ceed.h>
#include <ceed/backend.h>
#include <mat-ceed.h>
#include <math.h>
#include <petsc-ceed-utils.h>
#include <petscdm.h>
#include <petscdmplex.h>
#include <ratel-dm.h>
#include <ratel-fem.h>
#include <ratel-material.h>
#include <ratel-mpm.h>
#include <ratel-solver.h>
#include <ratel.h>
#include <ratel/qfunctions/forcing/body.h>
#include <ratel/qfunctions/forcing/mpm-body.h>
#include <ratel/qfunctions/geometry/volumetric-mpm.h>
#include <ratel/qfunctions/geometry/volumetric-symmetric-mpm.h>
#include <ratel/qfunctions/mass.h>
#include <ratel/qfunctions/mpm-migrate.h>
#include <ratel/qfunctions/scale-lumped-dual-diagnostic-terms.h>

/// @addtogroup RatelMaterials
/// @{

const char DMSwarmPICField_volume[] = "volume";

/**
  @brief Create a `RatelMaterial` object for a material point method (MPM) model.

  Collective across MPI processes.

  @param[in]   ratel     `Ratel` context
  @param[out]  material  `RatelMaterial` context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialCreate_MPM(Ratel ratel, RatelMaterial material) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material MPM Create"));

  // -- Base material
  PetscCall(RatelMaterialCreate_FEM(ratel, material));

  // -- Geometry
  material->SetupVolumeQData = RatelMaterialSetupVolumeQData_MPM;

  // -- Residual and Jacobian operators
  material->SetupResidualSuboperators     = RatelMaterialSetupResidualSuboperators_MPM;
  material->SetupJacobianSuboperator      = RatelMaterialSetupJacobianSuboperator_MPM;
  material->SetupJacobianBlockSuboperator = RatelMaterialSetupJacobianBlockSuboperator_MPM;

  // -- Output operators
  material->SetupMMSErrorSuboperator     = RatelMaterialSetupMMSErrorSuboperator_MPM;
  material->SetupDiagnosticSuboperators  = RatelMaterialSetupDiagnosticSuboperators_MPM;
  material->SetupStrainEnergySuboperator = RatelMaterialSetupStrainEnergySuboperator_MPM;

  // -- MPM data
  material->mpm = NULL;

  // -- Set QData Qfunction
  if (!material->model_data->setup_q_data_volume_mpm_loc) {
    if (material->model_data->q_data_volume_size == Q_DATA_VOLUMETRIC_GEOMETRY_MPM_SIZE) {
      // Use normal QData
      material->model_data->setup_q_data_volume_mpm     = SetupVolumeGeometryMPM;
      material->model_data->setup_q_data_volume_mpm_loc = RatelQFunctionRelativePath(SetupVolumeGeometryMPM_loc);
    } else if (material->model_data->q_data_volume_size == Q_DATA_VOLUMETRIC_SYMMETRIC_GEOMETRY_MPM_SIZE) {
      // Use symmetric QData
      material->model_data->setup_q_data_volume_mpm     = SetupVolumeGeometrySymmetricMPM;
      material->model_data->setup_q_data_volume_mpm_loc = RatelQFunctionRelativePath(SetupVolumeGeometrySymmetricMPM_loc);
    } else {
      // LCOV_EXCL_START
      PetscCall(PetscPrintf(ratel->comm, "QData size %d not supported for MPM\n", material->model_data->q_data_volume_size));
      PetscFunctionReturn(PETSC_ERR_ARG_WRONG);
      // LCOV_EXCL_STOP
    }
  }
  // -- Set update volume QFunction
  if (!material->model_data->update_volume_mpm_loc) {
    material->model_data->update_volume_mpm     = UpdateVolume_MPM;
    material->model_data->update_volume_mpm_loc = RatelQFunctionRelativePath(UpdateVolume_MPM_loc);
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Create MPM Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Read options for a material point method (MPM) model from the command line.

  Collective across MPI processes.

  @note The caller is responsible for destroying `options` with `PetscFree()`.

  @param[in]   ratel    `Ratel` context
  @param[out]  options  `RatelMPMOptions` context to initialize

  @sa `RatelDMSetup_MPM()`, `RatelDMInitalizePointLocations_MPM()`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMPMOptionsCreateFromOptions(Ratel ratel, RatelMPMOptions *options) {
  PetscInt dim, num_cells;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel MPM Options Create From Options"));

  {
    PetscInt c_start, c_end, num_cells_local;
    DMType   dm_type;
    DM       dm_mesh;

    PetscCheck(ratel->dm_solution, ratel->comm, PETSC_ERR_ARG_NULL, "DM for solution must be set before calling RatelMPMOptionsCreateFromOptions");
    PetscCall(DMGetType(ratel->dm_solution, &dm_type));
    {
      PetscBool is_swarm = PETSC_FALSE;

      PetscCall(PetscStrcmp(dm_type, DMSWARM, &is_swarm));
      PetscCheck(is_swarm, ratel->comm, PETSC_ERR_SUP, "MPM only supported with DMSwarm solution DM");
    }

    // Compute total number of cells
    PetscCall(DMSwarmGetCellDM(ratel->dm_solution, &dm_mesh));
    PetscCall(DMPlexGetHeightStratum(dm_mesh, 0, &c_start, &c_end));
    num_cells_local = c_end - c_start;
    PetscCall(MPIU_Allreduce(&num_cells_local, &num_cells, 1, MPIU_INT, MPI_SUM, ratel->comm));

    // Get dimension
    PetscCall(DMGetDimension(dm_mesh, &dim));
  }

  PetscCall(PetscNew(options));
  PetscOptionsBegin(ratel->comm, NULL, "Ratel MPM Options", "Ratel");
  // Get point location type
  (*options)->point_location_type = RATEL_POINT_LOCATION_UNIFORM;  // Default
  PetscCall(PetscOptionsEnum("-mpm_point_location_type", "Point location type", NULL, RatelPointLocationTypesCL,
                             (PetscEnum)(*options)->point_location_type, (PetscEnum *)&(*options)->point_location_type, NULL));  // NOLINT

  // Get number of material points
  {
    PetscBool user_set_num_points_per_cell = PETSC_FALSE;
    PetscInt  num_points_per_cell_1d;

    (*options)->num_points_per_cell = 64;
    PetscCall(PetscOptionsInt("-mpm_num_points_per_cell", "Total number of material points in each cell, usually rounded to nearest cube", NULL,
                              (*options)->num_points_per_cell, &(*options)->num_points_per_cell, &user_set_num_points_per_cell));

    if (!user_set_num_points_per_cell) {
      (*options)->num_points = 1728;
      PetscCall(PetscOptionsInt("-mpm_num_points", "Total number of material points, divided uniformly over cells", NULL, (*options)->num_points,
                                &(*options)->num_points, NULL));
      (*options)->num_points_per_cell = PetscCeilInt((*options)->num_points, num_cells);
    }
    num_points_per_cell_1d = round(PetscCbrtReal((*options)->num_points_per_cell * 1.0));

    (*options)->num_points_per_cell = 1;
    for (PetscInt i = 0; i < dim; i++) (*options)->num_points_per_cell *= num_points_per_cell_1d;
  }
  (*options)->num_points = (*options)->num_points_per_cell * num_cells;
  PetscOptionsEnd();

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel MPM Options Create From Options Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get the `RatelMPMContext` for a `RatelMaterial`, creating it if necessary.

  Collective across MPI processes.

  @note @a `mpm` is a borrowed reference and should not be destroyed by the caller.

  @param[in]   material  `RatelMaterial` context
  @param[out]  mpm       `RatelMPMContext` context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetMPMContext(RatelMaterial material, RatelMPMContext *mpm) {
  Ratel ratel = material->ratel;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel MPM Context Create"));

  if (!material->mpm) {
    DM                  dm_swarm = ratel->dm_solution, dm_mesh;
    const char         *volume_label_name;
    DMLabel             domain_label;
    PetscInt           *domain_values;
    CeedElemRestriction restriction_x_points;
    CeedVector          x_ref_points;

    // DM information
    PetscCall(RatelGetSolutionMeshDM(ratel, &dm_mesh));
    PetscCall(RatelMaterialGetVolumeLabelName(material, &volume_label_name));
    PetscCall(DMGetLabel(dm_mesh, volume_label_name, &domain_label));
    PetscCall(RatelMaterialGetVolumeLabelValues(material, NULL, &domain_values));

    // Create points element restriction
    PetscCall(RatelDMSwarmCeedElemRestrictionPointsCreate(ratel, dm_swarm, domain_label, domain_values[0], &x_ref_points, &restriction_x_points));

    // Create MPM context
    PetscCall(RatelMPMContextCreate(ratel, x_ref_points, restriction_x_points, &material->mpm));

    // Cleanup
    PetscCall(DMDestroy(&dm_mesh));
    RatelCallCeed(ratel, CeedVectorDestroy(&x_ref_points));
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_x_points));
  }
  *mpm = material->mpm;

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel MPM Context Create Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Create a `RatelMPMContext` object.

  Collective across MPI processes.

  @param[in]   ratel                 `Ratel` context
  @param[in]   x_ref_points          `CeedVector` holding reference coordinates
  @param[in]   restriction_x_points  `CeedElemRestriction` for points
  @param[out]  mpm                   Created `RatelMPMContext` context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMPMContextCreate(Ratel ratel, CeedVector x_ref_points, CeedElemRestriction restriction_x_points, RatelMPMContext *mpm) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel MPM Context Create"));

  PetscCall(PetscNew(mpm));
  (*mpm)->ratel = ratel;
  RatelCallCeed(ratel, CeedVectorReferenceCopy(x_ref_points, &(*mpm)->x_ref_points));
  RatelCallCeed(ratel, CeedElemRestrictionReferenceCopy(restriction_x_points, &(*mpm)->restriction_x_points));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel MPM Context Create Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Destroy a `RatelMPMContext` object.

  Collective across MPI processes.

  @param[in,out]  mpm  `RatelMPMContext` context to destroy

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMPMContextDestroy(RatelMPMContext *mpm) {
  Ratel ratel;

  PetscFunctionBeginUser;
  if (!*mpm) PetscFunctionReturn(PETSC_SUCCESS);
  ratel = (*mpm)->ratel;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel MPM Context Destroy"));

  RatelCallCeed(ratel, CeedVectorDestroy(&(*mpm)->x_ref_points));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&(*mpm)->restriction_x_points));
  RatelCallCeed(ratel, CeedVectorDestroy(&(*mpm)->q_data_points));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&(*mpm)->restriction_q_data_points));
  PetscCall(PetscFree(*mpm));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel MPM Context Destroy Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get the reference coordinates for points in a `RatelMPMContext`.

  Not collective across MPI processes.

  @param[in]   mpm           `RatelMPMContext` context
  @param[out]  x_ref_points  `CeedVector` holding reference coordinates

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMPMContextGetPoints(RatelMPMContext mpm, CeedVector *x_ref_points) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(mpm->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel MPM Context Get Points"));

  *x_ref_points = NULL;
  RatelCallCeed(mpm->ratel, CeedVectorReferenceCopy(mpm->x_ref_points, x_ref_points));

  PetscCall(RatelDebug256(mpm->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel MPM Context Get Points Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get the `CeedElemRestriction` for points in a `RatelMPMContext`.

  Not collective across MPI processes.

  @param[in]   mpm                   `RatelMPMContext` context
  @param[out]  restriction_x_points  `CeedElemRestriction` for points

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMPMContextGetElemRestrictionPoints(RatelMPMContext mpm, CeedElemRestriction *restriction_x_points) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(mpm->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel MPM Context Get Elem Restriction Points"));

  *restriction_x_points = NULL;
  RatelCallCeed(mpm->ratel, CeedElemRestrictionReferenceCopy(mpm->restriction_x_points, restriction_x_points));

  PetscCall(RatelDebug256(mpm->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel MPM Context Get Elem Restriction Points Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get the number of points on this process

  Not collective across MPI processes.

  @param[in]   mpm               `RatelMPMContext` context
  @param[out]  num_points_local  Number of points on this process

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMPMContextGetNumPointsLocal(RatelMPMContext mpm, CeedSize *num_points_local) {
  CeedSize local_size;
  CeedInt  num_comp;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(mpm->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel MPM Context Get Num Points Local"));

  RatelCallCeed(mpm->ratel, CeedElemRestrictionGetLVectorSize(mpm->restriction_x_points, &local_size));
  RatelCallCeed(mpm->ratel, CeedElemRestrictionGetNumComponents(mpm->restriction_x_points, &num_comp));
  *num_points_local = (PetscInt)local_size / num_comp;

  PetscCall(RatelDebug256(mpm->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel MPM Context Get Num Points Local Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Create a `CeedElemRestriction` for a field at the points in a `RatelMPMContext`.

  Collective across MPI processes.

  @param[in]   mpm          `RatelMPMContext` context
  @param[in]   num_comp     Number of components
  @param[out]  restriction  `CeedElemRestriction` for QData

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMPMContextCeedElemRestrictionCreateAtPoints(RatelMPMContext mpm, CeedInt num_comp, CeedElemRestriction *restriction) {
  Ratel          ratel = mpm->ratel;
  Ceed           ceed  = ratel->ceed;
  CeedSize       num_points_local;
  CeedInt        num_elem, num_points;
  const CeedInt *offsets;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel MPM Context Create Elem Restriction At Points"));

  // Get number of elements, number of points, and offsets
  RatelCallCeed(ratel, CeedElemRestrictionGetNumElements(mpm->restriction_x_points, &num_elem));
  RatelCallCeed(ratel, CeedElemRestrictionGetNumPoints(mpm->restriction_x_points, &num_points));
  RatelCallCeed(ratel, CeedElemRestrictionGetOffsets(mpm->restriction_x_points, CEED_MEM_HOST, &offsets));
  PetscCall(RatelMPMContextGetNumPointsLocal(mpm, &num_points_local));

  // Create restriction
  RatelCallCeed(ratel, CeedElemRestrictionCreateAtPoints(ceed, num_elem, num_points, num_comp, num_points_local * (CeedSize)num_comp, CEED_MEM_HOST,
                                                         CEED_COPY_VALUES, offsets, restriction));

  // Cleanup
  RatelCallCeed(ratel, CeedElemRestrictionRestoreOffsets(mpm->restriction_x_points, &offsets));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel MPM Context Create Elem Restriction At Points Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Call `CeedOperatorAtPointsSetPoints()` for a `CeedOperator` using a `RatelMPMContext`.

  Not collective across MPI processes.

  @param[in]  mpm  `RatelMPMContext` context
  @param[in]  op   `CeedOperator` to set points for

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMPMContextCeedOperatorSetPoints(RatelMPMContext mpm, CeedOperator op) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(mpm->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel MPM Context Operator Set Points"));

  RatelCallCeed(mpm->ratel, CeedOperatorAtPointsSetPoints(op, mpm->restriction_x_points, mpm->x_ref_points));

  PetscCall(RatelDebug256(mpm->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel MPM Context Operator Set Points Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Interpolate mesh field to material points, updating the `DMSwarm` with the result.

  Collective across MPI processes.

  @param[in]  material          `RatelMaterial` context
  @param[in]  field_index       Index of field to interpolate
  @param[in]  op_mesh_to_swarm  Composite `CeedOperator` for mesh to swarm interpolation

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupMeshToSwarmSuboperators_MPM(RatelMaterial material, CeedInt field_index, CeedOperator op_mesh_to_swarm) {
  Ratel                ratel = material->ratel;
  RatelMPMContext      mpm;
  DM                   dm_mesh;
  CeedInt              num_active_fields;
  const CeedInt       *active_field_sizes;
  PetscInt             dim;
  const char          *volume_label_name, **active_field_names;
  DMLabel              domain_label;
  PetscInt            *domain_values;
  CeedElemRestriction *elem_restr_u_mesh = NULL, elem_restr_u_points;
  CeedBasis           *bases_u           = NULL;
  CeedOperator         sub_op_mesh_to_points;
  CeedQFunction        qf_mesh_to_points;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Mesh To Swarm MPM"));

  // DM information
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_mesh));
  PetscCall(DMGetDimension(dm_mesh, &dim));
  PetscCall(RatelMaterialGetVolumeLabelName(material, &volume_label_name));
  PetscCall(DMGetLabel(dm_mesh, volume_label_name, &domain_label));
  PetscCall(RatelMaterialGetVolumeLabelValues(material, NULL, &domain_values));

  // Active fields
  PetscCall(RatelMaterialGetActiveFieldSizes(material, &num_active_fields, &active_field_sizes));
  PetscCall(RatelMaterialGetActiveFieldNames(material, &active_field_names, NULL));

  // Points restriction and reference coordinates
  PetscCall(RatelMaterialGetMPMContext(material, &mpm));

  // Mesh to points interpolation operator for field
  {
    CeedOperator op_residual_u;

    PetscCall(CeedEvaluatorGetCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
    PetscCall(RatelMaterialGetSolutionData(material, op_residual_u, &num_active_fields, &elem_restr_u_mesh, &bases_u));
    PetscCall(CeedEvaluatorRestoreCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
  }
  // -- Create restriction
  PetscCall(RatelMPMContextCeedElemRestrictionCreateAtPoints(mpm, active_field_sizes[field_index], &elem_restr_u_points));

  // -- Create operator
  RatelCallCeed(ratel,
                CeedQFunctionCreateIdentity(ratel->ceed, active_field_sizes[field_index], CEED_EVAL_INTERP, CEED_EVAL_NONE, &qf_mesh_to_points));
  RatelCallCeed(ratel, CeedOperatorCreateAtPoints(ratel->ceed, qf_mesh_to_points, NULL, NULL, &sub_op_mesh_to_points));
  RatelCallCeed(ratel,
                CeedOperatorSetField(sub_op_mesh_to_points, "input", elem_restr_u_mesh[field_index], bases_u[field_index], CEED_VECTOR_ACTIVE));
  RatelCallCeed(ratel, CeedOperatorSetField(sub_op_mesh_to_points, "output", elem_restr_u_points, CEED_BASIS_NONE, CEED_VECTOR_ACTIVE));
  PetscCall(RatelMPMContextCeedOperatorSetPoints(mpm, sub_op_mesh_to_points));

  // -- Add to composite operator
  if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_mesh_to_points, stdout));
  RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_mesh_to_swarm, sub_op_mesh_to_points));

  // Cleanup
  for (PetscInt i = 0; i < num_active_fields; i++) {
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&elem_restr_u_mesh[i]));
    RatelCallCeed(ratel, CeedBasisDestroy(&bases_u[i]));
  }
  PetscCall(PetscFree(elem_restr_u_mesh));
  PetscCall(PetscFree(bases_u));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&elem_restr_u_points));
  RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_mesh_to_points));
  RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_mesh_to_points));
  PetscCall(DMDestroy(&dm_mesh));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Mesh To Swarm MPM Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup the suboperator for migrating points in a `RatelMaterial`.

  Collective across MPI processes.

  @param[in]  material           `RatelMaterial` context
  @param[in]  op_migrate_points  Composite `CeedOperator` for migrating points

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupMigratePointsSuboperator(RatelMaterial material, CeedOperator op_migrate_points) {
  Ratel               ratel = material->ratel;
  RatelMPMContext     mpm;
  CeedElemRestriction elem_restr_u_points, elem_restr_x_points;
  CeedInt             num_active_fields;
  const CeedInt      *active_field_sizes;
  const char        **active_field_names;
  PetscInt            dim;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Setup Material Migrate Points Suboperator"));

  // Get DM info
  PetscCall(DMGetDimension(ratel->dm_solution, &dim));

  // Get MPM context
  PetscCall(RatelMaterialGetMPMContext(material, &mpm));

  // Active fields
  PetscCall(RatelMaterialGetActiveFieldSizes(material, &num_active_fields, &active_field_sizes));
  PetscCall(RatelMaterialGetActiveFieldNames(material, &active_field_names, NULL));

  // -- Create restriction
  PetscCall(RatelMPMContextCeedElemRestrictionCreateAtPoints(mpm, active_field_sizes[0], &elem_restr_u_points));
  PetscCall(RatelMPMContextCeedElemRestrictionCreateAtPoints(mpm, dim, &elem_restr_x_points));
  // -- Add to composite operator
  {
    CeedQFunction qf_migrate_points;
    CeedOperator  sub_op_migrate_points;

    RatelCallCeed(ratel, CeedQFunctionCreateInterior(ratel->ceed, 1, RatelMigratePoints, RatelQFunctionRelativePath(RatelMigratePoints_loc),
                                                     &qf_migrate_points));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_migrate_points, "Delta u", active_field_sizes[0], CEED_EVAL_NONE));
    RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_migrate_points, "x", dim, CEED_EVAL_NONE));

    // Create operator
    RatelCallCeed(ratel, CeedOperatorCreateAtPoints(ratel->ceed, qf_migrate_points, NULL, NULL, &sub_op_migrate_points));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_migrate_points, "Delta u", elem_restr_u_points, CEED_BASIS_NONE, CEED_VECTOR_ACTIVE));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_migrate_points, "x", elem_restr_x_points, CEED_BASIS_NONE, CEED_VECTOR_ACTIVE));
    PetscCall(RatelMPMContextCeedOperatorSetPoints(mpm, sub_op_migrate_points));
    // Add to composite operator
    RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_migrate_points, sub_op_migrate_points));
    if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_migrate_points, stdout));

    // Cleanup
    RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_migrate_points));
    RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_migrate_points));
  }

  // Cleanup
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&elem_restr_u_points));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&elem_restr_x_points));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Migrate Points Suboperator Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Interpolate mesh fields to material points, updating the `DMSwarm` with the result.

  Collective across MPI processes.

  @param[in]  ratel            `Ratel` context
  @param[in]  delta_U          Global mesh solution vector
  @param[in]  time             Current time
  @param[in]  displace_coords  Boolean flag to point displace coordinates

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMPMMeshToSwarm(Ratel ratel, Vec delta_U, PetscReal time, PetscBool displace_coords) {
  DM       dm_swarm = ratel->dm_solution, dm_mesh;
  PetscInt dim;

  const char   **active_field_names = NULL;
  CeedInt        num_active_fields;
  const CeedInt *active_field_sizes = NULL;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel MPM Mesh To Swarm"));

  // DM information
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_mesh));
  PetscCall(DMGetDimension(dm_mesh, &dim));

  // Get field names
  PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[0], &num_active_fields, &active_field_sizes));
  PetscCall(RatelMaterialGetActiveFieldNames(ratel->materials[0], &active_field_names, NULL));

  // Create composite operator
  for (PetscInt i = 0; i < num_active_fields; i++) {
    CeedOperator op_mesh_to_swarm;
    Vec          delta_U_points_loc;
    const char  *field_name_u = active_field_names[i];
    CeedVector   delta_u_mesh, delta_u_points;
    Vec          delta_U_loc;
    PetscMemType U_mem_type_in, U_mem_type_out;
    PetscInt     size;

    RatelCallCeed(ratel, CeedCompositeOperatorCreate(ratel->ceed, &op_mesh_to_swarm));
    {
      char op_name[256];
      PetscCall(PetscSNPrintf(op_name, sizeof(op_name), "mesh to swarm, field %" PetscInt_FMT " [%s]", i, field_name_u));
      RatelCallCeed(ratel, CeedOperatorSetName(op_mesh_to_swarm, op_name));
    }

    // Create output vector
    PetscCall(DMSwarmVectorDefineField(dm_swarm, field_name_u));
    PetscCall(DMCreateLocalVector(dm_swarm, &delta_U_points_loc));
    PetscCall(VecZeroEntries(delta_U_points_loc));

    for (PetscInt m = 0; m < ratel->num_materials; m++) {
      PetscCall(RatelMaterialSetupMeshToSwarmSuboperators_MPM(ratel->materials[m], i, op_mesh_to_swarm));
    }

    // Get input vector
    PetscCall(DMGetLocalVector(dm_mesh, &delta_U_loc));
    PetscCall(DMGlobalToLocal(dm_mesh, delta_U, INSERT_VALUES, delta_U_loc));
    PetscCall(CeedEvaluatorGetLocalCeedVectors(ratel->evaluator_residual_u, &delta_u_mesh, NULL, NULL));

    // Create output vector
    PetscCall(VecGetLocalSize(delta_U_points_loc, &size));
    RatelCallCeed(ratel, CeedVectorCreate(ratel->ceed, size, &delta_u_points));

    PetscCall(DMPlexInsertBoundaryValues(dm_mesh, PETSC_TRUE, delta_U_loc, time, NULL, NULL, NULL));
    PetscCall(VecReadPetscToCeed(delta_U_loc, &U_mem_type_in, delta_u_mesh));
    PetscCall(VecPetscToCeed(delta_U_points_loc, &U_mem_type_out, delta_u_points));

    // Apply composite operator
    RatelCallCeed(ratel, CeedOperatorApply(op_mesh_to_swarm, delta_u_mesh, delta_u_points, CEED_REQUEST_IMMEDIATE));

    // Cleanup
    PetscCall(VecCeedToPetsc(delta_u_points, U_mem_type_out, delta_U_points_loc));
    PetscCall(VecReadCeedToPetsc(delta_u_mesh, U_mem_type_in, delta_U_loc));
    PetscCall(DMRestoreLocalVector(dm_mesh, &delta_U_loc));
    PetscCall(CeedEvaluatorRestoreLocalCeedVectors(ratel->evaluator_residual_u, &delta_u_mesh, NULL, NULL));

    // Update point coordinates and displacement
    if (displace_coords) {
      Vec U_loc_points;

      PetscCall(DMSwarmCreateLocalVectorFromField(dm_swarm, field_name_u, &U_loc_points));
      if (i == 0 && active_field_sizes[i] != dim) {
        // Special logic for 4 component damage models -- only displacement is incremental
        // Thus, zero out the other components before adding
        for (PetscInt d = dim; d < active_field_sizes[i]; d++) {
          PetscCall(VecStrideScale(U_loc_points, d, 0.0));
        }
      }
      PetscCall(VecAXPY(U_loc_points, 1.0, delta_U_points_loc));
      PetscCall(DMSwarmDestroyLocalVectorFromField(dm_swarm, field_name_u, &U_loc_points));

      // First field should be displacement (or displacement+damage)
      // Use this field to migrate point positions
      if (i == 0) {
        const char  **field_coords;
        PetscMemType  X_mem_type;
        Vec           X_loc_points;
        DMSwarmCellDM dm_cell;
        CeedVector    x_points;
        CeedOperator  op_migrate_points;

        RatelCallCeed(ratel, CeedCompositeOperatorCreate(ratel->ceed, &op_migrate_points));
        RatelCallCeed(ratel, CeedOperatorSetName(op_migrate_points, "migrate points"));
        for (PetscInt m = 0; m < ratel->num_materials; m++) {
          PetscCall(RatelMaterialSetupMigratePointsSuboperator(ratel->materials[m], op_migrate_points));
        }

        // Get input vector
        PetscCall(VecReadPetscToCeed(delta_U_points_loc, &U_mem_type_out, delta_u_points));

        // Get coords field
        PetscCall(DMSwarmGetCellDMActive(dm_swarm, &dm_cell));
        PetscCall(DMSwarmCellDMGetCoordinateFields(dm_cell, NULL, &field_coords));

        // Create output vector
        PetscCall(DMSwarmCreateLocalVectorFromField(dm_swarm, field_coords[0], &X_loc_points));
        PetscCall(VecGetLocalSize(X_loc_points, &size));
        RatelCallCeed(ratel, CeedVectorCreate(ratel->ceed, size, &x_points));
        PetscCall(VecPetscToCeed(X_loc_points, &X_mem_type, x_points));

        // Apply composite operator
        PetscCall(CeedOperatorApplyAdd(op_migrate_points, delta_u_points, x_points, CEED_REQUEST_IMMEDIATE));

        // Cleanup
        PetscCall(VecReadCeedToPetsc(delta_u_points, U_mem_type_out, delta_U_points_loc));
        PetscCall(VecCeedToPetsc(x_points, X_mem_type, X_loc_points));
        RatelCallCeed(ratel, CeedVectorDestroy(&x_points));
        PetscCall(DMSwarmDestroyLocalVectorFromField(dm_swarm, field_coords[0], &X_loc_points));
        RatelCallCeed(ratel, CeedOperatorDestroy(&op_migrate_points));
      }
    }
    // Cleanup
    RatelCallCeed(ratel, CeedVectorDestroy(&delta_u_points));
    PetscCall(VecDestroy(&delta_U_points_loc));
    RatelCallCeed(ratel, CeedOperatorDestroy(&op_mesh_to_swarm));
  }
  // Cleanup
  PetscCall(DMDestroy(&dm_mesh));
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel MPM Mesh To Swarm Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Add material suboperators for swarm-to-mesh projection.

  Collective across MPI processes.

  @param[in]     material          `RatelMaterial` context
  @param[inout]  op_swarm_to_mesh  Composite `CeedOperator` for swarm to mesh projection
  @param[inout]  op_mass           Composite `CeedOperator` for mass matrix

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupSwarmToMeshSuboperators_MPM(RatelMaterial material, CeedOperator op_swarm_to_mesh, CeedOperator op_mass) {
  Ratel               ratel = material->ratel;
  Ceed                ceed  = ratel->ceed;
  RatelMPMContext     mpm;
  RatelModelData      model_data = material->model_data;
  DM                  dm_swarm   = ratel->dm_solution, dm_mesh;
  CeedInt             num_active_fields;
  const CeedInt      *active_field_sizes;
  PetscInt            dim, q_data_size = model_data->q_data_volume_size;
  const PetscInt      height_cell = 0;
  const char         *volume_label_name, **active_field_names;
  DMLabel             domain_label;
  PetscInt           *domain_values;
  CeedElemRestriction elem_restr_q_data = NULL;
  CeedVector          q_data            = NULL;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Swarm To Mesh"));

  // DM information
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_mesh));
  PetscCall(DMGetDimension(dm_mesh, &dim));
  PetscCall(RatelMaterialGetVolumeLabelName(material, &volume_label_name));
  PetscCall(DMGetLabel(dm_mesh, volume_label_name, &domain_label));
  PetscCall(RatelMaterialGetVolumeLabelValues(material, NULL, &domain_values));

  // Active fields
  PetscCall(RatelMaterialGetActiveFieldSizes(material, &num_active_fields, &active_field_sizes));
  PetscCall(RatelMaterialGetActiveFieldNames(material, &active_field_names, NULL));

  // Points restriction and reference coordinates
  PetscCall(RatelMaterialGetMPMContext(material, &mpm));

  // QData restriction and vector
  PetscCall(RatelMaterialGetVolumeQData(material, &elem_restr_q_data, &q_data));

  // Points to mesh projection operator for field
  for (PetscInt i = 0; i < num_active_fields; i++) {
    CeedInt              num_comp = active_field_sizes[i];
    CeedElemRestriction  elem_restr_u_mesh, elem_restr_u_points;
    CeedBasis            basis_u;
    CeedOperator         sub_op_swarm_to_mesh, sub_op_mass;
    CeedQFunction        qf_swarm_to_mesh, qf_mass;
    CeedVector           u_points;
    CeedScalar          *U_loc_points;
    char                 field_name_in[PETSC_MAX_PATH_LEN], field_name_out[PETSC_MAX_PATH_LEN];
    CeedQFunctionContext ctx;

    // -- Create context
    RatelCallCeed(ratel, CeedQFunctionContextCreate(ceed, &ctx));
    RatelCallCeed(ratel, CeedQFunctionContextSetData(ctx, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(num_comp), &num_comp));

    // -- Create basis
    PetscCall(RatelDMPlexCeedBasisCreate(ratel, dm_mesh, domain_label, domain_values[0], height_cell, i, &basis_u));

    // -- Create restriction
    PetscCall(RatelDMPlexCeedElemRestrictionCreate(ratel, dm_mesh, domain_label, domain_values[0], height_cell, i, &elem_restr_u_mesh));
    PetscCall(RatelMPMContextCeedElemRestrictionCreateAtPoints(mpm, num_comp, &elem_restr_u_points));

    // -- Get points vector
    RatelCallCeed(ratel, CeedElemRestrictionCreateVector(elem_restr_u_points, &u_points, NULL));
    PetscCall(DMSwarmGetField(dm_swarm, active_field_names[i], NULL, NULL, (void **)&U_loc_points));
    RatelCallCeed(ratel, CeedVectorSetArray(u_points, CEED_MEM_HOST, CEED_USE_POINTER, U_loc_points));
    PetscCall(DMSwarmRestoreField(dm_swarm, active_field_names[i], NULL, NULL, (void **)&U_loc_points));

    // -- Create swarm to mesh qfunction
    RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, Mass, RatelQFunctionRelativePath(Mass_loc), &qf_swarm_to_mesh));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_swarm_to_mesh, "q data", q_data_size, CEED_EVAL_NONE));
    PetscCall(PetscSNPrintf(field_name_in, PETSC_MAX_PATH_LEN, "u_swarm, field %" PetscInt_FMT, i));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_swarm_to_mesh, field_name_in, num_comp, CEED_EVAL_NONE));
    PetscCall(PetscSNPrintf(field_name_out, PETSC_MAX_PATH_LEN, "u_mesh, field %" PetscInt_FMT, i));
    RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_swarm_to_mesh, field_name_out, num_comp, CEED_EVAL_INTERP));
    RatelCallCeed(ratel, CeedQFunctionSetContext(qf_swarm_to_mesh, ctx));
    RatelCallCeed(ratel, CeedQFunctionSetUserFlopsEstimate(qf_swarm_to_mesh, num_comp));

    // -- Create swarm to mesh operator
    RatelCallCeed(ratel, CeedOperatorCreateAtPoints(ceed, qf_swarm_to_mesh, NULL, NULL, &sub_op_swarm_to_mesh));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_swarm_to_mesh, "q data", elem_restr_q_data, CEED_BASIS_NONE, q_data));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_swarm_to_mesh, field_name_in, elem_restr_u_points, CEED_BASIS_NONE, u_points));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_swarm_to_mesh, field_name_out, elem_restr_u_mesh, basis_u, CEED_VECTOR_ACTIVE));
    PetscCall(RatelMPMContextCeedOperatorSetPoints(mpm, sub_op_swarm_to_mesh));

    // -- Add to composite operator
    RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_swarm_to_mesh, sub_op_swarm_to_mesh));
    if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_swarm_to_mesh, stdout));

    // -- Create mass qfunction
    RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, Mass, RatelQFunctionRelativePath(Mass_loc), &qf_mass));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_mass, "q data", q_data_size, CEED_EVAL_NONE));
    PetscCall(PetscSNPrintf(field_name_in, PETSC_MAX_PATH_LEN, "u, field %" PetscInt_FMT, i));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_mass, field_name_in, num_comp, CEED_EVAL_INTERP));
    PetscCall(PetscSNPrintf(field_name_out, PETSC_MAX_PATH_LEN, "v, field %" PetscInt_FMT, i));
    RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_mass, field_name_out, num_comp, CEED_EVAL_INTERP));
    RatelCallCeed(ratel, CeedQFunctionSetContext(qf_mass, ctx));
    RatelCallCeed(ratel, CeedQFunctionSetUserFlopsEstimate(qf_mass, num_comp));

    // -- Create mass operator
    RatelCallCeed(ratel, CeedOperatorCreateAtPoints(ceed, qf_mass, NULL, NULL, &sub_op_mass));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_mass, "q data", elem_restr_q_data, CEED_BASIS_NONE, q_data));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_mass, field_name_in, elem_restr_u_mesh, basis_u, CEED_VECTOR_ACTIVE));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_mass, field_name_out, elem_restr_u_mesh, basis_u, CEED_VECTOR_ACTIVE));
    PetscCall(RatelMPMContextCeedOperatorSetPoints(mpm, sub_op_mass));

    // -- Add to composite operator
    RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_mass, sub_op_mass));
    if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_mass, stdout));

    // -- Cleanup
    RatelCallCeed(ratel, CeedBasisDestroy(&basis_u));
    RatelCallCeed(ratel, CeedVectorDestroy(&u_points));
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&elem_restr_u_mesh));
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&elem_restr_u_points));
    RatelCallCeed(ratel, CeedQFunctionContextDestroy(&ctx));
    RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_swarm_to_mesh));
    RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_mass));
    RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_swarm_to_mesh));
    RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_mass));
  }

  // Cleanup
  PetscCall(DMDestroy(&dm_mesh));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&elem_restr_q_data));
  RatelCallCeed(ratel, CeedVectorDestroy(&q_data));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Swarm To Mesh Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Project material point quantities to mesh fields.

  Collective across MPI processes.

  @param[in]   ratel  `Ratel` context
  @param[out]  U      Global mesh solution vector

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMPMSwarmToMesh(Ratel ratel, Vec U) {
  DM           dm_mesh;
  PetscInt     dim;
  Vec          B;
  Mat          M;
  KSP          ksp_projection;
  PC           pc_projection;
  CeedOperator op_swarm_to_mesh, op_mass;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel MPM Swarm To Mesh"));

  // DM information
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_mesh));
  PetscCall(DMGetGlobalVector(dm_mesh, &B));
  PetscCall(DMGetDimension(dm_mesh, &dim));

  // Create composite operators
  RatelCallCeed(ratel, CeedCompositeOperatorCreate(ratel->ceed, &op_swarm_to_mesh));
  RatelCallCeed(ratel, CeedOperatorSetName(op_swarm_to_mesh, "swarm to mesh"));
  RatelCallCeed(ratel, CeedCompositeOperatorCreate(ratel->ceed, &op_mass));
  RatelCallCeed(ratel, CeedOperatorSetName(op_mass, "swarm mass operator"));

  for (PetscInt i = 0; i < ratel->num_materials; i++) {
    PetscCall(RatelMaterialSetupSwarmToMeshSuboperators_MPM(ratel->materials[i], op_swarm_to_mesh, op_mass));
  }

  // Compute RHS vector for projection
  {
    PetscMemType B_mem_type;
    Vec          B_loc;
    CeedVector   b_mesh;

    PetscCall(DMGetLocalVector(dm_mesh, &B_loc));
    PetscCall(CeedEvaluatorGetLocalCeedVectors(ratel->evaluator_residual_u, &b_mesh, NULL, NULL));
    PetscCall(VecZeroEntries(B_loc));
    PetscCall(VecPetscToCeed(B_loc, &B_mem_type, b_mesh));

    // Apply composite operator
    RatelCallCeed(ratel, CeedOperatorApply(op_swarm_to_mesh, CEED_VECTOR_NONE, b_mesh, CEED_REQUEST_IMMEDIATE));

    // Local to global
    PetscCall(VecCeedToPetsc(b_mesh, B_mem_type, B_loc));
    PetscCall(VecZeroEntries(B));
    PetscCall(DMLocalToGlobal(dm_mesh, B_loc, ADD_VALUES, B));

    // Cleanup
    PetscCall(DMRestoreLocalVector(dm_mesh, &B_loc));
    PetscCall(CeedEvaluatorRestoreLocalCeedVectors(ratel->evaluator_residual_u, &b_mesh, NULL, NULL));
  }

  // Create mass matrix
  PetscCall(MatCreateCeed(dm_mesh, dm_mesh, op_mass, NULL, &M));
  PetscCall(KSPCreate(ratel->comm, &ksp_projection));
  PetscCall(KSPSetOptionsPrefix(ksp_projection, "mpm_"));
  PetscCall(KSPSetType(ksp_projection, KSPCG));
  PetscCall(KSPSetNormType(ksp_projection, KSP_NORM_NATURAL));
  PetscCall(KSPGetPC(ksp_projection, &pc_projection));
  PetscCall(PCSetType(pc_projection, PCJACOBI));
  PetscCall(PCJacobiSetType(pc_projection, PC_JACOBI_ROWSUM));
  PetscCall(KSPSetFromOptions(ksp_projection));
  PetscCall(KSPSetOperators(ksp_projection, M, M));

  // Solve
  PetscCall(VecZeroEntries(U));
  PetscCall(KSPSolve(ksp_projection, B, U));

  // LCOV_EXCL_START
  if (ratel->is_debug) {
    // KSP summary
    KSPType            ksp_type;
    KSPConvergedReason reason;
    PetscReal          rnorm;
    PetscInt           its;
    PetscCall(KSPGetType(ksp_projection, &ksp_type));
    PetscCall(KSPGetConvergedReason(ksp_projection, &reason));
    PetscCall(KSPGetIterationNumber(ksp_projection, &its));
    PetscCall(KSPGetResidualNorm(ksp_projection, &rnorm));

    PetscCall(RatelPrintf256(ratel, RATEL_DEBUG_COLOR_SUCCESS,
                             "Swarm-to-Mesh Projection KSP Solve:\n"
                             "  KSP type: %s\n"
                             "  KSP convergence: %s\n"
                             "  Total KSP iterations: %" PetscInt_FMT "\n"
                             "  Final rnorm: %e\n",
                             ksp_type, KSPConvergedReasons[reason], its, (double)rnorm));
  }
  // LCOV_EXCL_STOP

  // Cleanup
  PetscCall(DMRestoreGlobalVector(dm_mesh, &B));
  PetscCall(DMDestroy(&dm_mesh));
  PetscCall(MatDestroy(&M));
  PetscCall(KSPDestroy(&ksp_projection));
  RatelCallCeed(ratel, CeedOperatorDestroy(&op_swarm_to_mesh));
  RatelCallCeed(ratel, CeedOperatorDestroy(&op_mass));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel MPM Swarm To Mesh Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Set the volume of each material point in the `DMSwarm`.

  Collective across MPI processes.

  @param[in]  ratel  `Ratel` context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMPMSetVolume(Ratel ratel) {
  DM             dm_swarm = ratel->dm_solution, dm_mesh;
  PetscInt       dim, cell_start, cell_end;
  const PetscInt height_cell = 0;
  PetscScalar   *volume;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel MPM Set Point Volume"));

  // DM information
  PetscCall(DMGetDimension(dm_swarm, &dim));
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_mesh));
  PetscCall(DMPlexGetHeightStratum(dm_mesh, height_cell, &cell_start, &cell_end));

  PetscCall(DMSwarmSortGetAccess(dm_swarm));

  // Get volume vector
  PetscCall(DMSwarmGetField(dm_swarm, DMSwarmPICField_volume, NULL, NULL, (void **)&volume));

  for (PetscInt c = cell_start; c < cell_end; c++) {
    PetscInt        num_points_per_cell;
    PetscInt       *points_in_cell;
    PetscScalar     point_volume = 1.0;
    PetscFVCellGeom cell_geom;
    PetscCall(DMPlexComputeCellGeometryFVM(dm_mesh, c, &cell_geom.volume, cell_geom.centroid, NULL));

    PetscCall(DMSwarmSortGetNumberOfPointsPerCell(dm_swarm, c, &num_points_per_cell));
    if (num_points_per_cell > 0) {
      point_volume = cell_geom.volume / num_points_per_cell;
    } else {
      continue;
    }
    PetscCall(DMSwarmSortGetPointsPerCell(dm_swarm, c, &num_points_per_cell, &points_in_cell));
    for (PetscInt p = 0; p < num_points_per_cell; p++) {
      volume[points_in_cell[p]] = point_volume;
    }
    PetscCall(DMSwarmSortRestorePointsPerCell(dm_swarm, c, &num_points_per_cell, &points_in_cell));
  }

  // Restore volume vector
  PetscCall(DMSwarmRestoreField(dm_swarm, DMSwarmPICField_volume, NULL, NULL, (void **)&volume));
  PetscCall(DMSwarmSortRestoreAccess(dm_swarm));
  PetscCall(DMDestroy(&dm_mesh));
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel MPM Set Point Volume Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Compute `CeedOperator` for updating MPM volume.

  Collective across MPI processes.

  @param[in]   material     `RatelMaterial` context
  @param[in]   volume_ceed  `CeedVector` holding previous volume
  @param[out]  op_volume    `CeedOperator` for updating volume

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelMaterialSetupUpdateVolumeSuboperator_MPM(RatelMaterial material, CeedVector volume_ceed, CeedOperator op_volume) {
  Ratel                ratel      = material->ratel;
  RatelModelData       model_data = material->model_data;
  DM                   dm_swarm   = ratel->dm_solution;
  PetscInt             dim;
  CeedElemRestriction  elem_restr_volume;
  CeedVector           q_data = NULL, volume_old_ceed = NULL;
  CeedOperator         sub_op_volume;
  CeedQFunction        qf_volume;
  RatelMPMContext      mpm;
  CeedInt              num_active_fields, q_data_size = model_data->q_data_volume_size;
  const CeedInt       *active_field_sizes;
  CeedElemRestriction *restrictions_u = NULL, restriction_q_data = NULL;
  CeedBasis           *bases_u = NULL;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Update Volume MPM"));

  // DM information
  PetscCall(DMGetDimension(dm_swarm, &dim));

  // libCEED objects
  // -- MPM context
  PetscCall(RatelMaterialGetMPMContext(material, &mpm));
  PetscCall(RatelMPMContextCeedElemRestrictionCreateAtPoints(mpm, 1, &elem_restr_volume));
  RatelCallCeed(ratel, CeedElemRestrictionCreateVector(elem_restr_volume, &volume_old_ceed, NULL));
  RatelCallCeed(ratel, CeedVectorCopy(volume_ceed, volume_old_ceed));
  // -- Active field sizes
  PetscCall(RatelMaterialGetActiveFieldSizes(material, &num_active_fields, &active_field_sizes));
  // -- QData
  PetscCall(RatelMaterialGetVolumeQData(material, &restriction_q_data, &q_data));
  // -- Residual data
  {
    CeedOperator op_residual_u;

    PetscCall(CeedEvaluatorGetCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
    // ---- Solution Data
    PetscCall(RatelMaterialGetSolutionData(material, op_residual_u, &num_active_fields, &restrictions_u, &bases_u));
    PetscCall(CeedEvaluatorRestoreCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
  }

  // Create volume qfunction which needs only the gradient of displacement (field 0) to compute J
  RatelCallCeed(ratel, CeedQFunctionCreateInterior(ratel->ceed, 1, model_data->update_volume_mpm, model_data->update_volume_mpm_loc, &qf_volume));
  RatelCallCeed(ratel, CeedQFunctionAddInput(qf_volume, "q data", q_data_size, CEED_EVAL_NONE));
  RatelCallCeed(ratel, CeedQFunctionAddInput(qf_volume, "du/dX", active_field_sizes[0] * dim, CEED_EVAL_GRAD));
  RatelCallCeed(ratel, CeedQFunctionAddInput(qf_volume, "volume_old", 1, CEED_EVAL_NONE));
  RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_volume, "volume", 1, CEED_EVAL_NONE));

  // Create volume operator
  RatelCallCeed(ratel, CeedOperatorCreateAtPoints(ratel->ceed, qf_volume, NULL, NULL, &sub_op_volume));
  RatelCallCeed(ratel, CeedOperatorSetField(sub_op_volume, "q data", restriction_q_data, CEED_BASIS_NONE, q_data));
  RatelCallCeed(ratel, CeedOperatorSetField(sub_op_volume, "du/dX", restrictions_u[0], bases_u[0], CEED_VECTOR_ACTIVE));
  RatelCallCeed(ratel, CeedOperatorSetField(sub_op_volume, "volume_old", elem_restr_volume, CEED_BASIS_NONE, volume_old_ceed));
  RatelCallCeed(ratel, CeedOperatorSetField(sub_op_volume, "volume", elem_restr_volume, CEED_BASIS_NONE, volume_ceed));
  // -- Set points
  PetscCall(RatelMPMContextCeedOperatorSetPoints(mpm, sub_op_volume));
  // -- Add to composite operator
  RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_volume, sub_op_volume));
  if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_volume, stdout));

  // -- Cleanup
  RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_volume));
  RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_volume));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&elem_restr_volume));
  for (PetscInt i = 0; i < num_active_fields; i++) {
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restrictions_u[i]));
    RatelCallCeed(ratel, CeedBasisDestroy(&bases_u[i]));
  }
  PetscCall(PetscFree(restrictions_u));
  PetscCall(PetscFree(bases_u));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_q_data));
  RatelCallCeed(ratel, CeedVectorDestroy(&q_data));
  RatelCallCeed(ratel, CeedVectorDestroy(&volume_old_ceed));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Update Volume MPM Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Update MPM volume.

  Collective across MPI processes.

  @param[in]  ratel    `Ratel` context
  @param[in]  U        Global mesh solution vector
  @param[in]  time     Current time

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMPMUpdateVolume(Ratel ratel, Vec U, PetscReal time) {
  DM           dm_swarm = ratel->dm_solution, dm_mesh;
  CeedOperator op_volume;
  PetscInt     lsize;
  Vec          U_loc, volume_loc;
  CeedVector   u_loc_ceed, volume_ceed;
  PetscMemType u_mem_type, volume_mem_type;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Update Volume"));

  // DM information
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_mesh));

  // Create swarm volume vectors
  PetscCall(DMSwarmCreateLocalVectorFromField(dm_swarm, DMSwarmPICField_volume, &volume_loc));
  PetscCall(VecGetLocalSize(volume_loc, &lsize));
  RatelCallCeed(ratel, CeedVectorCreate(ratel->ceed, lsize, &volume_ceed));

  // Get local solution vector
  PetscCall(DMGetLocalVector(dm_mesh, &U_loc));
  PetscCall(DMGlobalToLocal(dm_mesh, U, INSERT_VALUES, U_loc));
  PetscCall(DMPlexInsertBoundaryValues(dm_mesh, PETSC_TRUE, U_loc, time, NULL, NULL, NULL));
  PetscCall(CeedEvaluatorGetLocalCeedVectors(ratel->evaluator_residual_u, &u_loc_ceed, NULL, NULL));

  // Create composite operators
  RatelCallCeed(ratel, CeedCompositeOperatorCreate(ratel->ceed, &op_volume));
  RatelCallCeed(ratel, CeedOperatorSetName(op_volume, "update point volume operator"));

  // Petsc to Ceed
  PetscCall(VecPetscToCeed(volume_loc, &volume_mem_type, volume_ceed));
  PetscCall(VecPetscToCeed(U_loc, &u_mem_type, u_loc_ceed));

  for (PetscInt i = 0; i < ratel->num_materials; i++) {
    PetscCall(RatelMaterialSetupUpdateVolumeSuboperator_MPM(ratel->materials[i], volume_ceed, op_volume));
  }

  // Apply composite operator
  RatelCallCeed(ratel, CeedOperatorApply(op_volume, u_loc_ceed, CEED_VECTOR_NONE, CEED_REQUEST_IMMEDIATE));

  // Ceed to Petsc
  PetscCall(VecCeedToPetsc(volume_ceed, volume_mem_type, volume_loc));
  PetscCall(VecCeedToPetsc(u_loc_ceed, u_mem_type, U_loc));

  // Cleanup
  PetscCall(DMRestoreLocalVector(dm_mesh, &U_loc));
  PetscCall(DMSwarmDestroyLocalVectorFromField(dm_swarm, DMSwarmPICField_volume, &volume_loc));
  RatelCallCeed(ratel, CeedVectorDestroy(&volume_ceed));
  PetscCall(CeedEvaluatorRestoreLocalCeedVectors(ratel->evaluator_residual_u, &u_loc_ceed, NULL, NULL));
  PetscCall(DMDestroy(&dm_mesh));
  RatelCallCeed(ratel, CeedOperatorDestroy(&op_volume));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel MPM Update Volume Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup base (fine-grain) DMs, both DMSwarm and DMPlex, for a material point method (MPM) model.

  Collective across MPI processes.

  @param[in]   ratel        `Ratel` context
  @param[out]  dm_solution  DM for the solution
**/
PetscErrorCode RatelSolutionDMSetup_MPM(Ratel ratel, DM *dm_solution) {
  DM              dm_mesh;
  RatelMPMOptions mpm_options;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DM Setup MPM"));

  // Setup mesh DM
  PetscCall(RatelSolutionDMSetup_FEM(ratel, &dm_mesh));

  // Setup swarm DM
  PetscCall(RatelDMSwarmCreateFromOptions(ratel, dm_mesh, dm_solution));

  // Read options for MPM
  PetscCall(RatelMPMOptionsCreateFromOptions(ratel, &mpm_options));

  // Set up points and fields at points
  {
    CeedInt        num_active_fields;
    const CeedInt *active_field_sizes;
    const char   **active_field_names, **active_component_names;

    PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[0], &num_active_fields, &active_field_sizes));
    PetscCall(RatelMaterialGetActiveFieldNames(ratel->materials[0], &active_field_names, &active_component_names));
    PetscCall(RatelDMSetupByOrder_MPM(ratel, mpm_options, num_active_fields, active_field_sizes, active_field_names, *dm_solution));
  }

  // Initialize material points
  PetscCall(RatelDMSwarmInitalizePointLocations(ratel, mpm_options, *dm_solution));

  // Set volume
  PetscCall(RatelMPMSetVolume(ratel));
  if (ratel->materials[0]->model_data->num_comp_state > 0) {
    Vec state;

    PetscCall(DMSwarmCreateGlobalVectorFromField(*dm_solution, "model state", &state));
    PetscCall(VecSet(state, 0.0));
    PetscCall(DMSwarmDestroyGlobalVectorFromField(*dm_solution, "model state", &state));
  }

  // View final result
  PetscCall(PetscObjectSetName((PetscObject)dm_mesh, "Points Solution"));
  PetscCall(DMViewFromOptions(dm_mesh, NULL, "-dm_view"));

  // Set Ratel as DM application context
  PetscCall(DMSetApplicationContext(dm_mesh, ratel));

  // Cleanup
  PetscCall(PetscFree(mpm_options));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DM Setup MPM Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup `DMSwarm` with material points and point fields.

  @param[in]   ratel        `Ratel` context
  @param[in]   mpm_options  `RatelMPMOptions` context
  @param[in]   num_fields   Number of fields in solution vector
  @param[in]   field_sizes  Array of number of components for each field
  @param[in]   field_names  Names of fields in `RatelMaterial`
  @param[out]  dm           `DM` to setup

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDMSetupByOrder_MPM(Ratel ratel, RatelMPMOptions mpm_options, CeedInt num_fields, const CeedInt *field_sizes,
                                       const char **field_names, DM dm) {
  RatelMaterial  material = ratel->materials[0];
  CeedInt        num_point_fields;
  const CeedInt *point_field_sizes;
  const char   **point_field_names;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DM Setup By Order MPM"));

  // Setup DM fields on points
  PetscCall(DMSwarmInitializeFieldRegister(dm));
  for (PetscInt i = 0; i < num_fields; i++) {
    PetscCall(DMSwarmRegisterPetscDatatypeField(dm, field_names[i], field_sizes[i], PETSC_SCALAR));
  }

  // Add material property fields
  // TODO: Support multiple materials
  PetscCall(RatelMaterialGetPointFields(material, &num_point_fields, &point_field_sizes, &point_field_names));
  for (PetscInt i = 0; i < num_point_fields; i++) {
    PetscCall(DMSwarmRegisterPetscDatatypeField(dm, point_field_names[i], point_field_sizes[i], PETSC_SCALAR));
  }
  if (material->model_data->num_comp_state > 0) {
    PetscCall(DMSwarmRegisterPetscDatatypeField(dm, "model state", material->model_data->num_comp_state, PETSC_SCALAR));
  }
  PetscCall(DMSwarmRegisterPetscDatatypeField(dm, DMSwarmPICField_volume, 1, PETSC_SCALAR));
  PetscCall(DMSwarmFinalizeFieldRegister(dm));

  {
    DM       dm_mesh;
    PetscInt c_start, c_end, num_cells_local;

    PetscCall(DMSwarmGetCellDM(dm, &dm_mesh));
    PetscCall(DMPlexGetHeightStratum(dm_mesh, 0, &c_start, &c_end));
    num_cells_local = c_end - c_start;
    PetscCall(DMSwarmSetLocalSizes(dm, num_cells_local * mpm_options->num_points_per_cell, 0));
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DM Setup By Order MPM Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Reset material point operators and data.

  Collective across MPI processes.

  @param[in]  material  `RatelMaterial` context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialReset_MPM(RatelMaterial material) {
  Ratel ratel = material->ratel;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Reset Operators MPM"));

  // MPM Context
  PetscCall(RatelMPMContextDestroy(&material->mpm));

  // Indices
  PetscCall(PetscFree(material->residual_indices));
  material->num_residual_indices = 0;
  PetscCall(PetscFree(material->residual_ut_indices));
  material->num_residual_ut_indices = 0;
  PetscCall(PetscFree(material->jacobian_indices));
  material->num_jacobian_indices = 0;
  PetscCall(PetscFree(material->RatelMaterialSetupMultigridLevels));
  PetscCall(PetscFree(material->ctx_params_labels));

  // Note: We don't reset the qdata and element restrictions in the material, as those are for mesh quadrature points

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Reset Operators MPM Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Project solution to points and migrate, then reset material point method (MPM) context and data.

  Collective across MPI processes.

  @param[in]  ratel    `Ratel` context
  @param[in]  delta_U  Global mesh solution vector
  @param[in]  time     Current time

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMPMMigrate(Ratel ratel, Vec delta_U, PetscReal time) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel MPM Reset"));

  // Update volume
  PetscCall(RatelMPMUpdateVolume(ratel, delta_U, time));

  // Project current solution to material points
  PetscCall(RatelMPMMeshToSwarm(ratel, delta_U, time, PETSC_TRUE));

  // Migrate material points
  for (PetscInt i = 0; i < ratel->num_point_fields; i++) {
    // -- Pull fields to host
    RatelCallCeed(ratel, CeedVectorSyncArray(ratel->point_fields[i], CEED_MEM_HOST));
  }
  PetscCall(DMSwarmMigrate(ratel->dm_solution, PETSC_TRUE));

  // Reset materials
  for (PetscInt i = 0; i < ratel->num_materials; i++) {
    PetscCall(RatelMaterialReset_MPM(ratel->materials[i]));
  }

  // Destroy global objects
  // -- Point vectors
  for (PetscInt i = 0; i < ratel->num_point_fields; i++) {
    RatelCallCeed(ratel, CeedVectorDestroy(&ratel->point_fields[i]));
  }
  PetscCall(PetscFree(ratel->point_fields));
  // -- Diagnostic projection KSP
  PetscCall(KSPDestroy(&ratel->ksp_diagnostic_projection));
  // -- Mats
  PetscCall(MatDestroy(&ratel->mat_jacobian));
  PetscCall(MatDestroy(&ratel->mat_jacobian_pre));
  ratel->are_snes_mats_set = PETSC_FALSE;
  // -- Operator Apply Contexts
  PetscCall(CeedEvaluatorDestroy(&ratel->evaluator_residual_u));
  PetscCall(CeedEvaluatorDestroy(&ratel->evaluator_residual_ut));
  PetscCall(CeedEvaluatorDestroy(&ratel->evaluator_residual_utt));
  PetscCall(CeedEvaluatorDestroy(&ratel->evaluator_strain_energy));
  PetscCall(CeedEvaluatorDestroy(&ratel->evaluator_external_energy));
  PetscCall(CeedEvaluatorDestroy(&ratel->evaluator_projected_diagnostic));
  PetscCall(CeedEvaluatorDestroy(&ratel->evaluator_dual_diagnostic));
  PetscCall(CeedEvaluatorDestroy(&ratel->evaluator_dual_nodal_scale));
  PetscCall(CeedEvaluatorDestroy(&ratel->evaluator_surface_force));
  PetscCall(CeedEvaluatorDestroy(&ratel->evaluator_mms_error));
  for (PetscInt i = 0; i < ratel->surface_force_face_count; i++) {
    PetscCall(CeedEvaluatorDestroy(&ratel->evaluators_surface_displacement[i]));
    PetscCall(CeedEvaluatorDestroy(&ratel->evaluators_surface_force_face[i]));
    PetscCall(CeedEvaluatorDestroy(&ratel->evaluators_surface_force_cell_to_face[i]));
  }
  // -- Indices
  PetscCall(PetscFree(ratel->jacobian_multiplicity_skip_indices));
  ratel->num_jacobian_multiplicity_skip_indices = 0;
  ratel->first_jacobian_pressure_index          = 0;
  // -- Evaluators
  PetscCall(CeedBoundaryEvaluatorDestroy(&ratel->boundary_evaluator_u));
  PetscCall(CeedBoundaryEvaluatorDestroy(&ratel->boundary_evaluator_visualization));
  // DMs
  if (ratel->sub_dms_diagnostic) PetscCall(DMDestroy(&ratel->sub_dms_diagnostic[RATEL_DIAGNOSTIC_PROJECTED]));
  if (ratel->sub_dms_diagnostic) PetscCall(DMDestroy(&ratel->sub_dms_diagnostic[RATEL_DIAGNOSTIC_DUAL]));
  PetscFree(ratel->sub_dms_diagnostic);
  if (ratel->is_sub_dms_diagnostic) PetscCall(ISDestroy(&ratel->is_sub_dms_diagnostic[RATEL_DIAGNOSTIC_PROJECTED]));
  if (ratel->is_sub_dms_diagnostic) PetscCall(ISDestroy(&ratel->is_sub_dms_diagnostic[RATEL_DIAGNOSTIC_DUAL]));
  PetscFree(ratel->is_sub_dms_diagnostic);
  PetscCall(DMDestroy(&ratel->dm_diagnostic));

  // Remap coordinates if needed
  {
    DM dm_mesh;

    PetscCall(RatelGetSolutionMeshDM(ratel, &dm_mesh));
    PetscCall(RatelRemapScaleCoordinates(ratel, time, dm_mesh));
    PetscCall(DMDestroy(&dm_mesh));
  }

  // Rebuild global objects
  PetscCall(RatelDMSetupSolver(ratel));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel MPM Reset Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Set material point fields.

  Collective across MPI processes.

  @param[in]  ratel  `Ratel` context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMPMSetPointFields(Ratel ratel) {
  PetscFunctionBeginUser;
  if (ratel->method_type != RATEL_METHOD_MPM) PetscFunctionReturn(PETSC_SUCCESS);
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel MPM Set Point Fields"));

  if (!ratel->materials[0]->has_point_fields_initialized) {
    CeedOperator op_set_point_fields;

    // Initialize material point fields
    RatelCallCeed(ratel, CeedCompositeOperatorCreate(ratel->ceed, &op_set_point_fields));
    for (PetscInt i = 0; i < ratel->num_materials; i++) {
      PetscCall(RatelMaterialSetupSetPointFieldsSuboperator_MPM(ratel->materials[i], op_set_point_fields));
    }
    RatelCallCeed(ratel, CeedOperatorApply(op_set_point_fields, CEED_VECTOR_NONE, CEED_VECTOR_NONE, CEED_REQUEST_IMMEDIATE));

    for (PetscInt i = 0; i < ratel->num_materials; i++) {
      ratel->materials[i]->has_point_fields_initialized = PETSC_TRUE;
    }

    // Cleanup
    RatelCallCeed(ratel, CeedOperatorDestroy(&op_set_point_fields));
  } else {
    for (PetscInt i = 0; i < ratel->num_materials; i++) {
      PetscCall(RatelMaterialSetPointFieldsVectors_MPM(ratel->materials[i]));
    }
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel MPM Set Point Fields Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Compute `CeedOperator` volumetric QData for `RatelMaterial`.

  Collective across MPI processes.

  @param[in]   material     `RatelMaterial` context
  @param[in]   label_name   `DMPlex` label name for volume
  @param[in]   label_value  `DMPlex` label value for volume
  @param[out]  restriction  `CeedElemRestriction` for QData
  @param[out]  q_data       `CeedVector` holding QData

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupVolumeQData_MPM(RatelMaterial material, const char *label_name, PetscInt label_value,
                                                 CeedElemRestriction *restriction, CeedVector *q_data) {
  Ratel               ratel = material->ratel;
  Ceed                ceed  = ratel->ceed;
  RatelMPMContext     mpm;
  CeedVector          x_loc, volume_points;
  CeedElemRestriction elem_restr_x_mesh, elem_restr_volume_points;
  CeedBasis           basis_x;
  RatelModelData      model_data  = material->model_data;
  PetscInt            q_data_size = model_data->q_data_volume_size;
  DM                  dm_mesh, dm_swarm = ratel->dm_solution;
  const char         *volume_label_name;
  const PetscInt      height_cell = 0;
  DMLabel             domain_label;
  PetscInt           *domain_values;
  PetscInt            dim;
  Vec                 X_loc;
  PetscMemType        x_mem_type;
  PetscScalar        *volume_arr;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Volume QData MPM"));

  // DM information
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_mesh));
  PetscCall(DMGetDimension(dm_mesh, &dim));
  PetscCall(RatelMaterialGetVolumeLabelName(material, &volume_label_name));
  PetscCall(DMGetLabel(dm_mesh, volume_label_name, &domain_label));
  PetscCall(RatelMaterialGetVolumeLabelValues(material, NULL, &domain_values));

  // libCEED objects
  // -- MPM context
  PetscCall(RatelMaterialGetMPMContext(material, &mpm));
  // -- Coordinate basis
  PetscCall(RatelDebug(ratel, "---- Coordinate basis"));
  PetscCall(RatelDMPlexCeedBasisCoordinateCreate(ratel, dm_mesh, domain_label, label_value, height_cell, &basis_x));
  // -- Coordinate restriction
  PetscCall(RatelDebug(ratel, "---- Coordinate restriction"));
  PetscCall(RatelDMPlexCeedElemRestrictionCoordinateCreate(ratel, dm_mesh, domain_label, label_value, height_cell, &elem_restr_x_mesh));
  // -- Volume restriction
  PetscCall(RatelDebug(ratel, "---- Volume restriction"));
  PetscCall(RatelMPMContextCeedElemRestrictionCreateAtPoints(mpm, 1, &elem_restr_volume_points));
  // -- QData restriction
  PetscCall(RatelDebug(ratel, "---- QData restriction"));
  PetscCall(RatelMPMContextCeedElemRestrictionCreateAtPoints(mpm, q_data_size, restriction));

  // Element coordinates
  PetscCall(RatelDebug(ratel, "---- Retrieving element coordinates"));
  PetscCall(DMGetCoordinatesLocal(dm_mesh, &X_loc));
  RatelCallCeed(ratel, CeedElemRestrictionCreateVector(elem_restr_x_mesh, &x_loc, NULL));
  PetscCall(VecReadPetscToCeed(X_loc, &x_mem_type, x_loc));

  // libCEED vectors
  PetscCall(RatelDebug(ratel, "---- QData vector"));
  // -- Geometric data vector
  RatelCallCeed(ratel, CeedElemRestrictionCreateVector(*restriction, q_data, NULL));
  // -- Volume vector
  RatelCallCeed(ratel, CeedElemRestrictionCreateVector(elem_restr_volume_points, &volume_points, NULL));
  PetscCall(DMSwarmGetField(dm_swarm, DMSwarmPICField_volume, NULL, NULL, (void **)&volume_arr));
  RatelCallCeed(ratel, CeedVectorSetArray(volume_points, CEED_MEM_HOST, CEED_USE_POINTER, volume_arr));

  // Geometric factor computation
  {
    CeedQFunction qf_setup_q_data_volume;
    CeedOperator  op_setup_q_data_volume;

    PetscCall(RatelDebug(ratel, "---- Computing volumetric qdata"));

    // -- QFunction
    RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, model_data->setup_q_data_volume_mpm, model_data->setup_q_data_volume_mpm_loc,
                                                     &qf_setup_q_data_volume));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_setup_q_data_volume, "dx/dX", dim * dim, CEED_EVAL_GRAD));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_setup_q_data_volume, "weight", 1, CEED_EVAL_WEIGHT));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_setup_q_data_volume, "volume", 1, CEED_EVAL_NONE));
    RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_setup_q_data_volume, "q data", q_data_size, CEED_EVAL_NONE));

    // -- Operator
    RatelCallCeed(ratel, CeedOperatorCreateAtPoints(ceed, qf_setup_q_data_volume, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &op_setup_q_data_volume));
    PetscCall(RatelMaterialSetOperatorName(material, "volumetric geometric data", op_setup_q_data_volume));
    RatelCallCeed(ratel, CeedOperatorSetField(op_setup_q_data_volume, "dx/dX", elem_restr_x_mesh, basis_x, CEED_VECTOR_ACTIVE));
    RatelCallCeed(ratel, CeedOperatorSetField(op_setup_q_data_volume, "weight", CEED_ELEMRESTRICTION_NONE, basis_x, CEED_VECTOR_NONE));
    RatelCallCeed(ratel, CeedOperatorSetField(op_setup_q_data_volume, "volume", elem_restr_volume_points, CEED_BASIS_NONE, volume_points));
    RatelCallCeed(ratel, CeedOperatorSetField(op_setup_q_data_volume, "q data", *restriction, CEED_BASIS_NONE, CEED_VECTOR_ACTIVE));
    PetscCall(RatelMPMContextCeedOperatorSetPoints(mpm, op_setup_q_data_volume));

    // -- Compute the quadrature data
    if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(op_setup_q_data_volume, stdout));
    RatelCallCeed(ratel, CeedOperatorApply(op_setup_q_data_volume, x_loc, *q_data, CEED_REQUEST_IMMEDIATE));

    // -- Cleanup
    RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_setup_q_data_volume));
    RatelCallCeed(ratel, CeedOperatorDestroy(&op_setup_q_data_volume));
  }

  // Cleanup
  PetscCall(DMDestroy(&dm_mesh));
  PetscCall(VecCeedToPetsc(x_loc, x_mem_type, X_loc));
  RatelCallCeed(ratel, CeedVectorDestroy(&x_loc));
  PetscCall(DMSwarmRestoreField(dm_swarm, DMSwarmPICField_volume, NULL, NULL, (void **)&volume_arr));
  RatelCallCeed(ratel, CeedVectorDestroy(&volume_points));
  RatelCallCeed(ratel, CeedBasisDestroy(&basis_x));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&elem_restr_x_mesh));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&elem_restr_volume_points));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Volume QData MPM Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

// Operators
// -- Material properties
/**
  @brief Setup material properties for `RatelMaterial` by writing values from material context to `DMSwarm` fields.

  Collective across MPI processes.

  @param[in]  material             `RatelMaterial` context
  @param[in]  op_set_point_fields  `CeedOperator` to setup

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupSetPointFieldsSuboperator_MPM(RatelMaterial material, CeedOperator op_set_point_fields) {
  Ratel                ratel      = material->ratel;
  RatelModelData       model_data = material->model_data;
  RatelMPMContext      mpm;
  Ceed                 ceed = ratel->ceed;
  const char         **point_field_names;
  CeedInt              num_point_fields;
  const CeedInt       *point_field_sizes;
  CeedQFunction        qf_set_point_fields;
  CeedOperator         sub_op_set_point_fields;
  CeedElemRestriction *restrictions_point_fields = NULL;
  CeedVector          *point_fields              = NULL;

  PetscFunctionBeginUser;
  if (material->model_data->num_point_fields == 0) PetscFunctionReturn(PETSC_SUCCESS);
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Set Point Fields Suboperator MPM"));

  // Active fields
  PetscCall(RatelMaterialGetPointFields(material, &num_point_fields, &point_field_sizes, &point_field_names));

  // libCEED objects
  // -- MPM context
  PetscCall(RatelMaterialGetMPMContext(material, &mpm));

  // -- Point field objects from residual operator
  {
    CeedOperator op_residual_u;

    PetscCall(CeedEvaluatorGetCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
    PetscCall(RatelMaterialGetPointData(material, op_residual_u, &num_point_fields, &restrictions_point_fields, &point_fields));
    PetscCall(CeedEvaluatorRestoreCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
  }

  // -- QFunction
  RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, model_data->set_point_fields, model_data->set_point_fields_loc, &qf_set_point_fields));
  for (PetscInt i = 0; i < num_point_fields; i++) {
    RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_set_point_fields, point_field_names[i], point_field_sizes[i], CEED_EVAL_NONE));
  }
  if (model_data->num_comp_state > 0) {
    RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_set_point_fields, "model state", model_data->num_comp_state, CEED_EVAL_NONE));
    RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_set_point_fields, "current model state", model_data->num_comp_state, CEED_EVAL_NONE));
  }
  RatelCallCeed(ratel, CeedQFunctionSetContext(qf_set_point_fields, material->ctx_params));

  // -- Operator
  RatelCallCeed(ratel, CeedOperatorCreateAtPoints(ceed, qf_set_point_fields, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &sub_op_set_point_fields));
  for (PetscInt i = 0; i < num_point_fields; i++) {
    RatelCallCeed(
        ratel, CeedOperatorSetField(sub_op_set_point_fields, point_field_names[i], restrictions_point_fields[i], CEED_BASIS_NONE, point_fields[i]));
  }
  if (model_data->num_comp_state > 0) {
    CeedOperator        op_residual_u;
    CeedElemRestriction restriction_initial_state, restriction_state;
    CeedVector          initial_state, state;

    PetscCall(CeedEvaluatorGetCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
    PetscCall(RatelMaterialGetInitialStateData(material, op_residual_u, &restriction_initial_state, &initial_state));
    PetscCall(RatelMaterialGetStateData(material, op_residual_u, &restriction_state, &state));
    PetscCall(CeedEvaluatorRestoreCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_set_point_fields, "model state", restriction_initial_state, CEED_BASIS_NONE, initial_state));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_set_point_fields, "current model state", restriction_state, CEED_BASIS_NONE, state));
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_state));
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_initial_state));
    RatelCallCeed(ratel, CeedVectorDestroy(&state));
    RatelCallCeed(ratel, CeedVectorDestroy(&initial_state));
  }
  PetscCall(RatelMPMContextCeedOperatorSetPoints(mpm, sub_op_set_point_fields));
  RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_set_point_fields, sub_op_set_point_fields));

  // Cleanup
  for (PetscInt i = 0; i < num_point_fields; i++) {
    RatelCallCeed(ratel, CeedVectorDestroy(&point_fields[i]));
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restrictions_point_fields[i]));
  }
  PetscCall(PetscFree(point_fields));
  PetscCall(PetscFree(restrictions_point_fields));
  RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_set_point_fields));
  RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_set_point_fields));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Set Point Fields Suboperator MPM Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Set point fields vectors for `RatelMaterial` using `DMSwarm` field pointers.

  Not collective across MPI processes.

  @param[in]  material  `RatelMaterial` context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetPointFieldsVectors_MPM(RatelMaterial material) {
  Ratel                ratel = material->ratel;
  RatelMPMContext      mpm;
  const char         **point_field_names;
  CeedInt              num_point_fields;
  const CeedInt       *point_field_sizes;
  CeedElemRestriction *restrictions_point_fields = NULL;
  CeedVector          *point_fields              = NULL;

  PetscFunctionBeginUser;
  if (material->model_data->num_point_fields == 0) PetscFunctionReturn(PETSC_SUCCESS);
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Set Point Fields Vectors MPM"));

  // Active fields
  PetscCall(RatelMaterialGetPointFields(material, &num_point_fields, &point_field_sizes, &point_field_names));

  // libCEED objects
  // -- MPM context
  PetscCall(RatelMaterialGetMPMContext(material, &mpm));

  // -- Point field objects from residual operator
  {
    CeedOperator op_residual_u;

    PetscCall(CeedEvaluatorGetCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
    PetscCall(RatelMaterialGetPointData(material, op_residual_u, &num_point_fields, &restrictions_point_fields, &point_fields));
    PetscCall(CeedEvaluatorRestoreCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
  }
  // -- Point field vectors
  for (PetscInt i = 0; i < num_point_fields; i++) {
    PetscScalar *field_arr;

    PetscCall(DMSwarmGetField(ratel->dm_solution, point_field_names[i], NULL, NULL, (void **)&field_arr));
    RatelCallCeed(ratel, CeedVectorSetArray(point_fields[i], CEED_MEM_HOST, CEED_USE_POINTER, field_arr));
    PetscCall(DMSwarmRestoreField(ratel->dm_solution, point_field_names[i], NULL, NULL, (void **)&field_arr));
  }

  // Cleanup
  for (PetscInt i = 0; i < num_point_fields; i++) {
    RatelCallCeed(ratel, CeedVectorDestroy(&point_fields[i]));
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restrictions_point_fields[i]));
  }
  PetscCall(PetscFree(point_fields));
  PetscCall(PetscFree(restrictions_point_fields));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Set Point Fields Vectors MPM Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup Residual `CeedOperator` for `RatelMaterial`.

  Collective across MPI processes.

  @param[in]   material         `RatelMaterial` context
  @param[out]  op_residual_u    Composite residual u term `CeedOperator` to add `RatelMaterial` suboperator
  @param[out]  op_residual_ut   Composite residual u_t term `CeedOperator` to add `RatelMaterial` suboperator
  @param[out]  op_residual_utt  Composite residual u_tt term `CeedOperator` to add `RatelMaterial` suboperator

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupResidualSuboperators_MPM(RatelMaterial material, CeedOperator op_residual_u, CeedOperator op_residual_ut,
                                                          CeedOperator op_residual_utt) {
  Ratel           ratel      = material->ratel;
  RatelModelData  model_data = material->model_data;
  RatelMPMContext mpm;
  Ceed            ceed = ratel->ceed;
  DM              dm_mesh;
  CeedInt         num_active_fields, q_data_size                   = model_data->q_data_volume_size;
  const CeedInt  *active_field_sizes, *active_field_num_eval_modes = model_data->active_field_num_eval_modes,
                                     *ut_field_num_eval_modes = model_data->ut_field_num_eval_modes;
  const CeedEvalMode  *active_field_eval_modes = model_data->active_field_eval_modes, *ut_field_eval_modes = model_data->ut_field_eval_modes;
  PetscInt             dim;
  const PetscInt       height_cell = 0;
  const char          *volume_label_name, **active_field_names;
  DMLabel              domain_label;
  PetscInt            *domain_values;
  CeedVector           q_data = NULL, stored_values_u = NULL, state_values = NULL, state_values_current = NULL, *point_fields = NULL;
  CeedElemRestriction *restrictions_u = NULL, restriction_q_data = NULL, restriction_stored_values_u = NULL, restriction_state_values = NULL,
                      *restrictions_point_fields = NULL;
  CeedBasis *bases_u                             = NULL;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Residual Suboperators MPM"));

  // DM information
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_mesh));
  PetscCall(DMGetDimension(dm_mesh, &dim));
  PetscCall(RatelMaterialGetVolumeLabelName(material, &volume_label_name));
  PetscCall(DMGetLabel(dm_mesh, volume_label_name, &domain_label));
  PetscCall(RatelMaterialGetVolumeLabelValues(material, NULL, &domain_values));

  // Active fields
  PetscCall(RatelMaterialGetActiveFieldSizes(material, &num_active_fields, &active_field_sizes));
  PetscCall(RatelMaterialGetActiveFieldNames(material, &active_field_names, NULL));

  // libCEED objects
  // -- MPM context
  PetscCall(RatelMaterialGetMPMContext(material, &mpm));
  // -- QData
  PetscCall(RatelMaterialGetVolumeQData(material, &restriction_q_data, &q_data));
  // -- Stored data in residual_u at quadrature points
  if (model_data->num_comp_stored_u > 0) {
    PetscCall(RatelMPMContextCeedElemRestrictionCreateAtPoints(mpm, model_data->num_comp_stored_u, &restriction_stored_values_u));
    RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_stored_values_u, &stored_values_u, NULL));
    // Need to initialize to 0.0
    RatelCallCeed(ratel, CeedVectorSetValue(stored_values_u, 0.0));
  }
  // -- State data at quadrature points
  if (model_data->num_comp_state > 0) {
    PetscScalar *state_array;

    PetscCall(RatelMPMContextCeedElemRestrictionCreateAtPoints(mpm, model_data->num_comp_state, &restriction_state_values));
    RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_state_values, &state_values, NULL));
    RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_state_values, &state_values_current, NULL));
    PetscCall(DMSwarmGetField(ratel->dm_solution, "model state", NULL, NULL, (void **)&state_array));
    RatelCallCeed(ratel, CeedVectorSetArray(state_values, CEED_MEM_HOST, CEED_USE_POINTER, (CeedScalar *)state_array));
    PetscCall(DMSwarmRestoreField(ratel->dm_solution, "model state", NULL, NULL, (void **)&state_array));
    // Need to initialize current state
    RatelCallCeed(ratel, CeedVectorCopy(state_values, state_values_current));
  }
  // -- Point field data
  if (model_data->num_point_fields > 0) {
    if (ratel->point_fields == NULL) {
      PetscInt num_points;

      PetscCall(PetscCalloc1(model_data->num_point_fields, &point_fields));
      PetscCall(DMSwarmGetLocalSize(ratel->dm_solution, &num_points));
      for (PetscInt i = 0; i < model_data->num_point_fields; i++) {
        RatelCallCeed(ratel, CeedVectorCreate(ceed, num_points * model_data->point_field_sizes[i], &point_fields[i]));
      }
      ratel->point_fields      = point_fields;
      ratel->num_point_fields  = model_data->num_point_fields;
      ratel->point_field_sizes = model_data->point_field_sizes;
      ratel->point_field_names = model_data->point_field_names;
      // -- Point field vectors
      for (PetscInt i = 0; i < ratel->num_point_fields; i++) {
        PetscScalar *field_arr;
        PetscCall(DMSwarmGetField(ratel->dm_solution, ratel->point_field_names[i], NULL, NULL, (void **)&field_arr));
        RatelCallCeed(ratel, CeedVectorSetArray(point_fields[i], CEED_MEM_HOST, CEED_USE_POINTER, field_arr));
        PetscCall(DMSwarmRestoreField(ratel->dm_solution, ratel->point_field_names[i], NULL, NULL, (void **)&field_arr));
      }
    } else {
      PetscCheck(ratel->num_point_fields == model_data->num_point_fields, ratel->comm, PETSC_ERR_SUP, "Number of point fields mismatch");
      for (PetscInt i = 0; i < model_data->num_point_fields; i++) {
        PetscCheck(ratel->point_field_sizes[i] == model_data->point_field_sizes[i], ratel->comm, PETSC_ERR_SUP, "Point field size mismatch");
        PetscCheck(!strcmp(ratel->point_field_names[i], model_data->point_field_names[i]), ratel->comm, PETSC_ERR_SUP, "Point field size mismatch");
      }
      point_fields = ratel->point_fields;
    }
    PetscCall(PetscCalloc1(model_data->num_point_fields, &restrictions_point_fields));
    for (PetscInt i = 0; i < model_data->num_point_fields; i++) {
      PetscCall(RatelMPMContextCeedElemRestrictionCreateAtPoints(mpm, model_data->point_field_sizes[i], &restrictions_point_fields[i]));
    }
  }
  // -- Basis
  PetscCall(PetscCalloc1(num_active_fields, &bases_u));
  for (PetscInt i = 0; i < num_active_fields; i++) {
    PetscCall(RatelDMPlexCeedBasisCreate(ratel, dm_mesh, domain_label, domain_values[0], height_cell, i, &bases_u[i]));
  }
  // -- Restrictions
  PetscCall(PetscCalloc1(num_active_fields, &restrictions_u));
  for (PetscInt i = 0; i < num_active_fields; i++) {
    PetscCall(RatelDMPlexCeedElemRestrictionCreate(ratel, dm_mesh, domain_label, domain_values[0], height_cell, i, &restrictions_u[i]));
  }

  // Local residual evaluator
  {
    CeedQFunction qf_residual_u;
    CeedOperator  sub_op_residual_u;

    PetscCall(RatelDebug(ratel, "---- Setting up local residual evaluator, u term"));

    // -- QFunction
    RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, model_data->residual_u, model_data->residual_u_loc, &qf_residual_u));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_residual_u, "q data", q_data_size, CEED_EVAL_NONE));
    if (model_data->num_comp_state > 0) {
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_residual_u, "model state", model_data->num_comp_state, CEED_EVAL_NONE));
      RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_residual_u, "current model state", model_data->num_comp_state, CEED_EVAL_NONE));
    }
    if (model_data->num_comp_stored_u > 0) {
      RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_residual_u, "stored values u", model_data->num_comp_stored_u, CEED_EVAL_NONE));
    }
    {
      CeedInt eval_mode_index = 0;

      for (PetscInt i = 0; i < num_active_fields; i++) {
        for (PetscInt j = 0; j < active_field_num_eval_modes[i]; j++) {
          char         field_name[PETSC_MAX_PATH_LEN];
          CeedEvalMode eval_mode       = active_field_eval_modes[eval_mode_index++];
          CeedInt      quadrature_size = GetCeedQuadratureSize(eval_mode, dim, active_field_sizes[i]);

          PetscCheck(quadrature_size > 0, ratel->comm, PETSC_ERR_SUP, "Eval mode %s not supported", CeedEvalModes[eval_mode]);
          PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "du/dX" : "u", i));
          RatelCallCeed(ratel, CeedQFunctionAddInput(qf_residual_u, field_name, quadrature_size, eval_mode));
          PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "dv/dX" : "v", i));
          RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_residual_u, field_name, quadrature_size, eval_mode));
        }
      }
    }
    for (PetscInt i = 0; i < model_data->num_point_fields; i++) {
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_residual_u, model_data->point_field_names[i], model_data->point_field_sizes[i], CEED_EVAL_NONE));
    }
    RatelCallCeed(ratel, CeedQFunctionSetContext(qf_residual_u, material->ctx_params));
    RatelCallCeed(ratel, CeedQFunctionSetContextWritable(qf_residual_u, PETSC_FALSE));

    // -- Operator
    RatelCallCeed(ratel, CeedOperatorCreateAtPoints(ceed, qf_residual_u, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &sub_op_residual_u));
    PetscCall(RatelMaterialSetOperatorName(material, "volumetric term", sub_op_residual_u));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_residual_u, "q data", restriction_q_data, CEED_BASIS_NONE, q_data));
    if (model_data->num_comp_state > 0) {
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_residual_u, "model state", restriction_state_values, CEED_BASIS_NONE, state_values));
      RatelCallCeed(ratel,
                    CeedOperatorSetField(sub_op_residual_u, "current model state", restriction_state_values, CEED_BASIS_NONE, state_values_current));
    }
    if (model_data->num_comp_stored_u > 0) {
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_residual_u, "stored values u", restriction_stored_values_u, CEED_BASIS_NONE, stored_values_u));
    }
    {
      CeedInt eval_mode_index = 0;

      for (PetscInt i = 0; i < num_active_fields; i++) {
        for (PetscInt j = 0; j < active_field_num_eval_modes[i]; j++) {
          char         field_name[PETSC_MAX_PATH_LEN];
          CeedEvalMode eval_mode = active_field_eval_modes[eval_mode_index++];

          PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "du/dX" : "u", i));
          RatelCallCeed(ratel, CeedOperatorSetField(sub_op_residual_u, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
          PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "dv/dX" : "v", i));
          RatelCallCeed(ratel, CeedOperatorSetField(sub_op_residual_u, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
        }
      }
    }
    for (PetscInt i = 0; i < model_data->num_point_fields; i++) {
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_residual_u, model_data->point_field_names[i], restrictions_point_fields[i], CEED_BASIS_NONE,
                                                point_fields[i]));
    }
    PetscCall(RatelMPMContextCeedOperatorSetPoints(mpm, sub_op_residual_u));
    // -- Add to composite operator
    RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_residual_u, sub_op_residual_u));
    if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_residual_u, stdout));
    // -- Cleanup
    RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_residual_u));
    RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_residual_u));
  }

  if (material->model_data->has_ut_term) {
    ratel->has_ut_term                               = PETSC_TRUE;
    CeedVector          stored_values_ut             = NULL;
    CeedElemRestriction restriction_stored_values_ut = NULL;
    CeedQFunction       qf_residual_ut;
    CeedOperator        sub_op_residual_ut;

    PetscCall(RatelDebug(ratel, "---- Setting up local residual evaluator, u_t term"));

    // -- Stored data in residual_ut at quadrature points
    if (model_data->num_comp_stored_ut > 0) {
      PetscCall(RatelMPMContextCeedElemRestrictionCreateAtPoints(mpm, model_data->num_comp_stored_ut, &restriction_stored_values_ut));
      RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_stored_values_ut, &stored_values_ut, NULL));
      // Need to initialize to 0.0
      RatelCallCeed(ratel, CeedVectorSetValue(stored_values_ut, 0.0));
    }

    // -- QFunction
    RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, model_data->residual_ut, model_data->residual_ut_loc, &qf_residual_ut));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_residual_ut, "q data", q_data_size, CEED_EVAL_NONE));
    if (model_data->pass_stored_u_to_ut) {
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_residual_ut, "stored values u", model_data->num_comp_stored_u, CEED_EVAL_NONE));
    }
    if (model_data->num_comp_stored_ut > 0) {
      RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_residual_ut, "stored values ut", model_data->num_comp_stored_ut, CEED_EVAL_NONE));
    }
    {
      CeedInt eval_mode_index = 0;

      for (PetscInt i = 0; i < num_active_fields; i++) {
        for (PetscInt j = 0; j < ut_field_num_eval_modes[i]; j++) {
          char         field_name[PETSC_MAX_PATH_LEN];
          CeedEvalMode eval_mode       = ut_field_eval_modes[eval_mode_index++];
          CeedInt      quadrature_size = GetCeedQuadratureSize(eval_mode, dim, active_field_sizes[i]);

          PetscCheck(quadrature_size > 0, ratel->comm, PETSC_ERR_SUP, "Eval mode %s not supported", CeedEvalModes[eval_mode]);
          PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "du_t/dX" : "u_t", i));
          RatelCallCeed(ratel, CeedQFunctionAddInput(qf_residual_ut, field_name, quadrature_size, eval_mode));
          PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "dv_t/dX" : "v_t", i));
          RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_residual_ut, field_name, quadrature_size, eval_mode));
        }
      }
    }
    for (PetscInt i = 0; i < model_data->num_point_fields; i++) {
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_residual_ut, model_data->point_field_names[i], model_data->point_field_sizes[i], CEED_EVAL_NONE));
    }
    RatelCallCeed(ratel, CeedQFunctionSetContext(qf_residual_ut, material->ctx_params));
    RatelCallCeed(ratel, CeedQFunctionSetContextWritable(qf_residual_ut, PETSC_FALSE));
    // -- Operator
    RatelCallCeed(ratel, CeedOperatorCreateAtPoints(ceed, qf_residual_ut, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &sub_op_residual_ut));
    PetscCall(RatelMaterialSetOperatorName(material, "volumetric term", sub_op_residual_ut));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_residual_ut, "q data", restriction_q_data, CEED_BASIS_NONE, q_data));
    if (model_data->pass_stored_u_to_ut) {
      RatelCallCeed(ratel,
                    CeedOperatorSetField(sub_op_residual_ut, "stored values u", restriction_stored_values_u, CEED_BASIS_NONE, stored_values_u));
    }
    if (model_data->num_comp_stored_ut > 0) {
      RatelCallCeed(ratel,
                    CeedOperatorSetField(sub_op_residual_ut, "stored values ut", restriction_stored_values_ut, CEED_BASIS_NONE, stored_values_ut));
    }
    {
      CeedInt eval_mode_index = 0;

      for (PetscInt i = 0; i < num_active_fields; i++) {
        for (PetscInt j = 0; j < ut_field_num_eval_modes[i]; j++) {
          char         field_name[PETSC_MAX_PATH_LEN];
          CeedEvalMode eval_mode = ut_field_eval_modes[eval_mode_index++];

          PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "du_t/dX" : "u_t", i));
          RatelCallCeed(ratel, CeedOperatorSetField(sub_op_residual_ut, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
          PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "dv_t/dX" : "v_t", i));
          RatelCallCeed(ratel, CeedOperatorSetField(sub_op_residual_ut, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
        }
      }
    }
    for (PetscInt i = 0; i < model_data->num_point_fields; i++) {
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_residual_ut, model_data->point_field_names[i], restrictions_point_fields[i], CEED_BASIS_NONE,
                                                point_fields[i]));
    }
    PetscCall(RatelMPMContextCeedOperatorSetPoints(mpm, sub_op_residual_ut));
    // -- Add to composite operator
    RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_residual_ut, sub_op_residual_ut));
    if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_residual_ut, stdout));
    // -- Cleanup
    RatelCallCeed(ratel, CeedVectorDestroy(&stored_values_ut));
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_stored_values_ut));
    RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_residual_ut));
    RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_residual_ut));
  }

  // Cleanup
  PetscCall(DMDestroy(&dm_mesh));
  RatelCallCeed(ratel, CeedVectorDestroy(&q_data));
  RatelCallCeed(ratel, CeedVectorDestroy(&stored_values_u));
  RatelCallCeed(ratel, CeedVectorDestroy(&state_values));
  RatelCallCeed(ratel, CeedVectorDestroy(&state_values_current));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_q_data));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_stored_values_u));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_state_values));
  for (PetscInt i = 0; i < num_active_fields; i++) {
    RatelCallCeed(ratel, CeedBasisDestroy(&bases_u[i]));
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restrictions_u[i]));
  }
  PetscCall(PetscFree(bases_u));
  PetscCall(PetscFree(restrictions_u));
  for (PetscInt i = 0; i < model_data->num_point_fields; i++) {
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restrictions_point_fields[i]));
  }
  PetscCall(PetscFree(restrictions_point_fields));
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Residual Suboperators MPM Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup Jacobian `CeedOperator` for `RatelMaterial`.

  Collective across MPI processes.

  @param[in]   material        `RatelMaterial` context
  @param[out]  op_residual_u   Composite residual u term `CeedOperator`
  @param[out]  op_residual_ut  Composite residual ut term `CeedOperator`
  @param[out]  op_jacobian     Composite Jacobian `CeedOperator` to add `RatelMaterial` suboperator

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupJacobianSuboperator_MPM(RatelMaterial material, CeedOperator op_residual_u, CeedOperator op_residual_ut,
                                                         CeedOperator op_jacobian) {
  Ratel                ratel      = material->ratel;
  RatelModelData       model_data = material->model_data;
  RatelMPMContext      mpm;
  Ceed                 ceed = ratel->ceed;
  DM                   dm_mesh;
  CeedInt              num_active_fields, num_point_fields, q_data_size = model_data->q_data_volume_size;
  const CeedInt       *active_field_sizes, *active_field_num_eval_modes = model_data->active_field_num_eval_modes;
  const CeedEvalMode  *active_field_eval_modes = model_data->active_field_eval_modes;
  PetscInt             dim;
  CeedVector           q_data = NULL, stored_values_u = NULL, stored_values_ut = NULL, state_values = NULL, *point_fields = NULL;
  CeedElemRestriction *restrictions_u = NULL, restriction_q_data = NULL, restriction_stored_values_u = NULL, restriction_stored_values_ut = NULL,
                      restriction_state_values = NULL, *restrictions_point_fields = NULL;
  CeedBasis *bases_u = NULL;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Jacobian Suboperator MPM"));

  // DM information
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_mesh));
  PetscCall(DMGetDimension(dm_mesh, &dim));

  // Active fields
  PetscCall(RatelMaterialGetActiveFieldSizes(material, &num_active_fields, &active_field_sizes));

  // libCEED objects
  // -- MPM context
  PetscCall(RatelMaterialGetMPMContext(material, &mpm));
  // -- Solution objects from suboperator
  PetscCall(RatelMaterialGetSolutionData(material, op_residual_u, &num_active_fields, &restrictions_u, &bases_u));
  // -- QData
  PetscCall(RatelMaterialGetVolumeQData(material, &restriction_q_data, &q_data));
  // -- Stored data in residual_u at quadrature points
  PetscCall(RatelMaterialGetStoredDataU(material, op_residual_u, &restriction_stored_values_u, &stored_values_u));
  // -- Stored data in residual_ut at quadrature points
  if (material->model_data->has_ut_term) {
    PetscCall(RatelMaterialGetStoredDataUt(material, op_residual_ut, &restriction_stored_values_ut, &stored_values_ut));
  }
  // -- State data at quadrature points
  PetscCall(RatelMaterialGetStateData(material, op_residual_u, &restriction_state_values, &state_values));
  // -- Point field data
  PetscCall(RatelMaterialGetPointData(material, op_residual_u, &num_point_fields, &restrictions_point_fields, &point_fields));

  // Jacobian evaluator
  {
    CeedQFunction qf_jacobian;
    CeedOperator  sub_op_jacobian;

    PetscCall(RatelDebug(ratel, "---- Setting up Jacobian evaluator"));

    // -- QFunction
    RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, model_data->jacobian, model_data->jacobian_loc, &qf_jacobian));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_jacobian, "q data", q_data_size, CEED_EVAL_NONE));
    if (model_data->num_comp_state > 0) {
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_jacobian, "model state", model_data->num_comp_state, CEED_EVAL_NONE));
    }
    if (model_data->num_comp_stored_u > 0) {
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_jacobian, "stored values u", model_data->num_comp_stored_u, CEED_EVAL_NONE));
    }
    if (model_data->num_comp_stored_ut > 0) {
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_jacobian, "stored values ut", model_data->num_comp_stored_ut, CEED_EVAL_NONE));
    }
    {
      CeedInt eval_mode_index = 0;

      for (PetscInt i = 0; i < num_active_fields; i++) {
        for (PetscInt j = 0; j < active_field_num_eval_modes[i]; j++) {
          char         field_name[PETSC_MAX_PATH_LEN];
          CeedEvalMode eval_mode       = active_field_eval_modes[eval_mode_index++];
          CeedInt      quadrature_size = GetCeedQuadratureSize(eval_mode, dim, active_field_sizes[i]);

          PetscCheck(quadrature_size > 0, ratel->comm, PETSC_ERR_SUP, "Eval mode %s not supported", CeedEvalModes[eval_mode]);
          PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "d(du)/dX" : "du", i));
          RatelCallCeed(ratel, CeedQFunctionAddInput(qf_jacobian, field_name, quadrature_size, eval_mode));
          PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "d(dv)/dX" : "dv", i));
          RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_jacobian, field_name, quadrature_size, eval_mode));
        }
      }
    }
    for (PetscInt i = 0; i < num_point_fields; i++) {
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_jacobian, model_data->point_field_names[i], model_data->point_field_sizes[i], CEED_EVAL_NONE));
    }
    RatelCallCeed(ratel, CeedQFunctionSetContext(qf_jacobian, material->ctx_params));
    RatelCallCeed(ratel, CeedQFunctionSetContextWritable(qf_jacobian, PETSC_FALSE));
    // -- Operator
    RatelCallCeed(ratel, CeedOperatorCreateAtPoints(ceed, qf_jacobian, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &sub_op_jacobian));
    PetscCall(RatelMaterialSetOperatorName(material, "volumetric term", sub_op_jacobian));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_jacobian, "q data", restriction_q_data, CEED_BASIS_NONE, q_data));
    if (model_data->num_comp_state > 0) {
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_jacobian, "model state", restriction_state_values, CEED_BASIS_NONE, state_values));
    }
    if (model_data->num_comp_stored_u > 0) {
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_jacobian, "stored values u", restriction_stored_values_u, CEED_BASIS_NONE, stored_values_u));
    }
    if (model_data->num_comp_stored_ut > 0) {
      RatelCallCeed(ratel,
                    CeedOperatorSetField(sub_op_jacobian, "stored values ut", restriction_stored_values_ut, CEED_BASIS_NONE, stored_values_ut));
    }
    {
      CeedInt eval_mode_index = 0;

      for (PetscInt i = 0; i < num_active_fields; i++) {
        for (PetscInt j = 0; j < active_field_num_eval_modes[i]; j++) {
          char         field_name[PETSC_MAX_PATH_LEN];
          CeedEvalMode eval_mode = active_field_eval_modes[eval_mode_index++];

          PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "d(du)/dX" : "du", i));
          RatelCallCeed(ratel, CeedOperatorSetField(sub_op_jacobian, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
          PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "d(dv)/dX" : "dv", i));
          RatelCallCeed(ratel, CeedOperatorSetField(sub_op_jacobian, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
        }
      }
    }
    for (PetscInt i = 0; i < num_point_fields; i++) {
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_jacobian, model_data->point_field_names[i], restrictions_point_fields[i], CEED_BASIS_NONE,
                                                point_fields[i]));
    }
    // -- FLOPs counting
    CeedSize flops = model_data->flops_qf_jacobian_u;
    RatelCallCeed(ratel, CeedQFunctionSetUserFlopsEstimate(qf_jacobian, flops));
    // -- Set points
    PetscCall(RatelMPMContextCeedOperatorSetPoints(mpm, sub_op_jacobian));
    // -- Add to composite operator
    RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_jacobian, sub_op_jacobian));
    if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_jacobian, stdout));
    // -- Cleanup
    RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_jacobian));
    RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_jacobian));
  }

  // Cleanup
  PetscCall(DMDestroy(&dm_mesh));
  RatelCallCeed(ratel, CeedVectorDestroy(&q_data));
  RatelCallCeed(ratel, CeedVectorDestroy(&stored_values_u));
  RatelCallCeed(ratel, CeedVectorDestroy(&stored_values_ut));
  RatelCallCeed(ratel, CeedVectorDestroy(&state_values));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_q_data));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_stored_values_u));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_stored_values_ut));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_state_values));
  for (PetscInt i = 0; i < num_active_fields; i++) {
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restrictions_u[i]));
    RatelCallCeed(ratel, CeedBasisDestroy(&bases_u[i]));
  }
  PetscCall(PetscFree(restrictions_u));
  PetscCall(PetscFree(bases_u));
  for (PetscInt i = 0; i < num_point_fields; i++) {
    RatelCallCeed(ratel, CeedVectorDestroy(&point_fields[i]));
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restrictions_point_fields[i]));
  }
  PetscCall(PetscFree(point_fields));
  PetscCall(PetscFree(restrictions_point_fields));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Jacobian Suboperator MPM Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup Block Preconditioner CeedOperator for RatelMaterial

  @param[in]   material           RatelMaterial context
  @param[in]   dm                 DM for the given field
  @param[in]   field              Field that we wan to apply pc
  @param[out]  op_jacobian_block  CeedOperator created for the given field

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupJacobianBlockSuboperator_MPM(RatelMaterial material, DM dm, PetscInt field, CeedOperator op_jacobian_block) {
  Ratel               ratel      = material->ratel;
  RatelModelData      model_data = material->model_data;
  RatelMPMContext     mpm;
  DM                  dm_mesh;
  DMLabel             domain_label;
  PetscInt            dim, *domain_values;
  const CeedInt      *active_field_sizes;
  const char         *volume_label_name;
  CeedInt             num_point_fields, q_data_size = model_data->q_data_volume_size;
  Ceed                ceed                    = ratel->ceed;
  const CeedEvalMode *active_field_eval_modes = model_data->active_field_eval_modes;
  CeedVector          q_data = NULL, *point_fields = NULL;
  CeedBasis           basis;
  CeedElemRestriction restriction, restriction_q_data = NULL, *restrictions_point_fields = NULL;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Jacobian Block Suboperator MPM"));

  // DM information
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_mesh));
  PetscCall(DMGetDimension(dm_mesh, &dim));
  PetscCall(RatelMaterialGetVolumeLabelName(material, &volume_label_name));
  PetscCall(DMGetLabel(dm_mesh, volume_label_name, &domain_label));
  PetscCall(RatelMaterialGetVolumeLabelValues(material, NULL, &domain_values));

  // Active fields
  PetscCall(RatelMaterialGetActiveFieldSizes(material, NULL, &active_field_sizes));

  // libCEED objects
  // -- MPM context
  PetscCall(RatelMaterialGetMPMContext(material, &mpm));
  // -- Solution basis
  PetscCall(RatelDebug(ratel, "---- Solution bases"));
  PetscCall(RatelDMPlexCeedBasisCreate(ratel, dm_mesh, domain_label, domain_values[0], 0, field, &basis));

  // -- Solution restriction
  PetscCall(RatelDebug(ratel, "---- Solution restrictions"));
  PetscCall(RatelDMPlexCeedElemRestrictionCreate(ratel, dm, domain_label, domain_values[0], 0, 0, &restriction));

  // -- QData
  PetscCall(RatelMaterialGetVolumeQData(material, &restriction_q_data, &q_data));

  // -- Point field data
  {
    CeedOperator op_residual_u;

    PetscCall(CeedEvaluatorGetCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
    PetscCall(RatelMaterialGetPointData(material, op_residual_u, &num_point_fields, &restrictions_point_fields, &point_fields));
    PetscCall(CeedEvaluatorRestoreCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
  }

  {
    CeedQFunction qf_jacobian_block;
    CeedOperator  sub_op_jacobian_block, op_jacobian_pre, jacobian_pre_sub_op;

    // -- Get suboperators
    PetscCall(MatCeedGetCeedOperators(ratel->mat_jacobian_pre, &op_jacobian_pre, NULL));
    {
      PetscInt      sub_op_index = material->jacobian_indices[0];
      CeedOperator *sub_ops;

      RatelCallCeed(ratel, CeedCompositeOperatorGetSubList(op_jacobian_pre, &sub_ops));
      jacobian_pre_sub_op = sub_ops[sub_op_index];
    }

    // -- QFunction
    RatelCallCeed(ratel,
                  CeedQFunctionCreateInterior(ceed, 1, model_data->jacobian_block[field], model_data->jacobian_block_loc[field], &qf_jacobian_block));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_jacobian_block, "q data", q_data_size, CEED_EVAL_NONE));
    if (model_data->num_comp_state > 0) {
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_jacobian_block, "model state", model_data->num_comp_state, CEED_EVAL_NONE));
    }
    if (model_data->num_comp_stored_u > 0) {
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_jacobian_block, "stored values u", model_data->num_comp_stored_u, CEED_EVAL_NONE));
    }
    if (model_data->num_comp_stored_ut > 0) {
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_jacobian_block, "stored values ut", model_data->num_comp_stored_ut, CEED_EVAL_NONE));
    }
    {
      char         field_name[PETSC_MAX_PATH_LEN];
      CeedEvalMode eval_mode       = active_field_eval_modes[field];
      CeedInt      quadrature_size = GetCeedQuadratureSize(eval_mode, dim, active_field_sizes[field]);

      PetscCheck(quadrature_size > 0, ratel->comm, PETSC_ERR_SUP, "Eval mode %s not supported", CeedEvalModes[eval_mode]);
      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "d(du)/dX" : "du", field));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_jacobian_block, field_name, quadrature_size, eval_mode));
      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "d(dv)/dX" : "dv", field));
      RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_jacobian_block, field_name, quadrature_size, eval_mode));
    }
    for (PetscInt i = 0; i < num_point_fields; i++) {
      RatelCallCeed(ratel,
                    CeedQFunctionAddInput(qf_jacobian_block, model_data->point_field_names[i], model_data->point_field_sizes[i], CEED_EVAL_NONE));
    }
    // -- Add context
    {
      CeedQFunctionContext ctx_jacobian_pre = NULL;

      RatelCallCeed(ratel, CeedOperatorGetContext(jacobian_pre_sub_op, &ctx_jacobian_pre));
      RatelCallCeed(ratel, CeedQFunctionSetContext(qf_jacobian_block, ctx_jacobian_pre));
      RatelCallCeed(ratel, CeedQFunctionSetContextWritable(qf_jacobian_block, PETSC_FALSE));
      RatelCallCeed(ratel, CeedQFunctionContextDestroy(&ctx_jacobian_pre));
    }

    // -- Operator
    RatelCallCeed(ratel, CeedOperatorCreateAtPoints(ceed, qf_jacobian_block, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &sub_op_jacobian_block));
    PetscCall(RatelMaterialSetOperatorName(material, "volumetric term", sub_op_jacobian_block));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_jacobian_block, "q data", restriction_q_data, CEED_BASIS_NONE, q_data));
    if (model_data->num_comp_state > 0) {
      const char         *name = "model state";
      CeedVector          state_values;
      CeedElemRestriction restriction_state_values;
      CeedOperator        op_residual_u;

      PetscCall(CeedEvaluatorGetCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
      PetscCall(RatelMaterialGetStateData(material, op_residual_u, &restriction_state_values, &state_values));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_jacobian_block, name, restriction_state_values, CEED_BASIS_NONE, state_values));
      RatelCallCeed(ratel, CeedVectorDestroy(&state_values));
      RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_state_values));
      PetscCall(CeedEvaluatorRestoreCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
    }
    if (model_data->num_comp_stored_u > 0) {
      const char         *name = "stored values u";
      CeedVector          stored_values_u;
      CeedElemRestriction restriction_stored_values_u;
      CeedOperator        op_residual_u;

      PetscCall(CeedEvaluatorGetCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
      PetscCall(RatelMaterialGetStoredDataU(material, op_residual_u, &restriction_stored_values_u, &stored_values_u));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_jacobian_block, name, restriction_stored_values_u, CEED_BASIS_NONE, stored_values_u));
      RatelCallCeed(ratel, CeedVectorDestroy(&stored_values_u));
      RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_stored_values_u));
      PetscCall(CeedEvaluatorRestoreCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
    }
    if (model_data->num_comp_stored_ut > 0) {
      const char         *name = "stored values ut";
      CeedVector          stored_values_ut;
      CeedElemRestriction restriction_stored_values_ut;
      CeedOperator        op_residual_ut;

      PetscCall(CeedEvaluatorGetCeedOperator(ratel->evaluator_residual_ut, &op_residual_ut));
      PetscCall(RatelMaterialGetStoredDataU(material, op_residual_ut, &restriction_stored_values_ut, &stored_values_ut));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_jacobian_block, name, restriction_stored_values_ut, CEED_BASIS_NONE, stored_values_ut));
      RatelCallCeed(ratel, CeedVectorDestroy(&stored_values_ut));
      RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_stored_values_ut));
      PetscCall(CeedEvaluatorRestoreCeedOperator(ratel->evaluator_residual_ut, &op_residual_ut));
    }
    {
      char         field_name[PETSC_MAX_PATH_LEN];
      CeedEvalMode eval_mode = active_field_eval_modes[field];

      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "d(du)/dX" : "du", field));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_jacobian_block, field_name, restriction, basis, CEED_VECTOR_ACTIVE));
      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "d(dv)/dX" : "dv", field));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_jacobian_block, field_name, restriction, basis, CEED_VECTOR_ACTIVE));
    }
    for (PetscInt i = 0; i < num_point_fields; i++) {
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_jacobian_block, model_data->point_field_names[i], restrictions_point_fields[i],
                                                CEED_BASIS_NONE, point_fields[i]));
    }
    // -- FLOPs counting
    CeedSize flops = model_data->flops_qf_jacobian_block[field];
    RatelCallCeed(ratel, CeedQFunctionSetUserFlopsEstimate(qf_jacobian_block, flops));
    PetscCall(RatelMPMContextCeedOperatorSetPoints(mpm, sub_op_jacobian_block));
    // -- Add to composite operators
    RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_jacobian_block, sub_op_jacobian_block));
    if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_jacobian_block, stdout));
    // -- Cleanup
    PetscCall(MatCeedRestoreCeedOperators(ratel->mat_jacobian_pre, &op_jacobian_pre, NULL));
    RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_jacobian_block));
    RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_jacobian_block));
  }
  // Cleanup
  PetscCall(DMDestroy(&dm_mesh));
  RatelCallCeed(ratel, CeedVectorDestroy(&q_data));
  RatelCallCeed(ratel, CeedBasisDestroy(&basis));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_q_data));
  for (PetscInt i = 0; i < num_point_fields; i++) {
    RatelCallCeed(ratel, CeedVectorDestroy(&point_fields[i]));
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restrictions_point_fields[i]));
  }
  PetscCall(PetscFree(point_fields));
  PetscCall(PetscFree(restrictions_point_fields));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Jacobian Block Suboperator MPM Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup MMS error `CeedOperator` for `RatelMaterial`

  @param[in]   material      `RatelMaterial` context
  @param[out]  op_mms_error  Composite MMS error `CeedOperator` to add `RatelMaterial` suboperator

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupMMSErrorSuboperator_MPM(RatelMaterial material, CeedOperator op_mms_error) {
  Ratel                ratel      = material->ratel;
  RatelModelData       model_data = material->model_data;
  DM                   dm_solution;
  const char          *volume_label_name;
  DMLabel              domain_label;
  PetscInt             dim, *domain_values;
  const PetscInt       height_cell = 0;
  const PetscScalar   *x_array;
  Vec                  X;
  Ceed                 ceed = ratel->ceed;
  CeedInt              num_active_fields, num_comp_x, q_data_size = model_data->q_data_volume_size;
  const CeedInt       *active_field_sizes;
  CeedVector           q_data         = NULL, x;
  CeedElemRestriction *restrictions_u = NULL, restriction_q_data = NULL, restriction_x;
  CeedBasis           *bases_u = NULL, basis_x;
  RatelMPMContext      mpm;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup MMS Error Suboperator MPM"));

  // DM information
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));
  PetscCall(DMGetDimension(dm_solution, &dim));
  num_comp_x = dim;
  PetscCall(RatelMaterialGetVolumeLabelValues(material, NULL, &domain_values));
  PetscCall(RatelMaterialGetVolumeLabelName(material, &volume_label_name));
  PetscCall(DMGetLabel(dm_solution, volume_label_name, &domain_label));

  // Active fields
  PetscCall(RatelMaterialGetActiveFieldSizes(material, &num_active_fields, &active_field_sizes));

  // MPM context
  PetscCall(RatelMaterialGetMPMContext(material, &mpm));

  // libCEED objects
  // -- Coordinate basis
  PetscCall(RatelDebug(ratel, "---- Coordinate basis"));
  PetscCall(RatelDMPlexCeedBasisCoordinateCreate(ratel, dm_solution, domain_label, domain_values[0], height_cell, &basis_x));
  // -- Coordinate restriction
  PetscCall(RatelDebug(ratel, "---- Coordinate restriction"));
  PetscCall(RatelDMPlexCeedElemRestrictionCoordinateCreate(ratel, dm_solution, domain_label, domain_values[0], height_cell, &restriction_x));
  // -- Solution objects from suboperator
  {
    CeedOperator op_residual_u;

    PetscCall(CeedEvaluatorGetCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
    PetscCall(RatelMaterialGetSolutionData(material, op_residual_u, &num_active_fields, &restrictions_u, &bases_u));
    PetscCall(CeedEvaluatorRestoreCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
  }
  // -- QData
  PetscCall(RatelMaterialGetVolumeQData(material, &restriction_q_data, &q_data));

  // Element coordinates
  PetscCall(RatelDebug(ratel, "---- Retrieving element coordinates"));
  PetscCall(DMGetCoordinatesLocal(dm_solution, &X));
  PetscCall(VecGetArrayRead(X, &x_array));
  RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_x, &x, NULL));
  RatelCallCeed(ratel, CeedVectorSetArray(x, CEED_MEM_HOST, CEED_COPY_VALUES, (CeedScalar *)x_array));
  PetscCall(VecRestoreArrayRead(X, &x_array));

  // Error calculation, for MMS
  if (model_data->mms_error) {
    CeedQFunction qf_mms_error;
    CeedOperator  sub_op_mms_error;

    PetscCall(RatelDebug(ratel, "---- Setting up error computation for MMS"));

    // -- QFunction
    RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, model_data->mms_error, model_data->mms_error_loc, &qf_mms_error));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_mms_error, "q data", q_data_size, CEED_EVAL_NONE));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_mms_error, "x", num_comp_x, CEED_EVAL_INTERP));
    for (PetscInt i = 0; i < num_active_fields; i++) {
      char field_name[PETSC_MAX_PATH_LEN];

      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "u, field %" PetscInt_FMT, i));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_mms_error, field_name, active_field_sizes[i], CEED_EVAL_INTERP));
      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "l2 error, field %" PetscInt_FMT, i));
      RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_mms_error, field_name, active_field_sizes[i], CEED_EVAL_INTERP));
    }
    // -- QFunctionContext
    if (material->ctx_mms_params) {
      RatelCallCeed(ratel, CeedQFunctionSetContext(qf_mms_error, material->ctx_mms_params));
      RatelCallCeed(ratel, CeedQFunctionSetContextWritable(qf_mms_error, PETSC_FALSE));
    }

    // -- Operator
    RatelCallCeed(ratel, CeedOperatorCreateAtPoints(ceed, qf_mms_error, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &sub_op_mms_error));
    PetscCall(RatelMaterialSetOperatorName(material, "volumetric term", sub_op_mms_error));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_mms_error, "q data", restriction_q_data, CEED_BASIS_NONE, q_data));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_mms_error, "x", restriction_x, basis_x, x));
    for (PetscInt i = 0; i < num_active_fields; i++) {
      char field_name[PETSC_MAX_PATH_LEN];

      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "u, field %" PetscInt_FMT, i));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_mms_error, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "l2 error, field %" PetscInt_FMT, i));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_mms_error, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
    }
    // -- Set points
    PetscCall(RatelMPMContextCeedOperatorSetPoints(mpm, sub_op_mms_error));
    // -- Add to composite operator
    RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_mms_error, sub_op_mms_error));
    if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_mms_error, stdout));
    // -- Cleanup
    RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_mms_error));
    RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_mms_error));
  }

  // Cleanup
  PetscCall(DMDestroy(&dm_solution));
  RatelCallCeed(ratel, CeedVectorDestroy(&x));
  RatelCallCeed(ratel, CeedVectorDestroy(&q_data));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_q_data));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_x));
  for (PetscInt i = 0; i < num_active_fields; i++) {
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restrictions_u[i]));
    RatelCallCeed(ratel, CeedBasisDestroy(&bases_u[i]));
  }
  RatelCallCeed(ratel, CeedBasisDestroy(&basis_x));
  PetscCall(PetscFree(restrictions_u));
  PetscCall(PetscFree(bases_u));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup MMS Error Suboperator MPM Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Add point-wise forcing term to residual u term operator.

  Collective across MPI processes.

  @param[in]   material     `RatelMaterial` context
  @param[out]  op_residual  Composite residual u term `CeedOperator` to add `RatelMaterial` sub-operator

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupForcingSuboperator_MPM(RatelMaterial material, CeedOperator op_residual) {
  Ratel                ratel = material->ratel;
  DM                   dm_solution;
  RatelModelData       model_data = material->model_data;
  RatelMPMContext      mpm;
  DMLabel              domain_label;
  PetscInt             dim, *domain_values;
  CeedInt              num_active_fields, num_point_fields, q_data_size = model_data->q_data_volume_size, num_forcing = model_data->num_forcing;
  const CeedInt       *active_field_sizes = model_data->active_field_sizes;
  CeedVector           q_data = NULL, *point_fields = NULL;
  CeedBasis           *bases_u        = NULL;
  CeedElemRestriction *restrictions_u = NULL, restriction_q_data = NULL, *restrictions_point_fields = NULL;
  CeedQFunctionContext ctx_forcing = NULL;
  CeedQFunction        qf_forcing  = NULL;
  CeedOperator         sub_op_forcing;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Forcing Suboperator MPM"));

  // Check for support
  PetscCheck(ratel->method_type == RATEL_METHOD_MPM, ratel->comm, PETSC_ERR_SUP, "Cannot setup forcing operator at points for method '%s'",
             RatelMethodTypes[ratel->method_type]);

  // MPM context
  PetscCall(RatelMaterialGetMPMContext(material, &mpm));

  // Create the QFunction and Operator that computes the forcing term for the residual evaluator
  // DM information
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));
  PetscCall(DMGetDimension(dm_solution, &dim));
  PetscCall(RatelMaterialGetVolumeLabelValues(material, NULL, &domain_values));
  PetscCall(DMGetLabel(dm_solution, material->volume_label_name, &domain_label));

  // libCEED objects
  // -- Solution objects from sub-operator
  PetscCall(RatelMaterialGetSolutionData(material, op_residual, &num_active_fields, &restrictions_u, &bases_u));
  // -- Point field data from sub-operator
  PetscCall(RatelMaterialGetPointData(material, op_residual, &num_point_fields, &restrictions_point_fields, &point_fields));

  // -- QData
  PetscCall(RatelMaterialGetVolumeQData(material, &restriction_q_data, &q_data));

  // Forcing choice specific options
  switch (material->forcing_type) {
    // LCOV_EXCL_START
    case RATEL_FORCING_NONE:
      break;  // Excluded above
    // LCOV_EXCL_STOP
    case RATEL_FORCING_BODY: {
      RatelForcingBodyParams params_forcing;

      // -- Read in forcing vector(s)
      PetscCall(RatelMaterialForcingBodyDataFromOptions(material, &params_forcing));

      // -- QFunctionContext
      RatelCallCeed(ratel, CeedQFunctionContextCreate(ratel->ceed, &ctx_forcing));
      RatelCallCeed(ratel, CeedQFunctionContextSetData(ctx_forcing, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(params_forcing), &params_forcing));
      RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(ctx_forcing, "acceleration vectors", offsetof(RatelForcingBodyParams, acceleration),
                                                              3 * params_forcing.num_times, "vector describing traction in (x, y, z) direction"));
      RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(ctx_forcing, "times", offsetof(RatelForcingBodyParams, times), params_forcing.num_times,
                                                              "transition time values"));
      RatelCallCeed(ratel, CeedQFunctionContextRegisterInt32(ctx_forcing, "num times", offsetof(RatelForcingBodyParams, num_times), 1,
                                                             "number of time values and direction vectors"));
      RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(ctx_forcing, "time", offsetof(RatelForcingBodyParams, time), 1, "current solver time"));

      // -- QFunction
      if (num_point_fields > 0) {
        RatelCallCeed(ratel, CeedQFunctionCreateInterior(ratel->ceed, 1, MPMBodyForce, RatelQFunctionRelativePath(MPMBodyForce_loc), &qf_forcing));
        RatelCallCeed(ratel, CeedQFunctionAddInput(qf_forcing, "q data", q_data_size, CEED_EVAL_NONE));
        if (model_data->num_comp_state > 0) {
          RatelCallCeed(ratel, CeedQFunctionAddInput(qf_forcing, "model state", model_data->num_comp_state, CEED_EVAL_NONE));
        }
        RatelCallCeed(ratel, CeedQFunctionAddInput(qf_forcing, "rho", 1, CEED_EVAL_NONE));
      } else {
        RatelCallCeed(ratel, CeedQFunctionCreateInterior(ratel->ceed, 1, BodyForce, RatelQFunctionRelativePath(BodyForce_loc), &qf_forcing));
        RatelCallCeed(ratel, CeedQFunctionAddInput(qf_forcing, "q data", q_data_size, CEED_EVAL_NONE));
      }
      break;
    }
    case RATEL_FORCING_MMS: {
      // -- Check support
      PetscCheck(model_data->mms_forcing, ratel->comm, PETSC_ERR_SUP, "MMS forcing function for material '%s' not found", material->name);

      // -- QFunctionContext
      if (material->ctx_mms_params) {
        RatelCallCeed(ratel, CeedQFunctionContextReference(material->ctx_mms_params));
        ctx_forcing = material->ctx_mms_params;
      }

      // -- QFunction
      RatelCallCeed(ratel, CeedQFunctionCreateInterior(ratel->ceed, 1, model_data->mms_forcing, model_data->mms_forcing_loc, &qf_forcing));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_forcing, "q data", q_data_size, CEED_EVAL_NONE));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_forcing, "x", dim, CEED_EVAL_NONE));
      break;
    }
  }
  // -- Common QFunction output
  for (PetscInt i = 0; i < num_forcing; i++) {
    char field_name[PETSC_MAX_PATH_LEN];

    PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "force, field %" PetscInt_FMT, i));
    RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_forcing, field_name, active_field_sizes[i], CEED_EVAL_INTERP));
  }

  // -- QFunctionContext
  RatelCallCeed(ratel, CeedQFunctionSetContext(qf_forcing, ctx_forcing));
  RatelCallCeed(ratel, CeedQFunctionSetContextWritable(qf_forcing, PETSC_FALSE));
  if (ctx_forcing && ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedQFunctionContextView(ctx_forcing, stdout));
  RatelCallCeed(ratel, CeedQFunctionContextDestroy(&ctx_forcing));

  // -- Operator
  RatelCallCeed(ratel, CeedOperatorCreateAtPoints(ratel->ceed, qf_forcing, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &sub_op_forcing));
  PetscCall(RatelMaterialSetOperatorName(material, "forcing", sub_op_forcing));
  RatelCallCeed(ratel, CeedOperatorSetField(sub_op_forcing, "q data", restriction_q_data, CEED_BASIS_NONE, q_data));
  for (PetscInt i = 0; i < num_forcing; i++) {
    char field_name[PETSC_MAX_PATH_LEN];

    PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "force, field %" PetscInt_FMT, i));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_forcing, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
  }
  if (material->forcing_type == RATEL_FORCING_BODY && num_point_fields > 0) {
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_forcing, "rho", restrictions_point_fields[0], CEED_BASIS_NONE, point_fields[0]));
    if (model_data->num_comp_state > 0) {
      CeedElemRestriction restriction_state;
      CeedVector          state_values;

      // RatelMaterialGetStateData gets the current (output) model state, we want the previous one
      PetscCall(RatelMaterialGetInitialStateData(material, op_residual, &restriction_state, &state_values));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_forcing, "model state", restriction_state, CEED_BASIS_NONE, state_values));
      RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_state));
      RatelCallCeed(ratel, CeedVectorDestroy(&state_values));
    }
  }
  if (material->forcing_type == RATEL_FORCING_MMS) {
    CeedElemRestriction restriction_x;
    CeedVector          x_loc_points;

    // MPM context
    // libCEED objects
    // -- Coordinate restriction
    PetscCall(RatelMPMContextCeedElemRestrictionCreateAtPoints(mpm, dim, &restriction_x));
    // -- Coordinate vector
    RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_x, &x_loc_points, NULL));

    // -- Get point coordinates
    {
      const char  **field_coords;
      PetscScalar  *x_array;
      DMSwarmCellDM dm_cell;

      PetscCall(DMSwarmGetCellDMActive(ratel->dm_solution, &dm_cell));
      PetscCall(DMSwarmCellDMGetCoordinateFields(dm_cell, NULL, &field_coords));
      PetscCall(DMSwarmGetField(ratel->dm_solution, field_coords[0], NULL, NULL, (void **)&x_array));
      RatelCallCeed(ratel, CeedVectorSetArray(x_loc_points, CEED_MEM_HOST, CEED_COPY_VALUES, (CeedScalar *)x_array));
      PetscCall(DMSwarmRestoreField(ratel->dm_solution, field_coords[0], NULL, NULL, (void **)&x_array));
    }
    // -- Input field
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_forcing, "x", restriction_x, CEED_BASIS_NONE, x_loc_points));

    // Cleanup
    RatelCallCeed(ratel, CeedVectorDestroy(&x_loc_points));
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_x));
  }
  // Set points
  PetscCall(RatelMPMContextCeedOperatorSetPoints(mpm, sub_op_forcing));

  // Add to composite operator
  RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_residual, sub_op_forcing));
  if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_forcing, stdout));

  // Cleanup
  PetscCall(DMDestroy(&dm_solution));
  RatelCallCeed(ratel, CeedVectorDestroy(&q_data));
  for (PetscInt i = 0; i < num_active_fields; i++) {
    RatelCallCeed(ratel, CeedBasisDestroy(&bases_u[i]));
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restrictions_u[i]));
  }
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_q_data));
  for (PetscInt i = 0; i < num_point_fields; i++) {
    RatelCallCeed(ratel, CeedVectorDestroy(&point_fields[i]));
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restrictions_point_fields[i]));
  }
  RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_forcing));
  RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_forcing));
  PetscCall(PetscFree(restrictions_u));
  PetscCall(PetscFree(bases_u));
  PetscCall(PetscFree(point_fields));
  PetscCall(PetscFree(restrictions_point_fields));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Forcing Suboperator MPM Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Add point-wise forcing energy to external energy operator.

  Collective across MPI processes.

  @param[in]   material     `RatelMaterial` context
  @param[in]   dm_energy           `DM` for external energy computation
  @param[out]  op_external_energy  Composite external energy `CeedOperator` to add energy of body force sub-operators

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupForcingEnergySuboperator_MPM(RatelMaterial material, DM dm_energy, CeedOperator op_external_energy) {
  Ratel                ratel = material->ratel;
  DM                   dm_solution;
  RatelModelData       model_data = material->model_data;
  RatelMPMContext      mpm;
  DMLabel              domain_label;
  PetscInt             dim, *domain_values;
  const PetscInt       height_cell = 0;
  CeedInt              num_active_fields, num_comp_energy = 1, q_data_size = model_data->q_data_volume_size, num_forcing = model_data->num_forcing;
  const CeedInt       *active_field_sizes = model_data->active_field_sizes;
  CeedVector           q_data             = NULL;
  CeedBasis           *bases_u            = NULL, basis_energy;
  CeedElemRestriction *restrictions_u = NULL, restriction_q_data = NULL, restriction_energy;
  CeedQFunctionContext ctx_forcing       = NULL;
  CeedQFunction        qf_forcing_energy = NULL;
  CeedOperator         sub_op_forcing_energy;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Forcing Energy Suboperator MPM"));

  // Check for support
  PetscCheck(ratel->method_type == RATEL_METHOD_MPM, ratel->comm, PETSC_ERR_SUP, "Cannot setup forcing energy operator at points for method '%s'",
             RatelMethodTypes[ratel->method_type]);

  // MPM context
  PetscCall(RatelMaterialGetMPMContext(material, &mpm));

  // Create the QFunction and Operator that computes the forcing term for the residual evaluator
  // DM information
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));
  PetscCall(DMGetDimension(dm_solution, &dim));
  PetscCall(RatelMaterialGetVolumeLabelValues(material, NULL, &domain_values));
  PetscCall(DMGetLabel(dm_solution, material->volume_label_name, &domain_label));

  // libCEED objects
  // -- Energy basis
  PetscCall(RatelDebug(ratel, "------ Energy basis"));
  PetscCall(RatelDMPlexCeedBasisCreate(ratel, dm_energy, domain_label, domain_values[0], height_cell, 0, &basis_energy));
  // -- Energy restriction
  PetscCall(RatelDebug(ratel, "------ Energy restriction"));
  PetscCall(RatelDMPlexCeedElemRestrictionCreate(ratel, dm_energy, domain_label, domain_values[0], height_cell, 0, &restriction_energy));
  // -- Solution objects from sub-operator
  {
    CeedOperator op_residual_u;

    PetscCall(CeedEvaluatorGetCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
    PetscCall(RatelMaterialGetSolutionData(material, op_residual_u, &num_active_fields, &restrictions_u, &bases_u));
    PetscCall(CeedEvaluatorRestoreCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
  }
  // -- QData
  PetscCall(RatelMaterialGetVolumeQData(material, &restriction_q_data, &q_data));

  // Forcing choice specific options
  switch (material->forcing_type) {
    // LCOV_EXCL_START
    case RATEL_FORCING_NONE:
      break;  // Excluded above
    // LCOV_EXCL_STOP
    case RATEL_FORCING_BODY: {
      RatelForcingBodyParams params_forcing;

      // -- Read in forcing vector(s)
      PetscCall(RatelMaterialForcingBodyDataFromOptions(material, &params_forcing));

      // -- QFunctionContext
      RatelCallCeed(ratel, CeedQFunctionContextCreate(ratel->ceed, &ctx_forcing));
      RatelCallCeed(ratel, CeedQFunctionContextSetData(ctx_forcing, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(params_forcing), &params_forcing));
      RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(ctx_forcing, "acceleration vectors", offsetof(RatelForcingBodyParams, acceleration),
                                                              3 * params_forcing.num_times, "vector describing traction in (x, y, z) direction"));
      RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(ctx_forcing, "times", offsetof(RatelForcingBodyParams, times), params_forcing.num_times,
                                                              "transition time values"));
      RatelCallCeed(ratel, CeedQFunctionContextRegisterInt32(ctx_forcing, "num times", offsetof(RatelForcingBodyParams, num_times), 1,
                                                             "number of time values and direction vectors"));
      RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(ctx_forcing, "time", offsetof(RatelForcingBodyParams, time), 1, "current solver time"));

      // -- QFunction
      RatelCallCeed(
          ratel, CeedQFunctionCreateInterior(ratel->ceed, 1, BodyForceEnergy, RatelQFunctionRelativePath(BodyForceEnergy_loc), &qf_forcing_energy));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_forcing_energy, "q data", q_data_size, CEED_EVAL_NONE));
      break;
    }
    case RATEL_FORCING_MMS: {
      // -- Check support
      PetscCheck(model_data->mms_forcing, ratel->comm, PETSC_ERR_SUP, "MMS forcing function for material '%s' not found", material->name);

      // -- QFunctionContext
      if (material->ctx_mms_params) {
        RatelCallCeed(ratel, CeedQFunctionContextReference(material->ctx_mms_params));
        ctx_forcing = material->ctx_mms_params;
      }

      // -- QFunction
      RatelCallCeed(
          ratel, CeedQFunctionCreateInterior(ratel->ceed, 1, model_data->mms_forcing_energy, model_data->mms_forcing_energy_loc, &qf_forcing_energy));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_forcing_energy, "q data", q_data_size, CEED_EVAL_NONE));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_forcing_energy, "x", dim, CEED_EVAL_NONE));
      break;
    }
  }
  // -- Common QFunction input/output
  for (PetscInt i = 0; i < num_forcing; i++) {
    char field_name[PETSC_MAX_PATH_LEN];

    PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "force energy, field %" PetscInt_FMT, i));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_forcing_energy, field_name, active_field_sizes[i], CEED_EVAL_INTERP));
  }
  RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_forcing_energy, "external energy", num_comp_energy, CEED_EVAL_INTERP));

  // -- QFunctionContext
  RatelCallCeed(ratel, CeedQFunctionSetContext(qf_forcing_energy, ctx_forcing));
  RatelCallCeed(ratel, CeedQFunctionSetContextWritable(qf_forcing_energy, PETSC_FALSE));
  if (ctx_forcing && ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedQFunctionContextView(ctx_forcing, stdout));
  RatelCallCeed(ratel, CeedQFunctionContextDestroy(&ctx_forcing));

  // -- Operator
  RatelCallCeed(ratel, CeedOperatorCreateAtPoints(ratel->ceed, qf_forcing_energy, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &sub_op_forcing_energy));
  PetscCall(RatelMaterialSetOperatorName(material, "forcing energy", sub_op_forcing_energy));
  RatelCallCeed(ratel, CeedOperatorSetField(sub_op_forcing_energy, "q data", restriction_q_data, CEED_BASIS_NONE, q_data));
  for (PetscInt i = 0; i < num_forcing; i++) {
    char field_name[PETSC_MAX_PATH_LEN];

    PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "force energy, field %" PetscInt_FMT, i));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_forcing_energy, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
  }
  if (material->forcing_type == RATEL_FORCING_MMS) {
    CeedElemRestriction restriction_x;
    CeedVector          x_loc_points;

    // libCEED objects
    // -- Coordinate restriction
    PetscCall(RatelMPMContextCeedElemRestrictionCreateAtPoints(mpm, dim, &restriction_x));
    // -- Coordinate vector
    RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_x, &x_loc_points, NULL));

    // Get point coordinates
    {
      const char  **field_coords;
      PetscScalar  *x_array;
      DMSwarmCellDM dm_cell;

      PetscCall(DMSwarmGetCellDMActive(ratel->dm_solution, &dm_cell));
      PetscCall(DMSwarmCellDMGetCoordinateFields(dm_cell, NULL, &field_coords));
      PetscCall(DMSwarmGetField(ratel->dm_solution, field_coords[0], NULL, NULL, (void **)&x_array));
      RatelCallCeed(ratel, CeedVectorSetArray(x_loc_points, CEED_MEM_HOST, CEED_COPY_VALUES, (CeedScalar *)x_array));
      PetscCall(DMSwarmRestoreField(ratel->dm_solution, field_coords[0], NULL, NULL, (void **)&x_array));
    }

    // -- Input field
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_forcing_energy, "x", restriction_x, CEED_BASIS_NONE, x_loc_points));

    // Cleanup
    RatelCallCeed(ratel, CeedVectorDestroy(&x_loc_points));
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_x));
  }
  // Set points/output
  PetscCall(RatelMPMContextCeedOperatorSetPoints(mpm, sub_op_forcing_energy));
  RatelCallCeed(ratel, CeedOperatorSetField(sub_op_forcing_energy, "external energy", restriction_energy, basis_energy, CEED_VECTOR_ACTIVE));

  // Add to composite operator
  RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_external_energy, sub_op_forcing_energy));
  if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_forcing_energy, stdout));

  // Cleanup
  PetscCall(DMDestroy(&dm_solution));
  RatelCallCeed(ratel, CeedVectorDestroy(&q_data));
  for (PetscInt i = 0; i < num_active_fields; i++) {
    RatelCallCeed(ratel, CeedBasisDestroy(&bases_u[i]));
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restrictions_u[i]));
  }
  RatelCallCeed(ratel, CeedBasisDestroy(&basis_energy));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_energy));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_q_data));
  RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_forcing_energy));
  RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_forcing_energy));
  PetscCall(PetscFree(restrictions_u));
  PetscCall(PetscFree(bases_u));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Forcing Energy Suboperator MPM Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup strain energy `CeedOperator` for `RatelMaterial`, possibly with material parameters at points.

  Collective across MPI processes.

  @param[in]   material          `RatelMaterial` context
  @param[in]   dm_energy         `DM` for strain energy computation
  @param[out]  op_strain_energy  Composite strain energy `CeedOperator` to add `RatelMaterial` suboperator

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupStrainEnergySuboperator_MPM(RatelMaterial material, DM dm_energy, CeedOperator op_strain_energy) {
  Ratel                ratel      = material->ratel;
  RatelModelData       model_data = material->model_data;
  RatelMPMContext      mpm;
  DM                   dm_solution;
  const char          *volume_label_name, **point_field_names;
  DMLabel              domain_label;
  PetscInt             dim, *domain_values;
  const PetscInt       height_cell = 0;
  Ceed                 ceed        = ratel->ceed;
  CeedInt              num_active_fields, num_point_fields, num_comp_energy = 1, q_data_size = model_data->q_data_volume_size;
  const CeedInt       *active_field_sizes, *point_field_sizes = NULL;
  const CeedInt       *active_field_num_eval_modes = model_data->active_field_num_eval_modes;
  const CeedEvalMode  *active_field_eval_modes     = model_data->active_field_eval_modes;
  CeedVector          *point_fields = NULL, q_data = NULL;
  CeedElemRestriction *restrictions_u = NULL, *restrictions_point_fields = NULL, restriction_q_data = NULL, restriction_energy;
  CeedBasis           *bases_u = NULL, basis_energy;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Strain Energy Suboperator MPM"));

  // DM information
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));
  PetscCall(DMGetDimension(dm_solution, &dim));
  PetscCall(RatelMaterialGetVolumeLabelValues(material, NULL, &domain_values));
  PetscCall(RatelMaterialGetVolumeLabelName(material, &volume_label_name));
  PetscCall(DMGetLabel(dm_solution, volume_label_name, &domain_label));

  // Active fields
  PetscCall(RatelMaterialGetActiveFieldSizes(material, &num_active_fields, &active_field_sizes));
  // Point fields
  PetscCall(RatelMaterialGetPointFields(material, &num_point_fields, &point_field_sizes, &point_field_names));

  // MPM context
  PetscCall(RatelMaterialGetMPMContext(material, &mpm));

  // libCEED objects
  // -- Energy basis
  PetscCall(RatelDebug(ratel, "------ Energy basis"));
  PetscCall(RatelDMPlexCeedBasisCreate(ratel, dm_energy, domain_label, domain_values[0], height_cell, 0, &basis_energy));
  // -- Energy restriction
  PetscCall(RatelDebug(ratel, "------ Energy restriction"));
  PetscCall(RatelDMPlexCeedElemRestrictionCreate(ratel, dm_energy, domain_label, domain_values[0], height_cell, 0, &restriction_energy));
  // -- Solution and point field objects from residual operator
  {
    CeedOperator op_residual_u;

    PetscCall(CeedEvaluatorGetCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
    PetscCall(RatelMaterialGetSolutionData(material, op_residual_u, &num_active_fields, &restrictions_u, &bases_u));
    PetscCall(RatelMaterialGetPointData(material, op_residual_u, &num_point_fields, &restrictions_point_fields, &point_fields));
    PetscCall(CeedEvaluatorRestoreCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
  }
  // -- QData
  PetscCall(RatelMaterialGetVolumeQData(material, &restriction_q_data, &q_data));

  // Local energy computation
  {
    CeedQFunction qf_energy;
    CeedOperator  sub_op_strain_energy;

    PetscCall(RatelDebug(ratel, "---- Setting up local energy computation"));

    // -- QFunction
    RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, model_data->strain_energy, model_data->strain_energy_loc, &qf_energy));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_energy, "q data", q_data_size, CEED_EVAL_NONE));
    if (model_data->num_comp_state > 0) {
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_energy, "model state", model_data->num_comp_state, CEED_EVAL_NONE));
    }
    {
      CeedInt eval_mode_index = 0;

      for (PetscInt i = 0; i < num_active_fields; i++) {
        for (PetscInt j = 0; j < active_field_num_eval_modes[i]; j++) {
          char         field_name[PETSC_MAX_PATH_LEN];
          CeedEvalMode eval_mode       = active_field_eval_modes[eval_mode_index++];
          CeedInt      quadrature_size = GetCeedQuadratureSize(eval_mode, dim, active_field_sizes[i]);

          PetscCheck(quadrature_size > 0, ratel->comm, PETSC_ERR_SUP, "Eval mode %s not supported", CeedEvalModes[eval_mode]);
          PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%su, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "d" : "", i));
          RatelCallCeed(ratel, CeedQFunctionAddInput(qf_energy, field_name, quadrature_size, eval_mode));
        }
      }
    }
    for (PetscInt i = 0; i < num_point_fields; i++) {
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_energy, point_field_names[i], point_field_sizes[i], CEED_EVAL_NONE));
    }
    RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_energy, "strain energy", num_comp_energy, CEED_EVAL_INTERP));
    RatelCallCeed(ratel, CeedQFunctionSetContext(qf_energy, material->ctx_params));
    RatelCallCeed(ratel, CeedQFunctionSetContextWritable(qf_energy, PETSC_FALSE));
    // -- Operator
    RatelCallCeed(ratel, CeedOperatorCreateAtPoints(ceed, qf_energy, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &sub_op_strain_energy));
    PetscCall(RatelMaterialSetOperatorName(material, "volumetric term", sub_op_strain_energy));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_strain_energy, "q data", restriction_q_data, CEED_BASIS_NONE, q_data));
    {
      CeedInt eval_mode_index = 0;

      for (PetscInt i = 0; i < num_active_fields; i++) {
        for (PetscInt j = 0; j < active_field_num_eval_modes[i]; j++) {
          char         field_name[PETSC_MAX_PATH_LEN];
          CeedEvalMode eval_mode = active_field_eval_modes[eval_mode_index++];

          PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%su, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "d" : "", i));
          RatelCallCeed(ratel, CeedOperatorSetField(sub_op_strain_energy, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
        }
      }
    }
    // Add state field components to the energy QFunction
    if (model_data->num_comp_state > 0) {
      const char         *name = "model state";
      CeedVector          state_values;
      CeedElemRestriction restriction_state_values;

      {
        CeedOperator op_residual_u;

        PetscCall(CeedEvaluatorGetCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
        PetscCall(RatelMaterialGetStateData(material, op_residual_u, &restriction_state_values, &state_values));
        PetscCall(CeedEvaluatorRestoreCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
      }
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_strain_energy, name, restriction_state_values, CEED_BASIS_NONE, state_values));
      RatelCallCeed(ratel, CeedVectorDestroy(&state_values));
      RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_state_values));
    }
    for (PetscInt i = 0; i < num_point_fields; i++) {
      RatelCallCeed(ratel,
                    CeedOperatorSetField(sub_op_strain_energy, point_field_names[i], restrictions_point_fields[i], CEED_BASIS_NONE, point_fields[i]));
      RatelCallCeed(ratel, CeedVectorDestroy(&point_fields[i]));
      RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restrictions_point_fields[i]));
    }
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_strain_energy, "strain energy", restriction_energy, basis_energy, CEED_VECTOR_ACTIVE));
    // -- Set points
    PetscCall(RatelMPMContextCeedOperatorSetPoints(mpm, sub_op_strain_energy));
    // -- Add to composite operator
    RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_strain_energy, sub_op_strain_energy));
    if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_strain_energy, stdout));
    // -- Cleanup
    RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_energy));
    RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_strain_energy));
  }

  // Cleanup
  PetscCall(DMDestroy(&dm_solution));
  RatelCallCeed(ratel, CeedVectorDestroy(&q_data));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_energy));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_q_data));
  for (PetscInt i = 0; i < num_active_fields; i++) {
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restrictions_u[i]));
    RatelCallCeed(ratel, CeedBasisDestroy(&bases_u[i]));
  }

  RatelCallCeed(ratel, CeedBasisDestroy(&basis_energy));
  PetscCall(PetscFree(restrictions_u));
  PetscCall(PetscFree(bases_u));
  PetscCall(PetscFree(point_fields));
  PetscCall(PetscFree(restrictions_point_fields));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Energy Suboperator MPM Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup diagnostic value `CeedOperator` for `RatelMaterial`, possibly with material parameters at points.

  Collective across MPI processes.

  @param[in]   material                 `RatelMaterial` context
  @param[in]   dm_projected_diagnostic  `DM` for projected diagnostic value computation
  @param[in]   dm_dual_diagnostic       `DM` for dual space diagnostic value computation
  @param[out]  op_mass_diagnostic       Composite diagnostic value projection `CeedOperator` to add `RatelMaterial` suboperator
  @param[out]  op_projected_diagnostic  Composite projected diagnostic value `CeedOperator` to add `RatelMaterial` suboperator
  @param[out]  op_dual_diagnostic       Composite dual space diagnostic value `CeedOperator` to add `RatelMaterial` suboperator
  @param[out]  op_dual_nodal_scale      Composite dual space nodal scale `CeedOperator` to add `RatelMaterial` suboperator

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupDiagnosticSuboperators_MPM(RatelMaterial material, DM dm_projected_diagnostic, DM dm_dual_diagnostic,
                                                            CeedOperator op_mass_diagnostic, CeedOperator op_projected_diagnostic,
                                                            CeedOperator op_dual_diagnostic, CeedOperator op_dual_nodal_scale) {
  Ratel           ratel      = material->ratel;
  RatelModelData  model_data = material->model_data;
  RatelMPMContext mpm;
  DM              dm_solution;
  const char     *volume_label_name, **point_field_names;
  DMLabel         domain_label;
  PetscInt        dim, *domain_values;
  const PetscInt  height_cell = 0;
  Ceed            ceed        = ratel->ceed;
  CeedInt         num_active_fields, num_point_fields, num_comp_projected_diagnostic = model_data->num_comp_projected_diagnostic,
                                               num_comp_dual_diagnostic = model_data->num_comp_dual_diagnostic,
                                               q_data_size              = model_data->q_data_volume_size;
  const CeedInt       *active_field_sizes, *point_field_sizes = NULL;
  CeedVector          *point_fields = NULL, q_data = NULL;
  CeedElemRestriction *restrictions_u = NULL, *restrictions_point_fields = NULL, restriction_q_data = NULL;
  CeedBasis           *bases_u = NULL;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Diagnostic Suboperator MPM"));

  // DM information
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));
  PetscCall(DMGetDimension(dm_solution, &dim));
  PetscCall(RatelMaterialGetVolumeLabelValues(material, NULL, &domain_values));
  PetscCall(RatelMaterialGetVolumeLabelName(material, &volume_label_name));
  PetscCall(DMGetLabel(dm_solution, volume_label_name, &domain_label));

  // MPM context
  PetscCall(RatelMaterialGetMPMContext(material, &mpm));

  // Active fields
  PetscCall(RatelMaterialGetActiveFieldSizes(material, &num_active_fields, &active_field_sizes));
  // Point fields
  PetscCall(RatelMaterialGetPointFields(material, &num_point_fields, &point_field_sizes, &point_field_names));

  // libCEED objects
  // -- Solution and point field objects from residual operator
  {
    CeedOperator op_residual_u;

    PetscCall(CeedEvaluatorGetCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
    PetscCall(RatelMaterialGetSolutionData(material, op_residual_u, &num_active_fields, &restrictions_u, &bases_u));
    PetscCall(RatelMaterialGetPointData(material, op_residual_u, &num_point_fields, &restrictions_point_fields, &point_fields));
    PetscCall(CeedEvaluatorRestoreCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
  }
  // -- QData
  PetscCall(RatelMaterialGetVolumeQData(material, &restriction_q_data, &q_data));

  // Diagnostic value computation, projected
  PetscCall(RatelDebug(ratel, "---- Setting up projected diagnostic value computation"));
  {
    CeedElemRestriction  restriction_diagnostic;
    CeedBasis            basis_diagnostic;
    CeedQFunctionContext ctx_mass_diagnostic;
    CeedQFunction        qf_mass_diagnostic, qf_diagnostic;
    CeedOperator         sub_op_mass_diagnostic, sub_op_diagnostic;

    // -- Diagnostic basis
    PetscCall(RatelDebug(ratel, "------ Diagnostics basis"));
    PetscCall(RatelDMPlexCeedBasisCreate(ratel, dm_projected_diagnostic, domain_label, domain_values[0], height_cell, 0, &basis_diagnostic));
    // -- Diagnostic data restriction
    PetscCall(RatelDebug(ratel, "------ Diagnostics restriction"));
    PetscCall(RatelDMPlexCeedElemRestrictionCreate(ratel, dm_projected_diagnostic, domain_label, domain_values[0], height_cell, 0,
                                                   &restriction_diagnostic));

    // Mass matrix
    // -- QFunction
    RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, Mass, RatelQFunctionRelativePath(Mass_loc), &qf_mass_diagnostic));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_mass_diagnostic, "q data", q_data_size, CEED_EVAL_NONE));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_mass_diagnostic, "u", num_comp_projected_diagnostic, CEED_EVAL_INTERP));
    RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_mass_diagnostic, "v", num_comp_projected_diagnostic, CEED_EVAL_INTERP));
    RatelCallCeed(ratel, CeedQFunctionContextCreate(ceed, &ctx_mass_diagnostic));
    RatelCallCeed(ratel, CeedQFunctionContextSetData(ctx_mass_diagnostic, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(num_comp_projected_diagnostic),
                                                     &num_comp_projected_diagnostic));
    RatelCallCeed(ratel, CeedQFunctionSetContext(qf_mass_diagnostic, ctx_mass_diagnostic));
    RatelCallCeed(ratel, CeedQFunctionSetContextWritable(qf_mass_diagnostic, PETSC_FALSE));
    RatelCallCeed(ratel, CeedQFunctionSetUserFlopsEstimate(qf_mass_diagnostic, num_comp_projected_diagnostic));
    // -- Operator
    RatelCallCeed(ratel, CeedOperatorCreateAtPoints(ceed, qf_mass_diagnostic, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &sub_op_mass_diagnostic));
    PetscCall(RatelMaterialSetOperatorName(material, "volumetric term", sub_op_mass_diagnostic));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_mass_diagnostic, "q data", restriction_q_data, CEED_BASIS_NONE, q_data));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_mass_diagnostic, "u", restriction_diagnostic, basis_diagnostic, CEED_VECTOR_ACTIVE));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_mass_diagnostic, "v", restriction_diagnostic, basis_diagnostic, CEED_VECTOR_ACTIVE));
    // -- Set points
    PetscCall(RatelMPMContextCeedOperatorSetPoints(mpm, sub_op_mass_diagnostic));
    // -- Add to composite operator
    RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_mass_diagnostic, sub_op_mass_diagnostic));
    if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_mass_diagnostic, stdout));
    // -- Cleanup
    RatelCallCeed(ratel, CeedQFunctionContextDestroy(&ctx_mass_diagnostic));
    RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_mass_diagnostic));
    RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_mass_diagnostic));

    // Diagnostic quantities, projected
    // -- QFunction
    RatelCallCeed(ratel,
                  CeedQFunctionCreateInterior(ceed, 1, model_data->projected_diagnostic, model_data->projected_diagnostic_loc, &qf_diagnostic));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_diagnostic, "q data", q_data_size, CEED_EVAL_NONE));
    if (model_data->num_comp_state > 0) {
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_diagnostic, "model state", model_data->num_comp_state, CEED_EVAL_NONE));
    }
    for (PetscInt i = 0; i < num_active_fields; i++) {
      char field_name[PETSC_MAX_PATH_LEN];

      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "u, field %" PetscInt_FMT, i));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_diagnostic, field_name, active_field_sizes[i], CEED_EVAL_INTERP));
      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "du/dX, field %" PetscInt_FMT, i));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_diagnostic, field_name, active_field_sizes[i] * dim, CEED_EVAL_GRAD));
    }
    for (PetscInt i = 0; i < num_point_fields; i++) {
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_diagnostic, point_field_names[i], point_field_sizes[i], CEED_EVAL_NONE));
    }
    RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_diagnostic, "diagnostic", num_comp_projected_diagnostic, CEED_EVAL_INTERP));
    RatelCallCeed(ratel, CeedQFunctionSetContext(qf_diagnostic, material->ctx_params));
    RatelCallCeed(ratel, CeedQFunctionSetContextWritable(qf_diagnostic, PETSC_FALSE));
    // -- Operator
    RatelCallCeed(ratel, CeedOperatorCreateAtPoints(ceed, qf_diagnostic, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &sub_op_diagnostic));
    PetscCall(RatelMaterialSetOperatorName(material, "volumetric term", sub_op_diagnostic));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_diagnostic, "q data", restriction_q_data, CEED_BASIS_NONE, q_data));
    // ---- Add active fields
    for (PetscInt i = 0; i < num_active_fields; i++) {
      char field_name[PETSC_MAX_PATH_LEN];

      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "u, field %" PetscInt_FMT, i));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_diagnostic, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "du/dX, field %" PetscInt_FMT, i));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_diagnostic, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
    }
    // ---- Add state field components to the projected diagnostics QFunction
    if (model_data->num_comp_state > 0) {
      const char         *name = "model state";
      CeedVector          state_values;
      CeedElemRestriction restriction_state_values;

      // -- State objects from residual operator
      {
        CeedOperator op_residual_u;

        PetscCall(CeedEvaluatorGetCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
        PetscCall(RatelMaterialGetStateData(material, op_residual_u, &restriction_state_values, &state_values));
        PetscCall(CeedEvaluatorRestoreCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
      }
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_diagnostic, name, restriction_state_values, CEED_BASIS_NONE, state_values));
      RatelCallCeed(ratel, CeedVectorDestroy(&state_values));
      RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_state_values));
    }
    // ---- Add point fields to the projected diagnostics QFunction
    for (PetscInt i = 0; i < num_point_fields; i++) {
      RatelCallCeed(ratel,
                    CeedOperatorSetField(sub_op_diagnostic, point_field_names[i], restrictions_point_fields[i], CEED_BASIS_NONE, point_fields[i]));
    }
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_diagnostic, "diagnostic", restriction_diagnostic, basis_diagnostic, CEED_VECTOR_ACTIVE));
    // ---- Set points
    PetscCall(RatelMPMContextCeedOperatorSetPoints(mpm, sub_op_diagnostic));

    // -- Add to composite operator
    RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_projected_diagnostic, sub_op_diagnostic));
    if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_diagnostic, stdout));
    // -- Cleanup
    RatelCallCeed(ratel, CeedBasisDestroy(&basis_diagnostic));
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_diagnostic));
    RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_diagnostic));
    RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_diagnostic));
  }

  // Diagnostic value computation, dual space
  PetscCall(RatelDebug(ratel, "---- Setting up dual space diagnostic value computation"));
  {
    CeedElemRestriction restriction_diagnostic;
    CeedBasis           basis_diagnostic;
    CeedQFunction       qf_diagnostic;
    CeedOperator        sub_op_diagnostic;

    // -- Diagnostic basis
    PetscCall(RatelDebug(ratel, "------ Diagnostics basis"));
    PetscCall(RatelDMPlexCeedBasisCreate(ratel, dm_dual_diagnostic, domain_label, domain_values[0], height_cell, 0, &basis_diagnostic));
    // -- Diagnostic data restriction
    PetscCall(RatelDebug(ratel, "------ Diagnostics restriction"));
    PetscCall(
        RatelDMPlexCeedElemRestrictionCreate(ratel, dm_dual_diagnostic, domain_label, domain_values[0], height_cell, 0, &restriction_diagnostic));

    // -- QFunction
    RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, model_data->dual_diagnostic, model_data->dual_diagnostic_loc, &qf_diagnostic));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_diagnostic, "q data", q_data_size, CEED_EVAL_NONE));
    if (model_data->num_comp_state > 0) {
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_diagnostic, "model state", model_data->num_comp_state, CEED_EVAL_NONE));
    }
    for (PetscInt i = 0; i < num_active_fields; i++) {
      char field_name[PETSC_MAX_PATH_LEN];

      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "u, field %" PetscInt_FMT, i));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_diagnostic, field_name, active_field_sizes[i], CEED_EVAL_INTERP));
      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "du/dX, field %" PetscInt_FMT, i));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_diagnostic, field_name, active_field_sizes[i] * dim, CEED_EVAL_GRAD));
    }
    for (PetscInt i = 0; i < num_point_fields; i++) {
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_diagnostic, point_field_names[i], point_field_sizes[i], CEED_EVAL_NONE));
    }
    RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_diagnostic, "diagnostic", num_comp_dual_diagnostic, CEED_EVAL_INTERP));
    RatelCallCeed(ratel, CeedQFunctionSetContext(qf_diagnostic, material->ctx_params));
    RatelCallCeed(ratel, CeedQFunctionSetContextWritable(qf_diagnostic, PETSC_FALSE));
    // -- Operator
    RatelCallCeed(ratel, CeedOperatorCreateAtPoints(ceed, qf_diagnostic, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &sub_op_diagnostic));
    PetscCall(RatelMaterialSetOperatorName(material, "volumetric term", sub_op_diagnostic));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_diagnostic, "q data", restriction_q_data, CEED_BASIS_NONE, q_data));
    if (model_data->num_comp_state > 0) {
      const char         *name = "model state";
      CeedVector          state_values;
      CeedElemRestriction restriction_state_values;

      // -- State objects from residual operator
      {
        CeedOperator op_residual_u;

        PetscCall(CeedEvaluatorGetCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
        PetscCall(RatelMaterialGetStateData(material, op_residual_u, &restriction_state_values, &state_values));
        PetscCall(CeedEvaluatorRestoreCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
      }
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_diagnostic, name, restriction_state_values, CEED_BASIS_NONE, state_values));
      RatelCallCeed(ratel, CeedVectorDestroy(&state_values));
      RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_state_values));
    }
    for (PetscInt i = 0; i < num_active_fields; i++) {
      char field_name[PETSC_MAX_PATH_LEN];

      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "u, field %" PetscInt_FMT, i));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_diagnostic, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
      PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "du/dX, field %" PetscInt_FMT, i));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_diagnostic, field_name, restrictions_u[i], bases_u[i], CEED_VECTOR_ACTIVE));
    }
    for (PetscInt i = 0; i < num_point_fields; i++) {
      RatelCallCeed(ratel,
                    CeedOperatorSetField(sub_op_diagnostic, point_field_names[i], restrictions_point_fields[i], CEED_BASIS_NONE, point_fields[i]));
    }
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_diagnostic, "diagnostic", restriction_diagnostic, basis_diagnostic, CEED_VECTOR_ACTIVE));
    // -- Set points
    PetscCall(RatelMPMContextCeedOperatorSetPoints(mpm, sub_op_diagnostic));
    // -- Add to composite operator
    RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_dual_diagnostic, sub_op_diagnostic));
    if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_diagnostic, stdout));

    // Dual space nodal scale
    {
      CeedVector           multiplicity_stored, nodal_multiplicity_stored;
      CeedQFunction        qf_dual_nodal_scale;
      CeedOperator         sub_op_dual_nodal_scale;
      CeedQFunctionContext ctx_nodal_scale;

      RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_diagnostic, &multiplicity_stored, NULL));
      RatelCallCeed(ratel, CeedElemRestrictionGetMultiplicity(restriction_diagnostic, multiplicity_stored));

      // -- Create nodal multiplicity vector
      //    This will be filled using CeedCompositeOperatorGetMultiplicity after setup is complete
      RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_diagnostic, &nodal_multiplicity_stored, NULL));

      // -- QFunction
      RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, ScaleLumpedDualDiagnosticTerms,
                                                       RatelQFunctionRelativePath(ScaleLumpedDualDiagnosticTerms_loc), &qf_dual_nodal_scale));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_dual_nodal_scale, "multiplicity", num_comp_dual_diagnostic, CEED_EVAL_NONE));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_dual_nodal_scale, "nodal_multiplicity", num_comp_dual_diagnostic, CEED_EVAL_NONE));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_dual_nodal_scale, "u", num_comp_dual_diagnostic, CEED_EVAL_NONE));
      RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_dual_nodal_scale, "v", num_comp_dual_diagnostic, CEED_EVAL_NONE));
      RatelCallCeed(ratel, CeedQFunctionContextCreate(ceed, &ctx_nodal_scale));
      RatelCallCeed(ratel, CeedQFunctionContextSetData(ctx_nodal_scale, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(num_comp_dual_diagnostic),
                                                       &num_comp_dual_diagnostic));
      RatelCallCeed(ratel, CeedQFunctionSetContext(qf_dual_nodal_scale, ctx_nodal_scale));
      RatelCallCeed(ratel, CeedQFunctionSetContextWritable(qf_dual_nodal_scale, PETSC_FALSE));

      // -- Operator
      RatelCallCeed(ratel, CeedOperatorCreate(ceed, qf_dual_nodal_scale, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &sub_op_dual_nodal_scale));
      PetscCall(RatelMaterialSetOperatorName(material, "volumetric term", sub_op_dual_nodal_scale));
      RatelCallCeed(ratel,
                    CeedOperatorSetField(sub_op_dual_nodal_scale, "multiplicity", restriction_diagnostic, CEED_BASIS_NONE, multiplicity_stored));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_dual_nodal_scale, "nodal_multiplicity", restriction_diagnostic, CEED_BASIS_NONE,
                                                nodal_multiplicity_stored));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_dual_nodal_scale, "u", restriction_diagnostic, CEED_BASIS_NONE, CEED_VECTOR_ACTIVE));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_dual_nodal_scale, "v", restriction_diagnostic, CEED_BASIS_NONE, CEED_VECTOR_ACTIVE));
      // -- Add to composite operator
      RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_dual_nodal_scale, sub_op_dual_nodal_scale));
      if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_dual_nodal_scale, stdout));

      // -- Cleanup
      RatelCallCeed(ratel, CeedQFunctionContextDestroy(&ctx_nodal_scale));
      RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_dual_nodal_scale));
      RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_dual_nodal_scale));
      RatelCallCeed(ratel, CeedVectorDestroy(&multiplicity_stored));
      RatelCallCeed(ratel, CeedVectorDestroy(&nodal_multiplicity_stored));
    }

    // -- Cleanup
    RatelCallCeed(ratel, CeedBasisDestroy(&basis_diagnostic));
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_diagnostic));
    RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_diagnostic));
    RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_diagnostic));
  }

  // Cleanup
  PetscCall(DMDestroy(&dm_solution));
  RatelCallCeed(ratel, CeedVectorDestroy(&q_data));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_q_data));
  for (PetscInt i = 0; i < num_active_fields; i++) {
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restrictions_u[i]));
    RatelCallCeed(ratel, CeedBasisDestroy(&bases_u[i]));
  }
  PetscCall(PetscFree(restrictions_u));
  PetscCall(PetscFree(bases_u));
  for (PetscInt i = 0; i < num_point_fields; i++) {
    RatelCallCeed(ratel, CeedVectorDestroy(&point_fields[i]));
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restrictions_point_fields[i]));
  }
  PetscCall(PetscFree(point_fields));
  PetscCall(PetscFree(restrictions_point_fields));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Diagnostic Suboperator MPM Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}
