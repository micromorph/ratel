/// @file
/// Setup Plasticity model params context objects

#include <ceed.h>
#include <petscsys.h>
#include <ratel-impl.h>
#include <ratel-material.h>
#include <ratel-model.h>
#include <ratel.h>
#include <ratel/models/plasticity.h>
#include <stddef.h>
#include <stdio.h>

/// @addtogroup RatelMaterials
/// @{

/// @brief Plasticity model parameters for `CeedQFunctionContext` and viewing
struct RatelModelParameterData_private plasticity_param_data_private = {
    .parameters     = {{
                           .name        = "shift v",
                           .description = "shift for U_t",
                           .offset      = offsetof(RatelElastoPlasticityParams, elasticity_params.common_parameters[RATEL_COMMON_PARAMETER_SHIFT_V]),
                           .is_hidden   = PETSC_TRUE,
                           .type        = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name        = "shift a",
                           .description = "shift for U_tt",
                           .offset      = offsetof(RatelElastoPlasticityParams, elasticity_params.common_parameters[RATEL_COMMON_PARAMETER_SHIFT_A]),
                           .is_hidden   = PETSC_TRUE,
                           .type        = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name            = "rho",
                           .description     = "Density",
                           .units           = "kg/m^3",
                           .restrictions    = "rho >= 0",
                           .default_value.s = 1,
                           .is_required     = PETSC_FALSE,
                           .offset          = offsetof(RatelElastoPlasticityParams, elasticity_params.common_parameters[RATEL_COMMON_PARAMETER_RHO]),
                           .type            = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "nu",
                           .description  = "Poisson's ratio",
                           .restrictions = "0 <= nu < 0.5",
                           .is_required  = PETSC_TRUE,
                           .offset       = offsetof(RatelElastoPlasticityParams, elasticity_params.nu),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "E",
                           .description  = "Young's Modulus",
                           .units        = "Pa",
                           .restrictions = "E >= 0",
                           .is_required  = PETSC_TRUE,
                           .offset       = offsetof(RatelElastoPlasticityParams, elasticity_params.E),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "yield_stress",
                           .description  = "Initial yield stress threshold",
                           .units        = "Pa",
                           .restrictions = "yield_stress >= 0",
                           .is_required  = PETSC_TRUE,
                           .offset       = offsetof(RatelElastoPlasticityParams, plasticity_params.yield_stress),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "linear_hardening",
                           .description  = "Isotropic hardening parameter",
                           .units        = "Pa",
                           .restrictions = "linear_hardening>= 0",
                           .is_required  = PETSC_TRUE,
                           .offset       = offsetof(RatelElastoPlasticityParams, plasticity_params.linear_hardening),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "saturation_stress",
                           .description  = "Saturation stress for Voce hardening component",
                           .units        = "Pa",
                           .restrictions = "saturation_stress >= 0",
                           .is_required  = PETSC_TRUE,
                           .offset       = offsetof(RatelElastoPlasticityParams, plasticity_params.saturation_stress),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "hardening_decay",
                           .description  = "Exponential decay rate component for Voce hardening component",
                           .restrictions = "hardening_decay >= 0",
                           .is_required  = PETSC_TRUE,
                           .offset       = offsetof(RatelElastoPlasticityParams, plasticity_params.hardening_decay),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "mu",
                           .description  = "Shear Modulus: E / (2 (1 + nu))",
                           .units        = "Pa",
                           .restrictions = "mu >= 0",
                           .is_required  = PETSC_FALSE,
                           .offset       = offsetof(RatelElastoPlasticityParams, elasticity_params.mu),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "lambda",
                           .description  = "First Lame parameter: 2 mu nu / (1 - 2 nu)",
                           .units        = "Pa",
                           .restrictions = "lambda >= 0",
                           .is_required  = PETSC_FALSE,
                           .offset       = offsetof(RatelElastoPlasticityParams, elasticity_params.lambda),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "bulk",
                           .description  = "Bulk Modulus: 2 mu (1 + nu) / (3 (1 - 2 nu))",
                           .units        = "Pa",
                           .restrictions = "bulk >= 0",
                           .is_required  = PETSC_FALSE,
                           .offset       = offsetof(RatelElastoPlasticityParams, elasticity_params.bulk),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "two_mu",
                           .description  = "Shear Modulus multiplied by 2",
                           .units        = "Pa",
                           .restrictions = "mu >= 0",
                           .is_required  = PETSC_FALSE,
                           .offset       = offsetof(RatelElastoPlasticityParams, elasticity_params.two_mu),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }},
    .num_parameters = 13,
    .reference      = "doi:10.1016/0045-7825(92)90156-e"
};
RatelModelParameterData plasticity_param_data = &plasticity_param_data_private;

/**
  @brief Set `CeedQFunctionContext` data and register fields for plasticity parameters.

  Not collective across MPI processes.

  @param[in]   material           `RatelMaterial` to setup model params context
  @param[in]   yield_stress       Yield stress
  @param[in]   linear_hardening   Linear hardening factor
  @param[in]   saturation_stress  Saturation stress
  @param[in]   hardening_decay    voce hardening deacy parameter
  @param[out]  ctx                `CeedQFunctionContext` for plasticity parameters

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelCeedParamsContextCreate_Plasticity(RatelMaterial material, CeedScalar yield_stress, CeedScalar linear_hardening,
                                                              CeedScalar saturation_stress, CeedScalar hardening_decay, CeedQFunctionContext *ctx) {
  Ratel                        ratel = material->ratel;
  CeedQFunctionContext         elasticity_ctx;
  RatelLinearElasticityParams *elasticity_params;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Create Parameters Context Plasticity"));

  // Create sub-contexts - elasticity
  RatelModelParameterData param_data = material->model_data->param_data;
  PetscCall(RatelMaterialParamsContextCreate_NeoHookean(material, &elasticity_ctx));
  material->model_data->param_data = param_data;
  RatelCallCeed(ratel, CeedQFunctionContextGetDataRead(elasticity_ctx, CEED_MEM_HOST, &elasticity_params));

  // Create the RatelElastoPlasticityParams struct
  RatelElastoPlasticityParams elastoplasticity_data = {
      .elasticity_params                   = *elasticity_params,
      .plasticity_params.yield_stress      = yield_stress * ratel->material_units->Pascal,
      .plasticity_params.linear_hardening  = linear_hardening * ratel->material_units->Pascal,
      .plasticity_params.saturation_stress = saturation_stress * ratel->material_units->Pascal,
      .plasticity_params.hardening_decay   = hardening_decay,
  };

  // Free elasticity context
  RatelCallCeed(ratel, CeedQFunctionContextRestoreDataRead(elasticity_ctx, &elasticity_params));
  RatelCallCeed(ratel, CeedQFunctionContextDestroy(&elasticity_ctx));

  // Create context object
  RatelCallCeed(ratel, CeedQFunctionContextCreate(ratel->ceed, ctx));
  RatelCallCeed(ratel, CeedQFunctionContextSetData(*ctx, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(elastoplasticity_data), &elastoplasticity_data));

  // Register parameters
  PetscCall(RatelModelParameterDataRegisterContextFields(ratel, material->model_data->param_data, *ctx));

  // Debugging output
  if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedQFunctionContextView(*ctx, stdout));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Create Parameters Context Plasticity Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Build `CeedQFunctionContext` with plasticity parameters.

  Collective across MPI processes.

  @param[in]   material  `RatelMaterial` to setup model params context
  @param[out]  ctx       `CeedQFunctionContext` for plasticity parameters

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialParamsContextCreate_Plasticity(RatelMaterial material, CeedQFunctionContext *ctx) {
  Ratel      ratel                 = material->ratel;
  PetscBool  set_yield_stress      = PETSC_FALSE;
  PetscBool  set_linear_hardening  = PETSC_FALSE;
  PetscBool  set_saturation_stress = PETSC_FALSE;
  PetscBool  set_hardening_decay   = PETSC_FALSE;
  CeedScalar yield_stress = 0.0, linear_hardening = 0.0, saturation_stress = 0.0, hardening_decay = 0.0;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup Model Parameters Context Plasticity"));

  // Set model parameter data
  material->model_data->param_data = plasticity_param_data;

  // Get parameter values
  {
    char *cl_prefix;

    PetscCall(RatelMaterialGetCLPrefix(material, &cl_prefix));
    PetscOptionsBegin(ratel->comm, cl_prefix, "Plasticity physical parameters", NULL);

    PetscCall(PetscOptionsScalar("-yield_stress", "Young's Modulus", NULL, yield_stress, &yield_stress, &set_yield_stress));
    PetscCheck(yield_stress > 0, ratel->comm, PETSC_ERR_SUP, "Plasticity model requires non-negative -yield_stress option");
    PetscCall(RatelDebug(ratel, "---- Yield Stress: %f", yield_stress));

    PetscCall(
        PetscOptionsScalar("-linear_hardening", "Linear hardening parameter", NULL, linear_hardening, &linear_hardening, &set_linear_hardening));
    PetscCheck(linear_hardening >= 0, ratel->comm, PETSC_ERR_SUP, "Plasticity model requires non-negative -linear_hardening option");
    PetscCall(RatelDebug(ratel, "---- H: %f", linear_hardening));

    PetscCall(PetscOptionsScalar("-saturation_stress", "Saturation stress for Voce hardening component", NULL, saturation_stress, &saturation_stress,
                                 &set_saturation_stress));
    PetscCheck(saturation_stress >= yield_stress, ratel->comm, PETSC_ERR_SUP,
               "Plasticity model requires -saturation_stress option with saturation_stress >= yield_sstress");
    PetscCall(RatelDebug(ratel, "---- sigma_infinity: %f", saturation_stress));

    PetscCall(PetscOptionsScalar("-hardening_decay", "Hardening decay rate for Voce hardening component", NULL, hardening_decay, &hardening_decay,
                                 &set_hardening_decay));
    PetscCheck(hardening_decay >= 0, ratel->comm, PETSC_ERR_SUP, "Plasticity model requires non-negative -hardening_decay option");
    PetscCall(RatelDebug(ratel, "---- alpha: %f", hardening_decay));

    PetscOptionsEnd();  // End of setting model parameters
    PetscCall(PetscFree(cl_prefix));
  }

  // Check for all required options to be set
  PetscCheck(set_yield_stress, ratel->comm, PETSC_ERR_SUP, "-yield_stress option needed");
  PetscCheck(set_linear_hardening, ratel->comm, PETSC_ERR_SUP, "-linear_hardening option needed");
  PetscCheck(set_saturation_stress, ratel->comm, PETSC_ERR_SUP, "-saturation_stress option needed");
  PetscCheck(set_hardening_decay, ratel->comm, PETSC_ERR_SUP, "-hardening_decay option needed");

  // Create context
  PetscCall(RatelCeedParamsContextCreate_Plasticity(material, yield_stress, linear_hardening, saturation_stress, hardening_decay, ctx));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup Model Parameters Context Plasticity Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup data for `CeedQFunctionContext` for smoother with plasticity parameters.

  Collective across MPI processes.

  @param[in,out]  material  `RatelMaterial` to setup model params context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialParamsSmootherDataSetup_Plasticity(RatelMaterial material) {
  Ratel      ratel           = material->ratel;
  PetscBool  set_nu_smoother = PETSC_FALSE;
  CeedScalar nu = 0.0, nu_smoother = 0.0;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup Model Parameters Smoother Context Plasticity"));

  // Get parameter values
  if (material->num_params_smoother_values == 0) {
    char *cl_prefix;

    PetscCall(RatelMaterialGetCLPrefix(material, &cl_prefix));
    PetscOptionsBegin(ratel->comm, cl_prefix, "Plasticity physical parameters for smoother", NULL);

    PetscCall(PetscOptionsScalar("-nu", "Poisson's ratio", NULL, nu, &nu, NULL));
    PetscCheck(nu >= 0 && nu < 0.5, ratel->comm, PETSC_ERR_SUP, "Plasticity model requires Poisson ratio -nu option in [0, 0.5)");
    PetscCall(RatelDebug(ratel, "---- nu: %f", nu));

    PetscCall(PetscOptionsScalar("-nu_smoother", "Poisson's ratio for smoother", NULL, nu_smoother, &nu_smoother, &set_nu_smoother));
    if (set_nu_smoother) PetscCall(RatelDebug(ratel, "---- nu_smoother: %f", nu_smoother));
    else PetscCall(RatelDebug(ratel, "---- No Nu for smoother set"));

    PetscOptionsEnd();  // End of setting model parameters
    PetscCall(PetscFree(cl_prefix));

    // Store smoother values, if needed
    if (set_nu_smoother) {
      const char *label_name     = "nu";
      PetscSizeT  label_name_len = 0;

      material->num_params_smoother_values = 1;
      PetscCall(PetscNew(&material->ctx_params_values));
      PetscCall(PetscNew(&material->ctx_params_smoother_values));
      material->ctx_params_values[0]          = nu;
      material->ctx_params_smoother_values[0] = nu_smoother;
      PetscCall(PetscNew(&material->ctx_params_label_names));
      PetscCall(PetscStrlen(label_name, &label_name_len));
      PetscCall(PetscCalloc1(label_name_len + 1, &material->ctx_params_label_names[0]));
      PetscCall(PetscStrncpy(material->ctx_params_label_names[0], label_name, label_name_len + 1));
    }
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup Model Parameters Smoother Context Plasticity Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}
