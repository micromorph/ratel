/// @file
/// Setup Ogden model params context objects

#include <ceed.h>
#include <math.h>
#include <petscsys.h>
#include <ratel-impl.h>
#include <ratel-material.h>
#include <ratel-model.h>
#include <ratel.h>
#include <ratel/models/mixed-ogden.h>
#include <stddef.h>
#include <stdio.h>

/// @addtogroup RatelMaterials
/// @{

/// @brief Mixed Ogden model parameters for `CeedQFunctionContext` and viewing
struct RatelModelParameterData_private mixed_ogden_param_data_private = {
    .parameters     = {{
                           .name        = "shift v",
                           .description = "shift for U_t",
                           .is_hidden   = PETSC_TRUE,
                           .offset      = offsetof(RatelMixedOgdenElasticityParams, common_parameters[RATEL_COMMON_PARAMETER_SHIFT_V]),
                           .type        = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name        = "shift a",
                           .description = "shift for U_tt",
                           .is_hidden   = PETSC_TRUE,
                           .offset      = offsetof(RatelMixedOgdenElasticityParams, common_parameters[RATEL_COMMON_PARAMETER_SHIFT_A]),
                           .type        = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name            = "rho",
                           .description     = "Density",
                           .units           = "kg/m^3",
                           .restrictions    = "rho >= 0",
                           .default_value.s = 1,
                           .is_required     = PETSC_FALSE,
                           .offset          = offsetof(RatelMixedOgdenElasticityParams, common_parameters[RATEL_COMMON_PARAMETER_RHO]),
                           .type            = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "nu",
                           .description  = "Poisson's ratio",
                           .restrictions = "0 <= nu < 0.5",
                           .is_required  = PETSC_TRUE,
                           .offset       = offsetof(RatelMixedOgdenElasticityParams, nu),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "m",
                           .description  = "List of N shear moduli",
                           .units        = "Pa",
                           .restrictions = "m[i]*alpha[i] > 0",
                           .is_required  = PETSC_TRUE,
                           .offset       = offsetof(RatelMixedOgdenElasticityParams, m),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "alpha",
                           .description  = "List of N constants associated with shear moduli",
                           .restrictions = "m[i]*alpha[i] > 0",
                           .is_required  = PETSC_TRUE,
                           .offset       = offsetof(RatelMixedOgdenElasticityParams, alpha),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name            = "num ogden parameters",
                           .description     = "Number of Ogden parameters",
                           .is_hidden       = PETSC_TRUE,
                           .default_value.i = 1,
                           .offset          = offsetof(RatelMixedOgdenElasticityParams, num_ogden_parameters),
                           .type            = CEED_CONTEXT_FIELD_INT32,
                   }, {
                           .name         = "mu",
                           .description  = "Shear Modulus: sum {m[i] alpha[i] / 2}_{i=1:N}",
                           .units        = "Pa",
                           .restrictions = "mu >= 0",
                           .is_required  = PETSC_FALSE,
                           .offset       = offsetof(RatelMixedOgdenElasticityParams, mu),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "lambda",
                           .description  = "First Lame parameter: 2 mu nu / (1 - 2 nu)",
                           .units        = "Pa",
                           .restrictions = "lambda >= 0",
                           .is_required  = PETSC_FALSE,
                           .offset       = offsetof(RatelMixedOgdenElasticityParams, lambda),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "bulk",
                           .description  = "Bulk Modulus: 2 mu (1 + nu) / (3 (1 - 2 nu))",
                           .units        = "Pa",
                           .restrictions = "bulk >= 0",
                           .is_required  = PETSC_FALSE,
                           .offset       = offsetof(RatelMixedOgdenElasticityParams, bulk),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name            = "nu_primal",
                           .description     = "To change full strain to deviatoric in mixed formulation",
                           .restrictions    = "-1. <= nu_primal < nu",
                           .default_value.s = -1.,
                           .is_required     = PETSC_FALSE,
                           .offset          = offsetof(RatelMixedOgdenElasticityParams, nu_primal),
                           .type            = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name            = "nu_primal_pc",
                           .description     = "To change full strain to deviatoric in uu block pc",
                           .restrictions    = "-1. <= nu_primal_pc < nu",
                           .default_value.s = -1.,
                           .is_required     = PETSC_FALSE,
                           .offset          = offsetof(RatelMixedOgdenElasticityParams, nu_primal_pc),
                           .type            = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name            = "sign_pp",
                           .description     = "To change the sign in pp block pc",
                           .restrictions    = "-1 <= sign_pp <= 1",
                           .default_value.i = -1,
                           .is_required     = PETSC_FALSE,
                           .offset          = offsetof(RatelMixedOgdenElasticityParams, sign_pp),
                           .type            = CEED_CONTEXT_FIELD_INT32,
                   }, {
                           .name         = "bulk_primal",
                           .description  = "Primal bulk Modulus:  2 mu (1 + nu_primal) / (3 (1 - 2 nu_primal))",
                           .units        = "Pa",
                           .restrictions = "bulk_primal >= 0",
                           .is_required  = PETSC_FALSE,
                           .offset       = offsetof(RatelMixedOgdenElasticityParams, bulk_primal),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "bulk_primal_pc",
                           .description  = "Primal bulk Modulus for block pc: 2 mu (1 + nu_primal_pc) / (3 (1 - 2 nu_primal_pc))",
                           .units        = "Pa",
                           .restrictions = "bulk_primal_pc >= 0",
                           .is_required  = PETSC_FALSE,
                           .offset       = offsetof(RatelMixedOgdenElasticityParams, bulk_primal_pc),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "E",
                           .description  = "Young's Modulus: 2 mu (1 + nu)",
                           .units        = "Pa",
                           .restrictions = "E >= 0",
                           .is_required  = PETSC_FALSE,
                           .offset       = offsetof(RatelMixedOgdenElasticityParams, E),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }},
    .num_parameters = 16,
    .reference      = "ISBN: 0-471-82319-8"
};
RatelModelParameterData mixed_ogden_param_data = &mixed_ogden_param_data_private;

/**
  @brief Set `CeedQFunctionContext` data and register fields for Ogden parameters.

  Not collective across MPI processes.

  @param[in]   material              `RatelMaterial` to setup params context
  @param[in]   nu                    Poisson's ratio
  @param[in]   m                     Array of material Properties for m
  @param[in]   alpha                 Array of material Properties for alpha
  @param[in]   num_ogden_parameters  Number of parameters for Ogden model (Length of alpha=m)
  @param[in]   rho                   Density for scaled mass matrix
  @param[in]   nu_primal             To change full strain to deviatoric in mixed formulation
  @param[in]   nu_primal_pc          To change full strain to deviatoric in uu block pc
  @param[in]   sign_pp               To change the sign in pp block pc
  @param[out]  ctx                   `CeedQFunctionContext` for Ogden parameters

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelCeedParamsContextCreate_MixedOgden(RatelMaterial material, CeedScalar nu, CeedScalar *m, CeedScalar *alpha,
                                                              PetscInt num_ogden_parameters, CeedScalar rho, CeedScalar nu_primal,
                                                              CeedScalar nu_primal_pc, CeedInt sign_pp, CeedQFunctionContext *ctx) {
  Ratel                            ratel      = material->ratel;
  RatelModelParameterData          param_data = material->model_data->param_data;
  RatelMixedOgdenElasticityParams *params;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Create Parameters Context Mixed Ogden"));

  // Create context data with correct scaling
  PetscCall(PetscNew(&params));
  CeedScalar two_mu = 0.;
  for (CeedInt i = 0; i < num_ogden_parameters; i++) {
    params->m[i]     = m[i] * ratel->material_units->Pascal;
    params->alpha[i] = alpha[i];
    two_mu += params->alpha[i] * params->m[i];
  }
  params->mu                                            = two_mu / 2.;
  params->num_ogden_parameters                          = num_ogden_parameters;
  params->nu                                            = nu;
  params->common_parameters[RATEL_COMMON_PARAMETER_RHO] = rho * (ratel->material_units->kilogram) / pow(ratel->material_units->meter, 3);
  params->E                                             = two_mu * (1 + params->nu);
  params->nu_primal                                     = nu_primal;
  params->nu_primal_pc                                  = nu_primal_pc;
  params->sign_pp                                       = sign_pp;
  params->bulk_primal                                   = two_mu * (1 + params->nu_primal) / (3. * (1 - 2 * params->nu_primal));
  params->bulk_primal_pc                                = two_mu * (1 + params->nu_primal_pc) / (3. * (1 - 2 * params->nu_primal_pc));
  if (params->nu == 0.5) {
    params->lambda = 0.;
    params->bulk   = 0.;
  } else {
    params->lambda = two_mu * params->nu / (1 - 2 * params->nu);
    params->bulk   = two_mu * (1 + params->nu) / (3. * (1 - 2 * params->nu));
  }

  // Create context object
  RatelCallCeed(ratel, CeedQFunctionContextCreate(ratel->ceed, ctx));
  RatelCallCeed(ratel, CeedQFunctionContextSetData(*ctx, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(*params), params));
  PetscCall(PetscFree(params));

  // Register parameters
  PetscCall(RatelModelParameterDataRegisterContextFields(ratel, param_data, *ctx));

  // Debugging output
  if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedQFunctionContextView(*ctx, stdout));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Create Parameters Context mixed Ogden Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Build `CeedQFunctionContext` for mixed Ogden parameters.

  Collective across MPI processes.

  @param[in]   material  `RatelMaterial` to setup params context
  @param[out]  ctx       `CeedQFunctionContext` for Ogden parameters

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialParamsContextCreate_MixedOgden(RatelMaterial material, CeedQFunctionContext *ctx) {
  Ratel      ratel                = material->ratel;
  PetscBool  set_nu               = PETSC_FALSE;
  PetscBool  set_m                = PETSC_FALSE;
  PetscBool  set_alpha            = PETSC_FALSE;
  PetscInt   num_ogden_parameters = RATEL_NUMBER_OGDEN_PARAMETERS;
  CeedScalar nu                   = -1.0, m[RATEL_NUMBER_OGDEN_PARAMETERS], alpha[RATEL_NUMBER_OGDEN_PARAMETERS], rho, nu_primal, nu_primal_pc;
  PetscInt   sign_pp;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup Parameters Context mixed Ogden"));

  // Set model parameter data
  material->model_data->param_data = mixed_ogden_param_data;

  // Get default values;
  PetscCall(RatelModelParameterDataGetDefaultValue_Scalar(ratel, mixed_ogden_param_data, "rho", &rho));
  PetscCall(RatelModelParameterDataGetDefaultValue_Scalar(ratel, mixed_ogden_param_data, "nu_primal", &nu_primal));
  PetscCall(RatelModelParameterDataGetDefaultValue_Scalar(ratel, mixed_ogden_param_data, "nu_primal_pc", &nu_primal_pc));
  {
    CeedInt default_sign_pp;

    PetscCall(RatelModelParameterDataGetDefaultValue_Int(ratel, mixed_ogden_param_data, "sign_pp", &default_sign_pp));
    sign_pp = default_sign_pp;
  }

  // Get parameter values
  {
    char *cl_prefix;

    PetscCall(RatelMaterialGetCLPrefix(material, &cl_prefix));
    PetscOptionsBegin(ratel->comm, cl_prefix, "Mixed Ogden model parameters", NULL);

    PetscCall(PetscOptionsScalar("-nu", "Poisson ratio", NULL, nu, &nu, &set_nu));
    PetscCheck(nu >= 0 && nu <= 0.5, ratel->comm, PETSC_ERR_SUP, "Mixed Ogden model requires Poisson ratio -nu option in [0, 0.5]");
    PetscCall(RatelDebug(ratel, "---- nu: %f", nu));

    m[0] = 3, alpha[0] = 2;
    PetscCall(PetscOptionsScalarArray("-m", "Material Property for mixed Ogden model", NULL, m, &num_ogden_parameters, &set_m));
    PetscCall(PetscOptionsScalarArray("-alpha", "Material Property for mixed Ogden model", NULL, alpha, &num_ogden_parameters, &set_alpha));
    for (CeedInt i = 0; i < num_ogden_parameters; i++) {
      PetscCheck(m[i] * alpha[i] > 0.0, ratel->comm, PETSC_ERR_SUP, "Mixed Ogden model requires positive (m[i] * alpha[i])");
    }

    PetscCall(PetscOptionsScalar("-rho", "Density for scaled mass matrix", NULL, rho, &rho, NULL));
    PetscCheck(rho >= 0.0, ratel->comm, PETSC_ERR_SUP, "Density must be a positive value");
    PetscCall(RatelDebug(ratel, "---- rho: %f", rho));

    PetscCall(PetscOptionsScalar("-nu_primal", "To change full strain to deviatoric in mixed jacobian", NULL, nu_primal, &nu_primal, NULL));
    PetscCheck(nu_primal >= -1. && nu_primal < nu, ratel->comm, PETSC_ERR_SUP,
               "To change full strain to deviatoric in mixed jacobian -nu_primal option in [-1., nu)");
    PetscCall(RatelDebug(ratel, "---- nu_primal: %f", nu_primal));

    PetscCall(PetscOptionsScalar("-nu_primal_pc", "To change full strain to deviatoric in uu block pc", NULL, nu_primal_pc, &nu_primal_pc, NULL));
    PetscCheck(nu_primal_pc >= -1. && nu_primal_pc < nu, ratel->comm, PETSC_ERR_SUP,
               "To change full strain to deviatoric in uu block pc -nu_primal_pc option in [-1., nu)");
    PetscCall(RatelDebug(ratel, "---- nu_primal_pc: %f", nu_primal_pc));

    PetscCall(PetscOptionsInt("-sign_pp", "To change the sign in pp block pc", NULL, sign_pp, &sign_pp, NULL));
    PetscCheck(sign_pp >= -1 && sign_pp <= 1, ratel->comm, PETSC_ERR_SUP, "To change the sign in pp block pc -sign_pp option in [-1, 1]");
    PetscCall(RatelDebug(ratel, "---- sign_pp: %d", sign_pp));

    PetscOptionsEnd();  // End of setting parameters
    PetscCall(PetscFree(cl_prefix));
  }

  // Check for all required options to be set
  PetscCheck(set_nu, ratel->comm, PETSC_ERR_SUP, "-nu option needed");
  PetscCheck(set_m, ratel->comm, PETSC_ERR_SUP, "-m option needed");
  PetscCheck(set_alpha, ratel->comm, PETSC_ERR_SUP, "-alpha option needed");

  // Set number of components for parameters
  mixed_ogden_param_data->parameters[4].num_components = num_ogden_parameters;
  mixed_ogden_param_data->parameters[5].num_components = num_ogden_parameters;

  // Create context
  PetscCall(RatelCeedParamsContextCreate_MixedOgden(material, nu, m, alpha, num_ogden_parameters, rho, nu_primal, nu_primal_pc, sign_pp, ctx));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup Parameters Context Mixed Ogden Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup data for `CeedQFunctionContext` for smoother with mixed Ogden parameters.

  Collective across MPI processes.

  @param[in,out]  material  `RatelMaterial` to setup params context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialParamsSmootherDataSetup_MixedOgden(RatelMaterial material) {
  Ratel      ratel           = material->ratel;
  PetscBool  set_nu_smoother = PETSC_FALSE;
  CeedScalar nu = -1.0, nu_smoother = -1.0;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup Parameters Smoother Context mixed Ogden"));

  // Get parameter values
  if (material->num_params_smoother_values == 0) {
    char *cl_prefix;

    PetscCall(RatelMaterialGetCLPrefix(material, &cl_prefix));
    PetscOptionsBegin(ratel->comm, cl_prefix, "Ogden model parameters for smoother", NULL);

    PetscCall(PetscOptionsScalar("-nu", "Poisson ratio", NULL, nu, &nu, NULL));
    PetscCheck(nu >= 0 && nu <= 0.5, ratel->comm, PETSC_ERR_SUP, "Ogden model requires Poisson ratio -nu option in [0, 0.5]");
    PetscCall(RatelDebug(ratel, "---- nu: %f", nu));

    PetscCall(PetscOptionsScalar("-nu_smoother", "Poisson's ratio for smoother", NULL, nu_smoother, &nu_smoother, &set_nu_smoother));
    if (set_nu_smoother) PetscCall(RatelDebug(ratel, "---- nu_smoother: %f", nu_smoother));
    else PetscCall(RatelDebug(ratel, "---- No Nu for smoother set"));

    PetscOptionsEnd();  // End of setting model parameters
    PetscCall(PetscFree(cl_prefix));

    // Store smoother values, if needed
    if (set_nu_smoother) {
      const char *label_name     = "nu";
      PetscSizeT  label_name_len = 0;

      material->num_params_smoother_values = 1;
      PetscCall(PetscNew(&material->ctx_params_values));
      PetscCall(PetscNew(&material->ctx_params_smoother_values));
      material->ctx_params_values[0]          = nu;
      material->ctx_params_smoother_values[0] = nu_smoother;
      PetscCall(PetscNew(&material->ctx_params_label_names));
      PetscCall(PetscStrlen(label_name, &label_name_len));
      PetscCall(PetscCalloc1(label_name_len + 1, &material->ctx_params_label_names[0]));
      PetscCall(PetscStrncpy(material->ctx_params_label_names[0], label_name, label_name_len + 1));
    }
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup Model Parameters Smoother Context mixed Ogden Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}
