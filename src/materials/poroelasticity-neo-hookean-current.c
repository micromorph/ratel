/// @file
/// Finite strain Neo-Hookean poroelasticity

#include <ceed.h>
#include <petscsys.h>
#include <ratel-impl.h>
#include <ratel-model.h>
#include <ratel.h>
#include <ratel/qfunctions/geometry/surface-force.h>
#include <ratel/qfunctions/geometry/surface.h>
#include <ratel/qfunctions/geometry/volumetric.h>
#include <ratel/qfunctions/models/dual-diagnostic.h>
#include <ratel/qfunctions/models/poroelasticity-neo-hookean-current.h>

static const CeedInt      active_field_sizes[]          = {3, 1};
static const char *const  active_field_names[]          = {"displacement", "pressure"};
static const char *const  active_component_names[]      = {"u_x", "u_y", "u_z", "p"};
static const CeedInt      active_field_num_eval_modes[] = {NUM_ACTIVE_FIELD_EVAL_MODES_PoroElasticityNeoHookeanCurrent_u,
                                                           NUM_ACTIVE_FIELD_EVAL_MODES_PoroElasticityNeoHookeanCurrent_p};
static const CeedEvalMode active_field_eval_modes[]     = {CEED_EVAL_GRAD, CEED_EVAL_INTERP, CEED_EVAL_GRAD};
static const CeedInt      ut_field_num_eval_modes[]     = {NUM_U_t_FIELD_EVAL_MODES_PoroElasticityNeoHookeanCurrent_u,
                                                           NUM_U_t_FIELD_EVAL_MODES_PoroElasticityNeoHookeanCurrent_p};
static const CeedEvalMode ut_field_eval_modes[]         = {CEED_EVAL_GRAD, CEED_EVAL_INTERP};

static const char *const projected_diagnostic_component_names[] = {"displacement_x",
                                                                   "displacement_y",
                                                                   "displacement_z",
                                                                   "Cauchy_stress_xx",
                                                                   "Cauchy_stress_xy",
                                                                   "Cauchy_stress_xz",
                                                                   "Cauchy_stress_yy",
                                                                   "Cauchy_stress_yz",
                                                                   "Cauchy_stress_zz",
                                                                   "pressure",
                                                                   "volumetric_strain",
                                                                   "trace_E2",
                                                                   "J",
                                                                   "strain_energy_density",
                                                                   "von_Mises_stress",
                                                                   "mass_density",
                                                                   "porosity"};
static const char *const dual_diagnostic_component_names[]      = {"nodal_volume"};

struct RatelModelData_private poroelasticity_Neo_Hookean_current_data_private = {
    .name                                 = "Neo-Hookean poroelasticity at finite strain, in current configuration",
    .command_line_option                  = "poroelasticity-neo-hookean-current",
    .setup_q_data_volume                  = SetupVolumeGeometry,
    .setup_q_data_volume_loc              = SetupVolumeGeometry_loc,
    .setup_q_data_surface                 = SetupSurfaceGeometry,
    .setup_q_data_surface_loc             = SetupSurfaceGeometry_loc,
    .setup_q_data_surface_grad            = SetupSurfaceForceGeometry,
    .setup_q_data_surface_grad_loc        = SetupSurfaceForceGeometry_loc,
    .q_data_volume_size                   = Q_DATA_VOLUMETRIC_GEOMETRY_SIZE,
    .q_data_surface_size                  = Q_DATA_SURFACE_GEOMETRY_SIZE,
    .q_data_surface_grad_size             = Q_DATA_SURFACE_FORCE_GEOMETRY_SIZE,
    .num_forcing                          = 2,
    .num_active_fields                    = sizeof(active_field_sizes) / sizeof(*active_field_sizes),
    .active_field_sizes                   = active_field_sizes,
    .active_field_names                   = active_field_names,
    .active_component_names               = active_component_names,
    .active_field_num_eval_modes          = active_field_num_eval_modes,
    .active_field_eval_modes              = active_field_eval_modes,
    .num_comp_stored_u                    = NUM_COMPONENTS_STORED_PoroElasticityNeoHookeanCurrent,
    .num_comp_stored_ut                   = NUM_COMPONENTS_STORED_PoroElasticityNeoHookeanCurrent_ut,
    .num_comp_projected_diagnostic        = NUM_COMPONENTS_DIAGNOSTIC_PoroElasticityNeoHookean,
    .projected_diagnostic_component_names = projected_diagnostic_component_names,
    .num_comp_dual_diagnostic             = NUM_COMPONENTS_DIAGNOSTIC_Dual,
    .dual_diagnostic_component_names      = dual_diagnostic_component_names,
    .quadrature_mode                      = CEED_GAUSS,
    .residual_u                           = PoroElasticityResidual_NeoHookeanCurrent,
    .residual_u_loc                       = PoroElasticityResidual_NeoHookeanCurrent_loc,
    .residual_ut                          = PoroElasticityResidual_NeoHookeanCurrent_ut,
    .residual_ut_loc                      = PoroElasticityResidual_NeoHookeanCurrent_ut_loc,
    .has_ut_term                          = PETSC_TRUE,
    .pass_stored_u_to_ut                  = PETSC_TRUE,
    .ut_field_num_eval_modes              = ut_field_num_eval_modes,
    .ut_field_eval_modes                  = ut_field_eval_modes,
    .jacobian                             = PoroElasticityJacobian_NeoHookeanCurrent,
    .jacobian_loc                         = PoroElasticityJacobian_NeoHookeanCurrent_loc,
    .jacobian_block                       = {PoroElasticityPC_uu_NeoHookeanCurrent,                   PoroElasticityPC_pp_NeoHookeanCurrent                  },
    .jacobian_block_loc                   = {PoroElasticityPC_uu_NeoHookeanCurrent_loc,               PoroElasticityPC_pp_NeoHookeanCurrent_loc              },
    .strain_energy                        = StrainEnergy_PoroElasticityNeoHookean,
    .strain_energy_loc                    = StrainEnergy_PoroElasticityNeoHookean_loc,
    .projected_diagnostic                 = Diagnostic_PoroElasticityNeoHookean,
    .projected_diagnostic_loc             = Diagnostic_PoroElasticityNeoHookean_loc,
    .dual_diagnostic                      = DualDiagnostic,
    .dual_diagnostic_loc                  = DualDiagnostic_loc,
    .flops_qf_jacobian_u                  = FLOPS_JACOBIAN_PoroElasticityNeoHookeanCurrent,
    .flops_qf_jacobian_block = {FLOPS_JACOBIAN_Block_uu_PoroElasticityNeoHookeanCurrent, FLOPS_JACOBIAN_Block_pp_PoroElasticityNeoHookeanCurrent},
};
RatelModelData poroelasticity_Neo_Hookean_current_data = &poroelasticity_Neo_Hookean_current_data_private;

/// @addtogroup RatelMaterials
/// @{

/**
  @brief Create `RatelMaterial` model data for Neo-Hookean poroelasticity at finite strain in curent configuration.

  Not collective across MPI processes.

  @param[in]   ratel     `Ratel` context
  @param[out]  material  `RatelMaterial` context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialCreate_PoroElasticityNeoHookeanCurrent(Ratel ratel, RatelMaterial material) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Create Poroelasticity Neo-Hookean Current"));

  // Model data
  PetscCall(RatelModelDataVerifyRelativePath(ratel, poroelasticity_Neo_Hookean_current_data));
  material->model_data = poroelasticity_Neo_Hookean_current_data;

  // QFunction contexts
  PetscCall(RatelMaterialParamsContextCreate_PoroElasticityNeoHookean(material, &material->ctx_params));
  PetscCall(RatelMaterialParamsSmootherDataSetup_PoroElasticityNeoHookean(material));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Create Poroelasticity Neo-Hookean Current Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Register Neo-Hookean poroelasticity model at finite strain in curent configuration model.

  Not collective across MPI processes.

  @param[in]   ratel                      `Ratel` context
  @param[in]   cl_argument                Command line argument to use for model
  @param[out]  material_create_functions  PETSc function list for `RatelMaterial`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelRegisterModel_PoroElasticityNeoHookeanCurrent(Ratel ratel, const char *cl_argument,
                                                                  PetscFunctionList *material_create_functions) {
  PetscFunctionBeginUser;
  RATEL_MODEL_REGISTER(material_create_functions, cl_argument, PoroElasticityNeoHookeanCurrent);
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}
