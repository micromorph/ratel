/// @file
/// Mixed Linear elasticity using Neo-Hookean model

#include <ceed.h>
#include <petscsys.h>
#include <ratel-impl.h>
#include <ratel-model.h>
#include <ratel.h>
#include <ratel/qfunctions/boundaries/mixed-linear-manufactured.h>
#include <ratel/qfunctions/error/mixed-linear-manufactured.h>
#include <ratel/qfunctions/forcing/mixed-linear-manufactured.h>
#include <ratel/qfunctions/geometry/surface-force.h>
#include <ratel/qfunctions/geometry/surface.h>
#include <ratel/qfunctions/geometry/volumetric.h>
#include <ratel/qfunctions/models/elasticity-common-utt.h>
#include <ratel/qfunctions/models/elasticity-linear-dual-diagnostic.h>
#include <ratel/qfunctions/models/elasticity-mixed-linear.h>
#include <stddef.h>

static const CeedInt      active_field_sizes[]          = {3, 1};
static const char *const  active_field_names[]          = {"displacement", "pressure"};
static const char *const  active_component_names[]      = {"u_x", "u_y", "u_z", "p"};
static const CeedInt      active_field_num_eval_modes[] = {NUM_ACTIVE_FIELD_EVAL_MODES_MixedLinear_u, NUM_ACTIVE_FIELD_EVAL_MODES_MixedLinear_p};
static const CeedEvalMode active_field_eval_modes[]     = {CEED_EVAL_GRAD, CEED_EVAL_INTERP};
static const CeedInt      utt_field_num_eval_modes[]    = {1};
static const CeedEvalMode utt_field_eval_modes[]        = {CEED_EVAL_INTERP};

static const char *const projected_diagnostic_component_names[] = {"displacement_x",
                                                                   "displacement_y",
                                                                   "displacement_z",
                                                                   "Cauchy_stress_xx",
                                                                   "Cauchy_stress_xy",
                                                                   "Cauchy_stress_xz",
                                                                   "Cauchy_stress_yy",
                                                                   "Cauchy_stress_yz",
                                                                   "Cauchy_stress_zz",
                                                                   "pressure",
                                                                   "volumetric_strain",
                                                                   "trace_E2",
                                                                   "J",
                                                                   "strain_energy_density",
                                                                   "von_Mises_stress",
                                                                   "mass_density"};

struct RatelModelData_private elasticity_mixed_linear_data_private = {
    .name                                 = "Mixed linear elasticity",
    .command_line_option                  = "elasticity-mixed-linear",
    .setup_q_data_volume                  = SetupVolumeGeometry,
    .setup_q_data_volume_loc              = SetupVolumeGeometry_loc,
    .setup_q_data_surface                 = SetupSurfaceGeometry,
    .setup_q_data_surface_loc             = SetupSurfaceGeometry_loc,
    .setup_q_data_surface_grad            = SetupSurfaceForceGeometry,
    .setup_q_data_surface_grad_loc        = SetupSurfaceForceGeometry_loc,
    .q_data_volume_size                   = Q_DATA_VOLUMETRIC_GEOMETRY_SIZE,
    .q_data_surface_size                  = Q_DATA_SURFACE_GEOMETRY_SIZE,
    .q_data_surface_grad_size             = Q_DATA_SURFACE_FORCE_GEOMETRY_SIZE,
    .num_forcing                          = 1,
    .num_active_fields                    = sizeof(active_field_sizes) / sizeof(*active_field_sizes),
    .active_field_sizes                   = active_field_sizes,
    .active_field_names                   = active_field_names,
    .active_component_names               = active_component_names,
    .active_field_num_eval_modes          = active_field_num_eval_modes,
    .active_field_eval_modes              = active_field_eval_modes,
    .utt_field_num_eval_modes             = utt_field_num_eval_modes,
    .utt_field_eval_modes                 = utt_field_eval_modes,
    .num_comp_projected_diagnostic        = NUM_COMPONENTS_DIAGNOSTIC_MixedLinear,
    .projected_diagnostic_component_names = projected_diagnostic_component_names,
    .num_comp_dual_diagnostic             = NUM_COMPONENTS_ELASTICITY_LINEAR_DIAGNOSTIC_Dual,
    .dual_diagnostic_component_names      = RatelElasticityDualDiagnosticComponentNames,
    .quadrature_mode                      = CEED_GAUSS,
    .residual_u                           = ElasticityResidual_MixedLinear,
    .residual_u_loc                       = ElasticityResidual_MixedLinear_loc,
    .residual_utt                         = ElasticityResidual_utt,
    .residual_utt_loc                     = ElasticityResidual_utt_loc,
    .jacobian                             = ElasticityJacobian_MixedLinear,
    .jacobian_loc                         = ElasticityJacobian_MixedLinear_loc,
    .jacobian_block                       = {ElasticityPC_uu_MixedLinear,         ElasticityPC_pp_MixedLinear        },
    .jacobian_block_loc                   = {ElasticityPC_uu_MixedLinear_loc,     ElasticityPC_pp_MixedLinear_loc    },
    .platen_residual_u                    = PlatenBCsResidual_MixedLinear,
    .platen_residual_u_loc                = PlatenBCsResidual_MixedLinear_loc,
    .platen_jacobian                      = PlatenBCsJacobian_MixedLinear,
    .platen_jacobian_loc                  = PlatenBCsJacobian_MixedLinear_loc,
    .strain_energy                        = StrainEnergy_MixedLinear,
    .strain_energy_loc                    = StrainEnergy_MixedLinear_loc,
    .projected_diagnostic                 = Diagnostic_MixedLinear,
    .projected_diagnostic_loc             = Diagnostic_MixedLinear_loc,
    .dual_diagnostic                      = ElasticityDualDiagnostic_Linear,
    .dual_diagnostic_loc                  = ElasticityDualDiagnostic_Linear_loc,
    .mms_boundary                         = {MMSBCs_MixedLinear,                  NULL                               },
    .mms_boundary_loc                     = {MMSBCs_MixedLinear_loc,              NULL                               },
    .mms_error                            = MMSError_MixedLinear,
    .mms_error_loc                        = MMSError_MixedLinear_loc,
    .mms_forcing                          = MMSForce_MixedLinear,
    .mms_forcing_loc                      = MMSForce_MixedLinear_loc,
    .mms_forcing_energy                   = MMSForceEnergy_MixedLinear,
    .mms_forcing_energy_loc               = MMSForceEnergy_MixedLinear_loc,
    .flops_qf_jacobian_u                  = FLOPS_JACOBIAN_MixedLinear,
    .flops_qf_jacobian_utt                = FLOPS_ScaledMass,
    .flops_qf_jacobian_platen             = FLOPS_Platen_without_df1 + FLOPS_JACOBIAN_MixedLinear,
    .flops_qf_jacobian_block              = {FLOPS_JACOBIAN_Block_uu_MixedLinear, FLOPS_JACOBIAN_Block_pp_MixedLinear},
};
RatelModelData elasticity_mixed_linear_data = &elasticity_mixed_linear_data_private;

/// @addtogroup RatelMaterials
/// @{

/**
  @brief Create `RatelMaterial` model data for linear elasticity.

  Not collective across MPI processes.

  @param[in]   ratel     `Ratel` context
  @param[out]  material  `RatelMaterial` context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialCreate_ElasticityMixedLinear(Ratel ratel, RatelMaterial material) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Create Elasticity Mixed Linear"));

  // Model data
  PetscCall(RatelModelDataVerifyRelativePath(ratel, elasticity_mixed_linear_data));
  material->model_data = elasticity_mixed_linear_data;

  // QFunction contexts
  PetscCall(RatelMaterialParamsContextCreate_MixedNeoHookean(material, &material->ctx_params));
  PetscCall(RatelMaterialParamsSmootherDataSetup_MixedNeoHookean(material));
  PetscCall(RatelMMSParamsContextCreate_Elasticity_MixedLinear(material, &material->ctx_mms_params));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Create Mixed Elasticity Linear Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Register mixed linear elastic model.

  Not collective across MPI processes.

  @param[in]   ratel                      `Ratel` context
  @param[in]   cl_argument                Command line argument to use for model
  @param[out]  material_create_functions  PETSc function list for `RatelMaterial`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelRegisterModel_ElasticityMixedLinear(Ratel ratel, const char *cl_argument, PetscFunctionList *material_create_functions) {
  PetscFunctionBeginUser;
  RATEL_MODEL_REGISTER(material_create_functions, cl_argument, ElasticityMixedLinear);
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}
