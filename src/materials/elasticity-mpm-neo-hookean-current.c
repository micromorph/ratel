/// @file
/// Hyperelastic material at finite strain using Neo-Hookean model in current configuration, with variable material parameters at points

#include <ceed.h>
#include <petscsys.h>
#include <ratel-impl.h>
#include <ratel-model.h>
#include <ratel.h>
#include <ratel/qfunctions/geometry/surface-force.h>
#include <ratel/qfunctions/geometry/surface.h>
#include <ratel/qfunctions/geometry/volumetric-mpm.h>
#include <ratel/qfunctions/models/elasticity-dual-diagnostic.h>
#include <ratel/qfunctions/models/elasticity-mpm-neo-hookean-current.h>

static const CeedInt      active_field_sizes[]          = {3};
static const char *const  active_field_names[]          = {"displacement"};
static const char *const  active_component_names[]      = {"u_x", "u_y", "u_z"};
static const CeedInt      active_field_num_eval_modes[] = {NUM_ACTIVE_FIELD_EVAL_MODES_MPM_NeoHookeanCurrent};
static const CeedEvalMode active_field_eval_modes[]     = {CEED_EVAL_GRAD};

static const CeedInt point_field_sizes[] = {1, 1, 1};
static const char   *point_field_names[] = {"rho", "mu", "bulk"};

static const char *const projected_diagnostic_component_names[] = {"displacement_x",
                                                                   "displacement_y",
                                                                   "displacement_z",
                                                                   "Cauchy_stress_xx",
                                                                   "Cauchy_stress_xy",
                                                                   "Cauchy_stress_xz",
                                                                   "Cauchy_stress_yy",
                                                                   "Cauchy_stress_yz",
                                                                   "Cauchy_stress_zz",
                                                                   "pressure",
                                                                   "volumetric_strain",
                                                                   "trace_E2",
                                                                   "J",
                                                                   "strain_energy_density",
                                                                   "von_Mises_stress",
                                                                   "mass_density"};

struct RatelModelData_private elasticity_MPM_Neo_Hookean_current_data_private = {
    .name                                 = "Neo-Hookean hyperelasticity at finite strain, in current configuration, with MPM",
    .command_line_option                  = "elasticity-mpm-neo-hookean-current",
    .setup_q_data_volume_mpm              = SetupVolumeGeometryMPM,
    .setup_q_data_volume_mpm_loc          = SetupVolumeGeometryMPM_loc,
    .setup_q_data_surface                 = SetupSurfaceGeometry,
    .setup_q_data_surface_loc             = SetupSurfaceGeometry_loc,
    .setup_q_data_surface_grad            = SetupSurfaceForceGeometry,
    .setup_q_data_surface_grad_loc        = SetupSurfaceForceGeometry_loc,
    .q_data_volume_size                   = Q_DATA_VOLUMETRIC_GEOMETRY_MPM_SIZE,
    .q_data_surface_size                  = Q_DATA_SURFACE_GEOMETRY_SIZE,
    .q_data_surface_grad_size             = Q_DATA_SURFACE_FORCE_GEOMETRY_SIZE,
    .num_forcing                          = 1,
    .num_active_fields                    = sizeof(active_field_sizes) / sizeof(*active_field_sizes),
    .active_field_sizes                   = active_field_sizes,
    .active_field_names                   = active_field_names,
    .active_component_names               = active_component_names,
    .active_field_num_eval_modes          = active_field_num_eval_modes,
    .active_field_eval_modes              = active_field_eval_modes,
    .num_point_fields                     = sizeof(point_field_sizes) / sizeof(*point_field_sizes),
    .point_field_sizes                    = point_field_sizes,
    .point_field_names                    = point_field_names,
    .num_comp_stored_u                    = NUM_COMPONENTS_STORED_MPM_NeoHookeanCurrent,
    .num_comp_state                       = NUM_COMPONENTS_STATE_MPM_NeoHookeanCurrent,
    .num_comp_projected_diagnostic        = NUM_COMPONENTS_DIAGNOSTIC_Elasticity,
    .projected_diagnostic_component_names = projected_diagnostic_component_names,
    .num_comp_dual_diagnostic             = NUM_COMPONENTS_ELASTICITY_DIAGNOSTIC_Dual,
    .dual_diagnostic_component_names      = RatelElasticityDualDiagnosticComponentNames,
    .quadrature_mode                      = CEED_GAUSS,
    .residual_u                           = ElasticityResidual_MPM_NeoHookeanCurrent,
    .residual_u_loc                       = ElasticityResidual_MPM_NeoHookeanCurrent_loc,
    .jacobian                             = ElasticityJacobian_MPM_NeoHookeanCurrent,
    .jacobian_loc                         = ElasticityJacobian_MPM_NeoHookeanCurrent_loc,
    .strain_energy                        = StrainEnergy_MPM_NeoHookean,
    .strain_energy_loc                    = StrainEnergy_MPM_NeoHookean_loc,
    .projected_diagnostic                 = Diagnostic_MPM_NeoHookean,
    .projected_diagnostic_loc             = Diagnostic_MPM_NeoHookean_loc,
    .dual_diagnostic                      = ElasticityDualDiagnostic,
    .dual_diagnostic_loc                  = ElasticityDualDiagnostic_loc,
    .set_point_fields                     = SetPointFields_MPM_NeoHookean,
    .set_point_fields_loc                 = SetPointFields_MPM_NeoHookean_loc,
    .update_volume_mpm                    = UpdateVolume_MPM,
    .update_volume_mpm_loc                = UpdateVolume_MPM_loc,
    .flops_qf_jacobian_u                  = FLOPS_JACOBIAN_MPM_NeoHookeanCurrent,
};
RatelModelData elasticity_MPM_Neo_Hookean_current_data = &elasticity_MPM_Neo_Hookean_current_data_private;

/// @addtogroup RatelMaterials
/// @{

/**
  @brief Create `RatelMaterial` model data for Neo-Hookean hyperelasticity at finite strain in curent configuration.

  Not collective across MPI processes.

  @param[in]   ratel     `Ratel` context
  @param[out]  material  `RatelMaterial` context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialCreate_ElasticityMPMNeoHookeanCurrent(Ratel ratel, RatelMaterial material) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Create Elasticity Neo-Hookean Current"));

  // Model data
  PetscCall(RatelModelDataVerifyRelativePath(ratel, elasticity_MPM_Neo_Hookean_current_data));
  material->model_data = elasticity_MPM_Neo_Hookean_current_data;

  // QFunction contexts
  PetscCall(RatelMaterialParamsContextCreate_NeoHookean(material, &material->ctx_params));
  PetscCall(RatelMaterialParamsSmootherDataSetup_NeoHookean(material));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Create Elasticity Neo-Hookean Current Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Register Neo-Hookean hyperelasticity at finite strain in curent configuration model.

  Not collective across MPI processes.

  @param[in]   ratel                      `Ratel` context
  @param[in]   cl_argument                Command line argument to use for model
  @param[out]  material_create_functions  PETSc function list for `RatelMaterial`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelRegisterModel_ElasticityMPMNeoHookeanCurrent(Ratel ratel, const char *cl_argument, PetscFunctionList *material_create_functions) {
  PetscFunctionBeginUser;
  RATEL_MODEL_REGISTER(material_create_functions, cl_argument, ElasticityMPMNeoHookeanCurrent);
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}
