/// @file
/// Setup Ratel models

#include <ceed.h>
#include <petscsys.h>
#include <ratel-impl.h>
#include <ratel-model.h>
#include <ratel.h>

const char *const RatelElasticityDualDiagnosticComponentNames[] = {"nodal_volume", "nodal_density"};

#define RATEL_MODEL(model_cl_argument, model_postfix) \
  RATEL_INTERN PetscErrorCode RatelRegisterModel_##model_postfix(Ratel, const char *, PetscFunctionList *);

#include "ratel-model-list.h"
#undef RATEL_MODEL

/// @addtogroup RatelMaterials
/// @{

/**
  @brief Register setup functions for models.

  Collective across MPI processes.

  @param[in]   ratel                      `Ratel` context
  @param[out]  material_create_functions  Function list for creating `RatelMaterial`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelRegisterModels(Ratel ratel, PetscFunctionList *material_create_functions) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Register Models"));

#define RATEL_MODEL(model_cl_argument, model_postfix) \
  PetscCall(RatelRegisterModel_##model_postfix(ratel, model_cl_argument, material_create_functions));

#include "ratel-model-list.h"
#undef RATEL_MODEL

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Register Models Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Verify all `CeedQFunction` source paths are relative to `Ratel` JiT root directory.

  Not collective across MPI processes.

  @param[in]      ratel       `Ratel` context
  @param[in,out]  model_data  Model data to check `CeedQFunction` source paths

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelModelDataVerifyRelativePath(Ratel ratel, RatelModelData model_data) {
  PetscFunctionBeginUser;
  // QData
  PetscCheck(model_data->setup_q_data_volume_loc || model_data->setup_q_data_volume_mpm_loc, ratel->comm, PETSC_ERR_LIB,
             "Model data %s is missing volumetric qdata setup QFunction source path", model_data->name);
  if (model_data->setup_q_data_volume_loc) model_data->setup_q_data_volume_loc = RatelQFunctionRelativePath(model_data->setup_q_data_volume_loc);
  if (model_data->setup_q_data_volume_mpm_loc) {
    model_data->setup_q_data_volume_loc = RatelQFunctionRelativePath(model_data->setup_q_data_volume_mpm_loc);
  }
  if (model_data->setup_q_data_surface_loc) model_data->setup_q_data_surface_loc = RatelQFunctionRelativePath(model_data->setup_q_data_surface_loc);
  if (model_data->setup_q_data_surface_grad_loc) {
    model_data->setup_q_data_surface_grad_loc = RatelQFunctionRelativePath(model_data->setup_q_data_surface_grad_loc);
  }
  // Residual
  PetscCheck(model_data->residual_u_loc, ratel->comm, PETSC_ERR_LIB, "Model data %s is missing residual u term QFunction source path",
             model_data->name);
  model_data->residual_u_loc = RatelQFunctionRelativePath(model_data->residual_u_loc);
  if (model_data->residual_ut_loc) model_data->residual_ut_loc = RatelQFunctionRelativePath(model_data->residual_ut_loc);
  if (model_data->residual_utt_loc) model_data->residual_utt_loc = RatelQFunctionRelativePath(model_data->residual_utt_loc);
  // Jacobian
  PetscCheck(model_data->jacobian_loc, ratel->comm, PETSC_ERR_LIB, "Model data %s is missing Jacobian term QFunction source path", model_data->name);
  model_data->jacobian_loc = RatelQFunctionRelativePath(model_data->jacobian_loc);
  for (PetscInt i = 0; i < RATEL_MAX_FIELDS; i++) {
    if (model_data->jacobian_block_loc[i]) model_data->jacobian_block_loc[i] = RatelQFunctionRelativePath(model_data->jacobian_block_loc[i]);
  }
  // Energy
  if (model_data->strain_energy_loc) model_data->strain_energy_loc = RatelQFunctionRelativePath(model_data->strain_energy_loc);
  // Diagnostics
  PetscCheck(model_data->projected_diagnostic_loc, ratel->comm, PETSC_ERR_LIB, "Model data %s is missing projected diagnostic QFunction source path",
             model_data->name);
  model_data->projected_diagnostic_loc = RatelQFunctionRelativePath(model_data->projected_diagnostic_loc);
  PetscCheck(model_data->dual_diagnostic_loc, ratel->comm, PETSC_ERR_LIB, "Model data %s is missing dual space diagnostic QFunction source path",
             model_data->name);
  model_data->dual_diagnostic_loc = RatelQFunctionRelativePath(model_data->dual_diagnostic_loc);
  // MMS
  for (PetscInt i = 0; i < RATEL_MAX_FIELDS; i++) {
    if (model_data->mms_boundary_loc[i]) model_data->mms_boundary_loc[i] = RatelQFunctionRelativePath(model_data->mms_boundary_loc[i]);
  }
  if (model_data->mms_forcing_loc) model_data->mms_forcing_loc = RatelQFunctionRelativePath(model_data->mms_forcing_loc);
  if (model_data->mms_error_loc) model_data->mms_error_loc = RatelQFunctionRelativePath(model_data->mms_error_loc);
  if (model_data->mms_forcing_energy_loc) model_data->mms_forcing_energy_loc = RatelQFunctionRelativePath(model_data->mms_forcing_energy_loc);
  // Platens
  if (model_data->platen_residual_u_loc) model_data->platen_residual_u_loc = RatelQFunctionRelativePath(model_data->platen_residual_u_loc);
  if (model_data->platen_jacobian_loc) model_data->platen_jacobian_loc = RatelQFunctionRelativePath(model_data->platen_jacobian_loc);
  // MPM
  if (model_data->setup_q_data_volume_mpm_loc) {
    model_data->setup_q_data_volume_mpm_loc = RatelQFunctionRelativePath(model_data->setup_q_data_volume_mpm_loc);
  }
  if (model_data->set_point_fields_loc) model_data->set_point_fields_loc = RatelQFunctionRelativePath(model_data->set_point_fields_loc);
  if (model_data->update_volume_mpm_loc) model_data->update_volume_mpm_loc = RatelQFunctionRelativePath(model_data->update_volume_mpm_loc);
  // Other
  if (model_data->surface_force_cell_to_face_loc) {
    model_data->surface_force_cell_to_face_loc = RatelQFunctionRelativePath(model_data->surface_force_cell_to_face_loc);
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get the default scalar value from model parameter data.

  Not collective across MPI processes.

  @param[in]   ratel       `Ratel` context
  @param[in]   params_data  Model parameter data to check
  @param[in]   name         Parameter name
  @param[out]  value        Variable to store default scalar value

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelModelParameterDataGetDefaultValue_Scalar(Ratel ratel, RatelModelParameterData params_data, const char *name, CeedScalar *value) {
  PetscFunctionBeginUser;
  *value = 0.0;
  for (PetscInt i = 0; i < params_data->num_parameters; i++) {
    PetscBool is_parameter = PETSC_FALSE;

    PetscCall(PetscStrcmp(params_data->parameters[i].name, name, &is_parameter));
    if (is_parameter) {
      PetscCheck(params_data->parameters[i].type == CEED_CONTEXT_FIELD_DOUBLE, ratel->comm, PETSC_ERR_SUP, "Parameter %s is not a scalar", name);
      *value = params_data->parameters[i].default_value.s;
    }
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get the default int value from model parameter data.

  Not collective across MPI processes.

  @param[in]   ratel       `Ratel` context
  @param[in]   params_data  Model parameter data to check
  @param[in]   name         Parameter name
  @param[out]  value        Variable to store default int value

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelModelParameterDataGetDefaultValue_Int(Ratel ratel, RatelModelParameterData params_data, const char *name, CeedInt *value) {
  PetscFunctionBeginUser;
  *value = 0;
  for (PetscInt i = 0; i < params_data->num_parameters; i++) {
    PetscBool is_parameter = PETSC_FALSE;

    PetscCall(PetscStrcmp(params_data->parameters[i].name, name, &is_parameter));
    if (is_parameter) {
      PetscCheck(params_data->parameters[i].type == CEED_CONTEXT_FIELD_INT32, ratel->comm, PETSC_ERR_SUP, "Parameter %s is not an int", name);
      *value = params_data->parameters[i].default_value.i;
    }
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get the default bool value from model parameter data.

  Not collective across MPI processes.

  @param[in]   ratel       `Ratel` context
  @param[in]   params_data  Model parameter data to check
  @param[in]   name         Parameter name
  @param[out]  value        Variable to store default int value

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelModelParameterDataGetDefaultValue_Bool(Ratel ratel, RatelModelParameterData params_data, const char *name, PetscBool *value) {
  PetscFunctionBeginUser;
  *value = PETSC_FALSE;
  for (PetscInt i = 0; i < params_data->num_parameters; i++) {
    PetscBool is_parameter = PETSC_FALSE;

    PetscCall(PetscStrcmp(params_data->parameters[i].name, name, &is_parameter));
    if (is_parameter) {
      PetscCheck(params_data->parameters[i].type == CEED_CONTEXT_FIELD_BOOL, ratel->comm, PETSC_ERR_SUP, "Parameter %s is not a bool", name);
      *value = params_data->parameters[i].default_value.b;
    }
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Register model parameters as fields in a `CeedQFunctionContext`.

  Not collective across MPI processes.

  @param[in]   ratel       `Ratel` context
  @param[in]   parameters  Parameters to register
  @param[out]  ctx         `CeedQFunctionContext` to register parameters in

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelModelParameterDataRegisterContextFields(Ratel ratel, RatelModelParameterData parameters, CeedQFunctionContext ctx) {
  PetscFunctionBeginUser;
  // Register parameters
  for (PetscInt i = 0; i < parameters->num_parameters; i++) {
    RatelModelParameter parameter      = parameters->parameters[i];
    CeedInt             num_components = parameter.num_components > 0 ? parameter.num_components : 1;

    switch (parameter.type) {
      case CEED_CONTEXT_FIELD_INT32:
        RatelCallCeed(ratel, CeedQFunctionContextRegisterInt32(ctx, parameter.name, parameter.offset, num_components, parameter.description));
        break;
      case CEED_CONTEXT_FIELD_BOOL:
        RatelCallCeed(ratel, CeedQFunctionContextRegisterBoolean(ctx, parameter.name, parameter.offset, num_components, parameter.description));
        break;
      case CEED_CONTEXT_FIELD_DOUBLE:
      default:
        // Default to double
        RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(ctx, parameter.name, parameter.offset, num_components, parameter.description));
        break;
    }
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}
