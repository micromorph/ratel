// List each model registration function once here.
// The RATEL_MODEL macro is used to to call each registration function in the order listed in `ratel-model.c`.
// Also, the macro creates weak symbols in `ratel-model-weak.c` for each material model.
//
// The macro takes two arguments:
// RATEL_MODEL("command-line-argument", ModelSuffix)
// The command line argument to select the material model is given by `command-line-argument`.
// The source file `material-model.c` must define `RatelMaterialCreate_ModelSuffix` and `RatelRegisterModel_ModelSuffix`.
// Inside of `RatelRegisterModel_ModelSuffix`, use the RATEL_MODEL_REGISTER macro.

#include <ratel.h>

// CEED BPs
RATEL_MODEL("ceed-bp1", CEED_BP1)
RATEL_MODEL("ceed-bp2", CEED_BP2)
RATEL_MODEL("ceed-bp3", CEED_BP3)
RATEL_MODEL("ceed-bp4", CEED_BP4)

// Elasticity
RATEL_MODEL("elasticity-linear", ElasticityLinear)
RATEL_MODEL("elasticity-mixed-linear", ElasticityMixedLinear)
RATEL_MODEL("elasticity-neo-hookean-current", ElasticityNeoHookeanCurrent)
RATEL_MODEL("elasticity-neo-hookean-current-ad-enzyme", ElasticityNeoHookeanCurrentAD_Enzyme)
RATEL_MODEL("elasticity-neo-hookean-current-ad-adolc", ElasticityNeoHookeanCurrentAD_ADOLC)
RATEL_MODEL("elasticity-neo-hookean-initial", ElasticityNeoHookeanInitial)
RATEL_MODEL("elasticity-neo-hookean-initial-ad-enzyme", ElasticityNeoHookeanInitialAD_Enzyme)
RATEL_MODEL("elasticity-neo-hookean-initial-ad-adolc", ElasticityNeoHookeanInitialAD_ADOLC)
RATEL_MODEL("elasticity-mooney-rivlin-initial", ElasticityMooneyRivlinInitial)
RATEL_MODEL("elasticity-mooney-rivlin-current", ElasticityMooneyRivlinCurrent)
RATEL_MODEL("elasticity-isochoric-neo-hookean-initial", ElasticityIsochoricNeoHookeanInitial)
RATEL_MODEL("elasticity-isochoric-neo-hookean-current", ElasticityIsochoricNeoHookeanCurrent)
RATEL_MODEL("elasticity-isochoric-mooney-rivlin-initial", ElasticityIsochoricMooneyRivlinInitial)
RATEL_MODEL("elasticity-isochoric-mooney-rivlin-current", ElasticityIsochoricMooneyRivlinCurrent)
RATEL_MODEL("elasticity-isochoric-ogden-initial", ElasticityIsochoricOgdenInitial)
RATEL_MODEL("elasticity-isochoric-ogden-current", ElasticityIsochoricOgdenCurrent)
RATEL_MODEL("elasticity-mixed-neo-hookean-initial", ElasticityMixedNeoHookeanInitial)
RATEL_MODEL("elasticity-mixed-neo-hookean-current", ElasticityMixedNeoHookeanCurrent)
RATEL_MODEL("elasticity-mixed-neo-hookean-PL-initial", ElasticityMixedNeoHookeanPLInitial)
RATEL_MODEL("elasticity-mixed-neo-hookean-PL-current", ElasticityMixedNeoHookeanPLCurrent)
RATEL_MODEL("elasticity-mixed-ogden-initial", ElasticityMixedOgdenInitial)
RATEL_MODEL("elasticity-hencky-initial-principal", ElasticityHenckyInitialPrincipal)
RATEL_MODEL("elasticity-hencky-current-principal", ElasticityHenckyCurrentPrincipal)

// MPM
RATEL_MODEL("elasticity-mpm-neo-hookean-current", ElasticityMPMNeoHookeanCurrent)
RATEL_MODEL("elasticity-mpm-neo-hookean-damage-current", ElasticityMPMNeoHookeanDamageCurrent)

// Plasticity
RATEL_MODEL("plasticity-linear", PlasticityLinear)
RATEL_MODEL("plasticity-hencky-initial", PlasticityHenckyInitial)
RATEL_MODEL("plasticity-hencky-current", PlasticityHenckyCurrent)
RATEL_MODEL("plasticity-hencky-initial-principal", PlasticityHenckyInitialPrincipal)
RATEL_MODEL("plasticity-hencky-current-principal", PlasticityHenckyCurrentPrincipal)

// Brittle Fracture
RATEL_MODEL("elasticity-linear-damage", ElasticityLinearDamage)
RATEL_MODEL("elasticity-neo-hookean-damage-initial", ElasticityNeoHookeanDamageInitial)
RATEL_MODEL("elasticity-neo-hookean-damage-current", ElasticityNeoHookeanDamageCurrent)
RATEL_MODEL("elasticity-hencky-damage-initial-principal", ElasticityHenckyDamageInitialPrincipal)
RATEL_MODEL("elasticity-hencky-damage-current-principal", ElasticityHenckyDamageCurrentPrincipal)

// PoroElasticity
RATEL_MODEL("poroelasticity-linear", PoroElasticityLinear)
RATEL_MODEL("poroelasticity-neo-hookean-current", PoroElasticityNeoHookeanCurrent)
