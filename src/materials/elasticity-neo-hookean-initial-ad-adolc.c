/// @file
/// Hyperelastic material at finite strain using Neo-Hookean model in initial configuration

#include <ceed.h>
#include <petscsys.h>
#include <ratel-impl.h>
#include <ratel-model.h>
#include <ratel.h>
#include <ratel/qfunctions/geometry/surface-force.h>
#include <ratel/qfunctions/geometry/surface.h>
#include <ratel/qfunctions/geometry/volumetric.h>
#include <ratel/qfunctions/models/elasticity-common-utt.h>
#include <ratel/qfunctions/models/elasticity-dual-diagnostic.h>
#include <ratel/qfunctions/models/elasticity-neo-hookean-cell-to-face.h>
#include <ratel/qfunctions/models/elasticity-neo-hookean-common.h>
#include <ratel/qfunctions/models/elasticity-neo-hookean-initial.h>

static const CeedInt      active_field_sizes[]       = {3};
static const char *const  active_field_names[]       = {"displacement"};
static const char *const  active_component_names[]   = {"u_x", "u_y", "u_z"};
static const CeedEvalMode active_field_eval_modes[]  = {CEED_EVAL_GRAD};
static const CeedInt      utt_field_num_eval_modes[] = {1};
static const CeedEvalMode utt_field_eval_modes[]     = {CEED_EVAL_INTERP};

static const char *const projected_diagnostic_component_names[] = {"displacement_x",
                                                                   "displacement_y",
                                                                   "displacement_z",
                                                                   "Cauchy_stress_xx",
                                                                   "Cauchy_stress_xy",
                                                                   "Cauchy_stress_xz",
                                                                   "Cauchy_stress_yy",
                                                                   "Cauchy_stress_yz",
                                                                   "Cauchy_stress_zz",
                                                                   "pressure",
                                                                   "volumetric_strain",
                                                                   "trace_E2",
                                                                   "J",
                                                                   "strain_energy_density",
                                                                   "von_Mises_stress",
                                                                   "mass_density"};

struct RatelModelData_private elasticity_Neo_Hookean_initial_ad_adolc_data_private = {
    .name                                 = "Neo-Hookean hyperelasticity at finite strain, in initial configuration using ADOL-C",
    .command_line_option                  = "elasticity-neo-hookean-initial-ad-adolc",
    .setup_q_data_volume                  = SetupVolumeGeometry,
    .setup_q_data_surface                 = SetupSurfaceGeometry,
    .setup_q_data_surface_grad            = SetupSurfaceForceGeometry,
    .residual_ut                          = NULL,
    .residual_utt                         = ElasticityResidual_utt,
    .jacobian_block                       = {NULL},
    .strain_energy                        = StrainEnergy_NeoHookean,
    .projected_diagnostic                 = Diagnostic_NeoHookean,
    .dual_diagnostic                      = ElasticityDualDiagnostic,
    .mms_boundary                         = {NULL},
    .mms_forcing                          = NULL,
    .mms_error                            = NULL,
    .mms_forcing_energy                   = NULL,
    .platen_residual_u                    = PlatenBCsResidual_NeoHookeanInitial,
    .platen_jacobian                      = PlatenBCsJacobian_NeoHookeanInitial,
    .setup_q_data_volume_mpm              = NULL,
    .set_point_fields                     = NULL,
    .update_volume_mpm                    = NULL,
    .surface_force_cell_to_face           = SurfaceForceCellToFace_NeoHookean,
    .setup_q_data_volume_loc              = SetupVolumeGeometry_loc,
    .setup_q_data_surface_loc             = SetupSurfaceGeometry_loc,
    .setup_q_data_surface_grad_loc        = SetupSurfaceForceGeometry_loc,
    .residual_ut_loc                      = NULL,
    .residual_utt_loc                     = ElasticityResidual_utt_loc,
    .jacobian_block_loc                   = {NULL},
    .strain_energy_loc                    = StrainEnergy_NeoHookean_loc,
    .projected_diagnostic_loc             = Diagnostic_NeoHookean_loc,
    .dual_diagnostic_loc                  = ElasticityDualDiagnostic_loc,
    .mms_boundary_loc                     = {NULL},
    .mms_forcing_loc                      = NULL,
    .mms_error_loc                        = NULL,
    .mms_forcing_energy_loc               = NULL,
    .platen_residual_u_loc                = PlatenBCsResidual_NeoHookeanInitial_loc,
    .platen_jacobian_loc                  = PlatenBCsJacobian_NeoHookeanInitial_loc,
    .setup_q_data_volume_mpm_loc          = NULL,
    .set_point_fields_loc                 = NULL,
    .update_volume_mpm_loc                = NULL,
    .surface_force_cell_to_face_loc       = SurfaceForceCellToFace_NeoHookean_loc,
    .num_active_fields                    = sizeof(active_field_sizes) / sizeof(*active_field_sizes),
    .num_point_fields                     = 0,
    .num_comp_stored_platen               = NUM_COMPONENTS_STORED_NeoHookeanInitial,
    .num_comp_state                       = 0,
    .num_comp_projected_diagnostic        = NUM_COMPONENTS_DIAGNOSTIC_Elasticity,
    .num_comp_dual_diagnostic             = NUM_COMPONENTS_ELASTICITY_DIAGNOSTIC_Dual,
    .q_data_volume_size                   = Q_DATA_VOLUMETRIC_GEOMETRY_SIZE,
    .q_data_surface_size                  = Q_DATA_SURFACE_GEOMETRY_SIZE,
    .q_data_surface_grad_size             = Q_DATA_SURFACE_FORCE_GEOMETRY_SIZE,
    .num_forcing                          = 1,
    .active_field_names                   = active_field_names,
    .point_field_names                    = NULL,
    .active_component_names               = active_component_names,
    .active_field_sizes                   = active_field_sizes,
    .ut_field_num_eval_modes              = NULL,
    .utt_field_num_eval_modes             = utt_field_num_eval_modes,
    .point_field_sizes                    = NULL,
    .active_field_eval_modes              = active_field_eval_modes,
    .ut_field_eval_modes                  = NULL,
    .utt_field_eval_modes                 = utt_field_eval_modes,
    .projected_diagnostic_component_names = projected_diagnostic_component_names,
    .dual_diagnostic_component_names      = RatelElasticityDualDiagnosticComponentNames,
    .quadrature_mode                      = CEED_GAUSS,
    .initial_random_scaling               = 0.0,
    .has_ut_term                          = PETSC_FALSE,
    .flops_qf_jacobian_u                  = FLOPS_JACOBIAN_NeoHookeanInitial,
    .flops_qf_jacobian_ut                 = 0,
    .flops_qf_jacobian_utt                = FLOPS_ScaledMass,
    .flops_qf_jacobian_platen             = FLOPS_Platen_without_df1 + FLOPS_df1_NH_Initial,
    .flops_qf_jacobian_block              = {0},
    .param_data                           = NULL,
    .mms_param_data                       = NULL};
RatelModelData elasticity_Neo_Hookean_initial_ad_adolc_data = &elasticity_Neo_Hookean_initial_ad_adolc_data_private;

/// @addtogroup RatelMaterials
/// @{

/**
  @brief Create `RatelMaterial` model data for Neo-Hookean hyperelasticity at finite strain in initial configuration.

  Not collective across MPI processes.

  @param[in]   ratel     `Ratel` context
  @param[out]  material  `RatelMaterial` context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialCreate_ElasticityNeoHookeanInitialAD_ADOLC_C(Ratel ratel, RatelMaterial material) {
  PetscFunctionBeginUser;

  // Model data
  material->model_data = elasticity_Neo_Hookean_initial_ad_adolc_data;
  (void)ElasticityResidual_NeoHookeanInitial;
  (void)ElasticityJacobian_NeoHookeanInitial;

  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}
