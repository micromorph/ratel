/// @file
/// Setup Ratel models

#include <ceed.h>
#include <ceed/backend.h>
#include <petscdm.h>
#include <ratel-boundary.h>
#include <ratel-dm.h>
#include <ratel-fem.h>
#include <ratel-impl.h>
#include <ratel-material.h>
#include <ratel-mpm.h>
#include <ratel.h>
#include <ratel/models/forcing.h>
#include <stdbool.h>
#include <string.h>

/// @addtogroup RatelMaterials
/// @{

/**
  @brief Create a `RatelMaterial` object.

  Collective across MPI processes.

  @param[in]   ratel          `Ratel` context
  @param[in]   material_name  Material name for command line prefix
  @param[out]  material       Address to store newly created `RatelMaterial`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialCreate(Ratel ratel, const char *material_name, RatelMaterial *material) {
  PetscErrorCode (*MaterialCreate)(Ratel, RatelMaterial);

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Create"));

  // Common setup
  PetscCall(PetscNew(material));
  (*material)->ratel = ratel;
  {
    // Material name
    char      *name_copy = NULL;
    PetscSizeT name_len  = 0;

    PetscCall(PetscStrlen(material_name, &name_len));
    if (name_len > 0) {
      PetscCall(PetscCalloc1(name_len + 1, &name_copy));
      PetscCall(PetscStrncpy(name_copy, material_name, name_len + 1));
    }
    (*material)->name = name_copy;
  }
  {
    // Default global options
    PetscOptionsBegin(ratel->comm, NULL, "", NULL);

    // -- Forcing function
    (*material)->forcing_type = RATEL_FORCING_NONE;
    PetscCall(PetscOptionsEnum("-forcing", "Set forcing function option", NULL, RatelForcingTypesCL, (PetscEnum)(*material)->forcing_type,
                               (PetscEnum *)&(*material)->forcing_type, NULL));

    PetscOptionsEnd();
  }
  {
    // CL options
    char *cl_prefix, *cl_message, *model_name[1];

    PetscCall(RatelMaterialGetCLPrefix(*material, &cl_prefix));
    PetscCall(RatelMaterialGetCLMessage(*material, &cl_message));
    PetscOptionsBegin(ratel->comm, cl_prefix, cl_message, NULL);

    // -- Material model
    PetscInt  max_strings = 1;
    PetscBool model_set;
    PetscCall(PetscOptionsStringArray("-model", "Material model", NULL, model_name, &max_strings, &model_set));
    PetscCheck(model_set, ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Material model not set for material %s", material_name);
    {
      char     *loc;
      PetscBool is_mpm;

      PetscCall(PetscStrstr(model_name[0], "-mpm-", &loc));
      is_mpm = !!loc;
      PetscCheck(!is_mpm || (is_mpm && ratel->method_type == RATEL_METHOD_MPM), ratel->comm, PETSC_ERR_SUP,
                 "Material model '%s' requires -method mpm", model_name[0]);
    }
    PetscCall(PetscFunctionListFind(ratel->material_create_functions, model_name[0], &MaterialCreate));
    PetscCheck(MaterialCreate, ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Model data '%s' for material '%s' not found", model_name[0], material_name);
    PetscCall(PetscFree(model_name[0]));

    // -- Name
    PetscBool volume_label_name_set = PETSC_FALSE;
    PetscCall(PetscOptionsStringArray("-label_name", "Mesh partitioning name for material", NULL, &(*material)->volume_label_name, &max_strings,
                                      &volume_label_name_set));

    // -- Use default, if needed
    if (!volume_label_name_set) {
      char       *volume_label_name_copy;
      const char *volume_label_name     = "Cell Sets";
      PetscSizeT  volume_label_name_len = 0;

      PetscCall(PetscStrlen(volume_label_name, &volume_label_name_len));
      PetscCall(PetscCalloc1(volume_label_name_len + 1, &volume_label_name_copy));
      PetscCall(PetscStrncpy(volume_label_name_copy, volume_label_name, volume_label_name_len + 1));
      (*material)->volume_label_name = volume_label_name_copy;
    }
    // -- Check for mismatch with previous label, if any
    if (ratel->material_volume_label_name) {
      PetscSizeT name_len    = 0;
      PetscBool  names_match = PETSC_TRUE;

      PetscCall(PetscStrlen(ratel->material_volume_label_name, &name_len));
      PetscCall(PetscStrncmp((*material)->volume_label_name, ratel->material_volume_label_name, name_len, &names_match));
      PetscCheck(names_match, ratel->comm, PETSC_ERR_SUP, "Label name set for material %s different than previous name set", material_name);
    } else {
      char      *volume_label_name_copy;
      PetscSizeT volume_label_name_len = 0;

      PetscCall(PetscStrlen((*material)->volume_label_name, &volume_label_name_len));
      PetscCall(PetscCalloc1(volume_label_name_len + 1, &volume_label_name_copy));
      PetscCall(PetscStrncpy(volume_label_name_copy, (*material)->volume_label_name, volume_label_name_len + 1));
      ratel->material_volume_label_name = volume_label_name_copy;
    }

    // -- Label values
    PetscBool volume_label_values_set    = PETSC_FALSE;
    (*material)->num_volume_label_values = RATEL_MAX_MATERIAL_LABEL_VALUES;
    PetscCall(PetscCalloc1((*material)->num_volume_label_values, &(*material)->volume_label_values));
    PetscCall(PetscOptionsIntArray("-label_value", "Mesh partitioning values for multi-materials", NULL, (*material)->volume_label_values,
                                   &(*material)->num_volume_label_values, &volume_label_values_set));
    if (!volume_label_values_set) {
      PetscCheck(ratel->num_materials == 1, ratel->comm, PETSC_ERR_SUP, "Label values must be set for %s material!", material_name);
      (*material)->num_volume_label_values = 1;
      (*material)->volume_label_values[0]  = -1;
    }

    // -- Forcing function
    PetscCall(PetscOptionsEnum("-forcing", "Set forcing function option", NULL, RatelForcingTypesCL, (PetscEnum)(*material)->forcing_type,
                               (PetscEnum *)&(*material)->forcing_type, NULL));

    PetscOptionsEnd();
    PetscCall(PetscFree(cl_prefix));
    PetscCall(PetscFree(cl_message));
  }
  // Set flag values
  (*material)->num_face_orientations                   = -1;
  (*material)->num_surface_grad_dm_face_ids            = -1;
  (*material)->num_surface_grad_diagnostic_dm_face_ids = -1;

  // Material specific setup
  PetscCall((*MaterialCreate)(ratel, *material));

  // Model type specific setup
  switch (ratel->method_type) {
    case RATEL_METHOD_FEM:
      PetscCall(RatelMaterialCreate_FEM(ratel, *material));
      break;
    case RATEL_METHOD_MPM:
      PetscCall(RatelMaterialCreate_MPM(ratel, *material));
      break;
    // LCOV_EXCL_START
    default:
      SETERRQ(ratel->comm, PETSC_ERR_SUP, "Model %s not implemented for material %s", (*material)->model_data->name, (*material)->name);
      // LCOV_EXCL_STOP
  }

  // Check if forcing type is compatible with model
  if ((*material)->forcing_type == RATEL_FORCING_BODY) {
    PetscBool is_small_strain;

    PetscCall(PetscStrInList((*material)->model_data->name, "elasticity-linear elasticity-mixed-linear plasticity-linear", ' ', &is_small_strain));
    PetscCheck(!is_small_strain, ratel->comm, PETSC_ERR_SUP,
               "Cannot use constant forcing and finite strain formulation. Constant forcing in reference frame currently unavailable.");
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Create Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief View a `RatelMaterial` object.

  Collective across MPI processes.

  @param[in]  material  `RatelMaterial` to view
  @param[in]  viewer    Visualization context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialView(RatelMaterial material, PetscViewer viewer) {
  const char *material_name, *model_name = NULL;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material View"));

  // Get names
  PetscCall(RatelMaterialGetMaterialName(material, &material_name));
  PetscCall(RatelMaterialGetModelName(material, &model_name));

  // View
  PetscCall(PetscViewerASCIIPrintf(viewer, "Ratel Material:\n"));
  PetscCall(PetscViewerASCIIPushTab(viewer));  // Ratel Material
  PetscCall(PetscViewerASCIIPrintf(viewer, "Name: %s\n", material_name ? material_name : "(none)"));
  PetscCall(RatelModelDataView(material->ratel, material->model_data, material->ctx_params, viewer));
  PetscCall(PetscViewerASCIIPrintf(viewer, "Forcing: %s\n", RatelForcingTypes[material->forcing_type]));
  PetscCall(PetscViewerASCIIPushTab(viewer));  // Forcing
  switch (material->forcing_type) {
    case RATEL_FORCING_NONE:
    case RATEL_FORCING_MMS:
      break;
    case RATEL_FORCING_BODY: {
      RatelForcingBodyParams params_forcing;

      PetscCall(RatelMaterialForcingBodyDataFromOptions(material, &params_forcing));
      PetscCall(PetscViewerASCIIPrintf(viewer, "%d acceleration vector(s) with %s\n", params_forcing.num_times,
                                       RatelBCInterpolationTypes[params_forcing.interpolation_type]));
      PetscCall(PetscViewerASCIIPushTab(viewer));  // Acceleration vectors
      for (PetscInt j = 0; j < params_forcing.num_times; j++) {
        PetscCall(PetscViewerASCIIPrintf(viewer, "t = %.3f: [%f, %f, %f]\n", params_forcing.times[j], params_forcing.acceleration[3 * j],
                                         params_forcing.acceleration[3 * j + 1], params_forcing.acceleration[3 * j + 2]));
      }
      PetscCall(PetscViewerASCIIPopTab(viewer));  // Acceleration vectors
      break;
    }
  }
  PetscCall(PetscViewerASCIIPopTab(viewer));  // Forcing
  PetscCall(PetscViewerASCIIPopTab(viewer));  // Ratel Material

  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material View Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief View material model parameters

  Collective across MPI processes.

  @param[in]  ratel       `Ratel` object for error handling
  @param[in]  model_data  `RatelModelData` to view
  @param[in]  context     Initialized `CeedQFunctionContext` with parameter values, or `NULL`
  @param[in]  viewer      `PetscViewer` to print to
**/
PetscErrorCode RatelModelDataView(Ratel ratel, RatelModelData model_data, CeedQFunctionContext context, PetscViewer viewer) {
  PetscInt eval_mode_index = 0;

  PetscFunctionBeginUser;
  PetscCall(PetscViewerASCIIPrintf(viewer, "Model: %s\n", model_data->name));
  PetscCall(PetscViewerASCIIPushTab(viewer));  // Ratel Model Data
  PetscCall(PetscViewerASCIIPrintf(viewer, "Command line option: %s\n", model_data->command_line_option));
  PetscCall(PetscViewerASCIIPrintf(viewer, "Active fields:\n"));
  PetscCall(PetscViewerASCIIPushTab(viewer));  // Active fields
  for (PetscInt i = 0; i < model_data->num_active_fields; i++) {
    PetscCall(PetscViewerASCIIPrintf(viewer, "Field %" PetscInt_FMT ":\n", i));
    PetscCall(PetscViewerASCIIPushTab(viewer));  // Field i
    PetscCall(PetscViewerASCIIPrintf(viewer, "Name:                 %s\n", model_data->active_field_names[i]));
    PetscCall(PetscViewerASCIIPrintf(viewer, "Number of components: %d\n", model_data->active_field_sizes[i]));
    PetscCall(PetscViewerASCIIPrintf(viewer, "libCEED eval mode(s): %s", CeedEvalModes[model_data->active_field_eval_modes[eval_mode_index]]));
    PetscCall(PetscViewerASCIIUseTabs(viewer, PETSC_FALSE));
    for (PetscInt j = 1; j < model_data->active_field_num_eval_modes[i]; j++) {
      PetscCall(PetscViewerASCIIPrintf(viewer, ", %s", CeedEvalModes[model_data->active_field_eval_modes[eval_mode_index + j]]));
    }
    eval_mode_index += model_data->active_field_num_eval_modes[i];
    PetscCall(PetscViewerASCIIPrintf(viewer, "\n"));
    PetscCall(PetscViewerASCIIUseTabs(viewer, PETSC_TRUE));
    PetscCall(PetscViewerASCIIPopTab(viewer));  // Field i
  }
  PetscCall(PetscViewerASCIIPopTab(viewer));  // Active fields
  if (model_data->num_comp_stored_u > 0) {
    PetscCall(PetscViewerASCIIPrintf(viewer, "Number of stored components: %d\n", model_data->num_comp_stored_u));
  }
  if (model_data->num_comp_state > 0) {
    PetscCall(PetscViewerASCIIPrintf(viewer, "Number of state components:  %d\n", model_data->num_comp_state));
  }
  PetscCall(PetscViewerASCIIPrintf(viewer, "Diagnostic output size:\n"));
  PetscCall(PetscViewerASCIIPushTab(viewer));  // Diagnostic output size
  PetscCall(PetscViewerASCIIPrintf(viewer, "Projected:  %d\n", model_data->num_comp_projected_diagnostic));
  PetscCall(PetscViewerASCIIPrintf(viewer, "Dual/Nodal: %d\n", model_data->num_comp_dual_diagnostic));
  PetscCall(PetscViewerASCIIPopTab(viewer));  // Diagnostic output size
  if (model_data->param_data) PetscCall(RatelModelParameterDataView(ratel, model_data->param_data, context, viewer));
  PetscCall(PetscViewerASCIIPopTab(viewer));  // Ratel Model Data
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief View material model parameters

  Collective across MPI processes.

  @param[in]  ratel       `Ratel` object for error handling
  @param[in]  param_data  `RatelModelParameterData` to view
  @param[in]  context     Initialized `CeedQFunctionContext` with parameter values, or `NULL`
  @param[in]  viewer      `PetscViewer` to print to
**/
PetscErrorCode RatelModelParameterDataView(Ratel ratel, RatelModelParameterData param_data, CeedQFunctionContext context, PetscViewer viewer) {
  PetscFunctionBeginUser;
  PetscCall(PetscViewerASCIIPrintf(viewer, "Model Parameters:\n"));
  PetscCall(PetscViewerASCIIPushTab(viewer));  // Model Parameters
  for (PetscInt i = 0; i < param_data->num_parameters; i++) {
    RatelModelParameter parameter = param_data->parameters[i];

    if (parameter.is_hidden) continue;
    PetscCall(PetscViewerASCIIPrintf(viewer, "-%s:\n", parameter.name));
    PetscCall(PetscViewerASCIIPushTab(viewer));  // Parameter i
    PetscCall(PetscViewerASCIIPrintf(viewer, "Description:  %s (%s)\n", parameter.description ? parameter.description : "(no description)",
                                     parameter.units ? parameter.units : "unitless"));
    PetscCall(PetscViewerASCIIPrintf(viewer, "Restrictions: %s\n", parameter.restrictions ? parameter.restrictions : "(none)"));
    PetscCall(PetscViewerASCIIPrintf(viewer, "Required:     %s", parameter.is_required ? "true\n" : "false"));
    PetscCall(PetscViewerASCIIUseTabs(viewer, PETSC_FALSE));
    if (!parameter.is_required) {
      switch (parameter.type) {
        case CEED_CONTEXT_FIELD_DOUBLE:
          PetscCall(PetscViewerASCIIPrintf(viewer, " (Default: %g)\n", parameter.default_value.s));
          break;
        // LCOV_EXCL_START
        case CEED_CONTEXT_FIELD_INT32:
          PetscCall(PetscViewerASCIIPrintf(viewer, " (Default: %" CeedInt_FMT ")\n", parameter.default_value.i));
          break;
        // LCOV_EXCL_STOP
        case CEED_CONTEXT_FIELD_BOOL:
          PetscCall(PetscViewerASCIIPrintf(viewer, " (Default: %s)\n", parameter.default_value.b == PETSC_TRUE ? "true" : "false"));
          break;
      }
    }
    PetscCall(PetscViewerASCIIUseTabs(viewer, PETSC_TRUE));
    if (context) {
      CeedContextFieldLabel field_label;

      RatelCallCeed(ratel, CeedQFunctionContextGetFieldLabel(context, parameter.name, &field_label));
      if (field_label) {
        CeedContextFieldType field_type;

        RatelCallCeed(ratel, CeedContextFieldLabelGetDescription(field_label, NULL, NULL, NULL, NULL, &field_type));
        switch (field_type) {
          case CEED_CONTEXT_FIELD_DOUBLE: {
            PetscSizeT        num_values;
            const CeedScalar *values;

            RatelCallCeed(ratel, CeedQFunctionContextGetDoubleRead(context, field_label, &num_values, &values));
            PetscCall(PetscViewerASCIIPrintf(viewer, "Value:        %g", values[0]));
            PetscCall(PetscViewerASCIIUseTabs(viewer, PETSC_FALSE));
            for (PetscSizeT j = 1; j < num_values; j++) PetscCall(PetscViewerASCIIPrintf(viewer, ", %g", values[j]));
            PetscCall(PetscViewerASCIIPrintf(viewer, "\n"));
            PetscCall(PetscViewerASCIIUseTabs(viewer, PETSC_TRUE));
            RatelCallCeed(ratel, CeedQFunctionContextRestoreDoubleRead(context, field_label, &values));
            break;
          }
          case CEED_CONTEXT_FIELD_INT32: {
            PetscSizeT     num_values;
            const CeedInt *values;

            RatelCallCeed(ratel, CeedQFunctionContextGetInt32Read(context, field_label, &num_values, &values));
            PetscCall(PetscViewerASCIIPrintf(viewer, "Value:        %d", values[0]));
            PetscCall(PetscViewerASCIIUseTabs(viewer, PETSC_FALSE));
            for (PetscSizeT j = 1; j < num_values; j++) PetscCall(PetscViewerASCIIPrintf(viewer, ", %d", values[j]));
            PetscCall(PetscViewerASCIIPrintf(viewer, "\n"));
            PetscCall(PetscViewerASCIIUseTabs(viewer, PETSC_TRUE));
            RatelCallCeed(ratel, CeedQFunctionContextRestoreInt32Read(context, field_label, &values));
            break;
          }
          case CEED_CONTEXT_FIELD_BOOL: {
            PetscSizeT  num_values;
            const bool *values;

            RatelCallCeed(ratel, CeedQFunctionContextGetBooleanRead(context, field_label, &num_values, &values));
            PetscCall(PetscViewerASCIIPrintf(viewer, "Value:        %d", values[0]));
            PetscCall(PetscViewerASCIIUseTabs(viewer, PETSC_FALSE));
            for (PetscSizeT j = 1; j < num_values; j++) PetscCall(PetscViewerASCIIPrintf(viewer, ", %d", values[j]));
            PetscCall(PetscViewerASCIIPrintf(viewer, "\n"));
            PetscCall(PetscViewerASCIIUseTabs(viewer, PETSC_TRUE));
            RatelCallCeed(ratel, CeedQFunctionContextRestoreBooleanRead(context, field_label, &values));
            break;
          }
        }
      }
    }
    PetscCall(PetscViewerASCIIPopTab(viewer));  // Parameter i
  }
  PetscCall(PetscViewerASCIIPopTab(viewer));  // Model Parameters
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Destroy a `RatelMaterial` object.

  Not collective across MPI processes.

  @param[in,out]  material  `RatelMaterial` to destroy

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialDestroy(RatelMaterial *material) {
  Ratel ratel = (*material)->ratel;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Destroy"));

  // PETSc data
  PetscCall(PetscFree((*material)->name));
  PetscCall(PetscFree((*material)->residual_indices));
  PetscCall(PetscFree((*material)->residual_ut_indices));
  PetscCall(PetscFree((*material)->jacobian_indices));

  // DM labels
  PetscCall(PetscFree((*material)->volume_label_values));
  PetscCall(PetscFree((*material)->volume_label_name));
  PetscCall(PetscFree((*material)->surface_grad_dm_face_ids));
  for (PetscInt i = 0; i < (*material)->num_surface_grad_dm_face_ids; i++) PetscCall(PetscFree((*material)->surface_grad_label_names[i]));
  PetscCall(PetscFree((*material)->surface_grad_label_names));
  PetscCall(PetscFree((*material)->surface_grad_diagnostic_dm_face_ids));
  for (PetscInt i = 0; i < (*material)->num_surface_grad_diagnostic_dm_face_ids; i++) {
    PetscCall(PetscFree((*material)->surface_grad_diagnostic_label_names[i]));
  }
  PetscCall(PetscFree((*material)->surface_grad_diagnostic_label_names));

  // Multigrid setup functions array
  PetscCall(PetscFree((*material)->RatelMaterialSetupMultigridLevels));

  // Smoother data
  for (PetscInt i = 0; i < (*material)->num_params_smoother_values; i++) PetscCall(PetscFree((*material)->ctx_params_label_names[i]));
  PetscCall(PetscFree((*material)->ctx_params_values));
  PetscCall(PetscFree((*material)->ctx_params_smoother_values));
  PetscCall(PetscFree((*material)->ctx_params_label_names));
  PetscCall(PetscFree((*material)->ctx_params_labels));

  // MPM context
  PetscCall(RatelMPMContextDestroy(&(*material)->mpm));

  // libCEED data
  RatelCallCeed(ratel, CeedQFunctionContextDestroy(&(*material)->ctx_params));
  RatelCallCeed(ratel, CeedQFunctionContextDestroy(&(*material)->ctx_mms_params));
  RatelCallCeed(ratel, CeedVectorDestroy(&(*material)->q_data_volume));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&(*material)->restriction_q_data_volume));
  for (PetscInt i = 0; i < (*material)->num_surface_grad_diagnostic_dm_face_ids; i++) {
    for (PetscInt j = 0; j < (*material)->num_face_orientations; j++) {
      RatelCallCeed(ratel, CeedVectorDestroy(&(*material)->q_data_surface_grad_diagnostic[i][j]));
      RatelCallCeed(ratel, CeedElemRestrictionDestroy(&(*material)->restrictions_q_data_surface_grad_diagnostic[i][j]));
    }
    PetscCall(PetscFree((*material)->q_data_surface_grad_diagnostic[i]));
    PetscCall(PetscFree((*material)->restrictions_q_data_surface_grad_diagnostic[i]));
  }
  PetscCall(PetscFree((*material)->q_data_surface_grad_diagnostic));
  PetscCall(PetscFree((*material)->restrictions_q_data_surface_grad_diagnostic));
  for (PetscInt i = 0; i < (*material)->num_surface_grad_dm_face_ids; i++) {
    for (PetscInt j = 0; j < (*material)->num_face_orientations; j++) {
      RatelCallCeed(ratel, CeedVectorDestroy(&(*material)->q_data_surface_grad[i][j]));
      RatelCallCeed(ratel, CeedElemRestrictionDestroy(&(*material)->restrictions_q_data_surface_grad[i][j]));
    }
    PetscCall(PetscFree((*material)->q_data_surface_grad[i]));
    PetscCall(PetscFree((*material)->restrictions_q_data_surface_grad[i]));
  }
  PetscCall(PetscFree((*material)->q_data_surface_grad));
  PetscCall(PetscFree((*material)->restrictions_q_data_surface_grad));

  // Struct itself
  PetscCall(PetscFree(*material));
  *material = NULL;

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Destroy Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get number of components for a `RatelMaterial` object.

  Not collective across MPI processes.

  @param[in]   material            `RatelMaterial` context
  @param[out]  num_active_fields   Number of components in `RatelMaterial`
  @param[out]  active_field_sizes  Number of components in `RatelMaterial`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetActiveFieldSizes(RatelMaterial material, CeedInt *num_active_fields, const CeedInt **active_field_sizes) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Get Active Field Sizes"));

  PetscCheck(material->model_data, material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Incorrect or incomplete model data setup for material %s",
             material->name);
  if (num_active_fields) *num_active_fields = material->model_data->num_active_fields;
  if (active_field_sizes) *active_field_sizes = material->model_data->active_field_sizes;

  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Get Active Field Sizes Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get point fields for a `RatelMaterial` object.

  Not collective across MPI processes.

  @param[in]   material           `RatelMaterial` context
  @param[out]  num_point_fields   Number of point fields in `RatelMaterial`, or `NULL`
  @param[in]   point_field_sizes  Sizes of each point field in `RatelMaterial`, or `NULL`
  @param[in]   point_field_names  Names of each point field in `RatelMaterial`, or `NULL`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetPointFields(RatelMaterial material, CeedInt *num_point_fields, const CeedInt **point_field_sizes,
                                           const char ***point_field_names) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Get Active Field Sizes"));

  PetscCheck(material->model_data, material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Incorrect or incomplete model data setup for material %s",
             material->name);
  if (num_point_fields) *num_point_fields = material->model_data->num_point_fields;
  if (point_field_sizes) *point_field_sizes = material->model_data->point_field_sizes;
  if (point_field_names) *point_field_names = material->model_data->point_field_names;

  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Get Active Field Sizes Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get number of diagnostic components for a `RatelMaterial` object.

  Not collective across MPI processes.

  @param[in]   material                  `RatelMaterial` context
  @param[out]  num_components_projected  Number of projected diagnostic components in `RatelMaterial`
  @param[out]  num_components_dual       Number of dual space diagnostic components in `RatelMaterial`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetNumDiagnosticComponents(RatelMaterial material, CeedInt *num_components_projected, CeedInt *num_components_dual) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Get Number of Diagnostic Components"));

  PetscCheck(material->model_data, material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Incorrect or incomplete model data setup for material %s",
             material->name);
  if (num_components_projected) *num_components_projected = material->model_data->num_comp_projected_diagnostic;
  if (num_components_dual) *num_components_dual = material->model_data->num_comp_dual_diagnostic;

  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Get Number of Diagnostic Components Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get number of state components for a `RatelMaterial` object.

  Not collective across MPI processes.

  @param[in]   material        `RatelMaterial` context
  @param[out]  num_comp_state  Number of state fields in `RatelMaterial`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetNumStateComponents(RatelMaterial material, CeedInt *num_comp_state) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Get Number of State Fields"));

  PetscCheck(material->model_data, material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Incorrect or incomplete model data setup for material %s",
             material->name);
  *num_comp_state = material->model_data->num_comp_state;

  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Get Number of State Fields Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get active field names for a `RatelMaterial` object.

  Not collective across MPI processes.

  @param[in]   material                `RatelMaterial` context
  @param[out]  active_field_names      Names of fields in `RatelMaterial`
  @param[out]  active_component_names  Names of components in `RatelMaterial`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetActiveFieldNames(RatelMaterial material, const char ***active_field_names, const char ***active_component_names) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Get Field Names"));

  PetscCheck(material->model_data, material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Incorrect or incomplete model data setup for material %s",
             material->name);
  if (active_field_names) *active_field_names = (const char **)material->model_data->active_field_names;
  if (active_component_names) *active_component_names = (const char **)material->model_data->active_component_names;

  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Get Field Names Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get diagnostic component names for a `RatelMaterial` object.

  Not collective across MPI processes.

  @param[in]   material                   `RatelMaterial` context
  @param[out]  projected_component_names  Names of projected diagnostic component in `RatelMaterial`
  @param[out]  dual_component_names       Names of dual space diagnostic component in `RatelMaterial`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetDiagnosticComponentNames(RatelMaterial material, const char ***projected_component_names,
                                                        const char ***dual_component_names) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Get Diagnostic Component Names"));

  PetscCheck(material->model_data, material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Incorrect or incomplete model data setup for material %s",
             material->name);
  *projected_component_names = (const char **)material->model_data->projected_diagnostic_component_names;
  *dual_component_names      = (const char **)material->model_data->dual_diagnostic_component_names;

  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Get Diagnostic Component Names Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get volume label name for a `RatelMaterial` object.

  Not collective across MPI processes.

  @param[in]   material    `RatelMaterial` context
  @param[out]  label_name  Volume label name for `RatelMaterial`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetVolumeLabelName(RatelMaterial material, const char **label_name) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Get Volume Label Name"));

  PetscCheck(material->model_data, material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Incorrect or incomplete model data setup for material %s",
             material->name);
  *label_name = material->volume_label_name;

  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Get Volume Label Name Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get volume label values for a `RatelMaterial` object.

  Not collective across MPI processes.

  @param[in]   material          `RatelMaterial` context
  @param[out]  num_label_values  Number of label values for `RatelMaterial`
  @param[out]  label_values      Volume label values for `RatelMaterial`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetVolumeLabelValues(RatelMaterial material, PetscInt *num_label_values, PetscInt **label_values) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Get Volume Label Values"));

  PetscCheck(material->model_data, material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Incorrect or incomplete model data setup for material %s",
             material->name);
  if (num_label_values) *num_label_values = material->num_volume_label_values;
  *label_values = material->volume_label_values;

  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Get Volume Label Values Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Set volume label values for a `RatelMaterial` object.

  Not collective across MPI processes.

  @param[in,out]  material     `RatelMaterial` context
  @param[in]      label_value  Volume label value to set for `RatelMaterial`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetVolumeLabelValue(RatelMaterial material, PetscInt label_value) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Set Volume Label Value"));

  PetscCheck(material->num_volume_label_values == 1 && material->volume_label_values[0], material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE,
             "Volume label values already set for material %s", material->name);
  material->volume_label_values[0] = label_value;

  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Set Volume Label Value Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get `RatelForcingType` for a `RatelMaterial` object.

  Not collective across MPI processes.

  @param[in]   material      `RatelMaterial` context
  @param[out]  forcing_type  `RatelMethodType` for `RatelMaterial`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetForcingType(RatelMaterial material, RatelForcingType *forcing_type) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Get Model Type"));

  *forcing_type = material->forcing_type;

  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Get Model Type Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get material name for a `RatelMaterial`, or `NULL` if none set.

  Not collective across MPI processes.

  @param[in]   material       `RatelMaterial` context
  @param[out]  material_name  `char` array holding model name

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetMaterialName(RatelMaterial material, const char **material_name) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Get Material Name"));

  *material_name = material->name;

  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Get Material Name Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get model name for a `RatelMaterial`.

  Not collective across MPI processes.

  @param[in]   material    `RatelMaterial` context
  @param[out]  model_name  `char` array holding model name

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetModelName(RatelMaterial material, const char **model_name) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Get Model Name"));

  PetscCheck(material->model_data, material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Incorrect or incomplete model data setup for material %s",
             material->name);
  *model_name = material->model_data->name;

  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Get Model Name Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get label name for solution `DM` for a `RatelMaterial` by face number.

  Not collective across MPI processes.

  @param[in]   material    `RatelMaterial` context
  @param[in]   dm_face     `DMPlex` face number
  @param[out]  label_name  `char` array holding label name

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetSurfaceGradientLabelName(RatelMaterial material, PetscInt dm_face, const char **label_name) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Get Surface Gradient Label Name"));

  PetscCheck(material->num_surface_grad_dm_face_ids != -1, material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE,
             "DMPlex face labels not set up for material %s", material->name);

  // Get face index
  PetscInt i = -1;
  for (PetscInt j = 0; j < material->num_surface_grad_dm_face_ids && i == -1; j++) {
    if (material->surface_grad_dm_face_ids[j] == dm_face) i = j;
  }
  PetscCheck(i != -1, material->ratel->comm, PETSC_ERR_SUP, "DMPlex face %" PetscInt_FMT " not found in boundary face list for material %s", dm_face,
             material->name);
  *label_name = material->surface_grad_label_names[i];

  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Get Surface Gradient Label Name Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get label name for diagnostic `DM` for a `RatelMaterial` by face number.

  Not collective across MPI processes.

  @param[in]   material    `RatelMaterial` context
  @param[in]   dm_face     `DMPlex` face number
  @param[out]  label_name  `char` array holding label name

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetSurfaceGradientDiagnosticLabelName(RatelMaterial material, PetscInt dm_face, const char **label_name) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Get Surface Gradient Diagnostic Label Name"));

  // Check for labels
  PetscCheck(material->num_surface_grad_dm_face_ids != -1, material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE,
             "DMPlex face labels not set up for material %s", material->name);

  // Get face index
  PetscInt i = -1;
  for (PetscInt j = 0; j < material->num_surface_grad_diagnostic_dm_face_ids && i == -1; j++) {
    if (material->surface_grad_diagnostic_dm_face_ids[j] == dm_face) i = j;
  }
  PetscCheck(i != -1, material->ratel->comm, PETSC_ERR_SUP, "DMPlex face %" PetscInt_FMT " not found in boundary face list for material %s", dm_face,
             material->name);
  *label_name = material->surface_grad_diagnostic_label_names[i];

  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Get Surface Gradient Diagnostic Label Name Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get domain label and domain value for solution `DM` for a `RatelMaterial` from `CeedOperator` context.

  Not collective across MPI processes.

  @param[in]   material           `RatelMaterial` context
  @param[in]   op                 `CeedOperator` with `"face id"` and `"face domain value"` fields
  @param[out]  face_domain_label  `DMPlex` label corresponding to face domain
  @param[out]  face_domain_value  Stratum value

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetSurfaceGradientOperatorFaceLabelAndValue(RatelMaterial material, CeedOperator op, DMLabel *face_domain_label,
                                                                        PetscInt *face_domain_value) {
  Ratel    ratel = material->ratel;
  PetscInt face_id;

  PetscFunctionBeginUser;
  {
    const CeedInt        *id;
    PetscSizeT            count;
    CeedContextFieldLabel label;

    RatelCallCeed(ratel, CeedOperatorGetContextFieldLabel(op, "face id", &label));
    PetscCheck(label, ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Face context not initialized for platen Jacobian operator");
    RatelCallCeed(ratel, CeedOperatorGetContextInt32Read(op, label, &count, &id));
    face_id = (PetscInt)id[0];
    RatelCallCeed(ratel, CeedOperatorRestoreContextInt32Read(op, label, &id));
  }

  {
    DM          dm_solution;
    const char *face_domain_label_name;

    PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));
    PetscCall(RatelMaterialGetSurfaceGradientLabelName(material, face_id, &face_domain_label_name));
    PetscCall(DMGetLabel(dm_solution, face_domain_label_name, face_domain_label));
    PetscCheck((*face_domain_label), ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "No DM label corresponding to name %s found", face_domain_label_name);
    PetscCall(DMDestroy(&dm_solution));
  }

  {
    const CeedInt        *domain_value;
    PetscSizeT            count;
    CeedContextFieldLabel domain_value_label;

    RatelCallCeed(ratel, CeedOperatorGetContextFieldLabel(op, "face domain value", &domain_value_label));
    PetscCheck(domain_value_label, ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Face context not initialized for platen Jacobian operator");
    RatelCallCeed(ratel, CeedOperatorGetContextInt32Read(op, domain_value_label, &count, &domain_value));
    (*face_domain_value) = (PetscInt)domain_value[0];
    RatelCallCeed(ratel, CeedOperatorRestoreContextInt32Read(op, domain_value_label, &domain_value));
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get scaling factor for initial random noise to add to zero initial condition for material model.
         0.0 indicates that the material model does not require initial random noise for inital condition.

  Not collective across MPI processes.

  @param[in]   material                `RatelMaterial` context
  @param[out]  initial_random_scaling  Scaling factor for initial random noise to add to zero initial condition for material model

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetInitialRandomScaling(RatelMaterial material, PetscScalar *initial_random_scaling) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Get Initial Random Scaling"));

  PetscCheck(material->model_data, material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Incorrect or incomplete model data setup for material %s",
             material->name);
  *initial_random_scaling = material->model_data->initial_random_scaling;

  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Get Initial Random Scaling Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Determine if `RatelMaterial` has MMS solution.

  Not collective across MPI processes.

  @param[in]   material  `RatelMaterial` context
  @param[out]  has_mms   Boolean flag indicating if `RatelMaterial` has MMS solution

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialHasMMS(RatelMaterial material, PetscBool *has_mms) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Has MMS"));

  PetscCheck(material->model_data, material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Incorrect or incomplete model data setup for material %s",
             material->name);
  *has_mms = !!material->model_data->mms_error;

  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Has MMS Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get command line option prefix for a `RatelMaterial` object.

  Not collective across MPI processes.

  @param[in]   material   `RatelMaterial` context
  @param[out]  cl_prefix  Command line option prefix for `RatelMaterial`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetCLPrefix(RatelMaterial material, char **cl_prefix) {
  PetscSizeT name_len, prefix_len;
  PetscBool  has_name = PETSC_TRUE;

  PetscFunctionBeginUser;
  PetscCall(PetscStrlen(material->name, &name_len));
  has_name   = name_len > 0;
  prefix_len = (has_name ? name_len + 1 : 0) + 1;
  PetscCall(PetscCalloc1(prefix_len, cl_prefix));
  PetscCall(PetscSNPrintf(*cl_prefix, prefix_len, "%s%s", has_name ? material->name : "", has_name ? "_" : ""));
  PetscCall(RatelDebug(material->ratel, "---- RatelMaterial options prefix: %s", *cl_prefix));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get command line option message for a `RatelMaterial` object.

  Not collective across MPI processes.

  @param[in]   material    `RatelMaterial` context
  @param[out]  cl_message  Command line option message for `RatelMaterial`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetCLMessage(RatelMaterial material, char **cl_message) {
  PetscSizeT  name_len, message_start_len, message_len;
  PetscBool   has_name      = PETSC_TRUE;
  const char *message_start = "Material model options";

  PetscFunctionBeginUser;
  PetscCall(PetscStrlen(material->name, &name_len));
  has_name = name_len > 0;
  PetscCall(PetscStrlen(message_start, &message_start_len));
  message_len = message_start_len + (has_name ? name_len + 5 : 0) + 1;
  PetscCall(PetscCalloc1(message_len, cl_message));
  PetscCall(PetscSNPrintf(*cl_message, message_len, "%s%s%s", message_start, has_name ? " for " : "", has_name ? material->name : ""));
  PetscCall(RatelDebug(material->ratel, "---- Material options message: %s", *cl_message));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Build CeedOperator name for `RatelMaterial`.
           Note: Caller is responsible for freeing the allocated name string `PetscFree()`.

  Not collective across MPI processes.

  @param[in]   material       `RatelMaterial` context
  @param[in]   base_name      String holding base name for operator
  @param[out]  operator_name  String holding operator name for `RatelMaterial`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialCreateOperatorName(RatelMaterial material, const char *base_name, char **operator_name) {
  PetscSizeT name_len, base_name_len, operator_name_len;
  PetscBool  has_name = PETSC_TRUE;

  PetscFunctionBeginUser;
  PetscCall(PetscStrlen(material->name, &name_len));
  has_name = name_len > 0;
  PetscCall(PetscStrlen(base_name, &base_name_len));
  operator_name_len = base_name_len + (has_name ? 5 : 0) + name_len + 1;
  PetscCall(PetscCalloc1(operator_name_len, operator_name));
  PetscCall(PetscSNPrintf(*operator_name, operator_name_len, "%s%s%s", base_name, has_name ? " for " : "", has_name ? material->name : ""));
  PetscCall(RatelDebug(material->ratel, "---- Material operator name: %s", *operator_name));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get `CeedElemRestriction` and `CeedBasis` objects corresponding to residual evaluation.

  Not collective across MPI processes.

  @param[in]   material           `RatelMaterial` context
  @param[in]   op_residual_u      Composite residual u term `CeedOperator`
  @param[out]  num_active_fields  Number of active fields in residual evaluation
  @param[out]  restrictions       `CeedElemRestriction` objects corresponding to residual evaluation
  @param[out]  bases              `CeedBasis` objects corresponding to residual evaluation

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetSolutionData(RatelMaterial material, CeedOperator op_residual_u, CeedInt *num_active_fields,
                                            CeedElemRestriction **restrictions, CeedBasis **bases) {
  Ratel              ratel               = material->ratel;
  CeedInt            input_fields_offset = 1 + (material->model_data->num_comp_state > 0);
  CeedOperator      *sub_ops;
  CeedOperatorField *input_fields;

  PetscFunctionBeginUser;
  // Check if Residual setup
  PetscCheck(material->num_residual_indices != 0, ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Residual operator not set up for material %s",
             material->name);

  // Allocate arrays
  *num_active_fields = material->model_data->num_active_fields;
  PetscCall(PetscCalloc1(*num_active_fields, restrictions));
  PetscCall(PetscCalloc1(*num_active_fields, bases));

  // Retrieve objects and copy reference
  RatelCallCeed(ratel, CeedCompositeOperatorGetSubList(op_residual_u, &sub_ops));
  RatelCallCeed(ratel, CeedOperatorGetFields(sub_ops[material->residual_indices[0]], NULL, &input_fields, NULL, NULL));
  for (PetscInt i = 0; i < *num_active_fields; i++) {
    CeedElemRestriction restriction;
    CeedBasis           basis;

    RatelCallCeed(ratel, CeedOperatorFieldGetElemRestriction(input_fields[i + input_fields_offset], &restriction));
    RatelCallCeed(ratel, CeedElemRestrictionReferenceCopy(restriction, &(*restrictions)[i]));
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction));
    RatelCallCeed(ratel, CeedOperatorFieldGetBasis(input_fields[i + input_fields_offset], &basis));
    RatelCallCeed(ratel, CeedBasisReferenceCopy(basis, &(*bases)[i]));
    RatelCallCeed(ratel, CeedBasisDestroy(&basis));

    input_fields_offset += material->model_data->active_field_num_eval_modes[i] - 1;
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get `CeedElemRestriction` and `CeedVector` objects corresponding to stored data from residual_u evaluation.

  Not collective across MPI processes.

  @param[in]   material       `RatelMaterial` context
  @param[in]   op_residual_u  Composite residual u term `CeedOperator`
  @param[out]  restriction    `CeedElemRestriction` object corresponding to stored data from residual_u evaluation
  @param[out]  values         `CeedVector` object corresponding to stored data from residual evaluation

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetStoredDataU(RatelMaterial material, CeedOperator op_residual_u, CeedElemRestriction *restriction, CeedVector *values) {
  Ratel ratel = material->ratel;

  PetscFunctionBeginUser;
  // Check if Residual setup
  PetscCheck(material->num_residual_indices != 0, ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Residual operator not set up for material %s",
             material->name);

  // Retrieve objects and copy reference
  *restriction = NULL;
  *values      = NULL;
  if (material->model_data->num_comp_stored_u > 0) {
    const char         *name = "stored values u";
    CeedVector          stored_values;
    CeedElemRestriction stored_restriction;
    CeedOperator       *sub_ops;
    CeedOperatorField   stored_field;

    RatelCallCeed(ratel, CeedCompositeOperatorGetSubList(op_residual_u, &sub_ops));
    RatelCallCeed(ratel, CeedOperatorGetFieldByName(sub_ops[material->residual_indices[0]], name, &stored_field));
    RatelCallCeed(ratel, CeedOperatorFieldGetElemRestriction(stored_field, &stored_restriction));
    RatelCallCeed(ratel, CeedElemRestrictionReferenceCopy(stored_restriction, restriction));
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&stored_restriction));
    RatelCallCeed(ratel, CeedOperatorFieldGetVector(stored_field, &stored_values));
    RatelCallCeed(ratel, CeedVectorReferenceCopy(stored_values, values));
    RatelCallCeed(ratel, CeedVectorDestroy(&stored_values));
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get `CeedElemRestriction` and `CeedVector` objects corresponding to stored data from residual_ut evaluation.

  Not collective across MPI processes.

  @param[in]   material        `RatelMaterial` context
  @param[in]   op_residual_ut  Composite residual ut term `CeedOperator`
  @param[out]  restriction     `CeedElemRestriction` object corresponding to stored data from residual_ut evaluation
  @param[out]  values          `CeedVector` object corresponding to stored data from residual_ut evaluation

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetStoredDataUt(RatelMaterial material, CeedOperator op_residual_ut, CeedElemRestriction *restriction,
                                            CeedVector *values) {
  Ratel ratel = material->ratel;

  PetscFunctionBeginUser;
  // Check if Residual_ut setup
  PetscCheck(material->num_residual_ut_indices != 0, ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Residual ut operator not set up for material %s",
             material->name);

  // Retrieve objects and copy reference
  *restriction = NULL;
  *values      = NULL;
  if (material->model_data->num_comp_stored_ut > 0) {
    const char         *name = "stored values ut";
    CeedVector          stored_values;
    CeedElemRestriction stored_restriction;
    CeedOperator       *sub_ops;
    CeedOperatorField   stored_field;

    RatelCallCeed(ratel, CeedCompositeOperatorGetSubList(op_residual_ut, &sub_ops));
    RatelCallCeed(ratel, CeedOperatorGetFieldByName(sub_ops[material->residual_ut_indices[0]], name, &stored_field));
    RatelCallCeed(ratel, CeedOperatorFieldGetElemRestriction(stored_field, &stored_restriction));
    RatelCallCeed(ratel, CeedElemRestrictionReferenceCopy(stored_restriction, restriction));
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&stored_restriction));
    RatelCallCeed(ratel, CeedOperatorFieldGetVector(stored_field, &stored_values));
    RatelCallCeed(ratel, CeedVectorReferenceCopy(stored_values, values));
    RatelCallCeed(ratel, CeedVectorDestroy(&stored_values));
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get `CeedElemRestriction` and `CeedVector` objects corresponding to state data prior to residual evaluation.

  Not collective across MPI processes.

  @param[in]   material       `RatelMaterial` context
  @param[in]   op_residual_u  Composite residual u term `CeedOperator`
  @param[out]  restriction    `CeedElemRestriction` object corresponding to state data from residual evaluation
  @param[out]  values         `CeedVector` object corresponding to state data from residual evaluation

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetInitialStateData(RatelMaterial material, CeedOperator op_residual_u, CeedElemRestriction *restriction,
                                                CeedVector *values) {
  Ratel ratel = material->ratel;

  PetscFunctionBeginUser;
  // Check if Residual setup
  PetscCheck(material->num_residual_indices != 0, ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Residual operator not set up for material %s",
             material->name);

  // Retrieve objects and copy reference
  *restriction = NULL;
  *values      = NULL;
  if (material->model_data->num_comp_state > 0) {
    const char         *name = "model state";
    CeedVector          state_values;
    CeedElemRestriction state_restriction;
    CeedOperator       *sub_ops;
    CeedOperatorField   state_field;

    RatelCallCeed(ratel, CeedCompositeOperatorGetSubList(op_residual_u, &sub_ops));
    RatelCallCeed(ratel, CeedOperatorGetFieldByName(sub_ops[material->residual_indices[0]], name, &state_field));
    RatelCallCeed(ratel, CeedOperatorFieldGetElemRestriction(state_field, &state_restriction));
    RatelCallCeed(ratel, CeedElemRestrictionReferenceCopy(state_restriction, restriction));
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&state_restriction));
    RatelCallCeed(ratel, CeedOperatorFieldGetVector(state_field, &state_values));
    RatelCallCeed(ratel, CeedVectorReferenceCopy(state_values, values));
    RatelCallCeed(ratel, CeedVectorDestroy(&state_values));
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get `CeedElemRestriction` and `CeedVector` objects corresponding to state data from residual evaluation.

  Not collective across MPI processes.

  @param[in]   material       `RatelMaterial` context
  @param[in]   op_residual_u  Composite residual u term `CeedOperator`
  @param[out]  restriction    `CeedElemRestriction` object corresponding to state data from residual evaluation
  @param[out]  values         `CeedVector` object corresponding to state data from residual evaluation

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetStateData(RatelMaterial material, CeedOperator op_residual_u, CeedElemRestriction *restriction, CeedVector *values) {
  Ratel ratel = material->ratel;

  PetscFunctionBeginUser;
  // Check if Residual setup
  PetscCheck(material->num_residual_indices != 0, ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Residual operator not set up for material %s",
             material->name);

  // Retrieve objects and copy reference
  *restriction = NULL;
  *values      = NULL;
  if (material->model_data->num_comp_state > 0) {
    const char         *name = "current model state";
    CeedVector          state_values;
    CeedElemRestriction state_restriction;
    CeedOperator       *sub_ops;
    CeedOperatorField   state_field;

    RatelCallCeed(ratel, CeedCompositeOperatorGetSubList(op_residual_u, &sub_ops));
    RatelCallCeed(ratel, CeedOperatorGetFieldByName(sub_ops[material->residual_indices[0]], name, &state_field));
    RatelCallCeed(ratel, CeedOperatorFieldGetElemRestriction(state_field, &state_restriction));
    RatelCallCeed(ratel, CeedElemRestrictionReferenceCopy(state_restriction, restriction));
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&state_restriction));
    RatelCallCeed(ratel, CeedOperatorFieldGetVector(state_field, &state_values));
    RatelCallCeed(ratel, CeedVectorReferenceCopy(state_values, values));
    RatelCallCeed(ratel, CeedVectorDestroy(&state_values));
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get `CeedElemRestriction` and `CeedVector` objects corresponding to point data from residual evaluation.

  Not collective across MPI processes.

  @param[in]   material          `RatelMaterial` context
  @param[in]   op_residual_u     Composite residual u term `CeedOperator`
  @param[out]  num_point_fields  Number of point fields defined for material
  @param[out]  restrictions      `CeedElemRestriction` objects for point fields from residual operator
  @param[out]  values            `CeedVector` objects for point fields from residual operator

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetPointData(RatelMaterial material, CeedOperator op_residual_u, CeedInt *num_point_fields,
                                         CeedElemRestriction **restrictions, CeedVector **values) {
  Ratel              ratel               = material->ratel;
  CeedInt            input_fields_offset = 1 + (material->model_data->num_comp_state > 0);
  CeedOperator      *sub_ops;
  CeedOperatorField *input_fields;

  PetscFunctionBeginUser;
  // Check if Residual setup
  PetscCheck(material->num_residual_indices != 0, ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Residual operator not set up for material %s",
             material->name);

  // Compute offset
  for (PetscInt i = 0; i < material->model_data->num_active_fields; i++) {
    input_fields_offset += material->model_data->active_field_num_eval_modes[i];
  }

  // Allocate arrays
  *num_point_fields = material->model_data->num_point_fields;
  PetscCall(PetscCalloc1(*num_point_fields, restrictions));
  PetscCall(PetscCalloc1(*num_point_fields, values));

  // Retrieve objects and copy reference
  RatelCallCeed(ratel, CeedCompositeOperatorGetSubList(op_residual_u, &sub_ops));
  RatelCallCeed(ratel, CeedOperatorGetFields(sub_ops[material->residual_indices[0]], NULL, &input_fields, NULL, NULL));
  for (PetscInt i = 0; i < *num_point_fields; i++) {
    CeedElemRestriction restriction = NULL;
    CeedVector          value       = NULL;

    RatelCallCeed(ratel, CeedOperatorFieldGetElemRestriction(input_fields[i + input_fields_offset], &restriction));
    RatelCallCeed(ratel, CeedElemRestrictionReferenceCopy(restriction, &(*restrictions)[i]));
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction));
    RatelCallCeed(ratel, CeedOperatorFieldGetVector(input_fields[i + input_fields_offset], &value));
    RatelCallCeed(ratel, CeedVectorReferenceCopy(value, &(*values)[i]));
    RatelCallCeed(ratel, CeedVectorDestroy(&value));
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Build `CeedOperator` name for `RatelMaterial`.

  Not collective across MPI processes.

  @param[in]      material   `RatelMaterial` context
  @param[in]      base_name  String holding base name for operator
  @param[in,out]  op         `CeedOperator` to set the name for

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetOperatorName(RatelMaterial material, const char *base_name, CeedOperator op) {
  char *operator_name;

  PetscFunctionBeginUser;
  PetscCall(RatelMaterialCreateOperatorName(material, base_name, &operator_name));
  RatelCallCeed(material->ratel, CeedOperatorSetName(op, operator_name));
  PetscCall(PetscFree(operator_name));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get `CeedOperator` volumetric QData for `RatelMaterial`. Uses the material `SetupVolumeQData` function to set up the QData.

  Collective across MPI processes.

  @param[in]   material     `RatelMaterial` context
  @param[out]  restriction  `CeedElemRestriction` for QData
  @param[out]  q_data       `CeedVector` holding QData

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetVolumeQData(RatelMaterial material, CeedElemRestriction *restriction, CeedVector *q_data) {
  Ratel ratel = material->ratel;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Get Volumetric QData"));

  if (ratel->method_type == RATEL_METHOD_MPM) {
    // Get points q data
    RatelMPMContext mpm;

    PetscCall(RatelMaterialGetMPMContext(material, &mpm));
    if (!mpm->q_data_points) {
      PetscCall((*material->SetupVolumeQData)(material, material->volume_label_name, material->volume_label_values[0],
                                              &mpm->restriction_q_data_points, &mpm->q_data_points));
    }
    RatelCallCeed(material->ratel, CeedElemRestrictionReferenceCopy(mpm->restriction_q_data_points, restriction));
    RatelCallCeed(material->ratel, CeedVectorReferenceCopy(mpm->q_data_points, q_data));
  } else {
    // Get mesh q data
    if (!material->q_data_volume) {
      PetscCall((*material->SetupVolumeQData)(material, material->volume_label_name, material->volume_label_values[0],
                                              &material->restriction_q_data_volume, &material->q_data_volume));
    }
    RatelCallCeed(material->ratel, CeedElemRestrictionReferenceCopy(material->restriction_q_data_volume, restriction));
    RatelCallCeed(material->ratel, CeedVectorReferenceCopy(material->q_data_volume, q_data));
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Get Volumetric QData Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get `CeedOperator` volumetric QData for `RatelMaterial`. Uses `RatelMaterialGetVolumeQData_FEM` to set up the QData.

  Collective across MPI processes.

  @param[in]   material     `RatelMaterial` context
  @param[out]  restriction  `CeedElemRestriction` for QData
  @param[out]  q_data       `CeedVector` holding QData

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetMeshVolumeQData(RatelMaterial material, CeedElemRestriction *restriction, CeedVector *q_data) {
  Ratel ratel = material->ratel;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Get Mesh Volumetric QData"));

  // QData
  if (!material->q_data_volume) {
    PetscCall(RatelMaterialSetupVolumeQData_FEM(material, material->volume_label_name, material->volume_label_values[0],
                                                &material->restriction_q_data_volume, &material->q_data_volume));
  }
  RatelCallCeed(material->ratel, CeedElemRestrictionReferenceCopy(material->restriction_q_data_volume, restriction));
  RatelCallCeed(material->ratel, CeedVectorReferenceCopy(material->q_data_volume, q_data));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Get Mesh Volumetric QData Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get `CeedOperator` surface gradient QData for `RatelMaterial`.

  Collective across MPI processes.

  @param[in]   material     `RatelMaterial` context
  @param[in]   dm_face      `DMPlex` face number
  @param[in]   orientation  Face orientation number, giving `DMLabel` value
  @param[out]  restriction  `CeedElemRestriction` for QData
  @param[out]  q_data       `CeedVector` holding QData

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetSurfaceGradientQData(RatelMaterial material, PetscInt dm_face, PetscInt orientation, CeedElemRestriction *restriction,
                                                    CeedVector *q_data) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Get Surface Gradient QData"));

  // Check for labels
  PetscCheck(material->num_surface_grad_dm_face_ids != -1, material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE,
             "DMPlex face labels not set up for material %s", material->name);

  // Check for implementation
  PetscCheck(material->SetupSurfaceGradientQData, material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE,
             "Surface QData not supported for material model %s", material->model_data->name);

  // Get face index
  PetscInt i = -1;
  for (PetscInt j = 0; j < material->num_surface_grad_dm_face_ids && i == -1; j++) {
    if (material->surface_grad_dm_face_ids[j] == dm_face) i = j;
  }
  PetscCheck(i != -1, material->ratel->comm, PETSC_ERR_SUP, "DMPlex face %" PetscInt_FMT " not found in boundary face list for material %s", dm_face,
             material->name);

  // Get face QData
  if (material->surface_grad_label_names[i]) {
    if (!material->q_data_surface_grad[i][orientation]) {
      DM dm_solution;

      PetscCall(RatelGetSolutionMeshDM(material->ratel, &dm_solution));
      PetscCall((*material->SetupSurfaceGradientQData)(material, dm_solution, material->surface_grad_label_names[i], orientation,
                                                       &material->restrictions_q_data_surface_grad[i][orientation],
                                                       &material->q_data_surface_grad[i][orientation]));
      PetscCall(DMDestroy(&dm_solution));
    }
    RatelCallCeed(material->ratel, CeedElemRestrictionReferenceCopy(material->restrictions_q_data_surface_grad[i][orientation], restriction));
    RatelCallCeed(material->ratel, CeedVectorReferenceCopy(material->q_data_surface_grad[i][orientation], q_data));
  } else {
    // LCOV_EXCL_START
    *restriction = NULL;
    *q_data      = NULL;
    PetscCall(RatelDebug(material->ratel, "Warn: Label not found for index %" PetscInt_FMT " and face %" PetscInt_FMT, i, dm_face));
    // LCOV_EXCL_STOP
  }

  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Get Surface Gradient QData Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get `CeedOperator` diagnostic surface gradient QData for `RatelMaterial`.

  Collective across MPI processes.

  @param[in]   material     `RatelMaterial` context
  @param[in]   dm           `DMPlex` to use for diagnostic QData
  @param[in]   dm_face      `DMPlex` face number
  @param[in]   orientation  Face orientation number, giving `DMLabel` value
  @param[out]  restriction  `CeedElemRestriction` for QData
  @param[out]  q_data       `CeedVector` holding QData

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialGetSurfaceGradientDiagnosticQData(RatelMaterial material, DM dm, PetscInt dm_face, PetscInt orientation,
                                                              CeedElemRestriction *restriction, CeedVector *q_data) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Get Diagnostic Surface Gradient QData"));

  // Check for labels
  PetscCheck(material->num_surface_grad_dm_face_ids != -1, material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE,
             "DMPlex face labels not set up for material %s", material->name);

  // Check for implementation
  PetscCheck(material->SetupSurfaceGradientQData, material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE,
             "Surface QData not supported for material model %s", material->model_data->name);

  // Get face index
  PetscInt i = -1;
  for (PetscInt j = 0; j < material->num_surface_grad_diagnostic_dm_face_ids && i == -1; j++) {
    if (material->surface_grad_diagnostic_dm_face_ids[j] == dm_face) i = j;
  }
  PetscCheck(i != -1, material->ratel->comm, PETSC_ERR_SUP, "DMPlex face %" PetscInt_FMT " not found in boundary face list for material %s", dm_face,
             material->name);

  // Get face QData
  if (material->surface_grad_diagnostic_label_names[i]) {
    if (!material->q_data_surface_grad_diagnostic[i][orientation]) {
      PetscCall((*material->SetupSurfaceGradientQData)(material, dm, material->surface_grad_diagnostic_label_names[i], orientation,
                                                       &material->restrictions_q_data_surface_grad_diagnostic[i][orientation],
                                                       &material->q_data_surface_grad_diagnostic[i][orientation]));
    }
    RatelCallCeed(material->ratel,
                  CeedElemRestrictionReferenceCopy(material->restrictions_q_data_surface_grad_diagnostic[i][orientation], restriction));
    RatelCallCeed(material->ratel, CeedVectorReferenceCopy(material->q_data_surface_grad_diagnostic[i][orientation], q_data));
  } else {
    // LCOV_EXCL_START
    *restriction = NULL;
    *q_data      = NULL;
    // LCOV_EXCL_STOP
  }

  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Get Diagnostic Surface Gradient QData Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup residual `CeedOperator` for `RatelMaterial`.

  Collective across MPI processes.

  @param[in]   material         `RatelMaterial` context
  @param[out]  op_residual_u    Composite residual u term `CeedOperator` to add `RatelMaterial` suboperator
  @param[out]  op_residual_ut   Composite residual u_t term `CeedOperator` to add `RatelMaterial` suboperator
  @param[out]  op_residual_utt  Composite residual u_tt term `CeedOperator` to add `RatelMaterial` suboperator

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupResidualSuboperators(RatelMaterial material, CeedOperator op_residual_u, CeedOperator op_residual_ut,
                                                      CeedOperator op_residual_utt) {
  CeedInt residual_index, residual_ut_index;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Residual Suboperators"));

  PetscCheck(material->num_residual_indices == 0, material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Residual operator already set up for material %s",
             material->name);

  // Set residual_u operator indices
  material->num_residual_indices = 1;
  PetscCall(PetscCalloc1(material->num_residual_indices, &material->residual_indices));
  RatelCallCeed(material->ratel, CeedCompositeOperatorGetNumSub(op_residual_u, &residual_index));
  material->residual_indices[0] = residual_index;

  // Set residual_u operator indices
  if (material->model_data->has_ut_term) {
    material->num_residual_ut_indices = 1;
    PetscCall(PetscCalloc1(material->num_residual_ut_indices, &material->residual_ut_indices));
    RatelCallCeed(material->ratel, CeedCompositeOperatorGetNumSub(op_residual_ut, &residual_ut_index));
    material->residual_ut_indices[0] = residual_ut_index;
  }

  // Setup suboperators
  PetscCall((*material->SetupResidualSuboperators)(material, op_residual_u, op_residual_ut, op_residual_utt));

  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Residual Suboperators Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup Jacobian `CeedOperator` for `RatelMaterial`.

  Collective across MPI processes.

  @param[in]   material        `RatelMaterial` context
  @param[in]   op_residual_u   Composite residual u term `CeedOperator`
  @param[in]   op_residual_ut  Composite residual ut term `CeedOperator`
  @param[out]  op_jacobian     Composite Jacobian `CeedOperator` to add `RatelMaterial` suboperator

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupJacobianSuboperator(RatelMaterial material, CeedOperator op_residual_u, CeedOperator op_residual_ut,
                                                     CeedOperator op_jacobian) {
  CeedInt jacobian_index;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Jacobian Suboperator"));

  PetscCheck(material->num_residual_indices != 0, material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Residual operator not set up for material %s",
             material->name);

  // Set operator indices
  material->num_jacobian_indices = 1;
  PetscCall(PetscCalloc1(material->num_jacobian_indices, &material->jacobian_indices));
  RatelCallCeed(material->ratel, CeedCompositeOperatorGetNumSub(op_jacobian, &jacobian_index));
  material->jacobian_indices[0] = jacobian_index;
  PetscCall(PetscCalloc1(material->num_jacobian_indices, &material->RatelMaterialSetupMultigridLevels));
  material->RatelMaterialSetupMultigridLevels[0] = RatelMaterialSetupJacobianMultigridLevel_FEM;

  // Setup suboperators
  PetscCall((*material->SetupJacobianSuboperator)(material, op_residual_u, op_residual_ut, op_jacobian));

  // Smoother values
  if (material->num_params_smoother_values > 0) {
    CeedOperator *sub_operators;

    RatelCallCeed(material->ratel, CeedCompositeOperatorGetSubList(op_jacobian, &sub_operators));
    PetscCall(PetscCalloc1(material->num_params_smoother_values, &material->ctx_params_labels));
    for (PetscInt i = 0; i < material->num_params_smoother_values; i++) {
      RatelCallCeed(material->ratel, CeedOperatorGetContextFieldLabel(sub_operators[jacobian_index], material->ctx_params_label_names[i],
                                                                      &material->ctx_params_labels[i]));
    }
  }

  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Jacobian Suboperators Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup Block Preconditioner CeedOperator for RatelMaterial

  @param[in]   material           RatelMaterial context
  @param[in]   dm                 DM for the given field
  @param[in]   field              Field that we wan to apply pc
  @param[out]  op_jacobian_block  CeedOperator created for the given field

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupJacobianBlockSuboperator(RatelMaterial material, DM dm, PetscInt field, CeedOperator op_jacobian_block) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Jacobian Block Suboperator"));

  PetscCheck(material->num_jacobian_indices != 0, material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Jacobian operator not set up for material %s",
             material->name);

  // Setup suboperators
  PetscCall((*material->SetupJacobianBlockSuboperator)(material, dm, field, op_jacobian_block));

  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Jacobian Block Suboperator Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Set Jacobian multiplicity skip indices, multigrid level function, and smoother indices for a boundary for a `RatelMaterial`.

  Collective across MPI processes.

  @param[in]  material               `RatelMaterial` context
  @param[in]  op_jacobian            Jacobian `CeedOperator`
  @param[in]  old_num_sub_operators  Number of suboperators in Jacobian `CeedOperator` before adding boundary suboperators
  @param[in]  new_num_sub_operators  Number of suboperators in Jacobian `CeedOperator` after adding boundary suboperators
  @param[in]  setup_multigrid_level  `RatelMaterialSetupMultigridLevelFunction` to use for setting up multigrid level for boundary suboperators

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetBoundaryJacobianMultigridInfo(RatelMaterial material, CeedOperator op_jacobian, PetscInt old_num_sub_operators,
                                                             PetscInt                                 new_num_sub_operators,
                                                             RatelMaterialSetupMultigridLevelFunction setup_multigrid_level) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup Boundary Jacobian"));

  CeedInt num_boundary_sub_operators = new_num_sub_operators - old_num_sub_operators;
  if (num_boundary_sub_operators > 0) {
    CeedOperator *sub_operators;
    const CeedInt num_residual_indices = material->num_residual_indices + num_boundary_sub_operators;
    const CeedInt num_jacobian_indices = material->num_jacobian_indices + num_boundary_sub_operators;

    // -- Reallocate arrays
    {
      PetscSizeT int_size = sizeof(PetscInt);

      PetscCall(PetscRealloc(num_residual_indices * int_size, &material->residual_indices));
      PetscCall(PetscRealloc(num_jacobian_indices * int_size, &material->jacobian_indices));
      PetscCall(
          PetscRealloc(num_jacobian_indices * sizeof(*material->RatelMaterialSetupMultigridLevels), &material->RatelMaterialSetupMultigridLevels));
      if (!material->ratel->jacobian_multiplicity_skip_indices) {
        material->ratel->num_jacobian_multiplicity_skip_indices = 0;
        PetscCall(PetscCalloc1(num_boundary_sub_operators, &material->ratel->jacobian_multiplicity_skip_indices));
      } else {
        PetscInt num_skip = material->ratel->num_jacobian_multiplicity_skip_indices + num_boundary_sub_operators;
        PetscCall(PetscRealloc(num_skip * int_size, &material->ratel->jacobian_multiplicity_skip_indices));
      }
      if (material->num_params_smoother_values > 0) {
        PetscInt num_labels = num_jacobian_indices * material->num_params_smoother_values;
        PetscCall(PetscRealloc(num_labels * sizeof(*material->ctx_params_labels), &material->ctx_params_labels));
      }
    }

    // -- Update arrays
    for (PetscInt residual_index = old_num_sub_operators; residual_index < new_num_sub_operators; residual_index++) {
      // ---- Residual index
      material->residual_indices[material->num_residual_indices] = residual_index;
      material->num_residual_indices++;
    }
    RatelCallCeed(material->ratel, CeedCompositeOperatorGetSubList(op_jacobian, &sub_operators));
    for (PetscInt jacobian_index = old_num_sub_operators; jacobian_index < new_num_sub_operators; jacobian_index++) {
      // ---- Jacobian index
      material->jacobian_indices[material->num_jacobian_indices]                                                   = jacobian_index;
      material->RatelMaterialSetupMultigridLevels[material->num_jacobian_indices]                                  = setup_multigrid_level;
      // ---- Jacobian multiplicity skip index
      material->ratel->jacobian_multiplicity_skip_indices[material->ratel->num_jacobian_multiplicity_skip_indices] = jacobian_index;
      // ---- Smoother values
      if (material->num_params_smoother_values > 0) {
        for (PetscInt j = 0; j < material->num_params_smoother_values; j++) {
          PetscInt label_index = material->num_jacobian_indices * material->num_params_smoother_values + j;
          RatelCallCeed(material->ratel, CeedOperatorGetContextFieldLabel(sub_operators[jacobian_index], material->ctx_params_label_names[j],
                                                                          &material->ctx_params_labels[label_index]));
        }
      }
      material->num_jacobian_indices++;
      material->ratel->num_jacobian_multiplicity_skip_indices++;
    }
  }

  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup Boundary Jacobian Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup residual and Jacobian platen boundary condition `CeedOperator` for `RatelMaterial`.

  Collective across MPI processes.

  @param[in]   material       `RatelMaterial` context
  @param[in]   u_dot_loc      CeedVector for passive input velocity field
  @param[out]  op_residual_u  Composite residual u term `CeedOperator` to add `RatelMaterial` suboperator
  @param[out]  op_jacobian    Composite Jacobian `CeedOperator` to add `RatelMaterial` suboperator

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupPlatenSuboperators(RatelMaterial material, CeedVector u_dot_loc, CeedOperator op_residual_u,
                                                    CeedOperator op_jacobian) {
  Ratel   ratel = material->ratel;
  DM      dm_solution;
  CeedInt old_num_jacobian_sub_operators, new_num_jacobian_sub_operators;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Platen Suboperators"));

  // Get DM
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));

  if (material == ratel->materials[0]) {
    RatelCallCeed(ratel, CeedCompositeOperatorGetNumSub(op_jacobian, &old_num_jacobian_sub_operators));
    PetscCall(RatelCeedAddBoundariesPlatenPenalty(ratel, dm_solution, u_dot_loc, op_residual_u, op_jacobian));
    RatelCallCeed(ratel, CeedCompositeOperatorGetNumSub(op_jacobian, &new_num_jacobian_sub_operators));
    // Set Jacobian info for material
    PetscCall(RatelMaterialSetBoundaryJacobianMultigridInfo(material, op_jacobian, old_num_jacobian_sub_operators, new_num_jacobian_sub_operators,
                                                            &RatelMaterialSetupPlatenPenaltyJacobianMultigridLevel));
  }

  RatelCallCeed(ratel, CeedCompositeOperatorGetNumSub(op_jacobian, &old_num_jacobian_sub_operators));
  PetscCall(RatelMaterialCeedAddBoundariesPlatenNitsche(material, u_dot_loc, op_residual_u, op_jacobian));
  RatelCallCeed(ratel, CeedCompositeOperatorGetNumSub(op_jacobian, &new_num_jacobian_sub_operators));
  // Set Jacobian info for material
  PetscCall(RatelMaterialSetBoundaryJacobianMultigridInfo(material, op_jacobian, old_num_jacobian_sub_operators, new_num_jacobian_sub_operators,
                                                          &RatelMaterialSetupPlatenNitscheJacobianMultigridLevel));

  // Cleanup
  PetscCall(DMDestroy(&dm_solution));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Platen Suboperators Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}
/**
  @brief Setup multigrid level `CeedOperator` for `RatelMaterial`.
  @param[in]      material            `RatelMaterial` context
  @param[in]      dm_level            `DMPlex` for multigrid level to setup
  @param[in]      m_loc               `CeedVector` holding multiplicity
  @param[in]      op_jacobian_fine    Composite Jacobian `CeedOperator` holding fine grid suboperators
  @param[in,out]  op_jacobian_coarse  Composite Jacobian `CeedOperator` to add `RatelMaterial` suboperators
  @param[in,out]  op_prolong          Composite prolongation `CeedOperator` to add `RatelMaterial` suboperators
  @param[in,out]  op_restrict         Composite restriction `CeedOperator` to add `RatelMaterial` suboperators

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupMultigridLevel(RatelMaterial material, DM dm_level, CeedVector m_loc, CeedOperator op_jacobian_fine,
                                                CeedOperator op_jacobian_coarse, CeedOperator op_prolong, CeedOperator op_restrict) {
  CeedOperator *sub_operators;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Multigrid Level"));

  // Check for residual setup
  PetscCheck(material->num_residual_indices != 0, material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Residual suboperator not set up for material %s",
             material->name);

  // Setup suboperators
  RatelCallCeed(material->ratel, CeedCompositeOperatorGetSubList(op_jacobian_fine, &sub_operators));
  for (PetscInt i = 0; i < material->num_jacobian_indices; i++) {
    if (material->RatelMaterialSetupMultigridLevels[i] != NULL) {
      PetscCall((*material->RatelMaterialSetupMultigridLevels[i])(material, dm_level, m_loc, sub_operators[material->jacobian_indices[i]],
                                                                  op_jacobian_coarse, op_prolong, op_restrict));
    }
  }

  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Multigrid Level Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Set smoother contexts for Jacobian operators for `RatelMaterial`.

  Not collective across MPI processes.

  @param[in]      material      `RatelMaterial` context
  @param[in]      set_or_unset  Boolean flag to set (`PETSC_TRUE`) or unset (`PETSC_FALSE`) the smoother context
  @param[in,out]  op_jacobian   Composite Jacobian `CeedOperator` to update
  @param[out]     was_set       Boolean flag indicating if any smoother context was set.

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetJacobianSmootherContext(RatelMaterial material, PetscBool set_or_unset, CeedOperator op_jacobian, PetscBool *was_set) {
  Ratel ratel = material->ratel;

  PetscFunctionBeginUser;
  if (material->num_params_smoother_values > 0) {
    CeedOperator *sub_operators;

    RatelCallCeed(ratel, CeedCompositeOperatorGetSubList(op_jacobian, &sub_operators));

    // Loop over all Jacobians set by this material
    for (PetscInt i = 0; i < material->num_jacobian_indices; i++) {
      for (PetscInt j = 0; j < material->num_params_smoother_values; j++) {
        CeedScalar value = set_or_unset ? material->ctx_params_smoother_values[j] : material->ctx_params_values[j];

        RatelCallCeed(ratel, CeedOperatorSetContextDouble(sub_operators[material->jacobian_indices[i]],
                                                          material->ctx_params_labels[i * material->num_params_smoother_values + j], &value));
      }
      if (was_set) *was_set = PETSC_TRUE;
    }
  } else {
    if (was_set) *was_set = PETSC_FALSE;
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Accept material state update after `TS` update.

  not collective across MPI processes.

  @param[in]      material     `RatelMaterial` context
  @param[in,out]  op_residual  Composite residual `CeedOperator` to update

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialAcceptState(RatelMaterial material, CeedOperator op_residual) {
  Ratel ratel = material->ratel;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug(ratel, "-- State components: %d", material->model_data->num_comp_state));

  if (material->model_data->num_comp_state > 0) {
    CeedOperator     *sub_ops;
    const char       *input_name  = "model state";
    const char       *output_name = "current model state";
    CeedOperatorField input_field, output_field;
    CeedVector        input_vec, output_vec;

    RatelCallCeed(ratel, CeedCompositeOperatorGetSubList(op_residual, &sub_ops));
    for (PetscInt i = 0; i < material->num_residual_indices; i++) {
      RatelCallCeed(ratel, CeedOperatorGetFieldByName(sub_ops[material->residual_indices[i]], input_name, &input_field));
      RatelCallCeed(ratel, CeedOperatorGetFieldByName(sub_ops[material->residual_indices[i]], output_name, &output_field));
      if (input_field && output_field) {
        RatelCallCeed(ratel, CeedOperatorFieldGetVector(input_field, &input_vec));
        RatelCallCeed(ratel, CeedOperatorFieldGetVector(output_field, &output_vec));
        RatelCallCeed(ratel, CeedVectorCopy(output_vec, input_vec));
        // MPM data must be synced to the host array
        if (ratel->method_type == RATEL_METHOD_MPM) RatelCallCeed(ratel, CeedVectorSyncArray(input_vec, CEED_MEM_HOST));
        RatelCallCeed(ratel, CeedVectorDestroy(&input_vec));
        RatelCallCeed(ratel, CeedVectorDestroy(&output_vec));
        break;
      }
      PetscCall(RatelDebug(ratel, "-- Assigned %s to %s", input_name, output_name));
    }
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup strain energy `CeedOperator` for `RatelMaterial`.

  Collective across MPI processes.

  @param[in]   material          `RatelMaterial` context
  @param[in]   dm_energy         `DM` for strain energy computation
  @param[out]  op_strain_energy  Composite strain energy `CeedOperator` to add `RatelMaterial` suboperator

@return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupStrainEnergySuboperator(RatelMaterial material, DM dm_energy, CeedOperator op_strain_energy) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Strain Energy Suboperator"));

  // Skip if no energy QFunction
  if (!material->model_data->strain_energy) PetscFunctionReturn(PETSC_SUCCESS);

  // Check for residual setup
  PetscCheck(material->num_residual_indices != 0, material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Residual suboperator not set up for material %s",
             material->name);

  // Setup energy suboperator
  PetscCall((*material->SetupStrainEnergySuboperator)(material, dm_energy, op_strain_energy));

  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Strain Energy Suboperator Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup diagnostic value `CeedOperator` for `RatelMaterial`.

  Collective across MPI processes.

  @param[in]   material                 `RatelMaterial` context
  @param[in]   dm_projected_diagnostic  `DM` for projected diagnostic value computation
  @param[in]   dm_dual_diagnostic       `DM` for dual space diagnostic value computation
  @param[out]  op_mass_diagnostic       Composite diagnostic value projection `CeedOperator` to add `RatelMaterial` suboperator
  @param[out]  op_projected_diagnostic  Composite projected diagnostic value `CeedOperator` to add `RatelMaterial` suboperator
  @param[out]  op_dual_diagnostic       Composite dual space diagnostic value `CeedOperator` to add `RatelMaterial` suboperator
  @param[out]  op_dual_nodal_scale      Composite dual space nodal scale `CeedOperator` to add `RatelMaterial` suboperator

@return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupDiagnosticSuboperators(RatelMaterial material, DM dm_projected_diagnostic, DM dm_dual_diagnostic,
                                                        CeedOperator op_mass_diagnostic, CeedOperator op_projected_diagnostic,
                                                        CeedOperator op_dual_diagnostic, CeedOperator op_dual_nodal_scale) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Diagnostic Suboperators"));

  // Check for residual setup
  PetscCheck(material->num_residual_indices != 0, material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Residual suboperator not set up for material %s",
             material->name);

  // Setup volume diagnostic value suboperator
  PetscCall((*material->SetupDiagnosticSuboperators)(material, dm_projected_diagnostic, dm_dual_diagnostic, op_mass_diagnostic,
                                                     op_projected_diagnostic, op_dual_diagnostic, op_dual_nodal_scale));

  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Diagnostic Suboperators Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup surface force `CeedOperator` for `RatelMaterial`.

  Collective across MPI processes.

  @param[in]   material           `RatelMaterial` context
  @param[in]   dm_surface_force   `DM` for surface force computation
  @param[out]  ops_surface_force  Composite surface force `CeedOperator`s to add `RatelMaterial` suboperator

@return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupSurfaceForceCellToFaceSuboperators(RatelMaterial material, DM dm_surface_force, CeedOperator *ops_surface_force) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Surface Force Suboperators"));

  // Check for residual setup
  PetscCheck(material->num_residual_indices != 0, material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Residual suboperator not set up for material %s",
             material->name);

  // Setup surface force calculation suboperator
  PetscCall((*material->SetupSurfaceForceCellToFaceSuboperators)(material, dm_surface_force, ops_surface_force));

  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Surface Force Suboperators Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup MMS error `CeedOperator` for `RatelMaterial`.

  Collective across MPI processes.

  @param[in]   material      `RatelMaterial` context
  @param[out]  op_mms_error  Composite MMS error `CeedOperator` to add `RatelMaterial` suboperator

@return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupMMSErrorSuboperator(RatelMaterial material, CeedOperator op_mms_error) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup MMS Error Suboperator"));

  // Check for residual setup
  PetscCheck(material->num_residual_indices != 0, material->ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Residual suboperator not set up for material %s",
             material->name);

  // Check for MMS error operator
  PetscCheck(material->model_data->mms_error, material->ratel->comm, PETSC_ERR_SUP, "No MMS solution for material %s", material->model_data->name);

  // Setup MMS error suboperator
  PetscCall((*material->SetupMMSErrorSuboperator)(material, op_mms_error));

  PetscCall(RatelDebug256(material->ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup MMS Error Suboperator Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}
