/// @file
/// Hyperelastic material at finite strain using Neo-Hookean model in initial configuration

#include <ceed.h>
#include <petscsys.h>
#include <ratel-impl.h>
#include <ratel-model.h>
#include <ratel.h>
#include <ratel/qfunctions/models/elasticity-neo-hookean-initial-ad-adolc.h>

RATEL_EXTERN PetscErrorCode RatelMaterialCreate_ElasticityNeoHookeanInitialAD_ADOLC_C(Ratel ratel, RatelMaterial material);

static const CeedInt active_field_num_eval_modes[] = {NUM_ACTIVE_FIELD_EVAL_MODES_NeoHookeanInitial_AD_ADOLC};

/// @addtogroup RatelMaterials
/// @{

/**
  @brief Create `RatelMaterial` model data for Neo-Hookean hyperelasticity at finite strain in initial configuration.

  Not collective across MPI processes.

  @param[in]   ratel     `Ratel` context
  @param[out]  material  `RatelMaterial` context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialCreate_ElasticityNeoHookeanInitialAD_ADOLC(Ratel ratel, RatelMaterial material) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Create Elasticity Neo-Hookean Initial AD (ADOL-C)"));

  // Model data
  PetscCall(RatelMaterialCreate_ElasticityNeoHookeanInitialAD_ADOLC_C(ratel, material));
  material->model_data->residual_u                  = ElasticityResidual_NeoHookeanInitial_AD_ADOLC;
  material->model_data->residual_u_loc              = ElasticityResidual_NeoHookeanInitial_AD_ADOLC_loc;
  material->model_data->jacobian                    = ElasticityJacobian_NeoHookeanInitial_AD_ADOLC;
  material->model_data->jacobian_loc                = ElasticityJacobian_NeoHookeanInitial_AD_ADOLC_loc;
  material->model_data->num_comp_stored_u           = NUM_COMPONENTS_STORED_NeoHookeanInitial_AD_ADOLC;
  material->model_data->active_field_num_eval_modes = active_field_num_eval_modes;
  PetscCall(RatelModelDataVerifyRelativePath(ratel, material->model_data));

  // QFunction contexts
  PetscCall(RatelMaterialParamsContextCreate_NeoHookean(material, &material->ctx_params));
  PetscCall(RatelMaterialParamsSmootherDataSetup_NeoHookean(material));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Create Elasticity Neo-Hookean Initial AD (ADOL-C) Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
@brief Register Neo-Hookean hyperelasticity at finite strain in initial configuration using AD (ADOL-C) model.

  Not collective across MPI processes.

  @param[in]   ratel                      `Ratel` context
  @param[in]   cl_argument                Command line argument to use for model
  @param[out]  material_create_functions  PETSc function list for `RatelMaterial`

@return An error code: 0 - success, otherwise - failure
**/
CEED_INTERN PetscErrorCode RatelRegisterModel_ElasticityNeoHookeanInitialAD_ADOLC(Ratel ratel, const char *cl_argument,
                                                                                  PetscFunctionList *material_create_functions) {
  PetscFunctionBeginUser;
  RATEL_MODEL_REGISTER(material_create_functions, cl_argument, ElasticityNeoHookeanInitialAD_ADOLC);
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}
