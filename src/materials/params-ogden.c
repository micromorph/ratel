/// @file
/// Setup Ogden model params context objects

#include <ceed.h>
#include <math.h>
#include <petscsys.h>
#include <ratel-impl.h>
#include <ratel-material.h>
#include <ratel-model.h>
#include <ratel.h>
#include <ratel/models/ogden.h>
#include <stddef.h>
#include <stdio.h>

/// @addtogroup RatelMaterials
/// @{

/// @brief Ogden model parameters for `CeedQFunctionContext` and viewing
struct RatelModelParameterData_private ogden_param_data_private = {
    .parameters     = {{
                           .name        = "shift v",
                           .description = "shift for U_t",
                           .is_hidden   = PETSC_TRUE,
                           .offset      = offsetof(RatelOgdenElasticityParams, common_parameters[RATEL_COMMON_PARAMETER_SHIFT_V]),
                           .type        = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name        = "shift a",
                           .description = "shift for U_tt",
                           .is_hidden   = PETSC_TRUE,
                           .offset      = offsetof(RatelOgdenElasticityParams, common_parameters[RATEL_COMMON_PARAMETER_SHIFT_A]),
                           .type        = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name            = "rho",
                           .description     = "Density",
                           .units           = "kg/m^3",
                           .restrictions    = "rho >= 0",
                           .default_value.s = 1,
                           .is_required     = PETSC_FALSE,
                           .offset          = offsetof(RatelOgdenElasticityParams, common_parameters[RATEL_COMMON_PARAMETER_RHO]),
                           .type            = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "nu",
                           .description  = "Poisson's ratio",
                           .restrictions = "0 <= nu < 0.5",
                           .is_required  = PETSC_TRUE,
                           .offset       = offsetof(RatelOgdenElasticityParams, nu),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "m",
                           .description  = "List of N shear moduli",
                           .units        = "Pa",
                           .restrictions = "m[i]*alpha[i] > 0",
                           .is_required  = PETSC_TRUE,
                           .offset       = offsetof(RatelOgdenElasticityParams, m),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "alpha",
                           .description  = "List of N constants associated with shear moduli",
                           .restrictions = "m[i]*alpha[i] > 0",
                           .is_required  = PETSC_TRUE,
                           .offset       = offsetof(RatelOgdenElasticityParams, alpha),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name            = "num ogden parameters",
                           .description     = "Number of Ogden parameters",
                           .is_hidden       = PETSC_TRUE,
                           .default_value.i = 1,
                           .offset          = offsetof(RatelOgdenElasticityParams, num_ogden_parameters),
                           .type            = CEED_CONTEXT_FIELD_INT32,
                   }, {
                           .name         = "mu",
                           .description  = "Shear Modulus: sum {m[i] alpha[i] / 2}_{i=1:N}",
                           .units        = "Pa",
                           .restrictions = "mu >= 0",
                           .is_required  = PETSC_FALSE,
                           .offset       = offsetof(RatelOgdenElasticityParams, mu),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "lambda",
                           .description  = "First Lame parameter: 2 mu nu / (1 - 2 nu)",
                           .units        = "Pa",
                           .restrictions = "lambda >= 0",
                           .is_required  = PETSC_FALSE,
                           .offset       = offsetof(RatelOgdenElasticityParams, lambda),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "bulk",
                           .description  = "Bulk Modulus: 2 mu (1 + nu) / (3 (1 - 2 nu))",
                           .units        = "Pa",
                           .restrictions = "bulk >= 0",
                           .is_required  = PETSC_FALSE,
                           .offset       = offsetof(RatelOgdenElasticityParams, bulk),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "E",
                           .description  = "Young's Modulus: 2 mu (1 + nu)",
                           .units        = "Pa",
                           .restrictions = "E >= 0",
                           .is_required  = PETSC_FALSE,
                           .offset       = offsetof(RatelOgdenElasticityParams, E),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }},
    .num_parameters = 11,
    .reference      = "ISBN: 0-471-82319-8"
};
RatelModelParameterData ogden_param_data = &ogden_param_data_private;

/**
  @brief Set `CeedQFunctionContext` data and register fields for Ogden parameters.

  Not collective across MPI processes.

  @param[in]   material              `RatelMaterial` to setup params context
  @param[in]   nu                    Poisson's ratio
  @param[in]   m                     Array of material Properties for m
  @param[in]   alpha                 Array of material Properties for alpha
  @param[in]   num_ogden_parameters  Number of parameters for Ogden model (Length of alpha=m)
  @param[in]   rho                   Density for scaled mass matrix
  @param[out]  ctx                   `CeedQFunctionContext` for Ogden parameters

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelCeedParamsContextCreate_Ogden(RatelMaterial material, CeedScalar nu, CeedScalar *m, CeedScalar *alpha,
                                                         PetscInt num_ogden_parameters, CeedScalar rho, CeedQFunctionContext *ctx) {
  Ratel                       ratel      = material->ratel;
  RatelModelParameterData     param_data = material->model_data->param_data;
  RatelOgdenElasticityParams *params;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Create Parameters Context Ogden"));

  // Create context data with correct scaling
  PetscCall(PetscNew(&params));
  CeedScalar two_mu = 0.;
  for (CeedInt i = 0; i < num_ogden_parameters; i++) {
    params->m[i]     = m[i] * ratel->material_units->Pascal;
    params->alpha[i] = alpha[i];
    two_mu += params->alpha[i] * params->m[i];
  }
  params->mu                                            = two_mu / 2.;
  params->num_ogden_parameters                          = num_ogden_parameters;
  params->nu                                            = nu;
  params->common_parameters[RATEL_COMMON_PARAMETER_RHO] = rho * (ratel->material_units->kilogram) / pow(ratel->material_units->meter, 3);
  params->lambda                                        = two_mu * params->nu / (1 - 2 * params->nu);
  params->bulk                                          = two_mu * (1 + params->nu) / (3. * (1 - 2 * params->nu));
  params->E                                             = two_mu * (1 + params->nu);

  // Create context object
  RatelCallCeed(ratel, CeedQFunctionContextCreate(ratel->ceed, ctx));
  RatelCallCeed(ratel, CeedQFunctionContextSetData(*ctx, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(*params), params));
  PetscCall(PetscFree(params));

  // Register parameters
  PetscCall(RatelModelParameterDataRegisterContextFields(ratel, param_data, *ctx));

  // Debugging output
  if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedQFunctionContextView(*ctx, stdout));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Create Parameters Context Ogden Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Build `CeedQFunctionContext` for Ogden parameters.

  Collective across MPI processes.

  @param[in]   material  `RatelMaterial` to setup params context
  @param[out]  ctx       `CeedQFunctionContext` for Ogden parameters

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialParamsContextCreate_Ogden(RatelMaterial material, CeedQFunctionContext *ctx) {
  Ratel      ratel                = material->ratel;
  PetscBool  set_nu               = PETSC_FALSE;
  PetscBool  set_m                = PETSC_FALSE;
  PetscBool  set_alpha            = PETSC_FALSE;
  PetscInt   num_ogden_parameters = RATEL_NUMBER_OGDEN_PARAMETERS;
  CeedScalar nu                   = -1.0, m[RATEL_NUMBER_OGDEN_PARAMETERS], alpha[RATEL_NUMBER_OGDEN_PARAMETERS], rho;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup Parameters Context Ogden"));

  // Set model parameter data
  material->model_data->param_data = ogden_param_data;

  // Get default values;
  PetscCall(RatelModelParameterDataGetDefaultValue_Scalar(ratel, ogden_param_data, "rho", &rho));

  // Get parameter values
  {
    char *cl_prefix;

    PetscCall(RatelMaterialGetCLPrefix(material, &cl_prefix));
    PetscOptionsBegin(ratel->comm, cl_prefix, "Ogden model parameters", NULL);

    PetscCall(PetscOptionsScalar("-nu", "Poisson ratio", NULL, nu, &nu, &set_nu));
    PetscCheck(nu >= 0 && nu < 0.5, ratel->comm, PETSC_ERR_SUP, "Ogden model requires Poisson ratio -nu option in [0, 0.5)");
    PetscCall(RatelDebug(ratel, "---- nu: %f", nu));

    m[0] = 3, alpha[0] = 2;
    PetscCall(PetscOptionsScalarArray("-m", "Material Property for Ogden model", NULL, m, &num_ogden_parameters, &set_m));
    PetscCall(PetscOptionsScalarArray("-alpha", "Material Property for Ogden model", NULL, alpha, &num_ogden_parameters, &set_alpha));
    for (CeedInt i = 0; i < num_ogden_parameters; i++) {
      PetscCheck(m[i] * alpha[i] > 0.0, ratel->comm, PETSC_ERR_SUP, "Ogden model requires positive (m[i] * alpha[i])");
    }

    PetscCall(PetscOptionsScalar("-rho", "Density for scaled mass matrix", NULL, rho, &rho, NULL));
    PetscCheck(rho >= 0.0, ratel->comm, PETSC_ERR_SUP, "Density must be a positive value");
    PetscCall(RatelDebug(ratel, "---- rho: %f", rho));

    PetscOptionsEnd();  // End of setting parameters
    PetscCall(PetscFree(cl_prefix));
  }

  // Check for all required options to be set
  PetscCheck(set_nu, ratel->comm, PETSC_ERR_SUP, "-nu option needed");
  PetscCheck(set_m, ratel->comm, PETSC_ERR_SUP, "-m option needed");
  PetscCheck(set_alpha, ratel->comm, PETSC_ERR_SUP, "-alpha option needed");

  // Set number of components for parameters
  ogden_param_data->parameters[4].num_components = num_ogden_parameters;
  ogden_param_data->parameters[5].num_components = num_ogden_parameters;

  // Create context
  PetscCall(RatelCeedParamsContextCreate_Ogden(material, nu, m, alpha, num_ogden_parameters, rho, ctx));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup Parameters Context Ogden Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup data for `CeedQFunctionContext` for smoother with Ogden parameters.

  Collective across MPI processes.

  @param[in,out]  material  `RatelMaterial` to setup params context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialParamsSmootherDataSetup_Ogden(RatelMaterial material) {
  Ratel      ratel           = material->ratel;
  PetscBool  set_nu_smoother = PETSC_FALSE;
  CeedScalar nu = -1.0, nu_smoother = -1.0;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup Parameters Smoother Context Ogden"));

  // Get parameter values
  if (material->num_params_smoother_values == 0) {
    char *cl_prefix;

    PetscCall(RatelMaterialGetCLPrefix(material, &cl_prefix));
    PetscOptionsBegin(ratel->comm, cl_prefix, "Ogden model parameters for smoother", NULL);

    PetscCall(PetscOptionsScalar("-nu", "Poisson ratio", NULL, nu, &nu, NULL));
    PetscCheck(nu >= 0 && nu < 0.5, ratel->comm, PETSC_ERR_SUP, "Ogden model requires Poisson ratio -nu option in [0, 0.5)");
    PetscCall(RatelDebug(ratel, "---- nu: %f", nu));

    PetscCall(PetscOptionsScalar("-nu_smoother", "Poisson's ratio for smoother", NULL, nu_smoother, &nu_smoother, &set_nu_smoother));
    if (set_nu_smoother) PetscCall(RatelDebug(ratel, "---- nu_smoother: %f", nu_smoother));
    else PetscCall(RatelDebug(ratel, "---- No Nu for smoother set"));

    PetscOptionsEnd();  // End of setting model parameters
    PetscCall(PetscFree(cl_prefix));

    // Store smoother values, if needed
    if (set_nu_smoother) {
      const char *label_name     = "nu";
      PetscSizeT  label_name_len = 0;

      material->num_params_smoother_values = 1;
      PetscCall(PetscNew(&material->ctx_params_values));
      PetscCall(PetscNew(&material->ctx_params_smoother_values));
      material->ctx_params_values[0]          = nu;
      material->ctx_params_smoother_values[0] = nu_smoother;
      PetscCall(PetscNew(&material->ctx_params_label_names));
      PetscCall(PetscStrlen(label_name, &label_name_len));
      PetscCall(PetscCalloc1(label_name_len + 1, &material->ctx_params_label_names[0]));
      PetscCall(PetscStrncpy(material->ctx_params_label_names[0], label_name, label_name_len + 1));
    }
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup Model Parameters Smoother Context Ogden Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}
