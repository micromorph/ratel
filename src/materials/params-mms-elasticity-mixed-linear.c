/// @file
/// Setup mixed linear elasticity MMS parameters context objects

#include <ceed.h>
#include <ratel-impl.h>
#include <ratel-model.h>
#include <ratel.h>

/// @addtogroup RatelMaterials
/// @{

/**
  @brief Build `CeedQFunctionContext` for mixed linear elasticity MMS parameters.

  Collective across MPI processes.

  @param[in]   material  `RatelMaterial` to setup model parameters context
  @param[out]  ctx       `CeedQFunctionContext` for mixed linear elasticity MMS parameters

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMMSParamsContextCreate_Elasticity_MixedLinear(RatelMaterial material, CeedQFunctionContext *ctx) {
  Ratel ratel = material->ratel;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup MMS Parameters Context Elasticity Mixed Linear"));
  PetscCall(RatelMMSParamsContextCreate_Elasticity_Linear(material, ctx));
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup MMS Parameters Context Elasticity Mixed Linear Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}
