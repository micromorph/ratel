/// @file
/// Elasticity Hencky, current configuration with damage

#include <ceed.h>
#include <petscsys.h>
#include <ratel-impl.h>
#include <ratel-model.h>
#include <ratel.h>
#include <ratel/qfunctions/geometry/surface-force.h>
#include <ratel/qfunctions/geometry/surface.h>
#include <ratel/qfunctions/geometry/volumetric.h>
#include <ratel/qfunctions/models/elasticity-hencky-damage-current-principal.h>

static const CeedInt      active_field_sizes[]          = {4};
static const char *const  active_field_names[]          = {"solution"};
static const char *const  active_component_names[]      = {"displacement_x", "displacement_y", "displacement_z", "damage"};
static const CeedInt      active_field_num_eval_modes[] = {NUM_ACTIVE_FIELD_EVAL_MODES_ElasticityHenckyDamageCurrentPrincipal};
static const CeedEvalMode active_field_eval_modes[]     = {CEED_EVAL_INTERP, CEED_EVAL_GRAD};
static const CeedInt      ut_field_num_eval_modes[]     = {NUM_U_t_FIELD_EVAL_MODES_ElasticityHenckyDamageCurrentPrincipal};
static const CeedEvalMode ut_field_eval_modes[]         = {CEED_EVAL_INTERP};

static const char *const diagnostic_field_names[] = {"displacement_x",
                                                     "displacement_y",
                                                     "displacement_z",
                                                     "Cauchy_stress_xx",
                                                     "Cauchy_stress_xy",
                                                     "Cauchy_stress_xz",
                                                     "Cauchy_stress_yy",
                                                     "Cauchy_stress_yz",
                                                     "Cauchy_stress_zz",
                                                     "pressure",
                                                     "trace_e",
                                                     "trace_e2",
                                                     "J",
                                                     "total_energy_density",
                                                     "von_Mises_stress",
                                                     "mass_density",
                                                     "damage",
                                                     "undegraded_vol_strain_energy",
                                                     "undegraded_dev_strain_energy"};

struct RatelModelData_private elasticity_hencky_damage_current_principal_data_private = {
    .name                                 = "elasticity in logarithmic strains current configuration with damage",
    .command_line_option                  = "elasticity-hencky-damage-current",
    .setup_q_data_volume                  = SetupVolumeGeometry,
    .setup_q_data_volume_loc              = SetupVolumeGeometry_loc,
    .setup_q_data_surface                 = SetupSurfaceGeometry,
    .setup_q_data_surface_loc             = SetupSurfaceGeometry_loc,
    .setup_q_data_surface_grad            = SetupSurfaceForceGeometry,
    .setup_q_data_surface_grad_loc        = SetupSurfaceForceGeometry_loc,
    .q_data_volume_size                   = Q_DATA_VOLUMETRIC_GEOMETRY_SIZE,
    .q_data_surface_size                  = Q_DATA_SURFACE_GEOMETRY_SIZE,
    .q_data_surface_grad_size             = Q_DATA_SURFACE_FORCE_GEOMETRY_SIZE,
    .num_active_fields                    = sizeof(active_field_sizes) / sizeof(*active_field_sizes),
    .active_field_sizes                   = active_field_sizes,
    .active_field_names                   = active_field_names,
    .active_component_names               = active_component_names,
    .active_field_num_eval_modes          = active_field_num_eval_modes,
    .active_field_eval_modes              = active_field_eval_modes,
    .projected_diagnostic_component_names = diagnostic_field_names,
    .num_comp_projected_diagnostic        = NUM_COMPONENTS_DIAGNOSTIC_ElasticityHenckyDamage,
    .num_comp_dual_diagnostic             = NUM_COMPONENTS_DIAGNOSTIC_ElasticityHenckyDamage_Dual,
    .dual_diagnostic_component_names      = RatelElasticityDualDiagnosticComponentNames,
    .quadrature_mode                      = CEED_GAUSS,
    .residual_u                           = ElasticityHenckyDamageCurrentPrincipal_Residual,
    .residual_u_loc                       = ElasticityHenckyDamageCurrentPrincipal_Residual_loc,
    .residual_ut                          = ElasticityDamageResidual_ut_HenckyCurrent,
    .residual_ut_loc                      = ElasticityDamageResidual_ut_HenckyCurrent_loc,
    .has_ut_term                          = PETSC_TRUE,
    .ut_field_num_eval_modes              = ut_field_num_eval_modes,
    .ut_field_eval_modes                  = ut_field_eval_modes,
    .num_comp_stored_u                    = NUM_COMPONENTS_STORED_ElasticityHenckyDamageCurrentPrincipal,
    .jacobian                             = ElasticityHenckyDamageCurrentPrincipal_Jacobian,
    .jacobian_loc                         = ElasticityHenckyDamageCurrentPrincipal_Jacobian_loc,
    .num_comp_state                       = NUM_COMPONENTS_STATE_ElasticityHenckyDamageCurrentPrincipal,
    .projected_diagnostic                 = Diagnostic_ElasticityHenckyDamage,
    .projected_diagnostic_loc             = Diagnostic_ElasticityHenckyDamage_loc,
    .dual_diagnostic                      = ElasticityDamageDualDiagnostic_Hencky,
    .dual_diagnostic_loc                  = ElasticityDamageDualDiagnostic_Hencky_loc,
    .strain_energy                        = StrainEnergy_ElasticityHenckyDamage,
    .strain_energy_loc                    = StrainEnergy_ElasticityHenckyDamage_loc,
};
RatelModelData elasticity_hencky_damage_current_principal_data = &elasticity_hencky_damage_current_principal_data_private;

/// @addtogroup RatelMaterials
/// @{

/**
  @brief Create RatelMaterial for Elasticity Hencky with Damage

  @param[in]   ratel     Ratel context
  @param[out]  material  RatelMaterial context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialCreate_ElasticityHenckyDamageCurrentPrincipal(Ratel ratel, RatelMaterial material) {
  PetscFunctionBegin;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Create ElasticityHenckyDamageCurrentPrincipal"));

  // Model data
  PetscCall(RatelModelDataVerifyRelativePath(ratel, elasticity_hencky_damage_current_principal_data));
  material->model_data = elasticity_hencky_damage_current_principal_data;

  // QFunction contexts
  PetscCall(RatelMaterialParamsContextCreate_ElasticityDamage(material, &material->ctx_params));
  PetscCall(RatelMaterialParamsSmootherDataSetup_ElasticityDamage(material));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Create ElasticityHenckyDamageCurrentPrincipal Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Register elasticity model

  @param[in]   ratel                      Ratel context
  @param[in]   cl_argument                Command line argument to use for model
  @param[out]  material_create_functions  PETSc function list for RatelMaterials

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelRegisterModel_ElasticityHenckyDamageCurrentPrincipal(Ratel ratel, const char *cl_argument,
                                                                         PetscFunctionList *material_create_functions) {
  PetscFunctionBegin;
  RATEL_MODEL_REGISTER(material_create_functions, cl_argument, ElasticityHenckyDamageCurrentPrincipal);
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}
