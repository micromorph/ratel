/// @file
/// Setup damage phase field params context objects

#include <ceed.h>
#include <math.h>
#include <petscsys.h>
#include <ratel-impl.h>
#include <ratel-material.h>
#include <ratel-model.h>
#include <ratel.h>
#include <ratel/models/elasticity-damage.h>
#include <ratel/models/platen.h>
#include <stdbool.h>
#include <stddef.h>
#include <stdio.h>

/// @addtogroup RatelMaterials
/// @{

/// @brief Elasticity+damage model parameters for `CeedQFunctionContext` and viewing
struct RatelModelParameterData_private elasticity_damage_param_data_private = {
    .parameters     = {{
                           .name        = "shift v",
                           .description = "shift for U_t",
                           .offset      = offsetof(RatelElasticityDamageParams, common_parameters[RATEL_COMMON_PARAMETER_SHIFT_V]),
                           .is_hidden   = PETSC_TRUE,
                           .type        = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name        = "shift a",
                           .description = "shift for U_tt",
                           .offset      = offsetof(RatelElasticityDamageParams, common_parameters[RATEL_COMMON_PARAMETER_SHIFT_A]),
                           .is_hidden   = PETSC_TRUE,
                           .type        = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name            = "rho",
                           .description     = "Density",
                           .units           = "kg/m^3",
                           .restrictions    = "rho >= 0",
                           .default_value.s = 1,
                           .is_required     = PETSC_FALSE,
                           .offset          = offsetof(RatelElasticityDamageParams, common_parameters[RATEL_COMMON_PARAMETER_RHO]),
                           .type            = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "nu",
                           .description  = "Poisson's ratio",
                           .restrictions = "0 <= nu < 0.5",
                           .is_required  = PETSC_TRUE,
                           .offset       = offsetof(RatelElasticityDamageParams, nu),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "E",
                           .description  = "Young's Modulus",
                           .units        = "Pa",
                           .restrictions = "E >= 0",
                           .is_required  = PETSC_TRUE,
                           .offset       = offsetof(RatelElasticityDamageParams, E),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "fracture_toughness",
                           .description  = "critical energy release rate",
                           .units        = "J/m^2",
                           .restrictions = "fracture_toughness >= 0",
                           .is_required  = PETSC_TRUE,
                           .offset       = offsetof(RatelElasticityDamageParams, fracture_toughness),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "characteristic_length",
                           .description  = "length scale parameter in diffuse crack topology",
                           .units        = "m",
                           .restrictions = "characteristic_length >= 0 and >= min mesh size",
                           .is_required  = PETSC_TRUE,
                           .offset       = offsetof(RatelElasticityDamageParams, characteristic_length),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "residual_stiffness_factor",
                           .description  = "small positive number to prevent total stiffness loss",
                           .units        = "dimensionless",
                           .restrictions = "0 <= residual_stiffness << 1",
                           .is_required  = PETSC_TRUE,
                           .offset       = offsetof(RatelElasticityDamageParams, residual_stiffness),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name            = "damage_scaling",
                           .description     = "scaling factor for damage residual",
                           .units           = "dimensionless",
                           .restrictions    = "damage_scaling >= 0",
                           .default_value.s = 1,
                           .is_required     = PETSC_FALSE,
                           .offset          = offsetof(RatelElasticityDamageParams, damage_scaling),
                           .type            = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name            = "damage_viscosity",
                           .description     = "damage viscosity",
                           .units           = "Pa s",
                           .restrictions    = "damage_viscosity >= 0",
                           .default_value.s = 0,
                           .is_required     = PETSC_FALSE,
                           .offset          = offsetof(RatelElasticityDamageParams, damage_viscosity),
                           .type            = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name            = "use_offdiagonal",
                           .description     = "boolean, turns on/off diagonal terms in the Jacobian",
                           .default_value.b = true,
                           .is_required     = PETSC_FALSE,
                           .offset          = offsetof(RatelElasticityDamageParams, use_offdiagonal),
                           .type            = CEED_CONTEXT_FIELD_BOOL,
                   }, {
                           .name            = "use_AT1",
                           .description     = "boolean, AT1 model type (should be changed to char)",
                           .default_value.b = true,
                           .is_required     = PETSC_FALSE,
                           .offset          = offsetof(RatelElasticityDamageParams, use_AT1),
                           .type            = CEED_CONTEXT_FIELD_BOOL,
                   }, {
                           .name         = "mu",
                           .description  = "Shear Modulus: E / (2 (1 + nu))",
                           .units        = "Pa",
                           .restrictions = "mu >= 0",
                           .is_required  = PETSC_FALSE,
                           .offset       = offsetof(RatelElasticityDamageParams, mu),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name         = "bulk",
                           .description  = "Bulk Modulus: 2 mu (1 + nu) / (3 (1 - 2 nu))",
                           .units        = "Pa",
                           .restrictions = "bulk >= 0",
                           .is_required  = PETSC_FALSE,
                           .offset       = offsetof(RatelElasticityDamageParams, bulk),
                           .type         = CEED_CONTEXT_FIELD_DOUBLE,
                   }, {
                           .name        = "dt",
                           .description = "time step",
                           .units       = "s",
                           .is_hidden   = PETSC_TRUE,
                           .offset      = offsetof(RatelElasticityDamageParams, dt),
                           .type        = CEED_CONTEXT_FIELD_DOUBLE,
                   }},
    .num_parameters = 15,
    .reference      = "doi:10.1016/j.jmps.2017.09.006"
};

RatelModelParameterData elasticity_damage_param_data = &elasticity_damage_param_data_private;

/**
  @brief Process command line options for Damage Phase Field params

  @param[in]   material                 RatelMaterial to setup params context
  @param[in]   nu                       Poisson's ratio
  @param[in]   E                        Young's Modulus
  @param[in]   rho                      Density for scaled mass matrix
  @param[in]   fracture_toughness       Critical energy release rate (J/m^2)
  @param[in]   characteristic_length    Length scale parameter
  @param[in]   residual_stiffness       Residual stiffness parameter
  @param[in]   damage_scaling           Scaling factor for damage residual
  @param[in]   damage_viscosity         Viscosity for viscous regularization
  @param[in]   use_offdiagonal          Switch for Jacobian offdiagonal terms
  @param[in]   use_AT1                  AT1 model type
  @param[out]  ctx                      libCEED QFunctionContext for Damage Phase Field parameters

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelCeedParamsContextCreate_ElasticityDamage(RatelMaterial material, CeedScalar nu, CeedScalar E, CeedScalar rho,
                                                                    CeedScalar fracture_toughness, CeedScalar characteristic_length,
                                                                    CeedScalar residual_stiffness, CeedScalar damage_scaling,
                                                                    CeedScalar damage_viscosity, bool use_offdiagonal, bool use_AT1,
                                                                    CeedQFunctionContext *ctx) {
  Ratel                        ratel = material->ratel;
  RatelElasticityDamageParams *params;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Create Params Context Elasticity with Damage"));

  // Create context data with correct scaling
  PetscCall(PetscNew(&params));
  params->nu                                            = nu;
  params->E                                             = E * ratel->material_units->Pascal;
  params->common_parameters[RATEL_COMMON_PARAMETER_RHO] = rho * (ratel->material_units->kilogram) / pow(ratel->material_units->meter, 3);
  params->fracture_toughness                            = fracture_toughness * ratel->material_units->JoulePerSquareMeter;
  params->characteristic_length                         = characteristic_length * ratel->material_units->meter;
  params->residual_stiffness                            = residual_stiffness;
  params->damage_scaling                                = damage_scaling;
  params->damage_viscosity                              = damage_viscosity * ratel->material_units->Pascal * ratel->material_units->second;
  params->use_offdiagonal                               = use_offdiagonal;
  params->use_AT1                                       = use_AT1;
  params->mu                                            = params->E / (2 * (1 + params->nu));
  params->bulk                                          = 2 * params->mu * (1 + params->nu) / (3. * (1 - 2 * params->nu));

  // Create context object
  RatelCallCeed(ratel, CeedQFunctionContextCreate(ratel->ceed, ctx));
  RatelCallCeed(ratel, CeedQFunctionContextSetData(*ctx, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(*params), params));
  PetscCall(PetscFree(params));

  // Common parameters
  PetscCall(RatelModelParameterDataRegisterContextFields(ratel, material->model_data->param_data, *ctx));

  // Debugging output
  if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedQFunctionContextView(*ctx, stdout));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Create Parameters Context Elasticity with Damage Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Build libCEED QFunctionContext with damage phase field parameters

  @param[in]   material  RatelMaterial to setup parameters context
  @param[out]  ctx       libCEED QFunctionContext for damage phase field parameters

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialParamsContextCreate_ElasticityDamage(RatelMaterial material, CeedQFunctionContext *ctx) {
  Ratel      ratel                     = material->ratel;
  PetscBool  set_Youngs                = PETSC_FALSE;
  PetscBool  set_nu                    = PETSC_FALSE;
  PetscBool  set_fracture_toughness    = PETSC_FALSE;
  PetscBool  set_characteristic_length = PETSC_FALSE;
  PetscBool  set_residual_stiffness    = PETSC_FALSE;
  PetscBool  set_damage_scaling        = PETSC_FALSE;
  PetscBool  set_damage_viscosity      = PETSC_FALSE;
  PetscBool  set_offdiagonal           = PETSC_FALSE;
  PetscBool  set_AT1                   = PETSC_FALSE;
  CeedScalar nu = -1.0, E = -1.0, rho = 1.0, fracture_toughness = -1.0, characteristic_length = -1.0;
  CeedScalar residual_stiffness = -1.0, damage_scaling = 1.0, damage_viscosity = 0;
  PetscBool  use_offdiagonal = true, use_AT1 = true;

  PetscFunctionBegin;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup Parameters Context Damage Phase Field"));

  // Set model parameter data
  material->model_data->param_data = elasticity_damage_param_data;

  // Get default values
  PetscCall(RatelModelParameterDataGetDefaultValue_Scalar(ratel, elasticity_damage_param_data, "rho", &rho));
  PetscCall(RatelModelParameterDataGetDefaultValue_Bool(ratel, elasticity_damage_param_data, "use_offdiagonal", &use_offdiagonal));
  PetscCall(RatelModelParameterDataGetDefaultValue_Bool(ratel, elasticity_damage_param_data, "use_AT1", &use_AT1));

  // Get parameter values
  {
    char *cl_prefix;

    PetscCall(RatelMaterialGetCLPrefix(material, &cl_prefix));
    PetscOptionsBegin(ratel->comm, cl_prefix, "Damage Phase Field parameters", NULL);

    PetscCall(PetscOptionsScalar("-rho", "Density for scaled mass matrix", NULL, rho, &rho, NULL));
    PetscCheck(rho >= 0.0, ratel->comm, PETSC_ERR_SUP, "Density must be a positive value");
    PetscCall(RatelDebug(ratel, "---- rho: %f", rho));

    PetscCall(PetscOptionsScalar("-nu", "Poisson's ratio", NULL, nu, &nu, &set_nu));
    PetscCheck(nu >= 0.0, ratel->comm, PETSC_ERR_SUP, "Density must be a positive value");
    PetscCall(RatelDebug(ratel, "---- nu: %f", nu));

    PetscCall(PetscOptionsScalar("-E", "Young's Modulus", NULL, E, &E, &set_Youngs));
    PetscCheck(E >= 0.0, ratel->comm, PETSC_ERR_SUP, "Young modulus must be a positive value");
    PetscCall(RatelDebug(ratel, "---- E: %f", E));

    PetscCall(PetscOptionsScalar("-fracture_toughness", "Fracture toughness as critical energy release rate", NULL, fracture_toughness,
                                 &fracture_toughness, &set_fracture_toughness));
    PetscCheck(fracture_toughness >= 0.0, ratel->comm, PETSC_ERR_SUP, "Fracture toughness must be a positive value");
    PetscCall(RatelDebug(ratel, "---- fracture_toughness: %f", fracture_toughness));

    PetscCall(PetscOptionsScalar("-characteristic_length", "Length scale parameter", NULL, characteristic_length, &characteristic_length,
                                 &set_characteristic_length));
    PetscCheck(characteristic_length >= 0.0, ratel->comm, PETSC_ERR_SUP, "Length scale parameter must be a positive value");
    PetscCall(RatelDebug(ratel, "---- characteristic_length: %f", characteristic_length));

    PetscCall(
        PetscOptionsScalar("-residual_stiffness", "residual stiffness", NULL, residual_stiffness, &residual_stiffness, &set_residual_stiffness));
    PetscCheck(residual_stiffness >= 0.0, ratel->comm, PETSC_ERR_SUP, "Residual stiffness must be a positive value");
    PetscCall(RatelDebug(ratel, "---- residual_stiffness: %f", residual_stiffness));

    PetscCall(PetscOptionsScalar("-damage_scaling", "scaling for damage residual", NULL, damage_scaling, &damage_scaling, &set_damage_scaling));
    PetscCheck(damage_scaling >= 0.0, ratel->comm, PETSC_ERR_SUP, "Scaling factor for damage residual must be a positive value");
    PetscCall(RatelDebug(ratel, "---- damage_scaling: %f", damage_scaling));

    PetscCall(PetscOptionsScalar("-damage_viscosity", "damage viscosity", NULL, damage_viscosity, &damage_viscosity, &set_damage_viscosity));
    PetscCheck(damage_viscosity >= 0.0, ratel->comm, PETSC_ERR_SUP, "Damage viscosity must be a positive value");
    PetscCall(RatelDebug(ratel, "---- damage viscosity: %f", damage_viscosity));

    PetscCall(
        PetscOptionsBool("-use_offdiagonal", "Switch for jacobian offdiagonal terms", NULL, use_offdiagonal, &use_offdiagonal, &set_offdiagonal));
    if (set_offdiagonal) PetscCall(RatelDebug(ratel, "---- offdiagonal: ", use_offdiagonal));
    else PetscCall(RatelDebug(ratel, "---- No offdiagonal set"));

    PetscCall(PetscOptionsBool("-use_AT1", "Model type", NULL, use_AT1, &use_AT1, &set_AT1));
    if (set_AT1) PetscCall(RatelDebug(ratel, "---- AT1 model type (AT1 - true, AT2 - false) : ", use_AT1));
    else PetscCall(RatelDebug(ratel, "---- No AT1 model type set"));

    PetscOptionsEnd();  // End of setting Params
    PetscCall(PetscFree(cl_prefix));
  }

  // Check for all required options to be set
  PetscCheck(set_nu, ratel->comm, PETSC_ERR_SUP, "-nu option needed");
  PetscCheck(set_Youngs, ratel->comm, PETSC_ERR_SUP, "-E option needed");
  PetscCheck(set_fracture_toughness, ratel->comm, PETSC_ERR_SUP, "-Fracture toughness (Gc) option needed");
  PetscCheck(set_characteristic_length, ratel->comm, PETSC_ERR_SUP, "-Length scale parameter (l0) option needed");
  PetscCheck(set_residual_stiffness, ratel->comm, PETSC_ERR_SUP, "-Residual stiffness (eta) option needed");

  // Create context
  PetscCall(RatelCeedParamsContextCreate_ElasticityDamage(material, nu, E, rho, fracture_toughness, characteristic_length, residual_stiffness,
                                                          damage_scaling, damage_viscosity, use_offdiagonal, use_AT1, ctx));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup Model Parameters Context Elasticity with Damage Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup data for libCEED QFunctionContext for smoother with ELasticityDamage parameters

  @param[in, out]   material      RatelMaterial to setup model params context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialParamsSmootherDataSetup_ElasticityDamage(RatelMaterial material) {
  Ratel      ratel = material->ratel;
  CeedScalar nu = 0.0, nu_smoother = 0.0;
  PetscBool  set_nu_smoother = PETSC_FALSE;

  PetscFunctionBegin;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup Model Parameters Smoother Context Brittle Fracture"));

  // Get parameter values
  if (material->num_params_smoother_values == 0) {
    char *cl_prefix;

    PetscCall(RatelMaterialGetCLPrefix(material, &cl_prefix));
    PetscOptionsBegin(ratel->comm, cl_prefix, "Brittle Fracture model parameters for smoother", NULL);

    PetscCall(PetscOptionsScalar("-nu", "Poisson's ratio", NULL, nu, &nu, NULL));
    PetscCheck(nu >= 0 && nu < 0.5, ratel->comm, PETSC_ERR_SUP, "Brittle fracture model requires Poisson ratio -nu option in [0, 0.5)");
    PetscCall(RatelDebug(ratel, "---- nu: %f", nu));

    PetscCall(PetscOptionsScalar("-nu_smoother", "Poisson's ratio for smoother", NULL, nu_smoother, &nu_smoother, &set_nu_smoother));
    if (set_nu_smoother) PetscCall(RatelDebug(ratel, "---- nu_smoother: %f", nu_smoother));
    else PetscCall(RatelDebug(ratel, "---- No Nu for smoother set"));

    PetscOptionsEnd();  // End of setting model parameters
    PetscCall(PetscFree(cl_prefix));

    // Store smoother values, if needed
    if (set_nu_smoother) {
      const char *label_name     = "nu";
      PetscSizeT  label_name_len = 0;

      material->num_params_smoother_values = 1;
      PetscCall(PetscNew(&material->ctx_params_values));
      PetscCall(PetscNew(&material->ctx_params_smoother_values));
      material->ctx_params_values[0]          = nu;
      material->ctx_params_smoother_values[0] = nu_smoother;
      PetscCall(PetscNew(&material->ctx_params_label_names));
      PetscCall(PetscStrlen(label_name, &label_name_len));
      PetscCall(PetscCalloc1(label_name_len + 1, &material->ctx_params_label_names[0]));
      PetscCall(PetscStrncpy(material->ctx_params_label_names[0], label_name, label_name_len + 1));
    }
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup Model Parameters Smoother Context Brittle Fracture Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}
