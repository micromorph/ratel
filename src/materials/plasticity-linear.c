/// @file
/// Linear plasticity at finite strain

#include <ceed.h>
#include <petscsys.h>
#include <ratel-impl.h>
#include <ratel-model.h>
#include <ratel.h>
#include <ratel/qfunctions/geometry/surface-force.h>
#include <ratel/qfunctions/geometry/surface.h>
#include <ratel/qfunctions/geometry/volumetric.h>
#include <ratel/qfunctions/models/elasticity-dual-diagnostic.h>
#include <ratel/qfunctions/models/plasticity-linear.h>
#include <ratel/qfunctions/scaled-mass.h>

static const CeedInt      active_field_sizes[]                   = {3};
static const char *const  active_field_names[]                   = {"displacement"};
static const char *const  active_component_names[]               = {"u_x", "u_y", "u_z"};
static const CeedInt      active_field_num_eval_modes[]          = {NUM_ACTIVE_FIELD_EVAL_MODES_PlasticityLinear};
static const CeedEvalMode active_field_eval_modes[]              = {CEED_EVAL_GRAD};
static const char *const  projected_diagnostic_component_names[] = {"displacement_x",
                                                                    "displacement_y",
                                                                    "displacement_z",
                                                                    "Cauchy_stress_xx",
                                                                    "Cauchy_stress_xy",
                                                                    "Cauchy_stress_xz",
                                                                    "Cauchy_stress_yy",
                                                                    "Cauchy_stress_yz",
                                                                    "Cauchy_stress_zz",
                                                                    "pressure",
                                                                    "volumetric_strain",
                                                                    "trace_E2",
                                                                    "J",
                                                                    "strain_energy_density",
                                                                    "von_Mises_stress",
                                                                    "mass_density",
                                                                    "plastic_strain_xx",
                                                                    "plastic_strain_xy",
                                                                    "plastic_strain_xz",
                                                                    "plastic_strain_yy",
                                                                    "plastic_strain_yz",
                                                                    "plastic_strain_zz",
                                                                    "accumulated_plastic"};

// State fields used in this model:
// Plastic strain history variable (size of 6)
// A - hardening history variable (size of 1)

struct RatelModelData_private plasticity_linear_data_private = {
    .name                                 = "Linear plasticity",
    .command_line_option                  = "plasticity-linear",
    .setup_q_data_volume                  = SetupVolumeGeometry,
    .setup_q_data_volume_loc              = SetupVolumeGeometry_loc,
    .setup_q_data_surface                 = SetupSurfaceGeometry,
    .setup_q_data_surface_loc             = SetupSurfaceGeometry_loc,
    .setup_q_data_surface_grad            = SetupSurfaceForceGeometry,
    .setup_q_data_surface_grad_loc        = SetupSurfaceForceGeometry_loc,
    .q_data_volume_size                   = Q_DATA_VOLUMETRIC_GEOMETRY_SIZE,
    .q_data_surface_size                  = Q_DATA_SURFACE_GEOMETRY_SIZE,
    .q_data_surface_grad_size             = Q_DATA_SURFACE_FORCE_GEOMETRY_SIZE,
    .num_forcing                          = 1,
    .num_active_fields                    = sizeof(active_field_sizes) / sizeof(*active_field_sizes),
    .active_field_sizes                   = active_field_sizes,
    .active_field_names                   = active_field_names,
    .active_component_names               = active_component_names,
    .active_field_num_eval_modes          = active_field_num_eval_modes,
    .active_field_eval_modes              = active_field_eval_modes,
    .num_comp_projected_diagnostic        = NUM_COMPONENTS_DIAGNOSTIC_PlasticityLinear,
    .projected_diagnostic_component_names = projected_diagnostic_component_names,
    .num_comp_dual_diagnostic             = NUM_COMPONENTS_ELASTICITY_DIAGNOSTIC_Dual,
    .dual_diagnostic_component_names      = RatelElasticityDualDiagnosticComponentNames,
    .num_comp_state                       = NUM_COMPONENTS_STATE_PlasticityLinear,
    .num_comp_stored_u                    = NUM_COMPONENTS_STORED_PlasticityLinear,
    .quadrature_mode                      = CEED_GAUSS,
    .residual_u                           = PlasticityResidual_PlasticityLinear,
    .residual_u_loc                       = PlasticityResidual_PlasticityLinear_loc,
    .residual_utt                         = ScaledMass,
    .residual_utt_loc                     = ScaledMass_loc,
    .jacobian                             = PlasticityJacobian_PlasticityLinear,
    .jacobian_loc                         = PlasticityJacobian_PlasticityLinear_loc,
    .strain_energy                        = StrainEnergy_PlasticityLinear,
    .strain_energy_loc                    = StrainEnergy_PlasticityLinear_loc,
    .projected_diagnostic                 = Diagnostic_PlasticityLinear,
    .projected_diagnostic_loc             = Diagnostic_PlasticityLinear_loc,
    .dual_diagnostic                      = ElasticityDualDiagnostic,
    .dual_diagnostic_loc                  = ElasticityDualDiagnostic_loc,
    .platen_residual_u                    = PlatenBCsResidual_PlasticityLinear,
    .platen_residual_u_loc                = PlatenBCsResidual_PlasticityLinear_loc,
    .platen_jacobian                      = PlatenBCsJacobian_PlasticityLinear,
    .platen_jacobian_loc                  = PlatenBCsJacobian_PlasticityLinear_loc,
};
RatelModelData plasticity_linear_data = &plasticity_linear_data_private;

/// @addtogroup RatelMaterials
/// @{

/**
  @brief Create `RatelMaterial` model data for linear plasticity at finite strain in initial configuration.

  Not collective across MPI processes.

  @param[in]   ratel     `Ratel` context
  @param[out]  material  `RatelMaterial` context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialCreate_PlasticityLinear(Ratel ratel, RatelMaterial material) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Create Plasticity Linear"));

  // Model data
  PetscCall(RatelModelDataVerifyRelativePath(ratel, plasticity_linear_data));
  material->model_data = plasticity_linear_data;

  // QFunction contexts
  PetscCall(RatelMaterialParamsContextCreate_Plasticity(material, &material->ctx_params));
  PetscCall(RatelMaterialParamsSmootherDataSetup_Plasticity(material));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Create Plasticity Linear Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Register linear plasticity at finite strain in initial configuration model.

  Not collective across MPI processes.

  @param[in]   ratel                      `Ratel` context
  @param[in]   cl_argument                Command line argument to use for model
  @param[out]  material_create_functions  PETSc function list for `RatelMaterial`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelRegisterModel_PlasticityLinear(Ratel ratel, const char *cl_argument, PetscFunctionList *material_create_functions) {
  PetscFunctionBeginUser;
  RATEL_MODEL_REGISTER(material_create_functions, cl_argument, PlasticityLinear);
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}
