/// @file
/// Display and command line option strings for Ratel enums

#include <ratel.h>

const char *const RatelSolverTypes[] = {
    [RATEL_SOLVER_STATIC]      = "static solver",
    [RATEL_SOLVER_QUASISTATIC] = "quasistatic solver",
    [RATEL_SOLVER_DYNAMIC]     = "dynamic solver",
};

const char *const RatelMethodTypes[] = {
    [RATEL_METHOD_FEM] = "finite element method",
    [RATEL_METHOD_MPM] = "material point method",
};

const char *const RatelMethodTypesCL[] = {
    "fem", "mpm", "RatelMethodType", "RATEL_METHOD_", 0,
};

const char *const RatelForcingTypes[] = {
    [RATEL_FORCING_NONE] = "none",
    [RATEL_FORCING_BODY] = "user-specified body force",
    [RATEL_FORCING_MMS]  = "manufactured solution",
};

const char *const RatelForcingTypesCL[] = {
    "none", "body", "mms", "RatelForcingType", "RATEL_FORCING_", 0,
};

const char *const RatelBoundaryTypes[] = {
    [RATEL_BOUNDARY_CLAMP]    = "Dirichlet clamp",
    [RATEL_BOUNDARY_MMS]      = "Dirichlet mms",
    [RATEL_BOUNDARY_TRACTION] = "Neumann traction",
    [RATEL_BOUNDARY_PLATEN]   = "Nitsche contact (platen)",
};

const char *const RatelPlatenTypes[] = {
    [RATEL_PLATEN_NITSCHE] = "Nitsche's method based-platen",
    [RATEL_PLATEN_PENALTY] = "penalty method platen",
};

const char *const RatelPlatenTypesCL[] = {
    "nitsche", "penalty", "RatelPlatenType", "RATEL_PLATEN_", 0,
};

const char *const RatelFrictionTypes[] = {
    [RATEL_FRICTION_NONE]      = "no friction",
    [RATEL_FRICTION_COULOMB]   = "Coulomb friction",
    [RATEL_FRICTION_THRELFALL] = "Regularized Coulomb friction, Threlfall",
};

const char *const RatelFrictionTypesCL[] = {
    "none", "coulomb", "threlfall", "RatelFrictionType", "RATEL_FRICTION_", 0,
};

const char *const RatelBCInterpolationTypes[] = {
    [RATEL_BC_INTERP_NONE]   = "no interpolation",
    [RATEL_BC_INTERP_LINEAR] = "linear interpolation between nodes",
};

const char *const RatelBCInterpolationTypesCL[] = {
    "none", "linear", "RatelBCInterpolationType", "RATEL_BC_INTERP_", 0,
};

const char *const RatelInitialConditionTypes[] = {
    [RATEL_INITIAL_CONDITION_ZERO]     = "zero initial condition",
    [RATEL_INITIAL_CONDITION_CONTINUE] = "continue initial condition",
};

const char *const RatelPMutigridCoarseningTypes[] = {
    [RATEL_P_MULTIGRID_COARSENING_LOGARITHMIC] = "logarithmic coarsening",
    [RATEL_P_MULTIGRID_COARSENING_UNIFORM]     = "uniform coarsening",
    [RATEL_P_MULTIGRID_COARSENING_USER]        = "user defined coarsening",
};

const char *const RatelPMultigridCoarseningTypesCL[] = {
    "logarithmic", "uniform", "user", "RatelPMultigridCoarseningTypes", "RATEL_P_MULTIGRID_COARSENING", 0,
};

const char *const RatelPointLocationTypes[] = {
    [RATEL_POINT_LOCATION_GAUSS]       = "gauss",
    [RATEL_POINT_LOCATION_UNIFORM]     = "uniform",
    [RATEL_POINT_LOCATION_CELL_RANDOM] = "cellwise-random",
};

const char *const RatelPointLocationTypesCL[] = {
    "gauss", "uniform", "cell_random", "RatelPointLocationType", "RATEL_POINT_LOCATION_", 0,
};

const char *const RatelDirectionTypesCL[] = {
    "x", "y", "z", "RatelDirectionType", "RATEL_DIRECTION_", 0,
};
