/// @file
/// Implementation of Ratel solver management interfaces

#include <ceed-boundary-evaluator.h>
#include <ceed-evaluator.h>
#include <ceed.h>
#include <mat-ceed.h>
#include <petscdm.h>
#include <petscts.h>
#include <ratel-boundary.h>
#include <ratel-dm.h>
#include <ratel-impl.h>
#include <ratel-material.h>
#include <ratel-monitor.h>
#include <ratel-mpm.h>
#include <ratel-petsc-ops.h>
#include <ratel-solver.h>
#include <ratel-utils.h>
#include <ratel.h>
#include <stdio.h>
#include <string.h>

/// @ingroup RatelSolvers
/// @{

/**
  @brief Setup solver contexts.

  Collective across MPI processes.

  @param[in,out]  ratel  `Ratel` context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDMSetupSolver(Ratel ratel) {
  DM dm_solution;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DM Setup Solver"));
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));
  PetscCall(PetscLogEventBegin(RATEL_DMSetupSolver, dm_solution, 0, 0, 0));

  // Residual and Jacobian operators
  {
    PetscInt     X_loc_size;
    Vec          X_loc;
    CeedVector   x_loc, x_dot_loc;
    CeedOperator op_residual_u, op_residual_ut, op_residual_utt, op_jacobian, op_dirichlet;

    // -- Work vectors
    PetscCall(RatelDebug(ratel, "---- Setting up work vectors"));
    PetscCall(DMCreateLocalVector(dm_solution, &X_loc));
    PetscCall(VecZeroEntries(X_loc));
    PetscCall(VecGetSize(X_loc, &X_loc_size));
    RatelCallCeed(ratel, CeedVectorCreate(ratel->ceed, X_loc_size, &x_loc));
    RatelCallCeed(ratel, CeedVectorCreate(ratel->ceed, X_loc_size, &x_dot_loc));

    // -- libCEED operators
    RatelCallCeed(ratel, CeedCompositeOperatorCreate(ratel->ceed, &op_residual_u));
    RatelCallCeed(ratel, CeedOperatorSetName(op_residual_u, "Residual evaluator, u term"));
    RatelCallCeed(ratel, CeedCompositeOperatorCreate(ratel->ceed, &op_residual_ut));
    RatelCallCeed(ratel, CeedOperatorSetName(op_residual_ut, "Residual evaluator, ut term"));
    RatelCallCeed(ratel, CeedCompositeOperatorCreate(ratel->ceed, &op_residual_utt));
    RatelCallCeed(ratel, CeedOperatorSetName(op_residual_utt, "Residual evaluator, utt term"));
    RatelCallCeed(ratel, CeedCompositeOperatorCreate(ratel->ceed, &op_jacobian));
    RatelCallCeed(ratel, CeedOperatorSetName(op_jacobian, "Jacobian"));
    RatelCallCeed(ratel, CeedCompositeOperatorCreate(ratel->ceed, &op_dirichlet));
    RatelCallCeed(ratel, CeedOperatorSetName(op_dirichlet, "Dirichlet boundary conditions"));

    // -- Residual and Jacobian
    PetscCall(RatelDebug(ratel, "---- Residual and Jacobian operators"));
    for (PetscInt i = 0; i < ratel->num_materials; i++) {
      PetscCall(RatelDebug(ratel, "------ Material %" PetscInt_FMT " volumetric residual operators", i));
      PetscCall(RatelMaterialSetupResidualSuboperators(ratel->materials[i], op_residual_u, op_residual_ut, op_residual_utt));
      PetscCall(RatelDebug(ratel, "------ Material %" PetscInt_FMT " volumetric Jacobian operator", i));
      PetscCall(RatelMaterialSetupJacobianSuboperator(ratel->materials[i], op_residual_u, op_residual_ut, op_jacobian));
      PetscCall(RatelDebug(ratel, "------ Material %" PetscInt_FMT " platen residual and Jacobian operators"));
      if (ratel->solver_type == RATEL_SOLVER_STATIC) {
        // Use u instead of u_dot for friction
        PetscCall(RatelMaterialSetupPlatenSuboperators(ratel->materials[i], x_loc, op_residual_u, op_jacobian));
      } else {
        PetscCall(RatelMaterialSetupPlatenSuboperators(ratel->materials[i], x_dot_loc, op_residual_u, op_jacobian));
      }
    }
    RatelCallCeed(ratel, CeedOperatorSetQFunctionAssemblyReuse(op_jacobian, PETSC_TRUE));

    // -- Boundaries
    // ---- Neumann boundaries
    PetscCall(RatelDebug(ratel, "---- Neumann boundaries"));
    PetscCall(RatelCeedAddBoundariesNeumann(ratel, dm_solution, op_residual_u));
    // ---- Pressure boundaries
    PetscCall(RatelDebug(ratel, "---- Pressure boundaries"));
    {
      PetscSizeT int_size = sizeof(PetscInt);
      CeedInt    old_num_sub_operators, new_num_sub_operators, num_pressure_jacobian;

      // Get index of first Pressure BC suboperator
      RatelCallCeed(ratel, CeedCompositeOperatorGetNumSub(op_jacobian, &old_num_sub_operators));
      ratel->first_jacobian_pressure_index = old_num_sub_operators;
      // Setup pressure suboperators
      PetscCall(RatelCeedAddBoundariesPressure(ratel, dm_solution, op_residual_u, op_jacobian));
      // Set new multiplicity skip indices
      RatelCallCeed(ratel, CeedCompositeOperatorGetNumSub(op_jacobian, &new_num_sub_operators));
      num_pressure_jacobian = new_num_sub_operators - old_num_sub_operators;
      if (num_pressure_jacobian > 0) {
        PetscInt num_skip = ratel->num_jacobian_multiplicity_skip_indices + num_pressure_jacobian;

        PetscCall(PetscRealloc(num_skip * int_size, &ratel->jacobian_multiplicity_skip_indices));
        for (PetscInt j = 0; j < num_pressure_jacobian; j++) {
          ratel->jacobian_multiplicity_skip_indices[ratel->num_jacobian_multiplicity_skip_indices] = ratel->first_jacobian_pressure_index + j;
          ratel->num_jacobian_multiplicity_skip_indices++;
        }
      }
    }
    // ---- Dirichlet boundaries
    PetscCall(RatelDebug(ratel, "---- Dirichlet boundaries"));
    PetscCall(RatelCeedAddBoundariesDirichletMMS(ratel, dm_solution, op_dirichlet));
    switch (ratel->method_type) {
      case RATEL_METHOD_MPM: {
        CeedOperator op_dirichlet_visualization;

        // Dirichlet boundaries, incremental
        PetscCall(RatelDebug(ratel, "---- Incremental Dirichlet boundaries"));
        PetscCall(RatelCeedAddBoundariesDirichletClamp(ratel, dm_solution, PETSC_TRUE, op_dirichlet));
        PetscCall(RatelCeedAddBoundariesDirichletSlip(ratel, dm_solution, PETSC_TRUE, op_dirichlet));
        PetscCall(CeedBoundaryEvaluatorCreate(ratel->comm, op_dirichlet, &ratel->boundary_evaluator_u));

        // Dirichlet boundaries, non-incremental for visualization
        RatelCallCeed(ratel, CeedCompositeOperatorCreate(ratel->ceed, &op_dirichlet_visualization));
        RatelCallCeed(ratel, CeedOperatorSetName(op_dirichlet_visualization, "Dirichlet boundary conditions"));
        PetscCall(RatelDebug(ratel, "---- Dirichlet boundaries for visualization"));
        PetscCall(RatelCeedAddBoundariesDirichletMMS(ratel, dm_solution, op_dirichlet_visualization));
        PetscCall(RatelCeedAddBoundariesDirichletClamp(ratel, dm_solution, PETSC_FALSE, op_dirichlet_visualization));
        PetscCall(RatelCeedAddBoundariesDirichletSlip(ratel, dm_solution, PETSC_FALSE, op_dirichlet_visualization));
        PetscCall(CeedBoundaryEvaluatorCreate(ratel->comm, op_dirichlet_visualization, &ratel->boundary_evaluator_visualization));

        // Cleanup
        RatelCallCeed(ratel, CeedOperatorDestroy(&op_dirichlet_visualization));
        break;
      }
      case RATEL_METHOD_FEM: {
        PetscCall(RatelCeedAddBoundariesDirichletClamp(ratel, dm_solution, PETSC_FALSE, op_dirichlet));
        PetscCall(RatelCeedAddBoundariesDirichletSlip(ratel, dm_solution, PETSC_FALSE, op_dirichlet));
        PetscCall(CeedBoundaryEvaluatorCreate(ratel->comm, op_dirichlet, &ratel->boundary_evaluator_u));

        PetscCall(PetscObjectReference((PetscObject)ratel->boundary_evaluator_u));
        ratel->boundary_evaluator_visualization = ratel->boundary_evaluator_u;
        break;
      }
    }
    PetscCall(DMPlexSetCeedBoundaryEvaluator(dm_solution, ratel->boundary_evaluator_visualization));

    // -- Forcing term
    PetscCall(RatelDebug(ratel, "---- Forcing operators"));
    for (PetscInt i = 0; i < ratel->num_materials; i++) {
      PetscCall(RatelMaterialSetupForcingSuboperator(ratel->materials[i], op_residual_u));
    }

    // -- Setup contexts
    PetscCall(RatelDebug(ratel, "---- Creating CeedEvaluators"));
    // ---- Residual
    PetscCall(CeedEvaluatorCreate(dm_solution, NULL, op_residual_u, &ratel->evaluator_residual_u));
    PetscCall(CeedEvaluatorSetLogEvent(ratel->evaluator_residual_u, RATEL_Residual));
    PetscCall(CeedEvaluatorSetCeedOperatorLogEvent(ratel->evaluator_residual_u, RATEL_Residual_CeedOp));
    PetscCall(VecSet(X_loc, 0.0));
    PetscCall(CeedEvaluatorSetLocalVectors(ratel->evaluator_residual_u, X_loc, NULL, NULL));
    {
      PetscBool has_x_dot_loc;

      PetscCall(CeedEvaluatorHasField(ratel->evaluator_residual_u, "x_dot", &has_x_dot_loc));
      if (has_x_dot_loc || ratel->bc_platen_count > 0) {
        Vec X_dot_loc;

        PetscCall(DMCreateLocalVector(dm_solution, &X_dot_loc));
        PetscCall(VecSet(X_dot_loc, 0.0));
        PetscCall(CeedEvaluatorSetLocalVectors(ratel->evaluator_residual_u, NULL, X_dot_loc, NULL));
        PetscCall(VecDestroy(&X_dot_loc));
        ratel->has_mixed_u_ut_term = PETSC_TRUE;
      }
    }
    PetscCall(CeedEvaluatorSetLocalCeedVectors(ratel->evaluator_residual_u, x_loc, x_dot_loc, NULL));
    PetscCall(CeedEvaluatorCreate(dm_solution, NULL, op_residual_ut, &ratel->evaluator_residual_ut));
    PetscCall(CeedEvaluatorSetLogEvent(ratel->evaluator_residual_ut, RATEL_Residual));
    PetscCall(CeedEvaluatorSetCeedOperatorLogEvent(ratel->evaluator_residual_ut, RATEL_Residual_CeedOp));
    if (ratel->has_ut_term) {
      PetscBool must_destroy = PETSC_TRUE;
      Vec       X_dot_loc;

      PetscCall(CeedEvaluatorGetLocalVectors(ratel->evaluator_residual_u, NULL, &X_dot_loc, NULL));
      if (!X_dot_loc) PetscCall(DMCreateLocalVector(dm_solution, &X_dot_loc));
      else must_destroy = PETSC_FALSE;
      PetscCall(VecSet(X_dot_loc, 0.0));
      PetscCall(CeedEvaluatorSetLocalVectors(ratel->evaluator_residual_ut, X_dot_loc, NULL, NULL));
      if (must_destroy) PetscCall(VecDestroy(&X_dot_loc));
      else PetscCall(CeedEvaluatorRestoreLocalVectors(ratel->evaluator_residual_u, NULL, &X_dot_loc, NULL));
    }
    PetscCall(CeedEvaluatorCreate(dm_solution, NULL, op_residual_utt, &ratel->evaluator_residual_utt));
    PetscCall(CeedEvaluatorSetLogEvent(ratel->evaluator_residual_utt, RATEL_Residual));
    PetscCall(CeedEvaluatorSetCeedOperatorLogEvent(ratel->evaluator_residual_utt, RATEL_Residual_CeedOp));
    if (ratel->solver_type == RATEL_SOLVER_DYNAMIC) {
      PetscBool must_destroy = PETSC_TRUE;
      Vec       X_dot_loc;

      if (ratel->has_ut_term) PetscCall(CeedEvaluatorGetLocalVectors(ratel->evaluator_residual_ut, &X_dot_loc, NULL, NULL));
      else PetscCall(CeedEvaluatorGetLocalVectors(ratel->evaluator_residual_u, NULL, &X_dot_loc, NULL));
      if (!X_dot_loc) PetscCall(DMCreateLocalVector(dm_solution, &X_dot_loc));
      else must_destroy = PETSC_FALSE;
      PetscCall(VecSet(X_dot_loc, 0.0));
      PetscCall(CeedEvaluatorSetLocalVectors(ratel->evaluator_residual_utt, X_dot_loc, NULL, NULL));
      if (must_destroy) PetscCall(VecDestroy(&X_dot_loc));
      else if (ratel->has_ut_term) PetscCall(CeedEvaluatorRestoreLocalVectors(ratel->evaluator_residual_ut, &X_dot_loc, NULL, NULL));
      else PetscCall(CeedEvaluatorRestoreLocalVectors(ratel->evaluator_residual_u, NULL, &X_dot_loc, NULL));
    }

    // ---- Jacobian
    PetscCall(MatCreateCeed(dm_solution, NULL, op_jacobian, NULL, &ratel->mat_jacobian));
    PetscCall(MatCeedSetLogEvents(ratel->mat_jacobian, RATEL_Jacobian, RATEL_Jacobian));
    PetscCall(MatCeedSetCeedOperatorLogEvents(ratel->mat_jacobian, RATEL_Jacobian_CeedOp, RATEL_Jacobian_CeedOp));
    PetscCall(MatSetOption(ratel->mat_jacobian, MAT_SPD, ratel->is_spd));
    PetscCall(MatCeedSetContext(ratel->mat_jacobian, NULL, ratel));
    PetscCall(MatCeedSetOperation(ratel->mat_jacobian, MATOP_CREATE_SUBMATRIX, (void (*)(void))RatelCreateSubmatrix));
    PetscCall(PetscObjectSetName((PetscObject)ratel->mat_jacobian, "Jacobian"));

    // ---- Jacobian for preconditioning
    {
      PetscBool is_smoother_parameter_changed = PETSC_FALSE;

      // ------ Set smoother parameter values
      for (PetscInt i = 0; i < ratel->num_materials; i++) {
        PetscBool is_current_changed = PETSC_FALSE;

        PetscCall(RatelMaterialSetJacobianSmootherContext(ratel->materials[i], PETSC_TRUE, op_jacobian, &is_current_changed));
        is_smoother_parameter_changed |= is_current_changed;
      }

      if (is_smoother_parameter_changed) {
        // ------ New mat if different parameters for smoothing
        CeedOperator op_jacobian_pre;

        PetscCall(RatelCeedOperatorClone(ratel, op_jacobian, &op_jacobian_pre));
        RatelCallCeed(ratel, CeedOperatorSetName(op_jacobian_pre, "Jacobian, preconditioning"));
        PetscCall(MatCreateCeed(dm_solution, NULL, op_jacobian_pre, NULL, &ratel->mat_jacobian_pre));
        PetscCall(MatCeedSetLogEvents(ratel->mat_jacobian_pre, RATEL_Jacobian, RATEL_Jacobian));
        PetscCall(MatCeedSetCeedOperatorLogEvents(ratel->mat_jacobian_pre, RATEL_Jacobian_CeedOp, RATEL_Jacobian_CeedOp));
        PetscCall(MatSetOption(ratel->mat_jacobian_pre, MAT_SPD, ratel->is_spd));
        PetscCall(MatCeedSetContext(ratel->mat_jacobian_pre, NULL, ratel));
        PetscCall(MatCeedSetOperation(ratel->mat_jacobian_pre, MATOP_CREATE_SUBMATRIX, (void (*)(void))RatelCreateSubmatrix));
        PetscCall(PetscObjectSetName((PetscObject)ratel->mat_jacobian_pre, "Jacobian, preconditioning"));
        RatelCallCeed(ratel, CeedOperatorDestroy(&op_jacobian_pre));

        // ------ Reset smoother parameter values
        for (PetscInt i = 0; i < ratel->num_materials; i++) {
          PetscCall(RatelMaterialSetJacobianSmootherContext(ratel->materials[i], PETSC_FALSE, op_jacobian, NULL));
        }
      } else {
        // ------ Otherwise clone references
        ratel->mat_jacobian_pre = ratel->mat_jacobian;
        PetscCall(PetscObjectReference((PetscObject)ratel->mat_jacobian));
      }
    }

    // -- Debugging information
    if (ratel->is_debug) {
      // LCOV_EXCL_START
      PetscCall(RatelDebug(ratel, "---- Final residual evaluator"));
      PetscCall(CeedEvaluatorView(ratel->evaluator_residual_u, PETSC_VIEWER_STDOUT_WORLD));
      if (ratel->solver_type == RATEL_SOLVER_DYNAMIC) {
        PetscCall(RatelDebug(ratel, "---- Final scaled mass evaluator for residual"));
        PetscCall(CeedEvaluatorView(ratel->evaluator_residual_utt, PETSC_VIEWER_STDOUT_WORLD));
      }
      PetscCall(RatelDebug(ratel, "---- Final Jacobian MatCEED"));
      PetscCall(MatView(ratel->mat_jacobian, PETSC_VIEWER_STDOUT_WORLD));
      // LCOV_EXCL_STOP
    }

    // -- Cleanup
    PetscCall(VecDestroy(&X_loc));
    RatelCallCeed(ratel, CeedVectorDestroy(&x_loc));
    RatelCallCeed(ratel, CeedVectorDestroy(&x_dot_loc));
    RatelCallCeed(ratel, CeedOperatorDestroy(&op_residual_u));
    RatelCallCeed(ratel, CeedOperatorDestroy(&op_residual_ut));
    RatelCallCeed(ratel, CeedOperatorDestroy(&op_residual_utt));
    RatelCallCeed(ratel, CeedOperatorDestroy(&op_jacobian));
    RatelCallCeed(ratel, CeedOperatorDestroy(&op_dirichlet));
  }

  // -- Initial boundary values for residual evaluation
  PetscCall(CeedEvaluatorUpdateTimeDtAndBoundaryValues(ratel->evaluator_residual_u, 1.0, 1.0));

  // -- Solver context
  switch (ratel->solver_type) {
    case RATEL_SOLVER_STATIC:
      PetscCall(RatelDebug(ratel, "---- Setting SNES contexts"));
      // -- SNES Function
      PetscCall(DMSNESSetFunction(dm_solution, RatelSNESFormResidual, ratel));
      // -- SNES Objective
      if (ratel->use_objective) PetscCall(DMSNESSetObjective(dm_solution, RatelSNESFormObjective, ratel));
      // -- SNES Jacobian
      PetscCall(DMSNESSetJacobian(dm_solution, RatelSNESFormJacobian, ratel));
      break;
    case RATEL_SOLVER_QUASISTATIC:
      PetscCall(RatelDebug(ratel, "---- Setting TS contexts"));
      // -- TS IFunction
      PetscCall(DMTSSetIFunction(dm_solution, RatelTSFormIResidual, ratel));
      // -- SNES Objective
      if (ratel->use_objective) PetscCall(DMSNESSetObjective(dm_solution, RatelSNESFormObjective, ratel));
      // -- TS IJacobian
      PetscCall(DMTSSetIJacobian(dm_solution, RatelTSFormIJacobian, ratel));
      break;
    case RATEL_SOLVER_DYNAMIC:
      // -- TS I2Function
      PetscCall(DMTSSetI2Function(dm_solution, RatelTSFormI2Residual, ratel));
      // -- TS IJacobian
      PetscCall(DMTSSetI2Jacobian(dm_solution, RatelTSFormI2Jacobian, ratel));
      break;
  }

  PetscCall(RatelMPMSetPointFields(ratel));

  PetscCall(PetscLogEventEnd(RATEL_DMSetupSolver, dm_solution, 0, 0, 0));
  PetscCall(DMDestroy(&dm_solution));
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DM Setup Solver Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}

/// @addtogroup RatelCore
/// @{

/**
  @brief Setup default `TS` options, the `DM`, and options from command line.
         Note: Sets `SNES` defaults from `RatelSNESSetup()`.

  Collective across MPI processes.

  @param[in]      ratel  `Ratel` context
  @param[in,out]  ts     `TS` object to setup

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSSetup(Ratel ratel, TS ts) {
  DM dm_solution;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel TS Setup"));

  // Set DM
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));
  PetscCheck(dm_solution, ratel->comm, PETSC_ERR_SUP, "Must set up Ratel DM with RatelDMCreate before calling RatelTSSetup");
  PetscCall(TSSetDM(ts, dm_solution));

  // TS defaults
  PetscCall(TSSetTimeStep(ts, 1.0));
  if (ratel->solver_type == RATEL_SOLVER_DYNAMIC) {
    PetscCall(TSSetType(ts, TSALPHA2));
  }

  // SNES defaults
  {
    SNES snes;

    PetscCall(TSGetSNES(ts, &snes));
    PetscCall(RatelSNESSetup(ratel, snes));
  }

  // Set additional command line options
  PetscCall(RatelTSSetFromOptions(ratel, ts));

  // Set the pre step function
  PetscCall(TSSetPreStep(ts, RatelTSPreStep));
  PetscCall(RatelDebug(ratel, "---- RatelTSPreStep set successfully"));

  // Set the pre stage function
  PetscCall(TSSetPreStage(ts, RatelTSPreStage));
  PetscCall(RatelDebug(ratel, "---- RatelTSPreStage set successfully"));

  // Set the post evaluate function
  PetscCall(TSSetPostEvaluate(ts, RatelTSPostEvaluate));
  PetscCall(RatelDebug(ratel, "---- RatelTSPostEvaluate set successfully"));

  // Set the post step function
  PetscCall(TSSetPostStep(ts, RatelTSPostStep));
  PetscCall(RatelDebug(ratel, "---- RatelTSPostStep set successfully"));

  // Cleanup
  PetscCall(DMDestroy(&dm_solution));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel TS Setup Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Set additional `Ratel` specific `TS` options.

         `-ts_monitor_strain_energy viewer:filename.extension` sets TSMonitor function for strain energy
         `-ts_monitor_surface_force viewer:filename.extension` sets TSMonitor function for surface forces
         `-ts_monitor_surface_force_cell_to_face viewer:filename.extension` sets TSMonitor function for cell-to-face surface forces
         `-ts_monitor_diagnostic_quantities viewer:filename.extension` sets TSMonitor function for diagnostic quantities
         `-ts_monitor_checkpoint viewer:filename.extension` sets TSMonitor function for solution checkpoint binaries

  Collective across MPI processes.

  @param[in]      ratel  `Ratel` context
  @param[in,out]  ts     `TS` object to setup

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSSetFromOptions(Ratel ratel, TS ts) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel TS Set From Options"));

  // Create RatelViewer(s) for strain energy monitor
  {
    RatelViewer viewers[RATEL_MAX_VIEWERS] = {0};
    PetscInt    num_viewers                = RATEL_MAX_VIEWERS;
    PetscBool   set                        = PETSC_FALSE;

    PetscCall(RatelViewerListCreateFromOptions(ratel, "-ts_monitor_strain_energy", "Set TSMonitor for strain energy", viewers, &num_viewers, &set));

    if (set) {
      for (PetscInt i = 0; i < num_viewers; i++) {
        // -- Check for supported viewer for strain energy
        PetscCheck(!strcmp(viewers[i]->viewer_type, PETSCVIEWERASCII), ratel->comm, PETSC_ERR_SUP,
                   "Only ASCII viewer supported for -ts_monitor_strain_energy");
        PetscCall(TSMonitorSet(ts, RatelTSMonitorStrainEnergy, viewers[i], (PetscErrorCode(*)(void **))RatelViewerDestroy));
      }
    }
  }

  if (ratel->surface_force_face_count > 0) {
    DM dm_solution;

    PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));
    for (PetscInt i = 0; i < ratel->surface_force_face_count; i++) {
      PetscBool has_face;

      PetscCall(RatelDMHasFace(ratel, dm_solution, ratel->surface_force_face_label_values[i], &has_face));
      PetscCheck(has_face, ratel->comm, PETSC_ERR_USER_INPUT, "Face label value %" PetscInt_FMT " specified with -surface_force_faces does not exist",
                 ratel->surface_force_face_label_values[i]);
    }
    PetscCall(DMDestroy(&dm_solution));
  }

  // Create RatelViewer(s) for surface forces & displacements monitor
  {
    RatelViewer viewers[RATEL_MAX_VIEWERS] = {0};
    PetscInt    num_viewers                = RATEL_MAX_VIEWERS;
    PetscBool   set                        = PETSC_FALSE;

    // Check for deprecated option -ts_monitor_reaction_force
    PetscOptionsBegin(ratel->comm, NULL, "", NULL);
    PetscCall(PetscOptionsDeprecated("-ts_monitor_reaction_force", "-ts_monitor_surface_force", "0.3",
                                     "The option -ts_monitor_surface_force now uses reaction forces by default."));
    PetscOptionsEnd();
    PetscCall(RatelViewerListCreateFromOptions(ratel, "-ts_monitor_surface_force", "Set TSMonitor for surface forces", viewers, &num_viewers, &set));

    if (set) {
      PetscCheck(ratel->surface_force_face_count > 0, ratel->comm, PETSC_ERR_USER_INPUT,
                 "Must specify face IDs to monitor with -surface_force_faces to use -ts_monitor_surface_force");
      // Set all viewers
      for (PetscInt i = 0; i < num_viewers; i++) {
        // -- Check for supported viewer for surface forces
        PetscCheck(!strcmp(viewers[i]->viewer_type, PETSCVIEWERASCII), ratel->comm, PETSC_ERR_SUP,
                   "Only ASCII viewer supported for -ts_monitor_surface_force");
        PetscCall(TSMonitorSet(ts, RatelTSMonitorSurfaceForce, viewers[i], (PetscErrorCode(*)(void **))RatelViewerDestroy));
      }
    }
  }

  {
    RatelViewers viewer_list;
    PetscBool    set = PETSC_FALSE;

    // Create viewer list for per-face surface forces & displacements monitor
    PetscCall(RatelViewersCreateFromOptions(ratel, "-ts_monitor_surface_force_per_face",
                                            "Set TSMonitor for surface forces, with a different file for each face", ratel->surface_force_face_count,
                                            (const char **)ratel->surface_force_face_names, &viewer_list, &set));
    // -- Check for supported viewer for surface forces
    if (set) {
      PetscCheck(ratel->surface_force_face_count > 0, ratel->comm, PETSC_ERR_USER_INPUT,
                 "Must specify face IDs to monitor with -surface_force_faces to use -ts_monitor_surface_force");
      PetscCheck(!strcmp(viewer_list->viewers[0]->viewer_type, PETSCVIEWERASCII), ratel->comm, PETSC_ERR_SUP,
                 "Only ASCII viewer supported for -ts_monitor_surface_force");
      PetscCall(TSMonitorSet(ts, RatelTSMonitorSurfaceForcePerFace, viewer_list, (PetscErrorCode(*)(void **))RatelViewersDestroy));
    }
  }

  {
    RatelViewer viewers[RATEL_MAX_VIEWERS] = {0};
    PetscInt    num_viewers                = RATEL_MAX_VIEWERS;
    PetscBool   set                        = PETSC_FALSE;

    // Create RatelViewer for cell-to-face surface forces & displacements monitor
    PetscCall(RatelViewerListCreateFromOptions(ratel, "-ts_monitor_surface_force_cell_to_face", "Set TSMonitor for surface forces", viewers,
                                               &num_viewers, &set));
    if (set) {
      PetscBool is_quiet = PETSC_FALSE;

      PetscOptionsBegin(ratel->comm, NULL, "", NULL);
      PetscCall(PetscOptionsBool("-quiet", "Quiet output", NULL, is_quiet, &is_quiet, NULL));
      PetscOptionsEnd();

      if (!is_quiet) {
        // LCOV_EXCL_START
        PetscCall(RatelPrintf256(ratel, RATEL_DEBUG_COLOR_WARNING,
                                 "WARNING: -ts_monitor_surface_force_cell_to_face is inaccurate. Use -ts_monitor_surface_force instead.\n"));
        // LCOV_EXCL_STOP
      }
      PetscCheck(ratel->surface_force_face_count > 0, ratel->comm, PETSC_ERR_USER_INPUT,
                 "Must specify face IDs to monitor with -surface_force_faces to use -ts_monitor_surface_force_cell_to_face");
      // Set all viewers
      for (PetscInt i = 0; i < num_viewers; i++) {
        PetscCheck(!strcmp(viewers[i]->viewer_type, PETSCVIEWERASCII), ratel->comm, PETSC_ERR_SUP,
                   "Only ASCII viewer supported for -ts_monitor_surface_force_cell_to_face");
        PetscCall(TSMonitorSet(ts, RatelTSMonitorSurfaceForceCellToFace, viewers[i], (PetscErrorCode(*)(void **))RatelViewerDestroy));
      }
    }
  }

  // Create a RatelViewer for diagnostics quantities
  {
    RatelViewer viewers[RATEL_MAX_VIEWERS] = {0};
    PetscInt    num_viewers                = RATEL_MAX_VIEWERS;
    PetscBool   set                        = PETSC_FALSE;

    PetscCall(RatelViewerListCreateFromOptions(ratel, "-ts_monitor_diagnostic_quantities", "Set TSMonitor for diagnostic quantities", viewers,
                                               &num_viewers, &set));
    if (set) {
      // Set all viewers
      for (PetscInt i = 0; i < num_viewers; i++) {
        PetscCall(TSMonitorSet(ts, RatelTSMonitorDiagnosticQuantities, viewers[i], (PetscErrorCode(*)(void **))RatelViewerDestroy));
      }
    }
  }

  if (ratel->method_type == RATEL_METHOD_MPM) {
    {
      RatelViewer viewers[RATEL_MAX_VIEWERS] = {0};
      PetscInt    num_viewers                = RATEL_MAX_VIEWERS;
      PetscBool   set                        = PETSC_FALSE;

      // Create a RatelViewer for swarm
      PetscCall(RatelViewerListCreateFromOptions(ratel, "-ts_monitor_swarm", "Set TSMonitor for swarm", viewers, &num_viewers, &set));
      if (set) {
        // Set all viewers
        for (PetscInt i = 0; i < num_viewers; i++) {
          PetscCheck(viewers[i]->viewer_format == PETSC_VIEWER_ASCII_XML, ratel->comm, PETSC_ERR_SUP,
                     "Only ASCII XML viewer supported for -ts_monitor_swarm");
          PetscCall(TSMonitorSet(ts, RatelTSMonitorSwarm, viewers[i], (PetscErrorCode(*)(void **))RatelViewerDestroy));
        }
      }
    }
    {
      RatelViewer viewers[RATEL_MAX_VIEWERS] = {0};
      PetscInt    num_viewers                = RATEL_MAX_VIEWERS;
      PetscBool   set                        = PETSC_FALSE;

      // Create a RatelViewer for swarm
      PetscCall(RatelViewerListCreateFromOptions(ratel, "-ts_monitor_swarm_solution", "Set TSMonitor for swarm", viewers, &num_viewers, &set));
      if (set) {
        // Set all viewers
        for (PetscInt i = 0; i < num_viewers; i++) {
          PetscCheck(viewers[i]->viewer_format == PETSC_VIEWER_ASCII_XML, ratel->comm, PETSC_ERR_SUP,
                     "Only ASCII XML viewer supported for -ts_monitor_swarm_solution");
          PetscCall(TSMonitorSet(ts, RatelTSMonitorSwarmSolution, viewers[i], (PetscErrorCode(*)(void **))RatelViewerDestroy));
        }
      }
    }
  }

  // Checkpoint monitor
  {
    RatelCheckpointCtx checkpoint_ctx;
    PetscBool          set = PETSC_FALSE;

    PetscCall(RatelCheckpointCtxCreateFromOptions(ratel, "-ts_monitor_checkpoint", "Set TSMonitor for solution checkpoint binaries", &checkpoint_ctx,
                                                  &set));
    if (set) {
      PetscCall(TSMonitorSet(ts, RatelTSMonitorCheckpoint, checkpoint_ctx, (PetscErrorCode(*)(void **))RatelCheckpointCtxDestroy));
    }
  }

  // Set rest of command line options
  PetscCall(TSSetFromOptions(ts));
  PetscCall(TSViewFromOptions(ts, NULL, "-ts_view"));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel TS Set From Options Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup default `KSP` options.

  Collective across MPI processes.

  @param[in]      ratel  `Ratel` context
  @param[in,out]  ksp    `KSP` object to setup

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelKSPSetup(Ratel ratel, KSP ksp) {
  DM      dm_solution;
  CeedInt num_active_fields = 1;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel KSP Set Defaults"));

  // Check DM
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));
  PetscCheck(dm_solution, ratel->comm, PETSC_ERR_SUP, "Must set up Ratel DM with RatelDMCreate before calling RatelKSPSetup");

  PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[0], &num_active_fields, NULL));
  // Set default KSP type and norm
  if (ratel->is_spd && num_active_fields == 1) {
    PetscCall(KSPSetType(ksp, KSPCG));
    PetscCall(KSPSetNormType(ksp, KSP_NORM_NATURAL));
  } else {
    PetscCall(KSPSetType(ksp, KSPGMRES));
    PetscCall(KSPSetNormType(ksp, KSP_NORM_PRECONDITIONED));
  }

  // Set default PC type
  {
    PetscBool is_pc_type_cl                     = PETSC_FALSE;
    char      pc_type_cl[PETSC_MAX_OPTION_NAME] = "";
    PC        pc;

    PetscOptionsBegin(ratel->comm, NULL, "", NULL);
    PetscCall(PetscOptionsString("-pc_type", "pc type", NULL, pc_type_cl, pc_type_cl, sizeof(pc_type_cl), &is_pc_type_cl));
    PetscOptionsEnd();

    PetscCall(KSPGetPC(ksp, &pc));
    if (!is_pc_type_cl) {
      if (num_active_fields == 1 && ratel->fine_grid_orders[0] == 1) {
        PetscCall(PCSetType(pc, PCGAMG));
        {
          VecType vec_type;
          MatType mat_type;

          PetscCall(DMGetVecType(dm_solution, &vec_type));
          if (strstr(vec_type, VECCUDA)) mat_type = MATAIJCUSPARSE;
          else if (strstr(vec_type, VECKOKKOS)) mat_type = MATAIJKOKKOS;
          else mat_type = MATAIJ;
          PetscCall(DMSetMatType(dm_solution, mat_type));
        }
      } else if (num_active_fields == 1 && ratel->fine_grid_orders[0] > 1) {
        PetscCall(PCSetType(pc, RATEL_PCPMG));
      } else {
        PetscCall(PCSetType(pc, PCJACOBI));
      }
    }
  }

  // Cleanup
  PetscCall(DMDestroy(&dm_solution));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel KSP Set Defaults Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup default `SNES` options, the DM, and options from command line.
           Note: Sets default `SNES` line search to critical point method.
           Note: Sets default `KSP` to `CG` with natural norm for most problems.
                 Sets default `KSP` to `GMRES` with preconditioned norm for problems with multiple active fields or platen boundary conditions.
           Note: Sets default `PC` to `PCPMG` most problems.
                 Sets default `PC` to `PCGAMG` for problems one material and linear elements.
                 Sets default `PC` to `PCJACOBI` for problems with multiple materials.

  Collective across MPI processes.

  @param[in]      ratel  `Ratel` context
  @param[in,out]  snes   `SNES` object to setup

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSNESSetup(Ratel ratel, SNES snes) {
  DM  dm_solution;
  PC  pc;
  KSP ksp;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel SNES Setup"));

  // Set DM
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));
  PetscCheck(dm_solution, ratel->comm, PETSC_ERR_SUP, "Must set up Ratel DM with RatelDMCreate before calling RatelSNESSetup");
  PetscCall(SNESSetDM(snes, dm_solution));

  // Default linesearch
  {
    CeedInt        num_active_fields;
    SNESLineSearch line_search;

    PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[0], &num_active_fields, NULL));
    PetscCall(SNESGetLineSearch(snes, &line_search));
    if (num_active_fields == 1) PetscCall(SNESLineSearchSetType(line_search, SNESLINESEARCHCP));
    else PetscCall(SNESLineSearchSetType(line_search, SNESLINESEARCHBT));
  }

  // Set KSP options
  PetscCall(SNESGetKSP(snes, &ksp));
  PetscCall(RatelKSPSetup(ratel, ksp));

  // Set additional command line options and view
  PetscCall(SNESSetFromOptions(snes));
  PetscCall(KSPGetPC(ksp, &pc));
  PetscCall(PCViewFromOptions(pc, NULL, "-pc_view"));
  PetscCall(KSPViewFromOptions(ksp, NULL, "-ksp_view"));
  PetscCall(SNESViewFromOptions(snes, NULL, "-snes_view"));

  // Cleanup
  PetscCall(DMDestroy(&dm_solution));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel SNES Setup Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup zero initial condition with random noise, if needed.

  Collective across MPI processes.

  @param[in]   ratel  `Ratel` context
  @param[out]  U      Global vector to set with zero initial condition

  @return An error code: 0 - success, otherwise - failure
**/
static inline PetscErrorCode RatelSetupZeroInitialCondition(Ratel ratel, Vec U) {
  PetscFunctionBeginUser;
  PetscScalar initial_random_scaling = 0.0;

  for (PetscInt i = 0; i < ratel->num_materials; i++) {
    PetscScalar material_initial_random_scaling = 0.0;

    PetscCall(RatelMaterialGetInitialRandomScaling(ratel->materials[i], &material_initial_random_scaling));
    initial_random_scaling = PetscMax(initial_random_scaling, material_initial_random_scaling);
  }

  PetscOptionsBegin(ratel->comm, NULL, "Ratel options", NULL);
  PetscCall(PetscOptionsReal("-ic_random_scaling", "Scaling for random noise in initial guess", NULL, initial_random_scaling, &initial_random_scaling,
                             NULL));
  PetscOptionsEnd();

  if (initial_random_scaling != 0.0) {
    PetscCall(VecSetRandom(U, NULL));
    PetscCall(VecScale(U, initial_random_scaling));
  } else {
    PetscCall(VecZeroEntries(U));
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup inital conditions for `TS` based upon command line options.

  Collective across MPI processes.

  @param[in]      ratel  `Ratel` context
  @param[in,out]  ts     `TS` to setup
  @param[out]     U      Global vector to set with initial conditions

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSSetupInitialCondition(Ratel ratel, TS ts, Vec U) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel TS Setup Initial Condition"));

  switch (ratel->initial_condition_type) {
    case RATEL_INITIAL_CONDITION_ZERO:
      PetscCall(RatelSetupZeroInitialCondition(ratel, U));
      PetscCall(TSSetTime(ts, 0.0));
      PetscCall(TSSetStepNumber(ts, 0));
      break;
    case RATEL_INITIAL_CONDITION_CONTINUE: {
      RatelCheckpointData checkpoint_data;

      // -- Read checkpoint data
      PetscCall(RatelCheckpointDataRead(ratel, ratel->continue_file_name, &checkpoint_data));

      // -- Read restart data
      PetscCall(TSSetStepNumber(ts, checkpoint_data->step));
      PetscCall(TSSetTime(ts, checkpoint_data->time));
      PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "---- Restarting at time step %" PetscInt_FMT ", time %f", checkpoint_data->step,
                              checkpoint_data->time));
      PetscCall(VecLoad(U, checkpoint_data->viewer));

      // -- Cleanup
      PetscCall(RatelCheckpointDataDestroy(&checkpoint_data));
      break;
    }
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel TS Setup Initial Condition Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup inital conditions for SNES based upon command line options.

  Collective across MPI processes.

  @param[in]      ratel  `Ratel` context
  @param[in,out]  snes   `SNES` to setup
  @param[out]     U      Global vector to set with initial conditions

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSNESSetupInitialCondition(Ratel ratel, SNES snes, Vec U) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel SNES Setup Initial Condition"));

  switch (ratel->initial_condition_type) {
    case RATEL_INITIAL_CONDITION_ZERO:
      PetscCall(RatelSetupZeroInitialCondition(ratel, U));
      break;
    case RATEL_INITIAL_CONDITION_CONTINUE: {
      PetscInt      step_number;
      PetscInt32    checkpoint_version;
      PetscDataType file_int_type = PETSC_INT32;
      PetscReal     time;
      PetscViewer   viewer;

      // -- Open file
      PetscCall(PetscViewerBinaryOpen(ratel->comm, ratel->continue_file_name, FILE_MODE_READ, &viewer));

      // -- Read restart data, discard time and step number
      PetscCall(PetscViewerBinaryRead(viewer, &checkpoint_version, 1, NULL, PETSC_INT32));
      PetscCheck(checkpoint_version == RATEL_CHECKPOINT_VERSION_INT32 || checkpoint_version == RATEL_CHECKPOINT_VERSION_INT64, ratel->comm,
                 PETSC_ERR_FILE_UNEXPECTED, "Binary file was not generated by Ratel");
      PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "---- Reading file written with PetscInt == %s",
                              checkpoint_version == RATEL_CHECKPOINT_VERSION_INT32 ? "Int32" : "Int64"));
      if (checkpoint_version == RATEL_CHECKPOINT_VERSION_INT32) file_int_type = PETSC_INT32;
      else if (checkpoint_version == RATEL_CHECKPOINT_VERSION_INT64) file_int_type = PETSC_INT64;
      PetscCall(PetscViewerBinaryRead(viewer, &step_number, 1, NULL, file_int_type));
      PetscCall(PetscViewerBinaryRead(viewer, &time, 1, NULL, PETSC_REAL));
      PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "---- Restarting at time step %" PetscInt_FMT ", time %f", step_number, time));
      PetscCall(VecLoad(U, viewer));

      // -- Cleanup
      PetscCall(PetscViewerDestroy(&viewer));
      break;
    }
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel SNES Setup Initial Condition Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Checkpoint final solution for `TS`.

  Collective across MPI processes.

  @param[in]   ratel  `Ratel` context
  @param[in]   ts     `TS` to checkpoint
  @param[out]  U      Global vector to checkpoint

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSCheckpointFinalSolutionFromOptions(Ratel ratel, TS ts, Vec U) {
  PetscBool          set;
  PetscInt           num_steps;
  PetscReal          time;
  RatelCheckpointCtx checkpoint_ctx = NULL;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel TS Checkpoint Final Solution"));

  // Get checkpoint context
  PetscCall(
      RatelCheckpointCtxCreateFromOptions(ratel, "-checkpoint_final_solution", "Checkpoint final solution for continuation", &checkpoint_ctx, &set));

  // Checkpoint
  if (set) {
    PetscCall(TSGetSolveTime(ts, &time));
    PetscCall(TSGetStepNumber(ts, &num_steps));
    PetscCall(RatelTSMonitorCheckpoint(ts, num_steps, time, U, checkpoint_ctx));
  }

  // Cleanup
  PetscCall(RatelCheckpointCtxDestroy(&checkpoint_ctx));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel TS Checkpoint Final Solution Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Checkpoint final solution for `SNES`.

  Collective across MPI processes.

  @param[in]   ratel  `Ratel` context
  @param[in]   snes   `SNES` to checkpoint
  @param[out]  U      Global vector to checkpoint

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSNESCheckpointFinalSolutionFromOptions(Ratel ratel, SNES snes, Vec U) {
  PetscBool          monitor_checkpoint;
  RatelCheckpointCtx checkpoint_ctx = NULL;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel SNES Checkpoint Final Solution"));

  // Get checkpoint context
  PetscCall(RatelCheckpointCtxCreateFromOptions(ratel, "-ts_monitor_checkpoint", "Set TSMonitor for solution checkpoint binaries", &checkpoint_ctx,
                                                &monitor_checkpoint));
  if (!monitor_checkpoint) {
    PetscCall(RatelCheckpointCtxCreateFromOptions(ratel, "-checkpoint_final_solution", "Checkpoint final solution for continuation", &checkpoint_ctx,
                                                  &monitor_checkpoint));
  }

  // Checkpoint
  if (checkpoint_ctx) PetscCall(RatelTSMonitorCheckpoint(NULL, 0, 0.0, U, checkpoint_ctx));

  // Cleanup
  PetscCall(RatelCheckpointCtxDestroy(&checkpoint_ctx));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel SNES Checkpoint Final Solution Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}
