/// @file
/// Ratel matrix shell and SNES/TS operators

#include <ceed-evaluator.h>
#include <ceed.h>
#include <mat-ceed.h>
#include <petsc/private/tsimpl.h>
#include <petscdmplex.h>
#include <petscts.h>
#include <ratel-diagnostic.h>
#include <ratel-dm.h>
#include <ratel-impl.h>
#include <ratel-material.h>
#include <ratel-mpm.h>
#include <ratel-petsc-ops.h>
#include <ratel.h>
#include <stdlib.h>
#include <string.h>

/// @addtogroup RatelInternal
/// @{

// -----------------------------------------------------------------------------
// Preconditioning
// -----------------------------------------------------------------------------

/**
  @brief Create or return a submatrix for a given `MATCEED` corresponding to a Jacobian matrix from Ratel.
         By default, this submatrix has a `MatType` of `MATCEED`.
         The `MatType` of the sub-matrix can be set with the command line option `-ceed_field_[index]_mat_type` or `-ceed_[field name]_mat_type`.

  Collective across MPI processes.

  @param[in]   A                Jacobian `MATCEED` created by Ratel
  @param[in]   is_row           Parallel `IS` for rows of the submatrix on this process
  @param[in]   is_col           Parallel `IS` for all columns of the submatrix
  @param[in]   reuse_submatrix  Either `MAT_INTITIAL_MATRIX` or `MAT_REUSE_MATRIX`
  @param[out]  A_sub            Submatrix `MATCEED`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelCreateSubmatrix(Mat A, IS is_row, IS is_col, MatReuse reuse_submatrix, Mat *A_sub) {
  Ratel ratel;

  PetscFunctionBeginUser;
  PetscCall(MatCeedGetContext(A, &ratel));

  PetscBool is_diagonal;
  PetscCall(ISEqual(is_row, is_col, &is_diagonal));
  if (!is_diagonal) {
    switch (reuse_submatrix) {
      case MAT_INITIAL_MATRIX:
        PetscCall(MatCreateSubMatrixVirtual(A, is_row, is_col, A_sub));
        break;
      case MAT_REUSE_MATRIX:
        PetscCall(MatSubMatrixVirtualUpdate(*A_sub, A, is_row, is_col));
        break;
      // LCOV_EXCL_START
      default:
        SETERRQ(PetscObjectComm((PetscObject)A), PETSC_ERR_ARG_OUTOFRANGE, "Invalid MatReuse, must be either MAT_INITIAL_MATRIX or MAT_REUSE_MATRIX");
        // LCOV_EXCL_STOP
    }
  } else {
    // Check for which block is being returned
    PetscInt  num_fields;
    DM        dm_solution, *sub_dms;
    IS       *field_is;
    PetscBool is_simplex = PETSC_FALSE;

    PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));
    PetscCall(DMCreateFieldDecomposition(dm_solution, &num_fields, NULL, &field_is, &sub_dms));
    PetscCall(DMPlexIsSimplex(dm_solution, &is_simplex));
    if (!is_simplex) {
      for (PetscInt f = 0; f < num_fields; f++) {
        PetscCall(DMPlexSetClosurePermutationTensor(sub_dms[f], PETSC_DETERMINE, NULL));
      }
    }
    PetscCheck(num_fields == ratel->num_active_fields, PetscObjectComm((PetscObject)A), PETSC_ERR_ARG_INCOMP,
               "Ratel expects %" PetscInt_FMT " fields, but DM creates %" PetscInt_FMT " splits", ratel->num_active_fields, num_fields);
    // Setup block Mat
    for (PetscInt f = 0; f < ratel->num_active_fields; f++) {
      IS        is = field_is[f];
      PetscBool equal;

      PetscCall(ISEqual(is_row, is, &equal));
      if (!equal) continue;
      // -- Create MATCEED if needed
      if (reuse_submatrix == MAT_INITIAL_MATRIX) {
        Mat          A_sub_ceed;
        CeedOperator op_jacobian_block;

        RatelCallCeed(ratel, CeedCompositeOperatorCreate(ratel->ceed, &op_jacobian_block));
        {
          char         name[PETSC_MAX_PATH_LEN];
          const char **active_field_names;

          PetscCall(RatelMaterialGetActiveFieldNames(ratel->materials[0], &active_field_names, NULL));
          PetscCall(PetscSNPrintf(name, PETSC_MAX_PATH_LEN, "block Jacobian, %s'", active_field_names[f]));
          RatelCallCeed(ratel, CeedOperatorSetName(op_jacobian_block, name));
        }
        for (PetscInt i = 0; i < ratel->num_materials; i++) {
          PetscCall(RatelMaterialSetupJacobianBlockSuboperator(ratel->materials[i], sub_dms[f], f, op_jacobian_block));
        }
        const char   *A_name;
        char          A_sub_name[PETSC_MAX_PATH_LEN];
        PetscLogEvent log_event_mult, log_event_ceed_mult, log_event_ceed_mult_transpose, log_event_mult_transpose;
        PetscBool     is_spd, is_spd_known;
        MatType       coo_mat_type;

        PetscCall(MatCeedGetLogEvents(A, &log_event_mult, &log_event_mult_transpose));
        PetscCall(MatCeedGetCeedOperatorLogEvents(A, &log_event_ceed_mult, &log_event_ceed_mult_transpose));
        PetscCall(MatCreateCeed(sub_dms[f], NULL, op_jacobian_block, NULL, &A_sub_ceed));
        PetscCall(MatCeedSetLogEvents(A_sub_ceed, log_event_mult, log_event_mult_transpose));
        PetscCall(MatCeedSetCeedOperatorLogEvents(A_sub_ceed, log_event_ceed_mult, log_event_ceed_mult_transpose));
        PetscCall(MatCeedGetCOOMatType(A, &coo_mat_type));
        PetscCall(MatCeedSetCOOMatType(A_sub_ceed, coo_mat_type));

        PetscCall(MatIsSPDKnown(A, &is_spd_known, &is_spd));
        if (is_spd_known) PetscCall(MatSetOption(A_sub_ceed, MAT_SPD, is_spd));
        PetscCall(PetscObjectGetName((PetscObject)A, &A_name));
        PetscCall(PetscSNPrintf(A_sub_name, PETSC_MAX_PATH_LEN, "%s, submatrix field %" PetscInt_FMT, A_name, f));
        PetscCall(PetscObjectSetName((PetscObject)A_sub_ceed, A_sub_name));
        PetscCall(PetscObjectCompose((PetscObject)A, A_sub_name, (PetscObject)A_sub_ceed));
        RatelCallCeed(ratel, CeedOperatorDestroy(&op_jacobian_block));
        PetscCall(MatDestroy(&A_sub_ceed));
      }
      // -- Get submatrix of correct type
      {
        const char *A_name;
        char        A_sub_name[PETSC_MAX_PATH_LEN];
        Mat         A_sub_ceed;

        // ---- Get MATCEED
        PetscCall(PetscObjectGetName((PetscObject)A, &A_name));
        PetscCall(PetscSNPrintf(A_sub_name, PETSC_MAX_PATH_LEN, "%s, submatrix field %" PetscInt_FMT, A_name, f));
        PetscCall(PetscObjectQuery((PetscObject)A, A_sub_name, (PetscObject *)&A_sub_ceed));

        // ---- Setup submatrix if needed
        if (reuse_submatrix == MAT_INITIAL_MATRIX) {
          char      option_name[PETSC_MAX_PATH_LEN], sub_mat_type[PETSC_MAX_PATH_LEN];
          PetscBool is_cl_type_set = PETSC_FALSE;

          // ---- Get MatType
          PetscOptionsBegin(ratel->comm, NULL, "", NULL);
          PetscCall(PetscSNPrintf(option_name, PETSC_MAX_PATH_LEN, "-ceed_field_%" PetscInt_FMT "_mat_type", f));
          PetscCall(PetscOptionsString(option_name, "CEEDMAT submatrix type", NULL, sub_mat_type, sub_mat_type, PETSC_MAX_PATH_LEN, &is_cl_type_set));
          if (!is_cl_type_set) {
            const char **active_field_names;

            PetscCall(RatelMaterialGetActiveFieldNames(ratel->materials[0], &active_field_names, NULL));
            PetscCall(PetscSNPrintf(option_name, PETSC_MAX_PATH_LEN, "-ceed_%s_mat_type", active_field_names[f]));
            PetscCall(
                PetscOptionsString(option_name, "CEEDMAT submatrix type", NULL, sub_mat_type, sub_mat_type, PETSC_MAX_PATH_LEN, &is_cl_type_set));
          }
          PetscOptionsEnd();

          if (is_cl_type_set && !strstr(sub_mat_type, MATCEED) && !strstr(sub_mat_type, MATSHELL)) {
            const CeedInt *active_field_sizes;

            PetscCall(DMSetMatType(sub_dms[f], sub_mat_type));
            PetscCall(DMCreateMatrix(sub_dms[f], A_sub));
            PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[0], NULL, &active_field_sizes));
            if (active_field_sizes[f] >= 3) {
              MatNullSpace near_null;

              PetscCall(DMPlexCreateRigidBody(sub_dms[f], f, &near_null));
              PetscCall(MatSetNearNullSpace(*A_sub, near_null));
              PetscCall(MatNullSpaceDestroy(&near_null));
            }
          } else {
            *A_sub = A_sub_ceed;
            PetscCall(PetscObjectReference((PetscObject)A_sub_ceed));
          }
        }
        // ---- Setup
        if (A_sub_ceed != *A_sub) {
          MatType mat_type;

          PetscCall(MatGetType(*A_sub, &mat_type));
          if (!strstr(mat_type, MATCEED)) {
            PetscCall(MatCeedAssembleCOO(A_sub_ceed, *A_sub));
          } else {
            PetscCall(MatDestroy(A_sub));
            *A_sub = A_sub_ceed;
            PetscCall(PetscObjectReference((PetscObject)A_sub_ceed));
          }
        }
      }
    }
    // Cleanup
    PetscCall(DMDestroy(&dm_solution));
    for (PetscInt f = 0; f < ratel->num_active_fields; f++) {
      PetscCall(ISDestroy(&field_is[f]));
      PetscCall(DMDestroy(&sub_dms[f]));
    }
    PetscCall(PetscFree(field_is));
    PetscCall(PetscFree(sub_dms));
  }
  // Mark as reassembly required - submats only re-requested once per SNES iteration
  PetscCall(MatAssemblyBegin(*A_sub, MAT_FINAL_ASSEMBLY));
  PetscCall(MatAssemblyEnd(*A_sub, MAT_FINAL_ASSEMBLY));
  PetscFunctionReturn(PETSC_SUCCESS);
}

// -----------------------------------------------------------------------------
// Solver callbacks
// -----------------------------------------------------------------------------

/**
  @brief Set Ratel `Mat`s for a `SNES`.

  Collective across MPI processes.

  @param[in]      ratel  `Ratel` context
  @param[in,out]  snes   Non-linear solver

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelSNESSetJacobianMats(Ratel ratel, SNES snes) {
  MatType mat_type_J = MATSHELL, mat_type_J_pre = MATSHELL;

  PetscFunctionBeginUser;
  if (ratel->are_snes_mats_set) PetscFunctionReturn(PETSC_SUCCESS);

  {
    Mat J, J_pre;

    PetscCall(SNESGetJacobian(snes, &J, &J_pre, NULL, NULL));
    if (J) PetscCall(MatGetType(J, &mat_type_J));
    if (J_pre) PetscCall(MatGetType(J_pre, &mat_type_J_pre));
  }
  {
    Mat J = NULL, J_pre = NULL;

    if (!strcmp(mat_type_J, MATSHELL) || !strcmp(mat_type_J, MATCEED)) J = ratel->mat_jacobian;
    if (!strcmp(mat_type_J_pre, MATSHELL) || !strcmp(mat_type_J_pre, MATCEED)) J_pre = ratel->mat_jacobian_pre;
    PetscCall(SNESSetJacobian(snes, J, J_pre, NULL, NULL));
  }
  ratel->are_snes_mats_set = PETSC_TRUE;
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Compute the non-linear residual for a `SNES`.

  Collective across MPI processes.

  @param[in]   snes  Non-linear solver
  @param[in]   X     Current state vector
  @param[out]  Y     Residual vector
  @param[in]   ctx   `Ratel` context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSNESFormResidual(SNES snes, Vec X, Vec Y, void *ctx) {
  Ratel ratel = (Ratel)ctx;

  PetscFunctionBeginUser;
  // Update time context and Dirichlet values
  PetscCall(CeedEvaluatorUpdateTimeDtAndBoundaryValues(ratel->evaluator_residual_u, 1.0, 1.0));

  // libCEED for local action of residual evaluator
  PetscCall(CeedEvaluatorApplyGlobalToGlobal(ratel->evaluator_residual_u, X, Y));

  // Set J, J_pre if needed
  if (!ratel->are_snes_mats_set) PetscCall(RatelSNESSetJacobianMats(ratel, snes));

  // Mark QFunction as requiring re-assembly
  PetscCall(MatCeedSetAssemblyDataUpdateNeeded(ratel->mat_jacobian, PETSC_TRUE));
  PetscCall(MatCeedSetAssemblyDataUpdateNeeded(ratel->mat_jacobian_pre, PETSC_TRUE));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Assemble `SNES` Jacobian.

  Collective across MPI processes.

  @param[in]      snes   Non-linear solver
  @param[in]      X      Current non-linear residual
  @param[out]     J      Jacobian operator
  @param[out]     J_pre  Jacobian operator for preconditioning
  @param[in,out]  ctx    Ratel context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSNESFormJacobian(SNES snes, Vec X, Mat J, Mat J_pre, void *ctx) {
  MatType mat_type;
  Ratel   ratel = (Ratel)ctx;

  PetscFunctionBeginUser;
  // Set context for fine level
  if (J_pre != ratel->mat_jacobian_pre) {
    PetscCall(MatGetType(J_pre, &mat_type));
    if (!strcmp(mat_type, MATSHELL)) {
      PetscCall(MatCeedCopy(ratel->mat_jacobian_pre, J_pre));
    } else if (strcmp(mat_type, MATCEED)) {
      PetscCall(MatCeedAssembleCOO(ratel->mat_jacobian_pre, J_pre));
    }
  }
  if (J != ratel->mat_jacobian) {
    PetscCall(MatGetType(J, &mat_type));
    if (!strcmp(mat_type, MATSHELL)) {
      PetscCall(MatCeedCopy(ratel->mat_jacobian, J));
    } else if (strcmp(mat_type, MATCEED) && J != J_pre) {
      PetscCall(MatCeedAssembleCOO(ratel->mat_jacobian, J));
    }
  }

  // Set assembly complete
  PetscCall(MatAssemblyBegin(J, MAT_FINAL_ASSEMBLY));
  PetscCall(MatAssemblyEnd(J, MAT_FINAL_ASSEMBLY));
  PetscCall(MatSetOption(J, MAT_SPD, ratel->is_spd));
  if (J != J_pre) {
    PetscCall(MatAssemblyBegin(J_pre, MAT_FINAL_ASSEMBLY));
    PetscCall(MatAssemblyEnd(J_pre, MAT_FINAL_ASSEMBLY));
    PetscCall(MatSetOption(J_pre, MAT_SPD, ratel->is_spd));
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Compute the non-linear residual for a `TS`.

  Collective across MPI processes.

  @param[in]      ts    Time-stepper
  @param[in]      time  Current time
  @param[in]      X     Current state vector
  @param[in]      X_t   Time derivative of current state vector
  @param[out]     Y     Function vector
  @param[in,out]  ctx   `Ratel` context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSFormIResidual(TS ts, PetscReal time, Vec X, Vec X_t, Vec Y, void *ctx) {
  Ratel     ratel = (Ratel)ctx;
  PetscReal dt;

  PetscFunctionBeginUser;
  // Update time context and Dirichlet values
  PetscCall(TSGetTimeStep(ts, &dt));
  PetscCall(CeedEvaluatorUpdateTimeDtAndBoundaryValues(ratel->evaluator_residual_u, time, dt));

  // libCEED for local action of residual evaluator
  if (ratel->has_mixed_u_ut_term) PetscCall(CeedEvaluatorApplyVelocityGlobalToGlobal(ratel->evaluator_residual_u, X, X_t, Y));
  else PetscCall(CeedEvaluatorApplyGlobalToGlobal(ratel->evaluator_residual_u, X, Y));

  // Add contribution of X_t term, if required
  if (ratel->has_ut_term) {
    PetscCall(CeedEvaluatorSetTime(ratel->evaluator_residual_ut, time));
    PetscCall(CeedEvaluatorSetDt(ratel->evaluator_residual_ut, dt));
    PetscCall(CeedEvaluatorApplyAddGlobalToGlobal(ratel->evaluator_residual_ut, X_t, Y));
  }

  // Set J, J_pre if needed
  if (!ratel->are_snes_mats_set) {
    SNES snes;

    PetscCall(TSGetSNES(ts, &snes));
    PetscCall(RatelSNESSetJacobianMats(ratel, snes));
  }

  // Mark QFunction as requiring re-assembly
  PetscCall(MatCeedSetAssemblyDataUpdateNeeded(ratel->mat_jacobian, PETSC_TRUE));
  PetscCall(MatCeedSetAssemblyDataUpdateNeeded(ratel->mat_jacobian_pre, PETSC_TRUE));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Assemble `TS` Jacobian.

  Collective across MPI processes.

  @param[in]      ts     Time-stepper
  @param[in]      time   Current time
  @param[in]      X      Current non-linear residual
  @param[in]      X_t    Time derivative of current non-linear residual
  @param[in]      v      Shift
  @param[out]     J      Jacobian operator
  @param[out]     J_pre  Jacobian operator for preconditioning
  @param[in,out]  ctx    Ratel context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSFormIJacobian(TS ts, PetscReal time, Vec X, Vec X_t, PetscReal v, Mat J, Mat J_pre, void *ctx) {
  Ratel ratel = (Ratel)ctx;

  PetscFunctionBeginUser;
  // Update time and shift
  PetscCall(MatCeedSetTime(ratel->mat_jacobian, time));
  PetscCall(MatCeedSetShifts(ratel->mat_jacobian, v, 0.0));
  PetscCall(MatCeedSetTime(ratel->mat_jacobian_pre, time));
  PetscCall(MatCeedSetShifts(ratel->mat_jacobian_pre, v, 0.0));
  {
    PetscReal dt;

    PetscCall(TSGetTimeStep(ts, &dt));
    PetscCall(MatCeedSetDt(ratel->mat_jacobian, dt));
    PetscCall(MatCeedSetDt(ratel->mat_jacobian_pre, dt));
  }
  // Form Jacobian
  {
    SNES snes;

    PetscCall(TSGetSNES(ts, &snes));
    PetscCall(RatelSNESFormJacobian(snes, X, J, J_pre, ratel));
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Compute the non-linear residual for a `TS`.

  Collective across MPI processes.

  @param[in]      ts    Time-stepper
  @param[in]      time  Current time
  @param[in]      X     Current state vector
  @param[in]      X_t   Time derivative of current state vector
  @param[in]      X_tt  Second time derivative of current state vector
  @param[out]     Y     Function vector
  @param[in,out]  ctx   User context struct containing `CeedOperator` data

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSFormI2Residual(TS ts, PetscReal time, Vec X, Vec X_t, Vec X_tt, Vec Y, void *ctx) {
  Ratel     ratel = (Ratel)ctx;
  PetscReal dt;

  PetscFunctionBeginUser;
  // Update time context and Dirichlet values
  PetscCall(TSGetTimeStep(ts, &dt));
  PetscCall(CeedEvaluatorUpdateTimeDtAndBoundaryValues(ratel->evaluator_residual_u, time, dt));

  // Apply the portion that only depends upon X, X_t
  PetscCall(RatelTSFormIResidual(ts, time, X, X_t, Y, ratel));

  // libCEED for local action of scaled mass matrix
  PetscCall(CeedEvaluatorApplyAddGlobalToGlobal(ratel->evaluator_residual_utt, X_tt, Y));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Assemble `TS` Jacobian.

  Collective across MPI processes.

  @param[in]      ts     Time-stepper
  @param[in]      time   Current time
  @param[in]      X      Current non-linear residual
  @param[in]      X_t    Time derivative of current non-linear residual
  @param[in]      X_tt   Second time derivative of current non-linear residual
  @param[in]      v      Shift for X_t
  @param[in]      a      Shift for X_tt
  @param[out]     J      Jacobian operator
  @param[out]     J_pre  Jacobian operator for preconditioning
  @param[in,out]  ctx    Ratel context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSFormI2Jacobian(TS ts, PetscReal time, Vec X, Vec X_t, Vec X_tt, PetscReal v, PetscReal a, Mat J, Mat J_pre, void *ctx) {
  Ratel ratel = (Ratel)ctx;

  PetscFunctionBeginUser;
  // Update shift contexts
  PetscCall(MatCeedSetTime(ratel->mat_jacobian, time));
  PetscCall(MatCeedSetShifts(ratel->mat_jacobian, v, a));
  PetscCall(MatCeedSetTime(ratel->mat_jacobian_pre, time));
  PetscCall(MatCeedSetShifts(ratel->mat_jacobian_pre, v, a));
  {
    PetscReal dt;

    PetscCall(TSGetTimeStep(ts, &dt));
    PetscCall(MatCeedSetDt(ratel->mat_jacobian, dt));
    PetscCall(MatCeedSetDt(ratel->mat_jacobian_pre, dt));
  }
  // Form Jacobian
  {
    SNES snes;

    PetscCall(TSGetSNES(ts, &snes));
    PetscCall(RatelSNESFormJacobian(snes, X, J, J_pre, ratel));
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Migrate from `DMSwarm` to `DMPlex` before `TS` step, if needed.

  Collective across MPI processes.

  @param[in]  ts  Time-stepper

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSPreStep(TS ts) {
  Ratel ratel;
  DM    dm;

  PetscFunctionBeginUser;
  PetscCall(TSGetDM(ts, &dm));
  PetscCall(DMGetApplicationContext(dm, &ratel));

  if (ratel->method_type == RATEL_METHOD_MPM) {
    Vec            U;
    DM             dm_mesh;
    PetscInt       stride, dim;
    const CeedInt *active_field_sizes;
    CeedInt        num_active_fields;

    PetscCall(RatelGetSolutionMeshDM(ratel, &dm_mesh));
    PetscCall(DMGetDimension(dm_mesh, &dim));

    // Save the value of ts->steprestart, as it will be set to true by TSPreStep since we change the solution vector
    ratel->is_ts_steprestart = ts->steprestart;
    PetscCall(RatelDebug(ratel, "Cached ts->steprestart: %d", ratel->is_ts_steprestart));

    PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[0], &num_active_fields, &active_field_sizes));

    // Clear mesh displacement solution before next stage
    PetscCall(TSGetSolution(ts, &U));

    // Special logic for 4 component damage models -- should copy damage from last timestep
    if (active_field_sizes[0] > dim && num_active_fields == 1) {
      Vec U_loc;

      PetscCall(DMGetLocalVector(dm_mesh, &U_loc));
      PetscCall(DMGlobalToLocal(dm_mesh, U, INSERT_VALUES, U_loc));
      PetscCall(VecGetBlockSize(U_loc, &stride));
      PetscCheck(stride == active_field_sizes[0], ratel->comm, PETSC_ERR_ARG_INCOMP,
                 "Stride %" PetscInt_FMT " must match active field size %" CeedInt_FMT, stride, active_field_sizes[0]);

      // Only zero-out the first three components
      for (PetscInt d = 0; d < dim; d++) {
        PetscCall(VecStrideScale(U_loc, d, 0.0));
      }
      PetscCall(VecZeroEntries(U));
      PetscCall(DMLocalToGlobal(dm_mesh, U_loc, ADD_VALUES, U));
      PetscCall(DMRestoreLocalVector(dm_mesh, &U_loc));
    } else {
      PetscCall(VecZeroEntries(U));
    }

    // Set boundary conditions
    PetscCall(DMPlexSetCeedBoundaryEvaluator(dm_mesh, ratel->boundary_evaluator_u));

    // Cleanup
    PetscCall(DMDestroy(&dm_mesh));
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Callback which runs before each stage of a time-stepper. Caches the value of ts->steprestart.

  Collective across MPI processes.

  @param[in]  ts          `TS` object
  @param[in]  stage_time  Current stage time (unused)

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSPreStage(TS ts, PetscScalar stage_time) {
  Ratel ratel;
  DM    dm;

  PetscFunctionBeginUser;
  PetscCall(TSGetDM(ts, &dm));
  PetscCall(DMGetApplicationContext(dm, &ratel));
  if (ratel->method_type == RATEL_METHOD_MPM) {
    // We cached the value of ts->steprestart in RatelTSPreStep
    // then changed the state of the solution vector.
    // Since TSPreStage runs after TSPreStep, we restore the original value of
    // ts->steprestart to prevent the step from being restarted.
    // TODO: Try to get a flag into TS to allow changes to the solution vector in TSPreStage
    ts->steprestart = ratel->is_ts_steprestart;
    PetscCall(RatelDebug(ratel, "Restored ts->steprestart: %d", ts->steprestart));
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief `TS` post-evaluate routine to accept state values and migrate MPM points, if needed.

  Collective across MPI processes.

  @param[in]  ts  Time-stepper

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSPostEvaluate(TS ts) {
  Ratel       ratel;
  DM          dm;
  PetscScalar time, dt;
  Vec         X;

  PetscFunctionBeginUser;
  PetscCall(TSGetDM(ts, &dm));
  PetscCall(DMGetApplicationContext(dm, &ratel));
  PetscCall(TSGetTime(ts, &time));
  PetscCall(TSGetTimeStep(ts, &dt));

  // Get current solution
  PetscCall(TSGetSolution(ts, &X));

  if (ratel->method_type == RATEL_METHOD_MPM) {
    // Store value of ts->steprestart, as it will be set to true by TSPostEvaluate since we change the solution vector
    ratel->is_ts_steprestart = ts->steprestart;
    PetscCall(RatelDebug(ratel, "Cached ts->steprestart: %d", ratel->is_ts_steprestart));
  }

  // Compute final state values
  {
    CeedVector x_dot_loc;
    DM         dm_y;
    Vec        Y_loc;

    PetscCall(CeedEvaluatorGetLocalCeedVectors(ratel->evaluator_residual_u, NULL, &x_dot_loc, NULL));
    RatelCallCeed(ratel, CeedVectorSetValue(x_dot_loc, 0.0));
    PetscCall(CeedEvaluatorRestoreLocalCeedVectors(ratel->evaluator_residual_u, NULL, &x_dot_loc, NULL));
    PetscCall(CeedEvaluatorUpdateTimeDtAndBoundaryValues(ratel->evaluator_residual_u, time, dt));
    PetscCall(CeedEvaluatorGetDMs(ratel->evaluator_residual_u, NULL, &dm_y));
    PetscCall(DMGetLocalVector(dm_y, &Y_loc));
    PetscCall(CeedEvaluatorApplyGlobalToLocal(ratel->evaluator_residual_u, X, Y_loc));
    PetscCall(DMRestoreLocalVector(dm_y, &Y_loc));
    PetscCall(CeedEvaluatorRestoreDMs(ratel->evaluator_residual_u, NULL, &dm_y));
  }

  // Accept state values
  {
    CeedOperator op_residual_u;

    PetscCall(CeedEvaluatorGetCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
    for (CeedInt i = 0; i < ratel->num_materials; i++) PetscCall(RatelMaterialAcceptState(ratel->materials[i], op_residual_u));
    PetscCall(CeedEvaluatorRestoreCeedOperator(ratel->evaluator_residual_u, &op_residual_u));
  }

  // Interpolate solution to particles and migrate
  if (ratel->method_type == RATEL_METHOD_MPM) {
    DM dm_mesh;

    // Map from particles to mesh for diagnostics
    // We clear the mesh solution again in RatelTSPreStep
    PetscCall(RatelMPMMigrate(ratel, X, time));
    PetscCall(RatelMPMSwarmToMesh(ratel, X));

    PetscCall(RatelGetSolutionMeshDM(ratel, &dm_mesh));
    PetscCall(DMPlexSetCeedBoundaryEvaluator(dm_mesh, ratel->boundary_evaluator_visualization));
    PetscCall(CeedEvaluatorUpdateTimeDtAndBoundaryValues(ratel->evaluator_residual_u, time, dt));
    PetscCall(DMDestroy(&dm_mesh));
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Callback which runs after each step of a time-stepper. Restores the cached value of ts->steprestart.

  Collective across MPI processes.

  @param[in]  ts  `TS` object

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelTSPostStep(TS ts) {
  Ratel ratel;
  DM    dm;

  PetscFunctionBeginUser;
  PetscCall(TSGetDM(ts, &dm));
  PetscCall(DMGetApplicationContext(dm, &ratel));
  if (ratel->method_type == RATEL_METHOD_MPM) {
    // We cached the value of ts->steprestart in RatelTSPostEvaluate
    // then changed the state of the solution vector.
    // Since TSPostStep runs after TSPostEvaluate, we restore the original value of
    // ts->steprestart to prevent the step from being restarted.
    // TODO: Try to get a flag into TS to allow changes to the solution vector in TSPostEvaluate
    ts->steprestart = ratel->is_ts_steprestart;
    PetscCall(RatelDebug(ratel, "Restored ts->steprestart: %d", ts->steprestart));
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Compute SNES objective.

  Collective across MPI processes.

  @param[in]   snes   Non-linear solver
  @param[in]   X      Current state vector
  @param[out]  merit  SNES objective
  @param[in]   ctx    `Ratel` context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSNESFormObjective(SNES snes, Vec X, PetscReal *merit, void *ctx) {
  Ratel       ratel        = (Ratel)ctx;
  PetscScalar local_energy = 0., global_energy = 0.;
  Vec         Y_loc;
  DM          dm_y;
  PetscReal   time;

  PetscFunctionBeginUser;
  // Setup context if needed
  if (!ratel->evaluator_strain_energy) PetscCall(RatelSetupStrainEnergyEvaluator(ratel, &ratel->evaluator_strain_energy));
  if (!ratel->evaluator_external_energy) PetscCall(RatelSetupExternalEnergyEvaluator(ratel, &ratel->evaluator_external_energy));

  // Get data
  PetscCall(CeedEvaluatorGetTime(ratel->evaluator_residual_u, &time));
  PetscCall(CeedEvaluatorGetDMs(ratel->evaluator_strain_energy, NULL, &dm_y));
  PetscCall(DMGetLocalVector(dm_y, &Y_loc));

  // Compute total energy
  // -- Compute strain energy part
  PetscCall(CeedEvaluatorSetTime(ratel->evaluator_strain_energy, time));
  PetscCall(CeedEvaluatorApplyGlobalToLocal(ratel->evaluator_strain_energy, X, Y_loc));
  // -- Compute external energy part
  PetscCall(CeedEvaluatorSetTime(ratel->evaluator_external_energy, time));
  {
    Vec X_loc;

    PetscCall(CeedEvaluatorGetLocalVectors(ratel->evaluator_strain_energy, &X_loc, NULL, NULL));
    PetscCall(CeedEvaluatorApplyAddLocalToLocal(ratel->evaluator_external_energy, X_loc, Y_loc));
    PetscCall(CeedEvaluatorRestoreLocalVectors(ratel->evaluator_strain_energy, &X_loc, NULL, NULL));
  }

  // Reduce energy
  PetscCall(VecSum(Y_loc, &local_energy));
  PetscCall(MPIU_Allreduce(&local_energy, &global_energy, 1, MPIU_REAL, MPI_SUM, ratel->comm));
  PetscCall(RatelDebug(ratel, "---- total energy: %f", global_energy));

  // Cleanup
  PetscCall(DMRestoreLocalVector(dm_y, &Y_loc));
  PetscCall(CeedEvaluatorRestoreDMs(ratel->evaluator_strain_energy, NULL, &dm_y));
  *merit = global_energy;
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}
