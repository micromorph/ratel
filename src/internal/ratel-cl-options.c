/// @file
/// Command line option processing for solid mechanics example using PETSc

#include <ceed.h>
#include <math.h>
#include <petscdevice.h>
#include <ratel-boundary.h>
#include <ratel-cl-options.h>
#include <ratel-impl.h>
#include <ratel-material.h>
#include <ratel.h>
#include <ratel/models/bounding-box.h>
#include <ratel/models/common-parameters.h>
#include <stdio.h>
#include <string.h>

/// @addtogroup RatelInternal
/// @{

/**
  @brief Process general command line options.

  Collective across MPI processes.

  @param[in,out]  ratel  `Ratel` context object

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelProcessCommandLineOptions(Ratel ratel) {
  char     *material_names[RATEL_MAX_MATERIALS];
  PetscBool bc_allow_no_clamp = PETSC_FALSE;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Process Command Line Options"));

  // Check options set
  {
    PetscInt num_options;

    PetscCall(PetscOptionsLeftGet(NULL, &num_options, NULL, NULL));
    PetscCheck(num_options > 0, ratel->comm, PETSC_ERR_USER_INPUT,
               "Command line options required for Ratel."
               " Please consult the documentation to see which options are required.");
    PetscCall(PetscOptionsLeftRestore(NULL, &num_options, NULL, NULL));
  }

  PetscOptionsBegin(ratel->comm, NULL, "Ratel library with PETSc and libCEED", NULL);

  // Numerical method
  {
    ratel->method_type = RATEL_METHOD_FEM;
    PetscCall(PetscOptionsEnum("-method", "Numerical method", NULL, RatelMethodTypesCL, (PetscEnum)ratel->method_type,
                               (PetscEnum *)&ratel->method_type, NULL));
    PetscCall(RatelDebug(ratel, "---- Numerical method: %s", RatelMethodTypes[ratel->method_type]));
  }

  // libCEED resource
  {
    PetscBool ceed_set    = PETSC_FALSE;
    PetscInt  max_strings = 1;

    PetscCall(PetscOptionsStringArray("-ceed", "CEED resource specifier", NULL, &ratel->ceed_resource, &max_strings, &ceed_set));
    PetscCall(RatelDebug(ratel, "---- Ceed resource requested by user: %s", ceed_set ? ratel->ceed_resource : "none"));

    // -- Provide default libCEED resource if not specified
    if (!ceed_set) {
      const char *default_ceed_resource     = "/cpu/self";
      PetscSizeT  default_ceed_resource_len = 0;

      PetscCall(PetscStrlen(default_ceed_resource, &default_ceed_resource_len));
      PetscCall(PetscCalloc1(default_ceed_resource_len + 1, &ratel->ceed_resource));
      PetscCall(PetscStrncpy(ratel->ceed_resource, default_ceed_resource, default_ceed_resource_len + 1));
    }
    PetscCall(RatelDebug(ratel, "---- Ceed resource to initialize: %s", ratel->ceed_resource));

    // Initialize GPU
    // Note: We make sure PETSc has initialized its device (which is MPI-aware in case multiple devices are available) before CeedInit.
    //       This ensures that PETSc and libCEED agree about which device to use.
    if (strncmp(ratel->ceed_resource, "/gpu", 4) == 0) PetscCall(PetscDeviceInitialize(PETSC_DEVICE_DEFAULT()));

    // Initialize Ceed
    {
      const char *ceed_resource;

      // -- Create Ceed
      PetscCheck(CeedInit(ratel->ceed_resource, &ratel->ceed) == CEED_ERROR_SUCCESS, ratel->comm, PETSC_ERR_LIB,
                 "libCEED resource initialization failed");

      // -- Set error handler
      PetscCheck(CeedSetErrorHandler(ratel->ceed, CeedErrorStore) == CEED_ERROR_SUCCESS, ratel->comm, PETSC_ERR_LIB,
                 "Setting libCEED error handler failed");

      // -- Set default JiT source root
      PetscCall(RatelDebug(ratel, "---- Adding Ratel JiT root: %s", RatelJitSourceRootDefault));
      RatelCallCeed(ratel, CeedAddJitSourceRoot(ratel->ceed, (char *)RatelJitSourceRootDefault));

      // -- View ceed
      RatelCallCeed(ratel, CeedGetResource(ratel->ceed, &ceed_resource));
      PetscCall(RatelDebug(ratel, "---- libCEED resource initialized: %s", ceed_resource));
    }
  }

  // Fine grid solution order
  {
    PetscBool found_orders_option = PETSC_FALSE;

    ratel->fine_grid_orders[0] = 3;
    ratel->num_active_fields   = RATEL_MAX_FIELDS;
    PetscCall(PetscOptionsIntArray("-orders", "Polynomial orders of basis functions", NULL, ratel->fine_grid_orders, &ratel->num_active_fields,
                                   &found_orders_option));
    if (!found_orders_option) {
      ratel->num_active_fields = 1;
      PetscCall(PetscOptionsIntArray("-order", "Polynomial orders of basis functions", NULL, ratel->fine_grid_orders, &ratel->num_active_fields,
                                     &found_orders_option));
    }
    if (ratel->num_active_fields == 0) ratel->num_active_fields = 1;
    PetscCall(RatelDebug(ratel, "---- Active fields found: %" PetscInt_FMT, ratel->num_active_fields));
    for (PetscInt i = 0; i < ratel->num_active_fields; i++) {
      ratel->highest_fine_order = PetscMax(ratel->highest_fine_order, ratel->fine_grid_orders[i]);
      PetscCheck(ratel->fine_grid_orders[i] >= 0, ratel->comm, PETSC_ERR_SUP, "Solution order must be a non-negative value: %" PetscInt_FMT,
                 ratel->fine_grid_orders[i]);
      PetscCall(RatelDebug(ratel, "---- Fine grid polynomial order field %" PetscInt_FMT ": %" PetscInt_FMT, i, ratel->fine_grid_orders[i]));
    }
  }

  // Diagnostic quantities order
  ratel->diagnostic_order = ratel->highest_fine_order;
  PetscCall(
      PetscOptionsInt("-diagnostic_order", "Polynomial order of diagnostic output", NULL, ratel->diagnostic_order, &ratel->diagnostic_order, NULL));
  PetscCheck(ratel->diagnostic_order > 0, ratel->comm, PETSC_ERR_SUP, "Diagnostic order must be a positive value: %" PetscInt_FMT,
             ratel->diagnostic_order);
  PetscCall(RatelDebug(ratel, "---- Diagnostic value order: %" PetscInt_FMT, ratel->diagnostic_order));

  // Geometry order for viewer
  ratel->diagnostic_geometry_order = RATEL_DECIDE;
  PetscCall(PetscOptionsInt("-diagnostic_geometry_order", "Geometry order for diagnostic values mesh", NULL, ratel->diagnostic_geometry_order,
                            &ratel->diagnostic_geometry_order, NULL));
  {
    PetscBool is_diagnostic_geometry_ratel_decide = ratel->diagnostic_geometry_order == RATEL_DECIDE;

    PetscCheck(is_diagnostic_geometry_ratel_decide || ratel->diagnostic_geometry_order >= 1, ratel->comm, PETSC_ERR_SUP,
               "Geometry order must be a positive value: %" PetscInt_FMT, ratel->diagnostic_geometry_order);
    // LCOV_EXCL_START
    if (is_diagnostic_geometry_ratel_decide) {
      PetscCall(RatelDebug(ratel, "---- Geometry order for diagnostic values mesh: use solution mesh geometric order"));
    } else {
      PetscCall(RatelDebug(ratel, "---- Geometry order for diagnostic values mesh: %" PetscInt_FMT, ratel->diagnostic_geometry_order));
    }
    // LCOV_EXCL_STOP
  }

  // Additional quadrature points
  ratel->q_extra = 0;
  PetscCall(PetscOptionsInt("-q_extra", "Number of extra quadrature points", NULL, ratel->q_extra, &ratel->q_extra, NULL));
  PetscCall(RatelDebug(ratel, "---- Extra quadrature points: %" PetscInt_FMT, ratel->q_extra));

  // Additional quadrature points for surface evaluation
  ratel->q_extra_surface_force = ratel->q_extra > 0 ? ratel->q_extra : 1;
  PetscCall(PetscOptionsInt("-q_extra_surface_force", "Number of extra quadrature points for surface force computation", NULL,
                            ratel->q_extra_surface_force, &ratel->q_extra_surface_force, NULL));
  PetscCall(RatelDebug(ratel, "---- Extra quadrature points for surface force computation: %" PetscInt_FMT, ratel->q_extra_surface_force));

  // Use objective function
  ratel->use_objective = PETSC_FALSE;
  PetscCall(PetscOptionsBool("-use_objective", "Use objective function", NULL, ratel->use_objective, &ratel->use_objective, NULL));

  // Physical scaling options
  {
    RatelUnits units;

    PetscCall(PetscNew(&units));
    units->meter = 1.0;
    PetscCall(PetscOptionsScalar("-units_meter", "1 meter in scaled length units", NULL, units->meter, &units->meter, NULL));
    units->meter = fabs(units->meter);
    PetscCall(RatelDebug(ratel, "---- units meter: %f", units->meter));

    units->second = 1.0;
    PetscCall(PetscOptionsScalar("-units_second", "1 second in scaled time units", NULL, units->second, &units->second, NULL));
    units->second = fabs(units->second);
    PetscCall(RatelDebug(ratel, "---- units second: %f", units->second));

    units->kilogram = 1.0;
    PetscCall(PetscOptionsScalar("-units_kilogram", "1 kilogram in scaled mass units", NULL, units->kilogram, &units->kilogram, NULL));
    units->kilogram = fabs(units->kilogram);
    PetscCall(RatelDebug(ratel, "---- units kilogram: %f", units->kilogram));

    // Define derived units
    units->Pascal              = units->kilogram / (units->meter * PetscSqr(units->second));
    units->JoulePerSquareMeter = units->kilogram * PetscSqr(units->meter) / PetscSqr(units->second);

    ratel->material_units = units;
  }

  // Material regions
  {
    PetscBool found_material_option = PETSC_FALSE;

    // -- Get number and names of materials
    ratel->num_materials = RATEL_MAX_MATERIALS;
    PetscCall(PetscOptionsStringArray("-material", "List of user-defined material names", __func__, material_names, &ratel->num_materials,
                                      &found_material_option));
    if (!found_material_option) {
      ratel->num_materials = 1;
      PetscCall(PetscNew(&material_names[0]));
    }
    PetscCall(PetscCalloc1(ratel->num_materials, &ratel->materials));
  }

  // Create materials
  PetscOptionsEnd();  // Pause Ratel options temporarily for easier `-help` output
  for (PetscInt i = 0; i < ratel->num_materials; i++) {
    PetscCall(RatelMaterialCreate(ratel, material_names[i], &ratel->materials[i]));
    PetscCall(PetscFree(material_names[i]));
  }
  PetscOptionsBegin(ratel->comm, NULL, "Ratel library with PETSc and libCEED", NULL);  // Resume Ratel options

  PetscCall(PetscOptionsBool("-bc_allow_no_clamp", "Allow no clamp BCs to be set (typically makes problem singular)", NULL, bc_allow_no_clamp,
                             &bc_allow_no_clamp, NULL));
  // Dirichlet boundary conditions
  for (PetscInt i = 0; i < ratel->num_active_fields; i++) {
    char      option_name[PETSC_MAX_OPTION_NAME];
    PetscBool option_set = PETSC_FALSE;

    ratel->bc_clamp_count[i] = RATEL_MAX_BOUNDARY_FACES;
    PetscCall(PetscSNPrintf(option_name, sizeof(option_name), "-bc_clamp_field_%" PetscInt_FMT, i));
    PetscCall(PetscOptionsIntArray(option_name, "Face IDs to apply incremental Dirichlet BC", NULL, ratel->bc_clamp_faces[i],
                                   &ratel->bc_clamp_count[i], &option_set));
    if (!option_set) {
      const char **active_field_names;

      PetscCall(RatelMaterialGetActiveFieldNames(ratel->materials[0], &active_field_names, NULL));
      PetscCall(PetscSNPrintf(option_name, sizeof(option_name), "-bc_clamp_%s", active_field_names[i]));
      ratel->bc_clamp_count[i] = RATEL_MAX_BOUNDARY_FACES;
      PetscCall(PetscOptionsIntArray(option_name, "Face IDs to apply incremental Dirichlet BC", NULL, ratel->bc_clamp_faces[i],
                                     &ratel->bc_clamp_count[i], &option_set));
    }
    if (i == 0 && !option_set) {
      ratel->bc_clamp_count[i] = RATEL_MAX_BOUNDARY_FACES;
      PetscCall(PetscOptionsIntArray("-bc_clamp", "Face IDs to apply incremental Dirichlet BC", NULL, ratel->bc_clamp_faces[i],
                                     &ratel->bc_clamp_count[i], &option_set));
    }
  }

  // MMS boundary conditions
  for (PetscInt i = 0; i < ratel->num_active_fields; i++) {
    char option_name[PETSC_MAX_OPTION_NAME];

    ratel->bc_mms_count[i] = RATEL_MAX_BOUNDARY_FACES;
    PetscCall(PetscSNPrintf(option_name, sizeof(option_name), "-bc_mms_field_%" PetscInt_FMT, i));
    PetscCall(PetscOptionsIntArray(option_name, "Face IDs to apply MMS Dirichlet BC", NULL, ratel->bc_mms_faces[i], &ratel->bc_mms_count[i], NULL));
    if (ratel->bc_mms_count[i] == 0) {
      const char **active_field_names;

      PetscCall(RatelMaterialGetActiveFieldNames(ratel->materials[0], &active_field_names, NULL));
      PetscCall(PetscSNPrintf(option_name, sizeof(option_name), "-bc_mms_%s", active_field_names[i]));
      ratel->bc_mms_count[i] = RATEL_MAX_BOUNDARY_FACES;
      PetscCall(PetscOptionsIntArray(option_name, "Face IDs to apply MMS Dirichlet BC", NULL, ratel->bc_mms_faces[i], &ratel->bc_mms_count[i], NULL));
    }
    if (i == 0 && ratel->bc_mms_count[i] == 0) {
      ratel->bc_mms_count[i] = RATEL_MAX_BOUNDARY_FACES;
      PetscCall(PetscOptionsIntArray("-bc_mms", "Face IDs to apply MMS Dirichlet BC", NULL, ratel->bc_mms_faces[i], &ratel->bc_mms_count[i], NULL));
    }

    PetscCheck(ratel->bc_mms_count[i] <= 0 || ratel->materials[0]->forcing_type != RATEL_FORCING_MMS || ratel->num_materials <= 1, ratel->comm,
               PETSC_ERR_SUP, "Cannot verify the manufactured solution with more than one material");
  }
  {
    RatelForcingType forcing_type;
    PetscInt         bc_mms_count = 0;

    PetscCall(RatelMaterialGetForcingType(ratel->materials[0], &forcing_type));
    for (PetscInt i = 0; i < ratel->num_active_fields; i++) bc_mms_count += ratel->bc_mms_count[i];
    if ((bc_mms_count > 0 || forcing_type == RATEL_FORCING_MMS) && (bc_mms_count == 0 || forcing_type != RATEL_FORCING_MMS)) {
      // LCOV_EXCL_START
      PetscCall(RatelPrintf256(ratel, RATEL_DEBUG_COLOR_WARNING, "WARNING: '-bc_mms' and '-forcing mms' typically should be used together"));
      // LCOV_EXCL_STOP
    }
  }

  // Slip boundary conditions
  for (PetscInt i = 0; i < ratel->num_active_fields; i++) {
    const char   **active_field_names;
    const CeedInt *active_field_sizes;
    char           option_name[PETSC_MAX_OPTION_NAME];
    PetscBool      option_set = PETSC_FALSE;

    PetscCall(RatelMaterialGetActiveFieldNames(ratel->materials[0], &active_field_names, NULL));
    PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[0], NULL, &active_field_sizes));

    ratel->bc_slip_count[i] = RATEL_MAX_BOUNDARY_FACES;
    PetscCall(PetscSNPrintf(option_name, sizeof(option_name), "-bc_slip_field_%" PetscInt_FMT, i));
    PetscCall(PetscOptionsIntArray(option_name, "Face IDs to apply slip BC", NULL, ratel->bc_slip_faces[i], &ratel->bc_slip_count[i], &option_set));
    if (!option_set) {
      PetscCall(PetscSNPrintf(option_name, sizeof(option_name), "-bc_slip_%s", active_field_names[i]));
      ratel->bc_slip_count[i] = RATEL_MAX_BOUNDARY_FACES;
      PetscCall(PetscOptionsIntArray(option_name, "Face IDs to apply slip BC", NULL, ratel->bc_slip_faces[i], &ratel->bc_slip_count[i], &option_set));
    }
    if (!option_set && i == 0) {
      ratel->bc_slip_count[i] = RATEL_MAX_BOUNDARY_FACES;
      PetscCall(PetscOptionsIntArray("-bc_slip", "Face IDs to apply slip BC", NULL, ratel->bc_slip_faces[i], &ratel->bc_slip_count[i], &option_set));
    }

    // -- Set components for each slip BC
    if (ratel->bc_slip_count[i]) PetscCall(RatelDebug(ratel, "---- Slip Boundaries, field %" PetscInt_FMT ": ", i));
    for (PetscInt j = 0; j < ratel->bc_slip_count[i]; j++) {
      PetscBool components_set = PETSC_FALSE;

      // ---- Constrained components
      ratel->bc_slip_components_count[i][j] = active_field_sizes[i];
      for (PetscInt k = 0; k < ratel->bc_slip_components_count[i][j]; k++) ratel->bc_slip_components[i][j][k] = 0.;

      PetscCall(PetscSNPrintf(option_name, sizeof(option_name), "-bc_slip_field_%" PetscInt_FMT "_face_%" PetscInt_FMT "_components", i,
                              ratel->bc_slip_faces[i][j]));
      PetscCall(PetscOptionsIntArray(option_name, "Components to constrain with slip BC", NULL, ratel->bc_slip_components[i][j],
                                     &ratel->bc_slip_components_count[i][j], &components_set));
      if (!components_set) {
        ratel->bc_slip_components_count[i][j] = active_field_sizes[i];
        PetscCall(PetscSNPrintf(option_name, sizeof(option_name), "-bc_slip_%s_face_%" PetscInt_FMT "_components", active_field_names[i],
                                ratel->bc_slip_faces[i][j]));
        PetscCall(PetscOptionsIntArray(option_name, "Components to constrain with slip BC", NULL, ratel->bc_slip_components[i][j],
                                       &ratel->bc_slip_components_count[i][j], &components_set));
      }
      if (i == 0 && !components_set) {
        ratel->bc_slip_components_count[i][j] = active_field_sizes[i];
        PetscCall(PetscSNPrintf(option_name, sizeof(option_name), "-bc_slip_%" PetscInt_FMT "_components", ratel->bc_slip_faces[i][j]));
        PetscCall(PetscOptionsIntArray(option_name, "Components to constrain with slip BC", NULL, ratel->bc_slip_components[i][j],
                                       &ratel->bc_slip_components_count[i][j], &components_set));
      }
      PetscCheck(components_set, ratel->comm, PETSC_ERR_SUP, "Must set slip components for face %" PetscInt_FMT, ratel->bc_slip_faces[i][j]);

      if (ratel->is_rank_0_debug) {
        // LCOV_EXCL_START
        char    components[8] = "";
        CeedInt head          = 0;

        for (CeedInt k = 0; k < ratel->bc_slip_components_count[i][j]; k++) {
          switch (ratel->bc_slip_components[i][j][k]) {
            case 0:
              components[head] = 'x';
              break;
            case 1:
              components[head] = 'y';
              break;
            case 2:
              components[head] = 'z';
              break;
            case 3:
              components[head] = 'd';
              break;
            default:
              PetscCheck(PETSC_FALSE, ratel->comm, PETSC_ERR_SUP, "Invalid slip component: %" PetscInt_FMT, ratel->bc_slip_components[i][j][k]);
          }
          head++;
          if (k < ratel->bc_slip_components_count[i][j] - 1) {
            components[head]     = ',';
            components[head + 1] = ' ';
            head += 2;
          }
        }
        PetscCall(RatelDebug(ratel, "----   Slip Boundary %" PetscInt_FMT ": components %s", ratel->bc_slip_faces[i][j], components));
        // LCOV_EXCL_STOP
      }
    }
  }

  // Neumann boundary conditions
  ratel->bc_traction_count = RATEL_MAX_BOUNDARY_FACES;
  PetscCall(PetscOptionsIntArray("-bc_traction", "Face IDs to apply traction (Neumann) BC", NULL, ratel->bc_traction_faces, &ratel->bc_traction_count,
                                 NULL));

  // Pressure boundary conditions
  ratel->bc_pressure_count = RATEL_MAX_BOUNDARY_FACES;
  PetscCall(PetscOptionsIntArray("-bc_pressure", "Face IDs to apply pressure BC", NULL, ratel->bc_pressure_faces, &ratel->bc_pressure_count, NULL));
  PetscOptionsEnd();

  // Platen boundary conditions names and label values
  {
    ratel->bc_platen_count = RATEL_MAX_BOUNDARY_FACES;
    PetscOptionsBegin(ratel->comm, NULL, "Ratel read in platen BC names", NULL);
    PetscCall(PetscOptionsStringArray("-bc_platen", "Name of each applied platen BC", NULL, ratel->bc_platen_names, &ratel->bc_platen_count, NULL));
    PetscOptionsEnd();  // TODO - better way of adding the prefix. probably just a new function here.

    // Check for provided label values. Use platen name if not
    for (PetscInt i = 0; i < ratel->bc_platen_count; i++) {
      char cl_prefix[PETSC_MAX_OPTION_NAME];

      PetscCall(RatelFaceLabelValueFromOptions(ratel, "bc_platen_", ratel->bc_platen_names[i], &ratel->bc_platen_label_values[i]));

      PetscCall(PetscSNPrintf(cl_prefix, sizeof(cl_prefix), "bc_platen_%s_", ratel->bc_platen_names[i]));
      PetscOptionsBegin(ratel->comm, cl_prefix, "Set platen BCs types", NULL);
      PetscCall(PetscOptionsEnum("-type", "Type of platen BC", NULL, RatelPlatenTypesCL, (PetscEnum)ratel->bc_platen_types[i],
                                 (PetscEnum *)&ratel->bc_platen_types[i], NULL));
      PetscOptionsEnd();  // End of platen BC options
    }
  }

  PetscOptionsBegin(ratel->comm, NULL, "Ratel library with PETSc and libCEED", NULL);  // Resume Ratel options

  // Initial condition type
  ratel->initial_condition_type = RATEL_INITIAL_CONDITION_ZERO;
  {
    PetscInt  max_strings    = 1;
    PetscBool is_continue_ic = PETSC_FALSE;

    PetscCall(PetscOptionsStringArray("-continue_file", "File-path for initial condition binary file", NULL, &ratel->continue_file_name, &max_strings,
                                      &is_continue_ic));
    if (is_continue_ic) ratel->initial_condition_type = RATEL_INITIAL_CONDITION_CONTINUE;
  }

  // Face diagnostics
  ratel->surface_force_face_count = RATEL_MAX_BOUNDARY_FACES;
  PetscCall(PetscOptionsStringArray("-surface_force_faces", "Face IDs to compute surface force", NULL, ratel->surface_force_face_names,
                                    &ratel->surface_force_face_count, NULL));
  PetscOptionsEnd();

  // Check for provided label values.
  for (PetscInt i = 0; i < ratel->surface_force_face_count; i++) {
    RatelBoundingBoxParams params_bounding_box;

    // prefix needs to be: surface_force_face_NAME
    PetscCall(RatelBoundingBoxParamsFromOptions(ratel, "surface_force_face_", ratel->surface_force_face_names[i], &params_bounding_box));
    PetscCall(
        RatelFaceLabelValueFromOptions(ratel, "surface_force_face_", ratel->surface_force_face_names[i], &ratel->surface_force_face_label_values[i]));
  }
  PetscOptionsBegin(ratel->comm, NULL, "Ratel library with PETSc and libCEED", NULL);  // Resume Ratel options

  // Set target strain energy
  {
    ratel->has_expected_strain_energy = PETSC_FALSE;

    ratel->expected_strain_energy = 0.;
    PetscCall(PetscOptionsReal("-expected_strain_energy", "Expected final strain energy", NULL, ratel->expected_strain_energy,
                               &ratel->expected_strain_energy, &ratel->has_expected_strain_energy));
    if (!ratel->has_expected_strain_energy) ratel->expected_strain_energy = 0.0;
  }

  // Set target max displacement
  {
    PetscInt   num_values   = 3;
    PetscBool  option_set   = PETSC_FALSE;
    const char option_str[] = "-expected_max_displacement";
    const char desc_str[]   = "Expected final max displacement";

    for (PetscInt i = 0; i < 3; i++) {
      ratel->has_expected_max_displacement[i] = PETSC_FALSE;
      ratel->expected_max_displacement[i]     = 0.;
    }

    PetscCall(PetscOptionsRealArray(option_str, desc_str, NULL, ratel->expected_max_displacement, &num_values, &option_set));
    PetscCheck(!option_set || num_values == 3, ratel->comm, PETSC_ERR_USER_INPUT,
               "All three max displacements are required, use `-expected_max_displacement_[x|y|z]` to specify single components");
    if (option_set) {
      for (PetscInt i = 0; i < 3; i++) ratel->has_expected_max_displacement[i] = PETSC_TRUE;
    } else {
      for (PetscInt i = 0; i < 3; i++) {
        char component_option_str[PETSC_MAX_OPTION_NAME];
        char component_desc_str[PETSC_MAX_OPTION_NAME];

        PetscCall(PetscSNPrintf(component_option_str, sizeof(component_option_str), "%s_%c", option_str, (char)('x' + i)));
        PetscCall(PetscSNPrintf(component_desc_str, sizeof(component_desc_str), "%s, %c component", desc_str, (char)('x' + i)));
        PetscCall(PetscOptionsReal(component_option_str, component_desc_str, NULL, ratel->expected_max_displacement[i],
                                   &ratel->expected_max_displacement[i], &ratel->has_expected_max_displacement[i]));
      }
    }
  }

  // Set target surface forces
  {
    for (PetscInt f = 0; f < ratel->surface_force_face_count; f++) {
      PetscInt  num_values = 3;
      PetscBool option_set = PETSC_FALSE;

      for (PetscInt i = 0; i < 3; i++) {
        ratel->has_expected_surface_force[f][i] = PETSC_FALSE;
        ratel->expected_surface_force[f][i]     = 0.;
      }
      char face_option_str[PETSC_MAX_OPTION_NAME] = "";
      char face_desc_str[PETSC_MAX_OPTION_NAME]   = "";
      PetscCall(PetscSNPrintf(face_option_str, sizeof(face_option_str), "-expected_surface_force_face_%s", ratel->surface_force_face_names[f]));
      PetscCall(PetscSNPrintf(face_desc_str, sizeof(face_desc_str), "Expected final surface force for face %s", ratel->surface_force_face_names[f]));
      PetscCall(PetscOptionsRealArray(face_option_str, face_desc_str, NULL, ratel->expected_surface_force[f], &num_values, &option_set));
      PetscCheck(
          !option_set || num_values == 3, ratel->comm, PETSC_ERR_USER_INPUT,
          "All three surface force components are required, use `-expected_surface_force_face_[facenumber]_[x|y|z]` to specify single components");
      if (option_set) {
        for (PetscInt i = 0; i < 3; i++) ratel->has_expected_surface_force[f][i] = PETSC_TRUE;
      } else {
        for (PetscInt i = 0; i < 3; i++) {
          char option_str[PETSC_MAX_OPTION_NAME] = "";
          char desc_str[PETSC_MAX_OPTION_NAME]   = "";
          PetscCall(PetscSNPrintf(option_str, sizeof(option_str), "%s_%c", face_option_str, (char)('x' + i)));
          PetscCall(PetscSNPrintf(desc_str, sizeof(desc_str), "%s, %c component", face_desc_str, (char)('x' + i)));

          PetscCall(PetscOptionsReal(option_str, desc_str, NULL, ratel->expected_surface_force[f][i], &ratel->expected_surface_force[f][i],
                                     &ratel->has_expected_surface_force[f][i]));
        }
      }
    }
  }

  // Set target centroids
  {
    for (PetscInt f = 0; f < ratel->surface_force_face_count; f++) {
      PetscInt  num_values                             = 3;
      PetscBool option_set                             = PETSC_FALSE;
      char      face_option_str[PETSC_MAX_OPTION_NAME] = "";
      char      face_desc_str[PETSC_MAX_OPTION_NAME]   = "";

      for (PetscInt i = 0; i < 3; i++) {
        ratel->has_expected_centroid[f][i] = PETSC_FALSE;
        ratel->expected_centroid[f][i]     = 0.;
      }
      PetscCall(PetscSNPrintf(face_option_str, sizeof(face_option_str), "-expected_centroid_face_%s", ratel->surface_force_face_names[f]));
      PetscCall(PetscSNPrintf(face_desc_str, sizeof(face_desc_str), "Expected final centroid for face %s", ratel->surface_force_face_names[f]));
      PetscCall(PetscOptionsRealArray(face_option_str, face_desc_str, NULL, ratel->expected_centroid[f], &num_values, &option_set));
      PetscCheck(!option_set || num_values == 3, ratel->comm, PETSC_ERR_USER_INPUT,
                 "All three centroid components are required, use `-expected_centroid_face_[facenumber]_[x|y|z]` to specify single components");
      if (option_set) {
        for (PetscInt i = 0; i < 3; i++) ratel->has_expected_centroid[f][i] = PETSC_TRUE;
      } else {
        for (PetscInt i = 0; i < 3; i++) {
          char option_str[PETSC_MAX_OPTION_NAME] = "";
          char desc_str[PETSC_MAX_OPTION_NAME]   = "";

          PetscCall(PetscSNPrintf(option_str, sizeof(option_str), "%s_%c", face_option_str, (char)('x' + i)));
          PetscCall(PetscSNPrintf(desc_str, sizeof(desc_str), "%s, %c component", face_desc_str, (char)('x' + i)));

          PetscCall(PetscOptionsReal(option_str, desc_str, NULL, ratel->expected_centroid[f][i], &ratel->expected_centroid[f][i],
                                     &ratel->has_expected_centroid[f][i]));
        }
      }
    }
  }
  PetscOptionsEnd();  // End of setting Ratel options

  // Check for all required values set
  {
    PetscInt clamp_bc_count = 0;

    for (PetscInt i = 0; i < ratel->num_active_fields; i++) {
      clamp_bc_count += ratel->bc_clamp_count[i] + ratel->bc_mms_count[i] + ratel->bc_slip_count[i];
    }
    PetscCheck(bc_allow_no_clamp || clamp_bc_count > 0, ratel->comm, PETSC_ERR_SUP,
               "At least one clamp boundary condition needed, or use -bc_allow_no_clamp");
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Process Command Line Options Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}
