/// @file
/// Ratel forcing function support

#include <ceed.h>
#include <ratel-fem.h>
#include <ratel-impl.h>
#include <ratel-material.h>
#include <ratel-mpm.h>
#include <ratel-utils.h>
#include <ratel.h>
#include <ratel/models/forcing.h>
#include <stddef.h>

/// @addtogroup RatelInternal
/// @{

static PetscErrorCode RatelForcingBodyParamsFromOptions(Ratel ratel, const char *cl_prefix, const char *cl_message,
                                                        RatelForcingBodyParams *params_forcing) {
  PetscInt                 max_num_directions = 3 * RATEL_MAX_FORCE_INTERP_POINTS;
  PetscInt                 max_num_times      = RATEL_MAX_FORCE_INTERP_POINTS;
  PetscBool                force_set = PETSC_FALSE, times_set = PETSC_FALSE, interp_set = PETSC_FALSE;
  PetscScalar              acceleration[3 * RATEL_MAX_FORCE_INTERP_POINTS] = {0}, times[RATEL_MAX_FORCE_INTERP_POINTS] = {1, 0};
  RatelBCInterpolationType interpolation_type = RATEL_BC_INTERP_LINEAR;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "--- Ratel Forcing Body Data From Options"));

  PetscOptionsBegin(ratel->comm, cl_prefix, cl_message, NULL);

  // Get transition times for forcing vectors
  PetscCall(
      PetscOptionsScalarArray("-forcing_body_times", "Transition times between each acceleration vector", NULL, times, &max_num_times, &times_set));
  if (times_set) {
    PetscCall(PetscMemcpy(params_forcing->times, times, max_num_times * sizeof(PetscScalar)));
    params_forcing->num_times = max_num_times;
    // Ensure times are monotonically increasing
    PetscBool monotonic       = PETSC_TRUE;

    for (PetscInt j = 0; j < params_forcing->num_times - 1; j++) {
      if (params_forcing->times[j] >= params_forcing->times[j + 1]) {
        monotonic = PETSC_FALSE;
      }
    }
    PetscCheck(monotonic, ratel->comm, PETSC_ERR_SUP, "Transition times must be monotonically increasing.");

    // -- Check bounds of times
    PetscCheck(params_forcing->times[0] >= 0, ratel->comm, PETSC_ERR_SUP, "First time must be greater than or equal to 0.");
  }
  // Get traction vectors
  PetscCall(PetscOptionsScalarArray("-forcing_body_vector", "Force vectors", NULL, acceleration, &max_num_directions, &force_set));
  if (force_set) {
    PetscCall(PetscMemcpy(params_forcing->acceleration, acceleration, max_num_directions * sizeof(PetscScalar)));

    PetscCheck(force_set, ratel->comm, PETSC_ERR_SUP, "Force vector must be set for user-specified body force.");
    PetscCheck(max_num_directions % 3 == 0, ratel->comm, PETSC_ERR_SUP, "Force vector must have a multiple of 3 components");

    PetscInt num_vectors = max_num_directions / 3;
    PetscCheck(num_vectors == params_forcing->num_times, ratel->comm, PETSC_ERR_SUP, "A time must be set for each provided acceleration vector.");
    PetscCheck(times_set || num_vectors == 1, ratel->comm, PETSC_ERR_SUP, "Times must be set if more than one acceleration vector is specified.");
  }

  // Interpolation type
  PetscCall(PetscOptionsEnum("-forcing_body_interpolation", "Set interpolation type for user-specified body force", NULL, RatelBCInterpolationTypesCL,
                             (PetscEnum)interpolation_type, (PetscEnum *)&interpolation_type, &interp_set));  // NOLINT

  if (interp_set) {
    params_forcing->interpolation_type = interpolation_type;
  }

  PetscOptionsEnd();  // End of reading user-specified body force options

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "--- Ratel Forcing Body Data From Options Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Read user forcing vectors from options database

  Collective across MPI processes.

  @param[in]   material      `RatelMaterial` context
  @param[out]  params_forcing  Data structure to store traction data
*/
PetscErrorCode RatelMaterialForcingBodyDataFromOptions(RatelMaterial material, RatelForcingBodyParams *params_forcing) {
  Ratel            ratel     = material->ratel;
  char            *cl_prefix = NULL, *cl_message = NULL;
  PetscBool        global_force_set = PETSC_FALSE, material_force_set = PETSC_FALSE;
  RatelForcingType forcing_type = RATEL_FORCING_NONE;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "--- Ratel Material Forcing Body Data From Options"));

  // Initialize context data
  for (PetscInt i = 0; i < RATEL_MAX_FORCE_INTERP_POINTS; i++) {
    for (PetscInt j = 0; j < 3; j++) params_forcing->acceleration[i * 3 + j] = 0.0;
    params_forcing->times[i] = 1.0;
  }
  params_forcing->rho                = 1.0;
  params_forcing->num_times          = 1;
  params_forcing->interpolation_type = RATEL_BC_INTERP_LINEAR;
  params_forcing->time               = RATEL_UNSET_INITIAL_TIME;

  // Read CL options for user-specified forcing vectors
  PetscCall(RatelMaterialGetCLPrefix(material, &cl_prefix));
  PetscCall(RatelMaterialGetCLMessage(material, &cl_message));

  // Determine whether to use global or material-specific forcing
  // -- Read material-specific forcing options
  PetscOptionsBegin(ratel->comm, cl_prefix, cl_message, NULL);
  // ---- Get density
  PetscCall(PetscOptionsScalar("-rho", "Density", NULL, params_forcing->rho, &params_forcing->rho, NULL));
  // ---- Get material-specific forcing type
  PetscCall(PetscOptionsEnum("-forcing", "Set forcing function option", NULL, RatelForcingTypesCL, (PetscEnum)forcing_type,  // NOLINT
                             (PetscEnum *)&forcing_type, &material_force_set));
  PetscOptionsEnd();
  // -- Read global forcing options
  PetscOptionsBegin(ratel->comm, NULL, "", NULL);
  PetscCall(PetscOptionsEnum("-forcing", "Set forcing function option", NULL, RatelForcingTypesCL, (PetscEnum)forcing_type,  // NOLINT
                             (PetscEnum *)&forcing_type, &global_force_set));
  PetscOptionsEnd();

  // Prefer material-specific forcing
  if (global_force_set) {
    PetscCall(RatelForcingBodyParamsFromOptions(ratel, NULL, "", params_forcing));
  }
  if (global_force_set || material_force_set) {
    PetscCall(RatelForcingBodyParamsFromOptions(ratel, cl_prefix, cl_message, params_forcing));
  }

  // Incrementalize if using MPM
  if (ratel->method_type == RATEL_METHOD_MPM) {
    PetscCall(RatelIncrementalize(ratel, PETSC_TRUE, 3, &params_forcing->num_times, params_forcing->acceleration, params_forcing->times,
                                  &params_forcing->interpolation_type));
  }

  PetscCall(RatelDebug(ratel, "---- User-Specified Body Force: %" PetscInt_FMT " acceleration vector(s) with interpolation: %s",
                       params_forcing->num_times, RatelBCInterpolationTypes[params_forcing->interpolation_type]));
  for (PetscInt j = 0; j < params_forcing->num_times; j++) {
    PetscCall(RatelDebug(ratel, "------   t = %.3f: [Fx: %.3f, Fy: %.3f, Fz: %.3f]", params_forcing->times[j], params_forcing->acceleration[3 * j],
                         params_forcing->acceleration[3 * j + 1], params_forcing->acceleration[3 * j + 2]));
  }

  PetscCall(PetscFree(cl_prefix));
  PetscCall(PetscFree(cl_message));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "--- Ratel Material Forcing Body Data From Options Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Add forcing term to residual u term operator.

  Collective across MPI processes.

  @param[in]   material     `RatelMaterial` context
  @param[out]  op_residual  Composite residual u term `CeedOperator` to add `RatelMaterial` sub-operator

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupForcingSuboperator(RatelMaterial material, CeedOperator op_residual) {
  Ratel ratel = material->ratel;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Forcing Sub-operator"));

  if (material->forcing_type != RATEL_FORCING_NONE) {
    switch (ratel->method_type) {
      case RATEL_METHOD_FEM:
        PetscCall(RatelMaterialSetupForcingSuboperator_FEM(material, op_residual));
        break;
      case RATEL_METHOD_MPM:
        PetscCall(RatelMaterialSetupForcingSuboperator_MPM(material, op_residual));
        break;
    }
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Forcing Sub-operator Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Add energy of forcing term to external energy operator.

  Collective across MPI processes.

  @param[in]   material            `RatelMaterial` context
  @param[in]   dm_energy           `DM` for external energy computation
  @param[out]  op_external_energy  Composite external energy `CeedOperator` to add energy of body force sub-operator

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupForcingEnergySuboperator(RatelMaterial material, DM dm_energy, CeedOperator op_external_energy) {
  Ratel ratel = material->ratel;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Forcing Energy Sub-operator"));

  if (material->forcing_type != RATEL_FORCING_NONE) {
    switch (ratel->method_type) {
      case RATEL_METHOD_FEM:
        PetscCall(RatelMaterialSetupForcingEnergySuboperator_FEM(material, dm_energy, op_external_energy));
        break;
      case RATEL_METHOD_MPM:
        PetscCall(RatelMaterialSetupForcingEnergySuboperator_MPM(material, dm_energy, op_external_energy));
        break;
    }
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Forcing Energy Sub-operator Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}
