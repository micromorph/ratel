/// @file
/// Implementation of Ratel solver management interfaces

#include <ceed.h>
#include <mat-ceed.h>
#include <math.h>
#include <petsc-ceed-utils.h>
#include <petscdmplex.h>
#include <petscksp.h>
#include <ratel-boundary.h>
#include <ratel-fem.h>
#include <ratel-impl.h>
#include <ratel-material.h>
#include <ratel-pmg-impl.h>
#include <ratel-pmg.h>
#include <ratel.h>
#include <ratel/models/common-parameters.h>
#include <stdio.h>
#include <string.h>

/// @ingroup RatelSolvers
/// @{

PetscClassId  PCPMG_CLASSID;
PetscLogEvent PCPMG_Setup;
PetscLogEvent RATEL_Prolong[RATEL_MAX_MULTIGRID_LEVELS], RATEL_Prolong_CeedOp[RATEL_MAX_MULTIGRID_LEVELS], RATEL_Restrict[RATEL_MAX_MULTIGRID_LEVELS],
    RATEL_Restrict_CeedOp[RATEL_MAX_MULTIGRID_LEVELS];

/**
  @brief Register p-multigrid `Ratel` log events.

  Not collective across MPI processes.

  @param[in]  num_multigrid_levels  Number of multigrid levels to register

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelRegisterPMultigridLogEvents(PetscInt num_multigrid_levels) {
  static PetscBool registered = PETSC_FALSE;

  PetscFunctionBeginUser;
  if (registered) PetscFunctionReturn(PETSC_SUCCESS);

  PetscCall(PetscClassIdRegister("PCpMG", &PCPMG_CLASSID));
  PetscCall(PetscLogEventRegister("PCpMGSetup", PCPMG_CLASSID, &PCPMG_Setup));
  for (PetscInt i = 0; i < num_multigrid_levels; i++) {
    char event_name[128];

    PetscCall(PetscSNPrintf(event_name, sizeof(event_name), "RatelProlong %" PetscInt_FMT, i + 1));
    PetscCall(PetscLogEventRegister(event_name, RATEL_CLASSID, &RATEL_Prolong[i + 1]));
    PetscCall(PetscSNPrintf(event_name, sizeof(event_name), "RatelProlong %" PetscInt_FMT " CeedOp", i + 1));
    PetscCall(PetscLogEventRegister(event_name, RATEL_CLASSID, &RATEL_Prolong_CeedOp[i + 1]));

    PetscCall(PetscSNPrintf(event_name, sizeof(event_name), "RatelRestrict %" PetscInt_FMT, i + 1));
    PetscCall(PetscLogEventRegister(event_name, RATEL_CLASSID, &RATEL_Restrict[i + 1]));
    PetscCall(PetscSNPrintf(event_name, sizeof(event_name), "RatelRestrict %" PetscInt_FMT " CeedOp", i + 1));
    PetscCall(PetscLogEventRegister(event_name, RATEL_CLASSID, &RATEL_Restrict_CeedOp[i + 1]));
  }
  registered = PETSC_TRUE;
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get `PCpMG` options.

  Collective across MPI processes.

  @param[in,out]  pmg        `PCpMG` context
  @param[in]      pmg_field  Index of field on the `DM`
  @param[in]      pc_prefix  Command line option prefix for `PC`

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelPMGProcessCommandLineOptions(RatelPMGContext pmg, PetscInt pmg_field, const char *pc_prefix) {
  Ratel ratel = pmg->ratel;
  char  prefix[PETSC_MAX_PATH_LEN];

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel PMG Process Command Line Options"));

  {
    size_t pc_prefix_len;

    PetscCall(PetscStrlen(pc_prefix, &pc_prefix_len));
    PetscCall(PetscSNPrintf(prefix, PETSC_MAX_PATH_LEN, "%s%s", pc_prefix ? pc_prefix : "", RATEL_PCPMG "_"));
  }
  PetscOptionsBegin(ratel->comm, prefix, "Ratel PMG preconditioner", NULL);

  // Type of multigrid and coarsening
  pmg->p_multigrid_coarsening_type = RATEL_P_MULTIGRID_COARSENING_LOGARITHMIC;
  PetscCall(PetscOptionsEnum("-coarsening_type", "Set multigrid type option", NULL, RatelPMultigridCoarseningTypesCL,
                             (PetscEnum)pmg->p_multigrid_coarsening_type, (PetscEnum *)&pmg->p_multigrid_coarsening_type, NULL));

  if (pmg->p_multigrid_coarsening_type == RATEL_P_MULTIGRID_COARSENING_USER) {
    // Multigrid level orders for user defined coarsening
    PetscBool found_orders_option;
    PetscInt  num_multigrid_levels = RATEL_MAX_MULTIGRID_LEVELS - 1;

    PetscCall(PetscOptionsIntArray("-level_orders", "Polynomial orders of basis functions for multigrid", NULL, &pmg->multigrid_level_orders[0],
                                   &num_multigrid_levels, &found_orders_option));
    PetscCheck(found_orders_option, ratel->comm, PETSC_ERR_SUP, "Level orders must be specified for '-coarsen %s'",
               RatelPMultigridCoarseningTypesCL[RATEL_P_MULTIGRID_COARSENING_USER]);
    pmg->multigrid_level_orders[num_multigrid_levels] = ratel->fine_grid_orders[pmg_field];
    pmg->num_multigrid_levels                         = num_multigrid_levels + 1;

    for (PetscInt i = 0; i < pmg->num_multigrid_levels; i++) {
      PetscCheck(pmg->multigrid_level_orders[i] > 0, ratel->comm, PETSC_ERR_SUP, "Polynomial order must be positive integer");
      PetscCheck(i == 0 || pmg->multigrid_level_orders[i] > pmg->multigrid_level_orders[i - 1], ratel->comm, PETSC_ERR_SUP,
                 "Polynomial orders for multigrid levels must be provided in strictly ascending order");
      PetscCall(RatelDebug(ratel, "---- Multigrid polynomial order level %" PetscInt_FMT ": %" PetscInt_FMT, i, pmg->multigrid_level_orders[i]));
    }
    pmg->coarse_grid_order = pmg->multigrid_level_orders[pmg->num_multigrid_levels - 1];
  } else {
    // Coarse grid order
    PetscBool found_coarse_orders_option = PETSC_FALSE;
    PetscInt  num_coarse_orders          = 1;

    pmg->coarse_grid_order = 1;
    PetscCall(PetscOptionsIntArray("-coarse_order", "Polynomial orders of coarse grid basis functions", NULL, &pmg->coarse_grid_order,
                                   &num_coarse_orders, &found_coarse_orders_option));
    if (found_coarse_orders_option) {
      PetscCheck(pmg->coarse_grid_order < ratel->fine_grid_orders[pmg_field], ratel->comm, PETSC_ERR_SUP,
                 "Coarse grid polynomial order must be less than fine grid order");
    }
    PetscCall(RatelDebug(ratel, "---- Coarse grid polynomial order: %" PetscInt_FMT, pmg->coarse_grid_order));
  }

  PetscOptionsEnd();  // End of setting PMG options

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel PMG Process Command Line Options Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup `CeedOperator` for multigrid prolongation, restriction, and coarse grid Jacobian evaluation.

  Collective across MPI processes.

  @param[in]      ratel             `Ratel` context
  @param[in]      dm_level          `DMPlex` for multigrid level to setup
  @param[in]      M_loc             PETSc local vector holding multiplicity data
  @param[in]      level             Multigrid level to set up
  @param[in,out]  op_jacobian_fine  Composite `CeedOperator` for Jacobian
  @param[in,out]  op_jacobian       Composite `CeedOperator` for Jacobian
  @param[in,out]  op_prolong        Composite `CeedOperator` for prolongation
  @param[in,out]  op_restrict       Composite `CeedOperator` for restriction

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelSetupMultigridLevel(Ratel ratel, DM dm_level, Vec M_loc, PetscInt level, CeedOperator op_jacobian_fine,
                                               CeedOperator op_jacobian, CeedOperator op_prolong, CeedOperator op_restrict) {
  PetscScalar *m;
  PetscMemType m_mem_type;
  CeedVector   m_loc;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Setup Multigrid Level"));
  PetscCall(RatelDebug(ratel, "---- Level: %" PetscInt_FMT, level));

  // Multiplicity
  {
    PetscInt   len;
    CeedVector jacobian_m_loc;

    PetscCall(VecGetSize(M_loc, &len));
    RatelCallCeed(ratel, CeedVectorCreate(ratel->ceed, len, &m_loc));
    RatelCallCeed(ratel, CeedVectorCreate(ratel->ceed, len, &jacobian_m_loc));

    PetscCall(VecGetArrayAndMemType(M_loc, &m, &m_mem_type));
    RatelCallCeed(ratel, CeedVectorSetArray(m_loc, MemTypePetscToCeed(m_mem_type), CEED_USE_POINTER, (CeedScalar *)m));
    RatelCallCeed(ratel, CeedCompositeOperatorGetMultiplicity(op_jacobian_fine, ratel->num_jacobian_multiplicity_skip_indices,
                                                              ratel->jacobian_multiplicity_skip_indices, jacobian_m_loc));
    RatelCallCeed(ratel, CeedVectorPointwiseMult(m_loc, m_loc, jacobian_m_loc));
    RatelCallCeed(ratel, CeedVectorDestroy(&jacobian_m_loc));
  }

  // Setup level operators
  for (PetscInt i = 0; i < ratel->num_materials; i++) {
    PetscCall(RatelMaterialSetupMultigridLevel(ratel->materials[i], dm_level, m_loc, op_jacobian_fine, op_jacobian, op_prolong, op_restrict));
  }
  PetscCall(RatelSetupPressureJacobianMultigridLevel(ratel, dm_level, NULL, op_jacobian_fine, op_jacobian));

  // Restore PETSc vector
  RatelCallCeed(ratel, CeedVectorTakeArray(m_loc, MemTypePetscToCeed(m_mem_type), (CeedScalar **)&m));
  RatelCallCeed(ratel, CeedVectorDestroy(&m_loc));
  PetscCall(VecRestoreArrayAndMemType(M_loc, &m));

  // Debugging output
  if (ratel->is_rank_0_debug) {
    // LCOV_EXCL_START
    RatelCallCeed(ratel, CeedOperatorView(op_jacobian, stdout));
    RatelCallCeed(ratel, CeedOperatorView(op_prolong, stdout));
    RatelCallCeed(ratel, CeedOperatorView(op_restrict, stdout));
    // LCOV_EXCL_STOP
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Setup Multigrid Level Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup `PCpMG` preconditioner from `Ratel` context.

  Collective across MPI processes.

  @param[in,out]  pc  `PCpMG` object

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelPCPMGCreate(PC pc) {
  Ratel           ratel     = NULL;
  RatelPMGContext pmg       = NULL;
  PetscInt        pmg_field = 0;
  Vec            *X_loc;
  DM              dm, dm_solution, *dm_hierarchy;
  PetscLogStage   stage_pmg_setup;

  PetscFunctionBeginUser;
  PetscCall(PCGetDM(pc, &dm));
  PetscCall(DMGetApplicationContext(dm, (void *)&ratel));
  PetscCheck(ratel != NULL, PetscObjectComm((PetscObject)pc), PETSC_ERR_SUP, "Must call RatelDMCreate before creating pMG preconditioner");
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel PC pMG Create"));

  // Get command line options
  PetscCall(PetscNew(&pmg));
  pmg->ratel = ratel;
  {
    const char *pc_prefix;

    PetscCall(PCGetOptionsPrefix(pc, &pc_prefix));
    // Find field index
    if (pc_prefix) {
      size_t       pc_field_name_len;
      char        *pc_field_name;
      const char **field_names;
      CeedInt      num_fields;

      // pc_prefix is of form fieldsplit_[fieldname]_
      PetscCall(PetscStrchr(pc_prefix, '_', &pc_field_name));
      pc_field_name++;  // advance to first character after 'fieldsplit_'
      PetscCall(PetscStrlen(pc_field_name, &pc_field_name_len));
      pc_field_name_len--;  // don't consider final _
      PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[0], &num_fields, NULL));
      PetscCall(RatelMaterialGetActiveFieldNames(ratel->materials[0], &field_names, NULL));
      for (PetscInt i = 0; i < num_fields; i++) {
        size_t    field_name_len;
        PetscBool is_match = PETSC_FALSE;

        PetscCall(PetscStrlen(field_names[i], &field_name_len));
        if (field_name_len == pc_field_name_len) PetscCall(PetscStrncmp(pc_field_name, field_names[i], field_name_len, &is_match));
        if (is_match) pmg_field = i;
      }
    }
    PetscCall(RatelPMGProcessCommandLineOptions(pmg, pmg_field, pc_prefix));
  }

  PetscCall(PetscLogStageGetId("Ratel pMG Setup", &stage_pmg_setup));
  if (stage_pmg_setup == -1) PetscCall(PetscLogStageRegister("Ratel pMG Setup", &stage_pmg_setup));
  PetscCall(PetscLogStagePush(stage_pmg_setup));

  // Determine number of levels
  switch (pmg->p_multigrid_coarsening_type) {
    case RATEL_P_MULTIGRID_COARSENING_LOGARITHMIC:
      pmg->num_multigrid_levels = ceil(log(ratel->fine_grid_orders[pmg_field]) / log(2)) - floor(log(pmg->coarse_grid_order) / log(2)) + 1;
      break;
    case RATEL_P_MULTIGRID_COARSENING_UNIFORM:
      pmg->num_multigrid_levels = ratel->fine_grid_orders[pmg_field] - pmg->coarse_grid_order + 1;
      break;
    case RATEL_P_MULTIGRID_COARSENING_USER:
      // Nothing to do
      break;
  }
  PetscInt fine_level = pmg->num_multigrid_levels - 1;

  // Register all log events
  PetscCall(RatelRegisterPMultigridLogEvents(pmg->num_multigrid_levels));

  // Populate array of degrees for each level for multigrid
  switch (pmg->p_multigrid_coarsening_type) {
    case RATEL_P_MULTIGRID_COARSENING_LOGARITHMIC:
      pmg->multigrid_level_orders[0]          = pmg->coarse_grid_order;
      pmg->multigrid_level_orders[fine_level] = ratel->fine_grid_orders[pmg_field];
      for (CeedInt i = fine_level - 1; i > 0; i--) {
        pmg->multigrid_level_orders[i] = PetscMax(ceil(pmg->multigrid_level_orders[i + 1] / 2.0), pmg->coarse_grid_order);
      }
      break;
    case RATEL_P_MULTIGRID_COARSENING_UNIFORM:
      pmg->multigrid_level_orders[fine_level] = ratel->fine_grid_orders[pmg_field];
      for (PetscInt i = fine_level - 1; i >= 0; i--) {
        pmg->multigrid_level_orders[i] = PetscMax(pmg->multigrid_level_orders[i + 1] - 1, pmg->coarse_grid_order);
      }
      break;
    case RATEL_P_MULTIGRID_COARSENING_USER:
      // Nothing to do
      break;
  }

  // -- Realloc arrays
  PetscCall(RatelDebug(ratel, "---- Reallocating arrays"));
  {
    // ---- DM
    DM       *sub_dms;
    IS       *field_is;
    PetscBool is_simplex = PETSC_FALSE;

    PetscCall(PetscCalloc1(pmg->num_multigrid_levels, &dm_hierarchy));
    PetscCall(DMCreateFieldDecomposition(dm_solution, &ratel->num_active_fields, NULL, &field_is, &sub_dms));
    PetscCall(DMPlexIsSimplex(dm_solution, &is_simplex));
    if (!is_simplex) {
      for (PetscInt f = 0; f < ratel->num_active_fields; f++) {
        PetscCall(DMPlexSetClosurePermutationTensor(sub_dms[f], PETSC_DETERMINE, NULL));
      }
    }
    dm_hierarchy[fine_level] = sub_dms[pmg_field];
    PetscCall(PetscObjectReference((PetscObject)dm_hierarchy[fine_level]));
    // ---- MatShells
    PetscCall(PetscCalloc1(pmg->num_multigrid_levels, &pmg->mats_operator));
    PetscCall(PetscCalloc1(pmg->num_multigrid_levels, &pmg->mats_prolong_restrict));
    {
      Mat mat_jacobian_fine;

      if (ratel->num_active_fields > 1) {
        PetscCall(MatCreateSubMatrix(ratel->mat_jacobian_pre, field_is[pmg_field], field_is[pmg_field], MAT_INITIAL_MATRIX, &mat_jacobian_fine));
      } else {
        PetscCall(PetscObjectReference((PetscObject)ratel->mat_jacobian_pre));
        mat_jacobian_fine = ratel->mat_jacobian_pre;
      }
      pmg->mats_operator[fine_level] = mat_jacobian_fine;
    }
    // ---- Vecs
    PetscCall(PetscCalloc1(pmg->num_multigrid_levels, &X_loc));
    {
      Vec X_loc_fine;

      PetscCall(MatCeedGetLocalVectors(pmg->mats_operator[fine_level], &X_loc_fine, NULL));
      X_loc[fine_level] = X_loc_fine;
      PetscCall(PetscObjectReference((PetscObject)X_loc_fine));
      PetscCall(MatCeedRestoreLocalVectors(pmg->mats_operator[fine_level], &X_loc_fine, NULL));
    }
    // Clean up sub dms
    for (PetscInt f = 0; f < ratel->num_active_fields; f++) {
      PetscCall(ISDestroy(&field_is[f]));
      PetscCall(DMDestroy(&sub_dms[f]));
    }
    PetscCall(PetscFree(field_is));
    PetscCall(PetscFree(sub_dms));
  }

  // Setup DMs for multigrid levels
  switch (ratel->method_type) {
    case RATEL_METHOD_FEM: {
      PetscCall(RatelDebug(ratel, "---- Mesh: FEM"));
      PetscInt       field_orders[RATEL_MAX_FIELDS];
      VecType        vec_type;
      CeedInt        num_active_fields;
      const CeedInt *active_field_sizes;

      PetscCall(DMGetVecType(dm_solution, &vec_type));
      PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[0], &num_active_fields, &active_field_sizes));
      for (CeedInt i = 0; i < fine_level; i++) {
        for (PetscInt j = 0; j < ratel->num_active_fields; j++) {
          if (j == pmg_field) field_orders[j] = pmg->multigrid_level_orders[i];
          else field_orders[j] = ratel->fine_grid_orders[j];
        }
        DM           dm, *sub_dms;
        PetscBool    is_simplex = PETSC_FALSE;
        const char **active_field_names, **active_component_names;

        PetscCall(RatelMaterialGetActiveFieldNames(ratel->materials[0], &active_field_names, &active_component_names));
        PetscCall(DMClone(dm_solution, &dm));
        PetscCall(DMSetMatrixPreallocateSkip(dm, PETSC_TRUE));
        PetscCall(RatelDMSetupByOrder_FEM(ratel, PETSC_TRUE, PETSC_FALSE, field_orders, RATEL_DECIDE, PETSC_FALSE, RATEL_DECIDE, num_active_fields,
                                          active_field_sizes, active_field_names, active_component_names, dm));
        PetscCall(DMCreateFieldDecomposition(dm, NULL, NULL, NULL, &sub_dms));
        PetscCall(DMPlexIsSimplex(dm, &is_simplex));
        if (!is_simplex) {
          for (PetscInt f = 0; f < num_active_fields; f++) {
            PetscCall(DMPlexSetClosurePermutationTensor(sub_dms[f], PETSC_DETERMINE, NULL));
          }
        }
        for (PetscInt f = 0; f < num_active_fields; f++) PetscCall(DMSetVecType(sub_dms[f], vec_type));
        dm_hierarchy[i] = sub_dms[pmg_field];
        PetscCall(PetscObjectReference((PetscObject)dm_hierarchy[i]));
        // Clean up sub dms
        for (PetscInt f = 0; f < num_active_fields; f++) PetscCall(DMDestroy(&sub_dms[f]));
        PetscCall(PetscFree(sub_dms));
        PetscCall(DMDestroy(&dm));
      }
      break;
    }
    // LCOV_EXCL_START
    case RATEL_METHOD_MPM:
      SETERRQ(PETSC_COMM_SELF, PETSC_ERR_SUP, "MPM not supported for multigrid");
      break;
    default:
      SETERRQ(PETSC_COMM_SELF, PETSC_ERR_SUP, "Numerical method not supported for multigrid");
      // LCOV_EXCL_STOP
  }

  // libCEED operators for each level
  PetscCall(RatelDebug(ratel, "---- Setup libCEED operators"));
  for (PetscInt i = fine_level - 1; i >= 0; i--) {
    PetscCall(RatelDebug(ratel, "------ Multigrid level: %" CeedInt_FMT, i));
    CeedOperator op_jacobian, op_prolong, op_restrict;

    // -- Vectors
    PetscCall(DMCreateLocalVector(dm_hierarchy[i], &X_loc[i]));
    // -- Composite operators
    RatelCallCeed(ratel, CeedCompositeOperatorCreate(ratel->ceed, &op_jacobian));
    {
      char op_name_jacobian[25];

      PetscCall(PetscSNPrintf(op_name_jacobian, sizeof(op_name_jacobian), "Jacobian %" PetscInt_FMT, i));
      RatelCallCeed(ratel, CeedOperatorSetName(op_jacobian, op_name_jacobian));
    }
    RatelCallCeed(ratel, CeedCompositeOperatorCreate(ratel->ceed, &op_prolong));
    {
      char op_name_prolong[25];

      PetscCall(PetscSNPrintf(op_name_prolong, sizeof(op_name_prolong), "prolongation %" PetscInt_FMT, i + 1));
      RatelCallCeed(ratel, CeedOperatorSetName(op_prolong, op_name_prolong));
    }
    RatelCallCeed(ratel, CeedCompositeOperatorCreate(ratel->ceed, &op_restrict));
    {
      char op_name_restrict[25];

      PetscCall(PetscSNPrintf(op_name_restrict, sizeof(op_name_restrict), "restriction %" PetscInt_FMT, i + 1));
      RatelCallCeed(ratel, CeedOperatorSetName(op_restrict, op_name_restrict));
    }
    // -- Compute Multiplicity
    {
      PetscCall(RatelDebug(ratel, "-------- Compute multiplicity"));
      Vec X;

      PetscCall(DMGetGlobalVector(dm_hierarchy[i + 1], &X));
      PetscCall(VecZeroEntries(X));
      PetscCall(VecSet(X_loc[i + 1], 1.0));
      PetscCall(DMLocalToGlobal(dm_hierarchy[i + 1], X_loc[i + 1], ADD_VALUES, X));
      PetscCall(DMGlobalToLocal(dm_hierarchy[i + 1], X, INSERT_VALUES, X_loc[i + 1]));
      PetscCall(DMRestoreGlobalVector(dm_hierarchy[i + 1], &X));
    }
    // -- Sub-operators for volumetric terms
    {
      CeedOperator op_jacobian_fine;

      PetscCall(MatCeedGetCeedOperators(pmg->mats_operator[i + 1], &op_jacobian_fine, NULL));
      PetscCall(RatelSetupMultigridLevel(ratel, dm_hierarchy[i], X_loc[i + 1], i, op_jacobian_fine, op_jacobian, op_prolong, op_restrict));
      PetscCall(MatCeedRestoreCeedOperators(pmg->mats_operator[i + 1], &op_jacobian_fine, NULL));
    }
    // -- Clean up multiplicity values in local vectors
    PetscCall(VecZeroEntries(X_loc[i + 1]));

    // -- MatShells
    {
      char          name[PETSC_MAX_PATH_LEN];
      PetscLogEvent log_event_mult, log_event_mult_transpose;

      PetscCall(MatCeedGetLogEvents(pmg->mats_operator[i + 1], &log_event_mult, &log_event_mult_transpose));
      PetscCall(MatCreateCeed(dm_hierarchy[i], NULL, op_jacobian, NULL, &pmg->mats_operator[i]));
      PetscCall(MatCeedSetLogEvents(pmg->mats_operator[i], log_event_mult, log_event_mult_transpose));
      PetscCall(MatCeedSetLocalVectors(pmg->mats_operator[i], X_loc[i], NULL));
      PetscCall(PetscSNPrintf(name, PETSC_MAX_PATH_LEN, "Jacobian, pMG level %" PetscInt_FMT, i));
      PetscCall(PetscObjectSetName((PetscObject)pmg->mats_operator[i], name));

      PetscCall(MatCreateCeed(dm_hierarchy[i], dm_hierarchy[i + 1], op_prolong, op_restrict, &pmg->mats_prolong_restrict[i + 1]));
      PetscCall(MatCeedSetLogEvents(pmg->mats_prolong_restrict[i + 1], RATEL_Prolong[i], RATEL_Restrict[i]));
      PetscCall(MatCeedSetCeedOperatorLogEvents(pmg->mats_prolong_restrict[i + 1], RATEL_Prolong_CeedOp[i], RATEL_Restrict_CeedOp[i]));
      PetscCall(MatCeedSetLocalVectors(pmg->mats_prolong_restrict[i + 1], X_loc[i], X_loc[i + 1]));
      PetscCall(PetscSNPrintf(name, PETSC_MAX_PATH_LEN, "Prolongation and restriction, pMG level %" PetscInt_FMT, i));
      PetscCall(PetscObjectSetName((PetscObject)pmg->mats_prolong_restrict[i + 1], name));
    }

    // -- Cleanup
    RatelCallCeed(ratel, CeedOperatorDestroy(&op_jacobian));
    RatelCallCeed(ratel, CeedOperatorDestroy(&op_prolong));
    RatelCallCeed(ratel, CeedOperatorDestroy(&op_restrict));
  }

  // Coarse Jacobian
  {
    // -- Coarse Mat
    {
      PetscBool is_mat_type_cl = PETSC_FALSE;
      char      mat_type_cl[25];
      VecType   vec_type;
      MatType   mat_type = MATAIJ;

      PetscCall(DMGetVecType(dm_solution, &vec_type));
      if (strstr(vec_type, VECCUDA)) mat_type = MATAIJCUSPARSE;
      else if (strstr(vec_type, VECKOKKOS)) mat_type = MATAIJKOKKOS;
      else mat_type = MATAIJ;

      PetscOptionsBegin(ratel->comm, NULL, "", NULL);
      PetscCall(
          PetscOptionsString("-coarse_dm_mat_type", "coarse grid DM MatType", NULL, mat_type_cl, mat_type_cl, sizeof(mat_type_cl), &is_mat_type_cl));
      PetscOptionsEnd();

      PetscCall(DMSetMatType(dm_hierarchy[0], is_mat_type_cl ? mat_type_cl : mat_type));
      PetscCall(DMSetMatrixPreallocateSkip(dm_hierarchy[0], PETSC_TRUE));
      PetscCall(DMSetOptionsPrefix(dm_hierarchy[0], "coarse_"));

      PetscCall(DMGetMatType(dm_hierarchy[0], &mat_type));
      PetscCall(RatelDebug(ratel, "------ Coarse DM MatType: %s", mat_type));
      PetscCall(PetscObjectSetName((PetscObject)dm_hierarchy[0], "Coarse Grid"));
      PetscCall(DMViewFromOptions(dm_hierarchy[0], NULL, "-dm_view"));
    }

    // -- Assemble sparsity pattern
    {
      CeedInt        num_active_fields;
      const CeedInt *active_field_sizes;

      PetscCall(DMCreateMatrix(dm_hierarchy[0], &pmg->mat_operator_coarse));
      PetscCall(MatSetOption(pmg->mat_operator_coarse, MAT_SPD, ratel->is_spd));
      PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[0], &num_active_fields, &active_field_sizes));
      if (active_field_sizes[pmg_field] >= 3) {
        MatNullSpace near_null;

        PetscCall(DMPlexCreateRigidBody(dm_hierarchy[0], pmg_field, &near_null));
        PetscCall(MatSetNearNullSpace(pmg->mat_operator_coarse, near_null));
        PetscCall(MatNullSpaceDestroy(&near_null));
      }
    }
  }

  // PC setup
  PetscCall(RatelDebug(ratel, "---- Setup PCMG"));
  {
    PC pc_mg;

    // ---- P-multigrid with PCMG
    PetscCall(RatelDebug(ratel, "------ Full p-multigrid"));
    PetscCall(PCCreate((PetscObjectComm((PetscObject)pc)), &pc_mg));
    PetscCall(PCSetType(pc_mg, PCMG));
    {
      const char *prefix;

      PetscCall(PCGetOptionsPrefix(pc, &prefix));
      PetscCall(PCAppendOptionsPrefix(pc_mg, prefix));
    }
    PetscCall(PCMGSetLevels(pc_mg, pmg->num_multigrid_levels, NULL));
    // ------ Loop over levels
    for (PetscInt i = 0; i < pmg->num_multigrid_levels; i++) {
      PetscCall(RatelDebug(ratel, "-------- Setup PCMG level: %" PetscInt_FMT, i));
      // -------- Smoother
      PC  pc_smoother;
      KSP ksp_smoother;

      // ---------- Smoother KSP
      PetscCall(PCMGGetSmoother(pc_mg, i, &ksp_smoother));
      PetscCall(KSPSetDM(ksp_smoother, dm_hierarchy[i]));
      PetscCall(KSPSetDMActive(ksp_smoother, PETSC_FALSE));
      // ---------- Chebyshev options
      PetscCall(KSPSetType(ksp_smoother, KSPCHEBYSHEV));
      PetscCall(KSPChebyshevEstEigSet(ksp_smoother, 0, 0.1, 0, 1.1));
      PetscCall(KSPChebyshevEstEigSetUseNoisy(ksp_smoother, PETSC_TRUE));
      PetscCall(KSPSetOperators(ksp_smoother, pmg->mats_operator[i], pmg->mats_operator[i]));
      // ---------- Smoother preconditioner
      PetscCall(KSPGetPC(ksp_smoother, &pc_smoother));
      PetscCall(PCSetType(pc_smoother, PCJACOBI));
      PetscCall(PCJacobiSetType(pc_smoother, PC_JACOBI_DIAGONAL));
      // -------- Level prolongation/restriction operator
      if (i > 0) {
        PetscCall(PCMGSetInterpolation(pc_mg, i, pmg->mats_prolong_restrict[i]));
        PetscCall(PCMGSetRestriction(pc_mg, i, pmg->mats_prolong_restrict[i]));
      }
    }
    // ------ PCMG coarse solve
    PetscCall(RatelDebug(ratel, "-------- Setup PCMG coarse solve"));
    PC  pc_coarse;
    KSP ksp_coarse;

    // -------- Coarse KSP
    PetscCall(PCMGGetCoarseSolve(pc_mg, &ksp_coarse));
    PetscCall(KSPSetType(ksp_coarse, KSPPREONLY));
    PetscCall(KSPSetOperators(ksp_coarse, pmg->mat_operator_coarse, pmg->mat_operator_coarse));
    // -------- Coarse preconditioner
    PetscCall(KSPGetPC(ksp_coarse, &pc_coarse));
    PetscCall(PCSetType(pc_coarse, PCGAMG));
    // ------ PCMG options
    PetscCall(PCMGSetType(pc_mg, PC_MG_MULTIPLICATIVE));
    PetscCall(PCMGSetNumberSmooth(pc_mg, 3));
    PetscCall(PCMGSetCycleType(pc_mg, PC_MG_CYCLE_V));

    pmg->pc_mg = pc_mg;
  }

  // Setup shell
  PetscCall(PCSetType(pc, PCSHELL));
  PetscCall(PCShellSetName(pc, RATEL_PCPMG));
  PetscCall(PCShellSetContext(pc, pmg));
  PetscCall(PCShellSetSetUp(pc, RatelPCSetUp_PMG));
  PetscCall(PCShellSetView(pc, RatelPCView_PMG));
  PetscCall(PCShellSetApply(pc, RatelPCApply_PMG));
  PetscCall(PCShellSetApplyTranspose(pc, RatelPCApplyTranspose_PMG));
  PetscCall(PCShellSetMatApply(pc, RatelPCMatApply_PMG));
  PetscCall(PCShellSetDestroy(pc, RatelPCDestroy_PMG));
  PetscCall(PCViewFromOptions(pc, NULL, "-pc_view"));

  // Cleanup
  PetscCall(DMDestroy(&dm_solution));
  for (PetscInt i = 0; i < pmg->num_multigrid_levels; i++) {
    PetscCall(VecDestroy(&X_loc[i]));
    PetscCall(DMDestroy(&dm_hierarchy[i]));
  }
  PetscCall(PetscFree(X_loc));
  PetscCall(PetscFree(dm_hierarchy));

  PetscCall(PetscLogStagePop());
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel PC pMG Create Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief SetUp for `PCpMG`. Reassemble coarse operator.

  Collective across MPI processes.

  @param[in,out]  pc   `PC` object to setup

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelPCSetUp_PMG(PC pc) {
  DM              dm;
  Ratel           ratel;
  RatelPMGContext pmg;

  PetscFunctionBeginUser;
  PetscCall(PCShellGetContext(pc, (void *)&pmg));
  PetscCall(PCGetDM(pc, &dm));
  PetscCall(DMGetApplicationContext(dm, (void *)&ratel));
  PetscCall(PetscLogEventBegin(PCPMG_Setup, pc, NULL, NULL, NULL));

  // Check mat state
  {
    PetscObjectState A_mat_state;
    Mat              A_mat;

    PetscCall(PCGetOperators(pc, &A_mat, NULL));
    PetscCall(MatGetState(A_mat, &A_mat_state));
    if (A_mat_state == pmg->fine_mat_state) {
      PetscCall(PetscLogEventEnd(PCPMG_Setup, pc, NULL, NULL, NULL));
      PetscFunctionReturn(PETSC_SUCCESS);
    }
    pmg->fine_mat_state = A_mat_state;
  }

  // Mats on levels
  for (PetscInt i = 1; i < pmg->num_multigrid_levels - 1; i++) {
    PetscCall(MatAssemblyBegin(pmg->mats_operator[i], MAT_FINAL_ASSEMBLY));
    PetscCall(MatAssemblyEnd(pmg->mats_operator[i], MAT_FINAL_ASSEMBLY));
    PetscCall(MatSetOption(pmg->mats_operator[i], MAT_SPD, ratel->is_spd));
  }

  // Coarse mat
  PetscCall(MatCeedAssembleCOO(pmg->mats_operator[0], pmg->mat_operator_coarse));
  PetscCall(MatSetOption(pmg->mat_operator_coarse, MAT_SPD, ratel->is_spd));

  // Inner PCMG
  {
    Mat A_mat, P_mat;

    PetscCall(PCGetOperators(pc, &A_mat, &P_mat));
    PetscCall(PCSetOperators(pmg->pc_mg, A_mat, P_mat));
    PetscCall(PCSetUp(pmg->pc_mg));
  }
  PetscCall(PetscLogEventEnd(PCPMG_Setup, pc, NULL, NULL, NULL));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief View `PCpMG`.

  Collective across MPI processes.

  @param[in]      pc      `PC` object to view
  @param[in,out]  viewer  Visualization context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelPCView_PMG(PC pc, PetscViewer viewer) {
  RatelPMGContext pmg;

  PetscFunctionBeginUser;
  PetscCall(PCShellGetContext(pc, (void **)&pmg));
  PetscCall(PCView(pmg->pc_mg, viewer));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Apply `PCpMG`.

  Collective across MPI processes.

  @param[in]   pc     `PC` object to apply
  @param[in]   X_in   Input vector
  @param[out]  X_out  Output vector

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelPCApply_PMG(PC pc, Vec X_in, Vec X_out) {
  RatelPMGContext pmg;

  PetscFunctionBeginUser;
  PetscCall(PCShellGetContext(pc, (void **)&pmg));
  PetscCall(PCApply(pmg->pc_mg, X_in, X_out));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Apply `PCpMG` transpose.

  Collective across MPI processes.

  @param[in]   pc     `PC` object to apply
  @param[in]   X_in   Input vector
  @param[out]  X_out  Output vector

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelPCApplyTranspose_PMG(PC pc, Vec X_in, Vec X_out) {
  RatelPMGContext pmg;

  PetscFunctionBeginUser;
  PetscCall(PCShellGetContext(pc, (void **)&pmg));
  PetscCall(PCApplyTranspose(pmg->pc_mg, X_in, X_out));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Apply `PCpMG` to multiple vectors stored as `MATDENSE`.

  Collective across MPI processes.

  @param[in]   pc     `PC` object to apply
  @param[in]   X_in   Input matrix
  @param[out]  X_out  Output matrix

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelPCMatApply_PMG(PC pc, Mat X_in, Mat X_out) {
  RatelPMGContext pmg;

  PetscFunctionBeginUser;
  PetscCall(PCShellGetContext(pc, (void **)&pmg));
  PetscCall(PCMatApply(pmg->pc_mg, X_in, X_out));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Destroy `PCpMG` context data.

  Collective across MPI processes.

  @param[in,out]  pc  `PC` object to destroy

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelPCDestroy_PMG(PC pc) {
  RatelPMGContext pmg;

  PetscFunctionBeginUser;
  PetscCall(PCShellGetContext(pc, (void **)&pmg));
  PetscCall(RatelPCPMGContextDestroy(pmg));
  PetscCall(PCShellSetContext(pc, NULL));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Destroy Ratel PMG preconditioner

  @param[in,out]  pmg  Ratel pMG context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelPCPMGContextDestroy(RatelPMGContext pmg) {
  Ratel ratel;

  PetscFunctionBeginUser;
  if (!pmg) PetscFunctionReturn(PETSC_SUCCESS);

  ratel = pmg->ratel;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel PC Destroy MG"));

  // PCMG
  PetscCall(PCDestroy(&pmg->pc_mg));

  // Mats
  for (PetscInt i = 0; i < pmg->num_multigrid_levels; i++) {
    PetscCall(MatDestroy(&pmg->mats_operator[i]));
    PetscCall(MatDestroy(&pmg->mats_prolong_restrict[i]));
  }
  PetscCall(MatDestroy(&pmg->mat_operator_coarse));
  PetscCall(PetscFree(pmg->mats_operator));
  PetscCall(PetscFree(pmg->mats_prolong_restrict));

  // Context itself
  PetscCall(PetscFree(pmg));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel PC Destroy PMG Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Register `PCpMG` preconditioner.

  Not collective across MPI processes.

  @param[in]  ratel  `Ratel` context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelPCRegisterPMG(Ratel ratel) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel PC Register PMG"));

  PetscCall(PCRegister(RATEL_PCPMG, RatelPCPMGCreate));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel PC Register PMG Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}
