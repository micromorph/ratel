/// @file
/// CeedBoundaryEvaluator implementation

#include <ceed-boundary-evaluator-impl.h>
#include <ceed-boundary-evaluator.h>
#include <ceed.h>
#include <ceed/backend.h>
#include <petsc-ceed-utils.h>
#include <petsc-ceed.h>
#include <petsc/private/petscimpl.h>
#include <stdio.h>

PetscClassId  CEED_BOUNDARYEVALUATOR_CLASSID;
PetscLogEvent CEED_BOUNDARYEVALUATOR_APPLY, CEED_BOUNDARYEVALUATOR_APPLY_CEEDOP;

/**
  @brief Initialize `CeedBoundaryEvaluator` class.

  Not collective across MPI Processes.

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode CeedBoundaryEvaluatorInitialize() {
  static PetscBool initalized = PETSC_FALSE;

  PetscFunctionBeginUser;
  if (initalized) PetscFunctionReturn(PETSC_SUCCESS);
  PetscCall(PetscClassIdRegister("CeedBdyEvaluator", &CEED_BOUNDARYEVALUATOR_CLASSID));
  PetscCall(PetscLogEventRegister("CeedBEvalApp", CEED_BOUNDARYEVALUATOR_CLASSID, &CEED_BOUNDARYEVALUATOR_APPLY));
  PetscCall(PetscLogEventRegister("CeedBEvalAppCeed", CEED_BOUNDARYEVALUATOR_CLASSID, &CEED_BOUNDARYEVALUATOR_APPLY_CEEDOP));
  initalized = PETSC_TRUE;
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Create a `CeedBoundaryEvaluator` object which computes Dirichlet boundary values via `CeedOperator`.

  Not collective across MPI processes.

  @param[in]   comm          The communicator for the `CeedBoundaryEvaluator` object
  @param[in]   op_dirichlet  `CeedOperator` to compute essential boundary values
  @param[out]  evaluator     `CeedBoundaryEvaluator` object

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedBoundaryEvaluatorCreate(MPI_Comm comm, CeedOperator op_dirichlet, CeedBoundaryEvaluator *evaluator) {
  PetscFunctionBeginUser;
  PetscAssertPointer(evaluator, 3);
  PetscCall(CeedBoundaryEvaluatorInitialize());
  // PETSc header
  {
    CeedBoundaryEvaluator evaluator_;

    PetscCall(PetscHeaderCreate(evaluator_, CEED_BOUNDARYEVALUATOR_CLASSID, "CeedBoundaryEvaluator", "CeedBoundaryEvaluator", "CeedBoundaryEvaluator",
                                comm, CeedBoundaryEvaluatorDestroy, CeedBoundaryEvaluatorView));
    *evaluator = evaluator_;
  }
  // libCEED objects
  (*evaluator)->ceed = CeedOperatorReturnCeed(op_dirichlet);
  PetscCallCeed((*evaluator)->ceed, CeedReference((*evaluator)->ceed));
  PetscCallCeed((*evaluator)->ceed, CeedOperatorReferenceCopy(op_dirichlet, &(*evaluator)->op_dirichlet));
  PetscCallCeed((*evaluator)->ceed, CeedOperatorGetContextFieldLabel(op_dirichlet, "time", &(*evaluator)->time_field));
  PetscCallCeed((*evaluator)->ceed, CeedOperatorGetContextFieldLabel(op_dirichlet, "dt", &(*evaluator)->dt_field));
  // Cached values
  (*evaluator)->time = RATEL_UNSET_INITIAL_TIME;
  (*evaluator)->dt   = NAN;
  // Log event
  PetscCall(CeedBoundaryEvaluatorSetLogEvent(*evaluator, CEED_BOUNDARYEVALUATOR_APPLY));
  PetscCall(CeedBoundaryEvaluatorSetCeedOperatorLogEvent(*evaluator, CEED_BOUNDARYEVALUATOR_APPLY_CEEDOP));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief View a `CeedBoundaryEvaluator` object.

  Not collective across MPI processes.

  @param[in]  evaluator  `CeedBoundaryEvaluator` object
  @param[in]  viewer     Optional `PetscViewer` context or `NULL`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedBoundaryEvaluatorView(CeedBoundaryEvaluator evaluator, PetscViewer viewer) {
  PetscBool         is_ascii;
  PetscViewerFormat format;
  PetscMPIInt       size, rank;

  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator, CEED_BOUNDARYEVALUATOR_CLASSID, 1);
  PetscValidHeaderSpecific(viewer, PETSC_VIEWER_CLASSID, 2);
  if (!viewer) PetscCall(PetscViewerASCIIGetStdout(PetscObjectComm((PetscObject)evaluator), &viewer));

  PetscCall(PetscViewerGetFormat(viewer, &format));
  PetscCallMPI(MPI_Comm_size(PetscObjectComm((PetscObject)evaluator), &size));
  if (size == 1 && format == PETSC_VIEWER_LOAD_BALANCE) PetscFunctionReturn(PETSC_SUCCESS);

  PetscCallMPI(MPI_Comm_rank(PetscObjectComm((PetscObject)evaluator), &rank));
  if (rank != 0) PetscFunctionReturn(PETSC_SUCCESS);

  PetscCall(PetscObjectTypeCompare((PetscObject)viewer, PETSCVIEWERASCII, &is_ascii));
  {
    PetscBool is_detailed     = format == PETSC_VIEWER_ASCII_INFO_DETAIL;
    char      rank_string[16] = {'\0'};
    FILE     *file;

    PetscCall(PetscViewerASCIIPrintf(viewer, "libCEED Operator Essential Boundary Value Evaluator\n"));
    PetscCall(PetscSNPrintf(rank_string, 16, "on Rank %d", rank));
    PetscCall(PetscViewerASCIIPushTab(viewer));  // BoundaryEvaluator
    PetscCall(PetscViewerASCIIPrintf(viewer, "CeedOperator %s:\n", is_detailed ? rank_string : "Summary"));
    PetscCall(PetscViewerASCIIGetPointer(viewer, &file));
    PetscCall(PetscViewerASCIIPushTab(viewer));  // CeedOperator
    if (is_detailed) PetscCallCeed(evaluator->ceed, CeedOperatorView(evaluator->op_dirichlet, file));
    else PetscCallCeed(evaluator->ceed, CeedOperatorViewTerse(evaluator->op_dirichlet, file));
    PetscCall(PetscViewerASCIIPopTab(viewer));  // CeedOperator
    PetscCall(PetscViewerASCIIPopTab(viewer));  // BoundaryEvaluator
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Destroy a `CeedBoundaryEvaluator` object.

  Not collective across MPI processes.

  @param[in,out]  evaluator  `CeedBoundaryEvaluator` object

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedBoundaryEvaluatorDestroy(CeedBoundaryEvaluator *evaluator) {
  PetscFunctionBeginUser;
  PetscAssertPointer(evaluator, 1);
  if (!*evaluator) PetscFunctionReturn(PETSC_SUCCESS);
  PetscValidHeaderSpecific(*evaluator, CEED_BOUNDARYEVALUATOR_CLASSID, 1);
  if (--((PetscObject)*evaluator)->refct > 0) {
    *evaluator = NULL;
    PetscFunctionReturn(PETSC_SUCCESS);
  }
  PetscCallCeed((*evaluator)->ceed, CeedVectorDestroy(&(*evaluator)->u_loc));
  PetscCallCeed((*evaluator)->ceed, CeedOperatorDestroy(&(*evaluator)->op_dirichlet));
  PetscCheck(CeedDestroy(&(*evaluator)->ceed) == CEED_ERROR_SUCCESS, PETSC_COMM_SELF, PETSC_ERR_LIB, "destroying libCEED context object failed");
  PetscCall(PetscHeaderDestroy(evaluator));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Set the current time for a `CeedBoundaryEvaluator` object.

  Not collective across MPI processes.

  @note Updates the cached value and the context field value, if it exists.

  @param[in,out]  evaluator  `CeedBoundaryEvaluator` object
  @param[in]      time       Current time

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedBoundaryEvaluatorSetTime(CeedBoundaryEvaluator evaluator, PetscReal time) {
  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator, CEED_BOUNDARYEVALUATOR_CLASSID, 1);
  // Update cached value
  evaluator->time = time;

  if (evaluator->time_field) {
    // Update contexts with cached value
    PetscCallCeed(evaluator->ceed, CeedOperatorSetContextDouble(evaluator->op_dirichlet, evaluator->time_field, &evaluator->time));
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get the current time for a `CeedBoundaryEvaluator` object.

  Not collective across MPI processes.

  @note Updates the cached value in the `CeedEvaluator` object using the context field value, if it exists.

  @param[in]   evaluator  `CeedBoundaryEvaluator` object
  @param[out]  time       Current time from time field or last cached value if no time field exists, or RATEL_UNSET_INITIAL_TIME if not set

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedBoundaryEvaluatorGetTime(CeedBoundaryEvaluator evaluator, PetscReal *time) {
  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator, CEED_BOUNDARYEVALUATOR_CLASSID, 1);
  PetscAssertPointer(time, 2);
  if (evaluator->time_field) {
    size_t        num_values;
    const double *time_ceed;

    PetscCallCeed(evaluator->ceed, CeedOperatorGetContextDoubleRead(evaluator->op_dirichlet, evaluator->time_field, &num_values, &time_ceed));
    // Update cached value
    evaluator->time = time_ceed[0];
    PetscCallCeed(evaluator->ceed, CeedOperatorRestoreContextDoubleRead(evaluator->op_dirichlet, evaluator->time_field, &time_ceed));
  }
  // Return the time from context, or the last time passed to CeedBoundaryEvaluatorSetTime if no time field
  *time = evaluator->time;
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Set the current time step for a `CeedBoundaryEvaluator` object.

  Not collective across MPI processes.

  @note Updates the cached value and the context field value, if it exists.

  @param[in,out]  evaluator  `CeedBoundaryEvaluator` object
  @param[in]      dt         Current dt

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedBoundaryEvaluatorSetDt(CeedBoundaryEvaluator evaluator, PetscReal dt) {
  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator, CEED_BOUNDARYEVALUATOR_CLASSID, 1);
  // Update cached value
  evaluator->dt = dt;

  if (evaluator->dt_field) {
    // Update contexts with cached value
    PetscCallCeed(evaluator->ceed, CeedOperatorSetContextDouble(evaluator->op_dirichlet, evaluator->dt_field, &evaluator->dt));
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get the current time step for a `CeedBoundaryEvaluator` object.

  Not collective across MPI processes.

  @note Updates the cached value in the `CeedEvaluator` object using the context field value, if it exists.

  @param[in]   evaluator  `CeedBoundaryEvaluator` object
  @param[out]  dt         Current dt from dt field or last cached value if no dt field exists, or NAN if not set

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedBoundaryEvaluatorGetDt(CeedBoundaryEvaluator evaluator, PetscReal *dt) {
  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator, CEED_BOUNDARYEVALUATOR_CLASSID, 1);
  PetscAssertPointer(dt, 2);
  // Get the current dt from the context
  if (evaluator->dt_field) {
    size_t        num_values;
    const double *dt_ceed;

    PetscCallCeed(evaluator->ceed, CeedOperatorGetContextDoubleRead(evaluator->op_dirichlet, evaluator->dt_field, &num_values, &dt_ceed));
    // Update cached value
    evaluator->dt = dt_ceed[0];
    PetscCallCeed(evaluator->ceed, CeedOperatorRestoreContextDoubleRead(evaluator->op_dirichlet, evaluator->dt_field, &dt_ceed));
  }
  // Return the dt from context, or the last dt passed to CeedBoundaryEvaluatorSetDt if no dt field
  *dt = evaluator->dt;
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Set `PetscLogEvent` for `CeedBoundaryEvaluator`.

  Not collective across MPI processes.

  @param[in,out]  evaluator  `CeedBoundaryEvaluator`
  @param[out]     log_event  `PetscLogEvent` for evaluation

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedBoundaryEvaluatorSetLogEvent(CeedBoundaryEvaluator evaluator, PetscLogEvent log_event) {
  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator, CEED_BOUNDARYEVALUATOR_CLASSID, 1);
  evaluator->log_event = log_event;
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get `PetscLogEvent` for `CeedEvaluator`.

  Not collective across MPI processes.

  @param[in,out]  evaluator  `CeedEvaluator`
  @param[out]     log_event  `PetscLogEvent` for evaluation

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedBoundaryEvaluatorGetLogEvent(CeedBoundaryEvaluator evaluator, PetscLogEvent *log_event) {
  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator, CEED_BOUNDARYEVALUATOR_CLASSID, 1);
  *log_event = evaluator->log_event;
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Set `CeedOperator` `PetscLogEvent` for `CeedBoundaryEvaluator`.

  Not collective across MPI processes.

  @param[in,out]  evaluator  `CeedBoundaryEvaluator`
  @param[out]     log_event  `PetscLogEvent` for `CeedOperator` evaluation

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedBoundaryEvaluatorSetCeedOperatorLogEvent(CeedBoundaryEvaluator evaluator, PetscLogEvent log_event) {
  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator, CEED_BOUNDARYEVALUATOR_CLASSID, 1);
  evaluator->log_event_ceed = log_event;
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get `CeedOperator` `PetscLogEvent` for `CeedBoundaryEvaluator`.

  Not collective across MPI processes.

  @param[in,out]  evaluator  `CeedBoundaryEvaluator`
  @param[out]     log_event  `PetscLogEvent` for `CeedOperator` evaluation

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedBoundaryEvaluatorGetCeedOperatorLogEvent(CeedBoundaryEvaluator evaluator, PetscLogEvent *log_event) {
  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator, CEED_BOUNDARYEVALUATOR_CLASSID, 1);
  *log_event = evaluator->log_event_ceed;
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Set `CeedBoundaryEvaluator` to computes Dirichlet boundary values for the `DMPlex`.

  Collective across MPI processes.

  @param[in,out]  dm         `DMPlex` to update the Dirichlet boundary value calculation for
  @param[in]      evaluator  `CeedBoundaryEvaluator` object

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode DMPlexSetCeedBoundaryEvaluator(DM dm, CeedBoundaryEvaluator evaluator) {
  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(dm, DM_CLASSID, 1);
  PetscValidHeaderSpecific(evaluator, CEED_BOUNDARYEVALUATOR_CLASSID, 2);
  PetscCall(PetscObjectCompose((PetscObject)dm, "CEED Boundary Evaluator U", (PetscObject)evaluator));
  // Create boundary mask
  {
    Vec boundary_mask, X;

    PetscCall(DMGetNamedLocalVector(dm, "boundary mask", &boundary_mask));
    PetscCall(VecZeroEntries(boundary_mask));
    PetscCall(DMGetGlobalVector(dm, &X));
    PetscCall(VecSet(X, 1.0));
    PetscCall(DMGlobalToLocal(dm, X, INSERT_VALUES, boundary_mask));
    PetscCall(DMRestoreNamedLocalVector(dm, "boundary mask", &boundary_mask));
    PetscCall(DMRestoreGlobalVector(dm, &X));
  }
  // Override DMPlexInsertBoundaryValues
  PetscCall(PetscObjectComposeFunction((PetscObject)dm, "DMPlexInsertBoundaryValues_C", DMPlexInsertBoundaryValues_Ceed));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Remove `CeedBoundaryEvaluator` for Dirichlet boundary values from the `DMPlex`.

  Collective across MPI processes.

  @param[in]  dm  `DMPlex` to remove the Dirichlet boundary value calculation from

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode DMPlexRemoveCeedBoundaryEvaluator(DM dm) {
  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(dm, DM_CLASSID, 1);
  PetscCall(PetscObjectCompose((PetscObject)dm, "CEED Boundary Evaluator U", NULL));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get the `CeedBoundaryEvaluator` for Dirichlet boundary values from the `DMPlex`.

  Collective across MPI processes.

  @param[in]   dm         `DMPlex` to get the Dirichlet boundary value calculation from
  @param[out]  evaluator  Pointer to the `CeedBoundaryEvaluator` object, or `NULL` if not set

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode DMPlexGetCeedBoundaryEvaluator(DM dm, CeedBoundaryEvaluator *evaluator) {
  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(dm, DM_CLASSID, 1);
  PetscAssertPointer(evaluator, 2);
  PetscCall(PetscObjectQuery((PetscObject)dm, "CEED Boundary Evaluator U", (PetscObject *)evaluator));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Compute the Dirichlet boundary values via `CeedOperator`.

  Collective across MPI processes.

  @param[in]   dm                 `DM` to insert boundary values on
  @param[in]   insert_essential   Boolean flag for Dirichlet or Neumann boundary conditions
  @param[out]  U_loc              Local vector to insert the boundary values into
  @param[in]   time               Current time
  @param[in]   face_geometry_FVM  Face geometry data for finite volume discretizations
  @param[in]   cell_geometry_FVM  Cell geometry data for finite volume discretizations
  @param[in]   grad_FVM           Gradient reconstruction data for finite volume discretizations

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode DMPlexInsertBoundaryValues_Ceed(DM dm, PetscBool insert_essential, Vec U_loc, PetscReal time, Vec face_geometry_FVM,
                                               Vec cell_geometry_FVM, Vec grad_FVM) {
  CeedBoundaryEvaluator evaluator;

  PetscFunctionBeginUser;
  // Get evaluator context
  PetscCall(PetscObjectQuery((PetscObject)dm, "CEED Boundary Evaluator U", (PetscObject *)&evaluator));

  if (!evaluator) PetscFunctionReturn(PETSC_SUCCESS);

  // Update time context information
  if (evaluator) PetscCall(CeedBoundaryEvaluatorSetTime(evaluator, time));

  // Update boundary values
  if (insert_essential) {
    Vec          boundary_mask;
    PetscMemType u_mem_type;

    // -- Mask Dirichlet entries
    PetscCall(PetscLogEventBegin(evaluator->log_event, dm, U_loc, NULL, NULL));
    PetscCall(DMGetNamedLocalVector(dm, "boundary mask", &boundary_mask));
    PetscCall(VecPointwiseMult(U_loc, U_loc, boundary_mask));
    PetscCall(DMRestoreNamedLocalVector(dm, "boundary mask", &boundary_mask));

    if (evaluator) {
      // -- Compute boundary values
      // Note: will be no-op if op has no suboperators
      if (!evaluator->u_loc) {
        PetscInt len_petsc;
        CeedSize len_ceed;

        PetscCall(VecGetSize(U_loc, &len_petsc));
        len_ceed = len_petsc;
        PetscCallCeed(evaluator->ceed, CeedVectorCreate(evaluator->ceed, len_ceed, &evaluator->u_loc));
      }
      PetscCall(VecPetscToCeed(U_loc, &u_mem_type, evaluator->u_loc));
      PetscCall(PetscLogEventBegin(evaluator->log_event_ceed, dm, U_loc, NULL, NULL));
      PetscCall(PetscLogGpuTimeBegin());
      PetscCallCeed(evaluator->ceed, CeedOperatorApplyAdd(evaluator->op_dirichlet, CEED_VECTOR_NONE, evaluator->u_loc, CEED_REQUEST_IMMEDIATE));
      PetscCall(PetscLogGpuTimeEnd());
      PetscCall(PetscLogEventEnd(evaluator->log_event_ceed, dm, U_loc, NULL, NULL));
      PetscCall(VecCeedToPetsc(evaluator->u_loc, u_mem_type, U_loc));
      PetscCall(PetscLogEventEnd(evaluator->log_event, dm, U_loc, NULL, NULL));
    }
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}
