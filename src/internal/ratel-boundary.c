/// @file
/// Boundary condition functions for Ratel

#include <ceed.h>
#include <math.h>
#include <petsc-ceed-utils.h>
#include <petscdmplex.h>
#include <ratel-boundary.h>
#include <ratel-dm.h>
#include <ratel-impl.h>
#include <ratel-material.h>
#include <ratel-utils.h>
#include <ratel.h>
#include <ratel/qfunctions/boundaries/clamp.h>
#include <ratel/qfunctions/boundaries/dirichlet.h>
#include <ratel/qfunctions/boundaries/platen-penalty.h>
#include <ratel/qfunctions/boundaries/pressure.h>
#include <ratel/qfunctions/boundaries/slip.h>
#include <ratel/qfunctions/boundaries/traction.h>
#include <ratel/qfunctions/geometry/surface-bounded.h>
#include <ratel/qfunctions/geometry/surface.h>
#include <ratel/qfunctions/mass.h>
#include <ratel/qfunctions/surface-centroid.h>
#include <stddef.h>
#include <stdio.h>

/// @addtogroup RatelBoundary
/// @{

// -----------------------------------------------------------------------------
// Setup DM
// -----------------------------------------------------------------------------

/**
  @brief Create boundary label.

  Not collective across MPI processes.

  @param[in,out]  dm    `DM` to add boundary label
  @param[in]      name  Name for new boundary label

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelCreateBCLabel(DM dm, const char name[]) {
  DMLabel label;

  PetscFunctionBeginUser;
  PetscCall(DMCreateLabel(dm, name));
  PetscCall(DMGetLabel(dm, name, &label));
  PetscCall(DMPlexMarkBoundaryFaces(dm, PETSC_DETERMINE, label));
  PetscCall(DMPlexLabelComplete(dm, label));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Add Dirichlet boundary conditions to `DM`.

  Collective across MPI processes.

  @param[in]   ratel  `Ratel` context
  @param[out]  dm     `DM` to update with Dirichlet boundaries

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDMAddBoundariesDirichlet(Ratel ratel, DM dm) {
  PetscBool   has_label;
  const char *name = "Face Sets";
  DMLabel     label;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMPlex Add Boundaries Dirichlet"));

  // DM label
  PetscCall(DMHasLabel(dm, name, &has_label));
  if (!has_label) PetscCall(RatelCreateBCLabel(dm, name));
  PetscCall(DMGetLabel(dm, name, &label));
  for (PetscInt i = 0; i < ratel->num_active_fields; i++) {
    PetscCall(RatelDebug(ratel, "---- Dirichlet boundaries for field %" PetscInt_FMT, i));

    // MMS specified BCs
    PetscCall(RatelDebug(ratel, "------ Dirichlet MMS boundaries"));
    for (PetscInt j = 0; j < ratel->bc_mms_count[i]; j++) {
      PetscBool    has_face;
      char         bc_name[PETSC_MAX_OPTION_NAME];
      const char **active_field_names;

      PetscCall(RatelDMHasFace(ratel, dm, ratel->bc_mms_faces[i][j], &has_face));
      PetscCheck(has_face, ratel->comm, PETSC_ERR_SUP, "Face %" PetscInt_FMT " does not exist.", ratel->bc_mms_faces[i][j]);

      PetscCall(RatelMaterialGetActiveFieldNames(ratel->materials[0], &active_field_names, NULL));
      PetscCall(PetscSNPrintf(bc_name, sizeof(bc_name), "mms_%s_face_%" PetscInt_FMT, active_field_names[i], ratel->bc_mms_faces[i][j]));
      PetscCall(DMAddBoundary(dm, DM_BC_ESSENTIAL, bc_name, label, 1, &ratel->bc_mms_faces[i][j], i, 0, NULL, NULL, NULL, NULL, NULL));
    }

    // User specified BCs
    PetscCall(RatelDebug(ratel, "------ Dirichlet clamp boundaries"));
    for (PetscInt j = 0; j < ratel->bc_clamp_count[i]; j++) {
      PetscBool    has_face;
      char         bc_name[PETSC_MAX_OPTION_NAME];
      const char **active_field_names;

      PetscCall(RatelDMHasFace(ratel, dm, ratel->bc_clamp_faces[i][j], &has_face));
      PetscCheck(has_face, ratel->comm, PETSC_ERR_SUP, "Face %" PetscInt_FMT " does not exist.", ratel->bc_clamp_faces[i][j]);

      PetscCall(RatelMaterialGetActiveFieldNames(ratel->materials[0], &active_field_names, NULL));
      PetscCall(PetscSNPrintf(bc_name, sizeof(bc_name), "clamp_%s_face_%" PetscInt_FMT, active_field_names[i], ratel->bc_clamp_faces[i][j]));
      PetscCall(DMAddBoundary(dm, DM_BC_ESSENTIAL, bc_name, label, 1, &ratel->bc_clamp_faces[i][j], i, 0, NULL, NULL, NULL, NULL, NULL));
    }
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMPlex Add Boundaries Dirichlet Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Add Dirichlet slip boundaries to `DM`.

  Collective across MPI processes.

  @param[in]   ratel  `Ratel` context
  @param[out]  dm     `DM` to update with dirichlet slip boundaries

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDMAddBoundariesSlip(Ratel ratel, DM dm) {
  PetscBool   has_label;
  const char *name = "Face Sets";
  DMLabel     label;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMPlex Add Boundaries Slip"));

  // Set slip BCs in each direction
  PetscCall(DMHasLabel(dm, name, &has_label));
  if (!has_label) PetscCall(RatelCreateBCLabel(dm, name));
  PetscCall(DMGetLabel(dm, name, &label));
  for (PetscInt i = 0; i < ratel->num_active_fields; i++) {
    PetscCall(RatelDebug(ratel, "---- Dirichlet boundaries for field %" PetscInt_FMT, i));
    for (PetscInt j = 0; j < ratel->bc_slip_count[i]; j++) {
      PetscBool    has_face;
      char         bc_name[PETSC_MAX_OPTION_NAME];
      const char **active_field_names;

      PetscCall(RatelDMHasFace(ratel, dm, ratel->bc_slip_faces[i][j], &has_face));
      PetscCheck(has_face, ratel->comm, PETSC_ERR_SUP, "Face %" PetscInt_FMT " does not exist.", ratel->bc_slip_faces[i][j]);

      PetscCall(RatelMaterialGetActiveFieldNames(ratel->materials[0], &active_field_names, NULL));
      PetscCall(PetscSNPrintf(bc_name, sizeof(bc_name), "slip_%s_face_%" PetscInt_FMT, active_field_names[i], ratel->bc_slip_faces[i][j]));
      PetscCall(DMAddBoundary(dm, DM_BC_ESSENTIAL, bc_name, label, 1, &ratel->bc_slip_faces[i][j], i, ratel->bc_slip_components_count[i][j],
                              ratel->bc_slip_components[i][j], (void (*)(void))NULL, NULL, NULL, NULL));
    }
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMPlex Add Boundaries Slip Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Read slip boundary conditions from options database

  Collective across MPI processes.

  @param[in]   ratel      `Ratel` context
  @param[in]   i          Field index
  @param[in]   j          Boundary index
  @param[out]  params_slip  Data structure to store slip data
*/
PetscErrorCode RatelBoundarySlipDataFromOptions(Ratel ratel, PetscInt i, PetscInt j, RatelBCSlipParams *params_slip) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Boundary Slip Data From Options"));

  // -- Initialize context data
  for (PetscInt i = 0; i < RATEL_MAX_BC_INTERP_POINTS; i++) {
    for (PetscInt j = 0; j < 4; j++) params_slip->translation[i * 4 + j] = 0.0;
    params_slip->times[i] = 1.0;
  }
  params_slip->num_times          = 1;
  params_slip->interpolation_type = RATEL_BC_INTERP_LINEAR;
  params_slip->components[0]      = ratel->bc_slip_components[i][j][0];
  params_slip->components[1]      = ratel->bc_slip_components[i][j][1];
  params_slip->components[2]      = ratel->bc_slip_components[i][j][2];
  params_slip->num_comp_slip      = ratel->bc_slip_components_count[i][j];
  params_slip->time               = RATEL_UNSET_INITIAL_TIME;
  params_slip->dt                 = 1.0;

  // Read CL options for slip conditions
  PetscOptionsBegin(ratel->comm, NULL, "", NULL);

  PetscBool times_set = PETSC_FALSE;
  // Get transition times
  {
    PetscCall(RatelDebug(ratel, "----- Reading transition times"));
    char     option_name[PETSC_MAX_OPTION_NAME];
    PetscInt max_num_times = RATEL_MAX_BC_INTERP_POINTS;
    PetscCall(PetscSNPrintf(option_name, sizeof(option_name), "-bc_slip_field_%" PetscInt_FMT "_face_%" PetscInt_FMT "_times", i,
                            ratel->bc_slip_faces[i][j]));
    PetscCall(PetscOptionsScalarArray(option_name, "Transition times for each translation for constrained face", NULL, params_slip->times,
                                      &max_num_times, &times_set));
    if (!times_set) {
      const char **active_field_names;

      PetscCall(RatelMaterialGetActiveFieldNames(ratel->materials[0], &active_field_names, NULL));
      PetscCall(PetscSNPrintf(option_name, sizeof(option_name), "-bc_slip_%s_face_%" PetscInt_FMT "_times", active_field_names[i],
                              ratel->bc_slip_faces[i][j]));
      max_num_times = 3 * RATEL_MAX_BC_INTERP_POINTS;
      PetscCall(PetscOptionsScalarArray(option_name, "Transition times for each translation for constrained face", NULL, params_slip->times,
                                        &max_num_times, &times_set));
    }
    if (i == 0 && !times_set) {
      PetscCall(PetscSNPrintf(option_name, sizeof(option_name), "-bc_slip_%" PetscInt_FMT "_times", ratel->bc_slip_faces[i][j]));
      max_num_times = 3 * RATEL_MAX_BC_INTERP_POINTS;
      PetscCall(PetscOptionsScalarArray(option_name, "Transition times for each translation for constrained face", NULL, params_slip->times,
                                        &max_num_times, &times_set));
    }
    if (times_set) {
      params_slip->num_times = max_num_times;
      // Ensure times are monotonically increasing
      PetscBool monotonic    = PETSC_TRUE;

      for (PetscInt j = 0; j < params_slip->num_times - 1; j++) {
        if (params_slip->times[j] >= params_slip->times[j + 1]) {
          monotonic = PETSC_FALSE;
        }
      }
      PetscCheck(monotonic, ratel->comm, PETSC_ERR_SUP, "Transition times must be monotonically increasing.");

      // Check bounds of times
      PetscCheck(params_slip->times[0] >= 0, ratel->comm, PETSC_ERR_SUP, "First time must be greater than or equal to 0.");
    }
  }

  // Translation vector
  PetscBool is_option_found = PETSC_FALSE;
  char      option_name[PETSC_MAX_OPTION_NAME];
  PetscInt  num_translations = params_slip->num_comp_slip * RATEL_MAX_BC_INTERP_POINTS;

  PetscCall(PetscSNPrintf(option_name, sizeof(option_name), "-bc_slip_field_%" PetscInt_FMT "_face_%" PetscInt_FMT "_translate", i,
                          ratel->bc_slip_faces[i][j]));
  PetscCall(PetscOptionsScalarArray(option_name, "Vector to translate constrained components end by", NULL, params_slip->translation,
                                    &num_translations, &is_option_found));
  if (!is_option_found) {
    const char **active_field_names;

    PetscCall(RatelMaterialGetActiveFieldNames(ratel->materials[0], &active_field_names, NULL));
    PetscCall(PetscSNPrintf(option_name, sizeof(option_name), "-bc_slip_%s_face_%" PetscInt_FMT "_translate", active_field_names[i],
                            ratel->bc_slip_faces[i][j]));
    num_translations = params_slip->num_comp_slip * RATEL_MAX_BC_INTERP_POINTS;
    PetscCall(PetscOptionsScalarArray(option_name, "Vector to translate constrained components end by", NULL, params_slip->translation,
                                      &num_translations, &is_option_found));
  }
  if (i == 0 && !is_option_found) {
    PetscCall(PetscSNPrintf(option_name, sizeof(option_name), "-bc_slip_%" PetscInt_FMT "_translate", ratel->bc_slip_faces[i][j]));
    num_translations = params_slip->num_comp_slip * RATEL_MAX_BC_INTERP_POINTS;
    PetscCall(PetscOptionsScalarArray(option_name, "Vector to translate constrained components end by", NULL, params_slip->translation,
                                      &num_translations, &is_option_found));
  }

  PetscCheck(!is_option_found || num_translations / params_slip->num_comp_slip == params_slip->num_times, ratel->comm, PETSC_ERR_SUP,
             "Must provide times for all translation vectors.");

  {
    char      option_name[PETSC_MAX_OPTION_NAME];
    PetscBool is_option_found = PETSC_FALSE;

    PetscCall(PetscSNPrintf(option_name, sizeof(option_name), "-bc_slip_field_%" PetscInt_FMT "_face_%" PetscInt_FMT "_interpolation", i,
                            ratel->bc_slip_faces[i][j]));
    PetscCall(PetscOptionsEnum(option_name, "Set interpolation type", NULL, RatelBCInterpolationTypesCL, (PetscEnum)params_slip->interpolation_type,
                               (PetscEnum *)&params_slip->interpolation_type, &is_option_found));
    if (!is_option_found) {
      const char **active_field_names;

      PetscCall(RatelMaterialGetActiveFieldNames(ratel->materials[0], &active_field_names, NULL));
      PetscCall(PetscSNPrintf(option_name, sizeof(option_name), "-bc_slip_%s_face_%" PetscInt_FMT "_interpolation", active_field_names[i],
                              ratel->bc_slip_faces[i][j]));
      PetscCall(PetscOptionsEnum(option_name, "Set interpolation type", NULL, RatelBCInterpolationTypesCL, (PetscEnum)params_slip->interpolation_type,
                                 (PetscEnum *)&params_slip->interpolation_type, &is_option_found));
    }
    if (i == 0 && !is_option_found) {
      PetscCall(PetscSNPrintf(option_name, sizeof(option_name), "-bc_slip_%" PetscInt_FMT "_interpolation", ratel->bc_slip_faces[i][j]));
      PetscCall(PetscOptionsEnum(option_name, "Set interpolation type", NULL, RatelBCInterpolationTypesCL, (PetscEnum)params_slip->interpolation_type,
                                 (PetscEnum *)&params_slip->interpolation_type, &is_option_found));
    }
  }
  PetscOptionsEnd();  // End of setting slip conditions

  // Get number of components in field
  {
    const CeedInt *field_sizes;

    PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[0], NULL, &field_sizes));
    params_slip->num_comp = field_sizes[i];
  }

  if (ratel->is_debug) {
    // LCOV_EXCL_START
    PetscCall(RatelDebug(ratel, "----   Slip Boundary %" PetscInt_FMT ": %" PetscInt_FMT " translation(s) with interpolation: %s",
                         ratel->bc_slip_faces[i][j], params_slip->num_times, RatelBCInterpolationTypes[params_slip->interpolation_type]));
    for (PetscInt t = 0; t < params_slip->num_times; t++) {
      char    translations[PETSC_MAX_PATH_LEN] = "";
      CeedInt head                             = 0;

      for (CeedInt k = 0; k < params_slip->num_comp_slip; k++) {
        PetscSizeT count     = 0;
        translations[head++] = 't';
        translations[head++] = 'x' + (char)params_slip->components[k];
        translations[head++] = ':';
        translations[head++] = ' ';
        PetscCall(PetscSNPrintfCount(&translations[head], 8, "%0.3f", &count, params_slip->translation[params_slip->num_comp_slip * t + k]));
        head += count - 1;
        if (k < params_slip->num_comp_slip - 1) {
          translations[head++] = ',';
          translations[head++] = ' ';
        }
      }
      translations[head] = '\0';
      PetscCall(RatelDebug(ratel, "------   t = %.3f: [%s]", params_slip->times[t], translations));
    }
    // LCOV_EXCL_STOP
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Boundary Slip Data From Options Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Add Dirichlet slip boundary conditions to Dirichlet boundary condition operator.

  Collective across MPI processes.

  @param[in]   ratel         `Ratel` context
  @param[in]   dm            `DM` to use for Dirichlet boundaries
  @param[in]   incremental   Set up operator to incrementally apply slip boundary conditions
  @param[out]  op_dirichlet  Composite Dirichlet `CeedOperator` to add sub-operators to

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelCeedAddBoundariesDirichletSlip(Ratel ratel, DM dm, PetscBool incremental, CeedOperator op_dirichlet) {
  PetscBool      has_dirichlet = PETSC_FALSE;
  PetscInt       dim;
  const PetscInt height_face = 1;
  Ceed           ceed        = ratel->ceed;
  CeedInt        elem_size;
  const CeedInt *field_sizes;
  const char   **field_names;
  CeedBasis      basis_x_face;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Ceed Add Boundaries Dirichlet Slip"));

  // Get dimension, number of solution components, and field names
  {
    PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[0], NULL, &field_sizes));
    PetscCall(RatelMaterialGetActiveFieldNames(ratel->materials[0], &field_names, NULL));
  }

  PetscCall(RatelDebug(ratel, "---- Setting up libCEED coordinate basis"));
  PetscCall(RatelDMPlexCeedBasisCoordinateCreate(ratel, dm, NULL, 0, height_face, &basis_x_face));

  for (PetscInt i = 0; i < ratel->num_active_fields; i++) {
    CeedBasis     basis_u_face = NULL, basis_x_to_u_face = NULL;
    CeedQFunction qf_setup;

    // Setup QFunction
    RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, SetupSlipBCs, RatelQFunctionRelativePath(SetupSlipBCs_loc), &qf_setup));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_setup, "multiplicity", field_sizes[i], CEED_EVAL_NONE));
    RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_setup, "scale", 1, CEED_EVAL_NONE));

    if (ratel->bc_slip_count[i] > 0) {
      DMLabel domain_label;

      PetscCall(RatelDebug(ratel, "---- Dirichlet Slip boundaries for field %" PetscInt_FMT, i));
      has_dirichlet = PETSC_TRUE;

      PetscCall(DMGetLabel(dm, "Face Sets", &domain_label));
      PetscCall(DMGetDimension(dm, &dim));

      // Basis
      PetscCall(RatelDebug(ratel, "------ Setting up libCEED bases"));
      PetscCall(RatelDMPlexCeedBasisCreate(ratel, dm, NULL, 0, height_face, i, &basis_u_face));
      PetscCall(CeedBasisCreateProjection(basis_x_face, basis_u_face, &basis_x_to_u_face));

      // Compute contribution on each boundary face
      for (CeedInt j = 0; j < ratel->bc_slip_count[i]; j++) {
        PetscBool            has_face;
        CeedVector           multiplicity, scale_stored;
        CeedElemRestriction  restriction_u_face, restriction_scale;
        CeedQFunction        qf_slip;
        CeedQFunctionContext ctx_slip;
        CeedOperator         op_setup, sub_op_slip;
        RatelBCSlipParams    params_slip;

        PetscCall(RatelDebug(ratel, "------ Setting up slip boundary: %" CeedInt_FMT, j));

        // Ensure face exists
        PetscCall(RatelDMHasFace(ratel, dm, ratel->bc_slip_faces[i][j], &has_face));
        PetscCheck(has_face, ratel->comm, PETSC_ERR_SUP, "Face %" PetscInt_FMT " does not exist.", ratel->bc_slip_faces[i][j]);

        // -- Read slip data
        PetscCall(RatelBoundarySlipDataFromOptions(ratel, i, j, &params_slip));

        // -- Incrementalize if using MPM
        if (incremental && ratel->method_type == RATEL_METHOD_MPM) {
          PetscCall(RatelIncrementalize(ratel, PETSC_FALSE, params_slip.num_comp_slip, &params_slip.num_times, params_slip.translation,
                                        params_slip.times, &params_slip.interpolation_type));
        }

        // -- Skip if zero BC
        const CeedScalar norm_translation = RatelNorm3(params_slip.translation);

        if (params_slip.num_times == 1 && (norm_translation) < CEED_EPSILON) {
          PetscCall(RatelDebug(ratel, "---- Zero Dirichlet slip boundary, no operator needed"));
          continue;
        }
        ratel->bc_clamp_nonzero_count++;

        // -- Restrictions
        PetscCall(RatelDebug(ratel, "---- Setting up libCEED restrictions"));
        PetscCall(RatelDMPlexCeedElemRestrictionCreate(ratel, dm, domain_label, ratel->bc_slip_faces[i][j], height_face, i, &restriction_u_face));
        RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_u_face, &multiplicity, NULL));
        RatelCallCeed(ratel, CeedElemRestrictionGetMultiplicity(restriction_u_face, multiplicity));
        RatelCallCeed(ratel, CeedElemRestrictionGetElementSize(restriction_u_face, &elem_size));
        PetscCall(RatelDMPlexCeedElemRestrictionCollocatedCreate(ratel, dm, domain_label, ratel->bc_slip_faces[i][j], height_face, i, 1,
                                                                 &restriction_scale));
        RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_scale, &scale_stored, NULL));

        // -- Setup Operator
        RatelCallCeed(ratel, CeedOperatorCreate(ceed, qf_setup, NULL, NULL, &op_setup));
        RatelCallCeed(ratel, CeedOperatorSetName(op_setup, "surface geometric data"));
        RatelCallCeed(ratel, CeedOperatorSetField(op_setup, "multiplicity", restriction_u_face, CEED_BASIS_NONE, multiplicity));
        RatelCallCeed(ratel, CeedOperatorSetField(op_setup, "scale", restriction_scale, CEED_BASIS_NONE, scale_stored));

        // -- Compute geometric factors
        PetscCall(RatelDebug(ratel, "---- Projecting coordinates and computing multiplicity scaling factor"));
        if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(op_setup, stdout));
        RatelCallCeed(ratel, CeedOperatorApply(op_setup, CEED_VECTOR_NONE, CEED_VECTOR_NONE, CEED_REQUEST_IMMEDIATE));

        // -- Apply QFunction
        PetscCall(RatelDebug(ratel, "---- Setting up Dirichlet slip operator"));
        RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, SlipBCs, RatelQFunctionRelativePath(SlipBCs_loc), &qf_slip));

        RatelCallCeed(ratel, CeedQFunctionAddInput(qf_slip, "scale", 1, CEED_EVAL_NONE));
        RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_slip, field_names[i], field_sizes[i], CEED_EVAL_NONE));
        RatelCallCeed(ratel, CeedQFunctionContextCreate(ceed, &ctx_slip));
        RatelCallCeed(ratel, CeedQFunctionContextSetData(ctx_slip, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(params_slip), &params_slip));
        RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(ctx_slip, "translation", offsetof(RatelBCSlipParams, translation),
                                                                params_slip.num_times * params_slip.num_comp_slip,
                                                                "translation vectors for constrained components"));
        RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(ctx_slip, "times", offsetof(RatelBCSlipParams, times), params_slip.num_times,
                                                                "transition time values"));
        RatelCallCeed(ratel, CeedQFunctionContextRegisterInt32(ctx_slip, "num times", offsetof(RatelBCSlipParams, num_times), 1,
                                                               "number of time values and direction vectors"));
        RatelCallCeed(ratel, CeedQFunctionContextRegisterInt32(ctx_slip, "components", offsetof(RatelBCSlipParams, components), 4,
                                                               "components to constrain with slip BC"));
        RatelCallCeed(ratel, CeedQFunctionContextRegisterInt32(ctx_slip, "num_comp_slip", offsetof(RatelBCSlipParams, num_comp_slip), 1,
                                                               "number of constrained components"));
        RatelCallCeed(ratel, CeedQFunctionContextRegisterInt32(ctx_slip, field_names[i], offsetof(RatelBCSlipParams, num_comp), 1,
                                                               "number of components in field"));
        RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(ctx_slip, "time", offsetof(RatelBCSlipParams, time), 1, "current solver time"));
        if (incremental) {
          // Only update dt for incremental slip BCs
          RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(ctx_slip, "dt", offsetof(RatelBCSlipParams, dt), 1, "current solver time step"));
        }

        RatelCallCeed(ratel, CeedQFunctionSetContext(qf_slip, ctx_slip));
        RatelCallCeed(ratel, CeedQFunctionSetContextWritable(qf_slip, PETSC_FALSE));
        if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedQFunctionContextView(ctx_slip, stdout));

        // -- Apply operator
        RatelCallCeed(ratel, CeedOperatorCreate(ceed, qf_slip, NULL, NULL, &sub_op_slip));
        {
          char operator_name[PETSC_MAX_PATH_LEN];

          PetscCall(PetscSNPrintf(operator_name, sizeof(operator_name), "slip - face %" PetscInt_FMT ", %s" PetscInt_FMT, ratel->bc_slip_faces[i][j],
                                  field_names[i]));
          RatelCallCeed(ratel, CeedOperatorSetName(sub_op_slip, operator_name));
        }
        RatelCallCeed(ratel, CeedOperatorSetField(sub_op_slip, "scale", restriction_scale, CEED_BASIS_NONE, scale_stored));
        RatelCallCeed(ratel, CeedOperatorSetField(sub_op_slip, field_names[i], restriction_u_face, CEED_BASIS_NONE, CEED_VECTOR_ACTIVE));
        // -- Add to composite operator
        RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_dirichlet, sub_op_slip));
        if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_slip, stdout));

        // -- Cleanup
        RatelCallCeed(ratel, CeedVectorDestroy(&scale_stored));
        RatelCallCeed(ratel, CeedVectorDestroy(&multiplicity));
        RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_u_face));
        RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_scale));
        RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_slip));
        RatelCallCeed(ratel, CeedQFunctionContextDestroy(&ctx_slip));
        RatelCallCeed(ratel, CeedOperatorDestroy(&op_setup));
        RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_slip));
      }
    }
    // -- Cleanup
    RatelCallCeed(ratel, CeedBasisDestroy(&basis_u_face));
    RatelCallCeed(ratel, CeedBasisDestroy(&basis_x_to_u_face));
    RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_setup));
  }
  // Cleanup
  RatelCallCeed(ratel, CeedBasisDestroy(&basis_x_face));

  if (has_dirichlet) PetscCall(RatelSetNonSPD(ratel));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Ceed Add Boundaries Dirichlet Slip Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Add Dirichlet MMS boundary conditions to Dirichlet boundary condition operator.

  Collective across MPI processes.

  @param[in]   ratel         `Ratel` context
  @param[in]   dm            `DM` to use for MMS boundaries
  @param[out]  op_dirichlet  Composite Dirichlet `CeedOperator` to add sub-operators to

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelCeedAddBoundariesDirichletMMS(Ratel ratel, DM dm, CeedOperator op_dirichlet) {
  PetscInt       dim;
  const PetscInt height_face = 1;
  PetscMemType   x_mem_type;
  Vec            X_loc;
  Ceed           ceed = ratel->ceed;
  CeedInt        num_comp_x, elem_size;
  const CeedInt *field_sizes;
  const char   **field_names;
  CeedVector     x_loc;
  CeedBasis      basis_x_face;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Ceed Add Boundaries MMS"));

  // Get dimension, number of solution components, and field names
  {
    PetscCall(DMGetDimension(dm, &dim));
    num_comp_x = dim;
    PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[0], NULL, &field_sizes));
    PetscCall(RatelMaterialGetActiveFieldNames(ratel->materials[0], &field_names, NULL));
  }

  // Coordinates vector
  {
    PetscInt x_size;

    PetscCall(DMGetCoordinatesLocal(dm, &X_loc));
    PetscCall(VecGetLocalSize(X_loc, &x_size));
    RatelCallCeed(ratel, CeedVectorCreate(ratel->ceed, x_size, &x_loc));
    PetscCall(VecReadPetscToCeed(X_loc, &x_mem_type, x_loc));
  }

  // Coordinate Basis
  PetscCall(RatelDebug(ratel, "---- Setting up libCEED coordinate basis"));
  PetscCall(RatelDMPlexCeedBasisCoordinateCreate(ratel, dm, NULL, 0, height_face, &basis_x_face));

  for (PetscInt i = 0; i < ratel->num_active_fields; i++) {
    // Check that material has an MMS boundary for current active field
    if (!ratel->materials[0]->model_data->mms_boundary[i]) continue;

    // Setup QFunction for current active field
    CeedQFunction qf_setup;
    RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, SetupDirichletBCs, RatelQFunctionRelativePath(SetupDirichletBCs_loc), &qf_setup));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_setup, "x", num_comp_x, CEED_EVAL_INTERP));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_setup, "multiplicity", field_sizes[i], CEED_EVAL_NONE));
    RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_setup, "x stored", num_comp_x, CEED_EVAL_NONE));
    RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_setup, "scale", 1, CEED_EVAL_NONE));

    if (ratel->bc_mms_count[i] > 0) {
      DMLabel             domain_label;
      CeedVector          operator_multiplicity;
      CeedElemRestriction restrictions_u_face[ratel->bc_mms_count[i]];
      CeedBasis           basis_u_face = NULL, basis_x_to_u_face = NULL;

      PetscCall(RatelDebug(ratel, "---- Dirichlet boundaries for field %s", field_names[i]));

      PetscCall(DMGetLabel(dm, "Face Sets", &domain_label));
      PetscCall(DMGetDimension(dm, &dim));

      // Restrictions and multiplicity
      {
        CeedVector multiplicity_lvec;

        for (CeedInt j = 0; j < ratel->bc_mms_count[i]; j++) {
          CeedVector multiplicity_evec;

          PetscCall(
              RatelDMPlexCeedElemRestrictionCreate(ratel, dm, domain_label, ratel->bc_mms_faces[i][j], height_face, i, &restrictions_u_face[j]));
          if (j == 0) {
            RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restrictions_u_face[j], &operator_multiplicity, NULL));
            RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restrictions_u_face[j], &multiplicity_lvec, NULL));
            RatelCallCeed(ratel, CeedVectorSetValue(operator_multiplicity, 0.0));
          }
          RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restrictions_u_face[j], NULL, &multiplicity_evec));
          RatelCallCeed(ratel, CeedVectorSetValue(multiplicity_evec, 1.0));
          RatelCallCeed(ratel, CeedVectorSetValue(multiplicity_lvec, 0.0));
          RatelCallCeed(
              ratel, CeedElemRestrictionApply(restrictions_u_face[j], CEED_TRANSPOSE, multiplicity_evec, multiplicity_lvec, CEED_REQUEST_IMMEDIATE));
          {
            CeedSize          lvec_len;
            CeedScalar       *multiplicity_array;
            const CeedScalar *lvec_array;

            RatelCallCeed(ratel, CeedVectorGetLength(multiplicity_lvec, &lvec_len));
            RatelCallCeed(ratel, CeedVectorGetArrayRead(multiplicity_lvec, CEED_MEM_HOST, &lvec_array));
            RatelCallCeed(ratel, CeedVectorGetArray(operator_multiplicity, CEED_MEM_HOST, &multiplicity_array));
            for (PetscInt i = 0; i < lvec_len; i++) {
              if (lvec_array[i] > 0.0) multiplicity_array[i] += 1.0;
            }
            RatelCallCeed(ratel, CeedVectorRestoreArrayRead(multiplicity_lvec, &lvec_array));
            RatelCallCeed(ratel, CeedVectorRestoreArray(operator_multiplicity, &multiplicity_array));
          }
          RatelCallCeed(ratel, CeedVectorDestroy(&multiplicity_evec));
        }
        RatelCallCeed(ratel, CeedVectorDestroy(&multiplicity_lvec));
      }

      // Bases
      PetscCall(RatelDebug(ratel, "------ Setting up libCEED bases"));
      PetscCall(RatelDMPlexCeedBasisCreate(ratel, dm, NULL, 0, height_face, i, &basis_u_face));
      PetscCall(CeedBasisCreateProjection(basis_x_face, basis_u_face, &basis_x_to_u_face));

      // Compute contribution on each boundary face
      for (CeedInt j = 0; j < ratel->bc_mms_count[i]; j++) {
        PetscBool           has_face;
        CeedVector          multiplicity, x_stored, scale_stored;
        CeedElemRestriction restriction_u_face = restrictions_u_face[j], restriction_x_face, restriction_x_stored, restriction_scale;
        CeedQFunction       qf_mms;
        CeedOperator        op_setup, sub_op_mms;

        PetscCall(RatelDebug(ratel, "------ Setting up MMS boundary: %" CeedInt_FMT, j));

        // Check face exists
        PetscCall(RatelDMHasFace(ratel, dm, ratel->bc_mms_faces[i][j], &has_face));
        PetscCheck(has_face, ratel->comm, PETSC_ERR_SUP, "Face %" PetscInt_FMT " does not exist.", ratel->bc_mms_faces[i][j]);
        ratel->bc_clamp_nonzero_count++;

        // -- Restrictions
        PetscCall(RatelDebug(ratel, "---- Setting up libCEED restrictions"));
        PetscCall(
            RatelDMPlexCeedElemRestrictionCoordinateCreate(ratel, dm, domain_label, ratel->bc_mms_faces[i][j], height_face, &restriction_x_face));
        RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_u_face, &multiplicity, NULL));
        {
          CeedVector multiplicity_evec;

          RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restrictions_u_face[j], NULL, &multiplicity_evec));
          RatelCallCeed(ratel, CeedElemRestrictionApply(restrictions_u_face[j], CEED_NOTRANSPOSE, operator_multiplicity, multiplicity_evec,
                                                        CEED_REQUEST_IMMEDIATE));
          RatelCallCeed(ratel, CeedVectorSetValue(multiplicity, 0.0));
          RatelCallCeed(ratel,
                        CeedElemRestrictionApply(restrictions_u_face[j], CEED_TRANSPOSE, multiplicity_evec, multiplicity, CEED_REQUEST_IMMEDIATE));
          RatelCallCeed(ratel, CeedVectorDestroy(&multiplicity_evec));
        }
        RatelCallCeed(ratel, CeedElemRestrictionGetElementSize(restriction_u_face, &elem_size));
        PetscCall(RatelDMPlexCeedElemRestrictionCollocatedCreate(ratel, dm, domain_label, ratel->bc_mms_faces[i][j], height_face, i, num_comp_x,
                                                                 &restriction_x_stored));
        RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_x_stored, &x_stored, NULL));
        PetscCall(RatelDMPlexCeedElemRestrictionCollocatedCreate(ratel, dm, domain_label, ratel->bc_mms_faces[i][j], height_face, i, 1,
                                                                 &restriction_scale));
        RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_scale, &scale_stored, NULL));

        // -- Setup Operator
        RatelCallCeed(ratel, CeedOperatorCreate(ceed, qf_setup, NULL, NULL, &op_setup));
        RatelCallCeed(ratel, CeedOperatorSetName(op_setup, "surface geometric data"));
        RatelCallCeed(ratel, CeedOperatorSetField(op_setup, "x", restriction_x_face, basis_x_to_u_face, CEED_VECTOR_ACTIVE));
        RatelCallCeed(ratel, CeedOperatorSetField(op_setup, "multiplicity", restriction_u_face, CEED_BASIS_NONE, multiplicity));
        RatelCallCeed(ratel, CeedOperatorSetField(op_setup, "x stored", restriction_x_stored, CEED_BASIS_NONE, x_stored));
        RatelCallCeed(ratel, CeedOperatorSetField(op_setup, "scale", restriction_scale, CEED_BASIS_NONE, scale_stored));

        // -- Compute geometric factors
        PetscCall(RatelDebug(ratel, "---- Projecting coordinates and computing multiplicity scaling factor"));
        if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(op_setup, stdout));
        RatelCallCeed(ratel, CeedOperatorApply(op_setup, x_loc, CEED_VECTOR_NONE, CEED_REQUEST_IMMEDIATE));

        // -- Apply QFunction
        PetscCall(RatelDebug(ratel, "---- Setting up Dirichlet MMS operator"));
        RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, ratel->materials[0]->model_data->mms_boundary[i],
                                                         RatelQFunctionRelativePath(ratel->materials[0]->model_data->mms_boundary_loc[i]), &qf_mms));
        RatelCallCeed(ratel, CeedQFunctionAddInput(qf_mms, "x", num_comp_x, CEED_EVAL_NONE));
        RatelCallCeed(ratel, CeedQFunctionAddInput(qf_mms, "scale", 1, CEED_EVAL_NONE));
        RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_mms, field_names[i], field_sizes[i], CEED_EVAL_NONE));
        if (ratel->materials[0]->ctx_mms_params) {
          RatelCallCeed(ratel, CeedQFunctionSetContext(qf_mms, ratel->materials[0]->ctx_mms_params));
          RatelCallCeed(ratel, CeedQFunctionSetContextWritable(qf_mms, PETSC_FALSE));
        }

        // -- Apply operator
        RatelCallCeed(ratel, CeedOperatorCreate(ceed, qf_mms, NULL, NULL, &sub_op_mms));
        {
          char operator_name[PETSC_MAX_PATH_LEN];

          PetscCall(
              PetscSNPrintf(operator_name, sizeof(operator_name), "mms - face %" PetscInt_FMT ", %s", ratel->bc_mms_faces[i][j], field_names[i]));
          RatelCallCeed(ratel, CeedOperatorSetName(sub_op_mms, operator_name));
        }
        RatelCallCeed(ratel, CeedOperatorSetField(sub_op_mms, "x", restriction_x_stored, CEED_BASIS_NONE, x_stored));
        RatelCallCeed(ratel, CeedOperatorSetField(sub_op_mms, "scale", restriction_scale, CEED_BASIS_NONE, scale_stored));
        RatelCallCeed(ratel, CeedOperatorSetField(sub_op_mms, field_names[i], restriction_u_face, CEED_BASIS_NONE, CEED_VECTOR_ACTIVE));
        // -- Add to composite operator
        RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_dirichlet, sub_op_mms));
        if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_mms, stdout));

        // -- Cleanup
        RatelCallCeed(ratel, CeedVectorDestroy(&x_stored));
        RatelCallCeed(ratel, CeedVectorDestroy(&scale_stored));
        RatelCallCeed(ratel, CeedVectorDestroy(&multiplicity));
        RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_x_face));
        RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restrictions_u_face[j]));
        RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_x_stored));
        RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_scale));
        RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_mms));
        RatelCallCeed(ratel, CeedOperatorDestroy(&op_setup));
        RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_mms));
      }
      // -- Cleanup
      RatelCallCeed(ratel, CeedVectorDestroy(&operator_multiplicity));
      RatelCallCeed(ratel, CeedBasisDestroy(&basis_u_face));
      RatelCallCeed(ratel, CeedBasisDestroy(&basis_x_to_u_face));
    }
    RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_setup));
  }
  // Cleanup
  PetscCall(VecReadCeedToPetsc(x_loc, x_mem_type, X_loc));
  RatelCallCeed(ratel, CeedVectorDestroy(&x_loc));
  RatelCallCeed(ratel, CeedBasisDestroy(&basis_x_face));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Ceed Add Boundaries MMS Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Read Dirichlet boundary conditions from options database

  Collective across MPI processes.

  @param[in]   ratel         `Ratel` context
  @param[in]   i             Field index
  @param[in]   j             Boundary index
  @param[out]  params_clamp  Data structure to store clamp data
*/
PetscErrorCode RatelBoundaryClampDataFromOptions(Ratel ratel, PetscInt i, PetscInt j, RatelBCClampParams *params_clamp) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Boundary Clamp Data From Options"));

  // Initialize context data
  for (PetscInt i = 0; i < RATEL_MAX_BC_INTERP_POINTS; i++) {
    for (PetscInt j = 0; j < 4; j++) params_clamp->translation[i * 4 + j] = 0.0;
    for (PetscInt j = 0; j < 4; j++) params_clamp->rotation_axis[i * 4 + j] = 0.0;
    for (PetscInt j = 0; j < 2; j++) params_clamp->rotation_polynomial[i * 2 + j] = 0.0;
    params_clamp->times[i] = 1.0;
  }
  params_clamp->num_times          = 1;
  params_clamp->interpolation_type = RATEL_BC_INTERP_LINEAR;
  params_clamp->time               = RATEL_UNSET_INITIAL_TIME;
  params_clamp->dt                 = 1.0;
  {
    const CeedInt *active_field_sizes;

    PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[0], NULL, &active_field_sizes));
    params_clamp->num_comp = active_field_sizes[i];
  }

  // Read CL options for clamp conditions
  PetscOptionsBegin(ratel->comm, NULL, "", NULL);

  PetscBool times_set = PETSC_FALSE;
  // Get transition times
  {
    PetscCall(RatelDebug(ratel, "----- Reading transition times"));
    char     option_name[PETSC_MAX_OPTION_NAME];
    PetscInt max_num_times = RATEL_MAX_BC_INTERP_POINTS;

    PetscCall(PetscSNPrintf(option_name, sizeof(option_name), "-bc_clamp_field_%" PetscInt_FMT "_face_%" PetscInt_FMT "_times", i,
                            ratel->bc_clamp_faces[i][j]));
    PetscCall(PetscOptionsScalarArray(option_name, "Transition times for each clamp option for constrained face", NULL, params_clamp->times,
                                      &max_num_times, &times_set));
    if (!times_set) {
      const char **active_field_names;

      PetscCall(RatelMaterialGetActiveFieldNames(ratel->materials[0], &active_field_names, NULL));
      PetscCall(PetscSNPrintf(option_name, sizeof(option_name), "-bc_clamp_%s_face_%" PetscInt_FMT "_times", active_field_names[i],
                              ratel->bc_clamp_faces[i][j]));
      max_num_times = params_clamp->num_comp * RATEL_MAX_BC_INTERP_POINTS;
      PetscCall(PetscOptionsScalarArray(option_name, "Transition times for each clamp option for constrained face", NULL, params_clamp->times,
                                        &max_num_times, &times_set));
    }
    if (i == 0 && !times_set) {
      PetscCall(PetscSNPrintf(option_name, sizeof(option_name), "-bc_clamp_%" PetscInt_FMT "_times", ratel->bc_clamp_faces[i][j]));
      max_num_times = params_clamp->num_comp * RATEL_MAX_BC_INTERP_POINTS;
      PetscCall(PetscOptionsScalarArray(option_name, "Transition times for each clamp option for constrained face", NULL, params_clamp->times,
                                        &max_num_times, &times_set));
    }
    if (times_set) {
      params_clamp->num_times = max_num_times;
      // -- Ensure times are monotonically increasing
      PetscBool monotonic     = PETSC_TRUE;

      for (PetscInt j = 0; j < params_clamp->num_times - 1; j++) {
        if (params_clamp->times[j] >= params_clamp->times[j + 1]) {
          monotonic = PETSC_FALSE;
        }
      }
      PetscCheck(monotonic, ratel->comm, PETSC_ERR_SUP, "Transition times must be monotonically increasing.");

      // -- Check bounds of times
      PetscCheck(params_clamp->times[0] >= 0, ratel->comm, PETSC_ERR_SUP, "First time must be greater than or equal to 0.");
    }
  }

  //  Translation vector
  PetscBool is_option_found = PETSC_FALSE;
  char      option_name[PETSC_MAX_OPTION_NAME];
  PetscInt  num_translations = params_clamp->num_comp * RATEL_MAX_BC_INTERP_POINTS;

  PetscCall(PetscSNPrintf(option_name, sizeof(option_name), "-bc_clamp_field_%" PetscInt_FMT "_face_%" PetscInt_FMT "_translate", i,
                          ratel->bc_clamp_faces[i][j]));
  PetscCall(PetscOptionsScalarArray(option_name, "Vector to translate clamped end by", NULL, params_clamp->translation, &num_translations,
                                    &is_option_found));
  if (!is_option_found) {
    const char **active_field_names;

    PetscCall(RatelMaterialGetActiveFieldNames(ratel->materials[0], &active_field_names, NULL));
    PetscCall(PetscSNPrintf(option_name, sizeof(option_name), "-bc_clamp_%s_face_%" PetscInt_FMT "_translate", active_field_names[i],
                            ratel->bc_clamp_faces[i][j]));
    num_translations = params_clamp->num_comp * RATEL_MAX_BC_INTERP_POINTS;
    PetscCall(PetscOptionsScalarArray(option_name, "Vector to translate clamped end by", NULL, params_clamp->translation, &num_translations,
                                      &is_option_found));
  }
  if (i == 0 && !is_option_found) {
    PetscCall(PetscSNPrintf(option_name, sizeof(option_name), "-bc_clamp_%" PetscInt_FMT "_translate", ratel->bc_clamp_faces[i][j]));
    num_translations = params_clamp->num_comp * RATEL_MAX_BC_INTERP_POINTS;
    PetscCall(PetscOptionsScalarArray(option_name, "Vector to translate clamped end by", NULL, params_clamp->translation, &num_translations,
                                      &is_option_found));
  }

  PetscCheck(!is_option_found || num_translations / params_clamp->num_comp == params_clamp->num_times, ratel->comm, PETSC_ERR_SUP,
             "Must provide times for all translation vectors.");

  // Rotation vector
  is_option_found                                      = PETSC_FALSE;
  PetscInt    num_rotations                            = 5 * RATEL_MAX_BC_INTERP_POINTS;
  PetscScalar rotation[5 * RATEL_MAX_BC_INTERP_POINTS] = {0.};

  PetscCall(PetscSNPrintf(option_name, sizeof(option_name), "-bc_clamp_field_%" PetscInt_FMT "_face_%" PetscInt_FMT "_rotate", i,
                          ratel->bc_clamp_faces[i][j]));
  PetscCall(PetscOptionsScalarArray(option_name, "Vector with axis of rotation and rotation, in radians", NULL, rotation, &num_rotations,
                                    &is_option_found));
  if (!is_option_found) {
    const char **active_field_names;

    PetscCall(RatelMaterialGetActiveFieldNames(ratel->materials[0], &active_field_names, NULL));
    PetscCall(PetscSNPrintf(option_name, sizeof(option_name), "-bc_clamp_%s_face_%" PetscInt_FMT "_rotate", active_field_names[i],
                            ratel->bc_clamp_faces[i][j]));
    num_rotations = 5 * RATEL_MAX_BC_INTERP_POINTS;
    PetscCall(PetscOptionsScalarArray(option_name, "Vector with axis of rotation and rotation, in radians", NULL, rotation, &num_rotations,
                                      &is_option_found));
  }
  if (i == 0 && !is_option_found) {
    PetscCall(PetscSNPrintf(option_name, sizeof(option_name), "-bc_clamp_%" PetscInt_FMT "_rotate", ratel->bc_clamp_faces[i][j]));
    num_rotations = 5 * RATEL_MAX_BC_INTERP_POINTS;
    PetscCall(PetscOptionsScalarArray(option_name, "Vector with axis of rotation and rotation, in radians", NULL, rotation, &num_rotations,
                                      &is_option_found));
  }

  PetscCheck(!is_option_found || num_rotations / 5 == params_clamp->num_times, ratel->comm, PETSC_ERR_SUP, "Must provide times for all rotations.");

  // Normalize
  for (PetscInt k = 0; k < params_clamp->num_times; k++) {
    CeedScalar norm_rotation = RatelNorm3((CeedScalar *)&rotation[5 * k]);

    if (fabs(norm_rotation) < CEED_EPSILON) norm_rotation = 1;
    for (PetscInt l = 0; l < 3; l++) params_clamp->rotation_axis[3 * k + l] = rotation[5 * k + l] / norm_rotation;
    params_clamp->rotation_polynomial[2 * k + 0] = rotation[5 * k + 3];
    params_clamp->rotation_polynomial[2 * k + 1] = rotation[5 * k + 4];
  }

  {
    char      option_name[PETSC_MAX_OPTION_NAME];
    PetscBool is_option_found = PETSC_FALSE;

    PetscCall(PetscSNPrintf(option_name, sizeof(option_name), "-bc_clamp_field_%" PetscInt_FMT "_face_%" PetscInt_FMT "_interpolation", i,
                            ratel->bc_clamp_faces[i][j]));
    PetscCall(PetscOptionsEnum(option_name, "Set interpolation type", NULL, RatelBCInterpolationTypesCL, (PetscEnum)params_clamp->interpolation_type,
                               (PetscEnum *)&params_clamp->interpolation_type, NULL));
    if (!is_option_found) {
      const char **active_field_names;

      PetscCall(RatelMaterialGetActiveFieldNames(ratel->materials[0], &active_field_names, NULL));
      PetscCall(PetscSNPrintf(option_name, sizeof(option_name), "-bc_clamp_%s_face_%" PetscInt_FMT "_interpolation", active_field_names[i],
                              ratel->bc_clamp_faces[i][j]));
      PetscCall(PetscOptionsEnum(option_name, "Set interpolation type", NULL, RatelBCInterpolationTypesCL,
                                 (PetscEnum)params_clamp->interpolation_type, (PetscEnum *)&params_clamp->interpolation_type, NULL));
    }
    if (i == 0 && !is_option_found) {
      PetscCall(PetscSNPrintf(option_name, sizeof(option_name), "-bc_clamp_%" PetscInt_FMT "_interpolation", ratel->bc_clamp_faces[i][j]));
      PetscCall(PetscOptionsEnum(option_name, "Set interpolation type", NULL, RatelBCInterpolationTypesCL,
                                 (PetscEnum)params_clamp->interpolation_type, (PetscEnum *)&params_clamp->interpolation_type, NULL));
    }
  }

  PetscCall(RatelDebug(ratel, "----   Clamp Boundary %" PetscInt_FMT ": %" PetscInt_FMT " translations/rotations with interpolation: %s",
                       ratel->bc_clamp_faces[i][j], params_clamp->num_times, RatelBCInterpolationTypes[params_clamp->interpolation_type]));
  for (PetscInt k = 0; k < params_clamp->num_times; k++) {
    PetscCall(RatelDebug(ratel, "------   t = %.3f: [tx: %.3f, ty: %.3f, tz: %.3f, rx: %.3f, ry: %.3f, rz: %.3f, rt0: %.3f, rt1: %.3f]",
                         params_clamp->times[k], params_clamp->translation[params_clamp->num_comp * k],
                         params_clamp->translation[params_clamp->num_comp * k + 1], params_clamp->translation[params_clamp->num_comp * k + 2],
                         params_clamp->rotation_axis[3 * k], params_clamp->rotation_axis[3 * k + 1], params_clamp->rotation_axis[3 * k + 2],
                         params_clamp->rotation_polynomial[2 * k], params_clamp->rotation_polynomial[2 * k + 1]));
  }
  PetscOptionsEnd();  // End of setting clamp conditions

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Boundary Clamp Data From Options Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Add Dirichlet clamp boundary conditions to Dirichlet boundary condition operator.

  Collective across MPI processes.

  @param[in]   ratel         `Ratel` context
  @param[in]   dm            `DM` to use for Dirichlet boundaries
  @param[in]   incremental   Set up operator to incrementally apply slip boundary conditions
  @param[out]  op_dirichlet  Composite Dirichlet `CeedOperator` to add sub-operators to

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelCeedAddBoundariesDirichletClamp(Ratel ratel, DM dm, PetscBool incremental, CeedOperator op_dirichlet) {
  PetscInt       dim;
  const PetscInt height_face = 1;
  PetscMemType   x_mem_type;
  Vec            X_loc;
  Ceed           ceed = ratel->ceed;
  CeedInt        num_comp_x, elem_size;
  const CeedInt *field_sizes;
  const char   **field_names;
  CeedVector     x_loc;
  CeedBasis      basis_x_face;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Ceed Add Boundaries Dirichlet Clamp"));

  // Get dimension, number of solution components, and field names
  {
    PetscCall(DMGetDimension(dm, &dim));
    num_comp_x = dim;
    PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[0], NULL, &field_sizes));
    PetscCall(RatelMaterialGetActiveFieldNames(ratel->materials[0], &field_names, NULL));
  }

  // Coordinates vector
  {
    PetscInt x_size;

    PetscCall(DMGetCoordinatesLocal(dm, &X_loc));
    PetscCall(VecGetLocalSize(X_loc, &x_size));
    RatelCallCeed(ratel, CeedVectorCreate(ratel->ceed, x_size, &x_loc));
    PetscCall(VecReadPetscToCeed(X_loc, &x_mem_type, x_loc));
  }

  // Coordinate Basis
  PetscCall(RatelDebug(ratel, "---- Setting up libCEED coordinate basis"));
  PetscCall(RatelDMPlexCeedBasisCoordinateCreate(ratel, dm, NULL, 0, height_face, &basis_x_face));

  for (PetscInt i = 0; i < ratel->num_active_fields; i++) {
    CeedBasis     basis_u_face = NULL, basis_x_to_u_face = NULL;
    CeedQFunction qf_setup;
    // Setup QFunction
    RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, SetupDirichletBCs, RatelQFunctionRelativePath(SetupDirichletBCs_loc), &qf_setup));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_setup, "x", num_comp_x, CEED_EVAL_INTERP));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_setup, "multiplicity", field_sizes[i], CEED_EVAL_NONE));
    RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_setup, "x stored", num_comp_x, CEED_EVAL_NONE));
    RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_setup, "scale", 1, CEED_EVAL_NONE));

    if (ratel->bc_clamp_count[i] > 0) {
      DMLabel domain_label;

      PetscCall(RatelDebug(ratel, "---- Dirichlet boundaries for field  %s", field_names[i]));

      PetscCall(DMGetLabel(dm, "Face Sets", &domain_label));
      PetscCall(DMGetDimension(dm, &dim));

      // Basis
      PetscCall(RatelDebug(ratel, "------ Setting up libCEED bases"));
      PetscCall(RatelDMPlexCeedBasisCreate(ratel, dm, NULL, 0, height_face, i, &basis_u_face));
      PetscCall(CeedBasisCreateProjection(basis_x_face, basis_u_face, &basis_x_to_u_face));

      // Compute contribution on each boundary face
      for (CeedInt j = 0; j < ratel->bc_clamp_count[i]; j++) {
        PetscBool            has_face;
        CeedVector           multiplicity, x_stored, scale_stored;
        CeedElemRestriction  restriction_x_face, restriction_u_face, restriction_x_stored, restriction_scale;
        CeedQFunctionContext ctx_clamp;
        CeedOperator         op_setup, sub_op_clamp;
        CeedQFunction        qf_clamp;
        RatelBCClampParams   params_clamp;

        PetscCall(RatelDebug(ratel, "------ Setting up clamp boundary: %" CeedInt_FMT, j));

        // Check face exists
        PetscCall(RatelDMHasFace(ratel, dm, ratel->bc_clamp_faces[i][j], &has_face));
        PetscCheck(has_face, ratel->comm, PETSC_ERR_SUP, "Face %" PetscInt_FMT " does not exist.", ratel->bc_clamp_faces[i][j]);

        // -- Set up context data
        PetscCall(RatelBoundaryClampDataFromOptions(ratel, i, j, &params_clamp));
        if (incremental && ratel->method_type == RATEL_METHOD_MPM) {
          // Incrementalize if using MPM
          RatelBCInterpolationType type                                  = params_clamp.interpolation_type;
          CeedScalar               old_times[RATEL_MAX_BC_INTERP_POINTS] = {0};

          PetscCall(PetscMemcpy(old_times, params_clamp.times, params_clamp.num_times * sizeof(CeedScalar)));
          PetscCall(RatelIncrementalize(ratel, PETSC_FALSE, 3, &params_clamp.num_times, params_clamp.translation, old_times, &type));
          PetscCall(RatelIncrementalize(ratel, PETSC_FALSE, 2, &params_clamp.num_times, params_clamp.rotation_polynomial, params_clamp.times,
                                        &params_clamp.interpolation_type));
        }

        // -- Skip if zero BC
        const CeedScalar norm_translation = RatelNorm3(params_clamp.translation);
        const CeedScalar norm_rotation    = RatelNorm3(params_clamp.rotation_axis);

        if (params_clamp.num_times == 1 && (norm_translation) < CEED_EPSILON && fabs(norm_rotation) < CEED_EPSILON) {
          PetscCall(RatelDebug(ratel, "---- Zero Dirichlet clamp boundary, no operator needed"));
          continue;
        }
        ratel->bc_clamp_nonzero_count++;

        // -- Restrictions
        PetscCall(RatelDebug(ratel, "---- Setting up libCEED restrictions"));
        PetscCall(RatelDMPlexCeedElemRestrictionCreate(ratel, dm, domain_label, ratel->bc_clamp_faces[i][j], height_face, i, &restriction_u_face));
        PetscCall(
            RatelDMPlexCeedElemRestrictionCoordinateCreate(ratel, dm, domain_label, ratel->bc_clamp_faces[i][j], height_face, &restriction_x_face));
        RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_u_face, &multiplicity, NULL));
        RatelCallCeed(ratel, CeedElemRestrictionGetMultiplicity(restriction_u_face, multiplicity));
        RatelCallCeed(ratel, CeedElemRestrictionGetElementSize(restriction_u_face, &elem_size));
        PetscCall(RatelDMPlexCeedElemRestrictionCollocatedCreate(ratel, dm, domain_label, ratel->bc_clamp_faces[i][j], height_face, i, num_comp_x,
                                                                 &restriction_x_stored));
        RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_x_stored, &x_stored, NULL));
        PetscCall(RatelDMPlexCeedElemRestrictionCollocatedCreate(ratel, dm, domain_label, ratel->bc_clamp_faces[i][j], height_face, i, 1,
                                                                 &restriction_scale));
        RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_scale, &scale_stored, NULL));

        // -- Setup Operator
        RatelCallCeed(ratel, CeedOperatorCreate(ceed, qf_setup, NULL, NULL, &op_setup));
        RatelCallCeed(ratel, CeedOperatorSetName(op_setup, "surface geometric data"));
        RatelCallCeed(ratel, CeedOperatorSetField(op_setup, "x", restriction_x_face, basis_x_to_u_face, CEED_VECTOR_ACTIVE));
        RatelCallCeed(ratel, CeedOperatorSetField(op_setup, "multiplicity", restriction_u_face, CEED_BASIS_NONE, multiplicity));
        RatelCallCeed(ratel, CeedOperatorSetField(op_setup, "x stored", restriction_x_stored, CEED_BASIS_NONE, x_stored));
        RatelCallCeed(ratel, CeedOperatorSetField(op_setup, "scale", restriction_scale, CEED_BASIS_NONE, scale_stored));

        // -- Compute geometric factors
        PetscCall(RatelDebug(ratel, "---- Projecting coordinates and computing multiplicity scaling factor"));
        if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(op_setup, stdout));
        RatelCallCeed(ratel, CeedOperatorApply(op_setup, x_loc, CEED_VECTOR_NONE, CEED_REQUEST_IMMEDIATE));

        // -- Apply QFunction
        PetscCall(RatelDebug(ratel, "---- Setting up Dirichlet clamp operator"));
        RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, ClampBCs, RatelQFunctionRelativePath(ClampBCs_loc), &qf_clamp));

        RatelCallCeed(ratel, CeedQFunctionAddInput(qf_clamp, "x", num_comp_x, CEED_EVAL_NONE));
        RatelCallCeed(ratel, CeedQFunctionAddInput(qf_clamp, "scale", 1, CEED_EVAL_NONE));
        RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_clamp, field_names[i], field_sizes[i], CEED_EVAL_NONE));
        RatelCallCeed(ratel, CeedQFunctionContextCreate(ceed, &ctx_clamp));
        RatelCallCeed(ratel, CeedQFunctionContextSetData(ctx_clamp, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(params_clamp), &params_clamp));
        RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(ctx_clamp, "translation x", offsetof(RatelBCClampParams, translation[0]), 1,
                                                                "translation in x direction"));
        RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(ctx_clamp, "translation y", offsetof(RatelBCClampParams, translation[1]), 1,
                                                                "translation in y direction"));
        RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(ctx_clamp, "translation z", offsetof(RatelBCClampParams, translation[2]), 1,
                                                                "translation in z direction"));
        RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(ctx_clamp, "rotation x", offsetof(RatelBCClampParams, rotation_axis[0]), 1,
                                                                "x component for axis of rotation"));
        RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(ctx_clamp, "rotation y", offsetof(RatelBCClampParams, rotation_axis[1]), 1,
                                                                "y component for axis of rotation"));
        RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(ctx_clamp, "rotation z", offsetof(RatelBCClampParams, rotation_axis[2]), 1,
                                                                "z component for axis of rotation"));
        RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(ctx_clamp, "rotation c_0", offsetof(RatelBCClampParams, rotation_polynomial[0]), 1,
                                                                "coefficient 0 for rotation polynomial"));
        RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(ctx_clamp, "rotation c_1", offsetof(RatelBCClampParams, rotation_polynomial[1]), 1,
                                                                "coefficient 1 for rotation polynomial"));
        RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(ctx_clamp, "time", offsetof(RatelBCClampParams, time), 1, "current solver time"));
        if (incremental) {
          // Only update dt for incremental
          RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(ctx_clamp, "dt", offsetof(RatelBCClampParams, dt), 1, "current solver time step"));
        }
        RatelCallCeed(ratel, CeedQFunctionSetContext(qf_clamp, ctx_clamp));
        RatelCallCeed(ratel, CeedQFunctionSetContextWritable(qf_clamp, PETSC_FALSE));
        if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedQFunctionContextView(ctx_clamp, stdout));

        // -- Apply operator
        RatelCallCeed(ratel, CeedOperatorCreate(ceed, qf_clamp, NULL, NULL, &sub_op_clamp));
        {
          char operator_name[PETSC_MAX_PATH_LEN];

          PetscCall(
              PetscSNPrintf(operator_name, sizeof(operator_name), "clamp - face %" PetscInt_FMT ", %s", ratel->bc_clamp_faces[i][j], field_names[i]));
          RatelCallCeed(ratel, CeedOperatorSetName(sub_op_clamp, operator_name));
        }
        RatelCallCeed(ratel, CeedOperatorSetField(sub_op_clamp, "x", restriction_x_stored, CEED_BASIS_NONE, x_stored));
        RatelCallCeed(ratel, CeedOperatorSetField(sub_op_clamp, "scale", restriction_scale, CEED_BASIS_NONE, scale_stored));
        RatelCallCeed(ratel, CeedOperatorSetField(sub_op_clamp, field_names[i], restriction_u_face, CEED_BASIS_NONE, CEED_VECTOR_ACTIVE));
        // -- Add to composite operator
        RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_dirichlet, sub_op_clamp));
        if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_clamp, stdout));

        // -- Cleanup
        RatelCallCeed(ratel, CeedVectorDestroy(&x_stored));
        RatelCallCeed(ratel, CeedVectorDestroy(&scale_stored));
        RatelCallCeed(ratel, CeedVectorDestroy(&multiplicity));
        RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_x_face));
        RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_u_face));
        RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_x_stored));
        RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_scale));
        RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_clamp));
        RatelCallCeed(ratel, CeedQFunctionContextDestroy(&ctx_clamp));
        RatelCallCeed(ratel, CeedOperatorDestroy(&op_setup));
        RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_clamp));
      }
    }
    // -- Cleanup
    RatelCallCeed(ratel, CeedBasisDestroy(&basis_u_face));
    RatelCallCeed(ratel, CeedBasisDestroy(&basis_x_to_u_face));
    RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_setup));
  }
  // Cleanup
  PetscCall(VecReadCeedToPetsc(x_loc, x_mem_type, X_loc));
  RatelCallCeed(ratel, CeedVectorDestroy(&x_loc));
  RatelCallCeed(ratel, CeedBasisDestroy(&basis_x_face));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Ceed Add Boundaries Dirichlet Clamp Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Read traction boundary conditions from options database

  Collective across MPI processes.

  @param[in]   ratel          `Ratel` context
  @param[in]   i              Boundary index
  @param[out] params_traction  Data structure to store traction data
*/
PetscErrorCode RatelBoundaryTractionDataFromOptions(Ratel ratel, PetscInt i, RatelBCTractionParams *params_traction) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Boundary Traction Data From Options"));

  // Initialize context data
  for (PetscInt i = 0; i < RATEL_MAX_BC_INTERP_POINTS; i++) {
    for (PetscInt j = 0; j < 3; j++) params_traction->direction[i * 3 + j] = 0.0;
    params_traction->times[i] = 1.0;
  }
  params_traction->num_times          = 1;
  params_traction->interpolation_type = RATEL_BC_INTERP_LINEAR;
  params_traction->time               = RATEL_UNSET_INITIAL_TIME;

  // Read CL options for traction conditions
  PetscOptionsBegin(ratel->comm, NULL, "", NULL);

  // Translation vector(s)
  PetscBool traction_set = PETSC_FALSE, times_set = PETSC_FALSE;
  PetscInt  max_num_directions = 3 * RATEL_MAX_BC_INTERP_POINTS;
  PetscInt  max_num_times      = RATEL_MAX_BC_INTERP_POINTS;

  // Get transition times for traction vectors
  {
    char option_name[PETSC_MAX_OPTION_NAME];

    PetscCall(PetscSNPrintf(option_name, sizeof(option_name), "-bc_traction_%" PetscInt_FMT "_times", ratel->bc_traction_faces[i]));
    PetscCall(PetscOptionsScalarArray(option_name, "Transition times for each traction vector for constrained face", NULL, params_traction->times,
                                      &max_num_times, &times_set));
    if (times_set) {
      params_traction->num_times = max_num_times;
      // -- Ensure times are monotonically increasing
      PetscBool monotonic        = PETSC_TRUE;

      for (PetscInt j = 0; j < params_traction->num_times - 1; j++) {
        if (params_traction->times[j] >= params_traction->times[j + 1]) {
          monotonic = PETSC_FALSE;
        }
      }
      PetscCheck(monotonic, ratel->comm, PETSC_ERR_SUP, "Transition times must be monotonically increasing.");

      // -- Check bounds of times
      PetscCheck(params_traction->times[0] >= 0, ratel->comm, PETSC_ERR_SUP, "First time must be greater than or equal to 0.");
    }
  }
  // Get traction vectors
  {
    char option_name[PETSC_MAX_OPTION_NAME];

    PetscCall(PetscSNPrintf(option_name, sizeof(option_name), "-bc_traction_%" PetscInt_FMT, ratel->bc_traction_faces[i]));

    PetscCall(PetscOptionsScalarArray(option_name, "Traction vectors for constrained face", NULL, params_traction->direction, &max_num_directions,
                                      &traction_set));

    PetscCheck(traction_set, ratel->comm, PETSC_ERR_SUP, "Traction vector must be set for all traction boundary conditions.");
    PetscCheck(max_num_directions % 3 == 0, ratel->comm, PETSC_ERR_SUP, "Direction vector must have a multiple of 3 components");

    PetscInt num_vectors = max_num_directions / 3;
    PetscCheck(num_vectors == params_traction->num_times, ratel->comm, PETSC_ERR_SUP, "A time must be set for each provided direction vector.");
    PetscCheck(times_set || num_vectors == 1, ratel->comm, PETSC_ERR_SUP, "Times must be set if more than one direction is specified.");
  }

  {
    char option_name[PETSC_MAX_OPTION_NAME];

    PetscCall(PetscSNPrintf(option_name, sizeof(option_name), "-bc_traction_%" PetscInt_FMT "_interpolation", ratel->bc_traction_faces[i]));

    PetscCall(PetscOptionsEnum(option_name, "Set interpolation type for traction BC", NULL, RatelBCInterpolationTypesCL,
                               (PetscEnum)params_traction->interpolation_type, (PetscEnum *)&params_traction->interpolation_type, NULL));
  }

  // Incrementalize if using MPM
  if (ratel->method_type == RATEL_METHOD_MPM) {
    PetscCall(RatelIncrementalize(ratel, PETSC_TRUE, 3, &params_traction->num_times, params_traction->direction, params_traction->times,
                                  &params_traction->interpolation_type));
  }

  PetscCall(RatelDebug(ratel, "----   Traction Boundary %" PetscInt_FMT ": %" PetscInt_FMT " traction vector(s) with interpolation: %s",
                       ratel->bc_traction_faces[i], params_traction->num_times, RatelBCInterpolationTypes[params_traction->interpolation_type]));
  for (PetscInt j = 0; j < params_traction->num_times; j++) {
    PetscCall(RatelDebug(ratel, "------   t = %.3f: [tx: %.3f, ty: %.3f, tz: %.3f]", params_traction->times[j], params_traction->direction[3 * j],
                         params_traction->direction[3 * j + 1], params_traction->direction[3 * j + 2]));
  }

  PetscOptionsEnd();  // End of setting traction conditions

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Boundary Traction Data From Options Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Compute `CeedOperator` surface QData.

  Collective across MPI processes.

  @param[in]   ratel        `Ratel` context
  @param[in]   dm           `DM` to use for Neumann boundaries
  @param[in]   label_name   `DMPlex` label name for surface
  @param[in]   label_value  `DMPlex` label value for surface
  @param[out]  restriction  `CeedElemRestriction` for QData
  @param[out]  q_data       `CeedVector` holding QData

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelCeedSetupSurfaceQData(Ratel ratel, DM dm, const char *label_name, PetscInt label_value, CeedElemRestriction *restriction,
                                          CeedVector *q_data) {
  DMLabel             domain_label;
  PetscInt            dim;
  const PetscInt      height_face = 1;
  PetscMemType        x_mem_type;
  Vec                 X_loc;
  Ceed                ceed = ratel->ceed;
  CeedInt             num_comp_x, q_data_size = 4;
  CeedVector          x_loc = NULL;
  CeedElemRestriction restriction_x_face;
  CeedBasis           basis_x_face;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Ceed Setup Surface QData"));

  // DM information
  PetscCall(DMGetDimension(dm, &dim));
  num_comp_x = dim;
  PetscCall(DMGetLabel(dm, label_name, &domain_label));

  // libCEED objects
  // -- Coordinate bases
  PetscCall(RatelDebug(ratel, "---- Coordinate bases"));
  PetscCall(RatelDMPlexCeedBasisCoordinateCreate(ratel, dm, NULL, 0, height_face, &basis_x_face));
  // -- Coordinate restrictions
  PetscCall(RatelDebug(ratel, "---- Coordinate restrictions"));
  PetscCall(RatelDMPlexCeedElemRestrictionCoordinateCreate(ratel, dm, domain_label, label_value, height_face, &restriction_x_face));
  // ---- QData restriction
  PetscCall(RatelDebug(ratel, "---- QData restriction"));
  PetscCall(RatelDMPlexCeedElemRestrictionQDataCreate(ratel, dm, domain_label, label_value, height_face, q_data_size, restriction));

  // Coordinates vector
  {
    PetscInt x_size;

    PetscCall(DMGetCoordinatesLocal(dm, &X_loc));
    PetscCall(VecGetLocalSize(X_loc, &x_size));
    RatelCallCeed(ratel, CeedVectorCreate(ratel->ceed, x_size, &x_loc));
    PetscCall(VecReadPetscToCeed(X_loc, &x_mem_type, x_loc));
  }

  // libCEED vectors
  PetscCall(RatelDebug(ratel, "---- QData vector"));
  // -- Geometric data vector
  RatelCallCeed(ratel, CeedElemRestrictionCreateVector(*restriction, q_data, NULL));

  // Geometric factor computation
  {
    CeedQFunction qf_setup_q_data_surface;
    CeedOperator  op_setup_q_data_surface;

    PetscCall(RatelDebug(ratel, "---- Computing surface qdata"));

    // -- QFunction
    RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, SetupSurfaceGeometry, RatelQFunctionRelativePath(SetupSurfaceGeometry_loc),
                                                     &qf_setup_q_data_surface));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_setup_q_data_surface, "dx/dX face", num_comp_x * (dim - height_face), CEED_EVAL_GRAD));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_setup_q_data_surface, "weight", 1, CEED_EVAL_WEIGHT));
    RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_setup_q_data_surface, "q data", q_data_size, CEED_EVAL_NONE));

    // -- Operator
    RatelCallCeed(ratel, CeedOperatorCreate(ceed, qf_setup_q_data_surface, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &op_setup_q_data_surface));
    RatelCallCeed(ratel, CeedOperatorSetName(op_setup_q_data_surface, "surface geometric data"));
    RatelCallCeed(ratel, CeedOperatorSetField(op_setup_q_data_surface, "dx/dX face", restriction_x_face, basis_x_face, CEED_VECTOR_ACTIVE));
    RatelCallCeed(ratel, CeedOperatorSetField(op_setup_q_data_surface, "weight", CEED_ELEMRESTRICTION_NONE, basis_x_face, CEED_VECTOR_NONE));
    RatelCallCeed(ratel, CeedOperatorSetField(op_setup_q_data_surface, "q data", *restriction, CEED_BASIS_NONE, CEED_VECTOR_ACTIVE));

    // -- Compute the quadrature data
    PetscCall(RatelDebug(ratel, "---- Computing surface q data"));
    if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(op_setup_q_data_surface, stdout));
    RatelCallCeed(ratel, CeedOperatorApply(op_setup_q_data_surface, x_loc, *q_data, CEED_REQUEST_IMMEDIATE));

    // -- Cleanup
    RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_setup_q_data_surface));
    RatelCallCeed(ratel, CeedOperatorDestroy(&op_setup_q_data_surface));
  }
  // Cleanup
  PetscCall(VecCeedToPetsc(x_loc, x_mem_type, X_loc));
  RatelCallCeed(ratel, CeedVectorDestroy(&x_loc));
  RatelCallCeed(ratel, CeedBasisDestroy(&basis_x_face));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_x_face));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Ceed Setup Surface QData Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Add Neumann boundary conditions to residual operator.

  Collective across MPI processes.

  @param[in]   ratel        `Ratel` context
  @param[in]   dm           `DM` to use for Neumann boundaries
  @param[out]  op_residual  Composite residual u term `CeedOperator` to add Neumann sub-operators

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelCeedAddBoundariesNeumann(Ratel ratel, DM dm, CeedOperator op_residual) {
  const PetscInt height_face = 1;
  Ceed           ceed        = ratel->ceed;
  CeedInt        num_comp_u = 3, q_data_size = 4;
  CeedBasis      basis_u_face;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Ceed Add Boundaries Neumann"));

  // Get number of solution components and dimension
  {
    const CeedInt *field_sizes;

    PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[0], NULL, &field_sizes));
    num_comp_u = field_sizes[0];  // First field should be displacement
  }

  if (ratel->bc_traction_count > 0) {
    DMLabel domain_label;

    PetscCall(DMGetLabel(dm, "Face Sets", &domain_label));

    // Basis
    PetscCall(RatelDebug(ratel, "---- Setting up libCEED bases"));
    PetscCall(RatelDMPlexCeedBasisCreate(ratel, dm, NULL, 0, height_face, 0, &basis_u_face));

    // Compute contribution on each boundary face
    for (CeedInt i = 0; i < ratel->bc_traction_count; i++) {
      PetscBool             has_face;
      CeedVector            q_data = NULL;
      CeedElemRestriction   restriction_u_face, restriction_q_data = NULL;
      CeedQFunctionContext  ctx_traction;
      CeedQFunction         qf_traction;
      CeedOperator          sub_op_traction;
      RatelBCTractionParams params_traction;

      PetscCall(RatelDebug(ratel, "---- Setting up traction boundary: %" CeedInt_FMT, i));

      // -- Check face exists
      PetscCall(RatelDMHasFace(ratel, dm, ratel->bc_traction_faces[i], &has_face));
      PetscCheck(has_face, ratel->comm, PETSC_ERR_SUP, "Face %" PetscInt_FMT " does not exist.", ratel->bc_traction_faces[i]);

      // -- Compute geometric factors
      PetscCall(RatelCeedSetupSurfaceQData(ratel, dm, "Face Sets", ratel->bc_traction_faces[i], &restriction_q_data, &q_data));

      // -- Set up context data
      PetscCall(RatelBoundaryTractionDataFromOptions(ratel, i, &params_traction));

      // -- Restrictions
      PetscCall(RatelDebug(ratel, "---- Setting up libCEED restrictions"));
      PetscCall(RatelDMPlexCeedElemRestrictionCreate(ratel, dm, domain_label, ratel->bc_traction_faces[i], height_face, 0, &restriction_u_face));

      // -- Apply QFunction
      PetscCall(RatelDebug(ratel, "---- Setting up Neumann traction operator"));
      RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, TractionBCs, RatelQFunctionRelativePath(TractionBCs_loc), &qf_traction));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_traction, "q data", q_data_size, CEED_EVAL_NONE));
      RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_traction, "v", num_comp_u, CEED_EVAL_INTERP));
      RatelCallCeed(ratel, CeedQFunctionContextCreate(ceed, &ctx_traction));
      RatelCallCeed(ratel, CeedQFunctionContextSetData(ctx_traction, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(params_traction), &params_traction));
      RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(ctx_traction, "traction directions", offsetof(RatelBCTractionParams, direction),
                                                              3 * params_traction.num_times, "vector describing traction in (x, y, z) direction"));
      RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(ctx_traction, "times", offsetof(RatelBCTractionParams, times),
                                                              params_traction.num_times, "transition time values"));
      RatelCallCeed(ratel, CeedQFunctionContextRegisterInt32(ctx_traction, "num times", offsetof(RatelBCTractionParams, num_times), 1,
                                                             "number of time values and direction vectors"));
      RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(ctx_traction, "time", offsetof(RatelBCTractionParams, time), 1, "current solver time"));
      RatelCallCeed(ratel, CeedQFunctionSetContext(qf_traction, ctx_traction));
      RatelCallCeed(ratel, CeedQFunctionSetContextWritable(qf_traction, PETSC_FALSE));
      if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedQFunctionContextView(ctx_traction, stdout));

      // -- Apply operator
      RatelCallCeed(ratel, CeedOperatorCreate(ceed, qf_traction, NULL, NULL, &sub_op_traction));
      {
        char operator_name[PETSC_MAX_PATH_LEN];

        PetscCall(PetscSNPrintf(operator_name, sizeof(operator_name), "traction - face %" PetscInt_FMT, ratel->bc_traction_faces[i]));
        RatelCallCeed(ratel, CeedOperatorSetName(sub_op_traction, operator_name));
      }
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_traction, "q data", restriction_q_data, CEED_BASIS_NONE, q_data));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_traction, "v", restriction_u_face, basis_u_face, CEED_VECTOR_ACTIVE));
      // -- Add to composite operator
      RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_residual, sub_op_traction));
      if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_traction, stdout));

      // -- Cleanup
      RatelCallCeed(ratel, CeedVectorDestroy(&q_data));
      RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_u_face));
      RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_q_data));
      RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_traction));
      RatelCallCeed(ratel, CeedQFunctionContextDestroy(&ctx_traction));
      RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_traction));
    }
    // Cleanup
    RatelCallCeed(ratel, CeedBasisDestroy(&basis_u_face));
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Ceed Add Boundaries Neumann Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Add traction energy to energy operator.

  Collective across MPI processes.

  @param[in]   ratel              `Ratel` context
  @param[in]   dm_energy          `DM` for strain energy computation
  @param[out]  op_external_energy  Composite external energy `CeedOperator` to add traction energy sub-operators

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSetupTractionEnergySuboperator(Ratel ratel, DM dm_energy, CeedOperator op_external_energy) {
  DM             dm_solution;
  const PetscInt height_face = 1;
  Ceed           ceed        = ratel->ceed;
  CeedInt        num_comp_u = 3, num_comp_energy = 1, q_data_size = 4;
  CeedBasis      basis_u_face, basis_energy_face;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Ceed Add Traction Energy"));

  // DM information
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));

  // Get number of solution components and dimension
  {
    const CeedInt *field_sizes;

    PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[0], NULL, &field_sizes));
    num_comp_u = field_sizes[0];  // First field should be displacement
  }

  if (ratel->bc_traction_count > 0) {
    DMLabel domain_label;

    PetscCall(DMGetLabel(dm_solution, "Face Sets", &domain_label));

    // Basis
    PetscCall(RatelDebug(ratel, "---- Setting up libCEED bases"));
    PetscCall(RatelDMPlexCeedBasisCreate(ratel, dm_solution, NULL, 0, height_face, 0, &basis_u_face));
    PetscCall(RatelDMPlexCeedBasisCreate(ratel, dm_energy, NULL, 0, height_face, 0, &basis_energy_face));

    // Compute contribution of traction energy on each boundary face
    for (CeedInt i = 0; i < ratel->bc_traction_count; i++) {
      PetscBool             has_face;
      CeedVector            q_data = NULL;
      CeedElemRestriction   restriction_u_face, restriction_energy_face, restriction_q_data = NULL;
      CeedQFunctionContext  ctx_traction;
      CeedQFunction         qf_traction_energy;
      CeedOperator          sub_op_traction_energy;
      RatelBCTractionParams params_traction;

      PetscCall(RatelDebug(ratel, "---- Setting up traction boundary: %" CeedInt_FMT, i));

      // -- Check face exists
      PetscCall(RatelDMHasFace(ratel, dm_solution, ratel->bc_traction_faces[i], &has_face));
      PetscCheck(has_face, ratel->comm, PETSC_ERR_SUP, "Face %" PetscInt_FMT " does not exist.", ratel->bc_traction_faces[i]);

      // -- Compute geometric factors
      PetscCall(RatelCeedSetupSurfaceQData(ratel, dm_solution, "Face Sets", ratel->bc_traction_faces[i], &restriction_q_data, &q_data));

      // -- Set up context data
      PetscCall(RatelBoundaryTractionDataFromOptions(ratel, i, &params_traction));

      // -- Restrictions
      PetscCall(RatelDebug(ratel, "---- Setting up libCEED restrictions"));
      PetscCall(
          RatelDMPlexCeedElemRestrictionCreate(ratel, dm_solution, domain_label, ratel->bc_traction_faces[i], height_face, 0, &restriction_u_face));
      PetscCall(RatelDMPlexCeedElemRestrictionCreate(ratel, dm_energy, domain_label, ratel->bc_traction_faces[i], height_face, 0,
                                                     &restriction_energy_face));

      // -- Apply QFunction
      PetscCall(RatelDebug(ratel, "---- Setting up traction energy operator"));
      RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, TractionEnergy, RatelQFunctionRelativePath(TractionEnergy_loc), &qf_traction_energy));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_traction_energy, "q data", q_data_size, CEED_EVAL_NONE));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_traction_energy, "u", num_comp_u, CEED_EVAL_INTERP));
      RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_traction_energy, "energy", num_comp_energy, CEED_EVAL_INTERP));
      RatelCallCeed(ratel, CeedQFunctionContextCreate(ceed, &ctx_traction));
      RatelCallCeed(ratel,
                    CeedQFunctionContextSetData(ctx_traction, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(RatelBCTractionParams), &params_traction));
      RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(ctx_traction, "traction directions", offsetof(RatelBCTractionParams, direction),
                                                              3 * params_traction.num_times, "vector describing traction in (x, y, z) direction"));
      RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(ctx_traction, "times", offsetof(RatelBCTractionParams, times),
                                                              params_traction.num_times, "transition time values"));
      RatelCallCeed(ratel, CeedQFunctionContextRegisterInt32(ctx_traction, "num times", offsetof(RatelBCTractionParams, num_times), 1,
                                                             "number of time values and direction vectors"));
      RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(ctx_traction, "time", offsetof(RatelBCTractionParams, time), 1, "current solver time"));
      RatelCallCeed(ratel, CeedQFunctionSetContext(qf_traction_energy, ctx_traction));
      RatelCallCeed(ratel, CeedQFunctionSetContextWritable(qf_traction_energy, PETSC_FALSE));
      if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedQFunctionContextView(ctx_traction, stdout));

      // -- Apply operator
      RatelCallCeed(ratel, CeedOperatorCreate(ceed, qf_traction_energy, NULL, NULL, &sub_op_traction_energy));
      {
        char operator_name[PETSC_MAX_PATH_LEN];

        PetscCall(PetscSNPrintf(operator_name, sizeof(operator_name), "traction - face %" PetscInt_FMT, ratel->bc_traction_faces[i]));
        RatelCallCeed(ratel, CeedOperatorSetName(sub_op_traction_energy, operator_name));
      }
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_traction_energy, "q data", restriction_q_data, CEED_BASIS_NONE, q_data));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_traction_energy, "u", restriction_u_face, basis_u_face, CEED_VECTOR_ACTIVE));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_traction_energy, "energy", restriction_energy_face, basis_energy_face, CEED_VECTOR_ACTIVE));
      // -- Add to composite operator
      RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_external_energy, sub_op_traction_energy));
      if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_traction_energy, stdout));

      // -- Cleanup
      RatelCallCeed(ratel, CeedVectorDestroy(&q_data));
      RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_u_face));
      RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_q_data));
      RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_energy_face));
      RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_traction_energy));
      RatelCallCeed(ratel, CeedQFunctionContextDestroy(&ctx_traction));
      RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_traction_energy));
    }
    // Cleanup
    RatelCallCeed(ratel, CeedBasisDestroy(&basis_u_face));
    RatelCallCeed(ratel, CeedBasisDestroy(&basis_energy_face));
  }

  // Cleanup
  PetscCall(DMDestroy(&dm_solution));
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Ceed Add Traction Energy Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup surface force face centroid computation.

  Collective across MPI processes.

  @param[in]  ratel  `Ratel` context
  @param[in]  dm     `DM` with surface force faces

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSetupSurfaceForceCentroids(Ratel ratel, DM dm) {
  PetscInt             dim         = 3;
  const PetscInt       height_face = 1;
  PetscScalar          centroid_local[3], surface_area_local;
  PetscMemType         x_mem_type;
  Vec                  X_loc;
  Ceed                 ceed = ratel->ceed;
  CeedInt              num_comp_x, q_data_size = 4;
  CeedVector           x_loc = NULL, centroid = NULL, one = NULL;
  CeedBasis            basis_x_face;
  CeedQFunctionContext ctx_mass;
  CeedQFunction        qf_mass, qf_centroid;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Setup Surface Force Centroids"));

  PetscCall(DMGetDimension(dm, &dim));
  num_comp_x = dim;

  if (ratel->surface_force_face_count > 0) {
    DMLabel domain_label;

    PetscCall(DMGetLabel(dm, "Face Sets", &domain_label));

    // Vectors
    {
      PetscInt x_size;

      // -- Coordinates
      PetscCall(DMGetCoordinatesLocal(dm, &X_loc));
      PetscCall(VecGetLocalSize(X_loc, &x_size));
      RatelCallCeed(ratel, CeedVectorCreate(ratel->ceed, x_size, &x_loc));
      PetscCall(VecReadPetscToCeed(X_loc, &x_mem_type, x_loc));

      // -- Other vectors
      RatelCallCeed(ratel, CeedVectorCreate(ratel->ceed, x_size, &centroid));
      RatelCallCeed(ratel, CeedVectorCreate(ratel->ceed, x_size, &one));
      RatelCallCeed(ratel, CeedVectorSetValue(one, 1.0));
    }

    // Basis
    PetscCall(RatelDebug(ratel, "---- Setting up libCEED bases"));
    PetscCall(RatelDMPlexCeedBasisCoordinateCreate(ratel, dm, NULL, 0, height_face, &basis_x_face));

    // Surface area QFunction
    RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, Mass, RatelQFunctionRelativePath(Mass_loc), &qf_mass));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_mass, "q data", q_data_size, CEED_EVAL_NONE));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_mass, "u", num_comp_x, CEED_EVAL_INTERP));
    RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_mass, "v", num_comp_x, CEED_EVAL_INTERP));
    RatelCallCeed(ratel, CeedQFunctionContextCreate(ceed, &ctx_mass));
    RatelCallCeed(ratel, CeedQFunctionContextSetData(ctx_mass, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(dim), &dim));
    RatelCallCeed(ratel, CeedQFunctionSetContext(qf_mass, ctx_mass));
    RatelCallCeed(ratel, CeedQFunctionSetContextWritable(qf_mass, PETSC_FALSE));

    // Centroid QFunction
    RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, ComputeCentroid, RatelQFunctionRelativePath(ComputeCentroid_loc), &qf_centroid));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_centroid, "q data", q_data_size, CEED_EVAL_NONE));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_centroid, "x", num_comp_x, CEED_EVAL_INTERP));
    RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_centroid, "centroid", num_comp_x, CEED_EVAL_INTERP));

    // Compute contribution on each boundary face
    for (CeedInt i = 0; i < ratel->surface_force_face_count; i++) {
      PetscBool            has_face;
      CeedVector           q_data;
      CeedElemRestriction  restriction_x_face, restriction_q_data;
      CeedQFunction        qf_setup = NULL;
      CeedQFunctionContext ctx_bounding_box;
      CeedOperator         op_setup, op_mass, op_centroid;

      PetscCall(RatelDebug(ratel, "---- Setting up centroid on surface face %" CeedInt_FMT, i));

      // Check face exists
      PetscCall(RatelDMHasFace(ratel, dm, ratel->surface_force_face_label_values[i], &has_face));
      PetscCheck(has_face, ratel->comm, PETSC_ERR_SUP, "Face label %" PetscInt_FMT " does not exist.", ratel->surface_force_face_label_values[i]);

      // -- Restrictions
      PetscCall(RatelDebug(ratel, "---- Setting up libCEED restrictions"));
      PetscCall(RatelDMPlexCeedElemRestrictionCoordinateCreate(ratel, dm, domain_label, ratel->surface_force_face_label_values[i], height_face,
                                                               &restriction_x_face));
      PetscCall(RatelDMPlexCeedElemRestrictionQDataCreate(ratel, dm, domain_label, ratel->surface_force_face_label_values[i], height_face,
                                                          q_data_size, &restriction_q_data));
      RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_q_data, &q_data, NULL));

      // -- Setup QFunction
      RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, SetupSurfaceGeometryBounded,
                                                       RatelQFunctionRelativePath(SetupSurfaceGeometryBounded_loc), &qf_setup));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_setup, "x", num_comp_x, CEED_EVAL_INTERP));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_setup, "dx/dX", num_comp_x * (num_comp_x - height_face), CEED_EVAL_GRAD));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_setup, "weight", 1, CEED_EVAL_WEIGHT));
      RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_setup, "q data", q_data_size, CEED_EVAL_NONE));

      // -- Setup QFunction context
      RatelCallCeed(ratel, CeedQFunctionContextCreate(ceed, &ctx_bounding_box));
      {
        RatelBoundingBoxParams params_bounding_box;

        PetscCall(RatelBoundingBoxParamsFromOptions(ratel, "surface_force_face_", ratel->surface_force_face_names[i], &params_bounding_box));
        RatelCallCeed(
            ratel, CeedQFunctionContextSetData(ctx_bounding_box, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(params_bounding_box), &params_bounding_box));
      }
      RatelCallCeed(ratel, CeedQFunctionSetContext(qf_setup, ctx_bounding_box));
      RatelCallCeed(ratel, CeedQFunctionSetContextWritable(qf_setup, PETSC_FALSE));

      // -- Setup Operator
      RatelCallCeed(ratel, CeedOperatorCreate(ceed, qf_setup, NULL, NULL, &op_setup));
      RatelCallCeed(ratel, CeedOperatorSetName(op_setup, "surface geometric data"));
      RatelCallCeed(ratel, CeedOperatorSetField(op_setup, "x", restriction_x_face, basis_x_face, CEED_VECTOR_ACTIVE));
      RatelCallCeed(ratel, CeedOperatorSetField(op_setup, "dx/dX", restriction_x_face, basis_x_face, CEED_VECTOR_ACTIVE));
      RatelCallCeed(ratel, CeedOperatorSetField(op_setup, "weight", CEED_ELEMRESTRICTION_NONE, basis_x_face, CEED_VECTOR_NONE));
      RatelCallCeed(ratel, CeedOperatorSetField(op_setup, "q data", restriction_q_data, CEED_BASIS_NONE, CEED_VECTOR_ACTIVE));

      // -- Compute geometric factors
      PetscCall(RatelDebug(ratel, "---- Computing geometric factors"));
      if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(op_setup, stdout));
      RatelCallCeed(ratel, CeedOperatorApply(op_setup, x_loc, q_data, CEED_REQUEST_IMMEDIATE));

      // -- Surface area operator
      PetscCall(RatelDebug(ratel, "---- Setting surface area operator"));
      RatelCallCeed(ratel, CeedOperatorCreate(ceed, qf_mass, NULL, NULL, &op_mass));
      {
        char operator_name[PETSC_MAX_PATH_LEN];

        PetscCall(PetscSNPrintf(operator_name, sizeof(operator_name), "surface area - face %s", ratel->surface_force_face_names[i]));
        RatelCallCeed(ratel, CeedOperatorSetName(op_mass, operator_name));
      }
      RatelCallCeed(ratel, CeedOperatorSetField(op_mass, "q data", restriction_q_data, CEED_BASIS_NONE, q_data));
      RatelCallCeed(ratel, CeedOperatorSetField(op_mass, "u", restriction_x_face, basis_x_face, CEED_VECTOR_ACTIVE));
      RatelCallCeed(ratel, CeedOperatorSetField(op_mass, "v", restriction_x_face, basis_x_face, CEED_VECTOR_ACTIVE));

      // -- Compute surface area
      PetscCall(RatelDebug(ratel, "---- Computing surface area"));
      if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(op_mass, stdout));
      RatelCallCeed(ratel, CeedOperatorApply(op_mass, one, centroid, CEED_REQUEST_IMMEDIATE));
      {
        CeedSize          length;
        const CeedScalar *array;

        RatelCallCeed(ratel, CeedVectorGetLength(centroid, &length));
        RatelCallCeed(ratel, CeedVectorGetArrayRead(centroid, CEED_MEM_HOST, &array));
        surface_area_local = 0.0;
        for (CeedInt j = 0; j < length; j++) surface_area_local += array[j];
        surface_area_local /= dim;
        RatelCallCeed(ratel, CeedVectorRestoreArrayRead(centroid, &array));
      }
      PetscCall(MPIU_Allreduce(&surface_area_local, &ratel->initial_surface_area[i], 1, MPIU_REAL, MPI_SUM, ratel->comm));
      PetscCall(RatelDebug(ratel, "------   Surface area: %0.10e", ratel->initial_surface_area[i]));

      // -- Centroid QFunction
      PetscCall(RatelDebug(ratel, "---- Setting up centroid operator"));
      RatelCallCeed(ratel, CeedOperatorCreate(ceed, qf_centroid, NULL, NULL, &op_centroid));
      {
        char operator_name[PETSC_MAX_PATH_LEN];

        PetscCall(PetscSNPrintf(operator_name, sizeof(operator_name), "centroid - face %s", ratel->surface_force_face_names[i]));
        RatelCallCeed(ratel, CeedOperatorSetName(op_centroid, operator_name));
      }
      RatelCallCeed(ratel, CeedOperatorSetField(op_centroid, "q data", restriction_q_data, CEED_BASIS_NONE, q_data));
      RatelCallCeed(ratel, CeedOperatorSetField(op_centroid, "x", restriction_x_face, basis_x_face, CEED_VECTOR_ACTIVE));
      RatelCallCeed(ratel, CeedOperatorSetField(op_centroid, "centroid", restriction_x_face, basis_x_face, CEED_VECTOR_ACTIVE));

      // -- Compute initial centroid
      PetscCall(RatelDebug(ratel, "---- Computing initial centroid"));
      if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(op_centroid, stdout));
      RatelCallCeed(ratel, CeedOperatorApply(op_centroid, x_loc, centroid, CEED_REQUEST_IMMEDIATE));
      {
        CeedSize          length;
        const CeedScalar *array;

        RatelCallCeed(ratel, CeedVectorGetLength(centroid, &length));
        RatelCallCeed(ratel, CeedVectorGetArrayRead(centroid, CEED_MEM_HOST, &array));
        for (CeedInt d = 0; d < dim; d++) centroid_local[d] = 0.0;
        for (CeedInt j = 0; j < length / dim; j++) {
          for (CeedInt d = 0; d < dim; d++) centroid_local[d] += array[j * dim + d];
        }
        RatelCallCeed(ratel, CeedVectorRestoreArrayRead(centroid, &array));
      }
      PetscCall(MPIU_Allreduce(centroid_local, &ratel->initial_surface_centroid[i], dim, MPIU_REAL, MPI_SUM, ratel->comm));
      for (CeedInt d = 0; d < dim; d++) ratel->initial_surface_centroid[i][d] /= ratel->initial_surface_area[i];
      PetscCall(RatelDebug(ratel, "------   Centroid: [%0.10e, %0.10e, %0.10e]", ratel->initial_surface_centroid[i][0],
                           ratel->initial_surface_centroid[i][1], ratel->initial_surface_centroid[i][2]));

      // -- Cleanup
      RatelCallCeed(ratel, CeedVectorDestroy(&q_data));
      RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_x_face));
      RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_q_data));
      RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_setup));
      RatelCallCeed(ratel, CeedQFunctionContextDestroy(&ctx_bounding_box));
      RatelCallCeed(ratel, CeedOperatorDestroy(&op_setup));
      RatelCallCeed(ratel, CeedOperatorDestroy(&op_centroid));
      RatelCallCeed(ratel, CeedOperatorDestroy(&op_mass));
    }
    // Cleanup
    PetscCall(VecReadCeedToPetsc(x_loc, x_mem_type, X_loc));
    RatelCallCeed(ratel, CeedVectorDestroy(&x_loc));
    RatelCallCeed(ratel, CeedVectorDestroy(&centroid));
    RatelCallCeed(ratel, CeedVectorDestroy(&one));
    RatelCallCeed(ratel, CeedBasisDestroy(&basis_x_face));
    RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_mass));
    RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_centroid));
    RatelCallCeed(ratel, CeedQFunctionContextDestroy(&ctx_mass));
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Setup Surface Force Centroid Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup average surface displacement `CeedOperators`

  @param[in]   ratel                     `Ratel` context
  @param[in]   dm_surface_displacement   `DM` for surface displacement computation
  @param[out]  ops_surface_displacement  Composite surface displacement `CeedOperator` objects to add `RatelMaterial` suboperator to

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelBoundarySetupSurfaceDisplacementSuboperators(Ratel ratel, DM dm_surface_displacement, CeedOperator ops_surface_displacement[]) {
  PetscInt       dim;
  const PetscInt height_face = 1;
  PetscMemType   x_mem_type;
  DMLabel        face_sets_label = NULL;
  Vec            X_loc;
  Ceed           ceed = ratel->ceed;
  CeedInt        num_comp_x, num_comp_u = 3, q_data_size = 4;
  CeedVector     x_loc = NULL, q_data;
  CeedBasis      basis_x_face, basis_u_face;
  CeedQFunction  qf_surface_displacement = NULL;

  PetscFunctionBeginUser;
  if (ratel->surface_force_face_count == 0) PetscFunctionReturn(PETSC_SUCCESS);
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Setup Surface Displacement Suboperators"));

  // DM information
  {
    const CeedInt *field_sizes;

    PetscCall(DMGetDimension(dm_surface_displacement, &dim));
    num_comp_x = dim;
    PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[0], NULL, &field_sizes));
    num_comp_u = field_sizes[0];  // First field should be displacement
  }

  // Coordinates vector
  {
    PetscInt x_size;

    PetscCall(DMGetCoordinatesLocal(dm_surface_displacement, &X_loc));
    PetscCall(VecGetLocalSize(X_loc, &x_size));
    RatelCallCeed(ratel, CeedVectorCreate(ratel->ceed, x_size, &x_loc));
    PetscCall(VecReadPetscToCeed(X_loc, &x_mem_type, x_loc));
  }

  // CeedBases
  PetscCall(RatelDMPlexCeedBasisCreate(ratel, dm_surface_displacement, NULL, 0, height_face, 0, &basis_u_face));
  PetscCall(RatelDMPlexCeedBasisCoordinateCreate(ratel, dm_surface_displacement, NULL, 0, height_face, &basis_x_face));

  // QFunction
  // Surface displacement QFunction
  RatelCallCeed(ratel,
                CeedQFunctionCreateInterior(ceed, 1, ComputeCentroid, RatelQFunctionRelativePath(ComputeCentroid_loc), &qf_surface_displacement));
  RatelCallCeed(ratel, CeedQFunctionAddInput(qf_surface_displacement, "q data", q_data_size, CEED_EVAL_NONE));
  RatelCallCeed(ratel, CeedQFunctionAddInput(qf_surface_displacement, "u", num_comp_u, CEED_EVAL_INTERP));
  RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_surface_displacement, "v", num_comp_x, CEED_EVAL_INTERP));

  // Domain label for Face Sets
  PetscCall(DMGetLabel(dm_surface_displacement, "Face Sets", &face_sets_label));

  // Surface displacement computation
  for (PetscInt i = 0; i < ratel->surface_force_face_count; i++) {
    PetscInt             face_id = ratel->surface_force_face_label_values[i];
    CeedElemRestriction  restriction_u_face, restriction_x_face, restriction_q_data;
    CeedQFunction        qf_setup = NULL;
    CeedQFunctionContext ctx_bounding_box;
    CeedOperator         sub_op_surface_displacement, op_setup;

    PetscCall(RatelDebug(ratel, "---- Setting up surface %" PetscInt_FMT " average displacement computation", face_id));

    PetscCall(RatelDMPlexCeedElemRestrictionCreate(ratel, dm_surface_displacement, face_sets_label, face_id, height_face, 0, &restriction_u_face));
    PetscCall(
        RatelDMPlexCeedElemRestrictionCoordinateCreate(ratel, dm_surface_displacement, face_sets_label, face_id, height_face, &restriction_x_face));
    PetscCall(RatelDMPlexCeedElemRestrictionQDataCreate(ratel, dm_surface_displacement, face_sets_label, face_id, height_face, q_data_size,
                                                        &restriction_q_data));
    RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_q_data, &q_data, NULL));

    // -- Setup QFunction
    RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, SetupSurfaceGeometryBounded,
                                                     RatelQFunctionRelativePath(SetupSurfaceGeometryBounded_loc), &qf_setup));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_setup, "x", num_comp_x, CEED_EVAL_INTERP));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_setup, "dx/dX", num_comp_x * (num_comp_x - height_face), CEED_EVAL_GRAD));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_setup, "weight", 1, CEED_EVAL_WEIGHT));
    RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_setup, "q data", q_data_size, CEED_EVAL_NONE));

    // -- Setup QFunctionContext
    RatelCallCeed(ratel, CeedQFunctionContextCreate(ceed, &ctx_bounding_box));
    {
      RatelBoundingBoxParams bounding_box;

      PetscCall(RatelBoundingBoxParamsFromOptions(ratel, "surface_force_face_", ratel->surface_force_face_names[i], &bounding_box));
      RatelCallCeed(ratel, CeedQFunctionContextSetData(ctx_bounding_box, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(bounding_box), &bounding_box));
    }
    RatelCallCeed(ratel, CeedQFunctionSetContext(qf_setup, ctx_bounding_box));
    RatelCallCeed(ratel, CeedQFunctionSetContextWritable(qf_setup, PETSC_FALSE));

    // -- Setup Operator
    RatelCallCeed(ratel, CeedOperatorCreate(ceed, qf_setup, NULL, NULL, &op_setup));
    RatelCallCeed(ratel, CeedOperatorSetName(op_setup, "surface geometric data"));
    RatelCallCeed(ratel, CeedOperatorSetField(op_setup, "x", restriction_x_face, basis_x_face, CEED_VECTOR_ACTIVE));
    RatelCallCeed(ratel, CeedOperatorSetField(op_setup, "dx/dX", restriction_x_face, basis_x_face, CEED_VECTOR_ACTIVE));
    RatelCallCeed(ratel, CeedOperatorSetField(op_setup, "weight", CEED_ELEMRESTRICTION_NONE, basis_x_face, CEED_VECTOR_NONE));
    RatelCallCeed(ratel, CeedOperatorSetField(op_setup, "q data", restriction_q_data, CEED_BASIS_NONE, CEED_VECTOR_ACTIVE));

    // -- Compute geometric factors
    PetscCall(RatelDebug(ratel, "---- Computing geometric factors"));
    if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(op_setup, stdout));
    RatelCallCeed(ratel, CeedOperatorApply(op_setup, x_loc, q_data, CEED_REQUEST_IMMEDIATE));

    // -- Surface displacement operator
    PetscCall(RatelDebug(ratel, "---- Setting up average displacement operator"));
    RatelCallCeed(ratel, CeedOperatorCreate(ceed, qf_surface_displacement, NULL, NULL, &sub_op_surface_displacement));
    {
      char operator_name[PETSC_MAX_PATH_LEN];

      PetscCall(PetscSNPrintf(operator_name, sizeof(operator_name), "average displacement - face %s", ratel->surface_force_face_names[i]));
      RatelCallCeed(ratel, CeedOperatorSetName(sub_op_surface_displacement, operator_name));
    }
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_surface_displacement, "q data", restriction_q_data, CEED_BASIS_NONE, q_data));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_surface_displacement, "u", restriction_u_face, basis_u_face, CEED_VECTOR_ACTIVE));
    RatelCallCeed(ratel, CeedOperatorSetField(sub_op_surface_displacement, "v", restriction_x_face, basis_x_face, CEED_VECTOR_ACTIVE));

    // -- Add to composite operator
    RatelCallCeed(ratel, CeedCompositeOperatorAddSub(ops_surface_displacement[i], sub_op_surface_displacement));
    if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_surface_displacement, stdout));

    // -- Cleanup
    RatelCallCeed(ratel, CeedVectorDestroy(&q_data));
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_u_face));
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_x_face));
    RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_q_data));
    RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_setup));
    RatelCallCeed(ratel, CeedQFunctionContextDestroy(&ctx_bounding_box));
    RatelCallCeed(ratel, CeedOperatorDestroy(&op_setup));
    RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_surface_displacement));
  }
  // Cleanup
  PetscCall(VecReadCeedToPetsc(x_loc, x_mem_type, X_loc));
  RatelCallCeed(ratel, CeedVectorDestroy(&x_loc));
  RatelCallCeed(ratel, CeedBasisDestroy(&basis_u_face));
  RatelCallCeed(ratel, CeedBasisDestroy(&basis_x_face));
  RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_surface_displacement));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Setup Surface Displacement Suboperator Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Register material specific fields for platen boundary conditions `CeedQFunctionContext`

  Not collective across MPI processes.

  @param[in]      material  `RatelMaterial` to setup platen context
  @param[in,out]  ctx       `CeedQFunctionContext` for platen

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelMaterialPlatenContextRegisterFields(RatelMaterial material, CeedQFunctionContext ctx) {
  Ratel                   ratel           = material->ratel;
  RatelModelParameterData param_data      = material->model_data->param_data;
  size_t                  material_offset = offsetof(RatelBCPlatenParams, material), offset = 0;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Register Platen Context Neo-Hookean"));

  // Register parameters
  for (PetscInt i = 0; i < param_data->num_parameters; i++) {
    offset = material_offset + param_data->parameters[i].offset;
    RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(ctx, param_data->parameters[i].name, offset, 1, param_data->parameters[i].description));
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Register Platen Context Neo-Hookean Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Create libCEED `CeedQFunctionContext` for platen boundary conditions.

  Not collective across MPI processes.

  @param[in]   material       `RatelMaterial` to setup platen context
  @param[in]   params_platen  `RatelBCPlatenParamsCommon` for platen
  @param[out]  ctx            `CeedQFunctionContext` for platen

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelCeedPlatenContextCreate(RatelMaterial material, const RatelBCPlatenParamsCommon *params_platen,
                                                   CeedQFunctionContext *ctx) {
  size_t            material_size;
  const CeedScalar *material_ctx;
  Ratel             ratel = material->ratel;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Create Platen Parameters Context"));

  // Create platen context
  RatelBCPlatenParams material_platen_params = {
      .time     = RATEL_UNSET_INITIAL_TIME,
      .material = {0},
  };
  // -- Copy platen parameters
  PetscCall(PetscMemcpy(&material_platen_params.platen, params_platen, sizeof(RatelBCPlatenParamsCommon)));

  // -- Copy material parameters
  RatelCallCeed(ratel, CeedQFunctionContextGetContextSize(material->ctx_params, &material_size));
  RatelCallCeed(ratel, CeedQFunctionContextGetDataRead(material->ctx_params, CEED_MEM_HOST, &material_ctx));
  PetscCheck(material_size <= sizeof(material_platen_params.material), ratel->comm, PETSC_ERR_SUP,
             "Material context size %" PetscInt_FMT " exceeds maximum size %" PetscInt_FMT, (PetscInt)material_size,
             (PetscInt)sizeof(material_platen_params.material));
  PetscCall(PetscMemcpy(&material_platen_params.material, material_ctx, material_size));
  RatelCallCeed(ratel, CeedQFunctionContextRestoreDataRead(material->ctx_params, &material_ctx));

  // Create QFunctionContext
  RatelCallCeed(ratel, CeedQFunctionContextCreate(ratel->ceed, ctx));
  RatelCallCeed(ratel, CeedQFunctionContextSetData(*ctx, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(RatelBCPlatenParams), &material_platen_params));

  // Register fields
  // Platen parameters
  RatelCallCeed(ratel,
                CeedQFunctionContextRegisterDouble(*ctx, "normal", offsetof(RatelBCPlatenParams, platen.normal), 3, "external normal of platen"));
  RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(*ctx, "center", offsetof(RatelBCPlatenParams, platen.center), 3, "center of platen"));
  RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(*ctx, "distance", offsetof(RatelBCPlatenParams, platen.distance), params_platen->num_times,
                                                          "displacement of platen along normal"));
  RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(*ctx, "times", offsetof(RatelBCPlatenParams, platen.times), params_platen->num_times,
                                                          "transition times for each distance value"));
  RatelCallCeed(
      ratel, CeedQFunctionContextRegisterInt32(*ctx, "num times", offsetof(RatelBCPlatenParams, platen.num_times), 1, "Number of transition times"));
  RatelCallCeed(ratel,
                CeedQFunctionContextRegisterDouble(*ctx, "gamma", offsetof(RatelBCPlatenParams, platen.gamma), 1, "Nitsche's method parameter"));
  RatelCallCeed(ratel, CeedQFunctionContextRegisterInt32(*ctx, "face id", offsetof(RatelBCPlatenParams, platen.face_id), 1, "DMPlex face domain id"));
  RatelCallCeed(ratel, CeedQFunctionContextRegisterInt32(*ctx, "face domain value", offsetof(RatelBCPlatenParams, platen.face_domain_value), 1,
                                                         "Stratum value for face domain and orientation"));
  RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(*ctx, "time", offsetof(RatelBCPlatenParams, time), 1, "current solver time"));

  // Material parameters
  PetscCall(RatelMaterialPlatenContextRegisterFields(material, *ctx));

  // Debugging output
  if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedQFunctionContextView(*ctx, stdout));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Create Platen Parameters Context Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Print friction parameters to a viewer.

  Collective across MPI processes.

  @param[in]      ratel            `Ratel` context
  @param[in]      params_friction  `RatelFrictionParams` to print
  @param[in,out]  viewer           `PetscViewer` to print to

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelFrictionParamsView(Ratel ratel, const RatelFrictionParams *params_friction, PetscViewer viewer) {
  PetscFunctionBeginUser;
  PetscCall(PetscViewerASCIIPrintf(viewer, "Friction Model: %s\n", RatelFrictionTypes[params_friction->model]));
  PetscCall(PetscViewerASCIIPushTab(viewer));
  PetscCall(PetscViewerASCIIPrintf(viewer, "Static Coefficient of Friction:    %g\n", params_friction->static_coefficient));
  PetscCall(PetscViewerASCIIPrintf(viewer, "Kinetic Coefficient of Friction:   %g\n", params_friction->kinetic_coefficient));
  PetscCall(PetscViewerASCIIPrintf(viewer, "Coeficient of Viscosity:           %g\n", params_friction->viscous_coefficient));
  PetscCall(PetscViewerASCIIPrintf(viewer, "Tolerance Velocity:                %g\n", params_friction->tolerance_velocity));
  PetscCall(PetscViewerASCIIPopTab(viewer));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Check Coulomb friction parameters.

  Collective across MPI processes.

  @param[in]  ratel            `Ratel` context
  @param[in]  params_friction  Friction parameters to check

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelFrictionParamsCheck_Coulomb(Ratel ratel, RatelFrictionParams *params_friction) {
  PetscFunctionBeginUser;
  if (params_friction->model != RATEL_FRICTION_COULOMB) PetscFunctionReturn(PETSC_SUCCESS);
  PetscCheck(params_friction->kinetic_coefficient >= 0.0, ratel->comm, PETSC_ERR_USER_INPUT,
             "Kinetic coefficient of friction must be non-negative for Coulomb friction model.");
  PetscCheck(params_friction->viscous_coefficient >= 0.0, ratel->comm, PETSC_ERR_USER_INPUT,
             "Viscous damping coefficient must be non-negative for Coulomb friction model.");
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Check Threlfall friction parameters.

  Collective across MPI processes.

  @param[in]  ratel            `Ratel` context
  @param[in]  params_friction  Friction parameters to check

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelFrictionParamsCheck_Threlfall(Ratel ratel, RatelFrictionParams *params_friction) {
  PetscFunctionBeginUser;
  if (params_friction->model != RATEL_FRICTION_THRELFALL) PetscFunctionReturn(PETSC_SUCCESS);
  PetscCheck(params_friction->kinetic_coefficient >= 0.0, ratel->comm, PETSC_ERR_USER_INPUT,
             "Kinetic coefficient of friction must be non-negative for Threlfall friction model.");
  PetscCheck(params_friction->viscous_coefficient >= 0.0, ratel->comm, PETSC_ERR_USER_INPUT,
             "Viscous damping coefficient must be non-negative for Threlfall friction model.");
  PetscCheck(params_friction->tolerance_velocity > CEED_EPSILON, ratel->comm, PETSC_ERR_USER_INPUT,
             "Tolerance velocity must be positive for Threlfall friction model.");
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Set friction parameters from command line options.

  Collective across MPI processes.

  @param[in]   ratel            `Ratel` context
  @param[in]   option_prefix    Prefix string for command line options
  @param[out]  params_friction  `RatelFrictionParams` to set

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelFrictionParamsFromOptions(Ratel ratel, const char option_prefix[], RatelFrictionParams *params_friction) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Friction Parameters From Options"));

  // Set default values
  params_friction->model               = RATEL_FRICTION_NONE;
  params_friction->static_coefficient  = 0.15;  // For stiction, currently unused
  params_friction->kinetic_coefficient = 0.1;   // For slipping friction
  params_friction->viscous_coefficient = 0.0;   // Ns/m, for lubricated friction
  params_friction->tolerance_velocity  = 0.05;  // m/s, for regularized slipping friction

  // Read CL options for friction parameters
  PetscOptionsBegin(ratel->comm, option_prefix, "Friction Parameters", NULL);
  // -- Friction model
  PetscCall(PetscOptionsEnum("-type", "Set friction model type", NULL, RatelFrictionTypesCL, (PetscEnum)params_friction->model,
                             (PetscEnum *)&params_friction->model, NULL));
  // -- Static coefficient
  PetscCall(PetscOptionsScalar("-static", "Set static coefficient of friction", NULL, params_friction->static_coefficient,
                               &params_friction->static_coefficient, NULL));
  // -- Kinetic coefficient
  PetscCall(PetscOptionsScalar("-kinetic", "Set kinetic coefficient of friction", NULL, params_friction->kinetic_coefficient,
                               &params_friction->kinetic_coefficient, NULL));
  // -- Viscous coefficient
  PetscCall(PetscOptionsScalar("-viscous", "Set viscous coefficient of friction", NULL, params_friction->viscous_coefficient,
                               &params_friction->viscous_coefficient, NULL));
  // -- Tolerance velocity
  PetscCall(PetscOptionsScalar("-tolerance_velocity", "Set tolerance velocity for regularized friction", NULL, params_friction->tolerance_velocity,
                               &params_friction->tolerance_velocity, NULL));
  PetscOptionsEnd();  // End of setting friction parameters

  PetscCall(RatelFrictionParamsCheck_Coulomb(ratel, params_friction));
  PetscCall(RatelFrictionParamsCheck_Threlfall(ratel, params_friction));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Friction Parameters From Options Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Print platen boundary condition parameters to a viewer.

  Collective across MPI processes.

  @param[in]      ratel          `Ratel` context
  @param[in]      params_platen  `RatelBCPlatenParamsCommon` to print
  @param[in,out]  viewer         `PetscViewer` to print to

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelBoundaryPlatenParamsCommonView(Ratel ratel, const RatelBCPlatenParamsCommon *params_platen, PetscViewer viewer) {
  PetscFunctionBeginUser;
  PetscCall(PetscViewerASCIIPrintf(viewer, "Face %s:\n", ratel->bc_platen_names[params_platen->name_index]));
  PetscCall(PetscViewerASCIIPushTab(viewer));
  PetscCall(PetscViewerASCIIPrintf(viewer, "Contact type: %s\n", RatelPlatenTypes[ratel->bc_platen_types[params_platen->name_index]]));
  PetscCall(
      PetscViewerASCIIPrintf(viewer, "Platen center: [%f, %f, %f]\n", params_platen->center[0], params_platen->center[1], params_platen->center[2]));
  PetscCall(
      PetscViewerASCIIPrintf(viewer, "Platen normal: [%f, %f, %f]\n", params_platen->normal[0], params_platen->normal[1], params_platen->normal[2]));
  PetscCall(PetscViewerASCIIPrintf(viewer, "Nitsche's method/penalty parameter: %f\n", params_platen->gamma));
  PetscCall(RatelFrictionParamsView(ratel, &params_platen->friction, viewer));
  PetscCall(PetscViewerASCIIPrintf(viewer, "Interpolation type: %s\n", RatelBCInterpolationTypesCL[params_platen->interpolation_type]));
  PetscCall(PetscViewerASCIIPrintf(viewer, "Number of transition times: %d\n", params_platen->num_times));
  PetscCall(PetscViewerASCIIPushTab(viewer));
  for (PetscInt t = 0; t < params_platen->num_times; t++) {
    PetscCall(PetscViewerASCIIPrintf(viewer, "Distance at time %f: %f\n", params_platen->times[t], params_platen->distance[t]));
  }
  PetscCall(PetscViewerASCIIPopTab(viewer));
  PetscCall(PetscViewerASCIIPopTab(viewer));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Read platen boundary condition parameters from options.

  Collective across MPI processes.

  @param[in]   ratel           `Ratel` context
  @param[in]   options_prefix  Prefix string for command line options
  @param[out]  params_platen   `RatelBCPlatenParamsCommon` to set from options, must be allocated

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelBoundaryPlatenParamsCommonFromOptions(Ratel ratel, const char options_prefix[], RatelBCPlatenParamsCommon *params_platen) {
  PetscBool set_normal, set_center, set_distance, set_times;
  PetscInt  max_num_times = RATEL_MAX_BC_INTERP_POINTS, max_num_distance = RATEL_MAX_BC_INTERP_POINTS;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Boundary Platen Parameters From Options with prefix %s",
                          options_prefix ? options_prefix : "(NULL)"));

  // Clear parameters
  PetscCall(PetscMemzero(params_platen, sizeof(*params_platen)));

  // Set default values
  params_platen->normal[0]          = 0.0;
  params_platen->normal[1]          = 0.0;
  params_platen->normal[2]          = -1.0;
  params_platen->center[0]          = 0.0;
  params_platen->center[1]          = 0.0;
  params_platen->center[2]          = 0.0;
  params_platen->times[0]           = 1.0;
  params_platen->num_times          = 1;
  params_platen->interpolation_type = RATEL_BC_INTERP_LINEAR;
  params_platen->gamma              = 500.0;

  // Read CL options for platen conditions
  {
    PetscInt max_n = 3;

    PetscOptionsBegin(ratel->comm, options_prefix, "Platen BC Parameters", NULL);
    // -- Normal vector
    PetscCall(PetscOptionsScalarArray("-normal", "Exterior normal to the half-space platen", NULL, params_platen->normal, &max_n, &set_normal));
    // -- Center point
    PetscCall(PetscOptionsScalarArray("-center", "Point on the surface of the half-plane platen", NULL, params_platen->center, &max_n, &set_center));
    // -- Total distance
    PetscCall(PetscOptionsScalarArray("-distance", "Distance to move platen along normal vector", NULL, params_platen->distance, &max_num_distance,
                                      &set_distance));
    // -- Transition times
    PetscCall(PetscOptionsScalarArray("-times", "Transition times for each distance value", NULL, params_platen->times, &max_num_times, &set_times));
    // -- Interpolation type
    PetscCall(PetscOptionsEnum("-interpolation", "Set interpolation type for platen BC", NULL, RatelBCInterpolationTypesCL,
                               (PetscEnum)params_platen->interpolation_type, (PetscEnum *)&params_platen->interpolation_type, NULL));
    // -- Nitsche parameter
    PetscCall(PetscOptionsScalar("-gamma", "Nitsche's method penalty parameter", NULL, params_platen->gamma, &params_platen->gamma, NULL));
    // -- Deprecated friction argument
    {
      char help[PETSC_MAX_PATH_LEN];
      PetscCall(PetscSNPrintf(help, sizeof(help),
                              "For Coulomb friction, use `-%sfriction_type coulomb -%sfriction_kinetic [value]` instead of `-%sf [value]`",
                              options_prefix, options_prefix, options_prefix));
      PetscCall(PetscOptionsDeprecated("-f", NULL, "0.4.0", help));
    }
    PetscOptionsEnd();  // End of setting platen conditions
  }

  // Check required parameters
  PetscCheck(set_normal, ratel->comm, PETSC_ERR_SUP, "-%snormal option needed", options_prefix);
  PetscCheck(set_center, ratel->comm, PETSC_ERR_SUP, "-%scenter option needed", options_prefix);

  // -- Normalize
  const CeedScalar norm_normal = RatelNorm3((CeedScalar *)params_platen->normal);

  PetscCheck(norm_normal > CEED_EPSILON, ratel->comm, PETSC_ERR_SUP, "-%snormal must be non-zero vector", options_prefix);
  for (PetscInt j = 0; j < 3; j++) params_platen->normal[j] /= norm_normal;

  // -- Validate flexible BCs
  if (set_times) {
    params_platen->num_times = max_num_times;

    // ---- Ensure times are monotonically increasing
    PetscBool monotonic = PETSC_TRUE;

    for (PetscInt j = 0; j < params_platen->num_times - 1; j++) {
      if (params_platen->times[j] >= params_platen->times[j + 1]) {
        monotonic = PETSC_FALSE;
      }
    }
    PetscCheck(monotonic, ratel->comm, PETSC_ERR_SUP, "Transition times must be monotonically increasing.");

    // ---- Check bounds of times
    PetscCheck(params_platen->times[0] >= 0, ratel->comm, PETSC_ERR_SUP, "First time must be greater than or equal to 0.");
  }

  // Default distance of 0
  if (!set_distance) max_num_distance = 1;

  // -- Check number of times and distances are compatible
  PetscCheck((params_platen->num_times == 1) || (set_distance && set_times), ratel->comm, PETSC_ERR_SUP,
             "Distances must be set if more than one time is specified.");
  PetscCheck(params_platen->num_times == max_num_distance, ratel->comm, PETSC_ERR_SUP, "Number of times must be equal to the number of distances.");

  // -- Friction parameters
  {
    char friction_suffix[]                = "friction_";
    char cl_prefix[PETSC_MAX_OPTION_NAME] = {0};
    PetscCall(PetscSNPrintf(cl_prefix, sizeof(cl_prefix), "%s%s", options_prefix, friction_suffix));
    PetscCall(RatelFrictionParamsFromOptions(ratel, cl_prefix, &params_platen->friction));
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Boundary Platen Parameters From Options Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Process command line options for platen boundary conditions.

  Collective across MPI processes.

  @param[in]   material          `RatelMaterial` to setup platen context
  @param[in]   platen_name        `const char *` name of platen
  @param[in]   face_id           `DMPlex` face domain id
  @param[in]   face_orientation  `DMPlex` face domain stratum value
  @param[in]   name_index        Index of platen in name array
  @param[out]  ctx               `CeedQFunctionContext` for platen

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelMaterialPlatenContextCreate(RatelMaterial material, const char *platen_name, PetscInt face_id, PetscInt face_orientation,
                                                       PetscInt name_index, CeedQFunctionContext *ctx) {
  Ratel                      ratel                            = material->ratel;
  char                       cl_prefix[PETSC_MAX_OPTION_NAME] = {0};
  RatelBCPlatenParamsCommon *params_platen;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup Model Platen Parameters Context"));

  PetscCall(PetscNew(&params_platen));

  PetscCall(PetscSNPrintf(cl_prefix, sizeof(cl_prefix), "bc_platen_%s_", platen_name));
  PetscCall(RatelBoundaryPlatenParamsCommonFromOptions(material->ratel, cl_prefix, params_platen));
  params_platen->face_id           = face_id;
  params_platen->face_domain_value = face_orientation;
  params_platen->name_index        = name_index;

  if (ratel->is_debug) PetscCall(RatelBoundaryPlatenParamsCommonView(ratel, params_platen, PETSC_VIEWER_STDOUT_WORLD));

  // Create context
  PetscCall(RatelCeedPlatenContextCreate(material, params_platen, ctx));

  // Cleanup
  PetscCall(PetscFree(params_platen));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Setup Model Platen Parameters Context Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Add platen contact boundary conditions to the residual u term and Jacobian operators.

  Collective across MPI processes.

  @param[in]   material     `RatelMaterial` context
  @param[in]   u_dot_loc    `CeedVector` for passive input velocity field
  @param[out]  op_residual  Composite residual u term `CeedOperator` to add RatelMaterial sub-operator
  @param[out]  op_jacobian  Composite Jacobian `CeedOperator` to add `RatelMaterial` sub-operator

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialCeedAddBoundariesPlatenNitsche(RatelMaterial material, CeedVector u_dot_loc, CeedOperator op_residual,
                                                           CeedOperator op_jacobian) {
  // ---- Get material data
  Ratel          ratel        = material->ratel;
  RatelModelData model_params = material->model_data;
  PetscInt       dim;
  const PetscInt height_face = 1, height_cell = 0;
  DM             dm_solution;
  Ceed           ceed                     = ratel->ceed;
  CeedInt        num_active_fields        = model_params->num_active_fields;
  const CeedInt  num_stored_values_platen = 4,
                num_comp_stored_u = model_params->num_comp_stored_platen > 0 ? model_params->num_comp_stored_platen : model_params->num_comp_stored_u;
  const CeedInt      *active_field_sizes          = model_params->active_field_sizes;
  CeedInt             num_comp_u                  = active_field_sizes[0];
  const CeedInt      *active_field_num_eval_modes = model_params->active_field_num_eval_modes;
  const CeedEvalMode *active_field_eval_modes     = model_params->active_field_eval_modes;
  CeedVector          x_loc                       = NULL;
  CeedBasis           basis_x_face;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Ceed Add Boundaries Platen"));

  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));

  if (ratel->bc_platen_count > 0 && num_active_fields > 0) {
    PetscCall(DMGetDimension(dm_solution, &dim));

    // Face coordinate basis
    PetscCall(RatelDMPlexCeedBasisCoordinateCreate(ratel, dm_solution, NULL, 0, height_face, &basis_x_face));

    for (PetscInt face_index = 0; face_index < ratel->bc_platen_count; face_index++) {
      PetscBool has_face;
      char     *platen_name = ratel->bc_platen_names[face_index];
      PetscInt  dm_face_id  = ratel->bc_platen_label_values[face_index];

      if (ratel->bc_platen_types[face_index] != RATEL_PLATEN_NITSCHE) continue;
      PetscCall(RatelDebug(ratel, "---- Setting up platen BC on face %s with value %" PetscInt_FMT, platen_name, dm_face_id));

      // Check face exists
      PetscCall(RatelDMHasFace(ratel, dm_solution, dm_face_id, &has_face));
      PetscCheck(has_face, ratel->comm, PETSC_ERR_SUP, "Face with value %" PetscInt_FMT " does not exist.", dm_face_id);

      // Domain label for Face Sets
      const char *face_domain_label_name = NULL;
      DMLabel     face_sets_label        = NULL;
      DMLabel     face_domain_label      = NULL;

      PetscCall(DMGetLabel(dm_solution, "Face Sets", &face_sets_label));
      PetscCall(RatelMaterialGetSurfaceGradientLabelName(material, dm_face_id, &face_domain_label_name));
      PetscCall(DMGetLabel(dm_solution, face_domain_label_name, &face_domain_label));
      PetscCheck(face_domain_label, ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "No DM label corresponding to name %s found", face_domain_label_name);

      // Loop over all face orientations
      for (PetscInt orientation = 0; orientation < material->num_face_orientations; orientation++) {
        CeedInt             q_data_size          = model_params->q_data_surface_grad_size;
        CeedVector          stored_values_platen = NULL, q_data = NULL, stored_values_u = NULL, state_values = NULL, state_values_current = NULL;
        CeedElemRestriction restriction_x_face, restriction_x_cell, restriction_stored_values_platen, restrictions_u_cell_to_face[RATEL_MAX_FIELDS],
            restriction_q_data = NULL, restriction_stored_values_u = NULL, restriction_state_values = NULL;
        CeedBasis            bases_u_cell_to_face[RATEL_MAX_FIELDS], basis_x_cell_to_face;
        CeedQFunctionContext ctx_platen;
        CeedQFunction        qf_platen;
        CeedOperator         op_platen;

        PetscCall(RatelDebug(ratel, "------ Face orientation %" PetscInt_FMT, orientation));

        // Check for face on any process
        {
          PetscInt first_point_local = -1, first_point_global = -1, ids[1] = {orientation};

          PetscCall(DMGetFirstLabeledPoint(dm_solution, dm_solution, face_domain_label, 1, ids, height_cell, &first_point_local, NULL));
          PetscCall(MPIU_Allreduce(&first_point_local, &first_point_global, 1, MPIU_INT, MPI_MAX, ratel->comm));
          if (first_point_global == -1) {
            PetscCall(RatelDebug(ratel, "-------- Skipping, no process uses this face orientation"));
            continue;
          }
        }

        PetscCall(RatelDebug(ratel, "------ Setting up platen BC operator"));
        PetscCall(RatelMaterialPlatenContextCreate(material, platen_name, dm_face_id, orientation, face_index, &ctx_platen));
        PetscCall(RatelMaterialGetSurfaceGradientQData(material, dm_face_id, orientation, &restriction_q_data, &q_data));

        // -- Bases
        for (CeedInt j = 0; j < num_active_fields; j++) {
          PetscCall(
              RatelDMPlexCeedBasisCellToFaceCreate(ratel, dm_solution, face_domain_label, orientation, orientation, j, &bases_u_cell_to_face[j]));
        }
        PetscCall(
            RatelDMPlexCeedBasisCellToFaceCoordinateCreate(ratel, dm_solution, face_domain_label, orientation, orientation, &basis_x_cell_to_face));

        // -- ElemRestriction
        // ---- Active fields
        for (CeedInt j = 0; j < num_active_fields; j++) {
          PetscCall(RatelDMPlexCeedElemRestrictionCreate(ratel, dm_solution, face_domain_label, orientation, height_cell, j,
                                                         &restrictions_u_cell_to_face[j]));
        }
        // ---- Stored values in residual_u
        if (num_comp_stored_u > 0) {
          PetscCall(RatelDMPlexCeedElemRestrictionQDataCreate(ratel, dm_solution, face_domain_label, orientation, height_face, num_comp_stored_u,
                                                              &restriction_stored_values_u));
          RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_stored_values_u, &stored_values_u, NULL));
        }
        // ---- Stored values for platen
        PetscCall(RatelDMPlexCeedElemRestrictionQDataCreate(ratel, dm_solution, face_domain_label, orientation, height_face, num_stored_values_platen,
                                                            &restriction_stored_values_platen));
        RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_stored_values_platen, &stored_values_platen, NULL));
        // ---- State values
        if (model_params->num_comp_state > 0) {
          PetscCall(RatelDMPlexCeedElemRestrictionQDataCreate(ratel, dm_solution, face_domain_label, orientation, height_face,
                                                              model_params->num_comp_state, &restriction_state_values));
          RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_state_values, &state_values, NULL));
          RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_state_values, &state_values_current, NULL));
          // Need to initialize to 0.0
          RatelCallCeed(ratel, CeedVectorSetValue(state_values, 0.0));
          RatelCallCeed(ratel, CeedVectorSetValue(state_values_current, 0.0));
        }
        // ---- Coordinates vector
        PetscCall(RatelDMPlexCeedElemRestrictionCoordinateCreate(ratel, dm_solution, face_sets_label, dm_face_id, height_face, &restriction_x_face));
        PetscCall(
            RatelDMPlexCeedElemRestrictionCoordinateCreate(ratel, dm_solution, face_domain_label, orientation, height_cell, &restriction_x_cell));
        if (!x_loc) {
          const PetscScalar *x_array;
          Vec                X_loc;

          PetscCall(RatelDebug(ratel, "---- Retrieving element coordinates"));
          PetscCall(DMGetCoordinatesLocal(dm_solution, &X_loc));
          PetscCall(VecGetArrayRead(X_loc, &x_array));
          RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_x_face, &x_loc, NULL));
          RatelCallCeed(ratel, CeedVectorSetArray(x_loc, CEED_MEM_HOST, CEED_COPY_VALUES, (CeedScalar *)x_array));
          PetscCall(VecRestoreArrayRead(X_loc, &x_array));
        }

        // -- QFunction
        RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, model_params->platen_residual_u, model_params->platen_residual_u_loc, &qf_platen));
        RatelCallCeed(ratel, CeedQFunctionAddInput(qf_platen, "q data", q_data_size, CEED_EVAL_NONE));
        if (model_params->num_comp_state > 0) {
          RatelCallCeed(ratel, CeedQFunctionAddInput(qf_platen, "model state", model_params->num_comp_state, CEED_EVAL_NONE));
          RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_platen, "current model state", model_params->num_comp_state, CEED_EVAL_NONE));
        }
        if (num_comp_stored_u > 0) {
          RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_platen, "stored values u", num_comp_stored_u, CEED_EVAL_NONE));
        }
        {
          CeedInt eval_mode_index = 0;

          for (PetscInt k = 0; k < num_active_fields; k++) {
            for (PetscInt j = 0; j < active_field_num_eval_modes[k]; j++) {
              char         field_name[PETSC_MAX_PATH_LEN];
              CeedEvalMode eval_mode       = active_field_eval_modes[eval_mode_index++];
              CeedInt      quadrature_size = GetCeedQuadratureSize(eval_mode, dim, active_field_sizes[k]);

              PetscCheck(quadrature_size > 0, ratel->comm, PETSC_ERR_SUP, "Eval mode %s not supported", CeedEvalModes[eval_mode]);
              PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "du/dX" : "u", k));
              RatelCallCeed(ratel, CeedQFunctionAddInput(qf_platen, field_name, quadrature_size, eval_mode));
            }
          }
        }
        // platen specific inputs/outputs come after any material inputs/outputs
        RatelCallCeed(ratel, CeedQFunctionAddInput(qf_platen, "u", num_comp_u, CEED_EVAL_INTERP));
        RatelCallCeed(ratel, CeedQFunctionAddInput(qf_platen, "u_t", num_comp_u, CEED_EVAL_INTERP));
        RatelCallCeed(ratel, CeedQFunctionAddInput(qf_platen, "x", dim, CEED_EVAL_INTERP));
        RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_platen, "v", num_comp_u, CEED_EVAL_INTERP));
        RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_platen, "stored values platen", num_stored_values_platen, CEED_EVAL_NONE));
        // -- Add Context
        RatelCallCeed(ratel, CeedQFunctionSetContext(qf_platen, ctx_platen));
        RatelCallCeed(ratel, CeedQFunctionSetContextWritable(qf_platen, PETSC_FALSE));
        // -- Operator
        RatelCallCeed(ratel, CeedOperatorCreate(ceed, qf_platen, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &op_platen));
        {
          char operator_name[PETSC_MAX_PATH_LEN];

          PetscCall(PetscSNPrintf(operator_name, sizeof(operator_name), "platen - value %" PetscInt_FMT " orientation %" PetscInt_FMT, dm_face_id,
                                  orientation));
          PetscCall(RatelMaterialSetOperatorName(material, operator_name, op_platen));
        }
        RatelCallCeed(ratel, CeedOperatorSetField(op_platen, "q data", restriction_q_data, CEED_BASIS_NONE, q_data));
        if (model_params->num_comp_state > 0) {
          RatelCallCeed(ratel, CeedOperatorSetField(op_platen, "model state", restriction_state_values, CEED_BASIS_NONE, state_values));
          RatelCallCeed(ratel,
                        CeedOperatorSetField(op_platen, "current model state", restriction_state_values, CEED_BASIS_NONE, state_values_current));
        }
        if (num_comp_stored_u > 0) {
          RatelCallCeed(ratel, CeedOperatorSetField(op_platen, "stored values u", restriction_stored_values_u, CEED_BASIS_NONE, stored_values_u));
        }
        {
          CeedInt eval_mode_index = 0;

          for (PetscInt k = 0; k < num_active_fields; k++) {
            for (PetscInt j = 0; j < active_field_num_eval_modes[k]; j++) {
              char         field_name[PETSC_MAX_PATH_LEN];
              CeedEvalMode eval_mode = active_field_eval_modes[eval_mode_index++];
              PetscCall(PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "du/dX" : "u", k));
              RatelCallCeed(ratel,
                            CeedOperatorSetField(op_platen, field_name, restrictions_u_cell_to_face[k], bases_u_cell_to_face[k], CEED_VECTOR_ACTIVE));
            }
          }
        }
        RatelCallCeed(ratel, CeedOperatorSetField(op_platen, "u", restrictions_u_cell_to_face[0], bases_u_cell_to_face[0], CEED_VECTOR_ACTIVE));
        RatelCallCeed(ratel, CeedOperatorSetField(op_platen, "u_t", restrictions_u_cell_to_face[0], bases_u_cell_to_face[0], u_dot_loc));
        RatelCallCeed(ratel, CeedOperatorSetField(op_platen, "x", restriction_x_cell, basis_x_cell_to_face, x_loc));
        RatelCallCeed(ratel, CeedOperatorSetField(op_platen, "v", restrictions_u_cell_to_face[0], bases_u_cell_to_face[0], CEED_VECTOR_ACTIVE));
        RatelCallCeed(
            ratel, CeedOperatorSetField(op_platen, "stored values platen", restriction_stored_values_platen, CEED_BASIS_NONE, stored_values_platen));
        // -- Add to composite operator
        RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_residual, op_platen));

        if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(op_platen, stdout));

        // Set up Jacobian
        {
          CeedQFunction sub_qf_jacobian;
          CeedOperator  sub_op_jacobian;

          PetscCall(RatelDebug(ratel, "---- Setting up Jacobian evaluator"));
          // -- QFunction
          RatelCallCeed(ratel,
                        CeedQFunctionCreateInterior(ceed, 1, model_params->platen_jacobian, model_params->platen_jacobian_loc, &sub_qf_jacobian));
          RatelCallCeed(ratel, CeedQFunctionAddInput(sub_qf_jacobian, "q data", q_data_size, CEED_EVAL_NONE));
          if (model_params->num_comp_state > 0) {
            RatelCallCeed(ratel, CeedQFunctionAddInput(sub_qf_jacobian, "model state", model_params->num_comp_state, CEED_EVAL_NONE));
          }
          if (num_comp_stored_u > 0) {
            RatelCallCeed(ratel, CeedQFunctionAddInput(sub_qf_jacobian, "stored values u", num_comp_stored_u, CEED_EVAL_NONE));
          }
          {
            CeedInt eval_mode_index = 0;

            for (PetscInt k = 0; k < num_active_fields; k++) {
              for (PetscInt j = 0; j < active_field_num_eval_modes[k]; j++) {
                char         field_name[PETSC_MAX_PATH_LEN];
                CeedEvalMode eval_mode       = active_field_eval_modes[eval_mode_index++];
                CeedInt      quadrature_size = GetCeedQuadratureSize(eval_mode, dim, active_field_sizes[k]);

                PetscCheck(quadrature_size > 0, ratel->comm, PETSC_ERR_SUP, "Eval mode %s not supported", CeedEvalModes[eval_mode]);
                PetscCall(
                    PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "d(du)/dX" : "du", k));
                RatelCallCeed(ratel, CeedQFunctionAddInput(sub_qf_jacobian, field_name, quadrature_size, eval_mode));
              }
            }
          }
          RatelCallCeed(ratel, CeedQFunctionAddInput(sub_qf_jacobian, "du", num_comp_u, CEED_EVAL_INTERP));
          RatelCallCeed(ratel, CeedQFunctionAddInput(sub_qf_jacobian, "stored values platen", num_stored_values_platen, CEED_EVAL_NONE));
          RatelCallCeed(ratel, CeedQFunctionAddOutput(sub_qf_jacobian, "dv", num_comp_u, CEED_EVAL_INTERP));
          RatelCallCeed(ratel, CeedQFunctionSetUserFlopsEstimate(sub_qf_jacobian, model_params->flops_qf_jacobian_platen));
          // -- Context
          RatelCallCeed(ratel, CeedQFunctionSetContext(sub_qf_jacobian, ctx_platen));
          RatelCallCeed(ratel, CeedQFunctionSetContextWritable(sub_qf_jacobian, PETSC_FALSE));
          // -- Operator
          RatelCallCeed(ratel, CeedOperatorCreate(ceed, sub_qf_jacobian, CEED_QFUNCTION_NONE, CEED_QFUNCTION_NONE, &sub_op_jacobian));
          {
            char operator_name[PETSC_MAX_PATH_LEN];

            PetscCall(PetscSNPrintf(operator_name, sizeof(operator_name), "platen jacobian - face %" PetscInt_FMT " orientation %" PetscInt_FMT,
                                    dm_face_id, orientation));
            PetscCall(RatelMaterialSetOperatorName(material, operator_name, sub_op_jacobian));
          }
          RatelCallCeed(ratel, CeedOperatorSetField(sub_op_jacobian, "q data", restriction_q_data, CEED_BASIS_NONE, q_data));
          if (model_params->num_comp_state > 0) {
            RatelCallCeed(ratel, CeedOperatorSetField(sub_op_jacobian, "model state", restriction_state_values, CEED_BASIS_NONE, state_values));
          }
          if (num_comp_stored_u > 0) {
            RatelCallCeed(ratel,
                          CeedOperatorSetField(sub_op_jacobian, "stored values u", restriction_stored_values_u, CEED_BASIS_NONE, stored_values_u));
          }
          {
            CeedInt eval_mode_index = 0;

            for (PetscInt k = 0; k < num_active_fields; k++) {
              for (PetscInt j = 0; j < active_field_num_eval_modes[k]; j++) {
                char         field_name[PETSC_MAX_PATH_LEN];
                CeedEvalMode eval_mode = active_field_eval_modes[eval_mode_index++];

                PetscCall(
                    PetscSNPrintf(field_name, PETSC_MAX_PATH_LEN, "%s, field %" PetscInt_FMT, eval_mode == CEED_EVAL_GRAD ? "d(du)/dX" : "du", k));
                RatelCallCeed(ratel, CeedOperatorSetField(sub_op_jacobian, field_name, restrictions_u_cell_to_face[k], bases_u_cell_to_face[k],
                                                          CEED_VECTOR_ACTIVE));
              }
            }
          }

          RatelCallCeed(ratel,
                        CeedOperatorSetField(sub_op_jacobian, "du", restrictions_u_cell_to_face[0], bases_u_cell_to_face[0], CEED_VECTOR_ACTIVE));
          RatelCallCeed(ratel,
                        CeedOperatorSetField(sub_op_jacobian, "dv", restrictions_u_cell_to_face[0], bases_u_cell_to_face[0], CEED_VECTOR_ACTIVE));
          RatelCallCeed(ratel, CeedOperatorSetField(sub_op_jacobian, "stored values platen", restriction_stored_values_platen, CEED_BASIS_NONE,
                                                    stored_values_platen));

          // -- Add to composite operator
          RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_jacobian, sub_op_jacobian));
          if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_jacobian, stdout));

          // -- Cleanup
          RatelCallCeed(ratel, CeedQFunctionDestroy(&sub_qf_jacobian));
          RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_jacobian));
        }

        // -- Cleanup
        RatelCallCeed(ratel, CeedVectorDestroy(&q_data));
        RatelCallCeed(ratel, CeedVectorDestroy(&stored_values_u));
        RatelCallCeed(ratel, CeedVectorDestroy(&state_values));
        RatelCallCeed(ratel, CeedVectorDestroy(&state_values_current));
        RatelCallCeed(ratel, CeedVectorDestroy(&stored_values_platen));
        for (CeedInt j = 0; j < num_active_fields; j++) {
          RatelCallCeed(ratel, CeedBasisDestroy(&bases_u_cell_to_face[j]));
        }
        RatelCallCeed(ratel, CeedBasisDestroy(&basis_x_cell_to_face));
        RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_q_data));
        RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_stored_values_u));
        RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_state_values));
        for (CeedInt j = 0; j < num_active_fields; j++) {
          RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restrictions_u_cell_to_face[j]));
        }
        RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_x_face));
        RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_x_cell));
        RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_stored_values_platen));
        RatelCallCeed(ratel, CeedQFunctionContextDestroy(&ctx_platen));
        RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_platen));
        RatelCallCeed(ratel, CeedOperatorDestroy(&op_platen));
      }
    }
    // In general, platen BCs do not preserve symmetry
    PetscCall(RatelSetNonSPD(ratel));

    // Cleanup
    RatelCallCeed(ratel, CeedVectorDestroy(&x_loc));
    RatelCallCeed(ratel, CeedBasisDestroy(&basis_x_face));
  }

  // Cleanup
  PetscCall(DMDestroy(&dm_solution));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Ceed Add Boundaries Platen Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup multigrid level suboperator for `RatelMaterial` platen boundary conditions Jacobian.

  Collective across MPI processes.

  @param[in]      material                `RatelMaterial` context
  @param[in]      dm_level                `DMPlex` for multigrid level to setup
  @param[in]      m_loc                   `CeedVector` holding multiplicity
  @param[in]      sub_op_jacobian_fine    Fine grid Jacobian `CeedOperator`
  @param[in,out]  op_jacobian_coarse      Composite Jacobian `CeedOperator` to add `RatelMaterial` suboperators
  @param[in,out]  op_prolong              Composite prolongation `CeedOperator` to add `RatelMaterial` suboperators,
                                            unused but needed for compatable function signature
  @param[in,out]  op_restrict             Composite restriction `CeedOperator` to add `RatelMaterial` suboperators,
                                            unused but needed for compatable function signature

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupPlatenNitscheJacobianMultigridLevel(RatelMaterial material, DM dm_level, CeedVector m_loc,
                                                                     CeedOperator sub_op_jacobian_fine, CeedOperator op_jacobian_coarse,
                                                                     CeedOperator op_prolong, CeedOperator op_restrict) {
  Ratel               ratel = material->ratel;
  PetscInt            dim, face_domain_value;
  const PetscInt      height_cell       = 0;
  DMLabel             face_domain_label = NULL;
  CeedInt             num_elem;
  CeedElemRestriction restriction_u_cell;
  CeedBasis           basis_u_cell_to_face;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Platen Jacobian Multigrid Level FEM"));

  // Loudly error if these are accidentally used
  op_prolong  = NULL;
  op_restrict = NULL;

  PetscCall(DMGetDimension(dm_level, &dim));

  // Get DM face label and domain value from context
  PetscCall(RatelMaterialGetSurfaceGradientOperatorFaceLabelAndValue(material, sub_op_jacobian_fine, &face_domain_label, &face_domain_value));

  // Setup level operators
  CeedOperator sub_op_jacobian_coarse;
  PetscCall(RatelDebug(ratel, "---- Setting up platen BC Jacobian for domain stratum value %" PetscInt_FMT, face_domain_value));

  // -- Bases
  PetscCall(RatelDMPlexCeedBasisCellToFaceCreate(ratel, dm_level, face_domain_label, face_domain_value, face_domain_value, 0, &basis_u_cell_to_face));

  // -- ElemRestriction
  PetscCall(RatelDMPlexCeedElemRestrictionCreate(ratel, dm_level, face_domain_label, face_domain_value, height_cell, 0, &restriction_u_cell));
  RatelCallCeed(ratel, CeedElemRestrictionGetNumElements(restriction_u_cell, &num_elem));

  // -- Create coarse grid operator
  RatelCallCeed(ratel, CeedOperatorMultigridLevelCreate(sub_op_jacobian_fine, NULL, restriction_u_cell, basis_u_cell_to_face, &sub_op_jacobian_coarse,
                                                        NULL, NULL));

  // -- Add to composite operators
  RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_jacobian_coarse, sub_op_jacobian_coarse));

  // -- Cleanup
  RatelCallCeed(ratel, CeedBasisDestroy(&basis_u_cell_to_face));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_u_cell));
  RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_jacobian_coarse));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Platen Jacobian Multigrid Level FEM Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Add platen penalty boundary conditions to the residual u term and Jacobian operators.

  Collective across MPI processes.

  @param[in]   ratel        `Ratel` context
  @param[in]   dm           `DM` to use for pressure boundaries
  @param[in]   u_dot_loc    Local CeedVector for time derivative of u
  @param[out]  op_residual  Composite residual u term `CeedOperator` to add platen sub-operators
  @param[out]  op_jacobian  Composite Jacobian `CeedOperator` to add platen sub-operators

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelCeedAddBoundariesPlatenPenalty(Ratel ratel, DM dm, CeedVector u_dot_loc, CeedOperator op_residual, CeedOperator op_jacobian) {
  PetscInt       dim;
  const PetscInt height_face = 1;
  Ceed           ceed        = ratel->ceed;
  CeedInt        num_comp_x, num_comp_u = 3, q_data_size = 5, num_stored_values_platen = 4;
  CeedVector     x_loc = NULL;
  CeedBasis      basis_x_face, basis_u_face;
  CeedQFunction  qf_setup;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Ceed Add Boundaries Platen Penalty"));

  // Get number of solution components and dimension
  {
    const CeedInt *field_sizes;

    PetscCall(DMGetDimension(dm, &dim));
    num_comp_x = dim;
    PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[0], NULL, &field_sizes));
    num_comp_u = field_sizes[0];  // First field should be displacement
  }

  if (ratel->bc_platen_count > 0) {
    DMLabel domain_label;

    PetscCall(DMGetLabel(dm, "Face Sets", &domain_label));

    // Basis
    PetscCall(RatelDebug(ratel, "---- Setting up libCEED bases"));
    PetscCall(RatelDMPlexCeedBasisCreate(ratel, dm, NULL, 0, height_face, 0, &basis_u_face));
    PetscCall(RatelDMPlexCeedBasisCoordinateCreate(ratel, dm, NULL, 0, height_face, &basis_x_face));

    // Setup QFunction
    RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, SetupPenaltyPlatens, RatelQFunctionRelativePath(SetupPenaltyPlatens_loc), &qf_setup));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_setup, "dx/dX", num_comp_x * (num_comp_x - height_face), CEED_EVAL_GRAD));
    RatelCallCeed(ratel, CeedQFunctionAddInput(qf_setup, "weight", 1, CEED_EVAL_WEIGHT));
    RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_setup, "q data", q_data_size, CEED_EVAL_NONE));

    // Compute contribution on each boundary face
    for (PetscInt i = 0; i < ratel->bc_platen_count; i++) {
      PetscBool            has_face;
      char                *platen_name = ratel->bc_platen_names[i];
      PetscInt             dm_face_id  = ratel->bc_platen_label_values[i];
      CeedElemRestriction  restriction_x_face, restriction_u_face, restriction_q_data, restriction_stored_values_platen;
      CeedQFunctionContext ctx_platen;
      CeedQFunction        qf_platen, qf_jacobian;
      CeedOperator         op_setup, sub_op_platen, sub_op_jacobian;
      CeedVector           q_data, stored_values_platen;

      if (ratel->bc_platen_types[i] != RATEL_PLATEN_PENALTY) continue;
      PetscCall(RatelDebug(ratel, "---- Setting up platen BC on face %s with value %" PetscInt_FMT, platen_name, dm_face_id));

      // Check face exists
      PetscCall(RatelDMHasFace(ratel, dm, dm_face_id, &has_face));
      PetscCheck(has_face, ratel->comm, PETSC_ERR_SUP, "Face with value %" PetscInt_FMT " does not exist.", dm_face_id);

      PetscCall(RatelDebug(ratel, "---- Setting up platen boundary: %" CeedInt_FMT, i));

      // -- Set up context data
      PetscCall(RatelMaterialPlatenContextCreate(ratel->materials[0], platen_name, dm_face_id, 0, i, &ctx_platen));
      if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedQFunctionContextView(ctx_platen, stdout));

      // -- Restrictions
      PetscCall(RatelDebug(ratel, "---- Setting up libCEED restrictions"));
      PetscCall(RatelDMPlexCeedElemRestrictionCreate(ratel, dm, domain_label, dm_face_id, height_face, 0, &restriction_u_face));
      PetscCall(RatelDMPlexCeedElemRestrictionCoordinateCreate(ratel, dm, domain_label, dm_face_id, height_face, &restriction_x_face));
      PetscCall(RatelDMPlexCeedElemRestrictionQDataCreate(ratel, dm, domain_label, dm_face_id, height_face, q_data_size, &restriction_q_data));
      RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_q_data, &q_data, NULL));
      // ---- Stored values for platen
      PetscCall(RatelDMPlexCeedElemRestrictionQDataCreate(ratel, dm, domain_label, dm_face_id, height_face, num_stored_values_platen,
                                                          &restriction_stored_values_platen));
      RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_stored_values_platen, &stored_values_platen, NULL));

      // -- Coordinates vector
      if (!x_loc) {
        const PetscScalar *x_array;
        Vec                X_loc;

        PetscCall(RatelDebug(ratel, "---- Retrieving element coordinates"));
        PetscCall(DMGetCoordinatesLocal(dm, &X_loc));
        PetscCall(VecGetArrayRead(X_loc, &x_array));
        RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_x_face, &x_loc, NULL));
        RatelCallCeed(ratel, CeedVectorSetArray(x_loc, CEED_MEM_HOST, CEED_COPY_VALUES, (CeedScalar *)x_array));
        PetscCall(VecRestoreArrayRead(X_loc, &x_array));
      }

      // -- Setup Operator
      RatelCallCeed(ratel, CeedOperatorCreate(ceed, qf_setup, NULL, NULL, &op_setup));
      RatelCallCeed(ratel, CeedOperatorSetName(op_setup, "surface geometric data"));
      RatelCallCeed(ratel, CeedOperatorSetField(op_setup, "dx/dX", restriction_x_face, basis_x_face, CEED_VECTOR_ACTIVE));
      RatelCallCeed(ratel, CeedOperatorSetField(op_setup, "weight", CEED_ELEMRESTRICTION_NONE, basis_x_face, CEED_VECTOR_NONE));
      RatelCallCeed(ratel, CeedOperatorSetField(op_setup, "q data", restriction_q_data, CEED_BASIS_NONE, CEED_VECTOR_ACTIVE));

      // -- Compute geometric factors
      PetscCall(RatelDebug(ratel, "---- Computing geometric factors"));
      if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(op_setup, stdout));
      RatelCallCeed(ratel, CeedOperatorApply(op_setup, x_loc, q_data, CEED_REQUEST_IMMEDIATE));

      // -- Apply QFunction
      PetscCall(RatelDebug(ratel, "---- Setting up platen operator"));
      RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, PlatenPenaltyBCs, RatelQFunctionRelativePath(PlatenPenaltyBCs_loc), &qf_platen));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_platen, "q data", q_data_size, CEED_EVAL_NONE));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_platen, "u", num_comp_u, CEED_EVAL_INTERP));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_platen, "u dot", num_comp_u, CEED_EVAL_INTERP));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_platen, "x", dim, CEED_EVAL_INTERP));
      RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_platen, "v", num_comp_u, CEED_EVAL_INTERP));
      RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_platen, "stored values platen", num_stored_values_platen, CEED_EVAL_NONE));
      RatelCallCeed(ratel, CeedQFunctionSetContext(qf_platen, ctx_platen));
      RatelCallCeed(ratel, CeedQFunctionSetContextWritable(qf_platen, PETSC_FALSE));

      // -- Apply operator
      RatelCallCeed(ratel, CeedOperatorCreate(ceed, qf_platen, NULL, NULL, &sub_op_platen));
      {
        char operator_name[PETSC_MAX_PATH_LEN];

        PetscCall(PetscSNPrintf(operator_name, sizeof(operator_name), "platen (penalty) - face %s", ratel->bc_platen_names[i]));
        RatelCallCeed(ratel, CeedOperatorSetName(sub_op_platen, operator_name));
      }
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_platen, "q data", restriction_q_data, CEED_BASIS_NONE, q_data));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_platen, "u", restriction_u_face, basis_u_face, CEED_VECTOR_ACTIVE));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_platen, "u dot", restriction_u_face, basis_u_face, u_dot_loc));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_platen, "x", restriction_x_face, basis_x_face, x_loc));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_platen, "v", restriction_u_face, basis_u_face, CEED_VECTOR_ACTIVE));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_platen, "stored values platen", restriction_stored_values_platen, CEED_BASIS_NONE,
                                                stored_values_platen));
      // -- Add to composite operator
      RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_residual, sub_op_platen));
      if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_platen, stdout));

      // Set up jacobian
      // ---- Jacobian QFunction
      PetscCall(RatelDebug(ratel, "---- Setting up Jacobian evaluator"));
      RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, PlatenPenaltyBCsJacobian, RatelQFunctionRelativePath(PlatenPenaltyBCsJacobian_loc),
                                                       &qf_jacobian));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_jacobian, "q data", q_data_size, CEED_EVAL_NONE));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_jacobian, "du", num_comp_u, CEED_EVAL_INTERP));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_jacobian, "stored values platen", num_stored_values_platen, CEED_EVAL_NONE));
      RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_jacobian, "v", num_comp_u, CEED_EVAL_INTERP));
      RatelCallCeed(ratel, CeedQFunctionSetUserFlopsEstimate(qf_jacobian, FLOPS_Jacobian_PlatenPenaltyBC));
      // ---- Context
      RatelCallCeed(ratel, CeedQFunctionSetContext(qf_jacobian, ctx_platen));
      RatelCallCeed(ratel, CeedQFunctionSetContextWritable(qf_jacobian, PETSC_FALSE));
      // ---- Jacobian operator
      RatelCallCeed(ratel, CeedOperatorCreate(ceed, qf_jacobian, NULL, NULL, &sub_op_jacobian));
      {
        char operator_name[PETSC_MAX_PATH_LEN];

        PetscCall(PetscSNPrintf(operator_name, sizeof(operator_name), "platen (penalty) - face %s", ratel->bc_platen_names[i]));
        RatelCallCeed(ratel, CeedOperatorSetName(sub_op_jacobian, operator_name));
      }
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_jacobian, "q data", restriction_q_data, CEED_BASIS_NONE, q_data));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_jacobian, "du", restriction_u_face, basis_u_face, CEED_VECTOR_ACTIVE));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_jacobian, "stored values platen", restriction_stored_values_platen, CEED_BASIS_NONE,
                                                stored_values_platen));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_jacobian, "v", restriction_u_face, basis_u_face, CEED_VECTOR_ACTIVE));

      // -- Add to composite operator
      RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_jacobian, sub_op_jacobian));
      if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_jacobian, stdout));

      // -- Cleanup
      RatelCallCeed(ratel, CeedVectorDestroy(&q_data));
      RatelCallCeed(ratel, CeedVectorDestroy(&stored_values_platen));
      RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_x_face));
      RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_u_face));
      RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_q_data));
      RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_stored_values_platen));
      RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_platen));
      RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_jacobian));
      RatelCallCeed(ratel, CeedQFunctionContextDestroy(&ctx_platen));
      RatelCallCeed(ratel, CeedOperatorDestroy(&op_setup));
      RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_platen));
      RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_jacobian));
    }
    // Frictional contact is non-SPD
    PetscCall(RatelSetNonSPD(ratel));

    // Cleanup
    RatelCallCeed(ratel, CeedVectorDestroy(&x_loc));
    RatelCallCeed(ratel, CeedBasisDestroy(&basis_x_face));
    RatelCallCeed(ratel, CeedBasisDestroy(&basis_u_face));
    RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_setup));
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Ceed Add Boundaries Platen Penalty Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup multigrid level suboperator for `RatelMaterial` platen penalty boundary conditions Jacobian.

  Collective across MPI processes.

  @param[in]      material                `RatelMaterial` context
  @param[in]      dm_level                `DMPlex` for multigrid level to setup
  @param[in]      m_loc                   `CeedVector` holding multiplicity
  @param[in]      sub_op_jacobian_fine    Fine grid Jacobian `CeedOperator`
  @param[in,out]  op_jacobian_coarse      Composite Jacobian `CeedOperator` to add `RatelMaterial` suboperators
  @param[in,out]  op_prolong              Composite prolongation `CeedOperator` to add `RatelMaterial` suboperators,
                                            unused but needed for compatable function signature
  @param[in,out]  op_restrict             Composite restriction `CeedOperator` to add `RatelMaterial` suboperators,
                                            unused but needed for compatable function signature

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelMaterialSetupPlatenPenaltyJacobianMultigridLevel(RatelMaterial material, DM dm_level, CeedVector m_loc,
                                                                     CeedOperator sub_op_jacobian_fine, CeedOperator op_jacobian_coarse,
                                                                     CeedOperator op_prolong, CeedOperator op_restrict) {
  Ratel               ratel = material->ratel;
  DM                  dm_solution;
  PetscInt            dim, face_value;
  const PetscInt      height_face = 1;
  DMLabel             face_label  = NULL;
  CeedInt             num_elem;
  CeedElemRestriction restriction_u_face;
  CeedBasis           basis_u_face;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Platen Penalty Jacobian Multigrid Level FEM"));

  // Loudly error if these are accidentally used
  op_prolong  = NULL;
  op_restrict = NULL;

  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));
  PetscCall(DMGetDimension(dm_solution, &dim));
  PetscCall(DMGetLabel(dm_solution, "Face Sets", &face_label));

  // Get DM face label and domain value from context
  {
    const CeedInt        *id;
    PetscSizeT            count;
    CeedContextFieldLabel label;

    RatelCallCeed(ratel, CeedOperatorGetContextFieldLabel(sub_op_jacobian_fine, "face id", &label));
    PetscCheck(label, ratel->comm, PETSC_ERR_ARG_WRONGSTATE, "Face context not initialized for platen Jacobian operator");
    RatelCallCeed(ratel, CeedOperatorGetContextInt32Read(sub_op_jacobian_fine, label, &count, &id));
    face_value = (PetscInt)id[0];
    RatelCallCeed(ratel, CeedOperatorRestoreContextInt32Read(sub_op_jacobian_fine, label, &id));
  }

  // Setup level operators
  CeedOperator sub_op_jacobian_coarse;
  PetscCall(RatelDebug(ratel, "---- Setting up platen penalty BC Jacobian for face value %" PetscInt_FMT, face_value));

  // -- Bases
  PetscCall(RatelDMPlexCeedBasisCreate(ratel, dm_level, NULL, 0, height_face, 0, &basis_u_face));

  // -- ElemRestriction
  PetscCall(RatelDMPlexCeedElemRestrictionCreate(ratel, dm_level, face_label, face_value, height_face, 0, &restriction_u_face));
  RatelCallCeed(ratel, CeedElemRestrictionGetNumElements(restriction_u_face, &num_elem));

  // If the face is present on the process
  if (num_elem > 0) {
    // -- Create coarse grid operator
    RatelCallCeed(
        ratel, CeedOperatorMultigridLevelCreate(sub_op_jacobian_fine, NULL, restriction_u_face, basis_u_face, &sub_op_jacobian_coarse, NULL, NULL));

    // -- Add to composite operators
    RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_jacobian_coarse, sub_op_jacobian_coarse));
    if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_jacobian_coarse, stdout));
  }

  // -- Cleanup
  RatelCallCeed(ratel, CeedBasisDestroy(&basis_u_face));
  RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_u_face));
  RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_jacobian_coarse));
  PetscCall(DMDestroy(&dm_solution));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Platen Penalty Jacobian Multigrid Level FEM Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Read pressure boundary conditions from options database

  Collective across MPI processes.

  @param[in]   ratel           `Ratel` context
  @param[in]   i               Boundary index
  @param[out]  params_pressure Data structure to store pressure data
*/
PetscErrorCode RatelBoundaryPressureDataFromOptions(Ratel ratel, PetscInt i, RatelBCPressureParams *params_pressure) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Boundary Pressure Data From Options"));

  // Initialize context data
  for (PetscInt i = 0; i < RATEL_MAX_BC_INTERP_POINTS; i++) {
    params_pressure->pressure[i] = 0.0;
    params_pressure->times[i]    = 1.0;
  }
  params_pressure->num_times          = 1;
  params_pressure->interpolation_type = RATEL_BC_INTERP_LINEAR;
  params_pressure->time               = RATEL_UNSET_INITIAL_TIME;

  // Read CL options for pressure conditions
  PetscOptionsBegin(ratel->comm, NULL, "", NULL);

  // Pressure value(s)
  PetscInt  max_num_pressures = RATEL_MAX_BC_INTERP_POINTS;
  PetscInt  max_num_times     = RATEL_MAX_BC_INTERP_POINTS;
  PetscBool pressure_set = PETSC_FALSE, times_set = PETSC_FALSE;

  // Get transition times for pressure vectors
  {
    char option_name[PETSC_MAX_OPTION_NAME];

    PetscCall(PetscSNPrintf(option_name, sizeof(option_name), "-bc_pressure_%" PetscInt_FMT "_times", ratel->bc_pressure_faces[i]));
    PetscCall(PetscOptionsScalarArray(option_name, "Transition times for each pressure vector for constrained face", NULL, params_pressure->times,
                                      &max_num_times, &times_set));
    if (times_set) {
      params_pressure->num_times = max_num_times;
      // -- Ensure times are monotonically increasing
      PetscBool is_monotonic     = PETSC_TRUE;

      for (PetscInt j = 0; j < params_pressure->num_times - 1; j++) {
        if (params_pressure->times[j] >= params_pressure->times[j + 1]) {
          is_monotonic = PETSC_FALSE;
        }
      }
      PetscCheck(is_monotonic, ratel->comm, PETSC_ERR_SUP, "Transition times must be monotonically increasing.");

      // -- Check bounds of times
      PetscCheck(params_pressure->times[0] >= 0, ratel->comm, PETSC_ERR_SUP, "First time must be greater than or equal to 0.");
    }
  }
  // Get pressure vectors
  {
    char option_name[PETSC_MAX_OPTION_NAME];

    PetscCall(PetscSNPrintf(option_name, sizeof(option_name), "-bc_pressure_%" PetscInt_FMT, ratel->bc_pressure_faces[i]));

    PetscCall(PetscOptionsScalarArray(option_name, "Pressure vectors for constrained face", NULL, params_pressure->pressure, &max_num_pressures,
                                      &pressure_set));

    PetscCheck(pressure_set, ratel->comm, PETSC_ERR_SUP, "Pressure must be set for all pressure boundary conditions.");

    PetscCheck(max_num_pressures == params_pressure->num_times, ratel->comm, PETSC_ERR_SUP, "A time must be set for each provided pressure.");
    PetscCheck(times_set || max_num_pressures == 1, ratel->comm, PETSC_ERR_SUP, "Times must be set if more than one pressure is specified.");
  }

  {
    char option_name[PETSC_MAX_OPTION_NAME];

    PetscCall(PetscSNPrintf(option_name, sizeof(option_name), "-bc_pressure_%" PetscInt_FMT "_interpolation", ratel->bc_pressure_faces[i]));

    PetscCall(PetscOptionsEnum(option_name, "Set interpolation type for pressure BC", NULL, RatelBCInterpolationTypesCL,
                               (PetscEnum)params_pressure->interpolation_type, (PetscEnum *)&params_pressure->interpolation_type, NULL));
  }

  PetscCall(RatelDebug(ratel, "----   Pressure Boundary %" PetscInt_FMT ": %" PetscInt_FMT " pressure vector(s) with interpolation: %s",
                       ratel->bc_traction_faces[i], params_pressure->num_times, RatelBCInterpolationTypes[params_pressure->interpolation_type]));
  for (PetscInt j = 0; j < params_pressure->num_times; j++) {
    PetscCall(RatelDebug(ratel, "------   t = %.3f: tx: %.3f", params_pressure->times[j], params_pressure->pressure[j]));
  }

  PetscOptionsEnd();  // End of setting traction conditions

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Boundary Pressure Data From Options Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Add pressure boundary conditions to residual and Jacobian operators.

  Collective across MPI processes.

  @param[in]   ratel        `Ratel` context
  @param[in]   dm           `DM` to use for pressure boundaries
  @param[out]  op_residual  Composite residual u term `CeedOperator` to add pressure sub-operators
  @param[out]  op_jacobian  Composite Jacobian `CeedOperator` to add pressure sub-operators

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelCeedAddBoundariesPressure(Ratel ratel, DM dm, CeedOperator op_residual, CeedOperator op_jacobian) {
  PetscInt       dim;
  const PetscInt height_face = 1;
  Ceed           ceed        = ratel->ceed;
  CeedInt        num_comp_x, num_comp_u = 3, stored_dxdX_size = 6;
  CeedVector     x_loc = NULL;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Ceed Add Boundaries Pressure"));

  // Get number of solution components and dimension
  {
    const CeedInt *field_sizes;

    PetscCall(DMGetDimension(dm, &dim));
    num_comp_x = dim;
    PetscCall(RatelMaterialGetActiveFieldSizes(ratel->materials[0], NULL, &field_sizes));
    num_comp_u = field_sizes[0];  // First field should be displacement
  }

  if (ratel->bc_pressure_count > 0) {
    DMLabel   domain_label;
    CeedBasis basis_x_face, basis_u_face;

    PetscCall(DMGetLabel(dm, "Face Sets", &domain_label));
    PetscCall(DMGetDimension(dm, &dim));

    // Coordinates vector
    {
      PetscInt           x_size;
      const PetscScalar *x_array;
      Vec                X_loc;

      PetscCall(DMGetCoordinatesLocal(dm, &X_loc));
      PetscCall(VecGetLocalSize(X_loc, &x_size));
      RatelCallCeed(ratel, CeedVectorCreate(ratel->ceed, x_size, &x_loc));
      PetscCall(VecGetArrayRead(X_loc, &x_array));
      RatelCallCeed(ratel, CeedVectorSetArray(x_loc, CEED_MEM_HOST, CEED_COPY_VALUES, (CeedScalar *)x_array));
      PetscCall(VecRestoreArrayRead(X_loc, &x_array));
    }

    // Basis
    PetscCall(RatelDebug(ratel, "---- Setting up libCEED bases"));
    PetscCall(RatelDMPlexCeedBasisCreate(ratel, dm, NULL, 0, height_face, 0, &basis_u_face));
    PetscCall(RatelDMPlexCeedBasisCoordinateCreate(ratel, dm, NULL, 0, height_face, &basis_x_face));

    // Compute contribution on each boundary face
    for (CeedInt i = 0; i < ratel->bc_pressure_count; i++) {
      PetscBool             has_face;
      CeedVector            stored_dxdX        = NULL;
      CeedElemRestriction   restriction_x_face = NULL, restriction_u_face = NULL, restriction_dxdX = NULL;
      CeedQFunctionContext  ctx_pressure = NULL;
      CeedQFunction         qf_pressure = NULL, sub_qf_jacobian = NULL;
      CeedOperator          sub_op_pressure = NULL, sub_op_jacobian = NULL;
      RatelBCPressureParams params_pressure;

      PetscCall(RatelDebug(ratel, "---- Setting up pressure boundary: %" CeedInt_FMT, i));

      // -- Check face exists
      PetscCall(RatelDMHasFace(ratel, dm, ratel->bc_pressure_faces[i], &has_face));
      PetscCheck(has_face, ratel->comm, PETSC_ERR_SUP, "Face %" PetscInt_FMT " does not exist.", ratel->bc_pressure_faces[i]);

      // -- Set up context data
      PetscCall(RatelBoundaryPressureDataFromOptions(ratel, i, &params_pressure));

      // -- Restrictions
      PetscCall(RatelDebug(ratel, "---- Setting up libCEED restrictions"));
      PetscCall(RatelDMPlexCeedElemRestrictionCreate(ratel, dm, domain_label, ratel->bc_pressure_faces[i], height_face, 0, &restriction_u_face));
      PetscCall(
          RatelDMPlexCeedElemRestrictionCoordinateCreate(ratel, dm, domain_label, ratel->bc_pressure_faces[i], height_face, &restriction_x_face));
      // -- Restriction for stored two columns of dxdX data at quadrature points
      PetscCall(RatelDMPlexCeedElemRestrictionQDataCreate(ratel, dm, domain_label, ratel->bc_pressure_faces[i], height_face, stored_dxdX_size,
                                                          &restriction_dxdX));
      RatelCallCeed(ratel, CeedElemRestrictionCreateVector(restriction_dxdX, &stored_dxdX, NULL));

      // ---- Residual QFunction
      PetscCall(RatelDebug(ratel, "---- Setting up pressure operator"));
      RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, 1, PressureBCs, RatelQFunctionRelativePath(PressureBCs_loc), &qf_pressure));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_pressure, "dx_initial/dX", num_comp_x * (num_comp_x - height_face), CEED_EVAL_GRAD));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_pressure, "weight", 1, CEED_EVAL_WEIGHT));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_pressure, "du/dX", num_comp_u * (num_comp_u - height_face), CEED_EVAL_GRAD));
      RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_pressure, "v", num_comp_u, CEED_EVAL_INTERP));
      RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_pressure, "dx/dX", stored_dxdX_size, CEED_EVAL_NONE));
      RatelCallCeed(ratel, CeedQFunctionContextCreate(ceed, &ctx_pressure));
      RatelCallCeed(ratel, CeedQFunctionContextSetData(ctx_pressure, CEED_MEM_HOST, CEED_COPY_VALUES, sizeof(params_pressure), &params_pressure));
      RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(ctx_pressure, "pressure", offsetof(RatelBCPressureParams, pressure),
                                                              params_pressure.num_times, "pressure value applied on face"));
      RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(ctx_pressure, "times", offsetof(RatelBCPressureParams, times),
                                                              params_pressure.num_times, "transition time values"));
      RatelCallCeed(ratel, CeedQFunctionContextRegisterInt32(ctx_pressure, "num times", offsetof(RatelBCPressureParams, num_times), 1,
                                                             "number of time values and direction vectors"));
      RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(ctx_pressure, "time", offsetof(RatelBCPressureParams, time), 1, "current solver time"));
      RatelCallCeed(ratel, CeedQFunctionSetContext(qf_pressure, ctx_pressure));
      RatelCallCeed(ratel, CeedQFunctionSetContextWritable(qf_pressure, PETSC_FALSE));
      if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedQFunctionContextView(ctx_pressure, stdout));

      // ---- Residual operator
      RatelCallCeed(ratel, CeedOperatorCreate(ceed, qf_pressure, NULL, NULL, &sub_op_pressure));
      {
        char operator_name[PETSC_MAX_PATH_LEN];

        PetscCall(PetscSNPrintf(operator_name, sizeof(operator_name), "pressure - face %" PetscInt_FMT, ratel->bc_pressure_faces[i]));
        RatelCallCeed(ratel, CeedOperatorSetName(sub_op_pressure, operator_name));
      }
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_pressure, "dx_initial/dX", restriction_x_face, basis_x_face, x_loc));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_pressure, "weight", CEED_ELEMRESTRICTION_NONE, basis_x_face, CEED_VECTOR_NONE));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_pressure, "du/dX", restriction_u_face, basis_u_face, CEED_VECTOR_ACTIVE));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_pressure, "v", restriction_u_face, basis_u_face, CEED_VECTOR_ACTIVE));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_pressure, "dx/dX", restriction_dxdX, CEED_BASIS_NONE, stored_dxdX));
      // ---- Add to composite operator
      RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_residual, sub_op_pressure));
      if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_pressure, stdout));

      // ---- Jacobian QFunction
      PetscCall(RatelDebug(ratel, "---- Setting up Jacobian evaluator"));
      RatelCallCeed(ratel,
                    CeedQFunctionCreateInterior(ceed, 1, PressureBCsJacobian, RatelQFunctionRelativePath(PressureBCsJacobian_loc), &sub_qf_jacobian));
      RatelCallCeed(ratel, CeedQFunctionAddInput(sub_qf_jacobian, "dx/dX", stored_dxdX_size, CEED_EVAL_NONE));
      RatelCallCeed(ratel, CeedQFunctionAddInput(sub_qf_jacobian, "weight", 1, CEED_EVAL_WEIGHT));
      RatelCallCeed(ratel, CeedQFunctionAddInput(sub_qf_jacobian, "d(du)/dX", num_comp_u * (num_comp_u - height_face), CEED_EVAL_GRAD));
      RatelCallCeed(ratel, CeedQFunctionAddOutput(sub_qf_jacobian, "v", num_comp_u, CEED_EVAL_INTERP));
      RatelCallCeed(ratel, CeedQFunctionSetUserFlopsEstimate(sub_qf_jacobian, FLOPS_Jacobian_PressureBC));
      // ---- Context
      RatelCallCeed(ratel, CeedQFunctionSetContext(sub_qf_jacobian, ctx_pressure));
      RatelCallCeed(ratel, CeedQFunctionSetContextWritable(sub_qf_jacobian, PETSC_FALSE));
      // ---- Jacobian operator
      RatelCallCeed(ratel, CeedOperatorCreate(ceed, sub_qf_jacobian, NULL, NULL, &sub_op_jacobian));
      {
        char operator_name[PETSC_MAX_PATH_LEN];

        PetscCall(PetscSNPrintf(operator_name, sizeof(operator_name), "pressure - face %" PetscInt_FMT, ratel->bc_pressure_faces[i]));
        RatelCallCeed(ratel, CeedOperatorSetName(sub_op_jacobian, operator_name));
      }
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_jacobian, "dx/dX", restriction_dxdX, CEED_BASIS_NONE, stored_dxdX));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_jacobian, "weight", CEED_ELEMRESTRICTION_NONE, basis_x_face, CEED_VECTOR_NONE));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_jacobian, "d(du)/dX", restriction_u_face, basis_u_face, CEED_VECTOR_ACTIVE));
      RatelCallCeed(ratel, CeedOperatorSetField(sub_op_jacobian, "v", restriction_u_face, basis_u_face, CEED_VECTOR_ACTIVE));

      // -- Add to composite operator
      RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_jacobian, sub_op_jacobian));
      if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedOperatorView(sub_op_jacobian, stdout));

      // -- Cleanup
      RatelCallCeed(ratel, CeedVectorDestroy(&stored_dxdX));
      RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_x_face));
      RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_u_face));
      RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_dxdX));
      RatelCallCeed(ratel, CeedQFunctionContextDestroy(&ctx_pressure));
      RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_pressure));
      RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_pressure));
      RatelCallCeed(ratel, CeedQFunctionDestroy(&sub_qf_jacobian));
      RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_jacobian));
    }
    // Cleanup
    RatelCallCeed(ratel, CeedVectorDestroy(&x_loc));
    RatelCallCeed(ratel, CeedBasisDestroy(&basis_x_face));
    RatelCallCeed(ratel, CeedBasisDestroy(&basis_u_face));
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Ceed Add Boundaries Pressure Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup multigrid level suboperators for `RatelMaterial` pressure boundary conditions Jacobian.

  Collective across MPI processes.

  @param[in]      ratel               `Ratel` context
  @param[in]      dm_level            `DMPlex` for multigrid level to setup
  @param[in]      m_loc               `CeedVector` holding multiplicity
  @param[in]      op_jacobian_fine    Fine grid Jacobian `CeedOperator`
  @param[in,out]  op_jacobian_coarse  Composite Jacobian `CeedOperator` to add `RatelMaterial` suboperators

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSetupPressureJacobianMultigridLevel(Ratel ratel, DM dm_level, CeedVector m_loc, CeedOperator op_jacobian_fine,
                                                        CeedOperator op_jacobian_coarse) {
  const PetscInt height_face = 1;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Pressure Jacobian Multigrid Level FEM"));

  if (ratel->bc_pressure_count > 0) {
    DM        dm_solution;
    DMLabel   domain_label;
    CeedBasis basis_u_face = NULL;

    PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));
    PetscCall(DMGetLabel(dm_solution, "Face Sets", &domain_label));

    // Basis
    PetscCall(RatelDebug(ratel, "---- Setting up libCEED bases"));
    PetscCall(RatelDMPlexCeedBasisCreate(ratel, dm_level, NULL, 0, height_face, 0, &basis_u_face));

    CeedOperator *sub_op_jacobian_fine;
    RatelCallCeed(ratel, CeedCompositeOperatorGetSubList(op_jacobian_fine, &sub_op_jacobian_fine));

    // Compute contribution on each boundary face
    PetscInt op_index = ratel->first_jacobian_pressure_index;
    for (PetscInt i = 0; i < ratel->bc_pressure_count; i++) {
      CeedInt             num_elem;
      CeedElemRestriction restriction_u_face     = NULL;
      CeedOperator        sub_op_jacobian_coarse = NULL;

      PetscCall(RatelDebug(ratel, "---- Setting up pressure boundary: %" CeedInt_FMT, i));

      // -- Restrictions
      PetscCall(RatelDebug(ratel, "---- Setting up libCEED restriction"));
      PetscCall(
          RatelDMPlexCeedElemRestrictionCreate(ratel, dm_level, domain_label, ratel->bc_pressure_faces[i], height_face, 0, &restriction_u_face));
      RatelCallCeed(ratel, CeedElemRestrictionGetNumElements(restriction_u_face, &num_elem));

      // If the face is present on the process
      if (num_elem > 0) {
        // -- Create coarse grid operator
        RatelCallCeed(ratel, CeedOperatorMultigridLevelCreate(sub_op_jacobian_fine[op_index++], NULL, restriction_u_face, basis_u_face,
                                                              &sub_op_jacobian_coarse, NULL, NULL));

        // -- Add to composite operators
        RatelCallCeed(ratel, CeedCompositeOperatorAddSub(op_jacobian_coarse, sub_op_jacobian_coarse));
      }

      // -- Cleanup
      RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_u_face));
      RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op_jacobian_coarse));
    }
    // Cleanup
    PetscCall(DMDestroy(&dm_solution));
    RatelCallCeed(ratel, CeedBasisDestroy(&basis_u_face));
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Material Setup Pressure Jacobian Multigrid Level FEM Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Read bounding box from options.

  Collective across MPI processes.

  @param[in]   ratel          `Ratel` context
  @param[in]   option_prefix  Prefix for options
  @param[in]   name           Name of face
  @param[out]  bounding_box   Output bounding box

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelBoundingBoxParamsFromOptions(Ratel ratel, const char option_prefix[], const char name[], RatelBoundingBoxParams *bounding_box) {
  char cl_prefix[PETSC_MAX_OPTION_NAME], cl_text[PETSC_MAX_OPTION_NAME];

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Setup Bounding Box Context"));

  // Set the options prefix first
  PetscCall(PetscSNPrintf(cl_prefix, sizeof(cl_prefix), "%s%s_", option_prefix, name));
  PetscCall(PetscSNPrintf(cl_text, sizeof(cl_text), "Read bounding box options for -%s", cl_prefix));
  PetscOptionsBegin(ratel->comm, cl_prefix, cl_text, NULL);

  // Read bounding box information
  PetscBool has_bounding_box = PETSC_FALSE;
  PetscInt  num_values       = 6;
  PetscReal box_values[6]    = {-INFINITY, -INFINITY, -INFINITY, INFINITY, INFINITY, INFINITY};
  PetscCall(
      PetscOptionsRealArray("-bounding_box", "Bounding box (xmin, ymin, zmin, xmax, ymax, zmax)", NULL, box_values, &num_values, &has_bounding_box));

  // Check inputs
  PetscCheck(!has_bounding_box || num_values == 6, ratel->comm, PETSC_ERR_USER_INPUT,
             "Bounding box must have 6 values: xmin, ymin, zmin, xmax, ymax, zmax");

  for (PetscInt j = 0; j < 3; j++) {
    bounding_box->min[j] = box_values[j];
    bounding_box->max[j] = box_values[j + 3];
  }
  PetscOptionsEnd();  // End of surface force face options

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Setup Bounding Box Context Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Read label value from options.

  Collective across MPI processes.

  @param[in]   ratel          `Ratel` context
  @param[in]   option_prefix  Prefix for options
  @param[in]   name           Name of face
  @param[out]  label_value    Label value read from options

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelFaceLabelValueFromOptions(Ratel ratel, const char option_prefix[], const char name[], PetscInt *label_value) {
  char      cl_prefix[PETSC_MAX_OPTION_NAME], cl_text[PETSC_MAX_OPTION_NAME];
  PetscBool has_label = PETSC_FALSE;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Face Name to Label Value"));

  // Read face label value options
  PetscCall(PetscSNPrintf(cl_prefix, sizeof(cl_prefix), "%s%s_", option_prefix, name));
  PetscCall(PetscSNPrintf(cl_text, sizeof(cl_text), "Read face label value options for -%s", cl_prefix));
  PetscOptionsBegin(ratel->comm, cl_prefix, cl_text, NULL);
  *label_value = -1;
  PetscCall(PetscOptionsInt("-label_value", "Mesh partitioning value for face", NULL, *label_value, label_value, &has_label));
  if (!has_label) PetscCall(PetscOptionsStringToInt(name, label_value));
  PetscOptionsEnd();  // End of surface force face options

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Face Name to Label Value Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}
