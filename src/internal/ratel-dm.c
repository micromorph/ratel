/// @file
/// Ratel DM setup

#include <ceed-evaluator.h>
#include <ceed.h>
#include <math.h>
#include <petsc-ceed-utils.h>
#include <petsc/private/petscfeimpl.h> /* For interpolation */
#include <petscdmplex.h>
#include <petscdmswarm.h>
#include <petscds.h>
#include <ratel-boundary.h>
#include <ratel-dm.h>
#include <ratel-fem.h>
#include <ratel-impl.h>
#include <ratel-material.h>
#include <ratel-mpm.h>
#include <ratel.h>
#include <stddef.h>
#include <stdio.h>

/// @addtogroup RatelInternal
/// @{

/**
  @brief Increment the state for mesh remapping to invalidate cached data.

  Not collective across MPI processes.

  @param[in,out]  ratel  `Ratel` context

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelIncrementMeshRemapState(Ratel ratel) {
  PetscFunctionBeginUser;
  ratel->mesh_remap_state++;
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get the state for mesh remapping.

  Not collective across MPI processes.

  @param[in]   ratel  `Ratel` context
  @param[out]  state  Current mesh remapping state

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelGetMeshRemapState(Ratel ratel, PetscInt *state) {
  PetscFunctionBeginUser;
  *state = ratel->mesh_remap_state;
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Remap coordinates by scaling in a direction.

  Not collective across MPI processes.

  @param[in]   cdim           Coordinate dimension
  @param[in]   x_prev         Old coordinates
  @param[in]   t              Time
  @param[in]   num_constants  Number of constants, should be 6
  @param[in]   constants      Array of constants: [direction, z0, h0, hf, h_prev, h]
  @param[out]  x              New coordinates
**/
void RatelRemapScale_inner(const PetscInt cdim, const PetscScalar x_prev[], const PetscReal t, const PetscInt num_constants,
                           const PetscScalar constants[], PetscScalar x[]) {
  RatelDirectionType dir;
  // Unpack constants
  // [0] direction: direction to remap, stored as RatelDirectionType
  // Have to memcpy to preserve enum value
  memcpy(&dir, constants, sizeof(dir));
  // [1] z0: initial minimum z value
  const PetscScalar z0     = constants[1];
  // [2] h0: initial height, unused here
  // [3] hf: final height, unused here
  // [4] h_prev: previous height at t_{n-1}
  const PetscScalar h_prev = constants[4];
  // [5] h: current height at t_n
  const PetscScalar h      = constants[5];

  // Remap:
  // 1. Copy old values
  for (PetscInt i = 0; i < cdim; i++) {
    if (i == dir) continue;
    x[i] = x_prev[i];
  }

  // Compute new z = x[dir]
  // Assume z(t) = z0 + alpha * height(t), where 0 <= alpha <= 1 is constant over time
  // Can solve for alpha using z(t_prev) = z0 + alpha * height(t_prev)
  //  => alpha = (z(t_prev) - z0) / height(t_prev)
  const PetscScalar alpha = (x_prev[dir] - z0) / h_prev;

  x[dir] = z0 + alpha * h;
}

/**
  @brief Callback function to remap coordinates by scaling in a direction.

  Not collective across MPI processes.

  @note This function signature is required by PETSc. See`RatelRemapScale_inner()` for the actual remapping logic.

  @param[in]   dim            Solution dimension
  @param[in]   Nf             Number of fields, 1
  @param[in]   NfAux          Number of auxiliary fields, 0
  @param[in]   uOff           Offset of this point into local solution vector, [0, cdim]
  @param[in]   uOff_x         Unused
  @param[in]   u              Previous coordinates at point
  @param[in]   u_t            Unused
  @param[in]   u_x            Unused
  @param[in]   aOff           Unused
  @param[in]   aOff_x         Unused
  @param[in]   a              Unused
  @param[in]   a_t            Unused
  @param[in]   a_x            Unused
  @param[in]   t              Current time
  @param[in]   x              Unused
  @param[in]   num_constants  Number of constants, should be 6
  @param[in]   constants      Array of constants
  @param[out]  f              Remapped coordinates at points
**/
void RatelRemapScale(PetscInt dim, PetscInt Nf, PetscInt NfAux, const PetscInt uOff[], const PetscInt uOff_x[], const PetscScalar u[],
                     const PetscScalar u_t[], const PetscScalar u_x[], const PetscInt aOff[], const PetscInt aOff_x[], const PetscScalar a[],
                     const PetscScalar a_t[], const PetscScalar a_x[], PetscReal t, const PetscReal x[], PetscInt num_constants,
                     const PetscScalar constants[], PetscScalar f[]) {
  // Not sure why dim isn't the coordinate dimension (especially since coordinates are passed as `u`), but alas
  const PetscInt cdim = uOff[1] - uOff[0];
  PetscCheckAbort(num_constants == 6, PETSC_COMM_SELF, PETSC_ERR_ARG_WRONG, "num_constants must be 6");

  RatelRemapScale_inner(cdim, u, t, num_constants, constants, f);
}

/**
  @brief Set remap scale parameters from command-line options.

  Collective across MPI processes.

  @param[in]      ratel  `Ratel` context
  @param[in,out]  dm     Solution mesh `DM`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelSetRemapScaleParametersFromOptions(Ratel ratel, DM dm) {
  PetscReal          scale     = 1.0;
  PetscBool          set_scale = PETSC_FALSE, set_dir = PETSC_FALSE;
  RatelDirectionType dir = RATEL_DIRECTION_X;

  PetscFunctionBeginUser;
  PetscOptionsBegin(ratel->comm, "", "Scale parameters for z coordinate", "");
  PetscCall(PetscOptionsEnum("-remap_direction", "Direction to remap", NULL, RatelDirectionTypesCL, (PetscEnum)dir, (PetscEnum *)&dir, &set_dir));
  PetscCall(PetscOptionsBoundedReal("-remap_scale", "Scaling factor for coordinate, linear over time", NULL, scale, &scale, &set_scale,
                                    PETSC_MACHINE_EPSILON));
  PetscOptionsEnd();

  if (set_dir && set_scale) {
    DM          cdm;
    PetscDS     cds;
    PetscScalar new_constants[6] = {0.};
    PetscScalar upper[3], lower[3];

    PetscCall(DMGetBoundingBox(dm, lower, upper));
    // Set constants
    // direction: directly copy enum memory, always smaller than a PetscScalar
    PetscCall(PetscMemcpy(new_constants, &dir, sizeof(dir)));
    // z0: initial minimum z value
    new_constants[1] = lower[dir];
    // h0: initial height
    new_constants[2] = upper[dir] - lower[dir];
    // hf: final height
    new_constants[3] = scale * new_constants[2];
    // h_prev: previous height at t_{n-1}, set to h0
    new_constants[4] = new_constants[2];
    // h: current height at t_n, set to h0
    new_constants[5] = new_constants[2];

    PetscCall(DMGetCoordinateDM(dm, &cdm));
    PetscCall(DMGetDS(cdm, &cds));
    PetscCall(PetscDSSetConstants(cds, 6, new_constants));
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Remap coordinates by scaling in a direction.

  Not collective across MPI processes.

  @param[in]      ratel  `Ratel` context
  @param[in]      t      Current time
  @param[in,out]  dm     Solution mesh `DM`, may have coordinates remapped by scaling

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelRemapScaleCoordinates(Ratel ratel, PetscReal t, DM dm) {
  DM                 cdm;
  PetscDS            cds;
  PetscInt           num_constants;
  const PetscInt     correct_num_constants = 6;
  const PetscScalar *constants;
  PetscScalar        new_constants[correct_num_constants];

  PetscFunctionBeginUser;
  PetscCall(DMGetCoordinateDM(dm, &cdm));
  PetscCall(DMGetDS(cdm, &cds));
  PetscCall(PetscDSGetConstants(cds, &num_constants, &constants));
  if (num_constants != correct_num_constants) {
    PetscCall(RatelSetRemapScaleParametersFromOptions(ratel, dm));
    PetscCall(PetscDSGetConstants(cds, &num_constants, &constants));
  }
  // If constants were not set, we shouldn't remap
  if (num_constants != correct_num_constants) PetscFunctionReturn(PETSC_SUCCESS);

  // Copy old contants and compute new height
  // Have to memcpy to preserve enum value
  PetscCall(PetscMemcpy(new_constants, constants, 4 * sizeof(PetscScalar)));
  // Set h_prev to old h
  new_constants[4] = constants[5];
  // Compute new h = h0 - t * (h0 - hf)
  new_constants[5] = constants[2] - t * (constants[2] - constants[3]);
  PetscCall(PetscDSSetConstants(cds, correct_num_constants, new_constants));

  {
    PetscBool    is_simplex;
    PetscSection coord_section;

    // Have to reset the closure permutation before remapping
    // This is janky, but it's the only way to get the correct permutation
    PetscCall(DMGetLocalSection(cdm, &coord_section));
    PetscCall(PetscSectionResetClosurePermutation(coord_section));
    // Remap coordinates by scaling in a direction
    PetscCall(DMPlexRemapGeometry(dm, t, RatelRemapScale));
    PetscCall(DMPlexIsSimplex(dm, &is_simplex));
    if (!is_simplex) {
      PetscCall(DMPlexSetClosurePermutationTensor(cdm, PETSC_DETERMINE, NULL));
    }
  }
  PetscCall(RatelIncrementMeshRemapState(ratel));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Transition from a value of "a" for x=0, to a value of "b" for x=1.

  Collective across MPI processes.

  @param[in]   a  Scalar value
  @param[in]   b  Scalar value
  @param[in]   x  Scalar value

  @return `a + (b - a) * (x)`
**/
static CeedScalar RatelTransition(CeedScalar a, CeedScalar b, CeedScalar x) {
  if (x <= 0) return a;
  if (x >= 1) return b;
  return a + (b - a) * (x);
}

/**
  @brief 1D transformation at the right boundary.

  Collective across MPI processes.

  @param[in]   eps  Scalar value in (0, 1]
  @param[in]   x    Scalar value

  @return 1D transformation at right boundary
**/
static CeedScalar RatelTransformRightBoundary_1D(CeedScalar eps, CeedScalar x) { return (x <= 0.5) ? (2 - eps) * x : 1 + eps * (x - 1); }

/**
  @brief 1D transformation at the left boundary.

  Collective across MPI processes.

  @param[in]   eps  Scalar value in (0, 1]
  @param[in]   x    Scalar value

  @return 1D transformation at left boundary
**/
static CeedScalar RatelTransformLeftBoundary_1D(CeedScalar eps, CeedScalar x) { return 1 - RatelTransformRightBoundary_1D(eps, 1 - x); }

/**
  @brief Apply 3D Kershaw mesh transformation.

  Collective across MPI processes.

  @param[in,out]  dm   `DMPlex` holding mesh
  @param[in]      eps   Scalar value in (0, 1]. Uniform mesh is recovered for eps=1

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelKershaw(DM dm, PetscScalar eps) {
  Vec          coord;
  PetscInt     coord_loc_size;
  PetscScalar *coord_array;

  PetscFunctionBeginUser;
  PetscCall(DMGetCoordinatesLocal(dm, &coord));
  PetscCall(VecGetLocalSize(coord, &coord_loc_size));
  PetscCall(VecGetArray(coord, &coord_array));

  for (PetscInt i = 0; i < coord_loc_size; i += 3) {
    PetscScalar x = coord_array[i], y = coord_array[i + 1], z = coord_array[i + 2];
    PetscInt    layer  = x * 6;
    PetscScalar lambda = (x - layer / 6.0) * 6;
    coord_array[i]     = x;

    switch (layer) {
      case 0:
        coord_array[i + 1] = RatelTransformLeftBoundary_1D(eps, y);
        coord_array[i + 2] = RatelTransformLeftBoundary_1D(eps, z);
        break;
      case 1:
      case 4:
        coord_array[i + 1] = RatelTransition(RatelTransformLeftBoundary_1D(eps, y), RatelTransformRightBoundary_1D(eps, y), lambda);
        coord_array[i + 2] = RatelTransition(RatelTransformLeftBoundary_1D(eps, z), RatelTransformRightBoundary_1D(eps, z), lambda);
        break;
      case 2:
        coord_array[i + 1] = RatelTransition(RatelTransformRightBoundary_1D(eps, y), RatelTransformLeftBoundary_1D(eps, y), lambda / 2);
        coord_array[i + 2] = RatelTransition(RatelTransformRightBoundary_1D(eps, z), RatelTransformLeftBoundary_1D(eps, z), lambda / 2);
        break;
      case 3:
        coord_array[i + 1] = RatelTransition(RatelTransformRightBoundary_1D(eps, y), RatelTransformLeftBoundary_1D(eps, y), (1 + lambda) / 2);
        coord_array[i + 2] = RatelTransition(RatelTransformRightBoundary_1D(eps, z), RatelTransformLeftBoundary_1D(eps, z), (1 + lambda) / 2);
        break;
      default:
        coord_array[i + 1] = RatelTransformRightBoundary_1D(eps, y);
        coord_array[i + 2] = RatelTransformRightBoundary_1D(eps, z);
    }
  }
  PetscCall(VecRestoreArray(coord, &coord_array));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Destroy `ElemRestriction` in a `PetscContainer`.

  Not collective across MPI processes.

  @param[in,out]  ctx  `CeedElemRestriction`

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelDMPlexCeedElemRestrictionDestroy(void **ctx) {
  CeedElemRestriction rstr = *(CeedElemRestriction *)ctx;

  PetscFunctionBegin;
  PetscCallCeed(CeedElemRestrictionReturnCeed(rstr), CeedElemRestrictionDestroy(&rstr));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Destroy `CeedBasis` in a `PetscContainer`.

  Not collective across MPI processes.

  @param[in,out]  ctx  `CeedBasis`

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelDMPlexCeedBasisDestroy(void **ctx) {
  CeedBasis basis = *(CeedBasis *)ctx;

  PetscFunctionBegin;
  PetscCallCeed(CeedBasisReturnCeed(basis), CeedBasisDestroy(&basis));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Read mesh and setup `DMPlex` from options.

  Collective across MPI processes.

  @param[in]   ratel     `Ratel` context
  @param[in]   vec_type  Default vector type (can be changed at run-time using `-dm_vec_type`)
  @param[out]  dm        New distributed `DMPlex`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDMPlexCreateFromOptions(Ratel ratel, VecType vec_type, DM *dm) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMPlex Create From Options"));

  // Distribute DM in parallel
  PetscCall(DMCreate(ratel->comm, dm));
  PetscCall(DMSetType(*dm, DMPLEX));
  PetscCall(DMSetVecType(*dm, vec_type));
  PetscCall(DMSetFromOptions(*dm));
  PetscCall(DMViewFromOptions(*dm, NULL, "-orig_dm_view"));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMPlex Create From Options Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup `DMSwarm` from options.

  Collective across MPI processes.

  @param[in]   ratel     `Ratel` context
  @param[out]  dm_mesh   Existing cell`DMPlex`
  @param[out]  dm        New distributed `DMSwarm`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDMSwarmCreateFromOptions(Ratel ratel, DM dm_mesh, DM *dm) {
  PetscInt dim;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMSwarm Create From Options"));
  PetscCall(DMGetDimension(dm_mesh, &dim));

  // Distribute DM in parallel
  PetscCall(DMCreate(ratel->comm, dm));
  PetscCall(DMSetType(*dm, DMSWARM));
  PetscCall(DMSetDimension(*dm, dim));
  PetscCall(DMSwarmSetType(*dm, DMSWARM_PIC));
  PetscCall(DMSwarmSetCellDM(*dm, dm_mesh));
  PetscCall(PetscObjectSetOptionsPrefix((PetscObject)*dm, "swarm_"));
  PetscCall(DMSetFromOptions(*dm));
  PetscCall(DMViewFromOptions(*dm, NULL, "-orig_dm_view"));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMSwarm Create From Options Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Initialize point locations for MPM using `RatelMPMOptions` context.

  Collective across MPI processes.

  @param[in]   ratel        `Ratel` context
  @param[in]   mpm_options  `RatelMPMOptions` context
  @param[out]  dm_swarm     `DMSwarm` to initialize point locations for

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDMSwarmInitalizePointLocations(Ratel ratel, RatelMPMOptions mpm_options, DM dm_swarm) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMSwarm Initialize Point Locations"));

  switch (mpm_options->point_location_type) {
    case RATEL_POINT_LOCATION_GAUSS:
    case RATEL_POINT_LOCATION_UNIFORM: {
      // -- Set gauss or uniform point locations in each cell
      PetscInt    num_points_per_cell_1d = round(cbrt(mpm_options->num_points_per_cell * 1.0)), dim = 3;
      PetscScalar point_coords[mpm_options->num_points_per_cell * 3];
      CeedScalar  points_1d[num_points_per_cell_1d], weights_1d[num_points_per_cell_1d];

      if (mpm_options->point_location_type == RATEL_POINT_LOCATION_GAUSS) {
        PetscCall(CeedGaussQuadrature(num_points_per_cell_1d, points_1d, weights_1d));
      } else {
        for (PetscInt i = 0; i < num_points_per_cell_1d; i++) points_1d[i] = 2.0 * (PetscReal)(i + 0.5) / (PetscReal)num_points_per_cell_1d - 1;
      }
      for (PetscInt i = 0; i < num_points_per_cell_1d; i++) {
        for (PetscInt j = 0; j < num_points_per_cell_1d; j++) {
          for (PetscInt k = 0; k < num_points_per_cell_1d; k++) {
            PetscInt p = (i * num_points_per_cell_1d + j) * num_points_per_cell_1d + k;

            point_coords[p * dim + 0] = points_1d[i];
            point_coords[p * dim + 1] = points_1d[j];
            point_coords[p * dim + 2] = points_1d[k];
          }
        }
      }
      PetscCall(DMSwarmSetPointCoordinatesCellwise(dm_swarm, num_points_per_cell_1d * num_points_per_cell_1d * num_points_per_cell_1d, point_coords));
    } break;
    case RATEL_POINT_LOCATION_CELL_RANDOM: {
      DM dm_mesh;

      PetscCall(DMSwarmGetCellDM(dm_swarm, &dm_mesh));
      // DMSwarmSetPointCoordinatesRandom expects local coordinates to be set up to ensure call is non-collective
      PetscCall(DMGetCoordinatesLocalSetUp(dm_mesh));
      PetscCall(DMSwarmSetPointCoordinatesRandom(dm_swarm, mpm_options->num_points_per_cell));
    } break;
  }
  PetscCall(DMSwarmMigrate(dm_swarm, PETSC_TRUE));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMSwarm Initialize Point Locations Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get the index set for the domain of a given height.

  Not collective across MPI processes.

  @param[in]   ratel         `Ratel` context
  @param[in]   dm            `DMPlex` to get domain index set from
  @param[in]   domain_label  `DMLabel` for domain stratum
  @param[in]   domain_value  Value in domain stratum
  @param[in]   height        Topological height, e.g., 0 for cells, 1 for faces
  @param[out]  is            Output index set for domain and height

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelDMGetDomainISLocal(Ratel ratel, DM dm, DMLabel domain_label, PetscInt domain_value, PetscInt height, IS *is) {
  DMLabel  depth_label;
  IS       is_depth;
  PetscInt depth;

  PetscFunctionBeginUser;
  // Get depth IS
  PetscCall(DMPlexGetDepthLabel(dm, &depth_label));
  PetscCall(DMPlexGetDepth(dm, &depth));
  PetscCall(DMLabelGetStratumIS(depth_label, depth - height, &is_depth));
  if (domain_label) {
    IS is_domain;

    PetscCall(DMLabelGetStratumIS(domain_label, domain_value, &is_domain));
    if (is_domain) {  // domainIS is non-empty
      PetscCall(ISIntersect(is_domain, is_depth, is));
      PetscCall(ISDestroy(&is_domain));
    } else {  // domainIS is NULL (empty)
      PetscCall(ISCreateGeneral(PETSC_COMM_SELF, 0, NULL, PETSC_OWN_POINTER, is));
    }
    PetscCall(ISDestroy(&is_depth));
  } else {
    *is = is_depth;
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  DMSwarmCreateReferenceCoordinates - Compute the cell reference coordinates for local DMSwarm points.

  Collective across MPI processes.

  The index set contains ranges of indices for each local cell. This range contains the indices of every point in the cell.

  ```
  total_num_cells
  cell_0_start_index
  cell_1_start_index
  cell_2_start_index
  ...
  cell_n_start_index
  cell_n_stop_index
  cell_0_point_0
  cell_0_point_0
  ...
  cell_n_point_m
  ```

  @param[in]   ratel         `Ratel` context
  @param[in]   dm_swarm      `DMSwarm` to compute reference coordinates for
  @param[in]   domain_label  Label for domain
  @param[in]   domain_value  Value for domain label
  @param[out]  is_points     Index set for each local cell
  @param[out]  X_points_ref  Reference coordinates for each local point

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDMSwarmCreateReferenceCoordinates(Ratel ratel, DM dm_swarm, DMLabel domain_label, PetscInt domain_value, IS *is_points,
                                                      Vec *X_points_ref) {
  const char       **field_coords;
  PetscInt           num_cells_local, dim, num_points_processed = 0, num_points_local, *cell_points, points_offset;
  PetscScalar       *coords_points_ref;
  const PetscScalar *coords_points_true;
  DM                 dm_mesh;
  IS                 is_cells_in_domain;
  const PetscInt    *cells_in_domain;
  const PetscInt     height_cell = 0;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMSwarm Create Reference Coordinates"));
  PetscCall(DMSwarmGetCellDM(dm_swarm, &dm_mesh));

  // Create vector to hold reference coordinates
  {
    Vec           X_points_true;
    DMSwarmCellDM dm_cell;

    PetscCall(DMSwarmGetCellDMActive(dm_swarm, &dm_cell));
    PetscCall(DMSwarmCellDMGetCoordinateFields(dm_cell, NULL, &field_coords));
    PetscCall(DMSwarmCreateLocalVectorFromField(dm_swarm, field_coords[0], &X_points_true));
    PetscCall(VecDuplicate(X_points_true, X_points_ref));
    PetscCall(DMSwarmDestroyLocalVectorFromField(dm_swarm, field_coords[0], &X_points_true));
  }

  // Allocate index set array
  PetscCall(RatelDMGetDomainISLocal(ratel, dm_mesh, domain_label, domain_value, height_cell, &is_cells_in_domain));
  PetscCall(ISGetLocalSize(is_cells_in_domain, &num_cells_local));
  points_offset = num_cells_local + 2;
  PetscCall(VecGetLocalSize(*X_points_ref, &num_points_local));
  PetscCall(DMGetDimension(dm_mesh, &dim));
  num_points_local /= dim;
  // Note: cell_points may be larger than needed
  PetscCall(PetscCalloc1(num_points_local + num_cells_local + 2, &cell_points));
  cell_points[0] = num_cells_local;
  if (num_cells_local > 0) {
    // Get reference coordinates for each swarm point wrt the elements in the background mesh
    PetscCall(DMSwarmSortGetAccess(dm_swarm));
    PetscCall(DMSwarmGetField(dm_swarm, field_coords[0], NULL, NULL, (void **)&coords_points_true));
    PetscCall(VecGetArray(*X_points_ref, &coords_points_ref));
    PetscCall(ISGetIndices(is_cells_in_domain, &cells_in_domain));
    for (PetscInt i = 0; i < num_cells_local; i++) {
      PetscInt  cell = cells_in_domain[i], *points_in_cell, num_points_in_cell;
      PetscReal v[3], J[9], invJ[9], detJ, v0_ref[3] = {-1.0, -1.0, -1.0};

      // Check if current point is on target DMPlex face
      PetscCall(DMSwarmSortGetPointsPerCell(dm_swarm, cell, &num_points_in_cell, &points_in_cell));
      // -- Reference coordinates for swarm points in background mesh element
      PetscCall(DMPlexComputeCellGeometryFEM(dm_mesh, cell, NULL, v, J, invJ, &detJ));
      cell_points[i + 1] = num_points_processed + points_offset;
      for (PetscInt p = 0; p < num_points_in_cell; p++) {
        const CeedInt point = points_in_cell[p];

        cell_points[points_offset + (num_points_processed++)] = point;
        CoordinatesRealToRef(dim, dim, v0_ref, v, invJ, &coords_points_true[point * dim], &coords_points_ref[point * dim]);
      }

      // -- Cleanup
      PetscCall(DMSwarmSortRestorePointsPerCell(dm_swarm, cell, &num_points_in_cell, &points_in_cell));
    }
    cell_points[points_offset - 1] = num_points_processed + points_offset;
    PetscCall(ISRestoreIndices(is_cells_in_domain, &cells_in_domain));
    PetscCall(VecRestoreArray(*X_points_ref, &coords_points_ref));
    PetscCall(DMSwarmRestoreField(dm_swarm, field_coords[0], NULL, NULL, (void **)&coords_points_true));
    PetscCall(DMSwarmSortRestoreAccess(dm_swarm));
  }

  // Cleanup
  PetscCall(ISDestroy(&is_cells_in_domain));

  // Create index set
  PetscCall(ISCreateGeneral(PETSC_COMM_SELF, num_points_processed + points_offset, cell_points, PETSC_OWN_POINTER, is_points));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMSwarm Create Reference Coordinates Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Create a `CeedElemRestriction` for the reference coordinates of the `DMSwarm` points.

  Collective across MPI processes.

  @param[in]   ratel                 `Ratel` context
  @param[in]   dm_swarm              `DMSwarm` to create reference coordinates for
  @param[in]   domain_label          DMLabel for domain in mesh `DM`
  @param[in]   domain_value          Value for domain label
  @param[out]  x_ref_points          Output `CeedVector` for reference coordinates
  @param[out]  restriction_x_points  Output `CeedElemRestriction` for reference coordinates

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDMSwarmCeedElemRestrictionPointsCreate(Ratel ratel, DM dm_swarm, DMLabel domain_label, PetscInt domain_value,
                                                           CeedVector *x_ref_points, CeedElemRestriction *restriction_x_points) {
  IS              is_points;
  Vec             X_ref;
  DM              dm_mesh;
  const PetscInt *cell_points;
  PetscInt        dim, num_points, num_points_total;
  CeedInt         num_elem;
  CeedInt        *offsets;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMSwarm Ceed Elem Restriction Points Create"));

  // Points
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_mesh));
  PetscCall(DMGetDimension(dm_mesh, &dim));
  PetscCall(RatelDMSwarmCreateReferenceCoordinates(ratel, dm_swarm, domain_label, domain_value, &is_points, &X_ref));

  PetscCall(ISGetIndices(is_points, &cell_points));
  num_elem = cell_points[0];
  PetscCall(ISGetLocalSize(is_points, &num_points));
  num_points -= num_elem + 2;
  PetscCall(DMSwarmGetLocalSize(dm_swarm, &num_points_total));
  PetscCall(PetscCalloc1(num_elem + 1 + num_points, &offsets));

  for (PetscInt i = 0; i < num_elem + 1; i++) offsets[i] = cell_points[i + 1] - 1;
  for (PetscInt i = num_elem + 1; i < num_points + num_elem + 1; i++) offsets[i] = cell_points[i + 1];
  PetscCall(ISRestoreIndices(is_points, &cell_points));

  {
    PetscMemType       X_mem_type;
    const PetscScalar *x;
    PetscInt           x_ref_size;

    PetscCall(VecGetLocalSize(X_ref, &x_ref_size));
    RatelCallCeed(ratel, CeedVectorCreate(ratel->ceed, x_ref_size, x_ref_points));

    PetscCall(VecGetArrayReadAndMemType(X_ref, (const PetscScalar **)&x, &X_mem_type));
    CeedVectorSetArray(*x_ref_points, MemTypePetscToCeed(X_mem_type), CEED_COPY_VALUES, (CeedScalar *)x);
    PetscCall(VecRestoreArrayReadAndMemType(X_ref, (const PetscScalar **)&x));
  }

  // libCEED objects
  RatelCallCeed(ratel, CeedElemRestrictionCreateAtPoints(ratel->ceed, num_elem, num_points, dim, num_points_total * dim, CEED_MEM_HOST,
                                                         CEED_COPY_VALUES, offsets, restriction_x_points));

  // Cleanup
  PetscCall(ISRestoreIndices(is_points, &cell_points));
  PetscCall(ISDestroy(&is_points));
  PetscCall(VecDestroy(&X_ref));
  PetscCall(PetscFree(offsets));
  PetscCall(DMDestroy(&dm_mesh));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMSwarm Ceed Elem Restriction Points Create Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

// LCOV_EXCL_START
/**
  @brief View `DMSwarm` fields from options.

  Collective across MPI processes.

  @param[in]  ratel     `Ratel` context
  @param[in]  dm_swarm  `DMSwarm` to view fields from
  @param[in]  option    View option string

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDMSwarmViewFromOptions(Ratel ratel, DM dm_swarm, const char option[]) {
  PetscViewer       viewer = NULL;
  PetscViewerFormat viewer_format;
  PetscViewerType   viewer_type;
  PetscBool         set, is_ascii;
  const char       *filename_tmp = NULL;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMSwarm View From Options"));

  PetscOptionsBegin(ratel->comm, NULL, "DMSwarm Options", "");
  PetscCall(PetscOptionsViewer(option, "DMSwarm to view", NULL, &viewer, &viewer_format, &set));
  PetscOptionsEnd();

  if (!set) {
    PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMSwarm View From Options Success!"));
    PetscFunctionReturn(PETSC_SUCCESS);
  }

  PetscCall(PetscViewerGetType(viewer, &viewer_type));
  PetscCall(PetscViewerFileGetName(viewer, &filename_tmp));
  PetscCall(PetscStrcmp(viewer_type, PETSCVIEWERASCII, &is_ascii));
  if (is_ascii == PETSC_TRUE && filename_tmp != NULL) {
    PetscBool is_xmdf;
    char     *extension;

    PetscCall(PetscStrrchr(filename_tmp, '.', &extension));
    PetscCall(PetscStrcmp(extension, "xmf", &is_xmdf));
    if (is_xmdf) {
      PetscCall(DMSwarmViewXDMF(dm_swarm, filename_tmp));
    } else PetscCall(DMView(dm_swarm, viewer));
    PetscCall(PetscViewerDestroy(&viewer));
  } else if (set) {
    PetscCall(DMView(dm_swarm, viewer));
    PetscCall(PetscViewerDestroy(&viewer));
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMSwarm View From Options Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}
// LCOV_EXCL_STOP

/**
  @brief Get the solution mesh `DMPlex`.
    Caller is responsible for destroying the returned `DMPlex`.

  Collective across MPI processes.

  @param[in]   ratel  `Ratel` context
  @param[out]  dm     `DMPlex` holding solution mesh

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelGetSolutionMeshDM(Ratel ratel, DM *dm) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Get Solution Mesh DM"));

  switch (ratel->method_type) {
    case RATEL_METHOD_FEM: {
      if (!ratel->dm_solution) PetscCall(RatelSolutionDMSetup_FEM(ratel, &ratel->dm_solution));
      *dm = ratel->dm_solution;
      PetscCall(PetscObjectReference((PetscObject)*dm));
    } break;
    case RATEL_METHOD_MPM: {
      if (!ratel->dm_solution) PetscCall(RatelSolutionDMSetup_MPM(ratel, &ratel->dm_solution));
      PetscCall(DMSwarmGetCellDM(ratel->dm_solution, dm));
      PetscCall(PetscObjectReference((PetscObject)*dm));
    } break;
    // LCOV_EXCL_START
    default:
      SETERRQ(ratel->comm, PETSC_ERR_SUP, "Unsupported method type");
      break;
      // LCOV_EXCL_STOP
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Get Solution Mesh DM Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get the energy mesh `DMPlex`.
    Caller is responsible for destroying the returned `DMPlex`.

  Collective across MPI processes.

  @param[in]   ratel  `Ratel` context
  @param[out]  dm     `DMPlex` holding energy mesh

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelGetEnergyDM(Ratel ratel, DM *dm) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Get Energy DM"));

  if (ratel->evaluator_strain_energy) {
    DM dm_energy;

    PetscCall(CeedEvaluatorGetDMs(ratel->evaluator_strain_energy, NULL, &dm_energy));
    PetscCall(PetscObjectReference((PetscObject)dm_energy));
    *dm = dm_energy;
    PetscCall(CeedEvaluatorRestoreDMs(ratel->evaluator_strain_energy, NULL, &dm_energy));
  } else if (ratel->evaluator_external_energy) {
    DM dm_energy;

    PetscCall(CeedEvaluatorGetDMs(ratel->evaluator_external_energy, NULL, &dm_energy));
    PetscCall(PetscObjectReference((PetscObject)dm_energy));
    *dm = dm_energy;
    PetscCall(CeedEvaluatorRestoreDMs(ratel->evaluator_external_energy, NULL, &dm_energy));
  } else {
    switch (ratel->method_type) {
      case RATEL_METHOD_MPM:
      case RATEL_METHOD_FEM: {
        PetscCall(RatelEnergyDMSetup_FEM(ratel, dm));
      } break;
      // LCOV_EXCL_START
      default:
        SETERRQ(ratel->comm, PETSC_ERR_SUP, "Unsupported method type");
        break;
        // LCOV_EXCL_STOP
    }
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Get Energy DM Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Create a label with separate values for the `FE` orientations for all cells with this material for the `DMPlex` face.

  Collective across MPI processes.

  @param[in,out]  ratel             `Ratel` context
  @param[in,out]  dm                `DMPlex` holding mesh
  @param[in]      material          `RatelMaterial` to setup operators for
  @param[in]      dm_face           Face number on `DMPlex`
  @param[out]     num_label_values  Number of label values for newly created label
  @param[out]     face_label_name   Label name for newly created label, or `NULL` if no elements match the `material` and `dm_face`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDMPlexCreateFaceLabel(Ratel ratel, DM dm, RatelMaterial material, PetscInt dm_face, PetscInt *num_label_values,
                                          char **face_label_name) {
  PetscBool       has_material_label;
  PetscInt        num_label_points = 0, face_height = 1, num_label_values_local = 0;
  const PetscInt *label_points;
  PetscInt       *material_label_values;
  DMLabel         face_label = NULL;
  IS              is_label_points;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMPlex Create Face Label"));

  // Set initial values
  {
    const char *material_name;
    PetscSizeT  material_name_len, label_name_len;
    PetscBool   has_name = PETSC_TRUE;

    // Face label
    PetscCall(RatelMaterialGetMaterialName(material, &material_name));
    PetscCall(PetscStrlen(material_name, &material_name_len));
    has_name       = material_name_len > 0;
    label_name_len = material_name_len + (has_name ? 1 : 0) + 26;
    PetscCall(PetscCalloc1(label_name_len, face_label_name));
    PetscCall(PetscSNPrintf(*face_label_name, label_name_len, "%s%sface label for DM face %" PetscInt_FMT, material_name ? material_name : "",
                            has_name ? " " : "", dm_face));
    PetscCall(DMCreateLabel(dm, *face_label_name));
    PetscCall(DMGetLabel(dm, *face_label_name, &face_label));
  }

  PetscCall(DMHasLabel(dm, ratel->material_volume_label_name, &has_material_label));
  PetscCall(RatelMaterialGetVolumeLabelValues(material, NULL, &material_label_values));
  {
    PetscBool has_label;
    PetscCall(DMHasLabel(dm, "Face Sets", &has_label));
    if (!has_label) PetscCall(RatelCreateBCLabel(dm, "Face Sets"));
  }

  PetscCall(DMGetStratumIS(dm, "Face Sets", dm_face, &is_label_points));
  if (is_label_points) {
    PetscCall(ISGetLocalSize(is_label_points, &num_label_points));
    PetscCall(ISGetIndices(is_label_points, &label_points));
  }

  for (PetscInt face_index = 0; face_index < num_label_points; face_index++) {
    const PetscInt *face_support, *cell_cone;
    PetscInt        face_support_size = 0, cell_cone_size = 0, fe_face_on_cell = -1, value = -1;
    PetscInt        face_point;

    {  // Verify label point is a face point
      PetscInt label_point = label_points[face_index], label_point_height = -1;

      PetscCall(DMPlexGetPointHeight(dm, label_point, &label_point_height));
      if (label_point_height != face_height) continue;
      else face_point = label_point;
    }

    // Get cell supporting face
    PetscCall(DMPlexGetSupport(dm, face_point, &face_support));
    PetscCall(DMPlexGetSupportSize(dm, face_point, &face_support_size));
    PetscCheck(face_support_size == 1, ratel->comm, PETSC_ERR_SUP, "Expected one cell in support of exterior face, but got %" PetscInt_FMT " cells",
               face_support_size);
    PetscInt cell_point = face_support[0];

    // Check if current point is on material domain label
    if (has_material_label) PetscCall(DMGetLabelValue(dm, ratel->material_volume_label_name, cell_point, &value));
    if (has_material_label && value != material_label_values[0]) continue;  // TODO: handle multiple label values

    // Find face number
    PetscCall(DMPlexGetCone(dm, cell_point, &cell_cone));
    PetscCall(DMPlexGetConeSize(dm, cell_point, &cell_cone_size));
    for (PetscInt i = 0; i < cell_cone_size; i++) {
      if (cell_cone[i] == face_point) fe_face_on_cell = i;
    }
    PetscCheck(fe_face_on_cell != -1, ratel->comm, PETSC_ERR_SUP, "Could not find face %" PetscInt_FMT " in cone of its support", face_point);

    // Add to label
    num_label_values_local = cell_cone_size;
    PetscCall(DMLabelSetValue(face_label, face_point, fe_face_on_cell));
  }
  PetscCall(DMPlexLabelAddFaceCells(dm, face_label));
  PetscCall(ISDestroy(&is_label_points));
  // Form agreement about total number of face orientations
  {
    PetscInt num_label_values_global = -1;

    PetscCall(MPIU_Allreduce(&num_label_values_local, &num_label_values_global, 1, MPIU_INT, MPI_MAX, ratel->comm));
    *num_label_values = num_label_values_global;
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMPlex Create Face Label Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Setup `DM` with FE space of appropriate degree

  @param[in]   comm        MPI communicator
  @param[in]   dim         Spatial dimension
  @param[in]   num_comp    Number of components
  @param[in]   is_simplex  Flax for simplex or tensor product element
  @param[in]   order       Polynomial order of space
  @param[in]   q_order     Quadrature order
  @param[in]   prefix      The options prefix, or `NULL`
  @param[out]  fem         `PetscFE` object

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode PetscFECreateLagrangeFromOptions(MPI_Comm comm, PetscInt dim, PetscInt num_comp, PetscBool is_simplex, PetscInt order,
                                                PetscInt q_order, const char prefix[], PetscFE *fem) {
  PetscBool       is_tensor     = !is_simplex;
  DMPolytopeType  polytope_type = DMPolytopeTypeSimpleShape(dim, is_simplex);
  PetscSpace      fe_space;
  PetscDualSpace  fe_dual_space;
  PetscQuadrature quadrature, face_quadrature;

  PetscFunctionBeginUser;

  // Create space
  PetscCall(PetscSpaceCreate(comm, &fe_space));
  PetscCall(PetscSpaceSetType(fe_space, PETSCSPACEPOLYNOMIAL));
  PetscCall(PetscObjectSetOptionsPrefix((PetscObject)fe_space, prefix));
  PetscCall(PetscSpacePolynomialSetTensor(fe_space, is_tensor));
  PetscCall(PetscSpaceSetNumComponents(fe_space, num_comp));
  PetscCall(PetscSpaceSetNumVariables(fe_space, dim));
  if (order >= 0) {
    PetscCall(PetscSpaceSetDegree(fe_space, order, PETSC_DETERMINE));
    if (polytope_type == DM_POLYTOPE_TRI_PRISM || polytope_type == DM_POLYTOPE_TRI_PRISM_TENSOR) {
      PetscSpace fe_space_end, fe_space_side;

      PetscCall(PetscSpaceSetNumComponents(fe_space, 1));
      PetscCall(PetscSpaceCreate(comm, &fe_space_end));
      PetscCall(PetscSpaceSetType(fe_space_end, PETSCSPACEPOLYNOMIAL));
      PetscCall(PetscSpacePolynomialSetTensor(fe_space_end, PETSC_FALSE));
      PetscCall(PetscSpaceSetNumComponents(fe_space_end, 1));
      PetscCall(PetscSpaceSetNumVariables(fe_space_end, dim - 1));
      PetscCall(PetscSpaceSetDegree(fe_space_end, order, PETSC_DETERMINE));
      PetscCall(PetscSpaceCreate(comm, &fe_space_side));
      PetscCall(PetscSpaceSetType(fe_space_side, PETSCSPACEPOLYNOMIAL));
      PetscCall(PetscSpacePolynomialSetTensor(fe_space_side, PETSC_FALSE));
      PetscCall(PetscSpaceSetNumComponents(fe_space_side, 1));
      PetscCall(PetscSpaceSetNumVariables(fe_space_side, 1));
      PetscCall(PetscSpaceSetDegree(fe_space_side, order, PETSC_DETERMINE));
      PetscCall(PetscSpaceSetType(fe_space, PETSCSPACETENSOR));
      PetscCall(PetscSpaceTensorSetNumSubspaces(fe_space, 2));
      PetscCall(PetscSpaceTensorSetSubspace(fe_space, 0, fe_space_end));
      PetscCall(PetscSpaceTensorSetSubspace(fe_space, 1, fe_space_side));
      PetscCall(PetscSpaceDestroy(&fe_space_end));
      PetscCall(PetscSpaceDestroy(&fe_space_side));

      if (num_comp > 1) {
        PetscSpace scalar_fe_space = fe_space;

        PetscCall(PetscSpaceCreate(comm, &fe_space));
        PetscCall(PetscSpaceSetNumVariables(fe_space, dim));
        PetscCall(PetscSpaceSetNumComponents(fe_space, num_comp));
        PetscCall(PetscSpaceSetType(fe_space, PETSCSPACESUM));
        PetscCall(PetscSpaceSumSetNumSubspaces(fe_space, num_comp));
        PetscCall(PetscSpaceSumSetConcatenate(fe_space, PETSC_TRUE));
        PetscCall(PetscSpaceSumSetInterleave(fe_space, PETSC_TRUE, PETSC_FALSE));
        for (PetscInt i = 0; i < num_comp; i++) PetscCall(PetscSpaceSumSetSubspace(fe_space, i, scalar_fe_space));
        PetscCall(PetscSpaceDestroy(&scalar_fe_space));
      }
    }
  }
  PetscCall(PetscSpaceSetFromOptions(fe_space));
  PetscCall(PetscSpaceSetUp(fe_space));
  PetscCall(PetscSpaceGetDegree(fe_space, &order, NULL));
  PetscCall(PetscSpacePolynomialGetTensor(fe_space, &is_tensor));
  PetscCall(PetscSpaceGetNumComponents(fe_space, &num_comp));

  // Create dual space
  PetscCall(PetscDualSpaceCreate(comm, &fe_dual_space));
  PetscCall(PetscDualSpaceSetType(fe_dual_space, PETSCDUALSPACELAGRANGE));
  PetscCall(PetscObjectSetOptionsPrefix((PetscObject)fe_dual_space, prefix));
  {
    DM dual_space_dm;

    PetscCall(DMPlexCreateReferenceCell(PETSC_COMM_SELF, polytope_type, &dual_space_dm));
    PetscCall(PetscDualSpaceSetDM(fe_dual_space, dual_space_dm));
    PetscCall(DMDestroy(&dual_space_dm));
  }
  PetscCall(PetscDualSpaceSetNumComponents(fe_dual_space, num_comp));
  PetscCall(PetscDualSpaceSetOrder(fe_dual_space, order));
  PetscCall(PetscDualSpaceLagrangeSetTensor(fe_dual_space, (is_tensor || (polytope_type == DM_POLYTOPE_TRI_PRISM)) ? PETSC_TRUE : PETSC_FALSE));
  PetscCall(PetscDualSpaceSetFromOptions(fe_dual_space));
  PetscCall(PetscDualSpaceSetUp(fe_dual_space));

  // Create quadrature
  q_order = q_order >= 0 ? q_order : order;
  PetscCall(PetscDTCreateDefaultQuadrature(polytope_type, q_order, &quadrature, &face_quadrature));

  // Create finite element
  PetscCall(PetscFECreateFromSpaces(fe_space, fe_dual_space, quadrature, face_quadrature, fem));
  PetscCall(PetscFESetFromOptions(*fem));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Convert `DM` field to `DS` field.

  Not collective across MPI processes.

  @param[in]   ratel         `Ratel` context
  @param[in]   dm            `DM` holding mesh
  @param[in]   domain_label  Label for `DM` domain
  @param[in]   dm_field      Index of `DM` field
  @param[out]  ds_field      Index of `DS` field

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelDMFieldToDSField(Ratel ratel, DM dm, DMLabel domain_label, PetscInt dm_field, PetscInt *ds_field) {
  const PetscInt *fields;
  PetscInt        num_fields;
  PetscDS         ds;
  IS              field_is;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DM Field to DS Field"));

  // Translate dm_field to ds_field
  PetscCall(DMGetRegionDS(dm, domain_label, &field_is, &ds, NULL));
  PetscCall(ISGetIndices(field_is, &fields));
  PetscCall(ISGetSize(field_is, &num_fields));
  for (PetscInt i = 0; i < num_fields; i++) {
    if (dm_field == fields[i]) {
      *ds_field = i;
      break;
    }
  }
  PetscCall(ISRestoreIndices(field_is, &fields));

  PetscCheck(*ds_field != -1, ratel->comm, PETSC_ERR_SUP, "Could not find dm_field %" PetscInt_FMT " in DS", dm_field);

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DM Field to DS Field Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get the permutation and field offset for a given depth.

  Not collective across MPI processes.

  @param[in]   ratel         `Ratel` context
  @param[in]   dm            `DMPlex` holding mesh
  @param[in]   depth         Depth of `DMPlex` topology
  @param[in]   field         Index of `DMPlex` field
  @param[out]  permutation   Permutation for `DMPlex` field
  @param[out]  field_offset  Offset to `DMPlex` field

  @return An error code: 0 - success, otherwise - failure
**/
static inline PetscErrorCode RatelGetClosurePermutationAndFieldOffsetAtDepth(Ratel ratel, DM dm, PetscInt depth, PetscInt field, IS *permutation,
                                                                             PetscInt *field_offset) {
  PetscBool    is_field_continuous = PETSC_TRUE;
  PetscInt     dim, num_fields, offset = 0, size = 0;
  PetscSection section;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Get Closure Permutation And Field Offset At Depth"));

  *field_offset = 0;

  PetscCall(DMGetDimension(dm, &dim));
  PetscCall(DMGetLocalSection(dm, &section));
  PetscCall(PetscSectionGetNumFields(section, &num_fields));
  // ---- Permutation size and offset to current field
  for (PetscInt f = 0; f < num_fields; f++) {
    PetscBool      is_continuous;
    PetscInt       num_components, num_dof_1d, dual_space_size, field_size;
    PetscObject    obj;
    PetscFE        fe = NULL;
    PetscDualSpace dual_space;

    PetscCall(PetscSectionGetFieldComponents(section, f, &num_components));
    PetscCall(DMGetField(dm, f, NULL, &obj));
    fe = (PetscFE)obj;
    PetscCall(PetscFEGetDualSpace(fe, &dual_space));
    PetscCall(PetscDualSpaceLagrangeGetContinuity(dual_space, &is_continuous));
    if (f == field) is_field_continuous = is_continuous;
    if (!is_continuous) continue;
    PetscCall(PetscDualSpaceGetDimension(dual_space, &dual_space_size));
    num_dof_1d = (PetscInt)PetscCeilReal(PetscPowReal(dual_space_size / num_components, 1.0 / dim));
    field_size = PetscPowInt(num_dof_1d, depth) * num_components;
    if (f < field) offset += field_size;
    size += field_size;
  }

  if (is_field_continuous) {
    *field_offset = offset;
    PetscCall(PetscSectionGetClosurePermutation(section, (PetscObject)dm, depth, size, permutation));
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Get Closure Permutation And Field Offset At Depth Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get quadrature data from `PetscQuadrature` for use with libCEED.

  Not collective across MPI processes.

  @note `q_weights` and `q_points` are allocated using `PetscCalloc1` and must be freed by the user.

  @param[in]   ratel       `Ratel` context
  @param[in]   fe          `PetscFE` object
  @param[in]   quadrature  PETSc basis quadrature
  @param[out]  q_dim       Quadrature dimension, or NULL if not needed
  @param[out]  num_comp    Number of components, or NULL if not needed
  @param[out]  Q           Number of quadrature points, or NULL if not needed
  @param[out]  q_points    Quadrature points in libCEED orientation, or NULL if not needed
  @param[out]  q_weights   Quadrature weights in libCEED orientation, or NULL if not needed

  @return An error code: 0 - success, otherwise - failure
**/
static inline PetscErrorCode RatelGetQuadratureDataP2C(Ratel ratel, PetscFE fe, PetscQuadrature quadrature, PetscInt *q_dim, PetscInt *num_comp,
                                                       PetscInt *Q, CeedScalar **q_points, CeedScalar **q_weights) {
  PetscInt         spatial_dim, dim, num_comp_quadrature, num_q_points;
  const PetscReal *q_points_petsc, *q_weights_petsc;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Get Quadrature Data P2C"));

  PetscCall(PetscFEGetSpatialDimension(fe, &spatial_dim));
  PetscCall(PetscQuadratureGetData(quadrature, &dim, &num_comp_quadrature, &num_q_points, &q_points_petsc, &q_weights_petsc));
  if (q_dim) *q_dim = dim;
  if (num_comp) *num_comp = num_comp_quadrature;
  if (Q) *Q = num_q_points;
  if (q_weights) {
    PetscSizeT q_weights_size = num_q_points;

    PetscCall(PetscCalloc1(q_weights_size, q_weights));
    for (CeedInt i = 0; i < num_q_points; i++) (*q_weights)[i] = q_weights_petsc[i];
  }
  if (q_points) {
    PetscSizeT q_points_size = num_q_points * spatial_dim;

    PetscCall(PetscCalloc1(q_points_size, q_points));
    for (CeedInt i = 0; i < num_q_points; i++) {
      for (CeedInt d = 0; d < dim; d++) (*q_points)[i * spatial_dim + d] = q_points_petsc[i * dim + d];
    }
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Get Quadrature Data P2C Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Create 1D tabulation from `PetscFE`.

  Collective across MPI processes.

  @note `q_weights` and `q_points` are allocated using `PetscCalloc1` and must be freed by the user.

  @param[in]   ratel         `Ratel` context
  @param[in]   fe            PETSc basis `FE` object
  @param[out]  tabulation    PETSc basis tabulation
  @param[out]  q_points_1d   Quadrature points in libCEED orientation
  @param[out]  q_weights_1d  Quadrature weights in libCEED orientation

  @return An error code: 0 - success, otherwise - failure
**/
static inline PetscErrorCode RatelCreate1DTabulation_Tensor(Ratel ratel, PetscFE fe, PetscTabulation *tabulation, PetscReal **q_points_1d,
                                                            CeedScalar **q_weights_1d) {
  PetscFE         fe_1d;
  PetscInt        dim, num_comp, Q = -1, q_dim = -1, num_derivatives = 1;
  PetscQuadrature quadrature;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Create 1D Tabulation Tensor"));

  // Create 1D FE
  PetscCall(PetscFEGetNumComponents(fe, &num_comp));
  PetscCall(PetscFEGetSpatialDimension(fe, &dim));
  {
    const char     *prefix;
    PetscBool       is_tensor;
    PetscInt        num_comp, order, q_order;
    PetscDualSpace  dual_space;
    PetscQuadrature quadrature;

    PetscCall(PetscObjectGetOptionsPrefix((PetscObject)fe, &prefix));
    PetscCall(PetscFEGetDualSpace(fe, &dual_space));
    PetscCall(PetscFEGetNumComponents(fe, &num_comp));
    PetscCall(PetscDualSpaceLagrangeGetTensor(dual_space, &is_tensor));
    PetscCall(PetscDualSpaceGetOrder(dual_space, &order));
    PetscCall(PetscFEGetQuadrature(fe, &quadrature));
    PetscCall(PetscQuadratureGetData(quadrature, NULL, NULL, &q_order, NULL, NULL));
    PetscCall(PetscFECreateLagrangeFromOptions(ratel->comm, 1, num_comp, !is_tensor, order,
                                               (PetscInt)PetscCeilReal(PetscPowReal(1.0 * q_order, 1.0 / dim)) - 1, prefix, &fe_1d));
  }

  // Get quadrature data
  PetscCall(PetscFEGetQuadrature(fe_1d, &quadrature));
  PetscCall(RatelGetQuadratureDataP2C(ratel, fe_1d, quadrature, &q_dim, NULL, &Q, q_points_1d, q_weights_1d));

  // Compute 1D tabulation
  PetscCall(PetscFECreateTabulation(fe_1d, 1, Q, *q_points_1d, num_derivatives, tabulation));

  // Cleanup
  PetscCall(PetscFEDestroy(&fe_1d));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Create 1D Tabulation Tensor Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Compute field tabulation from `PetscTabulation`.

  Not collective across MPI processes.

  @note `interp` and `grad` are allocated using `PetscCalloc1` and must be freed by the user.

  @param[in]   ratel       `Ratel` context
  @param[in]   dm          `DMPlex` holding mesh
  @param[in]   field       Index of `DMPlex` field
  @param[in]   face        Index of basis face, or 0
  @param[in]   tabulation  PETSc basis tabulation
  @param[out]  interp      Interpolation matrix in libCEED orientation
  @param[out]  grad        Gradient matrix in libCEED orientation

  @return An error code: 0 - success, otherwise - failure
**/
static inline PetscErrorCode RatelComputeFieldTabulationP2C(Ratel ratel, DM dm, PetscInt field, PetscInt face, PetscTabulation tabulation,
                                                            CeedScalar **interp, CeedScalar **grad) {
  PetscBool       is_simplex, has_permutation = PETSC_FALSE;
  PetscInt        field_offset = 0, num_comp, P, Q, dim;
  const PetscInt *permutation_indices;
  IS              permutation = NULL;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Compute Field Tabulation Petsc To Ceed"));

  num_comp = tabulation->Nc;
  P        = tabulation->Nb / num_comp;
  Q        = tabulation->Np;
  dim      = tabulation->cdim;

  PetscCall(PetscCalloc1(P * Q, interp));
  PetscCall(PetscCalloc1(P * Q * dim, grad));

  PetscCall(DMPlexIsSimplex(dm, &is_simplex));
  if (!is_simplex) {
    PetscCall(RatelGetClosurePermutationAndFieldOffsetAtDepth(ratel, dm, dim, field, &permutation, &field_offset));
    has_permutation = !!permutation;
    if (has_permutation) PetscCall(ISGetIndices(permutation, &permutation_indices));
  }

  const CeedInt c = 0;
  for (CeedInt q = 0; q < Q; q++) {
    for (CeedInt p_ceed = 0; p_ceed < P; p_ceed++) {
      CeedInt p_petsc           = has_permutation ? (permutation_indices[field_offset + p_ceed * num_comp] - field_offset) : (p_ceed * num_comp);
      (*interp)[q * P + p_ceed] = tabulation->T[0][((face * Q + q) * P * num_comp + p_petsc) * num_comp + c];
      for (CeedInt d = 0; d < dim; d++) {
        (*grad)[(d * Q + q) * P + p_ceed] = tabulation->T[1][(((face * Q + q) * P * num_comp + p_petsc) * num_comp + c) * dim + d];
      }
    }
  }

  // -- Cleanup
  if (has_permutation) PetscCall(ISRestoreIndices(permutation, &permutation_indices));
  PetscCall(ISDestroy(&permutation));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Compute Field Tabulation Petsc To Ceed Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get global `DMPlex` topology type.

  Collective across MPI processes.

  @param[in]   ratel         `Ratel` context
  @param[in]   dm            `DMPlex` holding mesh
  @param[in]   domain_label  `DMLabel` for `DMPlex` domain
  @param[in]   label_value   Stratum value
  @param[in]   height        Height of `DMPlex` topology
  @param[out]  cell_type     `DMPlex` topology type

  @return An error code: 0 - success, otherwise - failure
**/
static inline PetscErrorCode RatelGetGlobalDMPolytopeType(Ratel ratel, DM dm, DMLabel domain_label, PetscInt label_value, PetscInt height,
                                                          DMPolytopeType *cell_type) {
  PetscInt first_point;
  PetscInt ids[1] = {label_value};
  DMLabel  depth_label;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Get Global DMPolytope Type"));

  // Use depth label if no domain label present
  if (!domain_label) {
    PetscInt depth;

    PetscCall(DMPlexGetDepth(dm, &depth));
    PetscCall(DMPlexGetDepthLabel(dm, &depth_label));
    ids[0] = depth - height;
  }

  // Get cell interp, grad, and quadrature data
  PetscCall(DMGetFirstLabeledPoint(dm, dm, domain_label ? domain_label : depth_label, 1, ids, height, &first_point, NULL));
  if (first_point != -1) PetscCall(DMPlexGetCellType(dm, first_point, cell_type));
  // Form agreement about CellType
  {
    PetscInt cell_type_local = -1, cell_type_global = -1;

    if (first_point != -1) cell_type_local = (PetscInt)*cell_type;
    PetscCall(MPIU_Allreduce(&cell_type_local, &cell_type_global, 1, MPIU_INT, MPI_MAX, ratel->comm));
    *cell_type = (DMPolytopeType)cell_type_global;
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Get Global DMPolytope Type Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Create `CeedBasis` from `DMPlex`.

  Collective across MPI processes.

  @param[in]   ratel         `Ratel` context
  @param[in]   dm            `DMPlex` holding mesh
  @param[in]   domain_label  `DMLabel` for `DMPlex` domain
  @param[in]   label_value   Stratum value
  @param[in]   height        Height of `DMPlex` topology
  @param[in]   dm_field      Index of `DMPlex` field
  @param[out]  basis         `CeedBasis` for `DMPlex`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDMPlexCeedBasisCreate(Ratel ratel, DM dm, DMLabel domain_label, PetscInt label_value, PetscInt height, PetscInt dm_field,
                                          CeedBasis *basis) {
  char           container_name[1024];
  PetscContainer container = NULL;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMPlex CeedBasis Create"));

  {
    PetscInt    mesh_state = 0;
    const char *label_name = NULL;

    PetscCall(RatelGetMeshRemapState(ratel, &mesh_state));

    if (domain_label) PetscCall(PetscObjectGetName((PetscObject)domain_label, &label_name));
    PetscCall(PetscSNPrintf(container_name, sizeof(container_name),
                            "DM CeedBasis - label: %s label value: %" PetscInt_FMT " height: %" PetscInt_FMT " DM field: %" PetscInt_FMT
                            " State: %" PetscInt_FMT,
                            label_name ? label_name : "NONE", label_value, height, dm_field, mesh_state));
  }
  PetscCall(PetscObjectQuery((PetscObject)dm, container_name, (PetscObject *)&container));

  if (container) {
    CeedBasis container_basis;

    // Retrieve existing basis
    PetscCall(PetscContainerGetPointer(container, (void **)&container_basis));
    *basis = NULL;
    RatelCallCeed(ratel, CeedBasisReferenceCopy(container_basis, basis));
  } else {
    PetscDS         ds;
    PetscFE         fe;
    PetscDualSpace  dual_space;
    PetscQuadrature quadrature;
    PetscBool       is_tensor = PETSC_TRUE;
    PetscInt        ds_field  = -1;

    // Get element information
    PetscCall(DMGetRegionDS(dm, domain_label, NULL, &ds, NULL));
    PetscCall(RatelDMFieldToDSField(ratel, dm, domain_label, dm_field, &ds_field));
    PetscCall(PetscDSGetDiscretization(ds, ds_field, (PetscObject *)&fe));
    PetscCall(PetscFEGetHeightSubspace(fe, height, &fe));
    PetscCall(PetscFEGetQuadrature(fe, &quadrature));
    PetscCall(PetscFEGetDualSpace(fe, &dual_space));
    PetscCall(PetscDualSpaceLagrangeGetTensor(dual_space, &is_tensor));

    // Build libCEED basis
    if (!is_tensor) {
      PetscTabulation  basis_tabulation;
      PetscInt         num_derivatives = 1, face = 0;
      CeedScalar      *q_points, *q_weights, *interp, *grad;
      CeedElemTopology elem_topo;

      {
        DMPolytopeType cell_type;

        PetscCall(RatelGetGlobalDMPolytopeType(ratel, dm, domain_label, label_value, height, &cell_type));
        elem_topo = PolytopeTypePetscToCeed(cell_type);
        PetscCheck(elem_topo, ratel->comm, PETSC_ERR_SUP, "DMPlex topology not supported: %s", DMPolytopeTypes[cell_type]);
      }

      // Compute basis tabulation
      PetscCall(RatelGetQuadratureDataP2C(ratel, fe, quadrature, NULL, NULL, NULL, &q_points, &q_weights));
      PetscCall(PetscFEGetCellTabulation(fe, num_derivatives, &basis_tabulation));
      PetscCall(RatelComputeFieldTabulationP2C(ratel, dm, dm_field, face, basis_tabulation, &interp, &grad));
      {
        PetscInt num_comp = basis_tabulation->Nc, P = basis_tabulation->Nb / num_comp, Q = basis_tabulation->Np;

        RatelCallCeed(ratel, CeedBasisCreateH1(ratel->ceed, elem_topo, num_comp, P, Q, interp, grad, q_points, q_weights, basis));
      }

      PetscCall(PetscFree(q_points));
      PetscCall(PetscFree(q_weights));
      PetscCall(PetscFree(interp));
      PetscCall(PetscFree(grad));
    } else {
      PetscInt        P_1d, Q_1d, num_comp, dim;
      PetscTabulation basis_tabulation;
      CeedScalar     *q_points_1d, *q_weights_1d, *interp_1d, *grad_1d;

      PetscCall(PetscFEGetSpatialDimension(fe, &dim));
      PetscCall(RatelCreate1DTabulation_Tensor(ratel, fe, &basis_tabulation, &q_points_1d, &q_weights_1d));
      num_comp = basis_tabulation->Nc;
      P_1d     = basis_tabulation->Nb / num_comp;
      Q_1d     = basis_tabulation->Np;
      PetscCall(RatelComputeFieldTabulationP2C(ratel, dm, dm_field, 0, basis_tabulation, &interp_1d, &grad_1d));
      RatelCallCeed(ratel, CeedBasisCreateTensorH1(ratel->ceed, dim, num_comp, P_1d, Q_1d, interp_1d, grad_1d, q_points_1d, q_weights_1d, basis));

      // Cleanup
      PetscCall(PetscFree(q_points_1d));
      PetscCall(PetscFree(q_weights_1d));
      PetscCall(PetscFree(interp_1d));
      PetscCall(PetscFree(grad_1d));
      PetscCall(PetscTabulationDestroy(&basis_tabulation));
    }
    // Set in container
    {
      CeedBasis container_basis = NULL;

      PetscCall(PetscContainerCreate(PetscObjectComm((PetscObject)dm), &container));
      RatelCallCeed(ratel, CeedBasisReferenceCopy(*basis, &container_basis));
      PetscCall(PetscContainerSetPointer(container, container_basis));
      PetscCall(PetscContainerSetCtxDestroy(container, RatelDMPlexCeedBasisDestroy));
      PetscCall(PetscObjectCompose((PetscObject)dm, container_name, (PetscObject)container));
      PetscCall(PetscContainerDestroy(&container));
    }
  }
  if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedBasisView(*basis, stdout));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMPlex CeedBasis Create Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Create `CeedBasis` for coordinate space of `DMPlex`.

  Collective across MPI processes.

  @param[in]   ratel         `Ratel` context
  @param[in]   dm            `DMPlex` holding mesh
  @param[in]   domain_label  `DMLabel` for `DMPlex` domain
  @param[in]   label_value   Stratum value
  @param[in]   height        Height of `DMPlex` topology
  @param[out]  basis         `CeedBasis` for coordinate space of `DMPlex`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDMPlexCeedBasisCoordinateCreate(Ratel ratel, DM dm, DMLabel domain_label, PetscInt label_value, PetscInt height,
                                                    CeedBasis *basis) {
  DM dm_coord = NULL;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMPlex CeedBasis Coordinate Create"));

  PetscCall(DMGetCellCoordinateDM(dm, &dm_coord));
  if (!dm_coord) PetscCall(DMGetCoordinateDM(dm, &dm_coord));
  PetscCall(RatelDMPlexCeedBasisCreate(ratel, dm_coord, domain_label, label_value, height, 0, basis));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMPlex CeedBasis Coordinate Create Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Create `CeedBasis` for cell to face quadrature space evaluation from `DMPlex`.

  Collective across MPI processes.

  @param[in]   ratel         `Ratel` context
  @param[in]   dm            `DMPlex` holding mesh
  @param[in]   domain_label  `DMLabel` for `DMPlex` domain
  @param[in]   label_value   Stratum value
  @param[in]   face          Index of face
  @param[in]   dm_field      Index of `DMPlex` field
  @param[out]  basis         `CeedBasis` for cell to face evaluation

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDMPlexCeedBasisCellToFaceCreate(Ratel ratel, DM dm, DMLabel domain_label, PetscInt label_value, PetscInt face, PetscInt dm_field,
                                                    CeedBasis *basis) {
  char           container_name[1024];
  PetscContainer container = NULL;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMPlex CeedBasis Cell To Face Create"));

  {
    PetscInt    mesh_state = 0;
    const char *label_name = NULL;

    PetscCall(RatelGetMeshRemapState(ratel, &mesh_state));

    if (domain_label) PetscCall(PetscObjectGetName((PetscObject)domain_label, &label_name));
    PetscCall(PetscSNPrintf(container_name, sizeof(container_name),
                            "DM CeedBasis CellToFace - label: %s label value: %" PetscInt_FMT " face: %" PetscInt_FMT " DM field: %" PetscInt_FMT
                            " State: %" PetscInt_FMT,
                            label_name ? label_name : "NONE", label_value, face, dm_field, mesh_state));
  }
  PetscCall(PetscObjectQuery((PetscObject)dm, container_name, (PetscObject *)&container));

  if (container) {
    CeedBasis container_basis;

    // Retrieve existing basis
    PetscCall(PetscContainerGetPointer(container, (void **)&container_basis));
    *basis = NULL;
    RatelCallCeed(ratel, CeedBasisReferenceCopy(container_basis, basis));
  } else {
    PetscInt        ds_field = -1, height = 0;
    PetscDS         ds;
    PetscFE         fe;
    PetscQuadrature face_quadrature;

    // Get element information
    PetscCall(DMGetRegionDS(dm, domain_label, NULL, &ds, NULL));
    PetscCall(RatelDMFieldToDSField(ratel, dm, domain_label, dm_field, &ds_field));
    PetscCall(PetscDSGetDiscretization(ds, ds_field, (PetscObject *)&fe));
    PetscCall(PetscFEGetFaceQuadrature(fe, &face_quadrature));

    // Build libCEED basis
    {
      PetscInt         num_derivatives = 1, num_comp, P, Q = -1;
      PetscTabulation  basis_tabulation;
      CeedScalar      *q_points, *q_weights, *interp, *grad;
      CeedElemTopology elem_topo;

      {
        DMPolytopeType cell_type;

        PetscCall(RatelGetGlobalDMPolytopeType(ratel, dm, domain_label, label_value, height, &cell_type));
        elem_topo = PolytopeTypePetscToCeed(cell_type);
        PetscCheck(elem_topo, ratel->comm, PETSC_ERR_SUP, "DMPlex topology not supported: %s", DMPolytopeTypes[cell_type]);
      }

      // Compute basis tabulation
      PetscCall(RatelGetQuadratureDataP2C(ratel, fe, face_quadrature, NULL, NULL, &Q, &q_points, &q_weights));
      PetscCall(PetscFEGetFaceTabulation(fe, num_derivatives, &basis_tabulation));
      num_comp = basis_tabulation->Nc;
      P        = basis_tabulation->Nb / num_comp;
      PetscCall(RatelComputeFieldTabulationP2C(ratel, dm, dm_field, face, basis_tabulation, &interp, &grad));
      RatelCallCeed(ratel, CeedBasisCreateH1(ratel->ceed, elem_topo, num_comp, P, Q, interp, grad, q_points, q_weights, basis));

      PetscCall(PetscFree(q_points));
      PetscCall(PetscFree(q_weights));
      PetscCall(PetscFree(interp));
      PetscCall(PetscFree(grad));
    }
    // Set in container
    {
      CeedBasis container_basis = NULL;

      PetscCall(PetscContainerCreate(PetscObjectComm((PetscObject)dm), &container));
      RatelCallCeed(ratel, CeedBasisReferenceCopy(*basis, &container_basis));
      PetscCall(PetscContainerSetPointer(container, container_basis));
      PetscCall(PetscContainerSetCtxDestroy(container, RatelDMPlexCeedBasisDestroy));
      PetscCall(PetscObjectCompose((PetscObject)dm, container_name, (PetscObject)container));
      PetscCall(PetscContainerDestroy(&container));
    }
  }
  if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedBasisView(*basis, stdout));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMPlex CeedBasis Cell To Face Create Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Create `CeedBasis` for cell to face quadrature space evaluation on coordinate space of `DMPlex`.

  Collective across MPI processes.

  @param[in]   ratel         `Ratel` context
  @param[in]   dm            `DMPlex` holding mesh
  @param[in]   domain_label  `DMLabel` for `DMPlex` domain
  @param[in]   label_value   Stratum value
  @param[in]   face          Index of face
  @param[out]  basis         `CeedBasis` for cell to face evaluation on coordinate space of `DMPlex`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDMPlexCeedBasisCellToFaceCoordinateCreate(Ratel ratel, DM dm, DMLabel domain_label, PetscInt label_value, PetscInt face,
                                                              CeedBasis *basis) {
  DM dm_coord;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMPlex CeedBasis Cell To Face Coordinate Create"));

  PetscCall(DMGetCoordinateDM(dm, &dm_coord));
  PetscCall(RatelDMPlexCeedBasisCellToFaceCreate(ratel, dm_coord, domain_label, label_value, face, 0, basis));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMPlex CeedBasis Cell To Face Coordinate Create Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Create `CeedElemRestriction` from `DMPlex`.

  Not collective across MPI processes.

  @param[in]   ratel         `Ratel` context
  @param[in]   dm            `DMPlex` holding mesh
  @param[in]   domain_label  `DMLabel` for `DMPlex` domain
  @param[in]   label_value   Stratum value
  @param[in]   height        Height of `DMPlex` topology
  @param[in]   dm_field      Index of `DMPlex` field
  @param[out]  restriction   `CeedElemRestriction` for `DMPlex`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDMPlexCeedElemRestrictionCreate(Ratel ratel, DM dm, DMLabel domain_label, PetscInt label_value, PetscInt height,
                                                    PetscInt dm_field, CeedElemRestriction *restriction) {
  char           container_name[1024];
  PetscContainer container = NULL;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMPlex CeedElemRestriction Create"));

  {
    PetscInt    mesh_state = 0;
    const char *label_name = NULL;

    PetscCall(RatelGetMeshRemapState(ratel, &mesh_state));

    if (domain_label) PetscCall(PetscObjectGetName((PetscObject)domain_label, &label_name));
    PetscCall(PetscSNPrintf(container_name, sizeof(container_name),
                            "DM CeedElemRestriction - label: %s label value: %" PetscInt_FMT " height: %" PetscInt_FMT " DM field: %" PetscInt_FMT
                            " State: %" PetscInt_FMT,
                            label_name ? label_name : "NONE", label_value, height, dm_field, mesh_state));
  }
  PetscCall(PetscObjectQuery((PetscObject)dm, container_name, (PetscObject *)&container));

  if (container) {
    CeedElemRestriction container_restriction;

    // Retrieve existing restriction
    PetscCall(PetscContainerGetPointer(container, (void **)&container_restriction));
    *restriction = NULL;
    RatelCallCeed(ratel, CeedElemRestrictionReferenceCopy(container_restriction, restriction));
  } else {
    PetscInt num_elem, elem_size, num_dof, num_comp, *restriction_offsets_petsc;
    CeedInt *restriction_offsets_ceed = NULL;

    // Build restriction
    PetscCall(DMPlexGetLocalOffsets(dm, domain_label, label_value, height, dm_field, &num_elem, &elem_size, &num_comp, &num_dof,
                                    &restriction_offsets_petsc));
    PetscCall(IntArrayPetscToCeed(num_elem * elem_size, &restriction_offsets_petsc, &restriction_offsets_ceed));
    RatelCallCeed(ratel, CeedElemRestrictionCreate(ratel->ceed, num_elem, elem_size, num_comp, 1, num_dof, CEED_MEM_HOST, CEED_COPY_VALUES,
                                                   restriction_offsets_ceed, restriction));
    PetscCall(PetscFree(restriction_offsets_ceed));

    // Set in container
    {
      CeedElemRestriction container_restriction = NULL;

      PetscCall(PetscContainerCreate(PetscObjectComm((PetscObject)dm), &container));
      RatelCallCeed(ratel, CeedElemRestrictionReferenceCopy(*restriction, &container_restriction));
      PetscCall(PetscContainerSetPointer(container, container_restriction));
      PetscCall(PetscContainerSetCtxDestroy(container, RatelDMPlexCeedElemRestrictionDestroy));
      PetscCall(PetscObjectCompose((PetscObject)dm, container_name, (PetscObject)container));
      PetscCall(PetscContainerDestroy(&container));
    }
  }
  if (ratel->is_rank_0_debug) RatelCallCeed(ratel, CeedElemRestrictionView(*restriction, stdout));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMPlex CeedElemRestriction Create Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Create `CeedElemRestriction` from `DMPlex` domain for mesh coordinates.

  Not collective across MPI processes.

  @param[in]   ratel         `Ratel` context
  @param[in]   dm            `DMPlex` holding mesh
  @param[in]   domain_label  Label for `DMPlex` domain
  @param[in]   label_value   Stratum value
  @param[in]   height        Height of `DMPlex` topology
  @param[out]  restriction   `CeedElemRestriction` for mesh

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDMPlexCeedElemRestrictionCoordinateCreate(Ratel ratel, DM dm, DMLabel domain_label, PetscInt label_value, PetscInt height,
                                                              CeedElemRestriction *restriction) {
  DM dm_coord;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMPlex CeedElemRestriction Coordinate Create"));

  PetscCall(DMGetCoordinateDM(dm, &dm_coord));
  PetscCall(RatelDMPlexCeedElemRestrictionCreate(ratel, dm_coord, domain_label, label_value, height, 0, restriction));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMPlex CeedElemRestriction Coordinate Create Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Create `CeedElemRestriction` from `DMPlex` domain for auxiliary `QFunction` data.

  Not collective across MPI processes.

  @param[in]   ratel          `Ratel` context
  @param[in]   dm             `DMPlex` holding mesh
  @param[in]   domain_label   Label for `DMPlex` domain
  @param[in]   label_value    Stratum value
  @param[in]   height         Height of `DMPlex` topology
  @param[in]   dm_field       Index of `DMPlex` field
  @param[in]   q_data_size    Number of components for `QFunction` data
  @param[in]   is_collocated  Boolean flag indicating if the data is collocated on the nodes (`PETSC_TRUE`) on on quadrature points (`PETSC_FALSE`)
  @param[out]  restriction    Strided `CeedElemRestriction` for `QFunction` data

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelDMPlexCeedElemRestrictionStridedCreate(Ratel ratel, DM dm, DMLabel domain_label, PetscInt label_value, PetscInt height,
                                                                  PetscInt dm_field, PetscInt q_data_size, PetscBool is_collocated,
                                                                  CeedElemRestriction *restriction) {
  PetscInt num_elem, num_qpts;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMPlex CeedElemRestriction Strided Create"));

  // Get number of elements
  {
    PetscInt depth;
    DMLabel  depth_label;
    IS       point_is, depth_is;

    PetscCall(DMPlexGetDepth(dm, &depth));
    PetscCall(DMPlexGetDepthLabel(dm, &depth_label));
    PetscCall(DMLabelGetStratumIS(depth_label, depth - height, &depth_is));
    if (domain_label) {
      IS domain_is;

      PetscCall(DMLabelGetStratumIS(domain_label, label_value, &domain_is));
      if (domain_is) {
        PetscCall(ISIntersect(depth_is, domain_is, &point_is));
        PetscCall(ISDestroy(&domain_is));
      } else {
        point_is = NULL;
      }
      PetscCall(ISDestroy(&depth_is));
    } else {
      point_is = depth_is;
    }
    if (point_is) {
      PetscCall(ISGetLocalSize(point_is, &num_elem));
    } else {
      num_elem = 0;
    }
    PetscCall(ISDestroy(&point_is));
  }

  // Get number of quadrature points
  {
    PetscDS  ds;
    PetscFE  fe;
    PetscInt ds_field = -1;

    PetscCall(DMGetRegionDS(dm, domain_label, NULL, &ds, NULL));
    PetscCall(RatelDMFieldToDSField(ratel, dm, domain_label, dm_field, &ds_field));
    PetscCall(PetscDSGetDiscretization(ds, ds_field, (PetscObject *)&fe));
    PetscCall(PetscFEGetHeightSubspace(fe, height, &fe));
    if (is_collocated) {
      PetscDualSpace dual_space;
      PetscInt       num_dual_basis_vectors, dim, num_comp;

      PetscCall(PetscFEGetSpatialDimension(fe, &dim));
      PetscCall(PetscFEGetNumComponents(fe, &num_comp));
      PetscCall(PetscFEGetDualSpace(fe, &dual_space));
      PetscCall(PetscDualSpaceGetDimension(dual_space, &num_dual_basis_vectors));
      num_qpts = num_dual_basis_vectors / num_comp;
    } else {
      PetscQuadrature quadrature;

      PetscCall(DMGetRegionDS(dm, domain_label, NULL, &ds, NULL));
      PetscCall(RatelDMFieldToDSField(ratel, dm, domain_label, dm_field, &ds_field));
      PetscCall(PetscDSGetDiscretization(ds, ds_field, (PetscObject *)&fe));
      PetscCall(PetscFEGetHeightSubspace(fe, height, &fe));
      PetscCall(PetscFEGetQuadrature(fe, &quadrature));
      PetscCall(PetscQuadratureGetData(quadrature, NULL, NULL, &num_qpts, NULL, NULL));
    }
  }

  // Create the restriction
  RatelCallCeed(ratel, CeedElemRestrictionCreateStrided(ratel->ceed, num_elem, num_qpts, q_data_size, num_elem * num_qpts * q_data_size,
                                                        CEED_STRIDES_BACKEND, restriction));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMPlex CeedElemRestriction Strided Create Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Create `CeedElemRestriction` from `DMPlex` domain for auxiliary `QFunction` data.

  Not collective across MPI processes.

  @param[in]   ratel          `Ratel` context
  @param[in]   dm             `DMPlex` holding mesh
  @param[in]   domain_label   Label for `DMPlex` domain
  @param[in]   label_value    Stratum value
  @param[in]   height         Height of `DMPlex` topology
  @param[in]   q_data_size    Number of components for `QFunction` data
  @param[out]  restriction    Strided `CeedElemRestriction` for `QFunction` data

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDMPlexCeedElemRestrictionQDataCreate(Ratel ratel, DM dm, DMLabel domain_label, PetscInt label_value, PetscInt height,
                                                         PetscInt q_data_size, CeedElemRestriction *restriction) {
  PetscInt dm_field = 0;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMPlex CeedElemRestriction QData Create"));

  PetscCall(
      RatelDMPlexCeedElemRestrictionStridedCreate(ratel, dm, domain_label, label_value, height, dm_field, q_data_size, PETSC_FALSE, restriction));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMPlex CeedElemRestriction QData Create Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Create `CeedElemRestriction` from `DMPlex` domain for nodally collocated auxiliary `QFunction` data.

  Not collective across MPI processes.

  @param[in]   ratel          `Ratel` context
  @param[in]   dm             `DMPlex` holding mesh
  @param[in]   domain_label   Label for `DMPlex` domain
  @param[in]   label_value    Stratum value
  @param[in]   height         Height of `DMPlex` topology
  @param[in]   dm_field       Index of `DMPlex` field
  @param[in]   q_data_size    Number of components for `QFunction` data
  @param[out]  restriction    Strided `CeedElemRestriction` for `QFunction` data

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDMPlexCeedElemRestrictionCollocatedCreate(Ratel ratel, DM dm, DMLabel domain_label, PetscInt label_value, PetscInt height,
                                                              PetscInt dm_field, PetscInt q_data_size, CeedElemRestriction *restriction) {
  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMPlex CeedElemRestriction QData Create"));

  PetscCall(
      RatelDMPlexCeedElemRestrictionStridedCreate(ratel, dm, domain_label, label_value, height, dm_field, q_data_size, PETSC_TRUE, restriction));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DMPlex CeedElemRestriction QData Create Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Create index set for each local field DoF in `DM`, including constrained and ghost nodes.

  Collective across MPI processes.

  @param[in]   ratel       `Ratel` context
  @param[in]   dm          `DM` holding mesh
  @param[in]   field       Field index
  @param[out]  block_size  Number of components of requested field
  @param[out]  is          Output `IS` with one index for each DoF of field

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDMGetFieldISLocal(Ratel ratel, DM dm, PetscInt field, PetscInt *block_size, IS *is) {
  PetscInt    *sub_indices;
  PetscInt     point_start, point_end, sub_size = 0, sub_offset = 0;
  PetscSection local_section, field_section;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Get Field IS Local"));

  PetscCall(DMGetLocalSection(dm, &local_section));
  PetscCall(PetscSectionGetFieldComponents(local_section, field, block_size));
  PetscCall(PetscSectionGetField(local_section, field, &field_section));
  PetscCall(PetscSectionGetStorageSize(field_section, &sub_size));
  PetscCall(PetscSectionGetChart(field_section, &point_start, &point_end));
  PetscCall(PetscMalloc1(sub_size, &sub_indices));

  for (PetscInt p = point_start; p < point_end; ++p) {
    PetscInt field_dof, point_offset = 0;

    PetscCall(PetscSectionGetOffset(field_section, p, &point_offset));
    PetscCall(PetscSectionGetDof(field_section, p, &field_dof));
    for (PetscInt i = 0; i < field_dof; ++i, ++sub_offset) sub_indices[sub_offset] = point_offset + i;
  }
  PetscCall(ISCreateGeneral(PETSC_COMM_SELF, sub_size, sub_indices, PETSC_OWN_POINTER, is));

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel Get Field IS Local Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Check whether a given face id exists in the "Face Sets" `DMLabel`.

  Collective across MPI processes.

  @param[in]   ratel     `Ratel` context
  @param[in]   dm        `DM` holding mesh
  @param[in]   face_id   Stratum value of face in "Face Sets" `DM:abel`
  @param[out]  has_face  `PETSC_TRUE` if `face` exists in "Face Sets" `DMLabel`, otherwise `PETSC_FALSE`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelDMHasFace(Ratel ratel, DM dm, PetscInt face_id, PetscBool *has_face) {
  PetscBool has_face_local = PETSC_FALSE;
  DMLabel   face_sets      = NULL;

  PetscFunctionBeginUser;
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DM Has Face"));

  *has_face = PETSC_FALSE;
  PetscCall(DMGetLabel(dm, "Face Sets", &face_sets));

  if (face_sets) {
    PetscCall(DMLabelHasValue(face_sets, face_id, &has_face_local));
    PetscCall(MPIU_Allreduce(&has_face_local, has_face, 1, MPIU_BOOL, MPI_LOR, ratel->comm));
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "-- Ratel DM Has Face Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}
