/// @file
/// Implementation of Ratel utilities

#include <ceed.h>
#include <ceed/backend.h>
#include <ratel-impl.h>
#include <ratel-utils.h>
#include <ratel.h>
#include <ratel/models/boundary.h>
#include <stdbool.h>
#include <stdlib.h>

/// @addtogroup RatelInternal
/// @{

/**
  @brief Clone a single, non-composite `CeedOperator`.

  Not collective across MPI processes.

  @param[in]   ratel     `Ratel` context object
  @param[in]   op        `CeedOperator` to clone
  @param[out]  op_clone  Cloned `CeedOperator`

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode RatelCeedOperatorClone_Single(Ratel ratel, CeedOperator op, CeedOperator *op_clone) {
  Ceed          ceed;
  CeedQFunction qf, qf_clone;

  PetscFunctionBeginUser;
  RatelCallCeed(ratel, CeedOperatorGetCeed(op, &ceed));
  // Clone QFunction
  {
    const char         *qf_kernel_name, *qf_source_path;
    char                qf_source[PETSC_MAX_PATH_LEN];
    CeedInt             num_input_fields, num_output_fields, vector_length;
    CeedQFunctionUser   user_f;
    CeedQFunctionField *input_fields, *output_fields;

    RatelCallCeed(ratel, CeedOperatorGetQFunction(op, &qf));
    RatelCallCeed(ratel, CeedQFunctionGetFields(qf, &num_input_fields, &input_fields, &num_output_fields, &output_fields));
    RatelCallCeed(ratel, CeedQFunctionGetVectorLength(qf, &vector_length));
    RatelCallCeed(ratel, CeedQFunctionGetUserFunction(qf, &user_f));
    RatelCallCeed(ratel, CeedQFunctionGetSourcePath(qf, &qf_source_path));
    RatelCallCeed(ratel, CeedQFunctionGetKernelName(qf, &qf_kernel_name));
    PetscCall(PetscSNPrintf(qf_source, PETSC_MAX_PATH_LEN, "%s:%s", qf_source_path, qf_kernel_name));

    RatelCallCeed(ratel, CeedQFunctionCreateInterior(ceed, vector_length, user_f, qf_source, &qf_clone));
    for (PetscInt i = 0; i < num_input_fields; i++) {
      const char  *field_name;
      CeedInt      size;
      CeedEvalMode eval_mode;

      RatelCallCeed(ratel, CeedQFunctionFieldGetData(input_fields[i], &field_name, &size, &eval_mode));
      RatelCallCeed(ratel, CeedQFunctionAddInput(qf_clone, field_name, size, eval_mode));
    }
    for (PetscInt i = 0; i < num_output_fields; i++) {
      const char  *field_name;
      CeedInt      size;
      CeedEvalMode eval_mode;

      RatelCallCeed(ratel, CeedQFunctionFieldGetData(output_fields[i], &field_name, &size, &eval_mode));
      RatelCallCeed(ratel, CeedQFunctionAddOutput(qf_clone, field_name, size, eval_mode));
    }
    {
      CeedQFunctionContext ctx = NULL, ctx_clone;

      RatelCallCeed(ratel, CeedQFunctionGetContext(qf, &ctx));
      if (ctx) {
        char                               *data;
        size_t                              ctx_size;
        CeedMemType                         f_mem_type;
        CeedQFunctionContextDataDestroyUser destroy_f = NULL;

        RatelCallCeed(ratel, CeedQFunctionContextCreate(ceed, &ctx_clone));

        RatelCallCeed(ratel, CeedQFunctionContextGetContextSize(ctx, &ctx_size));
        RatelCallCeed(ratel, CeedQFunctionContextGetDataRead(ctx, CEED_MEM_HOST, &data));
        RatelCallCeed(ratel, CeedQFunctionContextSetData(ctx_clone, CEED_MEM_HOST, CEED_COPY_VALUES, ctx_size, (void *)data));
        RatelCallCeed(ratel, CeedQFunctionContextRestoreDataRead(ctx, &data));

        {
          CeedInt                      num_field_labels = 0;
          const CeedContextFieldLabel *field_labels;

          RatelCallCeed(ratel, CeedQFunctionContextGetAllFieldLabels(ctx, &field_labels, &num_field_labels));
          for (PetscInt i = 0; i < num_field_labels; i++) {
            const char          *field_name, *field_description;
            size_t               field_offset, num_values;
            CeedContextFieldType field_type;

            RatelCallCeed(ratel, CeedContextFieldLabelGetDescription(field_labels[i], &field_name, &field_offset, &num_values, &field_description,
                                                                     &field_type));
            switch (field_type) {
              case CEED_CONTEXT_FIELD_DOUBLE:
                RatelCallCeed(ratel, CeedQFunctionContextRegisterDouble(ctx_clone, field_name, field_offset, num_values, field_description));
                break;
              case CEED_CONTEXT_FIELD_INT32:
                RatelCallCeed(ratel, CeedQFunctionContextRegisterInt32(ctx_clone, field_name, field_offset, num_values, field_description));
                break;
              case CEED_CONTEXT_FIELD_BOOL:
                RatelCallCeed(ratel, CeedQFunctionContextRegisterBoolean(ctx_clone, field_name, field_offset, num_values, field_description));
                break;
            }
          }
        }

        RatelCallCeed(ratel, CeedQFunctionContextGetDataDestroy(ctx, &f_mem_type, &destroy_f));
        if (destroy_f) RatelCallCeed(ratel, CeedQFunctionContextSetDataDestroy(ctx_clone, f_mem_type, destroy_f));

        RatelCallCeed(ratel, CeedQFunctionSetContext(qf_clone, ctx_clone));
        RatelCallCeed(ratel, CeedQFunctionContextDestroy(&ctx_clone));
      }
      RatelCallCeed(ratel, CeedQFunctionContextDestroy(&ctx));
    }
    {
      CeedSize flops = 0;

      RatelCallCeed(ratel, CeedQFunctionGetFlopsEstimate(qf, &flops));
      // QFunction flops should always be set, but double check
      if (flops >= 0) RatelCallCeed(ratel, CeedQFunctionSetUserFlopsEstimate(qf_clone, flops));
    }
  }
  // Clone Operator
  {
    CeedInt            num_input_fields, num_output_fields;
    CeedOperatorField *input_fields, *output_fields;
    bool               is_at_points;

    RatelCallCeed(ratel, CeedOperatorIsAtPoints(op, &is_at_points));
    RatelCallCeed(ratel, CeedOperatorGetFields(op, &num_input_fields, &input_fields, &num_output_fields, &output_fields));

    if (is_at_points) {
      RatelCallCeed(ratel, CeedOperatorCreateAtPoints(ceed, qf_clone, NULL, NULL, op_clone));
    } else {
      RatelCallCeed(ratel, CeedOperatorCreate(ceed, qf_clone, NULL, NULL, op_clone));
    }
    for (PetscInt i = 0; i < num_input_fields; i++) {
      const char         *field_name;
      CeedVector          vec;
      CeedElemRestriction elem_restriction;
      CeedBasis           basis;

      RatelCallCeed(ratel, CeedOperatorFieldGetData(input_fields[i], &field_name, &elem_restriction, &basis, &vec));
      RatelCallCeed(ratel, CeedOperatorSetField(*op_clone, field_name, elem_restriction, basis, vec));
      RatelCallCeed(ratel, CeedVectorDestroy(&vec));
      RatelCallCeed(ratel, CeedElemRestrictionDestroy(&elem_restriction));
      RatelCallCeed(ratel, CeedBasisDestroy(&basis));
    }
    for (PetscInt i = 0; i < num_output_fields; i++) {
      const char         *field_name;
      CeedVector          vec;
      CeedElemRestriction elem_restriction;
      CeedBasis           basis;

      RatelCallCeed(ratel, CeedOperatorFieldGetData(output_fields[i], &field_name, &elem_restriction, &basis, &vec));
      RatelCallCeed(ratel, CeedOperatorSetField(*op_clone, field_name, elem_restriction, basis, vec));
      RatelCallCeed(ratel, CeedVectorDestroy(&vec));
      RatelCallCeed(ratel, CeedElemRestrictionDestroy(&elem_restriction));
      RatelCallCeed(ratel, CeedBasisDestroy(&basis));
    }
    if (is_at_points) {
      CeedElemRestriction restriction_points = NULL;
      CeedVector          points             = NULL;

      RatelCallCeed(ratel, CeedOperatorAtPointsGetPoints(op, &restriction_points, &points));
      RatelCallCeed(ratel, CeedOperatorAtPointsSetPoints(*op_clone, restriction_points, points));
      RatelCallCeed(ratel, CeedVectorDestroy(&points));
      RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_points));

      // Cleanup
      RatelCallCeed(ratel, CeedElemRestrictionDestroy(&restriction_points));
      RatelCallCeed(ratel, CeedVectorDestroy(&points));
    }
  }
  RatelCallCeed(ratel, CeedDestroy(&ceed));
  RatelCallCeed(ratel, CeedQFunctionDestroy(&qf));
  RatelCallCeed(ratel, CeedQFunctionDestroy(&qf_clone));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Create a new `CeedOperator` that references the same fields and inputs.
    This new `CeedOperator` will reference the same input and output vectors, so using `op_clone` will change any passive output `CeedVectors` for
`op`.

  Not collective across MPI processes.

  @param[in]   ratel     `Ratel` context object
  @param[in]   op        `CeedOperator` to clone
  @param[out]  op_clone  Cloned `CeedOperator`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelCeedOperatorClone(Ratel ratel, CeedOperator op, CeedOperator *op_clone) {
  bool is_composite;
  Ceed ceed = ratel->ceed;

  PetscFunctionBeginUser;
  RatelCallCeed(ratel, CeedOperatorIsComposite(op, &is_composite));
  if (is_composite) {
    CeedInt       num_sub_ops = 0;
    CeedOperator *sub_ops;

    RatelCallCeed(ratel, CeedCompositeOperatorCreate(ceed, op_clone));
    RatelCallCeed(ratel, CeedCompositeOperatorGetNumSub(op, &num_sub_ops));
    RatelCallCeed(ratel, CeedCompositeOperatorGetSubList(op, &sub_ops));
    for (PetscInt i = 0; i < num_sub_ops; i++) {
      CeedOperator sub_op;

      PetscCall(RatelCeedOperatorClone_Single(ratel, sub_ops[i], &sub_op));
      RatelCallCeed(ratel, CeedCompositeOperatorAddSub(*op_clone, sub_op));
      RatelCallCeed(ratel, CeedOperatorDestroy(&sub_op));
    }
  } else {
    PetscCall(RatelCeedOperatorClone_Single(ratel, op, op_clone));
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Convert flexible loading values to incremental versions by differentiating vectors.

  Not collective across MPI processes.

  @param[in]      ratel        `Ratel` context
  @param[in]      scale_dt     Should the time derivative be scaled by the time step?
  @param[in]      num_comp     Number of components in `vectors`
  @param[in,out]  num_times    Length of `times` and `vectors` arrays, maybe edited in-place
  @param[in,out]  vectors      Array of size `num_times x num_comp` containing the values of the flexible loading vectors, edited in-place
  @param[in,out]  times        Array of size `num_times` containing the times at which the flexible loading transitions occur, edited in-place
  @param[in,out]  interp_type  Type of interpolation to use for the flexible loading vectors, edited in-place

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode RatelIncrementalize(Ratel ratel, PetscBool scale_dt, CeedInt num_comp, CeedInt *num_times, CeedScalar *vectors, CeedScalar *times,
                                   RatelBCInterpolationType *interp_type) {
  PetscReal dt = 1.0;

  PetscFunctionBeginUser;
  if (*num_times == 0) PetscFunctionReturn(PETSC_SUCCESS);
  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "---- Incrementalizing Flexible Loading"));

  PetscOptionsBegin(ratel->comm, NULL, "Incrementalize Flexible Loading", NULL);
  PetscCall(PetscOptionsReal("-ts_dt", "Time step", NULL, dt, &dt, NULL));
  PetscOptionsEnd();

  switch (*interp_type) {
    case RATEL_BC_INTERP_NONE: {
      // TODO: Support for impulse loading
      SETERRQ(ratel->comm, PETSC_ERR_SUP, "Incrementalizing piecewise constant flexible loading not yet supported");
    } break;
    case RATEL_BC_INTERP_LINEAR: {
      // TODO: If initial time is zero, then treat the first vector as an initial impulse
      PetscBool has_initial_value = PETSC_FALSE;
      if (PetscEqualScalar(times[0], 0.0)) {
        for (PetscInt j = 0; j < num_comp; j++) has_initial_value = has_initial_value || !PetscEqualScalar(vectors[j], 0.0);
      }
      PetscCheck(!has_initial_value, ratel->comm, PETSC_ERR_SUP, "Non-zero initial vector not yet supported for incrementalizing");
      PetscCheck((*num_times + 1) < RATEL_MAX_BC_INTERP_POINTS, ratel->comm, PETSC_ERR_SUP, "Too many times provided to incrementalize");
      // Work backwards to differentiate vectors
      for (PetscInt i = *num_times - 1; i >= 0; i--) {
        PetscReal num_timesteps = (times[i] - (i == 0 ? 0 : times[i - 1])) / (scale_dt ? dt : 1);

        if (PetscEqualScalar(num_timesteps, 0.0)) {
          for (PetscInt j = 0; j < num_comp; j++) {
            vectors[i * num_comp + j] = 0.0;
          }
        } else {
          for (PetscInt j = 0; j < num_comp; j++) {
            vectors[i * num_comp + j] = (vectors[i * num_comp + j] - (i == 0 ? 0 : vectors[(i - 1) * num_comp + j])) / num_timesteps;
          }
        }
      }
      // Set interpolation type to constant
      *interp_type = RATEL_BC_INTERP_NONE;
    } break;
  }

  PetscCall(RatelDebug256(ratel, RATEL_DEBUG_COLOR_SUCCESS, "------ Incrementalizing Flexible Loading Success!"));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/// @}
