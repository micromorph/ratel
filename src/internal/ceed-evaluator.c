/// @file
/// CeedEvaluator implementation

#include <ceed-boundary-evaluator.h>
#include <ceed-evaluator-impl.h>
#include <ceed-evaluator.h>
#include <ceed.h>
#include <ceed/backend.h>
#include <petsc-ceed-utils.h>
#include <petsc-ceed.h>
#include <petsc/private/petscimpl.h>
#include <petscdmplex.h>
#include <stdio.h>

PetscClassId  CEED_EVALUATOR_CLASSID;
PetscLogEvent CEED_EVALUATOR_APPLY, CEED_EVALUATOR_APPLY_CEEDOP;

/**
  @brief Initalize `CeedEvaluator` class.

  Not collective across MPI processes.

  @return An error code: 0 - success, otherwise - failure
**/
static PetscErrorCode CeedEvaluatorInitalize() {
  static PetscBool registered = PETSC_FALSE;

  PetscFunctionBeginUser;
  if (registered) PetscFunctionReturn(PETSC_SUCCESS);
  PetscCall(PetscClassIdRegister("CeedEvaluator", &CEED_EVALUATOR_CLASSID));
  PetscCall(PetscLogEventRegister("CeedEvalApp", CEED_EVALUATOR_CLASSID, &CEED_EVALUATOR_APPLY));
  PetscCall(PetscLogEventRegister("CeedEvalAppCeed", CEED_EVALUATOR_CLASSID, &CEED_EVALUATOR_APPLY_CEEDOP));
  registered = PETSC_TRUE;
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Create a `CeedEvaluator` object which computes global operation via local `CeedOperator`.

  Not collective across MPI processes.

  @param[in]   dm_x       Input `DMPlex`
  @param[in]   dm_y       Output `DMPlex`, or `NULL` to use `dm_x`
  @param[in]   op         `CeedOperator` to compute essential boundary values
  @param[out]  evaluator  `CeedEvaluator` object

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedEvaluatorCreate(DM dm_x, DM dm_y, CeedOperator op, CeedEvaluator *evaluator) {
  PetscFunctionBeginUser;
  PetscAssertPointer(evaluator, 4);
  PetscCall(CeedEvaluatorInitalize());

  // PETSc header
  {
    CeedEvaluator evaluator_;

    PetscCall(PetscHeaderCreate(evaluator_, CEED_EVALUATOR_CLASSID, "CeedEvaluator", "CeedEvaluator", "CeedEvaluator",
                                PetscObjectComm((PetscObject)dm_x), CeedEvaluatorDestroy, CeedEvaluatorView));
    *evaluator = evaluator_;
  }

  // PETSc objects
  PetscValidType(dm_x, 1);
  PetscCall(DMReferenceCopy(dm_x, &(*evaluator)->dm_x));
  if (!dm_y) dm_y = dm_x;
  PetscValidType(dm_y, 2);
  PetscCall(DMReferenceCopy(dm_y, &(*evaluator)->dm_y));

  // libCEED objects
  (*evaluator)->ceed = CeedOperatorReturnCeed(op);
  PetscCallCeed((*evaluator)->ceed, CeedReference((*evaluator)->ceed));
  PetscCallCeed((*evaluator)->ceed, CeedOperatorReferenceCopy(op, &(*evaluator)->op));

  // Check DM compatibility and make libCEED vectors
  {
    PetscInt dm_len;
    CeedSize x_loc_len, y_loc_len;
    Vec      X_loc, Y_loc;

    PetscCallCeed((*evaluator)->ceed, CeedOperatorGetActiveVectorLengths(op, &x_loc_len, &y_loc_len));

    PetscCall(DMGetLocalVector(dm_x, &X_loc));
    PetscCall(VecGetSize(X_loc, &dm_len));
    PetscCall(DMRestoreLocalVector(dm_x, &X_loc));
    PetscCheck(x_loc_len == -1 || x_loc_len == dm_len, PetscObjectComm((PetscObject)dm_x), PETSC_ERR_LIB,
               "op must match dm_x dimensions, %" CeedSize_FMT " != %" PetscInt_FMT, x_loc_len, dm_len);
    PetscCallCeed((*evaluator)->ceed, CeedVectorCreate((*evaluator)->ceed, x_loc_len, &(*evaluator)->x_loc));
    PetscCallCeed((*evaluator)->ceed, CeedVectorCreate((*evaluator)->ceed, x_loc_len, &(*evaluator)->x_dot_loc));

    PetscCall(DMGetLocalVector(dm_y, &Y_loc));
    PetscCall(VecGetSize(Y_loc, &dm_len));
    PetscCall(DMRestoreLocalVector(dm_y, &Y_loc));
    PetscCheck(y_loc_len == -1 || y_loc_len == dm_len, PetscObjectComm((PetscObject)dm_y), PETSC_ERR_LIB,
               "op must match dm_y dimensions, %" CeedSize_FMT " != %" PetscInt_FMT, y_loc_len, dm_len);
    PetscCallCeed((*evaluator)->ceed, CeedVectorCreate((*evaluator)->ceed, y_loc_len, &(*evaluator)->y_loc));
  }

  // Cached values
  (*evaluator)->time = NAN;
  (*evaluator)->dt   = NAN;

  // Log event
  PetscCall(CeedEvaluatorSetLogEvent(*evaluator, CEED_EVALUATOR_APPLY));
  PetscCall(CeedEvaluatorSetCeedOperatorLogEvent(*evaluator, CEED_EVALUATOR_APPLY_CEEDOP));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Create a `CeedEvaluator` object which computes global operation via local `CeedOperator` and shares local input `DM` and `Vec` with another
`CeedEvaluator`.

  Not collective across MPI processes.

  @param[in]   evaluator_orig  `CeedEvaluator` object to share local input `Vec` with
  @param[in]   dm_y            Output `DMPlex`
  @param[in]   op              `CeedOperator` to compute essential boundary values
  @param[out]  evaluator       `CeedEvaluator` object

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedEvaluatorCreateWithSameInputs(CeedEvaluator evaluator_orig, DM dm_y, CeedOperator op, CeedEvaluator *evaluator) {
  Vec        X_loc, X_dot_loc;
  CeedVector x_loc, x_dot_loc;
  DM         dm_x;

  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator_orig, CEED_EVALUATOR_CLASSID, 1);
  PetscValidType(dm_y, 2);
  PetscAssertPointer(evaluator, 4);
  PetscCall(CeedEvaluatorGetLocalVectors(evaluator_orig, &X_loc, &X_dot_loc, NULL));
  PetscCall(CeedEvaluatorGetLocalCeedVectors(evaluator_orig, &x_loc, &x_dot_loc, NULL));
  PetscCall(CeedEvaluatorGetDMs(evaluator_orig, &dm_x, NULL));
  PetscCall(CeedEvaluatorCreate(dm_x, dm_y, op, evaluator));
  PetscCall(CeedEvaluatorRestoreDMs(evaluator_orig, &dm_x, NULL));
  PetscCall(CeedEvaluatorSetLocalVectors(*evaluator, X_loc, X_dot_loc, NULL));
  PetscCall(CeedEvaluatorSetLocalCeedVectors(*evaluator, x_loc, x_dot_loc, NULL));
  PetscCall(CeedEvaluatorRestoreLocalVectors(evaluator_orig, &X_loc, &X_dot_loc, NULL));
  PetscCall(CeedEvaluatorRestoreLocalCeedVectors(evaluator_orig, &x_loc, &x_dot_loc, NULL));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief View a `CeedEvaluator` object.

  Not collective across MPI processes.

  @param[in]  evaluator  `CeedEvaluator` object
  @param[in]  viewer     Optional `PetscViewer` context or `NULL`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedEvaluatorView(CeedEvaluator evaluator, PetscViewer viewer) {
  PetscBool         is_ascii;
  PetscViewerFormat format;
  PetscMPIInt       size, rank;

  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator, CEED_EVALUATOR_CLASSID, 1);
  PetscValidHeaderSpecific(viewer, PETSC_VIEWER_CLASSID, 2);
  if (!viewer) PetscCall(PetscViewerASCIIGetStdout(PetscObjectComm((PetscObject)evaluator), &viewer));

  PetscCall(PetscViewerGetFormat(viewer, &format));
  PetscCallMPI(MPI_Comm_size(PetscObjectComm((PetscObject)evaluator), &size));
  if (size == 1 && format == PETSC_VIEWER_LOAD_BALANCE) PetscFunctionReturn(PETSC_SUCCESS);

  PetscCallMPI(MPI_Comm_rank(PetscObjectComm((PetscObject)evaluator), &rank));
  if (rank != 0) PetscFunctionReturn(PETSC_SUCCESS);

  PetscCall(PetscObjectTypeCompare((PetscObject)viewer, PETSCVIEWERASCII, &is_ascii));
  {
    PetscBool is_detailed     = format == PETSC_VIEWER_ASCII_INFO_DETAIL;
    char      rank_string[16] = {'\0'};
    FILE     *file;

    PetscCall(PetscViewerASCIIPrintf(viewer, "libCEED Operator Evaluator\n"));
    PetscCall(PetscViewerASCIIPushTab(viewer));  // CeedEvaluator
    PetscCall(PetscSNPrintf(rank_string, 16, "on Rank %d", rank));
    PetscCall(PetscViewerASCIIPrintf(viewer, "CeedOperator %s:\n", is_detailed ? rank_string : "Summary"));
    PetscCall(PetscViewerASCIIGetPointer(viewer, &file));
    PetscCall(PetscViewerASCIIPushTab(viewer));  // CeedOperator
    if (is_detailed) PetscCallCeed(evaluator->ceed, CeedOperatorView(evaluator->op, file));
    else PetscCallCeed(evaluator->ceed, CeedOperatorViewTerse(evaluator->op, file));
    PetscCall(PetscViewerASCIIPopTab(viewer));  // CeedOperator
    PetscCall(PetscViewerASCIIPopTab(viewer));  // CeedEvaluator
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Create a copy to the reference of a `CeedEvaluator` object.
         Note: If `evaluator_copy` is non-null, it is assumed to be a valid pointer to a `CeedEvaluator` and is destroyed.

  Not collective across MPI processes.

  @param[in]   evaluator       `CeedEvaluator` object
  @param[out]  evaluator_copy  Copied reference to `CeedEvaluator` object

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedEvaluatorReferenceCopy(CeedEvaluator evaluator, CeedEvaluator *evaluator_copy) {
  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator, CEED_EVALUATOR_CLASSID, 1);
  PetscAssertPointer(evaluator_copy, 2);
  PetscCall(PetscObjectReference((PetscObject)evaluator));
  PetscCall(CeedEvaluatorDestroy(evaluator_copy));
  *evaluator_copy = evaluator;
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Destroy a `CeedEvaluator` object.

  Not collective across MPI processes.

  @param[in,out]  evaluator  `CeedEvaluator` object

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedEvaluatorDestroy(CeedEvaluator *evaluator) {
  PetscFunctionBeginUser;
  PetscAssertPointer(evaluator, 1);
  if (!*evaluator) PetscFunctionReturn(PETSC_SUCCESS);
  PetscValidHeaderSpecific(*evaluator, CEED_EVALUATOR_CLASSID, 1);
  if (--((PetscObject)*evaluator)->refct > 0) {
    *evaluator = NULL;
    PetscFunctionReturn(PETSC_SUCCESS);
  }
  PetscCall(VecDestroy(&(*evaluator)->X_loc));
  PetscCall(VecDestroy(&(*evaluator)->X_dot_loc));
  PetscCall(VecDestroy(&(*evaluator)->Y_loc));
  PetscCall(DMDestroy(&(*evaluator)->dm_x));
  PetscCall(DMDestroy(&(*evaluator)->dm_y));
  PetscCallCeed((*evaluator)->ceed, CeedVectorDestroy(&(*evaluator)->x_loc));
  PetscCallCeed((*evaluator)->ceed, CeedVectorDestroy(&(*evaluator)->x_dot_loc));
  PetscCallCeed((*evaluator)->ceed, CeedVectorDestroy(&(*evaluator)->y_loc));
  PetscCallCeed((*evaluator)->ceed, CeedOperatorDestroy(&(*evaluator)->op));
  PetscCheck(CeedDestroy(&(*evaluator)->ceed) == CEED_ERROR_SUCCESS, PETSC_COMM_SELF, PETSC_ERR_LIB, "destroying libCEED context object failed");
  PetscCall(PetscHeaderDestroy(evaluator));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Set the current value of a context field for a `CeedEvaluator` object.

  Not collective across MPI processes.

  @param[in,out]  evaluator  `CeedEvaluator` object
  @param[in]      name       Name of the context field
  @param[in]      value      New context field value

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedEvaluatorSetContextDouble(CeedEvaluator evaluator, const char *name, double value) {
  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator, CEED_EVALUATOR_CLASSID, 1);
  {
    CeedContextFieldLabel label = NULL;

    PetscCallCeed(evaluator->ceed, CeedOperatorGetContextFieldLabel(evaluator->op, name, &label));
    if (label) PetscCallCeed(evaluator->ceed, CeedOperatorSetContextDouble(evaluator->op, label, &value));
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get the current value of a context field for a `CeedEvaluator` object.

  Not collective across MPI processes.

  @param[in]   evaluator  `CeedEvaluator` object
  @param[in]   name       Name of the context field
  @param[out]  value      Current context field value, unchanged if @a name is not a registered context field

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedEvaluatorGetContextDouble(CeedEvaluator evaluator, const char *name, double *value) {
  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator, CEED_EVALUATOR_CLASSID, 1);
  {
    CeedContextFieldLabel label = NULL;

    PetscCallCeed(evaluator->ceed, CeedOperatorGetContextFieldLabel(evaluator->op, name, &label));
    if (label) {
      PetscSizeT    num_values;
      const double *values_ceed;

      PetscCallCeed(evaluator->ceed, CeedOperatorGetContextDoubleRead(evaluator->op, label, &num_values, &values_ceed));
      *value = values_ceed[0];
      PetscCallCeed(evaluator->ceed, CeedOperatorRestoreContextDoubleRead(evaluator->op, label, &values_ceed));
    }
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Set the current time and update local essential boundary values for a `CeedEvaluator` object.

  Not collective across MPI processes.

  @param[in,out]  evaluator  `CeedEvaluator` object
  @param[in]      time       Current time

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedEvaluatorUpdateTimeAndBoundaryValues(CeedEvaluator evaluator, PetscReal time) {
  PetscReal current_time = 0., dt = NAN;
  PetscBool update_boundary_values = PETSC_FALSE;

  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator, CEED_EVALUATOR_CLASSID, 1);
  // Update time, if needed
  PetscCall(CeedEvaluatorGetTime(evaluator, &current_time));
  if (!PetscEqualReal(time, current_time)) {
    PetscCall(CeedEvaluatorSetTime(evaluator, time));
    update_boundary_values = PETSC_TRUE;
  }

  // Update boundary evaluator's dt, if they exist, to match evaluator dt
  // If the dt or time has changed, need to update the boundary values
  PetscCall(CeedEvaluatorGetDt(evaluator, &dt));
  {
    CeedBoundaryEvaluator evaluator_boundary_u = NULL;

    // Update boundary evaluator U
    PetscCall(DMPlexGetCeedBoundaryEvaluator(evaluator->dm_x, &evaluator_boundary_u));
    if (evaluator_boundary_u) {
      PetscReal current_dt, current_boundary_time;

      // Check if dt has changed, if so, update dt and boundary values
      PetscCall(CeedBoundaryEvaluatorGetDt(evaluator_boundary_u, &current_dt));
      const PetscBool is_dt_changed = !PetscEqualReal(dt, current_dt);
      if (is_dt_changed) PetscCall(CeedBoundaryEvaluatorSetDt(evaluator_boundary_u, dt));
      update_boundary_values |= is_dt_changed;

      // Check if time has changed, if so, update boundary values
      PetscCall(CeedBoundaryEvaluatorGetTime(evaluator_boundary_u, &current_boundary_time));
      update_boundary_values |= !PetscEqualReal(time, current_boundary_time);
    }
  }

  if (update_boundary_values) {
    if (!evaluator->X_loc) PetscCall(DMCreateLocalVector(evaluator->dm_x, &evaluator->X_loc));
    PetscCall(DMPlexInsertBoundaryValues(evaluator->dm_x, PETSC_TRUE, evaluator->X_loc, time, NULL, NULL, NULL));
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Set the current time and timestep size and update local essential boundary values for a `CeedEvaluator` object.

  Not collective across MPI processes.

  @param[in,out]  evaluator  `CeedEvaluator` object
  @param[in]      time       Current time
  @param[in]      dt         Current timestep size

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedEvaluatorUpdateTimeDtAndBoundaryValues(CeedEvaluator evaluator, PetscReal time, PetscReal dt) {
  PetscReal current_dt = NAN;

  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator, CEED_EVALUATOR_CLASSID, 1);

  // Update evaluator dt, if needed
  PetscCall(CeedEvaluatorGetDt(evaluator, &current_dt));
  if (!PetscEqualReal(dt, current_dt)) PetscCall(CeedEvaluatorSetDt(evaluator, dt));

  // Update time, boundary evaluators, and boundary values
  PetscCall(CeedEvaluatorUpdateTimeAndBoundaryValues(evaluator, time));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Set the current time for a `CeedEvaluator` object.

  Not collective across MPI processes.

  @note Updates the cached value and the context field value, if it exists.

  @param[in,out]  evaluator  `CeedEvaluator` object
  @param[in]      time       Current time

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedEvaluatorSetTime(CeedEvaluator evaluator, PetscReal time) {
  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator, CEED_EVALUATOR_CLASSID, 1);
  // Update cached value
  evaluator->time = time;
  // Update contexts with cached value
  PetscCall(CeedEvaluatorSetContextDouble(evaluator, "time", evaluator->time));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get the current time for a `CeedEvaluator` object.

  Not collective across MPI processes.

  @note Updates the cached value in the `CeedEvaluator` object using the context field value, if it exists.

  @param[in]   evaluator  `CeedEvaluator` object
  @param[out]  time       Current time from time field or last cached value if no time field exists, or `NAN` if not set

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedEvaluatorGetTime(CeedEvaluator evaluator, PetscReal *time) {
  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator, CEED_EVALUATOR_CLASSID, 1);
  PetscAssertPointer(time, 2);
  // Update cached value from context, if field exists
  PetscCall(CeedEvaluatorGetContextDouble(evaluator, "time", &evaluator->time));
  // Return the time from context, or the last time passed to CeedEvaluatorSetTime if no time field
  *time = evaluator->time;
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Set the current time step size for a `CeedEvaluator` object.

  Not collective across MPI processes.

  @note Updates the cached value and the context field value, if it exists.

  @param[in,out]  evaluator  `CeedEvaluator` object
  @param[in]      dt         Current time step

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedEvaluatorSetDt(CeedEvaluator evaluator, PetscReal dt) {
  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator, CEED_EVALUATOR_CLASSID, 1);
  // Update cached value
  evaluator->dt = dt;
  // Update contexts with cached value
  PetscCall(CeedEvaluatorSetContextDouble(evaluator, "dt", evaluator->dt));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get the current time step size for a `CeedEvaluator` object.

  Not collective across MPI processes.

  @note Updates the cached value in the `CeedEvaluator` object using the context field value, if it exists.

  @param[in]   evaluator  `CeedEvaluator` object
  @param[out]  dt         Current dt from dt field or last cached value if no dt field exists, or NAN if not set

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedEvaluatorGetDt(CeedEvaluator evaluator, PetscReal *dt) {
  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator, CEED_EVALUATOR_CLASSID, 1);
  PetscAssertPointer(dt, 2);
  // Update cached value from context, if field exists
  PetscCall(CeedEvaluatorGetContextDouble(evaluator, "dt", &evaluator->dt));
  // Return the dt from context, or the last dt passed to CeedEvaluatorSetDt if no dt field
  *dt = evaluator->dt;
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Determine of a named field is present in the `CeedOperator` of a `CeedEvaluator` object.

  Not collective across MPI processes.

  @param[in]   evaluator  `CeedEvaluator` object
  @param[in]   name       Name of field to check for
  @param[out]  has_field  Boolean flag indicating if field is present on `CeedOperator`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedEvaluatorHasField(CeedEvaluator evaluator, const char *name, PetscBool *has_field) {
  bool is_composite = false;

  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator, CEED_EVALUATOR_CLASSID, 1);
  *has_field = PETSC_FALSE;

  PetscCallCeed(evaluator->ceed, CeedOperatorIsComposite(evaluator->op, &is_composite));
  if (is_composite) {
    CeedInt       num_sub;
    CeedOperator *sub_operators;

    PetscCallCeed(evaluator->ceed, CeedCompositeOperatorGetNumSub(evaluator->op, &num_sub));
    PetscCallCeed(evaluator->ceed, CeedCompositeOperatorGetSubList(evaluator->op, &sub_operators));
    for (CeedInt i = 0; i < num_sub; i++) {
      CeedOperatorField field;

      PetscCallCeed(evaluator->ceed, CeedOperatorGetFieldByName(sub_operators[i], name, &field));
      if (field) *has_field = PETSC_TRUE;
    }
  } else {
    CeedOperatorField field;

    PetscCallCeed(evaluator->ceed, CeedOperatorGetFieldByName(evaluator->op, name, &field));
    if (field) *has_field = PETSC_TRUE;
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get `DM`s for `CeedEvaluator` operations.

  Not collective across MPI processes.

  @param[in,out]  evaluator  `CeedEvaluator`
  @param[out]     dm_x       Input PETSc `DM`, or NULL
  @param[out]     dm_y       Output PETSc `DM`, or NULL

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedEvaluatorGetDMs(CeedEvaluator evaluator, DM *dm_x, DM *dm_y) {
  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator, CEED_EVALUATOR_CLASSID, 1);
  if (dm_x) {
    PetscAssertPointer(dm_x, 2);
    *dm_x = NULL;
    PetscCall(DMReferenceCopy(evaluator->dm_x, dm_x));
  }
  if (dm_y) {
    PetscAssertPointer(dm_y, 3);
    *dm_y = NULL;
    PetscCall(DMReferenceCopy(evaluator->dm_y, dm_y));
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Restore `DM`s for `CeedEvaluator` operations.

  Not collective across MPI processes.

  @param[in,out]  evaluator  `CeedEvaluator`
  @param[out]     dm_x       Input PETSc `DM`, or NULL
  @param[out]     dm_y       Output PETSc `DM`, or NULL

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedEvaluatorRestoreDMs(CeedEvaluator evaluator, DM *dm_x, DM *dm_y) {
  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator, CEED_EVALUATOR_CLASSID, 1);
  if (dm_x) PetscCall(DMDestroy(dm_x));
  if (dm_y) PetscCall(DMDestroy(dm_y));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Set local vectors for `CeedEvaluator` operations.

  Not collective across MPI processes.

  @param[in,out]  evaluator  `CeedEvaluator`
  @param[in]      X_loc      Input PETSc local vector, or NULL
  @param[in]      X_dot_loc  Input PETSc local vector for time derivative, or NULL
  @param[in]      Y_loc      Output PETSc local vector, or NULL

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedEvaluatorSetLocalVectors(CeedEvaluator evaluator, Vec X_loc, Vec X_dot_loc, Vec Y_loc) {
  PetscInt dm_len, set_len;
  Vec      dm_loc;

  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator, CEED_EVALUATOR_CLASSID, 1);
  if (X_loc) {
    PetscValidType(X_loc, 2);
    PetscCall(DMGetLocalVector(evaluator->dm_x, &dm_loc));
    PetscCall(VecGetSize(dm_loc, &dm_len));
    PetscCall(DMRestoreLocalVector(evaluator->dm_x, &dm_loc));
    PetscCall(VecGetSize(X_loc, &set_len));
    PetscCheck(set_len == dm_len, PetscObjectComm((PetscObject)evaluator->dm_x), PETSC_ERR_LIB,
               "X_loc must match dm_x local size, %" PetscInt_FMT " != %" PetscInt_FMT, set_len, dm_len);
    PetscCall(VecReferenceCopy(X_loc, &evaluator->X_loc));
  }
  if (X_dot_loc) {
    PetscValidType(X_dot_loc, 3);
    PetscCall(DMGetLocalVector(evaluator->dm_x, &dm_loc));
    PetscCall(VecGetSize(dm_loc, &dm_len));
    PetscCall(DMRestoreLocalVector(evaluator->dm_x, &dm_loc));
    PetscCall(VecGetSize(X_dot_loc, &set_len));
    PetscCheck(set_len == dm_len, PetscObjectComm((PetscObject)evaluator->dm_x), PETSC_ERR_LIB,
               "X_dot_loc must match dm_x local size, %" PetscInt_FMT " != %" PetscInt_FMT, set_len, dm_len);
    PetscCall(VecReferenceCopy(X_dot_loc, &evaluator->X_dot_loc));
  }
  if (Y_loc) {
    PetscValidType(Y_loc, 4);
    PetscCall(DMGetLocalVector(evaluator->dm_y, &dm_loc));
    PetscCall(VecGetSize(dm_loc, &dm_len));
    PetscCall(DMRestoreLocalVector(evaluator->dm_y, &dm_loc));
    PetscCall(VecGetSize(Y_loc, &set_len));
    PetscCheck(set_len == dm_len, PetscObjectComm((PetscObject)evaluator->dm_y), PETSC_ERR_LIB,
               "Y_loc must match dm_y local size, %" PetscInt_FMT " != %" PetscInt_FMT, set_len, dm_len);
    PetscCall(VecReferenceCopy(Y_loc, &evaluator->Y_loc));
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get local vectors for `CeedEvaluator` operations.

  Not collective across MPI processes.

  @param[in,out]  evaluator  `CeedEvaluator`
  @param[out]     X_loc      Input PETSc local vector, or NULL
  @param[out]     X_dot_loc  Input PETSc local vector for time derivative, or NULL
  @param[out]     Y_loc      Output PETSc local vector, or NULL

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedEvaluatorGetLocalVectors(CeedEvaluator evaluator, Vec *X_loc, Vec *X_dot_loc, Vec *Y_loc) {
  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator, CEED_EVALUATOR_CLASSID, 1);
  if (X_loc) {
    PetscAssertPointer(X_loc, 2);
    *X_loc = NULL;
    if (evaluator->X_loc) PetscCall(VecReferenceCopy(evaluator->X_loc, X_loc));
  }
  if (X_dot_loc) {
    PetscAssertPointer(X_dot_loc, 3);
    *X_dot_loc = NULL;
    if (evaluator->X_dot_loc) PetscCall(VecReferenceCopy(evaluator->X_dot_loc, X_dot_loc));
  }
  if (Y_loc) {
    PetscAssertPointer(Y_loc, 4);
    *Y_loc = NULL;
    if (evaluator->Y_loc) PetscCall(VecReferenceCopy(evaluator->Y_loc, Y_loc));
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Restore local vectors for `CeedEvaluator` operations.

  Not collective across MPI processes.

  @param[in,out]  evaluator  `CeedEvaluator`
  @param[in,out]  X_loc      Input PETSc local vector, or NULL
  @param[in,out]  X_dot_loc  Input PETSc local vector for time derivative, or NULL
  @param[in,out]  Y_loc      Output PETSc local vector, or NULL

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedEvaluatorRestoreLocalVectors(CeedEvaluator evaluator, Vec *X_loc, Vec *X_dot_loc, Vec *Y_loc) {
  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator, CEED_EVALUATOR_CLASSID, 1);
  if (X_loc) {
    PetscAssertPointer(X_loc, 2);
    if (evaluator->X_loc) PetscCall(VecDestroy(X_loc));
  }
  if (X_dot_loc) {
    PetscAssertPointer(X_dot_loc, 3);
    if (evaluator->X_dot_loc) PetscCall(VecDestroy(X_dot_loc));
  }
  if (Y_loc) {
    PetscAssertPointer(Y_loc, 4);
    if (evaluator->Y_loc) PetscCall(VecDestroy(Y_loc));
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Set local libCEED vectors for `CeedEvaluator` operations.

  Not collective across MPI processes.

  @param[in,out]  evaluator  `CeedEvaluator`
  @param[in]      x_loc      Input libCEED local vector, or NULL
  @param[in]      x_dot_loc  Input libCEED local vector for time derivative, or NULL
  @param[in]      y_loc      Output libCEED local vector, or NULL

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedEvaluatorSetLocalCeedVectors(CeedEvaluator evaluator, CeedVector x_loc, CeedVector x_dot_loc, CeedVector y_loc) {
  PetscInt dm_len;
  CeedSize set_len;
  Vec      dm_loc;

  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator, CEED_EVALUATOR_CLASSID, 1);
  if (x_loc) {
    PetscCall(DMGetLocalVector(evaluator->dm_x, &dm_loc));
    PetscCall(VecGetSize(dm_loc, &dm_len));
    PetscCall(DMRestoreLocalVector(evaluator->dm_x, &dm_loc));
    PetscCall(CeedVectorGetLength(x_loc, &set_len));
    PetscCheck(set_len == dm_len, PetscObjectComm((PetscObject)evaluator->dm_x), PETSC_ERR_LIB,
               "x_loc must match dm_x local size, %" CeedSize_FMT " != %" PetscInt_FMT, set_len, dm_len);
    PetscCall(CeedVectorReferenceCopy(x_loc, &evaluator->x_loc));
  }
  if (x_dot_loc) {
    PetscCall(DMGetLocalVector(evaluator->dm_x, &dm_loc));
    PetscCall(VecGetSize(dm_loc, &dm_len));
    PetscCall(DMRestoreLocalVector(evaluator->dm_x, &dm_loc));
    PetscCall(CeedVectorGetLength(x_dot_loc, &set_len));
    PetscCheck(set_len == dm_len, PetscObjectComm((PetscObject)evaluator->dm_x), PETSC_ERR_LIB,
               "x_dot_loc must match dm_x local size, %" CeedSize_FMT " != %" PetscInt_FMT, set_len, dm_len);
    PetscCall(CeedVectorReferenceCopy(x_dot_loc, &evaluator->x_dot_loc));
  }
  if (y_loc) {
    PetscCall(DMGetLocalVector(evaluator->dm_y, &dm_loc));
    PetscCall(VecGetSize(dm_loc, &dm_len));
    PetscCall(DMRestoreLocalVector(evaluator->dm_y, &dm_loc));
    PetscCall(CeedVectorGetLength(y_loc, &set_len));
    PetscCheck(set_len == dm_len, PetscObjectComm((PetscObject)evaluator->dm_x), PETSC_ERR_LIB,
               "y_loc must match dm_x local size, %" CeedSize_FMT " != %" PetscInt_FMT, set_len, dm_len);
    PetscCall(CeedVectorReferenceCopy(y_loc, &evaluator->y_loc));
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get local libCEED vectors for `CeedEvaluator` operations.

  Not collective across MPI processes.

  @param[in,out]  evaluator  `CeedEvaluator`
  @param[out]     x_loc      Input libCEED local vector, or NULL
  @param[out]     x_dot_loc  Input libCEED local vector for time derivative, or NULL
  @param[out]     y_loc      Output libCEED local vector, or NULL

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedEvaluatorGetLocalCeedVectors(CeedEvaluator evaluator, CeedVector *x_loc, CeedVector *x_dot_loc, CeedVector *y_loc) {
  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator, CEED_EVALUATOR_CLASSID, 1);
  if (x_loc) {
    *x_loc = NULL;
    if (evaluator->x_loc) PetscCallCeed(evaluator->ceed, CeedVectorReferenceCopy(evaluator->x_loc, x_loc));
  }
  if (x_dot_loc) {
    *x_dot_loc = NULL;
    if (evaluator->x_dot_loc) PetscCallCeed(evaluator->ceed, CeedVectorReferenceCopy(evaluator->x_dot_loc, x_dot_loc));
  }
  if (y_loc) {
    *y_loc = NULL;
    if (evaluator->y_loc) PetscCallCeed(evaluator->ceed, CeedVectorReferenceCopy(evaluator->y_loc, y_loc));
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Restore local libCEED vectors for `CeedEvaluator` operations.

  Not collective across MPI processes.

  @param[in,out]  evaluator  `CeedEvaluator`
  @param[in,out]  x_loc      Input libCEED local vector, or NULL
  @param[in,out]  x_dot_loc  Input libCEED local vector for time derivative, or NULL
  @param[in,out]  y_loc      Output libCEED local vector, or NULL

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedEvaluatorRestoreLocalCeedVectors(CeedEvaluator evaluator, CeedVector *x_loc, CeedVector *x_dot_loc, CeedVector *y_loc) {
  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator, CEED_EVALUATOR_CLASSID, 1);
  if (x_loc) {
    PetscCheck(evaluator->x_loc, PetscObjectComm((PetscObject)evaluator->dm_x), PETSC_ERR_ARG_NULL, "No libCEED x_loc vector was set or gotten");
    PetscCallCeed(evaluator->ceed, CeedVectorDestroy(x_loc));
  }
  if (x_dot_loc) {
    PetscCheck(evaluator->x_dot_loc, PetscObjectComm((PetscObject)evaluator->dm_x), PETSC_ERR_ARG_NULL,
               "No libCEED x_dot_loc vector was set or gotten");
    PetscCallCeed(evaluator->ceed, CeedVectorDestroy(x_dot_loc));
  }
  if (y_loc) {
    PetscCheck(evaluator->x_loc, PetscObjectComm((PetscObject)evaluator->dm_y), PETSC_ERR_ARG_NULL, "No libCEED y_loc vector was set or gotten");
    PetscCallCeed(evaluator->ceed, CeedVectorDestroy(y_loc));
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Apply the local action of a `CeedOperator` and store result in PETSc vector.

  Collective across MPI processes.

  @param[in]   evaluator  `CeedEvaluator`
  @param[in]   X          Input PETSc global vector, for logging only
  @param[in]   Y          Output PETSc global vector, for logging only
  @param[in]   X_loc      Input PETSc local vector
  @param[in]   X_dot_loc  Input PETSc local time derivative vector, or NULL if and only if X_dot is NULL
  @param[out]  Y_loc      Output PETSc local vector


  @return An error code: 0 - success, otherwise - failure
**/
static inline PetscErrorCode CeedEvaluatorApply_Core(CeedEvaluator evaluator, Vec X, Vec Y, Vec X_loc, Vec X_dot_loc, Vec Y_loc) {
  PetscMemType x_mem_type, x_dot_mem_type, y_mem_type;

  PetscFunctionBeginUser;
  // Setup libCEED vectors
  PetscCall(VecReadPetscToCeed(X_loc, &x_mem_type, evaluator->x_loc));
  if (X_dot_loc) PetscCall(VecReadPetscToCeed(X_dot_loc, &x_dot_mem_type, evaluator->x_dot_loc));
  PetscCall(VecPetscToCeed(Y_loc, &y_mem_type, evaluator->y_loc));

  // Apply libCEED operator
  PetscCall(PetscLogEventBegin(evaluator->log_event_ceed, X, Y, X_loc, Y_loc));
  PetscCall(PetscLogGpuTimeBegin());
  PetscCallCeed(evaluator->ceed, CeedOperatorApply(evaluator->op, evaluator->x_loc, evaluator->y_loc, CEED_REQUEST_IMMEDIATE));
  PetscCall(PetscLogGpuTimeEnd());
  PetscCall(PetscLogEventEnd(evaluator->log_event_ceed, X, Y, X_loc, Y_loc));

  // Restore PETSc vectors
  PetscCall(VecReadCeedToPetsc(evaluator->x_loc, x_mem_type, X_loc));
  if (X_dot_loc) PetscCall(VecReadCeedToPetsc(evaluator->x_dot_loc, x_dot_mem_type, X_dot_loc));
  PetscCall(VecCeedToPetsc(evaluator->y_loc, y_mem_type, Y_loc));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Apply the local action of a `CeedOperator` and sum result into PETSc vector.

  Collective across MPI processes.

  @param[in]   evaluator  `CeedEvaluator`
  @param[in]   X          Input PETSc global vector, for logging only
  @param[in]   Y          Output PETSc global vector, for logging only
  @param[in]   X_loc      Input PETSc local vector
  @param[in]   X_dot_loc  Input PETSc local time derivative vector, or NULL if and only if X_dot is NULL
  @param[out]  Y_loc      Output PETSc local vector


  @return An error code: 0 - success, otherwise - failure
**/
static inline PetscErrorCode CeedEvaluatorApplyAdd_Core(CeedEvaluator evaluator, Vec X, Vec Y, Vec X_loc, Vec X_dot_loc, Vec Y_loc) {
  PetscMemType x_mem_type, x_dot_mem_type, y_mem_type;

  PetscFunctionBeginUser;
  // Setup libCEED vectors
  PetscCall(VecReadPetscToCeed(X_loc, &x_mem_type, evaluator->x_loc));
  if (X_dot_loc) PetscCall(VecReadPetscToCeed(X_dot_loc, &x_dot_mem_type, evaluator->x_dot_loc));
  PetscCall(VecPetscToCeed(Y_loc, &y_mem_type, evaluator->y_loc));

  // Apply libCEED operator
  PetscCall(PetscLogEventBegin(evaluator->log_event_ceed, X, Y, X_loc, Y_loc));
  PetscCall(PetscLogGpuTimeBegin());
  PetscCallCeed(evaluator->ceed, CeedOperatorApplyAdd(evaluator->op, evaluator->x_loc, evaluator->y_loc, CEED_REQUEST_IMMEDIATE));
  PetscCall(PetscLogGpuTimeEnd());
  PetscCall(PetscLogEventEnd(evaluator->log_event_ceed, X, Y, X_loc, Y_loc));

  // Restore PETSc vectors
  PetscCall(VecReadCeedToPetsc(evaluator->x_loc, x_mem_type, X_loc));
  if (X_dot_loc) PetscCall(VecReadCeedToPetsc(evaluator->x_dot_loc, x_dot_mem_type, X_dot_loc));
  PetscCall(VecCeedToPetsc(evaluator->y_loc, y_mem_type, Y_loc));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Apply the local action of a `CeedOperator` and store result in PETSc vector.

  Collective across MPI processes.

  @param[in]   evaluator  `CeedEvaluator`
  @param[in]   X          Input PETSc global vector
  @param[out]  Y          Output PETSc global vector

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedEvaluatorApplyGlobalToGlobal(CeedEvaluator evaluator, Vec X, Vec Y) {
  Vec X_loc = evaluator->X_loc, Y_loc = evaluator->Y_loc;

  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator, CEED_EVALUATOR_CLASSID, 1);
  PetscValidType(X, 2);
  PetscValidType(Y, 3);
  PetscCall(PetscLogEventBegin(evaluator->log_event, X, Y, NULL, NULL));
  PetscCall(VecZeroEntries(Y));

  // Get local vectors
  if (!evaluator->X_loc) PetscCall(DMGetLocalVector(evaluator->dm_x, &X_loc));
  if (!evaluator->Y_loc) PetscCall(DMGetLocalVector(evaluator->dm_y, &Y_loc));

  // Global-to-local
  PetscCall(DMGlobalToLocal(evaluator->dm_x, X, INSERT_VALUES, X_loc));

  // Zero target vector
  PetscCall(VecZeroEntries(Y_loc));

  // Sum into target vector
  PetscCall(CeedEvaluatorApply_Core(evaluator, X, Y, X_loc, NULL, Y_loc));

  // Local-to-global
  PetscCall(DMLocalToGlobal(evaluator->dm_y, Y_loc, ADD_VALUES, Y));

  // Restore local vectors, as needed
  if (!evaluator->X_loc) PetscCall(DMRestoreLocalVector(evaluator->dm_x, &X_loc));
  if (!evaluator->Y_loc) PetscCall(DMRestoreLocalVector(evaluator->dm_y, &Y_loc));

  PetscCall(PetscLogEventEnd(evaluator->log_event, X, Y, NULL, NULL));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Apply the local action of a `CeedOperator` and sum result into PETSc vector.

  Collective across MPI processes.

  @param[in]   evaluator  `CeedEvaluator`
  @param[in]   X          Input PETSc global vector
  @param[out]  Y          Output PETSc global vector

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedEvaluatorApplyAddGlobalToGlobal(CeedEvaluator evaluator, Vec X, Vec Y) {
  Vec X_loc = evaluator->X_loc, Y_loc = evaluator->Y_loc;

  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator, CEED_EVALUATOR_CLASSID, 1);
  PetscValidType(X, 2);
  PetscValidType(Y, 3);
  PetscCall(PetscLogEventBegin(evaluator->log_event, X, Y, NULL, NULL));

  // Get local vectors
  if (!evaluator->X_loc) PetscCall(DMGetLocalVector(evaluator->dm_x, &X_loc));
  if (!evaluator->Y_loc) PetscCall(DMGetLocalVector(evaluator->dm_y, &Y_loc));

  // Global-to-local
  PetscCall(DMGlobalToLocal(evaluator->dm_x, X, INSERT_VALUES, X_loc));

  // Zero target vector
  PetscCall(VecZeroEntries(Y_loc));

  // Sum into target vector
  PetscCall(CeedEvaluatorApplyAdd_Core(evaluator, X, Y, X_loc, NULL, Y_loc));

  // Local-to-global
  PetscCall(DMLocalToGlobal(evaluator->dm_y, Y_loc, ADD_VALUES, Y));

  // Restore local vectors, as needed
  if (!evaluator->X_loc) PetscCall(DMRestoreLocalVector(evaluator->dm_x, &X_loc));
  if (!evaluator->Y_loc) PetscCall(DMRestoreLocalVector(evaluator->dm_y, &Y_loc));

  PetscCall(PetscLogEventEnd(evaluator->log_event, X, Y, NULL, NULL));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Apply the local action of a velocity-dependent `CeedOperator` and store result in PETSc vector.

  Collective across MPI processes.

  @param[in]   evaluator  `CeedEvaluator`
  @param[in]   X          Input PETSc global vector
  @param[in]   X_dot      Input PETSc global time derivative vector
  @param[out]  Y          Output PETSc global vector

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedEvaluatorApplyVelocityGlobalToGlobal(CeedEvaluator evaluator, Vec X, Vec X_dot, Vec Y) {
  Vec X_loc = evaluator->X_loc, X_dot_loc = evaluator->X_dot_loc, Y_loc = evaluator->Y_loc;

  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator, CEED_EVALUATOR_CLASSID, 1);
  PetscValidType(X, 2);
  PetscValidType(X_dot, 3);
  PetscValidType(Y, 4);
  PetscCheck(X_dot, PetscObjectComm((PetscObject)evaluator->dm_x), PETSC_ERR_ARG_NULL, "Global X_dot required for velocity-dependent operator");
  PetscCall(PetscLogEventBegin(evaluator->log_event, X, X_dot, Y, NULL));

  // Get local vectors
  if (!evaluator->X_loc) PetscCall(DMGetLocalVector(evaluator->dm_x, &X_loc));
  if (!evaluator->X_dot_loc) PetscCall(DMGetLocalVector(evaluator->dm_x, &X_dot_loc));
  if (!evaluator->Y_loc) PetscCall(DMGetLocalVector(evaluator->dm_y, &Y_loc));

  // Global-to-local
  PetscCall(DMGlobalToLocalBegin(evaluator->dm_x, X, INSERT_VALUES, X_loc));
  PetscCall(DMGlobalToLocalBegin(evaluator->dm_x, X_dot, INSERT_VALUES, X_dot_loc));
  PetscCall(DMGlobalToLocalEnd(evaluator->dm_x, X, INSERT_VALUES, X_loc));
  PetscCall(DMGlobalToLocalEnd(evaluator->dm_x, X_dot, INSERT_VALUES, X_dot_loc));

  // Zero target vector
  PetscCall(VecZeroEntries(Y_loc));
  PetscCall(VecZeroEntries(Y));

  // Sum into target vector
  PetscCall(CeedEvaluatorApply_Core(evaluator, X, Y, X_loc, X_dot_loc, Y_loc));

  // Local-to-global
  PetscCall(DMLocalToGlobal(evaluator->dm_y, Y_loc, ADD_VALUES, Y));

  // Restore local vectors, as needed
  if (!evaluator->X_loc) PetscCall(DMRestoreLocalVector(evaluator->dm_x, &X_loc));
  if (!evaluator->X_dot_loc) PetscCall(DMRestoreLocalVector(evaluator->dm_x, &X_dot_loc));
  if (!evaluator->Y_loc) PetscCall(DMRestoreLocalVector(evaluator->dm_y, &Y_loc));
  PetscCall(PetscLogEventEnd(evaluator->log_event, X, X_dot, Y, NULL));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Apply the local action of a `CeedOperator` and store result in PETSc vector.

  Collective across MPI processes.

  @param[in]   evaluator  `CeedEvaluator`
  @param[in]   X          Input PETSc global vector
  @param[out]  Y_loc      Output PETSc local vector

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedEvaluatorApplyGlobalToLocal(CeedEvaluator evaluator, Vec X, Vec Y_loc) {
  Vec X_loc = evaluator->X_loc;

  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator, CEED_EVALUATOR_CLASSID, 1);
  PetscValidType(X, 2);
  PetscValidType(Y_loc, 3);
  PetscCall(PetscLogEventBegin(evaluator->log_event, X, Y_loc, NULL, NULL));

  // Get local vectors
  if (!evaluator->X_loc) PetscCall(DMGetLocalVector(evaluator->dm_x, &X_loc));

  // Global-to-local
  PetscCall(DMGlobalToLocal(evaluator->dm_x, X, INSERT_VALUES, X_loc));

  // Zero target vector
  PetscCall(VecZeroEntries(Y_loc));

  // Sum into target vector
  PetscCall(CeedEvaluatorApply_Core(evaluator, X, NULL, X_loc, NULL, Y_loc));

  // Restore local vectors, as needed
  if (!evaluator->X_loc) PetscCall(DMRestoreLocalVector(evaluator->dm_x, &X_loc));
  PetscCall(PetscLogEventEnd(evaluator->log_event, X, Y_loc, NULL, NULL));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Apply the local action of a `CeedOperator` and store result in PETSc vector.

  Collective across MPI processes.

  @param[in]   evaluator  `CeedEvaluator`
  @param[in]   X_loc      Input PETSc local vector
  @param[out]  Y_loc      Output PETSc local vector

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedEvaluatorApplyLocalToLocal(CeedEvaluator evaluator, Vec X_loc, Vec Y_loc) {
  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator, CEED_EVALUATOR_CLASSID, 1);
  PetscValidType(X_loc, 2);
  PetscValidType(Y_loc, 3);
  PetscCall(PetscLogEventBegin(evaluator->log_event, X_loc, Y_loc, NULL, NULL));
  PetscCall(VecZeroEntries(Y_loc));
  PetscCall(CeedEvaluatorApply_Core(evaluator, NULL, NULL, X_loc, NULL, Y_loc));
  PetscCall(PetscLogEventEnd(evaluator->log_event, X_loc, Y_loc, NULL, NULL));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Apply the local action of a `CeedOperator` and sum result into PETSc vector.

  Collective across MPI processes.

  @param[in]   evaluator  `CeedEvaluator`
  @param[in]   X_loc      Input PETSc local vector
  @param[out]  Y_loc      Output PETSc local vector

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedEvaluatorApplyAddLocalToLocal(CeedEvaluator evaluator, Vec X_loc, Vec Y_loc) {
  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator, CEED_EVALUATOR_CLASSID, 1);
  PetscValidType(X_loc, 2);
  PetscValidType(Y_loc, 3);
  PetscCall(PetscLogEventBegin(evaluator->log_event, X_loc, Y_loc, NULL, NULL));
  PetscCall(CeedEvaluatorApplyAdd_Core(evaluator, NULL, NULL, X_loc, NULL, Y_loc));
  PetscCall(PetscLogEventEnd(evaluator->log_event, X_loc, Y_loc, NULL, NULL));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get libCEED `CeedOperator` for `CeedEvaluator` operations.

  Not collective across MPI processes.

  @param[in,out]  evaluator  `CeedEvaluator`
  @param[out]     op         libCEED `CeedOperator`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedEvaluatorGetCeedOperator(CeedEvaluator evaluator, CeedOperator *op) {
  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator, CEED_EVALUATOR_CLASSID, 1);
  *op = NULL;
  PetscCallCeed(evaluator->ceed, CeedOperatorReferenceCopy(evaluator->op, op));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Restore libCEED `CeedOperator` for `CeedEvaluator` operations.

  Not collective across MPI processes.

  @param[in,out]  evaluator  `CeedEvaluator`
  @param[out]     op         libCEED `CeedOperator`

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedEvaluatorRestoreCeedOperator(CeedEvaluator evaluator, CeedOperator *op) {
  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator, CEED_EVALUATOR_CLASSID, 1);
  PetscCallCeed(evaluator->ceed, CeedOperatorDestroy(op));
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Set `PetscLogEvent` for `CeedEvaluator`.

  Not collective across MPI processes.

  @param[in,out]  evaluator  `CeedEvaluator`
  @param[out]     log_event  `PetscLogEvent` for evaluation

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedEvaluatorSetLogEvent(CeedEvaluator evaluator, PetscLogEvent log_event) {
  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator, CEED_EVALUATOR_CLASSID, 1);
  evaluator->log_event = log_event;
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get `PetscLogEvent` for `CeedEvaluator`.

  Not collective across MPI processes.

  @param[in,out]  evaluator  `CeedEvaluator`
  @param[out]     log_event  `PetscLogEvent` for evaluation

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedEvaluatorGetLogEvent(CeedEvaluator evaluator, PetscLogEvent *log_event) {
  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator, CEED_EVALUATOR_CLASSID, 1);
  *log_event = evaluator->log_event;
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Set `CeedOperator` `PetscLogEvent` for `CeedEvaluator`.

  Not collective across MPI processes.

  @param[in,out]  evaluator  `CeedEvaluator`
  @param[out]     log_event  `PetscLogEvent` for `CeedOperator` evaluation

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedEvaluatorSetCeedOperatorLogEvent(CeedEvaluator evaluator, PetscLogEvent log_event) {
  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator, CEED_EVALUATOR_CLASSID, 1);
  evaluator->log_event_ceed = log_event;
  PetscFunctionReturn(PETSC_SUCCESS);
}

/**
  @brief Get `CeedOperator` `PetscLogEvent` for `CeedEvaluator`.

  Not collective across MPI processes.

  @param[in,out]  evaluator  `CeedEvaluator`
  @param[out]     log_event  `PetscLogEvent` for `CeedOperator` evaluation

  @return An error code: 0 - success, otherwise - failure
**/
PetscErrorCode CeedEvaluatorGetCeedOperatorLogEvent(CeedEvaluator evaluator, PetscLogEvent *log_event) {
  PetscFunctionBeginUser;
  PetscValidHeaderSpecific(evaluator, CEED_EVALUATOR_CLASSID, 1);
  *log_event = evaluator->log_event_ceed;
  PetscFunctionReturn(PETSC_SUCCESS);
}
