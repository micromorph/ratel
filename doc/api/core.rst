Core Ratel Functions
**************************************

These functions are publicly exposed for users.

.. doxygengroup:: RatelCore
   :project: Ratel
   :path: ../../../../xml
   :content-only:
   :members:
