**************************************
API Documentation
**************************************

The Ratel API sets up a PETSc `DM <https://petsc.org/release/docs/manualpages/DM/DM/>`_ object with the data to run a `SNES <https://petsc.org/release/docs/manual/snes/>`_ or `TS <https://petsc.org/release/docs/manual/ts/>`_ solver for solid mechanics problems.
This `DM <https://petsc.org/release/docs/manualpages/DM/DM/>`_ is set for a `SNES <https://petsc.org/release/docs/manual/snes/>`_ or `TS <https://petsc.org/release/docs/manual/ts/>`_ via `SNESSetDM <https://petsc.org/release/docs/manualpages/SNES/SNESSetDM/>`_ or `TSSetDM <https://petsc.org/release/docs/manualpages/TS/TSSetDM/>`_ to provide a complete solver for a solid mechanics problem.

The API also includes functions to set sensible defaults for the `TS <https://petsc.org/release/docs/manual/ts/>`_, `SNES <https://petsc.org/release/docs/manual/snes/>`_, and `KSP <https://petsc.org/release/docs/manual/ksp/>`_ as well as set up geometric multigrid with `PCMG <https://petsc.org/release/docs/manualpages/PC/PCMG/>`_.
The default options depend upon the :c:enum:`RatelSolverType`.

Lastly, the Ratel API includes convenience functions to facilitate computing diagnostic quantities of interest.
These quantities of interest can be monitored on every time step via command line options or computed at the end of the solve with the convenience functions.

Ratel Public API
======================================

The public API provides core functionality for users.

.. toctree::

   core

Ratel Internal API
======================================

The internal API builds composite `CeedOperator <https://libceed.org/en/latest/api/CeedOperator/>`_ for the residual, Jacobian, and diagnostic quantity evaluation.
These composite `CeedOperator <https://libceed.org/en/latest/api/CeedOperator/>`_ are used to set the appropriate `DMSNES <https://petsc.org/release/docs/manualpages/SNES/DMGetDMSNES/>`_ or `DMTS <https://petsc.org/release/docs/manualpages/TS/DMGetDMTS/>`_ options, depending upon the :c:enum:`RatelSolverType`.

.. figure:: ../img/internal-api.png
  :alt: Ratel internal API

The internal API is organized around :c:type:`RatelMaterial`, separate material regions in the domain.
See the :ref:`Using Ratel section<multiple-materials>` of the documentation for details on how the user specifies a material region with model parameters for the mesh.

Each :c:type:`RatelMaterial` is responsible for adding volumetric terms to the residual, Jacobian, and diagnostic quantity evaluation operators as well as any surface terms that require volumetric or material model values.
Additionally, each :c:type:`RatelMaterial` is responsible for building and modifying corresponding preconditioner components, as needed.

Boundary and forcing terms that do not require volumetric or material model parameters, such as Dirichlet boundary conditions, are handled separately from a :c:type:`RatelMaterial`.

New :c:type:`RatelMaterial` are added by creating a file `src/materials/new-material.c` with the material model data, material creation function, and material registration function.
Any new QFunction source files go in `include/ratel/qfunctions`, and the material is added to `src/materials/ratel-model-list.h`.

Any new `CeedQFunction` created for a :c:type:`RatelMaterial` must list their `CeedQFunction` inputs and outputs in the following order, omitting any inputs or outputs that are not applicable:

Inputs:
  * Quadrature point data, such as integration weights and change of coordinate matrices
  * State data, such as plastic state
  * Stored values, such as residual evaluation data stored for reuse in the Jacobian
  * Model variables and their derivatives, as needed
  * Additional inputs as needed by the specific `CeedQFunction`

Outputs:
  * Current state data
  * Stored values for material model
  * Action of the operator the QFunction represents
  * Additional outputs as needed by the specific `CeedQFunction`

The :c:type:`RatelModel` associated with the :c:type:`RatelMaterial` can specify the following `CeedQFunction`
  * `setup_q_data_volume` - volumetric quadrature data setup
  * `setup_q_data_surface` - surface quadrature data for integration setup
  * `setup_q_data_surface_grad` - surface quadrature data for gradients setup
  * `residual_u` - nonlinear residual evaluation
  * `residual_ut` - nonlinear residual evaluation time derivative term
  * `residual_utt` - nonlinear residual evaluation time second derivative term
  * `jacobian` - linear Jacobian evaluation
  * `jacobian_block` - block suboperators by field for Jacobian evaluation
  * `strain_energy` - strain energy evaluation
  * `projected_diagnostic` - diagnostic values projected from quadrature space to the dual space
  * `dual_diagnostic` - diagnostic values computed on the dual space
  * `platen_residual_u` - residual evaluation for platen boundary conditions
  * `platen_jacobian` - Jacobian evaluation for platen boundary conditions
  * `mms_boundary` - MMS Dirichlet boundary values
  * `mms_forcing` - MMS forcing term
  * `mms_error` - MMS error against true solution
  * `surface_force_cell_to_face` - state in cells used to evaluate strain and stresses on the face, stress integrated to get forces on the surface

Numerical solvers
--------------------------------------

These functions setup the numerical solver and preconditioner.

.. toctree::

   solver

Material models
--------------------------------------

These functions setup the material regions in the mesh.

.. toctree::

   material


Boundary conditions
--------------------------------------

These functions apply boundary conditions.

.. toctree::

   boundary


Internal functions
--------------------------------------

These functions are internal setup and application functions.

.. toctree::

   internal
