Numerical Solvers
**************************************

The :c:enum:`RatelSolverType` determines how the composite `CeedOperator <https://libceed.org/en/latest/api/CeedOperator/>`_ are build and used to set the appropriate `DMSNES <https://petsc.org/release/docs/manualpages/SNES/DMGetDMSNES/>`_ or `DMTS <https://petsc.org/release/docs/manualpages/TS/DMGetDMTS/>`_ options.

.. doxygengroup:: RatelSolvers
   :project: Ratel
   :path: ../../../../xml
   :content-only:
   :members:
