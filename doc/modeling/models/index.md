(constitutive-modeling)=

# Constitutive Modeling

In their most general form, constitutive models define stress, $\bm{S}$ ($\bm{\tau}$ in Eulerian coordinate), in terms of state variables.
For the materials used in Ratel, the state variables are constituted by the vector displacement field $\bm{u}$, and its gradient $\bm{H} = \nabla_X \bm{u}$.
We consider constitutive models for stress in terms of strain due to deformation, given by $\bm{S} \left( \bm{E} \right)$ ($\bm{\tau} \left( \bm{e} \right) $ in Eulerian coordinate), which is a tensor-valued function of a tensor-valued input.

A physical model must not depend on the coordinate system chosen to express it; an arbitrary choice of function will generally not be invariant under orthogonal transformations and thus will not admissible.
Given an orthogonal transformation $Q$, we desire

$$
  Q \bm{S} \left( \bm{E} \right) Q^T = \bm{S} \left( Q \bm{E} Q^T \right),
$$ (elastic-invariance)

which means that we can change our reference frame before or after computing $\bm{S}$ and get the same result.
Constitutive relations in which $\bm{S}$ is uniquely determined by $\bm{E}$ while satisfying the invariance property {eq}`elastic-invariance` are known as Cauchy elastic materials.


The strain energies may be expressed in terms of invariants of the symmetric Cauchy-Green tensors $\bm{C}$ or $\bm{b}$

$$
  \begin{aligned}
    \mathbb{I}_1 (\bm{C}) &= \trace \bm{C}, \\
    \mathbb{I}_2 (\bm{C}) &= \frac 1 2 \left( \mathbb{I}_1^2 - \bm{C} \tcolon \bm{C} \right) \\
    \mathbb{I}_3 (\bm{C}) &= \operatorname{det} \bm{C},
  \end{aligned}
$$ (invariants)

Since $\bm{C}$ and $\bm{b}$ have the same eigenvalues, which are the square of the principal stretches $\lambda_i^2$, we conclude that $\mathbb{I}_i (\bm{C}) = \mathbb{I}_i (\bm{b}), \, i=1,2,3$.
For the strain energy function $\psi \left( \bm{C} \right) = \psi \left( \mathbb{I}_1, \mathbb{I}_2, \mathbb{I}_3 \right)$ we can determine the constitutive equations for isotopic hyperelastic materials by taking the gradient of strain energy as

$$
  \bm{S} = \frac{\partial \psi}{\partial \bm{E}}= 2 \frac{\partial \psi}{\partial \bm{C}} = 2 \sum_{i=1}^3 \frac{\partial \psi}{\partial \mathbb{I}_i} \frac{\partial \mathbb{I}_i}{\partial \bm{C}}
$$ (strain-energy-grad)

where $\bm{S}$ is the general form of the stress relation in initial configuration and the derivative of invariants are

$$
  \begin{aligned}
    \frac{\partial \mathbb{I}_1}{\partial \bm{C}} &= \bm{I}, \\
    \frac{\partial \mathbb{I}_2}{\partial \bm{C}} &=  \mathbb{I}_1 \bm{I} - \bm{C}\\
    \frac{\partial \mathbb{I}_3}{\partial \bm{C}} &= \mathbb{I}_3 \bm{C}^{-1},
  \end{aligned}
$$ (invariants-derivative)

The symmetric Kirchhoff stress tensor in current configuration $\bm{\tau}$ can be obtained by representing strain energy in terms of $\bm{b}$'s invariants and taking the gradient as

$$
  \bm{\tau}= \frac{\partial \psi}{\partial \bm{e}} \bm{b} = 2 \frac{\partial \psi}{\partial \bm{b}} \bm{b}
$$ (strain-energy-grad-current)

or we can push-forward $\bm{S}$ using {eq}`S-to-tau` to define $\bm{\tau}$.

The strain energy function may be written in terms of principal stretches $\lambda_i$ as $\psi \left( \bm{C} \right) = \psi \left( \lambda_1, \lambda_2, \lambda_3 \right)$ where we can define the constitutive relations by

$$
  \bm{S} = 2 \frac{\partial \psi}{\partial \bm{C}} = 2 \sum_{i=1}^3 \frac{\partial \psi}{\partial \lambda_i^2} \frac{\partial \lambda_i^2}{\partial \bm{C}} = \sum_{i=1}^3 S_i \hat{\bm{N}_i} \hat{\bm{N}_i}^T
$$ (strain-energy-grad-stretch)

and push-forward $\bm{S}$

$$
  \bm{\tau} = \bm{F} \bm{S} \bm{F}^T = \sum_{i=1}^3 \tau_i \hat{\bm{n}_i} \hat{\bm{n}_i}^T
$$ (strain-energy-grad-stretch-current)

where $S_i$ and $\tau_i$ are three principal stresses given by

$$
  \begin{aligned}
    S_i &= \frac{1}{\lambda_i} \frac{\partial \psi}{\partial \lambda_i}, & \tau_i &= {\lambda_i} \frac{\partial \psi}{\partial \lambda_i}.
  \end{aligned}
$$ (principal-stresses)

where we have used

$$
  \begin{aligned}
    \frac{\partial \lambda_i^2}{\partial \bm{C}} &= \hat{\bm{N}_i} \hat{\bm{N}_i}^T, & \frac{\partial \psi}{\partial \lambda_i^2} &= \frac{1}{2\lambda_i} \frac{\partial \psi}{\partial \lambda_i}.
  \end{aligned}
$$ (stretch-derivative)

Note that the invariants can be written in terms of principal stretch as

$$
  \begin{aligned}
    \mathbb{I}_1 (\bm{C}) &= \lambda_1^2 + \lambda_2^2 + \lambda_3^2, \\
    \mathbb{I}_2 (\bm{C}) &= \lambda_1^2 \lambda_2^2 + \lambda_2^2 \lambda_3^2 + \lambda_1^2 \lambda_3^2, \\
    \mathbb{I}_3 (\bm{C}) &= \lambda_1^2 \lambda_2^2 \lambda_3^2,
  \end{aligned}
$$ (invariants-stretch)


:::{note}
The strain energy density functional cannot be an arbitrary function $\psi \left( \bm{E} \right)$.
It can only depend on *invariants*, scalar-valued functions $\gamma$ satisfying

$$
  \gamma \left( \bm{E} \right) = \gamma \left( Q \bm{E} Q^T \right)
$$

for all orthogonal matrices $Q$.
:::

## Isochoric-split

For the incompressible and nearly incompressible materials where the motions are constraint by spatial conditions, it is most beneficial to split the deformation locally into a so-called volumetric (dilational) part and an isochoric (distortional) part.
In order to be able to define strain energy functions that are separable into volumetric and isochoric parts, we decompose the deformation gradient in a multiplicative sense by

$$
  \bm{F} = (J^{1/3} \bm{I}) \bar{\bm{F}} = J^{1/3} \bar{\bm{F}}
$$ (modified-deformation-gradient)

where $J^{1/3} \bm{I}$ describes the purely volumetric deformation and $\bar{\bm{F}}$ captures the isochoric or volume-preserving deformation since $\operatorname{det} \bar{\bm{F}}  = \operatorname{det} J^{-1/3} \bm{F}  = 1$ .
Similar decomposition can be obtained for other tensor such as the right Cauchy-Green tensor $\bm{C}$ as

$$
  \bar{\bm{C}} = \bar{\bm{F}}^T \bar{\bm{F}} = J^{-2/3} \bm{C}
$$ (modified-right-cauchy-green)

where we call $\bar{\bm{F}}$ and $\bar{\bm{C}}$ the modified deformation gradient and modified right Cauchy-Green tensor, respectively.
The modified principal stretches $\bar{\lambda}_i$ and modified invariants $\mathbb{\bar{I}}_i$ are

$$
  \bar{\lambda}_i = J^{-1/3} \lambda_i, \, i=1,2,3
$$ (modified-stretch)

$$
  \begin{aligned}
    \mathbb{\bar{I}}_1 &= J^{-2/3}\mathbb{I}_1, & \mathbb{\bar{I}}_2 &= J^{-4/3}\mathbb{I}_2, & \mathbb{\bar{I}}_3 &= \left(J^{-2/3}\right)^3 \mathbb{I}_3 = 1.
  \end{aligned}
$$ (modified-invariants)

For the strain energy in terms of modified invariants $\psi \left( \bar{\bm{C}} \right) = \psi \left( \mathbb{\bar{I}}_1, \mathbb{\bar{I}}_2, \mathbb{\bar{I}}_3 \right)$ we can derive the stress relations as

$$
  \bm{S} = 2 \frac{\partial \psi}{\partial \bm{C}} = 2 \sum_{i=1}^3 \frac{\partial \psi}{\partial \mathbb{\bar{I}}_i} \frac{\partial \mathbb{\bar{I}}_i}{\partial \bm{C}}
$$ (strain-energy-grad-isochoric)

where the derivative of modified invariants are

$$
  \begin{aligned}
    \frac{\partial \mathbb{\bar{I}}_1}{\partial \bm{C}} &= \frac{\partial (J^{-2/3}\mathbb{I}_1)}{\partial \bm{C}} = J^{-2/3} \left( \bm{I} -\frac{1}{3} \mathbb{I}_1 \bm{C}^{-1}\right), \\
    \frac{\partial \mathbb{\bar{I}}_2}{\partial \bm{C}} &=  \frac{\partial (J^{-4/3}\mathbb{I}_2)}{\partial \bm{C}} = J^{-4/3} \left( \mathbb{I}_1 \bm{I} - \bm{C} - \frac{2}{3} \mathbb{I}_2 \bm{C}^{-1}\right), \\
    \frac{\partial \mathbb{\bar{I}}_3}{\partial \bm{C}} &= 0,
  \end{aligned}
$$ (modified-invariants-derivative)

where we have used

$$
  \begin{aligned}
    \frac{\partial J}{\partial \bm{C}} &= \frac{1}{2} J \bm{C}^{-1}, & \frac{\partial J^{-2/3}}{\partial \bm{C}} &= -\frac{1}{3} J^{-2/3} \bm{C}^{-1}.
  \end{aligned}
$$ (derivative-J-wrt-C)


Note that the derivatives above can be written with respect to $\bm{E}$ by use of chain rule and $\bm{C} = 2\bm{E} + \bm{I}$ as


$$
  \frac{\partial }{\partial \bm{E}} = \frac{\partial }{\partial \bm{C}} \frac{\partial C}{\partial \bm{E}} = 2 \frac{\partial }{\partial \bm{C}}.
$$ (derivative-wrt-E)

## Material Models

In the following sections we introduce the most common constitutive models in the literature.

```{toctree}
:maxdepth: 3

Linear elasticity <linear-elasticity>
Neo-Hookean <neo-hookean>
Neo-Hookean Isochoric-split <neo-hookean-isochoric>
Mooney-Rivlin <mooney-rivlin>
Mooney-Rivlin Isochoric-split <mooney-rivlin-isochoric>
Ogden Isochoric-split <ogden-isochoric>
Poroelasticity <poroelasticity>
```
