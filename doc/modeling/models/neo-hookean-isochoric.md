# Neo-Hookean Isochoric-split

The Neo-Hookean decoupled strain energy function is in terms of $J$ and modified invariant $\mathbb{\bar{I}}_1$

$$
  \begin{aligned}
    \psi \left(\bm{C} \right) &= \psi_{\text{vol}}(J) + \psi_{\text{iso}}(\bar{\bm{C}})\\
    &=\bulk \, V(J) + \frac \secondlame 2 \left( \mathbb{\bar{I}_1} - 3 \right).
  \end{aligned}
$$ (mixed-neo-hookean-energy)

where $\bulk$ is the bulk modulus of the material and $V(J)$ is a strictly convex function.

One approach to obtain mixed formulation is to consider the hydrostatic pressure $-\frac{\trace \bm \sigma}{3}$ as a variable, where in terms of energy is

$$
  \begin{aligned}
      p_{\text{hyd}} = - \frac{\trace \bm \sigma}{3} &= -\frac{1}{3J} \trace (\bm F \bm S \bm{F}^T) = -\frac{1}{3J} \trace (\bm S \bm F^T \bm F) = -\frac{1}{3J} \trace (\bm S \bm C) \\
                    &= -\frac{1}{3J} \trace \left( \frac{\partial \psi_{\text{vol}}}{\partial J} ~ J \bm{I} + \sum_{i=1}^3 \frac{\partial \psi_{\text{iso}}}{\partial \mathbb{\bar{I}}_i} \frac{\partial \mathbb{\bar{I}}_i}{\partial \bm E} \bm C \right) \\
                    &= - \frac{\partial \psi_{\text{vol}}}{\partial J} = -\bulk \frac{\partial V(J)}{\partial J} = -\bulk V',
  \end{aligned}
$$ (hydrostatic-pressure)

where we have used $\trace (\bm A \bm B) = \trace (\bm B \bm A)$ and

$$
    \begin{aligned}
        \trace \left( \frac{\partial \mathbb{\bar{I}}_1}{\partial \bm E} \bm C \right) &= 2 J^{-2/3} \trace\left( \bm C -\frac{1}{3} \mathbb{I}_1 \bm{I} \right) = 0, \\
        \trace \left( \frac{\partial \mathbb{\bar{I}}_2}{\partial \bm E} \bm C \right) &= 2 J^{-4/3} \trace \left( \mathbb{I}_1 \bm C - \bm C^2 - \frac{2}{3} \mathbb{I}_2 \bm{I} \right) = 0, \\
        \trace \left( \frac{\partial \mathbb{\bar{I}}_3}{\partial \bm E} \bm C \right) &= 0.
    \end{aligned}
$$

However, we define a pressure-like variable similar to mixed linear elasticity {eq}`general-mixed-linear-constitutive` in terms of bulk and primal bulk {eq}`primal-bulk` moduli

$$
   p = - (\bulk - \bulk_p) \frac{\partial V}{\partial J} = p_{\text{hyd}} + \bulk_p \frac{\partial V}{\partial J},
$$ (general-pressure-like)

and considered it as independent field variable in our mixed hyperelastic formulation.

The second Piola-Kirchoff tensor is computed via {eq}`strain-energy-grad-isochoric`

$$
  \begin{aligned}
    \bm{S} = \frac{\partial \psi}{\partial \bm{E}} &=  \underbrace{\frac{\partial \psi_{\text{vol}}}{\partial J}}_{-p_{\text{hyd}}}  \frac{\partial J}{\partial \bm{E}} +  \frac{\partial \psi}{\partial \mathbb{\bar{I}_1}}  \frac{\partial \mathbb{\bar{I}_1}}{\partial \bm{E}} =\bm{S}_{\text{vol}} + \bm{S}_{\text{iso}}\\
    &= \left(\bulk_p J \, V' - p \, J \right) \bm{C}^{-1} + \secondlame J^{-2/3}\left( \bm{I} - \frac{1}{3}\mathbb{{I}_1}\bm{C}^{-1} \right).
  \end{aligned}
$$ (mixed-neo-hookean-stress)

where the invariants are defined in {eq}`invariants` and {eq}`modified-invariants` and we have used {eq}`derivative-J-wrt-C`, {eq}`modified-invariants-derivative`, and {eq}`derivative-wrt-E`.

:::{tip}
In our simulation we use the stable version of {eq}`mixed-neo-hookean-stress` as

$$
\bm{S} = \left(\bulk_p J \, V' - p \, J \right) \bm{C}^{-1} + 2 \secondlame J^{-2/3} \bm{C}^{-1} \bm{E}_{\text{dev}},
$$ (mixed-neo-hookean-stress-stable)

where $\bm{E}_{\text{dev}} = \bm{E} - \frac{1}{3}\trace \bm{E} \, \bm{I} $ is the deviatoric part of Green-Lagrange strain tensor.

:::

The isochoric Neo-Hookean stress relation can be represented in current configuration by pushing forward {eq}`mixed-neo-hookean-stress` using {eq}`S-to-tau`

$$
  \bm{\tau} = \bm{F}\bm{S}\bm{F}^T = \left(\bulk_p J \, V' - p \, J \right) \bm{I} + \secondlame J^{-2/3}\left( \bm{b} - \frac{1}{3}\mathbb{{I}_1} \bm{I} \right).
$$ (mixed-neo-hookean-tau)

:::{tip}
In our simulation we use the stable version of Kirchhoff stress tensor {eq}`mixed-neo-hookean-tau` as

$$
\bm{\tau} = \left(\bulk_p J \, V' - p \, J \right) \bm{I} + 2 \secondlame J^{-2/3} \bm{e}_{\text{dev}}
$$ (mixed-neo-hookean-tau-stable)

where $\bm{e}_{\text{dev}} = \bm{e} - \frac{1}{3}\trace \bm{e} \, \bm{I} $ is the deviatoric part of Green-Euler strain tensor

:::

:::{note}
We can solve the isochoric model with single field displacement if the material is *not* incompressible which leads to the second Piola-Kirchoff tensor

$$
  \begin{aligned}
    \bm{S} = \frac{\partial \psi}{\partial \bm{E}} &=  \frac{\partial \psi_{\text{vol}}}{\partial J}  \frac{\partial J}{\partial \bm{E}} +  \frac{\partial \psi}{\partial \mathbb{\bar{I}_1}}  \frac{\partial \mathbb{\bar{I}_1}}{\partial \bm{E}} =\bm{S}_{\text{vol}} + \bm{S}_{\text{iso}}\\
    &= \bulk J \, V' \, \bm{C}^{-1} + \secondlame J^{-2/3}\left( \bm{I} - \frac{1}{3}\mathbb{{I}_1}\bm{C}^{-1} \right).
  \end{aligned}
$$ (isochoric-neo-hookean-stress)

and the kirchoff stress

$$
  \bm{\tau} = \bm{F}\bm{S}\bm{F}^T = \bulk J \, V' \, \bm{I} + \secondlame J^{-2/3}\left( \bm{b} - \frac{1}{3}\mathbb{{I}_1} \bm{I} \right).
$$ (isochoric-neo-hookean-tau)

Note that the stable form of the above stresses can be derived similar to {eq}`mixed-neo-hookean-stress-stable`, and {eq}`mixed-neo-hookean-tau-stable`.

:::