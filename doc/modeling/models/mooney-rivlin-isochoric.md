# Mooney-Rivlin Isochoric-split

The Mooney-Rivlin decoupled strain energy function is in terms of $J$ and modified invariants $\mathbb{\bar{I}}_1, \mathbb{\bar{I}}_2$ {eq}`modified-invariants`

$$
  \begin{aligned}
    \psi \left(\bm{C} \right) &= \psi_{\text{vol}}(J) + \psi_{\text{iso}}(\bar{\bm{C}})\\
    &=\bulk \, V(J) + \frac{\secondlame_1}{2} \left( \mathbb{\bar{I}_1} - 3 \right) + \frac{\secondlame_2}{2} \left( \mathbb{\bar{I}_2} - 3 \right).
  \end{aligned}
$$ (mixed-mooney-rivlin-energy)

where $\bulk$ is the bulk modulus of the material and $V(J)$ is a strictly convex function.

The second Piola-Kirchoff tensor is computed via {eq}`strain-energy-grad-isochoric`

$$
  \begin{aligned}
    \bm{S} = \frac{\partial \psi}{\partial \bm{E}} &=  \frac{\partial \psi_{\text{vol}}}{\partial J}  \frac{\partial J}{\partial \bm{E}} +  \frac{\partial \psi}{\partial \mathbb{\bar{I}_1}}  \frac{\partial \mathbb{\bar{I}_1}}{\partial \bm{E}} + \frac{\partial \psi}{\partial \mathbb{\bar{I}_2}}  \frac{\partial \mathbb{\bar{I}_2}}{\partial \bm{E}} = \bm{S}_{\text{vol}} + \bm{S}_{\text{iso}}\\
    &= \left(\bulk_p J \, V' - p \, J \right) \bm{C}^{-1} + \secondlame_1 J^{-2/3}\left( \bm{I} - \frac{1}{3}\mathbb{{I}_1}\bm{C}^{-1} \right) + \secondlame_2 J^{-4/3}\left( \mathbb{{I}_1} \bm{I} - \bm{C} - \frac{2}{3}\mathbb{{I}_2}\bm{C}^{-1} \right).
  \end{aligned}
$$ (mixed-mooney-rivlin-stress)

where the invariants are defined in {eq}`invariants` and {eq}`modified-invariants` and we have used {eq}`derivative-J-wrt-C`, {eq}`modified-invariants-derivative`, and {eq}`derivative-wrt-E`.

The pressure-like variable $p$ in {eq}`mixed-mooney-rivlin-stress` is defined similar to {eq}`general-pressure-like`.

:::{tip}
Similar to Neo-Hookean isochoric model {eq}`mixed-neo-hookean-stress-stable` we can derive a stable form of {eq}`mixed-mooney-rivlin-stress` as

$$
\begin{aligned}
  \bm{S} &= \left(\bulk_p J \, V' - p \, J \right) \bm{C}^{-1} + \left( 2 \secondlame_1 J^{-2/3} + 4 \secondlame_2 J^{-4/3} \right) \bm{C}^{-1} \bm{E}_{\text{dev}} \\
  &+ 2 \secondlame_2 J^{-4/3} \left( \mathbb{{I}_1}(\bm{E})\bm{I} - \bm{E} \right) - \frac{4}{3} \secondlame_2 J^{-4/3} \left( \mathbb{{I}_1}(\bm{E}) + 2 \mathbb{{I}_2}(\bm{E})\right) \bm{C}^{-1},
\end{aligned}
$$ (mixed-mooney-rivlin-stress-stable)

where $\bm{E}_{\text{dev}} = \bm{E} - \frac{1}{3}\trace \bm{E} \, \bm{I} $ is the deviatoric part of Green-Lagrange strain tensor and $\mathbb{{I}_1}(\bm{E}), \mathbb{{I}_2}(\bm{E})$ are first and second invariants of the tensor $\bm{E}$.

:::

The isochoric Mooney-Rivlin stress relation can be represented in current configuration by pushing forward {eq}`mixed-mooney-rivlin-stress` using {eq}`S-to-tau`

$$
  \bm{\tau} = \bm{F}\bm{S}\bm{F}^T = \left(\bulk_p J \, V' - p \, J \right) \bm{I} + \secondlame_1 J^{-2/3}\left( \bm{b} - \frac{1}{3}\mathbb{{I}_1} \bm{I} \right) +  \secondlame_2 J^{-4/3}\left( \mathbb{{I}_1} \bm{b} - \bm{b}^2 - \frac{2}{3}\mathbb{{I}_2} \bm{I} \right)
$$ (mixed-mooney-rivlin-tau)

:::{tip}
In our simulation we use the stable version of Kirchhoff stress tensor {eq}`mixed-mooney-rivlin-tau` as

$$
\begin{aligned}
  \bm{\tau} &= \left(\bulk_p J \, V' - p \, J \right) \bm{I} + \left( 2 \secondlame_1 J^{-2/3} + 4 \secondlame_2 J^{-4/3} \right) \bm{e}_{\text{dev}} \\
  &+ 2 \secondlame_2 J^{-4/3} \left( \mathbb{{I}_1}(\bm{e})\bm{I} - \bm{e} \right) \bm{b} - \frac{4}{3} \secondlame_2 J^{-4/3} \left( \mathbb{{I}_1}(\bm{e}) + 2 \mathbb{{I}_2}(\bm{e})\right) \bm{I},
\end{aligned}
$$ (mixed-mooney-rivlin-tau-stable)

where $\bm{e}_{\text{dev}} = \bm{e} - \frac{1}{3}\trace \bm{e} \, \bm{I} $ is the deviatoric part of Green-Euler strain tensor and $\mathbb{{I}_1}(\bm{e}), \mathbb{{I}_2}(\bm{e})$ are first and second invariants of the tensor $\bm{e}$.

:::

:::{note}
We can solve the isochoric model with single field displacement if the material is *not* incompressible which leads to the second Piola-Kirchoff tensor

$$
  \begin{aligned}
    \bm{S} = \frac{\partial \psi}{\partial \bm{E}} &=  \frac{\partial \psi_{\text{vol}}}{\partial J}  \frac{\partial J}{\partial \bm{E}} +  \frac{\partial \psi}{\partial \mathbb{\bar{I}_1}}  \frac{\partial \mathbb{\bar{I}_1}}{\partial \bm{E}} + \frac{\partial \psi}{\partial \mathbb{\bar{I}_2}}  \frac{\partial \mathbb{\bar{I}_2}}{\partial \bm{E}} = \bm{S}_{\text{vol}} + \bm{S}_{\text{iso}}\\
    &= \bulk J \, V' \,\bm{C}^{-1} + \secondlame_1 J^{-2/3}\left( \bm{I} - \frac{1}{3}\mathbb{{I}_1}\bm{C}^{-1} \right) + \secondlame_2 J^{-4/3}\left( \mathbb{{I}_1} \bm{I} - \bm{C} - \frac{2}{3}\mathbb{{I}_2}\bm{C}^{-1} \right).
  \end{aligned}
$$ (isochoric-mooney-rivlin-stress)

and the kirchoff stress

$$
  \bm{\tau} = \bulk J \, V' \, \bm{I} + \secondlame_1 J^{-2/3}\left( \bm{b} - \frac{1}{3}\mathbb{{I}_1} \bm{I} \right) +  \secondlame_2 J^{-4/3}\left( \mathbb{{I}_1} \bm{b} - \bm{b}^2 - \frac{2}{3}\mathbb{{I}_2} \bm{I} \right)
$$ (isochoric-mooney-rivlin-tau)

Note that the stable form of the above stresses can be derived similar to {eq}`mixed-mooney-rivlin-stress-stable`, and {eq}`mixed-mooney-rivlin-tau-stable`.

:::