# Ogden Isochoric-split

The strain energy of Neo-Hookean and Mooney-Rivlin are written in terms of invariants of $\bm{C}$. The decoupled Ogden strain energy function is in terms of modified principal stretch {eq}`modified-stretch`

$$
  \begin{aligned}
    \psi \left(\bar{\lambda}_1, \bar{\lambda}_2, \bar{\lambda}_3, J \right) &= \psi_{\text{vol}}(J) + \psi_{\text{iso}}\left( \bar{\lambda}_1, \bar{\lambda}_2, \bar{\lambda}_3 \right)\\
    &=\bulk \, V(J) + \sum_{i=1}^3 \omega(\bar{\lambda}_i).
  \end{aligned}
$$ (mixed-ogden-energy)

where

$$
  \omega(\bar{\lambda}_i) = \sum_{j=1}^N \frac{m_j}{\alpha_j} \left(\bar{\lambda}_i^{\alpha_j} -1 \right)
$$ (ogden-omega)

with the consistency condition

$$
  2 \secondlame = \sum_{j=1}^N m_j \alpha_j \quad \text{with} \quad m_j \alpha_j > 0
$$ (consistency-condition)

obtained from comparison with the linear theory.

:::{tip}
For $N=1, \alpha_1 = 2, m_1 = \secondlame$ the Ogden model {eq}`mixed-ogden-energy` simplifies to

$$
\psi \left( \bar{\lambda}_1, \bar{\lambda}_2, \bar{\lambda}_3, J \right) = \frac{\bulk}{4} \left( J^2 - 1 -2 \log J \right) + \frac{\secondlame}{2} \left(\bar{\lambda}_1^2 + \bar{\lambda}_2^2 + \bar{\lambda}_3^2 - 3\right)
$$

which is the same as the decoupled Neo-Hookean energy function {eq}`mixed-neo-hookean-energy`. Also for $J=1, N=2, \alpha_1 = 2, \alpha_2 = -2, m_1 = \secondlame_1, m_2 = -\secondlame_2$ and by using $J=1=\lambda_1 \lambda_2 \lambda_3 = \bar{\lambda}_1 \bar{\lambda}_2 \bar{\lambda}_3$ constrain, we can simplify {eq}`mixed-ogden-energy` as

$$
\begin{aligned}
\psi \left( \bar{\lambda}_1, \bar{\lambda}_2, \bar{\lambda}_3, J=1 \right) &= \frac{m_1}{2} \left(\bar{\lambda}_1^2 + \bar{\lambda}_2^2 + \bar{\lambda}_3^2 - 3\right) - \frac{m_2}{2} \left(\bar{\lambda}_1^{-2} + \bar{\lambda}_2^{-2} + \bar{\lambda}_3^{-2} - 3\right) \\
&= \frac{\secondlame_1}{2} \left(\mathbb{\bar{I}}_1 - 3\right) + \frac{\secondlame_2}{2} \left(\mathbb{\bar{I}}_2 - 3\right)
\end{aligned}
$$

which is the Mooney-Rivlin isochoric model.
:::

We differentiate $\psi$ as in the isochoric Neo-Hookean case {eq}`mixed-neo-hookean-stress` to yield the second Piola-Kirchoff tensor,

$$
  \begin{aligned}
    \bm{S} = \frac{\partial \psi}{\partial \bm{E}} &=  \frac{\partial \psi_{\text{vol}}}{\partial J}  \frac{\partial J}{\partial \bm{E}} +  \frac{\partial \psi_{\text{iso}}}{\partial \bm{E}} =\bm{S}_{\text{vol}} + \bm{S}_{\text{iso}}\\
    &= \left(\bulk_p J \, V' - p \, J \right) \bm{C}^{-1} + \sum_{i=1}^3 S_i^{\text{iso}} \hat{\bm{N}_i} \hat{\bm{N}_i}^T.
  \end{aligned}
$$ (mixed-ogden-stress)

where the pressure-like variable $p$ is defined via {eq}`general-pressure-like` and we have used {eq}`strain-energy-grad-stretch` and {eq}`stretch-derivative`. By employing $\frac{\partial J}{\partial {\lambda}_i} = J {\lambda}_i^{-1}$ and relation {eq}`modified-stretch`

$$
  \begin{aligned}
  S_i^{\text{iso}} = \frac{1}{\lambda_i}\frac{\partial \psi_{\text{iso}}}{\partial \lambda_i}
	=\frac{1}{\lambda_i}\frac{\partial \bar{\lambda}_k}{\partial \lambda_i}
	\frac{\partial \psi_{\text{iso}}}{\partial \bar{\lambda}_k}
	&=\frac{J^{-1/3}}{\lambda_i} \left(\delta_{ik} -\frac{1}{3}\lambda_i^{-1}\lambda_k \right) \frac{\partial \psi_{\text{iso}}}{\partial \bar{\lambda}_k} \\
	&=\frac{J^{-1/3}}{\lambda_i} \left(\delta_{ik} -\frac{1}{3}\bar{\lambda}_i^{-1}\bar{\lambda}_k \right) \frac{\partial \psi_{\text{iso}}}{\partial \bar{\lambda}_k}.
  \end{aligned}
$$ (mixed-ogden-principal-stress)

in which

$$
  \frac{\partial \psi_{\text{iso}}}{\partial \bar{\lambda}_k} = \frac{\partial \omega (\bar{\lambda}_k)}{ \partial \bar{\lambda}_k} = \sum_{j=1}^N m_j \bar{\lambda}_k^{\alpha_j - 1}
$$ (mixed-ogden-iso-derivative)

:::{tip}
To derive an equivalent numerically stable form of the isochoric part of {eq}`mixed-ogden-stress` we rewrite the $\psi_{\text{iso}}$ by substituting $\bar{\lambda}_i = J^{-1/3}\lambda_i$ as

$$
	\psi_{\text{iso}}({\lambda}_i) = \sum_{j=1}^N \frac{m_j}{\alpha_j} \left[\left({\lambda}_1^{\alpha_j} + {\lambda}_2^{\alpha_j} + {\lambda}_3^{\alpha_j} \right)J^{-\alpha_j/3}  -3 \right]
$$ (energy-iso-stable)

then

$$
  \begin{aligned}
  S_1^{\text{iso}} = \frac{1}{\lambda_1}\frac{\partial \psi_{\text{iso}}}{\partial \lambda_1} &= \frac{1}{\lambda_1} \sum_{j=1}^N \frac{m_j}{\alpha_j} \left[ \left(\alpha_j \lambda_1^{\alpha_j -1} \right)J^{-\alpha_j/3} - \frac{\alpha_j}{3} J^{-\alpha_j/3} \lambda_1^{-1} \left( {\lambda}_1^{\alpha_j} + {\lambda}_2^{\alpha_j} + {\lambda}_3^{\alpha_j} \right)\right] \\
	&= \frac{1}{\lambda_1} \sum_{j=1}^N m_j \left[ \lambda_1^{\alpha_j -1} - \frac{1}{3} \lambda_1^{-1} \left( {\lambda}_1^{\alpha_j} + {\lambda}_2^{\alpha_j} + {\lambda}_3^{\alpha_j} \right)\right] J^{-\alpha_j/3} \\
	&= \frac{1}{\lambda_1^2} \sum_{j=1}^N \frac{m_j}{3} \left[ 2\lambda_1^{\alpha_j} - {\lambda}_2^{\alpha_j} - {\lambda}_3^{\alpha_j} \right] J^{-\alpha_j/3} \\
	&= \frac{1}{\lambda_1^2} \sum_{j=1}^N \frac{m_j}{3} \left[ 2\left(\lambda_1^{\alpha_j} -1 \right) - \left({\lambda}_2^{\alpha_j} -1 \right) - \left({\lambda}_3^{\alpha_j} -1 \right)\right] J^{-\alpha_j/3} \\
	&=\frac{1}{\lambda_1^2} \sum_{j=1}^N \frac{m_j}{3} \left[ 2(e^{\alpha_j \ell_1}-1) - (e^{\alpha_j \ell_2}-1) - (e^{\alpha_j \ell_3}-1) \right] J^{-\alpha_j/3} \\
	&=\frac{1}{1 + 2\lambda_1^E} \sum_{j=1}^N \frac{m_j}{3} \left[ 2\operatorname{\tt expm1}(\alpha_j \ell_1) - \operatorname{\tt expm1}(\alpha_j \ell_2) - \operatorname{\tt expm1}(\alpha_j \ell_3) \right] J^{-\alpha_j/3}
  \end{aligned}
$$ (mixed-ogden-stress-iso-stable1)


where the eigen pair $(\lambda_i, \hat{\bm{N}_i})$ are computed by solving the eigen problem of Green-Lagrange strain tensor $\bm{E} \hat{\bm{N}_i} = \lambda_i^E \hat{\bm{N}_i}$ with $\lambda_i = \sqrt{1 + 2 \lambda_i^E}$ and and $\ell_i = \log \lambda_i = \frac 1 2 \operatorname{\tt log1p}(2\lambda_i^E)$.
Following the above approach we have

$$
  \begin{aligned}
  S_2^{\text{iso}} = \frac{1}{\lambda_2}\frac{\partial \psi_{\text{iso}}}{\partial \lambda_2} =\frac{1}{1 + 2\lambda_2^E} \sum_{j=1}^N \frac{m_j}{3} \left[ -\operatorname{\tt expm1}(\alpha_j \ell_1) +2 \operatorname{\tt expm1}(\alpha_j \ell_2) - \operatorname{\tt expm1}(\alpha_j \ell_3) \right] J^{-\alpha_j/3} \\
  S_3^{\text{iso}} = \frac{1}{\lambda_3}\frac{\partial \psi_{\text{iso}}}{\partial \lambda_3} =\frac{1}{1 + 2\lambda_3^E} \sum_{j=1}^N \frac{m_j}{3} \left[ -\operatorname{\tt expm1}(\alpha_j \ell_1) - \operatorname{\tt expm1}(\alpha_j \ell_2) + 2 \operatorname{\tt expm1}(\alpha_j \ell_3) \right] J^{-\alpha_j/3}
  \end{aligned}
$$ (mixed-ogden-stress-iso-stable2)

substituting the new definition of $S_i^{\text{iso}}$ {eq}`mixed-ogden-stress-iso-stable1`, {eq}`mixed-ogden-stress-iso-stable2` into {eq}`mixed-ogden-stress` gives the stable form of the second Piola-Kirchhoff stress for Ogden model.

:::

The Kirchhoff stress tensor $\bm{\tau}$ for Ogden model is given by

$$
  \bm{\tau} = \bm{F}\bm{S}\bm{F}^T = \bm{\tau}_{\text{vol}} + \bm{\tau}_{\text{iso}} = \left(\bulk_p J \, V' - p \, J \right) \bm{I} + \sum_{i=1}^3 \tau_i^{\text{iso}} \hat{\bm{n}_i} \hat{\bm{n}_i}^T.
$$ (mixed-ogden-tau)

where we have used $\bm{F} \hat{\bm{N}_i} = \lambda_i \hat{\bm{n}_i}$ which $\hat{\bm{n}_i}$ are eigenvectors of $\bm{b}$ and $\tau_i^{\text{iso}} = \lambda_i^2 S_i^{\text{iso}}$.

:::{tip}
Similar to the initial configuration, we can compute the $\bm{\tau}_{\text{iso}}$ in stable form by using the stable coefficients

$$
  \begin{aligned}
  \tau_1^{\text{iso}} &= \lambda_1 \frac{\partial \psi_{\text{iso}}}{\partial \lambda_1} =  \sum_{j=1}^N \frac{m_j}{3} \left[ 2\operatorname{\tt expm1}(\alpha_j \ell_1) - \operatorname{\tt expm1}(\alpha_j \ell_2) - \operatorname{\tt expm1}(\alpha_j \ell_3) \right] J^{-\alpha_j/3} \\
  \tau_2^{\text{iso}} &= \lambda_2 \frac{\partial \psi_{\text{iso}}}{\partial \lambda_2} = \sum_{j=1}^N \frac{m_j}{3} \left[ -\operatorname{\tt expm1}(\alpha_j \ell_1) +2 \operatorname{\tt expm1}(\alpha_j \ell_2) - \operatorname{\tt expm1}(\alpha_j \ell_3) \right] J^{-\alpha_j/3} \\
  \tau_3^{\text{iso}} &= \lambda_3 \frac{\partial \psi_{\text{iso}}}{\partial \lambda_3} = \sum_{j=1}^N \frac{m_j}{3} \left[ -\operatorname{\tt expm1}(\alpha_j \ell_1) - \operatorname{\tt expm1}(\alpha_j \ell_2) + 2 \operatorname{\tt expm1}(\alpha_j \ell_3) \right] J^{-\alpha_j/3}
  \end{aligned}
$$ (mixed-ogden-tau-iso-stable)

and computing the eigen pair $(\lambda_i, \hat{\bm{n}_i})$ of Green-Euler strain tensor $\bm{e} \hat{\bm{n}_i} = \lambda_i^e \hat{\bm{e}_i}$ with $\lambda_i = \sqrt{1 + 2 \lambda_i^e}$ and $\ell_i = \log \lambda_i = \frac 1 2 \operatorname{\tt log1p}(2\lambda_i^e)$.

:::

:::{note}
It should be noted that the Ogden model can be implemented with single field displacement as described in Neo-Hookean isochoric model {eq}`isochoric-neo-hookean-stress` which in that case we have

$$
  \begin{aligned}
    \bm{S} = \frac{\partial \psi}{\partial \bm{E}} &=  \frac{\partial \psi_{\text{vol}}}{\partial J}  \frac{\partial J}{\partial \bm{E}} +  \frac{\partial \psi_{\text{iso}}}{\partial \bm{E}} =\bm{S}_{\text{vol}} + \bm{S}_{\text{iso}}\\
    &= \bulk J \, V' \, \bm{C}^{-1} + \sum_{i=1}^3 S_i^{\text{iso}} \hat{\bm{N}_i} \hat{\bm{N}_i}^T.
  \end{aligned}
$$ (isochoric-ogden-stress)
:::
