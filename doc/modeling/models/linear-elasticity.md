# Linear elasticity

For the linear elasticity model, the strain energy density is given by

$$
\psi = \frac{\firstlame}{2} (\trace \bm{\varepsilon})^2 + \secondlame \bm{\varepsilon} \tcolon \bm{\varepsilon} .
$$

The constitutive law (stress-strain relationship) is therefore given by its gradient,

$$
\bm\sigma(\bm{u}) = \frac{\partial \psi}{\partial \bm{\varepsilon}} = \firstlame (\trace \bm\varepsilon) \bm{I} + 2 \secondlame \bm\varepsilon,
$$ (linear-constitutive)

where the colon represents a double contraction (over both indices of $\bm{\varepsilon}$), $\bm{\varepsilon}$ is (small/infintesimal) strain tensor defined by

$$
\bm{\varepsilon} = \dfrac{1}{2}\left(\nabla \bm{u} + \nabla \bm{u}^T \right),
$$ (linear-strain-tensor)

and the Lamé parameters are given in terms of Young's modulus $E$, and Poisson’s ratio $\nu$ by

$$
\begin{aligned}
  \firstlame &= \frac{E \nu}{(1 + \nu)(1 - 2 \nu)}, \\
  \secondlame &= \frac{E}{2(1 + \nu)}.
\end{aligned}
$$

The constitutive law (stress-strain relationship) can also be written as

$$
\bm{\sigma} = \mathsf{C} \tcolon \bm{\varepsilon}.
$$ (linear-stress-strain)

For notational convenience, we express the symmetric second order tensors $\bm \sigma$ and $\bm \varepsilon$ as vectors of length 6 using the [Voigt notation](https://en.wikipedia.org/wiki/Voigt_notation).
Hence, the fourth order elasticity tensor $\mathsf C$ (also known as elastic moduli tensor or material stiffness tensor) can be represented as

$$
\mathsf C = \begin{pmatrix}
\firstlame + 2\secondlame & \firstlame & \firstlame & & & \\
\firstlame & \firstlame + 2\secondlame & \firstlame & & & \\
\firstlame & \firstlame & \firstlame + 2\secondlame & & & \\
& & & \secondlame & & \\
& & & & \secondlame & \\
& & & & & \secondlame
\end{pmatrix}.
$$ (linear-elasticity-tensor)

Note that the incompressible limit $\nu \to \frac 1 2$ causes $\firstlame \to \infty$, and thus $\mathsf C$ becomes singular.

## Incompressibility

One can see from the above equations that as $\firstlame \to \infty$, it is necessary that $\nabla\cdot \bm{u} \longrightarrow 0$ which gives an idea of alternative strategies. One approach is to define an auxiliary variable $p$, and rewrite constitutive equation {eq}`linear-constitutive` as

$$
  \begin{aligned}
    \bm \sigma(\bm u, p) &= -p \, \bm{I} + 2 \mu \bm \varepsilon, \\
    p &= - \firstlame \trace \bm \varepsilon.
  \end{aligned}
$$ (mixed-linear-constitutive1)

Alternatively, we can use the definition of hydrostatic pressure i.e., $p_{\text{hyd}} = - \frac{\trace \bm \sigma}{3}$ and arrive at

$$
  \begin{aligned}
    \bm \sigma(\bm u, p_{\text{hyd}}) &= -p_{\text{hyd}} \, \bm{I} + 2 \mu \bm \varepsilon_{\text{dev}}, \\
    p_{\text{hyd}} &= -\bulk \trace \bm \varepsilon.
  \end{aligned}
$$ (mixed-linear-constitutive2)

where $\bm \varepsilon_{\text{dev}} = \bm \varepsilon - \frac{1}{3} \trace \bm \varepsilon ~ \bm{I}$ is the deviatoric part of the linear strain tensor and $\bulk$ is the bulk modulus. We present a general constitutive equation as

$$
  \begin{aligned}
    \bm \sigma(\bm u, p) &= \left(\bulk_p \trace \bm \varepsilon -p \right) \bm{I} + 2 \mu \bm \varepsilon_{\text{dev}}, \\
    p &= -\left(\bulk - \bulk_p \right) \trace \bm \varepsilon.
  \end{aligned}
$$ (general-mixed-linear-constitutive)

where

$$
  \bulk_p = \frac{2 \mu \left(1 + \nu_p \right)}{3 \left(1 - 2 \nu_p \right)},
$$ (primal-bulk)

is the primal portion of the bulk modulus, defined in terms of $\nu_p$ with $-1 \leq \nu_p < \nu$, where $\nu$ is the physical Poisson’s ratio. The standard full-train formulation {eq}`mixed-linear-constitutive1` is obtained using $\nu_p = 0$, and the deviatoric formulation {eq}`mixed-linear-constitutive2` with $\nu_p = -1$.


