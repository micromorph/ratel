# Poroelasticity

## Linear

For the linear poroelasticity model, the strain energy density is given by

$$
\psi = \frac{\firstlame_{u}}{2} (\trace \bm{\varepsilon})^2 + \secondlame_{d} \bm{\varepsilon} \tcolon \bm{\varepsilon} - B \, M \, \trace \bm{\varepsilon} \, \zeta + \frac{1}{2} M \zeta^2,
$$

where $\firstlame_{u} = \firstlame_{d} + B^2 M$ is undrained first Lamé parameter while $\firstlame_{d}, \secondlame_{d}$ are Lamé parameters measured in drained condition, $B, M$ are the Biot effective stress coefficient and Biot modulus defined by

$$
\begin{aligned}
  B &= 1 - \frac{\bulk_{d}}{\bulk_s}, \\
  \frac{1}{M} &= \frac{\phi^f}{\bulk_f} + \frac{B - \phi^f}{\bulk_s},
\end{aligned}
$$ (biot-coefficient-modulus)

with mixture bulk modulus $\bulk_{d}$ measured in drained condition and solid and fluid bulk moduli $\bulk_s, \bulk_f$, respectively.

To derive the constitutive law (stress-strain relationship) for the linear poroelasticity model we have

$$
\begin{aligned}
  \bm\sigma(\bm{u}, p) &= \frac{\partial \psi}{\partial \bm{\varepsilon}} = \bm\sigma'(\bm{u}) - B \, p \, \bm{I} = \firstlame_{d} \nabla\cdot \bm{u} \, \bm{I} + 2 \, \secondlame_{d} \, \bm{\varepsilon} - B \, p \, \bm{I}, \\
  p &= \frac{\partial \psi}{\partial \zeta} = M \left(\zeta - B \nabla \cdot \bm{u} \right),
\end{aligned}
$$ (constitutive-linear-poroelastic)


where , $\bm\sigma' = \firstlame_{d} (\trace \bm\varepsilon) \bm{I} + 2 \, \secondlame_{d} \, \bm{\varepsilon}$ is effective stress, $p$ is pore pressure, and the variation of fluid content $\zeta$ is

$$
  \zeta = \frac{p}{M} + B \nabla\cdot \bm u.
$$ (fluid-variation2)

## Finite strain

For finite strain poroelasticity, by writing Clausius-Duhem inequality we can derive constitutive equations for solid and fluid phases as {cite}`irwin2024porofinitestrain`

$$
  \bm{S}' = 2\frac{\partial \psi^s}{ \partial \bm{C}_s}, \quad p = \frac{\rho^{fR}}{B\phi^f}\frac{\partial \psi^f}{\partial \rho^{fR}},
$$

where $\bm{S}'$ is effective second Piola-Kirchoff stress tensor, $\psi^s$ is strain energy for solid phase which is defined in previous section for Neo-Hookean, Mooney-Rivlin, and Ogden models. Helmholtz free energy per unit mass for fluid phase $\psi^f$ is given by

$$
  \psi^f = \frac{1}{2}B \phi^f \bulk_f \left(\log \rho^{fR}\right)^2.
$$ (energy-fluid-phase)

The total stress for the mixture for Neo-Hookean model {eq}`neo-hookean-energy` is

$$
  \begin{aligned}
    \bm{S} &= J \bm{F}^{-1} \bm{\sigma} \bm{F}^{-T} = \bm{S}' - J B p \bm{C}^{-1}, \\
    \bm{S}' &= \firstlame J V' \bm{C}^{-1} + \secondlame \left( \bm{I} - \bm{C}^{-1} \right) = \frac{\firstlame}{2} \left( J^2 - 1 \right)\bm{C}^{-1} + \secondlame \left( \bm{I} - \bm{C}^{-1} \right),
  \end{aligned}
$$ (constitutive-hyper-poroelastic-initial)

and using {eq}`S-to-tau` we can write symmetric Kirchhoff stress tensor as

$$
  \begin{aligned}
    \bm{\tau} &= \bm{\tau}' - J B p \bm{I}, \\
    \bm{\tau}' &= \firstlame J V' \bm{I} + \secondlame \left( \bm{b} - \bm{I} \right) = \frac{\firstlame}{2} \left( J^2 - 1 \right)\bm{I} + \secondlame \left( \bm{b} - \bm{I} \right).
  \end{aligned}
$$ (constitutive-hyper-poroelastic-current)
