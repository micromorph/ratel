# Mooney-Rivlin Materials

While the Neo-Hookean model depends on just two scalar invariants, $\mathbb{I}_1 = \trace \bm{C} = 3 + 2 \trace \bm{E}$ and $J$, Mooney-Rivlin models depend on the additional invariant, $\mathbb{I}_2$ defined in {eq}`invariants`.
A coupled Mooney-Rivlin strain energy density (cf. Neo-Hookean {eq}`neo-hookean-energy`) is {cite}`holzapfel2000nonlinear`

$$
  \begin{aligned}
    \psi \left( \mathbb{I_1}, \mathbb{I_2}, J \right) &= \firstlame V(J) - \left( \secondlame_1 + 2\secondlame_2 \right) \log J + \frac{\secondlame_1}{2} \left( \mathbb{I_1} - 3 \right) + \frac{\secondlame_2}{2} \left( \mathbb{I_2} - 3 \right), \\
                             &= \frac{\firstlame}{4} \left( J^2 - 1 -2 \log J \right) - \left( \secondlame_1 + 2\secondlame_2 \right) \log J + \frac{\secondlame_1}{2} \left( \mathbb{I_1} - 3 \right) + \frac{\secondlame_2}{2} \left( \mathbb{I_2} - 3 \right).
  \end{aligned}
$$ (mooney-rivlin-energy_coupled)

We differentiate $\psi$ as in the Neo-Hookean case {eq}`neo-hookean-stress` to yield the second Piola-Kirchoff tensor,

$$
  \begin{aligned}
    \bm{S} &=  \firstlame J V' \bm{C}^{-1} - \left( \secondlame_1 + 2\secondlame_2 \right) \bm{C}^{-1} + \secondlame_1\bm{I} + \secondlame_2 \left( \mathbb{I_1} \bm{I} - \bm{C} \right) \\
           &= \frac{\firstlame}{2} \left( J^2 - 1 \right)\bm{C}^{-1} + \secondlame_1 \left( \bm{I} - \bm{C}^{-1} \right) + \secondlame_2 \left( \mathbb{I_1} \bm{I} - 2 \bm{C}^{-1} - \bm{C} \right),
  \end{aligned}
$$ (mooney-rivlin-stress_coupled)

where we have used {eq}`invariants-derivative`. This is a common model for vulcanized rubber, with a shear modulus (defined for the small-strain limit) of $\secondlame_1 + \secondlame_2$ that should be significantly smaller than the first Lamé parameter $\firstlame$.

:::{dropdown} Mooney-Rivlin strain energy comparison
We apply traction to a block and plot integrated strain energy $\psi$ as a function of the loading parameter.

```{altair-plot}
:hide-code:

import altair as alt
import pandas as pd
def source_path(rel):
    import os
    return os.path.join(os.path.dirname(os.environ["DOCUTILSCONFIG"]), rel)

nh = pd.read_csv(source_path("doc/data/NH-strain.csv"))
nh["model"] = "Neo-Hookean"
nh["parameters"] = "E=2.8, nu=0.4"

mr = pd.read_csv(source_path("doc/data/MR-strain.csv"))
mr["model"] = "Mooney-Rivlin; Neo-Hookean equivalent"
mr["parameters"] = "mu_1=1, mu_2=0, nu=.4"

mr1 = pd.read_csv(source_path("doc/data/MR-strain1.csv"))
mr1["model"] = "Mooney-Rivlin"
mr1["parameters"] = "mu_1=0.5, mu_2=0.5, nu=.4"

df = pd.concat([nh, mr, mr1])
highlight = alt.selection_point(
   on = "mouseover",
   nearest = True,
   fields=["model", "parameters"],
)
base = alt.Chart(df).encode(
   alt.X("increment"),
   alt.Y("energy", scale=alt.Scale(type="sqrt")),
   alt.Color("model"),
   alt.Tooltip(("model", "parameters")),
   opacity=alt.condition(highlight, alt.value(1), alt.value(.5)),
   size=alt.condition(highlight, alt.value(2), alt.value(1)),
)
base.mark_point().add_params(highlight) + base.mark_line()
```
:::

:::{tip}
Similar to the Neo-Hookean materials, the stable form for the Mooney-Rivlin model in initial configuration {eq}`mooney-rivlin-stress_coupled` can be written as

$$
\bm{S} = \frac{\firstlame}{2} \mathtt{J_{-1}} \left(\mathtt{J_{-1}} + 2 \right) \bm{C}^{-1} + 2 \left( \secondlame_1 + 2\secondlame_2 \right)\bm{C}^{-1} \bm{E} + 2\secondlame_2 \left(\trace \left(\bm{E} \right) \bm{I} - \bm{E} \right).
$$ (mooney-rivlin-stress-stable)
:::

The Kirchhoff stress tensor $\bm{\tau}$ for Mooney-Rivlin model is given by

$$
  \begin{aligned}
    \bm{\tau} = \bm{F}\bm{S}\bm{F}^T &= \firstlame J V' \bm{I} + \secondlame_1 \left( \bm{b} - \bm{I} \right) + \secondlame_2 \left( \mathbb{I_1} \bm{b} - 2 \bm{I} - \bm{b}^2 \right), \\
      &= \frac{\firstlame}{2} \left( J^2 - 1 \right)\bm{I} + \secondlame_1 \left( \bm{b} - \bm{I} \right) + \secondlame_2 \left( \mathbb{I_1} \bm{b} - 2 \bm{I} - \bm{b}^2 \right).
  \end{aligned}
$$ (mooney-rivlin-tau)


:::{tip}
The stable Kirchhoff stress tensor version of {eq}`mooney-rivlin-tau` is given by

$$
\bm{\tau} = \frac{\firstlame}{2} \mathtt{J_{-1}} \left(\mathtt{J_{-1}} + 2 \right) \bm{I} + 2 \left( \secondlame_1 + 2\secondlame_2 \right)\bm{e} + 2\secondlame_2 \left(\trace \left(\bm{e}\right) \bm{I} - \bm{e} \right) \bm{b}.
$$ (mooney-rivlin-tau-stable)

where $\mathtt{J_{-1}}$ is computed by {eq}`JM1`
:::
