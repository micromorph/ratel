(Poroelasticity)=
# Poroelasticity

## Linear

The strong form of linear poroelasticity (mixed $\bm u - p$ formulation) for constitutive equation {eq}`constitutive-linear-poroelastic` based on conservation of momentum (for static case) and mass may be stated as follows: Given body forces $\bm f^s, \bm f^f$ and volumetric injected fluid rate $\dot{\gamma}$, Dirichlet boundaries $\bar{\bm u}, \bar{p}$, applied traction $\bar{\bm t}$ and fluid flux $\bar{s}$ and initial conditions $\bm u_0, p_0$, find the displacement and pressure variables $(\bm u, p) \in \mathcal{V} \times \mathcal{Q} $ (here $\mathcal{V} = H^1(\Omega), \mathcal{Q} = H^1(\Omega) $ ), such that:

$$
  \begin{aligned}
    -\nabla \cdot \bm \sigma - \bm f^s   &= 0, \qquad \text{in $\Omega$} \\
    \frac{\dot{p}}{M} + \alpha \nabla \cdot \dot{\bm u} + \nabla\cdot \dot{\bm{w}} - \dot{\gamma} &= 0, \qquad \text{in $\Omega$} \\
    \bm{u} &= \bar{\bm u}, \quad    \text{on $\partial \Omega^{D}$} \\
    p &= \bar{p}, \quad    \text{on $\partial \Omega^{D}$} \\
    \bm \sigma \cdot \bm n &= \bar{\bm t}, \qquad \text{on $\partial \Omega^{N}$} \\
    \dot{\bm{w}} \cdot \bm n &= \bar{s}, \qquad \text{on $\partial \Omega^{N}$} \\
    \bm u &= \bm u_0, \qquad \text{in $\Omega$} \\
    p &= p_0, \qquad \text{in $\Omega$}
  \end{aligned}
$$ (poro-linear-strong-form)

with $\bm n$ be the unit normal on the boundary and in the conservation of mass (second equation), we replaced $\zeta$ from constitutive equation {eq}`fluid-variation2`. The weak form can be derived as:

$$
  \begin{aligned}
    \int_{\Omega}{ \nabla \bm{v} \tcolon \bm{\sigma}} \, dv
    - \int_{\partial \Omega}{\bm{v} \cdot \bar{\bm t}} \, da
    - \int_{\Omega}{\bm{v} \cdot \bm f^s} \, dv
    &= 0, \quad \forall \bm v \in \mathcal V \\
    \int_{\Omega} q \left(\frac{\dot{p}}{M} + \alpha \nabla \cdot \dot{\bm u} - \dot{\gamma} \right) \, dv + \int_{\Omega} \nabla q \cdot \left( \varkappa \nabla p - \varkappa \bm f^f \right) + \int_{\partial \Omega}{ q \, \bar{s}} \, da & = 0. \quad \forall q \in \mathcal Q
  \end{aligned}
$$ (poro-linear-weak-form)

where we have used Darcy's law $\dot{\bm w} = - \varkappa \left(\nabla p - \bm f^f \right)$ (equation {eq}`generalized-darcy` for static case).

## Finite strain

To derive the strong form for finite strain poroelasticity, we use the balance of momentum for solid phase and balance of mass {eq}`balance-mass-simplified-initial` as

$$
  \begin{aligned}
    -\nabla_X \cdot \bm{P} - \rho_0 \bm g &= 0, \qquad \text{in $\Omega_0$} \\
    \frac{\phi^f}{\bulk_f}\frac{D p}{D t} + \nabla_x\cdot \dot{\bm{u}} + \nabla_x\cdot \dot{\bm w} + \frac{\dot{\bm w}}{\bulk_f}\cdot \nabla_x p &= 0, \qquad \text{in $\Omega$} \\
    \bm{u} &= \bar{\bm u}, \quad    \text{on $\partial \Omega^{D}_0$} \\
    p &= \bar{p}, \quad    \text{on $\partial \Omega^{D}$} \\
    \bm{P} \cdot \bm N &= \bar{\bm t}, \qquad \text{on $\partial \Omega^{N}_0$} \\
    \dot{\bm{w}} \cdot \bm n &= \bar{s}, \qquad \text{on $\partial \Omega^{N}$} \\
    \bm u &= \bm u_0, \qquad \text{in $\Omega_0$} \\
    p &= p_0, \qquad \text{in $\Omega$}
  \end{aligned}
$$ (poro-hyper-strong-form)

where we dropped subscript $s$ for simplicity, $\bm N$ and $\bm n$ are the unit normal vectors on the face in initial and current configurations, $_X$ and $_x$ in $\nabla_X, \nabla_x$ indicate that the gradient are calculated with respect to the initial/current configurations. Multiplying the strong form {eq}`poro-hyper-strong-form` by test functions $(\bm{v}, q)$ and  integrate by parts, we can obtain the weak form for finite strain hyperporoelasticity as: find $(\bm{u}, p) \in \mathcal{V} \times \mathcal{Q} \subset H^1 \left( \Omega_0 \right) \times H^1 \left( \Omega_0 \right)$ such that

$$
  \begin{aligned}
    r_u(\bm u, p) &\coloneqq \int_{\Omega_0}{\nabla_x \bm{v} \tcolon \bm{\tau}} \, dV
     - \int_{\Omega_0}{\bm{v} \cdot \rho_0 \bm{g}} \, dV
     - \int_{\partial \Omega_0}{\bm{v} \cdot \bar{\bm t}} \, dA = 0, \quad \forall \bm{v} \in \mathcal{V}, \\
    r_p(\bm u, p) &\coloneqq \int_{\Omega_0} q \cdot \left( \frac{\phi^f}{\bulk_f}\dot{p} + \nabla_x\cdot \dot{\bm{u}} - \frac{\varkappa}{\bulk_f}\nabla_x p \cdot \nabla_x p \right) J \, dV \\
                  &+\int_{\Omega_0} \nabla_x q \cdot \varkappa \nabla_x p \, J dV + \int_{\partial \Omega}{ q \, \bar{s}} \, da = 0, \quad \forall q\in \mathcal{Q}, \\
  \end{aligned}
$$ (poro-hyper-weak-form)

where we have used the push forward {eq}`push-forward` to write the first equation with respect to $_x$ and $\bm \tau$, and replaced $\dot{\bm w} = -\varkappa \nabla_x p$ by assuming $\ddot{\bm u} = 0, \bm f^f = 0$. The definition for $\bm{\tau}$ for Neo-Hookean is given in {eq}`constitutive-hyper-poroelastic-current`.

### Newton linearization

As explained in {ref}`Newton-linearization`, we need the Jacobian form of the {eq}`poro-hyper-weak-form` which is:
find $(\diff \bm{u}, \diff p) \in \mathcal{V} \times \mathcal{Q}$ such that

$$
  \begin{aligned}
    &\int_{\Omega_0} \nabla_x \bm{v} \tcolon \left( \diff \bm{\tau} -\bm{\tau}(\nabla_x \diff\bm{u})^T \right) dV = -r_u, \quad \forall \bm{v} \in \mathcal{V}, \\
    &\int_{\Omega_0} q \, \left( \diff L J + L \diff J \right) dV + \int_{\Omega_0} \nabla_x q \cdot K \, dV = -r_p, \quad \forall q \in \mathcal{Q}
  \end{aligned}
$$ (poro-hyper-jacobian-current)

where

$$
  \begin{aligned}
    \diff \bm{\tau} -\bm{\tau}(\nabla_x \diff\bm{u})^T &= \diff \bm{\tau}' -\bm{\tau}'(\nabla_x \diff\bm{u})^T - \left(\diff J p + J \diff p \right)B \bm I + J p B (\nabla_x \diff\bm{u})^T, \\
    &= (\nabla_x \diff\bm{u}) \bm{\tau} + \bm{F} \diff \bm{S}' \bm{F}^T - \left(\diff J p + J \diff p \right) B \bm I + 2 B J p \diff\bm\epsilon, \\
    L &= \frac{\phi^f}{\bulk_f}\dot{p} + \nabla_x\cdot \dot{\bm{u}} - \frac{\varkappa}{\bulk_f}\nabla_x p \cdot \nabla_x p, \\
    \diff L &= \mathrm{shift_v} \left(\frac{\phi^f}{\bulk_f}\diff p + \diff \, \left(\nabla_x\cdot \bm{u} \right) \right) - \frac{\diff \varkappa}{\bulk_f}\nabla_x p \cdot \nabla_x p - 2 \frac{\varkappa}{\bulk_f}\nabla_x p \cdot \diff \, (\nabla_x p), \\
    K &= \diff \varkappa \nabla_x p \, J + \varkappa \diff \, (\nabla_x p) \, J + \varkappa \nabla_x p \diff J - \nabla_x \diff \bm{u} \varkappa \nabla_x p J,
  \end{aligned}
$$

with given $\diff \bm{\tau}' -\bm{\tau}'(\nabla_x \diff\bm{u})^T$ in {eq}`cur_simp_Jac` for Neo-Hookean model and

$$
  \begin{aligned}
  \diff J &= J \, \trace (\nabla_x \diff\bm u) = J \, \trace \diff\bm\epsilon, \\
  \diff \varkappa &= \frac{\varkappa_{\circ}}{\eta_f \mathcal{F}(\phi^f_0)} \diff \mathcal{F}(\phi^f) = \frac{\varkappa_{\circ}}{\eta_f \mathcal{F}(\phi^f_0)} \frac{\partial \mathcal{F}(\phi^f)}{\partial \phi^f} \diff \phi^f, \\
  \diff \phi^f &= \diff \left(1 - \phi^s \right) = \diff \left(1 - \frac{\phi^s_0}{J} \right) = \frac{\phi^s_0}{J^2} \diff J = \phi^s \trace \diff\bm\epsilon, \\
  \diff \, (\nabla_x p) &= \diff \, \left( \bm{F}^{-T} \nabla_X p \right) = \nabla_x \diff p - (\nabla_x \diff\bm{u})^T \nabla_x p, \\
  \diff \, \left(\nabla_x \cdot \bm{u} \right) &= \diff \, \trace \left( \nabla_x \bm{u}\right) = \diff \, \trace \left( \nabla_X \bm{u} \bm{F}^{-1}\right) = \trace \left( \nabla_x \diff \bm{u} - \nabla_x \bm{u} \nabla_x \diff \bm{u}\right), \\
  &= \nabla_x \cdot \diff\bm{u} - \nabla_x \bm{u} \tcolon (\nabla_x \diff\bm{u})^T,
  \end{aligned}
$$

where $\mathcal{F}(\phi^f)$ is defined in {eq}`kozeny-carman`.

## Matrix-free implementation

The matrix-free formulation for poroelasticity is similar to incompressible elasticity explained in {ref}`matrix-free-mixed-fields`. However, in equation {eq}`poro-linear-weak-form` we have time derivative of displacement and pressure fields and for deriving jacobian we need to add $\mathrm{shift_v}$ as shown in {eq}`time-dependent-jacobian`.

* For linear poroelasticity described in {eq}`poro-linear-weak-form` we have

$$
  \begin{aligned}
    \bm f_0 &= -\bm f^s, \quad \bm f_1 = \bm{\sigma}(\bm u, p) = \firstlame_{d} \nabla\cdot \bm{u} \, \bm{I} + 2 \, \secondlame_{d} \, \bm{\varepsilon} - \alpha \, p \, \bm{I}, \\
    \bm g_0 &=\frac{\dot{p}}{M} + \alpha \nabla \cdot \dot{\bm u} - \dot{\gamma}, \quad \bm g_1 = \varkappa \nabla p - \varkappa \bm f^f, \\
		\diff \bm f_1 &= \bm f_1(\diff \bm{u}, \diff p), \quad \diff \bm g_0 = \mathrm{shift_v} \left( \frac{\diff p}{M} + \alpha \nabla \cdot \diff \bm{u} \right), \quad \diff \bm g_1 = \varkappa \nabla \diff p.
  \end{aligned}
$$

where $\mathrm{shift_v}$ evaluated at time  $\mathrm{shift_v} = \frac{\partial\dot{\phi}}{\partial\phi}|_{\phi_n}$ and we have used the linearization of time dependent variables explained {eq}`time-dependent-jacobian`.

* For finite strain poroelasticity described in {eq}`poro-hyper-weak-form` and {eq}`poro-hyper-jacobian-current` we have

$$
  \begin{aligned}
    \bm f_0 &= -\rho_0 \bm g, \quad \bm f_1 = \bm{\tau} = \bm{\tau}' - J B p \bm{I}, \\
    \bm g_0 &= L \, J = \left( \frac{\phi^f}{\bulk_f}\dot{p} + \nabla_x\cdot \dot{\bm{u}} - \frac{\varkappa}{\bulk_f}\nabla_x p \cdot \nabla_x p \right) J, \quad \bm g_1 = \varkappa \nabla_x p \, J, \\
		\diff \bm f_1 &= \diff \bm{\tau} -\bm{\tau}(\nabla_x \diff\bm{u})^T, \\
    \diff \bm g_0 &= \diff L J + L \diff J, \\
    \diff \bm g_1 &= K = \diff \varkappa \nabla_x p \, J + \varkappa \diff \, (\nabla_x p) \, J + \varkappa \nabla_x p \diff J - \nabla_x \diff \bm{u} \varkappa \nabla_x p J.
  \end{aligned}
$$
