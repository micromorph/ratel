# Phase-field Modeling of Fracture

## Small strain

The variational formulation of the Griffith principle of (brittle) fracture {cite}`francfort1998revisiting` introduces a phase-field regularization of sharp cracks ($\Gamma$) via the crack surface density function $\gamma(l_0, \phi)$ where the (damage) phase field $0\leq\phi\leq1$ describes a smeared crack over a distance modulated by the length scale parameter $l_0$. A general expression for $\gamma(l_0, \phi)$ is:

$$
  \gamma(l_0, \phi) = \frac{1}{c_0 \, l_0} \left(\alpha(\phi)+l_0^2 \lvert \nabla\phi \rvert^2 \right),
$$

where $\alpha(\phi)$ is the geometric crack function and $c_0= 4\int^{1}_{0}\sqrt{\alpha(\phi)}\diff \phi$ a scaling factor.
The crack surface energy $\psi_{\Gamma}$ can thus be calculated by integrating over the domain $\Omega$:

$$
  \psi_{\Gamma}=\int_{\Gamma}G_c d \Gamma \approx \int_{\Omega} G_c \gamma(l_0, \phi) dv
$$

where $G_c$ [$J/m^2$] is the critical energy release rate, often related to the material fracture toughness.
With the above regularization, the strain energy stored in the solid can be expressed as the (elastic) strain energy $\psi(\bm\varepsilon(\bm{u}))$ multiplied by an appropriate degradation function $g(\phi)$.
The total energy of the solid is then given by

$$
  W(\bm{u},\phi)=\int_{\Omega}g(\phi)\psi(\bm\varepsilon(\bm{u}))dv + \int_{\Omega} \frac{G_c}{c_0l_0} \left( \alpha(\phi)+l_0^2 \lvert \nabla\phi \rvert^2 \right) dv
$$ (damage-total-energy)

Minimization of $W(\bm{u},\phi)$ with respect to $\bm{u}$ and $\phi$ solves the Griffith's (brittle) fracture problem but does not address crack irreversibility nor the effect of loading direction on damage evolution observed experimentally.

::: Note
$W(\bm{u},\phi)$ can be regarded as the free energy of a gradient damage model if $l_0$ is linked to the material tensile strength, $\bm{\sigma}_c$. It can be shown, in particular, that $\bm{\sigma}_c^2= \beta \frac{EGc}{l_0}$, where $\beta$ depends on the specific model (e.g. $\beta=3/8$ for AT1 and $\beta=27/256$ for AT2){cite}`tanne2018crack`.
:::

### Damage driving force and crack irreversibility

Voids growth and coalescence is inhibited by hydrostatic pressure, consistent with cracking preserving the part of strain energy associated with compressive strain components. Expression for $W(\bm{u},\phi)$ in {eq}`damage-total-energy` can then be recast as:

$$
  \bar{W}(\bm{u},\phi)=\int_{\Omega}(g(\phi)\psi^+(\bm\varepsilon(\bm{u})) + \psi^-(\bm\varepsilon(\bm{u})))dv + \int_{\Omega} \frac{G_c}{c_0l_0} \left( \alpha(\phi)+l_0^2 \lvert \nabla\phi \rvert^2 \right) dv
$$ (damage-total-energy-anisotropy)

Several expressions for $\psi^+$ can be found in the literature, including some function of tensile critical stresses or positive principal stretches {cite}`miehe2010phase`. For linear elasticity, we consider one by {cite}`amor2009regularized` based on the following deviatoric and volumetric split:

$$
\psi^{+}=
\begin{cases}
       \psi_{\text{vol}} + \psi_{\text{dev}} = \frac{\bulk}{2} (\trace \bm\varepsilon)^2 + \mu(\bm\varepsilon_{\text{dev}} : \bm\varepsilon_{\text{dev}}) \quad \text{if}  \quad \trace \bm\varepsilon \geq 0
\\
      \psi_{\text{dev}} = \mu(\bm\varepsilon_{\text{dev}} : \bm\varepsilon_{\text{dev}}) \qquad \qquad \qquad \qquad \qquad \text{if}  \quad \trace \bm\varepsilon < 0
\end{cases}
$$ (linear-Psi-plus)

where $\mu$ and $\bulk$ are the shear and bulk moduli, respectively. For $\psi^-$, this gives

$$
\psi^{-}=
\begin{cases}
      0  \qquad  \quad \text{if}  \quad \trace \bm\varepsilon \geq 0
\\
      \psi_{\text{vol}} \qquad \text{if}  \quad \trace \bm\varepsilon < 0
\end{cases}
$$ (linear-Psi-minus)

Finally, $\psi^{+}$ can be handled as a history variable evaluated at quadrature points {cite}`miehe2010phase` to avoid crack healing during unloading. The irreversibility condition, formally expressed as $\dot\phi\geq0$, is then fulfilled by setting

$$
\psi^{+}=\operatorname{max}(\psi^{+}_{n},\psi^{+}_{n+1})
$$

### Weak form

For the linear damage model we write the functional as

$$
  \begin{aligned}
    \Pi (\bm u, \phi) &= \bar{W}(\bm u, \phi) - \Pi_{\text{ext}} (\bm u), \\
    \Pi_{\text{ext}} (\bm u) &= \int_{\Omega}{\bm{u} \cdot \rho \bm{g}} \, dv + \int_{\partial \Omega}{\bm{u} \cdot \bar{\bm{t}}} \, da
  \end{aligned}
$$ (linear-damage-functional)

and by invoking the stationarity of $\Pi(\bm u, \phi)$ with respect to $\bm u$ and $\phi$, we obtain the weak form as: Find $(\bm u, \phi) \in \mathcal{V} \times \mathcal{Q} \subset H^1 \left( \Omega \right) \times H^1 \left( \Omega \right)$, such that

$$
  \begin{aligned}
      R_u(\bm u, \phi) &\coloneqq \int_{\Omega} \nabla \bm v \tcolon \bm{\sigma}_{\text{degr}} \, dv - \bm L_{\text{ext}}(\bm v) =0, \qquad \forall v \in \mathcal{V} \\
      R_{\phi}(\bm u, \phi) &\coloneqq \int_{\Omega} \left( q \, H  + \nabla q \cdot \frac{2G_c l_0}{c_0}\nabla\phi \right) \, dv = 0, \qquad \forall q \in \mathcal{Q}
  \end{aligned}
$$ (linear-damage-weak-form)

where $\bm L_{\text{ext}}(\bm v) = \int_{\Omega}{\bm{v} \cdot \rho \bm{g}} \, dv + \int_{\partial \Omega}{\bm{v} \cdot \bar{\bm{t}}} \, da$ is the external body and traction forces with $\bm{\sigma}_{\text{degr}} \cdot \bm{n} =\bar{\bm{t}} \quad \text{on} \; \partial\Omega_N$, and we replaced the virtual displacement and phase field $\delta \bm u$ and $\delta \phi$ with the test functions $\bm v, q$, i.e., $\delta \bm u = \bm v, \, \delta \phi = q$.

The degraded Cauchy stress $\bm{\sigma}_\text{degr}$ and $H$ in {eq}`linear-damage-weak-form` are derived as

$$
  \begin{aligned}
    \bm{\sigma}_{\text{degr}} &= \frac{\partial \left(g(\phi) \psi^{+} +\psi^{-} \right)}{\partial \bm u} =\begin{cases}
      g(\phi) \, \bm{\sigma},  \qquad \qquad \quad \text{if} \quad \trace \bm\varepsilon \geq 0
      \\
      \bm{\sigma}_{\text{vol}} + g(\phi) \, \bm{\sigma}_{\text{dev}}, \quad \text{if}  \quad \trace \bm\varepsilon < 0
      \end{cases}, \\
    \bm{\sigma} &= \bm{\sigma}_{\text{vol}} + \bm{\sigma}_{\text{dev}} = \bulk \trace(\bm\varepsilon) \bm{I} + 2 \mu \, \bm\varepsilon_{\text{dev}}
  \end{aligned}
$$ (linear-degraded-stress)

$$
  H = g'(\phi) \psi^{+} + \frac{G_c}{c_0l_0}\alpha'(\phi).
$$ (linear-H-term)

Alongside boundary conditions for displacement ($\bm{u} = \bar{u} \; \text{on} \; \partial\Omega_D$ and $\bm{\sigma}_{\text{degr}} \cdot \bm{n} =\bar{\bm{t}} \quad \text{on} \; \partial\Omega_N$) we have,

$$
\phi = 1 \quad \text{on} \; \Gamma \quad , \quad \nabla\phi \cdot \bm n=0 \quad \text{on} \; \partial\Omega_N
$$

::: {note}
The so-called (non-variationally consistent) hybrid formulation in {cite}`ambati2015review`, whereby $\bm{\sigma}_{\text{degr}} = g(\phi)\bm{\sigma}$, has shown to perform well in tensile loading conditions when staggered (i.e. alternate minimization) solution schemes are adopted.
:::

### Newton linearization

As explained in {ref}`Newton-linearization`, we need the Jacobian form of the {eq}`linear-damage-weak-form` which is:
Find $(\diff \bm{u}, \diff \phi) \in \mathcal{V} \times \mathcal{Q}$ such that

$$
  \begin{aligned}
    \int_{\Omega} \nabla \bm{v} \tcolon \diff \bm{\sigma}_{\text{degr}} dv &= -R_u, \quad \forall \bm{v} \in \mathcal{V}, \\
    \int_{\Omega} \left( q \diff H + \nabla q \cdot \frac{2G_c l_0}{c_0}\nabla \diff \phi \right) \, dv &= -R_{\phi}, \quad \forall q \in \mathcal{Q}
  \end{aligned}
$$ (linear-damage-linearization)

with

$$
  \diff H = \frac{\partial H}{\partial\bm u} \diff \bm u +\frac{\partial H}{\partial\phi} \diff \phi = g'(\phi) \diff \psi^{+} + g^{''}(\phi) \diff \phi \, \psi^{+} + \frac{G_c}{c_0l_0} \alpha^{''}(\phi) \, \diff \phi,
$$ (linear-H-term-linearization)

$$
  \begin{aligned}
    \diff \bm{\sigma}_{\text{degr}} &= \begin{cases}
      g(\phi) \, \diff \bm{\sigma} + g' \diff \phi \, \bm{\sigma},  \qquad \qquad \qquad \text{if} \quad \trace \bm\varepsilon \geq 0
      \\
      \diff \bm{\sigma}_{\text{vol}} + g(\phi) \, \diff \bm{\sigma}_{\text{dev}} + g' \diff \phi \, \bm{\sigma}_{\text{dev}}, \quad \text{if}  \quad \trace \bm\varepsilon < 0
      \end{cases}, \\
    \diff \bm{\sigma} &= \diff\bm{\sigma}_{\text{vol}} + \diff\bm{\sigma}_{\text{dev}} = \bm{\sigma}_{\text{vol}}(\diff \bm u) + \bm{\sigma}_{\text{dev}}(\diff \bm u). \\
  \end{aligned}
$$ (linear-degraded-stress-linearization)

where $\bm{\sigma}$ is defined in {eq}`linear-degraded-stress`. The term $\diff \psi^+$ is linked to crack irreversibility condition. If $\diff \psi^{+}_{n+1} < \psi^{+}_{n}$, $\diff \psi^{+}=0$, else

$$
  \diff \psi^{+} = \begin{cases}
      \bm{\sigma} \tcolon \diff \bm\varepsilon ,  \qquad \text{if} \quad \trace \bm\varepsilon \geq 0
      \\
      \bm{\sigma}_{\text{dev}} \tcolon \diff \bm\varepsilon, \quad \text{if}  \quad \trace \bm\varepsilon < 0
      \end{cases}
$$ (linear-Psi-plus-linearization)

::: {note}
The current implementation uses a single field of components $u_x$, $u_y$, $u_z$, and $\phi$.
:::

## AT1 and AT2 models

Quadratic, cubic, and quartic expressions for $g(\phi)$ have been proposed in the literature, as reviewed in {cite}`kuhn2015degradation`. The selected function should decrease monotonically between $g(0)=1$ and $g(1)=0$, and be $g'(1)=0$. Here, we use the widely adopted expression

$$
g(\phi) = (1 - \eta)(1 - \phi)^2 + \eta
$$ (degr-funct)

where $\eta \ll 1$ prevents numerical instabilities associated with the otherwise complete loss of stiffness for $\phi=1$.

Unlike that of $g(\phi)$, the choice of $\alpha(\phi)$ significantly influences predictions of damage evolution and overall mechanical response. Following Ambrosio and Tortorelli, the two standard models have been termed AT1, in which $\alpha(\phi)=\phi$, and AT2 model, in which $\alpha(\phi)=\phi^2$. The two yields $c_0=8/3$ and $c_0=2$, respectively.

In AT2, damage starts accumulating as load is applied and the initial response is non-linear for any material. In contrast, AT1 introduces a threshold for $\psi^{+}$ below which damage does not occur {cite}`tanne2018crack`. The  presence of an elastic stage makes AT1 appear most suitable to model the mechanical behaviour of materials in which damage is delayed by inelastic phenomena, such as in elastoplastic and viscoelastic materials.

::: {note}
Adopting AT2 with degradation function {eq}`degr-funct` guarantees $0\leq\phi\leq1$ {cite}`miehe2010thermodynamically`. For other  combinations, SNES's semi-smooth solver for variational inequalities can be used to bound $\phi$, e.g. {cite}`abali2021novel`.
:::

To complete the linearization given in {eq}`linear-damage-linearization` we need the first and second derivatives of $g(\phi)$

$$
  g'(\phi) = -2(1 - \eta)(1 - \phi), \qquad g^{''}(\phi) = 2(1 - \eta),
$$

and $\alpha(\phi)$ which for **AT1** are

$$
  \alpha'(\phi) = 1, \qquad \alpha^{''}(\phi) = 0,
$$

and for **AT2** are

$$
  \alpha'(\phi) = 2 \phi, \qquad \alpha^{''}(\phi) = 2.
$$

### Viscous regularization
To prevent convergence loss at solution jumps associated with an abrupt crack propagation, a viscous regularization is implemented to introduce a damping force opposing rapid changes in damage {cite}`miehe2010phase` with (damage) viscosity $\xi$.
Expressions for $H(\phi)$ and $\diff H(\phi)$ above are updated to include the viscous term as

$$
  H(\phi)= g'(\phi)\psi^{+} + \frac{G_c}{c_0l_0}\alpha'(\phi) + \xi \frac{\partial \phi}{\partial t}
$$

and

$$
  \diff H= g^{''}(\phi) \psi^{+} \diff \phi + g'(\phi) \diff \psi^{+} + \frac{G_c}{c_0l_0} \alpha^{''}(\phi) \diff \phi + \xi * \mathrm{shift_v} \diff \phi
$$

with $\mathrm{shift_v}$ evaluated at time  $\mathrm{shift_v} = \frac{\partial\dot{\phi}}{\partial\phi}|_{\phi_n}$ (see {eq}`time-dependent-jacobian`).

## Hyperelasticity, initial configuration

To extend the small strain model described in previous section, we need to use hyperelastic strain energy. For example, using volumetric-isochoric split for Neo-Hookean material defined in {eq}`mixed-neo-hookean-energy`, we can write $\psi^{+}$ and $\psi^{-}$ as

$$
\psi^{+}=
\begin{cases}
       \psi_{\text{vol}} + \psi_{\text{iso}} = \bulk \, V(J) + \frac \secondlame 2 \left( \mathbb{\bar{I}_1} - 3 \right) \quad \text{if}  \quad J - 1 \geq 0
\\
      \psi_{\text{iso}} = \frac \secondlame 2 \left( \mathbb{\bar{I}_1} - 3 \right) \qquad \qquad \qquad \qquad \text{if}  \quad J - 1 < 0
\end{cases}
$$ (neo-hookean-Psi-plus)

$$
\psi^{-}=
\begin{cases}
      0  \qquad  \quad \text{if}  \quad J - 1 \geq 0
\\
      \psi_{\text{vol}} \qquad \text{if}  \quad J - 1 < 0
\end{cases}
$$ (neo-hookean-Psi-minus)

Substituting above energies into {eq}`damage-total-energy-anisotropy` and taking derivative with respect to $\bm u$ and $\phi$ gives the weak form for hyperelastic model as

$$
  \begin{aligned}
      R_u(\bm u, \phi) &\coloneqq \int_{\Omega_0} \nabla_X \bm v \tcolon \bm{F}\bm{S}_{\text{degr}} \, dV - \bm L_{\text{ext}}(\bm v) =0, \qquad \forall v \in \mathcal{V} \\
      R_{\phi}(\bm u, \phi) &\coloneqq \int_{\Omega_0} \left( q \, H  + \nabla_X q \cdot \frac{2G_c l_0}{c_0}\nabla_X \phi \right) \, dV = 0, \qquad \forall q \in \mathcal{Q}
  \end{aligned}
$$ (hyperelastic-damage-weak-form)

where $\bm L_{\text{ext}}(\bm v)$ is the external force, and the degraded stress is defined by

$$
  \begin{aligned}
    \bm{S}_{\text{degr}} &=\begin{cases}
      g(\phi) \, \bm{S},  \qquad \qquad \qquad \qquad \text{if} \quad J - 1 \geq 0
      \\
      \bm{S}_{\text{vol}} + g(\phi) \, \bm{S}_{\text{iso}}, \qquad \qquad \quad \text{if}  \quad J - 1 < 0
      \end{cases}, \\
    \bm{S} &= \bm{S}_{\text{vol}} + \bm{S}_{\text{iso}} = \bulk J \, V' \, \bm{C}^{-1} + \secondlame J^{-2/3}\left( \bm{I} - \frac{1}{3}\mathbb{{I}_1}\bm{C}^{-1} \right)
  \end{aligned}
$$ (hyperelastic-degraded-stress)

where we have used {eq}`isochoric-neo-hookean-stress`. The linearization of {eq}`hyperelastic-damage-weak-form` can be written as

$$
  \begin{aligned}
    \int_{\Omega_0} \nabla_X \bm{v} \tcolon \left(\diff \bm{F} \bm{S}_{\text{degr}} + \bm{F} \diff \bm{S}_{\text{degr}} \right) dV &= -R_u, \quad \forall \bm{v} \in \mathcal{V}, \\
    \int_{\Omega_0} \left( q \diff H + \nabla_X q \cdot \frac{2G_c l_0}{c_0}\nabla_X \diff \phi \right) \, dV &= -R_{\phi}, \quad \forall q \in \mathcal{Q}
  \end{aligned}
$$ (hyperelastic-damage-linearization)

with

$$
  \begin{aligned}
    \diff \bm{S}_{\text{degr}} &= \begin{cases}
      g(\phi) \, \diff \bm{S} + g' \diff \phi \, \bm{S},  \qquad \qquad \qquad \text{if} \quad J - 1 \geq 0
      \\
      \diff \bm{S}_{\text{vol}} + g(\phi) \, \diff \bm{S}_{\text{iso}} + g' \diff \phi \, \bm{S}_{\text{iso}}, \quad \text{if}  \quad J - 1 < 0
      \end{cases}, \\
    \diff \bm{S} &= \diff\bm{S}_{\text{vol}} + \diff\bm{S}_{\text{iso}}. \\
  \end{aligned}
$$ (hyperelastic-degraded-stress-linearization)

where $\diff\bm{S}_{\text{iso}}$ and $\diff\bm{S}_{\text{vol}}$ are derived in {eq}`dS-iso-neo-hookean` and {eq}`dS-vol-isochoric-neo-hookean`, respectively. Note that adding viscosity for hyperelastic will be similar to small strain case, but in $H$ and its linearization $\diff H$ we need to use the Neo-Hookean energy given in {eq}`neo-hookean-Psi-plus` and its linearization

$$
  \diff \psi^{+} = \begin{cases}
      \bm{S} \tcolon \diff \bm{E} ,  \qquad \text{if} \quad J - 1 \geq 0
      \\
      \bm{S}_{\text{iso}} \tcolon \diff \bm{E}, \quad \text{if}  \quad J - 1 < 0
      \end{cases}
$$ (neo-hookean-Psi-plus-linearization)

## Hyperelasticity, current configuration

To write hyperelasticity damage model in current configuration we need to compute the invariants of left Cauchy-Green tensor $\bm b$ in {eq}`neo-hookean-Psi-plus` and write the residual as

$$
  \begin{aligned}
      r_u(\bm u, \phi) &\coloneqq \int_{\Omega_0} \nabla_x \bm v \tcolon \bm{\tau}_{\text{degr}} \, dV - \bm L_{\text{ext}}(\bm v) =0, \qquad \forall v \in \mathcal{V} \\
      r_{\phi}(\bm u, \phi) &\coloneqq \int_{\Omega_0} \left( q \, H  + \nabla_x q \cdot \frac{2G_c l_0}{c_0} \bm{b} \nabla_x \phi \right) \, dV = 0, \qquad \forall q \in \mathcal{Q}
  \end{aligned}
$$ (hyperelastic-damage-weak-form-current)

where we have used $\nabla_X \phi = \bm{F}^T \nabla_x \phi$, and the degraded stress $\bm{\tau}_{\text{degr}}$ is defined by

$$
  \begin{aligned}
    \bm{\tau}_{\text{degr}} &=\begin{cases}
      g(\phi) \, \bm{\tau},  \qquad \qquad \qquad \qquad \text{if} \quad J - 1 \geq 0
      \\
      \bm{\tau}_{\text{vol}} + g(\phi) \, \bm{\tau}_{\text{iso}}, \qquad \qquad \quad \text{if}  \quad J - 1 < 0
      \end{cases}, \\
    \bm{\tau} &= \bm{\tau}_{\text{vol}} + \bm{\tau}_{\text{iso}} = \bulk J \, V' \, \bm{I} + \secondlame J^{-2/3}\left( \bm{b} - \frac{1}{3}\mathbb{{I}_1}\bm{I} \right)
  \end{aligned}
$$ (hyperelastic-degraded-tau)

where we have used {eq}`isochoric-neo-hookean-tau`. The linearization of {eq}`hyperelastic-damage-weak-form-current` can be written as

$$
  \begin{aligned}
    \int_{\Omega_0} \nabla_x \bm{v} \tcolon \left( \diff \bm{\tau}_{\text{degr}} - \bm{\tau}_{\text{degr}} \left( \nabla_x \diff \bm{u} \right)^T \right) dV &= -r_u, \quad \forall \bm{v} \in \mathcal{V}, \\
    \int_{\Omega_0} \left( q \diff H + \nabla_x q \cdot \frac{2G_c l_0}{c_0} \bm{b} \nabla_x \diff \phi \right) \, dV &= -r_{\phi}, \quad \forall q \in \mathcal{Q}
  \end{aligned}
$$ (hyperelastic-damage-linearization-current)

where $\diff \bm{\tau}_{\text{degr}} - \bm{\tau}_{\text{degr}} \left( \nabla_x \diff \bm{u} \right)^T = \nabla_x \diff \bm{u} \ \bm{\tau}_{\text{degr}} + \bm{F} \diff \bm{S}_{\text{degr}} \bm{F}^T$ and

$$
  \begin{aligned}
    \bm{F}\diff \bm{S}_{\text{degr}} \bm{F}^T &= \begin{cases}
      g(\phi) \, \bm{F}\diff \bm{S} \bm{F}^T + g' \diff \phi \, \bm{\tau},  \qquad \qquad \qquad \text{if} \quad J - 1 \geq 0
      \\
      \bm{F} \diff \bm{S}_{\text{vol}}\bm{F}^T + g(\phi) \, \bm{F} \diff \bm{S}_{\text{iso}} \bm{F}^T + g' \diff \phi \, \bm{\tau}_{\text{iso}}, \quad \text{if}  \quad J - 1 < 0
      \end{cases}, \\
  \end{aligned}
$$ (hyperelastic-degraded-tau-linearization)

where $\bm{F}\diff\bm{S} \bm{F}^T$ is derived in {eq}`FdSFTranspose-NH` and finally

$$
  \diff \psi^{+} = \begin{cases}
      \bm{\tau} \tcolon \diff\bm\epsilon ,  \qquad \text{if} \quad J - 1 \geq 0
      \\
      \bm{\tau}_{\text{iso}} \tcolon \diff\bm\epsilon, \quad \text{if}  \quad J - 1 < 0
      \end{cases}
$$ (neo-hookean-Psi-plus-linearization-current)

(matrix-free-damage)=
## Matrix-free implementation

Ratel solves the momentum balance equations using unstructured high-order finite/spectral element spatial discretizations by matrix-free approach.
For phase-field model we use single field finite element formulation with four componenets. Using the matrix-free notation given in {eq}`residual-matrix-free` and {eq}`jacobian-matrix-free`, the residual and jacobian for four components case are

$$
  \langle\textbf{v},  \bm{R}(\textbf{u}) \rangle= \int_{\Omega_0}{\textbf{v} \cdot \bm{f}_0 (\textbf{u}, \nabla \textbf{u}) + \nabla \textbf{v} \tcolon \bm{f}_1 (\textbf{u}, \nabla \textbf{u})} \, dV = 0, \quad \forall \textbf{v} \in \mathcal{V},
$$ (residual-matrix-free-damage)

$$
  \langle\textbf{v},  \bm{J}(\textbf{u}) \diff \textbf{u} \rangle= \int_{\Omega_0}{\textbf{v} \cdot \diff \bm{f}_0 + \nabla \textbf{v} \tcolon \diff \bm{f}_1} \, dV, \quad \forall \textbf{v} \in \mathcal{V}
$$ (jacobian-matrix-free-damage)

where $\textbf{v} = \begin{bmatrix} \bm{v} & q \end{bmatrix} ^T$, $\textbf{u} = \begin{bmatrix} \bm{u} & \phi \end{bmatrix} ^T$, and the operators $\bm{f}_0$ and $\bm{f}_1$ contain all possible sources in the problem.

It should be noted that the gradient in the {eq}`residual-matrix-free-damage`, {eq}`jacobian-matrix-free-damage` depends on the configuration system and it could be with respect to initial configuration $\bm{X}$ i.e.,  $\nabla_X \bm{v}$ (Lagrangian approach) or current configuration $\bm{x}$ i.e.,  $\nabla_x \bm{v}$ (Eulerian approach).

Compare with governing equations derived in pervious sections for linear and large deformation, it is easy to verify that

* For linear phase field model described in {eq}`linear-damage-weak-form` we have

$$
  \bm f_0 = \begin{bmatrix} -\rho \bm g, & H \end{bmatrix} ^T, \quad \bm f_1 = \begin{bmatrix} \bm{\sigma}_{\text{degr}}, & \frac{2G_c l_0}{c_0}\nabla\phi \end{bmatrix} ^T, \\
  \diff \bm f_0 = \begin{bmatrix} 0, & \diff H \end{bmatrix} ^T, \quad \diff \bm f_1 = \begin{bmatrix} \diff \bm{\sigma}_{\text{degr}}, & \frac{2G_c l_0}{c_0}\nabla \diff \phi \end{bmatrix} ^T,
$$

* For hyperelastic phase field model in initial configuration described in {eq}`hyperelastic-damage-weak-form` and {eq}`hyperelastic-damage-linearization` we have

$$
  \bm f_0 = \begin{bmatrix} -\rho_0 \bm g, & H \end{bmatrix} ^T, \quad \bm f_1 = \begin{bmatrix} \bm F \bm{S}_{\text{degr}}, & \frac{2G_c l_0}{c_0}\nabla_X\phi \end{bmatrix} ^T, \\
  \diff \bm f_0 = \begin{bmatrix} 0, & \diff H \end{bmatrix} ^T, \quad \diff \bm f_1 = \begin{bmatrix} \diff \bm F \bm S_{\text{degr}} + \bm F \diff \bm S_{\text{degr}}, & \frac{2G_c l_0}{c_0}\nabla_X \diff \phi \end{bmatrix} ^T,
$$

* For hyperelastic phase field model in current configuration derived in {eq}`hyperelastic-damage-weak-form-current` and {eq}`hyperelastic-damage-linearization-current`

$$
  \bm f_0 = \begin{bmatrix} -\rho_0 \bm g, & H \end{bmatrix} ^T, \quad \bm f_1 = \begin{bmatrix} \bm{\tau}_{\text{degr}}, & \frac{2G_c l_0}{c_0} \bm{b} \nabla_x\phi \end{bmatrix} ^T, \\
  \diff \bm f_0 = \begin{bmatrix} 0, & \diff H \end{bmatrix} ^T, \quad \diff \bm f_1 = \begin{bmatrix} \diff \bm{\tau}_{\text{degr}} - \bm{\tau}_{\text{degr}} \left( \nabla_x \diff \bm{u} \right)^T, & \frac{2G_c l_0}{c_0} \bm{b} \nabla_x \diff \phi \end{bmatrix} ^T,
$$
