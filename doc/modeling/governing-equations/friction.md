# Friction Models

Friction is a complex phenomenon resulting in resistance to motion of two contacting surfaces.
In general, models describing friction can be divided into two categories: *static* friction models, which attempt to describe the steady-state behavior of frictional contact, and *dynamic* friction models, which model friction as the direct result of a dynamical system.
Ratel supports multiple static models for approximating the friction force of a contact interaction, which provide trade-offs in numerical stability and accuracy.

The notation and formulation for the friction models below are from {cite}`marques2016`.

## Static Friction Models
Static friction models approximate the complex interaction between two contacting surfaces which result in friction as a force $\bm F$ with magnitude dependent on the forces normal to the contact interaction in the direction opposing the relative motion of the surfaces tangent to the contact interaction.

### Coulomb friction
One of the simplest friction formulations is Coulomb friction, which, for non-zero tangential velocities, sets the magnitude of the frictional force to be $F_C = -\mu_k \sigma_n(\bm u)$, where $\hat{\sigma}_n(\bm u)$ is the normal contact force as defined in {eq}`nitsche-normal-force`, $\mu_k$ is the kinetic coefficient of friction, and $\left[ x \right] = \frac{1}{2} \left(x - \lvert x \rvert \right)$ is the projection onto the negative reals.
For non-zero tangential velocities, the direction of the frictional force is

$$
\sgn(\bm{v_{\text{T}}}) = \begin{cases}
  \frac{\bm{v_{\text{T}}}}{\|\bm{v_{\text{T}}}\|},& \quad \text{if } \|\bm{v_{\text{T}}}\| \neq 0;\\
  \bm 0,& \quad \text{if } \|\bm{v_{\text{T}}}\| = 0;
\end{cases}
$$ (sgn-func)

where $\bm{v_{\text{T}}} = (I - \bm n \otimes \bm n) \bm v$ is the relative tangential velocity with respect to the other surface, which has the normal vector $\bm n$.

Special consideration is required for the $\|\bm{v_{\text{T}}}\| = 0$ case, as this represents the "sticking" region, where the frictional force is high enough to prevent any relative motion.
The treatment of the so-called "null-velocity" region is one of the major differentiating factors for different static friction models.

Coulomb friction relies on the tangential forces external to the contact interaction, $\bm F_e$ to define the frictional force at null-velocity.
That is, the frictional force in the Coulomb model is given by,

$$
\bm F = \begin{cases}
  F_C \sgn(\bm{v_{\text{T}}}),& \quad \text{if } \|\bm{v_{\text{T}}}\| \neq 0;\\
  \min(F_C, \|\bm F_e\|) \sgn(\bm F_e),& \quad \text{if } \|\bm{v_{\text{T}}}\| = 0.
\end{cases}
$$ (coulomb-friction)

#### Viscous damping
Coulomb friction can be extended to allow for viscous damping, which is particularly relevant to lubricated contact.
In Ratel, we implement viscous damping which is linearly dependent on the tangential velocity:

$$
\bm F = \begin{cases}
  F_C \sgn(\bm{v_{\text{T}}}) + F_v \bm{v_{\text{T}}},& \quad \text{if } \|\bm{v_{\text{T}}}\| \neq 0;\\
  \min(F_C, \|\bm F_e\|) \sgn(\bm F_e),& \quad \text{if } \|\bm{v_{\text{T}}}\| = 0;
\end{cases}
$$ (coulomb-viscous-friction)

where $F_v$ is the viscous damping coefficient.

#### Command-line options
The relevant command-line options for Coulomb friction are given in Table [Coulomb Friction Options](coulomb_friction_options) below.
For brevity, the string `[prefix]` is used in place of the full prefix `bc_platen_[platen_name]` in the option listings.

(coulomb_friction_options)=

:::{list-table} Coulomb Friction Options
:header-rows: 1
:widths: 40 50 10
* - Option
  - Description
  - Default Value

* - `-[prefix]_friction_type coulomb`
  - Required to enable Coulomb friction for a particular contact boundary condition.
  -

* - `-[prefix]_friction_kinetic [non-negative real]`
  - Kinetic coefficient of friction $\mu_k$, unitless
  - `0.1`

* - `-[prefix]_friction_viscous [non-negative real]`
  - Viscous damping coefficient $F_v$, units of $\frac{\mathrm{N}\cdot\mathrm{s}}{\mathrm{m}}$
  - `0.0`
:::

### Threlfall friction
One of the largest weaknesses of the Coulomb friction model is the discontinuity at $\|\bm{v_{\text{T}}}\| = 0$.
The Threlfall model addresses this weakness by defining a tolerance velocity $v_0$ below which an exponential model is used to compute the frictional force.
The model is given as:

$$
\bm F = \begin{cases}
      F_C \left(\frac{1 - \exp\left(\frac{-3 \|\bm{v_{\text{T}}}\|}{v_0}\right)}{1 - \exp(-3)}\right) \sgn(\bm{v_{\text{T}}}), & \text{if } \|\bm{v_{\text{T}}}\| \leq v_0; \\
      F_C \ \sgn(\bm{v_{\text{T}}}), & \text{if } \|\bm{v_{\text{T}}}\| > v_0,,
\end{cases}
$$ (threlfall-viscous)

Note that as $v_0 \to 0$, the Threlfall model approaches the Coulomb model; that is, lower tolerance velocities more accurately approximate the discontinuity.


#### Viscous damping
Threlfall friction can also be extended to include viscous damping:

$$
\bm F = \begin{cases}
      F_C \left(\frac{1 - \exp\left(\frac{-3 \|\bm{v_{\text{T}}}\|}{v_0}\right)}{1 - \exp(-3)}\right) \sgn(\bm{v_{\text{T}}}), & \text{if } \|\bm{v_{\text{T}}}\| \leq v_0; \\
      F_C \ \sgn(\bm{v_{\text{T}}}) + F_v \ (\|\bm{v_{\text{T}}}\| - v_0) \sgn(\bm{v_{\text{T}}}), & \text{if } \|\bm{v_{\text{T}}}\| > v_0.
\end{cases}
$$ (threlfall-viscous-friction)

Note that due to the smoothed transition from null-velocity, the viscous damping is not active until $\|\bm{v_{\text{T}}}\| > v_0$.
As such, the magnitude of the damping must be rescaled accordingly to $\|\bm{v_{\text{T}}}\| - v_0$.



#### Command-line options
The relevant command-line options for Threlfall friction are given in Table [Threlfall Friction Options](threlfall_friction_options) below.
For brevity, the string `[prefix]` is used in place of the full prefix `bc_platen_[platen_name]` in the option listings.

(threlfall_friction_options)=

:::{list-table} Threlfall Friction Options
:header-rows: 1
:widths: 40 50 10
* - Option
  - Description
  - Default Value

* - `-[prefix]_friction_type threlfall`
  - Required to enable Threlfall friction for a particular contact boundary condition.
  -

* - `-[prefix]_friction_kinetic [non-negative real]`
  - Kinetic coefficient of friction $\mu_k$, unitless
  - `0.1`

* - `-[prefix]_friction_viscous [non-negative real]`
  - Viscous damping coefficient $F_v$, units of $\frac{\mathrm{N}\cdot\mathrm{s}}{\mathrm{m}}$
  - `0.0`

* - `-[prefix]_friction_tolerance_velocity [positive real]`
  - Tolerance velocity $v_0$ at which full frictional slipping is permitted, units of $\frac{\mathrm{m}}{\mathrm{s}^2}$. Only used by Threlfall friction model.
  - `0.05`
:::

## Newton linearization

To derive the newton linearization for friction model we start by

$$
\diff \sgn(\bm{v_{\text{T}}}) = \begin{cases}
  \frac{1}{\|\bm{v_{\text{T}}}\|} \left(\diff \bm{v_{\text{T}}} - \frac{\bm{v_{\text{T}}} \cdot \diff \bm{v_{\text{T}}}}{\|\bm{v_{\text{T}}}\|} \frac{\bm{v_{\text{T}}}}{\|\bm{v_{\text{T}}}\|} \right), & \quad \text{if } \|\bm{v_{\text{T}}}\| \neq 0;\\
  \bm 0,& \quad \text{if } \|\bm{v_{\text{T}}}\| = 0;
\end{cases}
$$ (sgn-func-linearization)

where $\diff \bm{v_{\text{T}}} = \mathrm{shift_v} \diff \bm{u_{\text{T}}}$, $\diff \bm{u_{\text{T}}} = (I - \bm n \otimes \bm n) \diff \bm u$ and we have used

$$
  \diff \|\bm{v_{\text{T}}}\| = \frac{\bm{v_{\text{T}}} \cdot \diff \bm{v_{\text{T}}}}{\|\bm{v_{\text{T}}}\|} = \sgn(\bm{v_{\text{T}}}) \cdot \diff \bm{v_{\text{T}}}.
$$

The linearization of {eq}`coulomb-friction` can be found in {ref}`Platen-contact`. For the Threlfall model, the linearization for the viscous case {eq}`threlfall-viscous-friction` is

$$
\diff \bm F = \begin{cases}
      F_C \left(\frac{\frac{3 \diff \|\bm{v_{\text{T}}}\|}{v_0} \exp\left(\frac{-3 \|\bm{v_{\text{T}}}\|}{v_0}\right)}{1 - \exp(-3)}\right) \sgn(\bm{v_{\text{T}}}) + \left(\frac{1 - \exp\left(\frac{-3 \|\bm{v_{\text{T}}}\|}{v_0}\right)}{1 - \exp(-3)}\right) \left(F_C \diff \sgn(\bm{v_{\text{T}}}) + \diff F_C \sgn(\bm{v_{\text{T}}})\right), & \text{if } \|\bm{v_{\text{T}}}\| \leq v_0; \\
      F_C \diff \sgn(\bm{v_{\text{T}}}) + \diff F_C \sgn(\bm{v_{\text{T}}}) + F_v \left(\diff \bm{v_{\text{T}}} - v_0 \diff \sgn(\bm{v_{\text{T}}}) \right), & \text{if } \|\bm{v_{\text{T}}}\| > v_0.
\end{cases}
$$ (threlfall-viscous-friction-linearization)

where we have used $\|\bm{v_{\text{T}}}\| \sgn(\bm{v_{\text{T}}}) = \bm{v_{\text{T}}}$ and

$$
\diff F_C = -\mu_k \diff \hat{\sigma}_n(\bm u) = -\mu_k \left[-(\diff \bm P \cdot \bm N)\cdot n_p + \gamma \diff g \right]_{\mathbb{R}^-}, \quad \diff g = \bm{n}_p \cdot \diff \bm u,
$$

and without viscous case can be derived by setting $F_v = 0$.

## Dynamic Friction Models
Ratel does not currently implement any dynamic friction models.
