# Isotropic Elastoplasticity in Logarithmic Strains Space

:::{note}
The formulation of rate-independent isotropic elastoplasticity at large strains and derived algorithms are based on the pioneer work by {cite}`eterovic1990` and {cite}`weber1990`.
:::

## General Framework

In elastoplasticity, the stress state depends on internal variable(s) associated with dissipative deformation mechanisms. The resulting strain path dependency is generally dealt with by introducing sufficiently small time steps for the problem to be considered path independent within one time increment $[t, t_{n+1}]$ and updating stresses implicitly via plastic corrector (or return-mapping) algorithms.
To ensure quadratic convergence of the Newton's method, the tangent modulus must be obtained by linearization of the algorithmic expression for the stress update procedure {cite}`simo1985`, which depends on the specific constitutive model.
Following the formalism of thermodynamics with internal variables, these laws are derived from postulated free energy potential $\psi$, dissipation potential $\Xi$, and yield function $\bm{\phi}$ {cite}`gurtin2010`.

### Free Energy Potential

In hyperelasticity in logarithmic strain space, the free energy potential $\psi$ can be expressed as a function of the (Eulerian) logarithmic strain tensor $\textbf{e}^e$ using the same functional format of the small-strains counterpart

$$
\psi(\textbf{e}^e)=\frac{1}{2}\textbf{e}^e:\textbf{D}:\textbf{e}^e
$$ (free-energy-potential)

where $\textbf{D}$ the fourth order elasticity tensor of small-strains (linear) elasticity.
As in {eq}`Seth-Hill-strain-measures`, $\textbf{e}^e$ can be equally defined via the (elastic) left stretch tensor $\textbf{v}^e$ or the (elastic) left Cauchy-Green strain tensor $\bm{b}^e=(\textbf{v}^e)^2=\bm{F}^e(\bm{F}^e)^T$

$$
\textbf{e}^e \equiv \log{\textbf{v}^e}=\frac{1}{2}\log\bm{b}^e
$$ (logarithmic-strain-tensor)

The (symmetric) Kirchhoff stress tensor $\bm{\tau}\equiv2\frac{\partial\psi}{\partial\bm{b}^e}\bm{b}^e$ can thus be expressed as

$$
\bm{\tau}=2\frac{\partial\psi}{\partial\bm{b}^e}\bm{b}^e=\frac{\partial\psi}{\partial\textbf{e}^e}:\frac{\partial\log\bm{b}^e}{\partial\bm{b}^e}\bm{b}^e
$$ (kirchhoff-stress-chain-rule)

Assuming isotropic elastic response and taking advantage of certain properties of derivatives of isotropic tensor functions, the following identity can be derived {cite}`desouzaneto1998`

$$
\frac{\partial\psi}{\partial\textbf{e}^e}:\frac{\partial\log\bm{b}^e}{\partial\bm{b}^e}\bm{b}^e=\frac{\partial\psi}{\partial\textbf{e}^e}
$$ (isotropic-tensor-function-identity)

Substituting {eq}`isotropic-tensor-function-identity` into {eq}`kirchhoff-stress-chain-rule` finally gives

$$
\bm{\tau}=\frac{\partial\psi}{\partial\textbf{e}^e}=\textbf{D}:\textbf{e}^e
$$ (kirchhoff-stress-log-strains)

which similarly retains the functional format of linear elasticity.

In elastoplasticity, the free energy potential also depends on a set of $k$ internal variables $\bm{\alpha}=\{\alpha_1, ..., \alpha_k\}$ such that $\psi=\psi(\textbf{e}^e, \bm{\alpha})$. Differentiation with respect to these variables defines so-called thermodynamic forces $A_i\space$ that drive plastic deformation and are work conjugates of the internal variables $\alpha_i\space$,

$$
A_i=\frac{\partial\psi}{\partial\alpha_i}
$$ (work-conjugates)

:::{admonition} Multiplicative decomposition of deformation gradient
Supported by experimental observations of plastic deformation in crystalline materials (e.g., metals), the deformation gradient tensor $\bm{F}= \bm{I}+\nabla_X\bm{u}$ can be conveniently decomposed into a plastic $\bm{F}^p$ and elastic part $\bm{F}^e$ such that {cite}`lee1969`

$$
\bm{F}=\bm{F}^e\bm{F}^p
$$ (def-grad-multiplicative-decomposition)

Plastic deformation is assumed isochoric (i.e., $\det\bm{F}^p=1$) and mapping material points to an unstressed intermediate configuration.
Hence, $\bm{F}^e = \textbf{v}^e \bm{R}^e$  maps the intermediate configuration to the spatial configuration via rigid rotations $\bm{R}^e$ and elastic distortions.
With {eq}`def-grad-multiplicative-decomposition`, the spatial quantity describing the evolution of $\bm{F}$, $\bm{l}\equiv\bm{\dot{F}}\bm{F}^{-1}$, can be additively decomposed as

$$
\bm{l}=\bm{l}^e+\bm{F}^e\bm{L}^p(\bm{F}^e)^{-1}
$$ (velocity-gradient)

where $\bm{l}^e\equiv\dot{\bm{F}^e}(\bm{F}^e)^{-1}$ and $\bm{L}^p\equiv\dot{\bm{F}^p}(\bm{F}^p)^{-1}$, which lives in the intermediate configuration.
To formulate the so-called (plastic) flow rule, it is convenient to further split $\bm{L}^p$ into its symmetric $\bm{D}^p\equiv\text{sym}(\bm{L}^p)$ and skew $\bm{W}^p\equiv\text{skew}(\bm{L}^p)$ parts.
So defined, the plastic stretching tensor $\bm{D}^p$ measures the instantaneous rate of plastic stretching along the orthonormal eigenvectors in the intermediate configuration.
On the other hand, $\bm{W}^p$ represents the triad's rigid spinning.
:::

:::{admonition} Additive decomposition of strain
As an alternative to the multiplicative decomposition, we can take an additive decomposition of the strain tensor which has comparable accuracy at small strains {cite}`miehe2002anisotropic` and co-axial loading conditions (e.g. pure shear). For studies on the softening effect of additive plasticity in non-coaxial loading conditions see {cite}`peric1992` and the more recent review in {cite}`friedlein2022`.
For additive plasticity, the Lagrangian strain $\bm{E}$ is broken into elastic and plastic parts:

$$
  \bm{E} = \bm{E}^e + \bm{E}^p
$$ (eq-additive-plasticity)

where $\bm{E}$ is the strain (for the linear case we use linear strain); in logarithmic space, using a geometric preprocessor, $\bm{E}^e$ becomes:

$$
  \bm{E} = \frac{1}{2} \log \left( \bm{C} \right).
$$ (eq-green-lagrange-strain-log)

From Miehe et al. {cite}`miehe2002anisotropic`, $\bm{E}^p$ is defined as follows:

$$
  \begin{aligned}
    \bm{E}^p &= \frac{1}{2} \log \left( \bm{C}^p \right), \\
    \bm{C}^p &= (\bm{F}^p)^T\bm{F}^p
  \end{aligned}
$$ (plastic-metric)

where $\bm{C}^p$ is the plastic part of the Cauchy-Green tensor (also refered to as the plastic metric).

The Second Piola-Kirchhoff stress, $\bm{S}$, now becomes

$$
  \bm{S} \left( \bm{E} \right) = \frac{\partial \psi}{\partial \bm{E}^e} \left( \bm{E}^e, \bm{\alpha} \right)
$$ (eq-strain-energy-grad-plastic)
:::

### Yield Function
The yield function $\phi (\bm{\tau}, \bm{\textbf{A}}, \bm{\alpha})$ is used to define the set of admissible (i.e., physically possible given the current value of the internal variables) stress states $\mathscr{S}$ as

$$
\mathscr{S} = \{ \phi (\bm{\tau}, \bm{\textbf{A}}, \bm{\alpha}) \leq 0 \}
$$ (yield-fuction)

In stress space, $\phi = 0$ defines a yield surface that delimits the elastic domain. Plastic deformation occurs when stress states lie on such surface and the flow vector points outwards. However, the incremental nature of applied load/displacements may cause stress states to fall outside $\mathscr{S}$.
To ensure admissibility, these are projected back to the yield surface.

### Dissipation Potential
 The non-negative and convex dissipation potential $\Xi(A, \bm\alpha)$ is used to state constitutive laws for the evolution of internal variables and the flow rule.
 To ensure thermodynamic principles are satisfied, a normality rule is assumed {cite}`halphen1975`:

$$
\dot{\bm{\alpha}}=-\dot\gamma\frac{\partial\Xi}{\partial\bm{\textbf{A}}}
$$ (normality-rule-disspipation-potential)

Where $\dot\gamma$ is the plastic multiplier calculated through optimality conditions

$$
\dot\gamma\geq0 \qquad \bm{\phi}\leq0 \qquad \dot\gamma\bm{\phi}=0
$$ (optimality-condition)

The plastic multiplier also appears in the flow rule, which is conveniently stated as constitutive laws for $\bm{D}^p$ (rotated to the spatial configuration via $\bm{R}^e$) and $\bm{W}^p$ as

$$
\bm{R}^e\bm{D}^p(\bm{R}^e)^T=\dot\gamma\frac{\partial\Xi}{\partial\bm{\tau}}
\medspace, \qquad
\bm{W}^p=\bm{0}
$$ (flow-rule)

Note that assumption of zero plastic spin in {eq}`flow-rule` is consistent with that of plastic isotropy.
Plastic isotropy dictates coaxiality, i.e. sharing of principal directions, between the flow vector ${\partial\Xi}/{\partial\bm{\tau}}$ in {eq}`flow-rule` and $\bm{\tau}$.
Assumption of isochoric plastic flow further implies that ${\partial\Xi}/{\partial\bm{\tau}}$ is a traceless tensor {cite}`gurtin2010`.
These properties help to greatly simplify the update of kinematic quantities as described next.

#### Exponential Map Integration
The exponential tensor function has desirable properties in the numerical integration of constitutive laws of (both isotropic and anisotropic) elastoplasticity {cite}`gurtin2010`.
In particular, it maps traceless tensor,$\space\bm{X}$, to unimodular tensors for which $\det{\bm{X}}=1$.
The function therefore preserves the isochoric properties of plastic deformation, the loss of which would otherwise require (nonphysical) ad-hoc corrections {cite}`borden2016`.
Using backward integration and via substitution of {eq}`def-grad-multiplicative-decomposition`, {eq}`velocity-gradient` and {eq}`flow-rule`, $\bm{F}^p_{n+1}$ at time $(t+\Delta{t})$ can be calculated as

$$
\bm{F}^p_{n+1}=\bm{R}_{n+1}^{e \space T}\exp(\Delta\gamma\frac{\partial\bm{\Xi}}{\partial\bm{\tau}}\vert_{n+1})\bm{R}_{n+1}^e\bm{F}_n^p
$$ (plastic-deformation-gradient-at-n+1)

Analogously, $\bm{F}^e_{n+1}$ can be expressed from {eq}`plastic-deformation-gradient-at-n+1` by introducing the deformation gradient increment $\bm{F}_\Delta=\bm{F}_{n+1}\bm{F}_n^{-1}$, which expresses the "difference" between deformations at different times.
Taking also advantage of the isotropic property of exponential function, it gives

$$
\bm{F}^e_{n+1}=\bm{F}_\Delta\bm{F}_n^e \bm{R}_{n+1}^{e \space T}\exp(-\Delta\gamma\frac{\partial\bm{\Xi}}{\partial\bm{\tau}}\vert_{n+1})\bm{R}_{n+1}^e
$$ (elastic-deformation-gradient-at-n+1)

Tentatively considering $\bm{F}_\Delta$ as purely elastic would generally lead to inadmissible stress states.
A trial elastic deformation increment is thus introduced as  $\bm{F}^{e \space tr}_{n+1}=\bm{F}_\Delta\bm{F}_n^e$ and {eq}`elastic-deformation-gradient-at-n+1` rewritten as:

$$
\bm{F}^e_{n+1}=\bm{F}^{e \space tr}_{n+1} \bm{R}_{n+1}^{e \space T}\exp(-\Delta\gamma\frac{\partial\bm{\Xi}}{\partial\bm{\tau}}\vert_{n+1})\bm{R}_{n+1}^e
$$ (elastic-deformation-gradient-at-n+1-simplified)

The update of kinematic quantities can be further simplified with manipulation via the stretch tensor $\bm{v}^e_{n+1}$ and {eq}`logarithmic-strain-tensor` to obtain

$$
\textbf{e}^e_{n+1} =\textbf{e}^{e \space tr}_{n+1} - \Delta\gamma\frac{\partial\bm{\Xi}}{\partial\bm{\tau}}\vert_{n+1}
$$ (elastic-deformation-gradient-n+1)

{eq}`elastic-deformation-gradient-n+1` is completed with the internal variables update

$$
\bm{\alpha}_{n+1} =\bm{\alpha}_n - \Delta\gamma\frac{\partial\bm{\Xi}}{\partial\bm{\textbf{A}}}\vert_{n+1}
$$ (internal-variables-n+1)

and constrains

$$
\Delta\gamma\geq0 \medspace, \qquad
\bm{\phi}(\bm{\tau}_{n+1} ,\bm{\textbf{A}}_{n+1}) \leq 0 \medspace,  \qquad
\Delta\gamma \bm{\phi}(\bm{\tau}_{n+1}, \bm{\textbf{A}}_{n+1})=0
$$ (loading/unloading-conditions)

This system of equations for $\Delta\gamma$ has the same functional format as its small-strains counterpart.

## Rate-independent Isotropic von Mises Model

Motivated by experimental observations of pressure insensitivity of yielding in metals, plastic deformation in von Mises plasticity occurs when the deviatoric strain energy exceeds a critical value.
This energy can be expressed in terms of the second invariant

$$
J_2=\frac{1}{2}\bm{s}:\bm{s}
$$ (J2)

of the (symmetric) deviatoric stress tensor $\bm{s}=I_d(\bm{\sigma})$.
The critical value is given by the strain hardening law $\sigma_y=\sigma_y(\bar{\varepsilon}^p_n)$ that is function of a single (scalar) internal variable termed the accumulated (effective or equivalent) plastic strain $\bar{\varepsilon}^p$ and defined as:

$$
\bar{\varepsilon}^p=\int_0^t |\dot{\gamma}|dt
$$ (accumulated-plastic-strain)

As uniaxial tensile testing is the preferred method to calibrate plasticity models, the von Mises yield function is generally stated in terms of the yield stress $\sigma_y$ as:

$$
\bm{\phi}=q-\sigma_y
$$ (vonMises-yield)

with (scalar) $q=\sqrt{3J_2({\bm{s}})}= \sqrt{\frac{3}{2}}||\bm{s}||$ termed the von Mises (effective or equivalent) stress.

The principle of maximum plastic dissipation leads to associative plasticity {cite}`halphen1975`, in which the dissipation potential coincides with the yield function and the flow vector ($\bm{N}^p$) is thus normal to the yield surface. For von Mises plasticity, we can then write

$$
\bm{N}^p \coloneqq \frac{\partial\Xi}{\partial\bm{\tau}} = \frac{\partial\phi}{\partial\bm{\tau}}=\sqrt{\frac{3}{{2}}}\frac{\bm{s}}{||\bm{s}||} = \frac{3}{{2}}\frac{\bm{s}}{q}
$$ (flow-vector)

It also follows from {eq}`plastic-deformation-gradient-at-n+1` that

$$
\bm{F}^p_{n+1}=\bm{R}_{n+1}^{e \space T}\exp(\Delta\gamma\bm{N}^p)\bm{R}_{n+1}^e\bm{F}_n^p
$$ (plastic-deformation-gradient-at-n+1-associative)

:::{note}
The implemented strain hardening law combines a linear hardening with Voce-type nonlinear hardening as

$$
\sigma_y= \sigma_0 + H\bar{\varepsilon}^p +  (\sigma_\infty - \sigma_0) (1-exp(-\beta\bar{\varepsilon}^p))
$$ (strain-hardening-law)

where $\sigma_0$ is the yield stress and {$H$, $\sigma_\infty$, $\beta$} are the three strain hardening parameters respectively representing the linear hardening factor, the saturation (flow) stress, and the hardening decay parameter.

:::

### Stress Update
The stress updating procedure reuses the returning mapping algorithm for small-strains with additional preprocessor and postprocessor steps to map strains and stresses from Euclidean to logarithmic strain spaces and vice versa {cite}`miehe1994`. An overview of the integration algorithm with kinematic state variables $\bar\varepsilon^{p}_n$ and $\bm{C}^{p-1}_n$ is given below.

:::{note}
Choosing $\bm{C}^{p-1}_n$ as additional state variable alongside $\bar\varepsilon^{p}_n$ minimizes the stored arrays, as $\bm{C}^{p-1}_n$ has length 6 in Voigt notation. However, other kinematic state variables could be considered. For instance, one could use $\bm{F}^{p-1}_n$ and update via {eq}`plastic-deformation-gradient-at-n+1-associative`. This approach would nevertheless increase storage and introduce a significant amount of additional computation, e.g. calculating $\textbf{v}^{e}_{n+1}$ needed in $\bm{R}^{e \space tr}_{n+1} = \bm{R}^e_{n+1} = (\textbf{v}^{e}_{n+1})^{-1} \bm{F}^{e}_{n+1}$.
:::

#### Elastic Predictor
The trial effective stress $q^{tr}$ can be calculated at $t+\Delta t$ as:

$$
\bm{b}^{e \space tr}_{n+1}=\bm{F}\bm{C}^{p-1}_n \bm{F}^{T}
\space \rightarrow \space
\textbf{e}^{e \space tr}_{\space n+1}=0.5\log(\bm{b}^{e \space tr}_{n+1})
\space \rightarrow
$$ (elastic-predictor1)

$$
\space \rightarrow \space
\bm{s}^{tr}_{n+1} = 2\mu \textbf{e}^{e \space tr}_{d \space n+1}\space \rightarrow \space
q^{tr}_{n+1}= \sqrt{3J_2({\bm{s}^{tr}_{n+1}})}
$$ (elastic-predictor2)

Admissibility can thus be verified after the initial guess $\bar\varepsilon^{p}_{n+1} =\bar{\varepsilon}^p_n$ :

$$
\begin{aligned}
& \text{if:} \qquad q^{tr}_{n+1}-\sigma_y(\bar\varepsilon^{p }_{n})\leq0 \qquad \text{elastic step}
\\
&\text {else: \quad elastoplastic step} \rightarrow \text{return mapping}
\end{aligned}
$$ (admissibility-check)

:::{note}
The isotropic tensor function for $\textbf{e}^{e \space tr}_{\space n+1}$ in {eq}`elastic-predictor1`can be conveniently computed via the spectral decomposition of $\textbf{b}^{e \space tr}$ as

$$
\textbf{e}^{e \space tr}_{\space n+1}=\frac{1}{2}\sum_{i=1}^3\log(\lambda^b_i)\hat{\bm{N}}_i \otimes \hat{\bm{N}}_i
$$ (decomposition-log-strain)

with eigenvalues $\lambda^b_i=\lambda^2_i$ and eigenvectors $\hat{\bm{N}}_i$.
:::

#### Return Mapping
In the von Mises model, the system of equations composed of {eq}`elastic-deformation-gradient-n+1`, {eq}`internal-variables-n+1` and {eq}`loading/unloading-conditions` can be reduced to a single non-linear equation

$$
\tilde{\phi}=q^{tr}_{n+1}-3\mu\Delta\gamma-\sigma_y=0
$$ (yield-function-trial-state)

which can be solved efficiently via Newton-Raphson iterations to tolerance $\tilde{\bm{\phi}}\leq tol$,

$$
H:=\frac{\diff\sigma_y}{\diff\bar{\varepsilon}^p}\vert_{\bar{\varepsilon}^p_n+\Delta\gamma}
\space \rightarrow
\frac{\diff\tilde{\phi}}{\diff\Delta\gamma}:=-3\mu-H
\space \rightarrow
\Delta\gamma:=\Delta\gamma+\frac{\tilde{\phi}}{3\mu +H}
$$ (delta-gamma-update)

#### Update
With $\Delta\gamma$, deviatoric stresses can be updated as

$$
\bm{s}_{n+1}=2\mu\textbf{e}^{e}_{d \space n+1} =2\mu(1-3\mu\frac{\Delta\gamma}{q^{tr}_{n+1}})\textbf{e}^{e \space tr}_{d \space n+1}
$$ (deviatoric-update)

and used to compute $\bm{\tau}_{n+1}$. The update of state variables is also straightforward. The current accumulated plastic strain is computed as

$$
\bar\varepsilon^{p}_{n+1} =\bar{\varepsilon}^p_n+\Delta\gamma
$$ (accumulated-plastic-update)

To update $\bm{C}^{p-1}_n$, on the other hand, we first compute $\bm{b}^e_{n+1} = \exp(2\textbf{e}^{e}_{n+1})$ and then

$$
\bm{C}^{p-1}_{n+1} = \bm{F}^{-1} \bm{b}^e_{n+1} \bm{F}^{-T}
$$ (right-cauchy-plastic-update)

:::{note}
Ratel also implements a principal stress space-based formulation whereby the stress update is directly carried out on the eigenvalues of $\textbf{s}$

$$
s_{i \space n+1}=(1-3\mu\frac{\Delta\gamma}{q^{tr}_{n+1}})s^{tr}_{i \space n+1}
$$ (deviatoric-stress-eigenvalues-update)

and $\textbf{s}$ retrieved as

$$
\textbf{s}_{n+1}=\sum_{i=1}^3 s_{i \space n+1} \hat{\bm{N}}_i \otimes \hat{\bm{N}}_i
$$ (deviatoric-stress-update)
:::

### Consistent Tangent
The (consistent) linearization of $\bm \tau$ with respect to $\bm F$ in {eq}`jacobian-weak-form-current` is derived from its algorithmic expression $\tilde\tau=\tilde\tau(\textbf{e}^{e \space tr}_{n+1}(\bm{B}^{e \space tr}_{n+1}(F_n^e,F_{n+1})), \bm{\alpha}_n)$ and is conveniently expressed as the double contraction of three fourth-order tensors {cite}`desouzaneto1998`

$$
\frac{\partial\tilde{\tau}}{\partial\bm{F}_{n+1}}=\textbf{D}^{ep}:\textbf{L}:\textbf{B}
$$ (consistent-tangent-plasticity)

with

$$
\textbf{D}^{ep}=\frac{\partial\tilde{\tau}}{\partial\textbf{e}^{e \space tr}_{n+1}}
\quad, \quad
\textbf{L}=\frac{1}{2}\frac{\partial\log(\bm{b}^{e \space tr}_{n+1})}{\partial\bm{b}^{e \space tr}_{n+1}}
\quad, \space \text{and} \quad
\textbf{B}=\frac{\partial\bm{b}^{e \space tr}_{n+1}}{\partial\bm{F}_{n+1}}
$$ (consistent-tangent-vonMises)

$\textbf{D}^{ep}$ is symmetric for associative plasticity and the only quantity in {eq}`consistent-tangent-vonMises` that depends on the specific plasticity model. Also note that $\textbf{L}$ is the derivative of tensor-valued function of symmetric tensor, for details see {cite}`miehe1998` {cite}`hudobivnik2016`,

#### Expression for $\textbf{B}$
The stored $\mathbf{C}_n^{p-1}$ can be retrieved to compute the change of $\bm{b}^{e \space tr}_{n+1}$ with $\bm{F}$ as,

$$
\diff\bm{b_e} = \diff(\bm{F} \bm{C}_n^{p-1} \bm{F}^T) = dF{\bm{C}_n^{p-1}}\bm{F}^T + \bm{F}\bm{C}_n^{p-1} \diff\bm{F}^T
$$ (consistent-tangent-B)

#### Expression for $\textbf{L}$
To calculate the change $\diff\textbf{e}^{e \space tr}_{n+1}$ due to a perturbation $\diff\bm{b}^{e \space tr}_{n+1}$, it is convenient to express the former in term of eigenvalues $\lambda^2_i$ and eigenbasis $\hat{\bm{N}}_i$ of $\bm{b}^{e \space tr}_{n+1}$ as

$$
\diff\textbf{e}^{e \space tr}_{n+1}= \diff(\frac{1}{2}\sum_{i=1}^3\log(\bm{b}^{e \space tr}_{n+1}))=\diff(\frac{1}{2}\sum_{i=1}^3\log(\lambda^b_i)\hat{\bm{N}}_i \otimes \hat{\bm{N}}_i) = \frac{1}{2}\sum_{i=1}^3(\frac{1}{\lambda^b_i}\diff\lambda^b_i\hat{\bm{N}}_i \otimes \hat{\bm{N}}_i +\log(\lambda^b_i){\diff(\hat{\bm{N}}_i \otimes \hat{\bm{N}}_i)}) = \\
\frac{1}{2}\sum_{i=1}^3( \frac{\hat{\bm{N}}_i ^T \diff\bm{b}^{e \space tr}_{n+1} \space \hat{\bm{N}}_i }{\lambda^b_i}\hat{\bm{N}}_i \otimes \hat{\bm{N}}_i + \log(\lambda^b_i)(\diff\hat{\bm{N}}_i  \otimes \hat{\bm{N}}_i  + \hat{\bm{N}}_i  \otimes \diff\hat{\bm{N}}_i ))
$$ (consistent-tangent-L)

:::{note}
Helper Qfunctions are implemented in Ratel to perform the spectral decomposition of a $3\times3$ symmetric matrix and compute the increments $\diff\lambda^b_i$ and $\diff\hat{\bm{N}}_i$ {eq}``, as described for mixed elasticity (see derivation of terms in {eq}`dC-ogden`).
:::

#### Expression for $\textbf D^{ep}$ : von Mises case

The elastoplastic consistent tangent tensor $\textbf{D}^{ep}$ in {eq}`consistent-tangent-plasticity` finally links $\diff\tilde{\bm \tau}$ to the change in  logarithmic elastic strains $\diff\textbf{e}^{e}_{\space n+1} = \diff\textbf{e}^{e}_{v \space n+1} + \diff\textbf{e}^{e}_{d \space n+1}$. For the von Mises model, only the deviatoric part is updated. Differentiating the expression for $\diff\textbf{e}^{e}_{d \space n+1}$ in {eq}`deviatoric-update`and making use of the relationships in {eq}`delta-gamma-update`, we get:

$$
\diff\bm{\tau} = \diff(2\mu (1-3\mu\frac{\Delta\gamma}{q^{tr}_{n+1}})\textbf{e}^{e}_{d \space n+1} + K\trace(\textbf{e}))= 2\mu(1-3\mu\frac{\Delta\gamma}{q^{tr}_{n+1}})\diff\textbf{e}^{e \space tr}_{d \space n+1} - 6 \mu^2 \frac{\space \diff\tilde{\phi}}{q^{tr}_{n+1}}( \frac{1}{3\mu+H}- \frac{\Delta\gamma}{q^{tr}_{n+1}})\textbf{e}^{e \space tr}_{d \space n+1} + K\trace(\diff\textbf{e})
$$ (dtau-vonMises)

where {eq}`yield-function-trial-state`gives $\diff\tilde{\phi}=\diff q^{tr}_{n+1}$. Recalling $q^{tr}_{n+1}=\sqrt{\frac{3}{2}s^{tr}_{n+1}: s^{tr}_{n+1}}$ and $s^{tr}_{n+1}=2\mu\textbf{e}^{e \space tr}_{d \space n+1}$, differentiating $q^{tr}_{n+1}$ with respect to $\textbf{e}^{e \space tr}_{d \space n+1}$ gives,

$$
\diff q^{tr}_{n+1} = \sqrt\frac{3}{2} \frac{\diff(s^{tr}_{n+1}: s^{tr}_{n+1})}{2\sqrt{s^{tr}_{n+1}: s^{tr}_{n+1}}} = \sqrt\frac{3}{2} \frac{2s^{tr}_{n+1} : \diff s^{tr}_{n+1}} {2\sqrt\frac{2}{3} {q^{tr}_{n+1}}}=\frac{3}{2}  \frac{s^{tr}_{n+1} : \diff s^{tr}_{n+1}} {{q^{tr}_{n+1}}}
$$ (dq-trial-vonMises)

:::{note}
In the principal axes implementation, the linearization of $\bm{\tau}$ in {eq}`dtau-vonMises` is expressed in terms of its eigenvalues as

$$
\diff\bm{\tau}= \sum_{i=1}^3(\diff\tau_i \hat{\bm{N}}_i \otimes \hat{\bm{N}}_i +\tau_i{\diff(\hat{\bm{N}}_i \otimes \hat{\bm{N}}_i)})
$$ (dtau-vonMises-principal)

where

$$
\diff\tau_i = (1-3\mu\frac{\Delta\gamma}{q^{tr}_{n+1}})\diff\tau^{tr}_{i} - 3\mu \frac{\space \diff\tilde{\phi}}{q^{tr}_{n+1}}( \frac{1}{3\mu+H}- \frac{\Delta\gamma}{q^{tr}_{n+1}})\tau_i^{tr} + K\trace(d\textbf{e})
$$ (dtau-vonMises-principal-update)
:::

## Hencky hyperelasticity only

The Hencky hyperelastic material can be retrieved by setting a sufficiently high yield stress to ensure admissible stress states. This way, the returning mapping would be inactive and the state variables remain unchanged from their initialization values $\bar{\varepsilon}^p = 0$ and $\mathbf{C}^{p-1} = \mathbf{I}$. The associated code can thus be removed. The consistent tangent {eq}`consistent-tangent-plasticity` would also greatly simplify

$$
\frac{\partial\tau}{\partial\bm{F}}=\textbf{D}^{e}:\textbf{L}:\textbf{B}
$$ (consistent-tangent-elasticity)

as $\textbf{D}^{ep} = \textbf{D}^{e}$ is now the (isotropic) linear elasticity tangent.
