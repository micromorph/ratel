# Governing equations

Ratel implements three formulations for simulating solid mechanics materials.
The static hyperelastic formulation solves the steady-state static momentum balance equations using unstructured high-order finite/spectral element spatial discretizations.
The quasistatic hyperelastic formulation solves the same set of steady-state equations but applies a constant to boundary conditions.
The elastodynamics formulation applies time varying boundary conditions to the hyperelastic formulation.
The material point method projects the Lagrangian materials onto a background Eulerian finite element mesh to compute the material derivatives.
Contact boundary conditions with a rigid platen are implemented using a Nitsche method approach.

```{toctree}
:caption: Contents
:maxdepth: 4

Elasticity <elasticity>
Mixed Elasticity <mixed-elasticity>
Plasticity <elastoplasticity.md>
Platen Contact Boundary Conditions <contact-platen>
Friction Models <friction>
Phase-Field Modeling of Brittle Fracture <phase-field-fracture>
Material Point Method <material-point-method>
Poroelasticity <poroelasticity>
```
