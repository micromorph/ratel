(continuum-mechanics)=

# Continuum Mechanics

In continuum mechanics, we model materials as continuous pieces of matter and define quantities of interest, such as density, velocity, and force, on these pieces of matter.
In this section, we cover notation and principals used in the mathematical modeling of materials and methods in Ratel.

```{toctree}
:caption: Contents
:maxdepth: 4

General motions <general-motions>
Strain and stress <strain-stress>
Balance laws <balance-laws>
```
