# General motions

Consider a continuum body $\Omega$ which is embedded in the 3D Euclidean space at a given time $t$ (see {ref}`general-motion-fig`).
As the continuum body $\Omega$ moves in space from one instant of time to another, it occupies a sequence of geometrical regions denoted by $\Omega_0, \dotsc, \Omega$.
We can measure the position vector of point $P \in \Omega_0$ at time $t=0$ with coordinate $\bm{X}$, which is referred to initial (or undeformed) configuration.
On the other hand, when $\Omega_0$ moves to $\Omega$ at time $t$, we measure the position vector of point $p \in \Omega$ by $\bm{x}$ which is known as current (or deformed) configuration.
The Lagrangian view of the material is based on the initial configuration while the Eulerian view of the material is based in the current configuration.

```{figure} ../../img/general-motion3D.svg
---
align: center
name: general-motion-fig
---
Initial (undeformed) and current (deformed) configurations of a continuum body.
```


## Lagrangian and Eulerian descriptions

Refer to {ref}`general-motion-fig`, consider $\bm{\chi}$ as a vector field motion that moves body $\Omega_0$ to $\Omega$.
The so-called material description is a characterization of the motion (or any other quantity) with respect to the material coordinates $(X_1, X_2, X_3)$ and time $t$ as

$$
  \bm x = \bm{\chi} (\bm X, t).
$$

Traditionally the material description is often referred to as the **Lagrangian description**. Note that at $t=0$ we have the consistency condition $\bm X = \bm x$.

The so-called **Eulerian (or spatial) description**, is a characterization of the motion (or any other quantity) with respect to the spatial coordinates $(x_1, x_2, x_3)$ and time $t$ as

$$
  \bm X = \bm{\chi}^{-1} (\bm x, t).
$$

In fluid mechanics we work in the Eulerian description in which we refer all relevant quantities to the position in space at time $t$. However, in solid mechanics we use both types of description.

## Displacement, velocity, acceleration fields

The vector field

$$
\bm U(\bm X, t) = \bm x(\bm X, t) - \bm X
$$ (displacement-initial)

represent the **displacement field** of a typical particle and relates its position $\bm X$ in the undeformed configuration to its position $\bm x$ in the deformed configuration at time $t$. The displacement field $\bm U$ is a function of the referential position $\bm X$ and time $t$, which characterizes the material description (Lagrangian form) of the displacement field.

The displacement field in the spatial description (Eulerian form), is a function of the current position $\bm x$ and time $t$ as

$$
\bm u(\bm X, t) = \bm x - \bm X (\bm x, t).
$$ (displacement-current)

The two descriptions are related by means of the motion $\bm x = \bm{\chi}(\bm X, t)$, namely

$$
\bm U(\bm X, t) = \bm U[\bm{\chi}^{-1}(\bm x, t), \, t] = \bm u(\bm x, t).
$$

Note that $\bm U$ and $\bm u$ have the same values. They represent functions of different arguments.

In the Lagrangian view, the time derivative is simply the partial derivative with respect to time because the material coordinate $\bm X$ do not change with time, and velocity and acceleration functions are given by

$$
  \begin{aligned}
    \bm{V} \left( \bm{X}, t \right) &= \frac{\partial \bm{\chi}}{\partial t} \left( \bm{X}, t \right), \\
    \bm{A} \left( \bm{X}, t \right) &= \frac{\partial \bm{V}}{\partial t} \left( \bm{X}, t \right) = \frac{\partial^2 \bm{\chi}}{\partial t^2} \left( \bm{X}, t \right).
  \end{aligned}
$$ (vel-acc-lagrangian)

In Eulerian view, the two descriptions are transformed into each other by using the motion $\bm \chi$ as

$$
  \begin{aligned}
    \bm{V} \left( \bm{X}, t \right) &= \bm{V} [\bm{\chi}^{-1}(\bm x, t), \, t] = \bm v(\bm x, t), \\
    \bm{A} \left( \bm{X}, t \right) &= \bm{A} [\bm{\chi}^{-1}(\bm x, t), \, t] = \bm a(\bm x, t),
  \end{aligned}
$$ (vel-acc-eulerian)

where $\bm v(\bm x, t)$ and $\bm a(\bm x, t)$ denote the spatial description of the velocity and the acceleration field, respectively.

## Material, special derivatives

We introduce the terminology of a **material field** in which the independent variables are $(\bm X, t)$, i.e. the referential position $\bm X$ with material coordinates $X_I, \, I=1,2,3,$ and the time $t$. In a **spatial field** the independent variables are $(\bm x, t)$, i.e. the current position $\bm x$ with spatial coordinates $x_i, \, i=1,2,3,$ and the time $t$. In the following we denote a smooth material and spatial fields of some physical scalar, vector or tensor quantity associated with the motion $\bm{\chi}$ as $\mathcal{F} = \mathcal{F}(\bm X, t)$ and $\mathcal{f} = \mathcal{f}(\bm x, t)$, respectively.

The **material time derivative** of a smooth material field $\mathcal{F}(\bm X, t)$ which is conventionally denoted by $D \mathcal{F}(\bm X, t)/D t$, can be obtained by ($\bm X$ do not change with time) simply partial derivative with respect to time i.e.

$$
  \begin{aligned}
    \bm{V} \left( \bm{X}, t \right) &= \frac{\partial \bm{U}(\bm X, t)}{\partial t} = \dot{\bm U}, \\
    \bm{A} \left( \bm{X}, t \right) &= \frac{\partial \bm{V}(\bm X, t)}{\partial t} = \dot{\bm V} = \ddot{\bm U}.
  \end{aligned}
$$

However, for spatial description $\mathcal{f} (\bm x, t)$, the material derivative is

$$
  \frac{D}{D t} \mathcal{f} \left( \bm{x}, t \right) =  \frac{\partial }{\partial t} \mathcal{f} \left( \bm{x}, t \right) + \frac{\partial }{\partial \bm{x}} [\mathcal{f}\left( \bm{x}, t \right)] \frac{\partial \bm x}{\partial t} = \frac{\partial \mathcal{f}}{\partial t} + \bm v \cdot \nabla_x \mathcal{f},
$$ (material-derivative-eulerian)

where $\bm v = \partial \bm x / \partial t = \dot{\bm x}$ is the spatial velocity and $_x$ in $\nabla_x$ denotes the gradient with respect to current configuration. Hence,we may write the acceleration of a particle in Eulerian view as

$$
  \bm{a} \left( \bm{x}, t \right) = \frac{D}{D t} \bm{v} \left( \bm{x}, t \right) =  \frac{\partial \bm{v}}{\partial t} + \bm{v} \cdot \frac{\partial \bm{v}}{\partial \bm{x}}
$$ (acc-eulerian)

Note that this leads to the somewhat surprising result in the Eulearian view that

$$
  \bm{a} \left( \bm{x}, t \right) \neq \frac{\partial \bm{v}}{\partial t} \left( \bm{x}, t \right).
$$

Also note that we can 'pull back' to the Lagrangian view via $\bm{x} = \bm{\chi} \left(\bm{X}, t  \right)$ as

$$
  \begin{aligned}
    \bm{V} \left( \bm{X}, t \right) &= \bm{v} \left( \bm{\chi} \left( \bm{X}, t \right), t \right), \\
    \bm{A} \left( \bm{X}, t \right) &= \bm{a} \left( \bm{\chi} \left( \bm{X}, t \right), t \right).
  \end{aligned}
$$

## Deformation gradient

As is shown in {ref}`general-motion-fig`, a typical point $P \in \Omega_0$ identified by the position vector $\bm X$ maps into the point $p \in \Omega$ with position vector $\bm x$. In this section we show how curves and tangent vectors deform. One of the key quantities in deformation analysis is the **deformation gradient** tensor, denoted $\bm F$, which gives the relationship of a material line $d \bm X$ before deformation to the line $d \bm x$ after deformation as

$$
  d \bm x = \bm F d \bm X
$$ (line-transformation)

with deformation gradient $\bm F$ defined by

$$
  \bm{F} = \frac{\partial \bm x}{\partial \bm X} = \bm{I} + \nabla_X \bm u.
$$ (deformation-gradient)

The infinitesimal volume in initial and current configurations denoted by $dV$ and $dv$ are related through the determinant of deformation gradient as

$$
  dv = J dV, \quad J = |{\bm F}| > 0.
$$ (volume-transformation-and-J)

In order to compute the relationship between the unit vector $\bm N$ and $\bm n$ in initial and current configuration, we express the infinitesimal volume element as a dot product. By means of {eq}`volume-transformation-and-J` we have

$$
  dv = d\bm a \cdot d\bm x = J d\bm A \cdot d\bm X,
$$

with $d\bm a = da \bm n$ and $d\bm A = dA \bm N$ denoting vector elements of infinitesimally small areas in current and initial configurations, respectively. With transformation {eq}`line-transformation` and identity $\bm{x}\cdot \bm{A}^T \bm{y} = \bm{y} \cdot \bm{A} \bm{x}$ we may rewrite above equation as

$$
  \left( \bm F^T d \bm a - J d \bm A \right) \cdot d \bm X = 0,
$$

and since it holds for arbitrary material line elements $d\bm X$, we find that {cite}`holzapfel2000nonlinear`

$$
  d\bm a = J \bm F^{-T} d \bm A,
$$ (area-transformation)

which is well-known as *Nanson's formula*.

## Mixture theory at finite strain

In poromechanincs, we consider the deformation of two constituents (phases) subjected external loading. In the theory of porous media, it is assumed that the control space is that of the solid phase $\Omega = \Omega^s$ , also known as the “solid skeleton.” {ref}`general-motion-fig` (A) shows the solid skeleton composed alveolar tissue and (B) Kinematics of a biphasic (solid–fluid) mixture theory. Note that the fluid domains in initial and current configurations i.e., $\Omega_0^f, \Omega^f$ are the potential in/out fluid flux into solid skeleton. Furthermore, the spatial vector $\bm x$ is simultaneously occupied by all constituent material points $\bm{X}_s, \bm{X}_f$ of the mixture

$$
  \bm x = \bm{\chi}_f (\bm{X}_f, t) = \bm{\chi}_s (\bm{X}_s, t) = \bm{\chi} (\bm{X}, t),
$$

where in the last equality we drop $s$ since the solid skeleton is the control space.

```{figure} ../../img/general-motion-poro.png
---
align: center
name: general-motion-poro-fig
---
General motion of biphasic (solid(s)-fluid(f)) porous material (adopted from {cite}`irwin2024porofinitestrain`).
```

The deformation gradient for phase $\alpha$ with respect to current configuration of solid skeleton $\Omega= \Omega^s$ is

$$
  \bm{F}_{\alpha} = \frac{\partial \bm{\chi}_{\alpha}}{\partial \bm{X}_{\alpha}} = \frac{\partial \bm x}{\partial \bm{X}_{\alpha}}.
$$ (deformation-gradient-alpha)

Note that the determinant of deformation gradient for each phase based on $\bm{F}_{\alpha}$ and

$$
  dv = J_s dV_s = J_f dV_f, \quad J_s = |\bm{F}_s| > 0, J_f = |\bm{F}_f| > 0.
$$ (volume-transformation-alpha)

Also, the material time derivative of a special field $f(\bm x, t)$ for constituent $\alpha$ is

$$
  \frac{D \mathcal{f} \left( \bm{\chi}_{\alpha} \left( \bm{X}_{\alpha}, t \right), t \right)}{D t} =  \frac{\partial \mathcal{f} \left( \bm{x}, t \right)}{\partial t} + \frac{\partial \mathcal{f}\left( \bm{x}, t \right)}{\partial \bm{x}} \frac{\partial \bm{\chi}_{\alpha} \left( \bm{\chi}_{\alpha}^{-1}(\bm x, t), t \right)}{\partial t} = \frac{\partial \mathcal{f}}{\partial t} + \bm{v}_{\alpha} \cdot \nabla_x \mathcal{f},
$$ (material-derivative-alpha)
