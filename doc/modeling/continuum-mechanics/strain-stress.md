# Strain and stress

## Strain

The general Seth-Hill formula of strain measures in the Lagrangian and Eulerian coordinates are defined as {cite}`odgen1997`

$$
  \begin{aligned}
    &\frac{1}{n} \left( \textbf{U}^n - \bm{I} \right), \quad  &\frac{1}{n} \left( \textbf{v}^n - \bm{I} \right), \quad \text{if} \quad n \neq 0,\\
    &\log \textbf{U}, \quad &\log \textbf{v}, \quad \text{if} \quad n = 0,
  \end{aligned}
$$ (Seth-Hill-strain-measures)

where $n$ is a real number and $\textbf{U}, \textbf{v}$ are unique SPD right (or material) and left (or spatial) stretch tensors defined by the *unique polar decomposition* of the deformation gradient {eq}`deformation-gradient` $\bm{F} = \textbf{R} \textbf{U} = \textbf{v} \textbf{R}$ with $\textbf{R}^T \textbf{R} = \bm{I}$ as the rotation tensor.
The eigenvalues of the symmetric material tensor $\textbf{U}$ are $\lambda_i, \, i=1,2,3$, called the principal stretches and their corresponding orthonormal eigenvectors $\hat{\bm{N}}_i$ called principal referential directions, i.e.

$$
  \textbf{U} \hat{\bm{N}}_i= \lambda_i \hat{\bm{N}}_i. \quad i=1,2,3
$$ (right-stretch-eigensystem)

From the polar decomposition we deduce the eigenvalues problems for symmetric spatial tensor $\textbf{v}$ as

$$
  \textbf{v} \hat{\bm{n}}_i= \lambda_i \hat{\bm{n}}_i. \quad i=1,2,3
$$ (left-stretch-eigensystem)

where $\hat{\bm{n}}_i = \bm{R} \hat{\bm{N}}_i$. For the special case $n=2$, we define right and left Cauchy-Green tensors

$$
  \bm{C} = \bm{F}^T \bm{F} = \textbf{U}^2, \quad \bm{b} = \bm{F} \bm{F}^T = \textbf{v}^2
$$

and Green-Lagrange and Green-Euler strains with $\bm H = \nabla_X \bm u$

$$
  \bm{E} = \frac{1}{2} \left( \bm{C} - \bm{I} \right) = \frac{1}{2} \left( \bm{H} + \bm{H}^T + \bm{H}^T \bm{H} \right).
$$ (eq-green-lagrange-strain)

$$
  \bm{e} = \frac{1}{2} \left( \bm{b} - \bm{I} \right) = \frac{1}{2} \left( \bm{H} + \bm{H}^T + \bm{H} \bm{H}^T \right).
$$ (eq-green-euler-strain)


Clearly $\textbf{U}$, $\bm{C}$ and $\bm{E}$ have the **same** orthonormal eigenvectors $\hat{\bm{N}}_i$ and their eigenvalues are related through $\lambda_i^C = \lambda_i^2 = 1 + 2\lambda_i^E $ i.e.,

$$
  \begin{aligned}
    \bm{C} \hat{\bm{N}}_i&= \lambda_i^C \hat{\bm{N}}_i = \lambda_i^2 \hat{\bm{N}}_i. \quad i=1,2,3 \\
    \bm{E} \hat{\bm{N}}_i&= \lambda_i^E \hat{\bm{N}}_i = \frac{1}{2} \left( \lambda_i^2 - 1 \right)\hat{\bm{N}}_i. \quad i=1,2,3,
  \end{aligned}
$$ (green-lagrange-tensor-eigensystem)


 Similarly the eigenvectors of $\textbf{v}$, $\bm{b}$ and $\bm{e}$ are $\hat{\bm{n}}_i$ and their eigenvalues satisfy $\lambda_i^b = \lambda_i^2 = 1 + 2\lambda_i^e $ i.e.,

$$
  \begin{aligned}
    \bm{b} \hat{\bm{n}}_i&= \lambda_i^b \hat{\bm{n}}_i = \lambda_i^2 \hat{\bm{n}}_i. \quad i=1,2,3 \\
    \bm{e} \hat{\bm{n}}_i&= \lambda_i^e \hat{\bm{n}}_i = \frac{1}{2} \left( \lambda_i^2 - 1 \right)\hat{\bm{n}}_i. \quad i=1,2,3,
  \end{aligned}
$$ (green-euler-tensor-eigensystem)

## Stress

Consider a continuum $\Omega$ subjected to a deformation mapping $\bm{\chi}$ that results in the deformed configuration as shown in {ref}`general-motion-fig`. Let the force vector on an elemental area $da$ with normal $\bm n$ in the deformed configuration be $d \bm f$. Suppose that the area element in the undeformed configuration that corresponds to $da$ is $dA$ with normal $\bm N$. The force $d \bm f$ can be expressed in terms of a stress vector $\bm t$ and $\bm T$ times the area in current and initial configurations, respectively as

$$
  d\bm f = \bm t d a = \bm T d A
$$ (traction-force-current)

From Cauchy's formula {cite}`holzapfel2000nonlinear`, we have $\bm t = \bm \sigma \cdot \bm n$, where $\bm \sigma$ is the Cauchy stress tensor. In a similar fashion, we introduce a stress tensor $\bm P$, called the *first Piola-Kirchhoff stress* tensor, such that $\bm T = \bm P \cdot \bm N$. Then using {eq}`traction-force-current` we can write

$$
  \bm \sigma \cdot \bm n da = \bm P \cdot \bm N d A \quad \text{or} \quad \bm \sigma \cdot d \bm a = \bm P \cdot d \bm A,
$$

from {eq}`area-transformation` we arrive at

$$
  \bm P = J \bm \sigma \bm{F}^{-T}.
$$ (first-Piola-Kirchhoff)

The first Piola-Kirchhoff stress tensor, also referred to as the nominal stress tensor, or Lagrangian stress tensor, gives the current force per unit undeformed area.

In large deformation analysis, we introduce the *second Piola-Kirchhoff stress* tensor $\bm S$, associated with the force $d \bm F$ in the undeformed elemental area $d \bm A$ that corresponds to the force $d \bm f$ on the deformed elemental area $d \bm a$

$$
  d \bm F = \bm S \cdot d \bm A.
$$ (traction-force-initial)

Thus, the second Piola-Kirchhoff stress tensor gives the *transformed current force* per unit undeformed area. Similar to relationship between $d\bm x$ and $d\bm X$ {eq}`line-transformation`, the force $d \bm f$ is related to the force $d \bm F$ as

$$
  d \bm F = \bm{F}^{-1} d \bm{f} = \bm{F}^{-1} (\bm P \cdot d \bm A),
$$

Hence, the second Piola-Kirchhoff stress tensor is related to the first Piola-Kirchhoff stress tensor and Cauchy stress tensor as

$$
  \bm S = \bm{F}^{-1} \bm{P} = J \bm{F}^{-1} \bm{\sigma} \bm{F}^{-T}.
$$ (second-Piola-Kirchhoff)

Clearly, $\bm S$ is symmetric whenever $\bm \sigma$ is symmetric but $\bm P$ is not symmetric. We can defined the *symmetric Kirchhoff stress* tensor $\bm \tau$, in current configuration by pushing forward $\bm S$, as

$$
  \bm{\tau} = \bm F \bm S \bm F^T = J \bm \sigma = J \bm{P} \bm{F}^T.
$$ (S-to-tau)
