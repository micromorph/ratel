#pragma once

#include <ceed.h>
#include <petscsys.h>
#include <ratel-impl.h>
#include <ratel.h>

// Material FEM setup
RATEL_INTERN PetscErrorCode RatelMaterialCreate_FEM(Ratel ratel, RatelMaterial material);

// FEM DM setup
RATEL_INTERN PetscErrorCode RatelSolutionDMSetup_FEM(Ratel ratel, DM *dm_solution);
RATEL_INTERN PetscErrorCode RatelEnergyDMSetup_FEM(Ratel ratel, DM *dm_energy);
RATEL_INTERN PetscErrorCode RatelDiagnosticDMsSetup_FEM(Ratel ratel, DM *dm_diagnostic, PetscInt *num_sub_dms_diagnostic, IS **is_sub_dms_diagnostic,
                                                        DM **sub_dms_diagnostic);

RATEL_INTERN PetscErrorCode RatelSurfaceForceCellToFaceDMSetup_FEM(Ratel ratel, DM *dm_surface_force);
RATEL_INTERN PetscErrorCode RatelDMSetupByOrder_FEM(Ratel ratel, PetscBool setup_boundary, PetscBool setup_faces, PetscInt *orders,
                                                    PetscInt coord_order, PetscBool setup_coords, PetscInt q_extra, CeedInt num_fields,
                                                    const CeedInt *field_sizes, const char **field_names, const char **component_names, DM dm);

// Geometry
RATEL_INTERN PetscErrorCode RatelMaterialSetupVolumeQData_FEM(RatelMaterial material, const char *label_name, PetscInt label_value,
                                                              CeedElemRestriction *restriction, CeedVector *q_data);
RATEL_INTERN PetscErrorCode RatelMaterialSetupSurfaceGradientQData_FEM(RatelMaterial material, DM dm, const char *label_name, PetscInt label_value,
                                                                       CeedElemRestriction *restriction, CeedVector *q_data);

// Operators
// -- Residual and Jacobian
RATEL_INTERN PetscErrorCode RatelMaterialSetupResidualSuboperators_FEM(RatelMaterial material, CeedOperator op_residual_u,
                                                                       CeedOperator op_residual_ut, CeedOperator op_residual_utt);
RATEL_INTERN PetscErrorCode RatelMaterialSetupJacobianSuboperator_FEM(RatelMaterial material, CeedOperator op_residual_u, CeedOperator op_residual_ut,
                                                                      CeedOperator op_jacobian);
RATEL_INTERN PetscErrorCode RatelMaterialSetupJacobianMultigridLevel_FEM(RatelMaterial material, DM dm_level, CeedVector m_loc,
                                                                         CeedOperator sub_op_jacobian_fine, CeedOperator op_jacobian_coarse,
                                                                         CeedOperator op_prolong, CeedOperator op_restrict);
RATEL_INTERN PetscErrorCode RatelMaterialSetupForcingSuboperator_FEM(RatelMaterial material, CeedOperator op_residual);

// -- Block preconditioner for mixed-FEM
RATEL_INTERN PetscErrorCode RatelMaterialSetupJacobianBlockSuboperator_FEM(RatelMaterial material, DM dm, PetscInt field,
                                                                           CeedOperator op_jacobian_block);

// -- Output
RATEL_INTERN PetscErrorCode RatelMaterialSetupStrainEnergySuboperator_FEM(RatelMaterial material, DM dm_energy, CeedOperator op_strain_energy);
RATEL_INTERN PetscErrorCode RatelMaterialSetupForcingEnergySuboperator_FEM(RatelMaterial material, DM dm_energy, CeedOperator op_external_energy);
RATEL_INTERN PetscErrorCode RatelMaterialSetupDiagnosticSuboperators_FEM(RatelMaterial material, DM dm_projected_diagnostic, DM dm_dual_diagnostic,
                                                                         CeedOperator op_mass_diagnostic, CeedOperator op_projected_diagnostic,
                                                                         CeedOperator op_dual_diagnostic, CeedOperator op_dual_nodal_scale);
RATEL_INTERN PetscErrorCode RatelMaterialSetupSurfaceForceCellToFaceSuboperators_FEM(RatelMaterial material, DM dm_surface_force,
                                                                                     CeedOperator ops_surface_force[]);
RATEL_INTERN PetscErrorCode RatelMaterialSetupSurfaceCentroidSuboperators_FEM(RatelMaterial material, DM dm_surface_displacement,
                                                                              CeedOperator ops_surface_centroid[]);
RATEL_INTERN PetscErrorCode RatelMaterialSetupMMSErrorSuboperator_FEM(RatelMaterial material, CeedOperator op_mms_error);
