#pragma once

#include <ceed.h>
#include <ratel.h>

RATEL_INTERN PetscErrorCode RatelCeedOperatorClone(Ratel ratel, CeedOperator op, CeedOperator *op_clone);
RATEL_INTERN PetscErrorCode RatelIncrementalize(Ratel ratel, PetscBool scale_dt, CeedInt num_comp, CeedInt *num_times, CeedScalar *vectors,
                                                CeedScalar *times, RatelBCInterpolationType *interp_type);
