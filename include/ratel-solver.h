#pragma once

#include <petscsys.h>
#include <ratel-impl.h>
#include <ratel.h>

RATEL_INTERN PetscErrorCode RatelDMSetupSolver(Ratel ratel);
