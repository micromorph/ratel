#pragma once

#include <ceed.h>
#include <petscdm.h>
#include <ratel-impl.h>
#include <ratel.h>

RATEL_INTERN PetscErrorCode RatelIncrementMeshRemapState(Ratel ratel);
RATEL_INTERN PetscErrorCode RatelGetMeshRemapState(Ratel ratel, PetscInt *state);
RATEL_INTERN PetscErrorCode RatelDMPlexCreateFromOptions(Ratel ratel, VecType vec_type, DM *dm);
RATEL_INTERN PetscErrorCode RatelDMSwarmCreateFromOptions(Ratel ratel, DM dm_mesh, DM *dm);
RATEL_INTERN PetscErrorCode RatelDMSwarmInitalizePointLocations(Ratel ratel, RatelMPMOptions mpm_options, DM dm_swarm);
RATEL_INTERN PetscErrorCode RatelDMSwarmCreateReferenceCoordinates(Ratel ratel, DM dm_swarm, DMLabel domain_label, PetscInt domain_value,
                                                                   IS *is_points, Vec *X_points_ref);
RATEL_INTERN PetscErrorCode RatelDMSwarmCeedElemRestrictionPointsCreate(Ratel ratel, DM dm_swarm, DMLabel domain_label, PetscInt domain_value,
                                                                        CeedVector *x_ref_points, CeedElemRestriction *restriction_x_points);
RATEL_INTERN PetscErrorCode RatelKershaw(DM dm, PetscScalar eps);
RATEL_INTERN PetscErrorCode RatelRemapScaleCoordinates(Ratel ratel, PetscReal t, DM dm);
RATEL_INTERN PetscErrorCode RatelDMPlexCreateFaceLabel(Ratel ratel, DM dm, RatelMaterial material, PetscInt dm_face, PetscInt *num_label_values,
                                                       char **face_label_name);
RATEL_INTERN PetscErrorCode RatelGetEnergyDM(Ratel ratel, DM *dm);

RATEL_INTERN PetscErrorCode PetscFECreateLagrangeFromOptions(MPI_Comm comm, PetscInt dim, PetscInt num_comp, PetscBool is_simplex, PetscInt order,
                                                             PetscInt q_order, const char prefix[], PetscFE *fem);

RATEL_INTERN PetscErrorCode RatelDMPlexCeedBasisCreate(Ratel ratel, DM dm, DMLabel domain_label, PetscInt label_value, PetscInt height,
                                                       PetscInt dm_field, CeedBasis *basis);
RATEL_INTERN PetscErrorCode RatelDMPlexCeedBasisCoordinateCreate(Ratel ratel, DM dm, DMLabel domain_label, PetscInt label_value, PetscInt height,
                                                                 CeedBasis *basis);
RATEL_INTERN PetscErrorCode RatelDMPlexCeedBasisCellToFaceCreate(Ratel ratel, DM dm, DMLabel domain_label, PetscInt label_value, PetscInt face,
                                                                 PetscInt dm_field, CeedBasis *basis);
RATEL_INTERN PetscErrorCode RatelDMPlexCeedBasisCellToFaceCoordinateCreate(Ratel ratel, DM dm, DMLabel domain_label, PetscInt label_value,
                                                                           PetscInt face, CeedBasis *basis);
RATEL_INTERN PetscErrorCode RatelDMPlexCeedElemRestrictionCreate(Ratel ratel, DM dm, DMLabel domain_label, PetscInt label_value, PetscInt height,
                                                                 PetscInt dm_field, CeedElemRestriction *restriction);
RATEL_INTERN PetscErrorCode RatelDMPlexCeedElemRestrictionCoordinateCreate(Ratel ratel, DM dm, DMLabel domain_label, PetscInt label_value,
                                                                           PetscInt height, CeedElemRestriction *restriction);
RATEL_INTERN PetscErrorCode RatelDMPlexCeedElemRestrictionQDataCreate(Ratel ratel, DM dm, DMLabel domain_label, PetscInt label_value, PetscInt height,
                                                                      PetscInt q_data_size, CeedElemRestriction *restriction);
RATEL_INTERN PetscErrorCode RatelDMPlexCeedElemRestrictionCollocatedCreate(Ratel ratel, DM dm, DMLabel domain_label, PetscInt label_value,
                                                                           PetscInt height, PetscInt dm_field, PetscInt q_data_size,
                                                                           CeedElemRestriction *restriction);
RATEL_INTERN PetscErrorCode RatelDMGetFieldISLocal(Ratel ratel, DM dm, PetscInt field, PetscInt *block_size, IS *is);
RATEL_INTERN PetscErrorCode RatelDMHasFace(Ratel ratel, DM dm, PetscInt face_id, PetscBool *has_face);
