/// @file
/// Ratel utility helpers QFunction source
#pragma once

#include <ceed/types.h>

#include "utils-common.h"

/// @addtogroup RatelInternal
/// @{

/**
  @brief Unpack qdata at quadrature point

  @param[in]   Q       Number of quadrature points
  @param[in]   i       Current quadrature point
  @param[in]   stored  All qdata
  @param[out]  local   Qdata for quadrature point i

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelQdataUnpack(CeedInt Q, CeedInt i, const CeedScalar stored[10][CEED_Q_VLA], CeedScalar local[3][3]) {
  local[0][0] = stored[1][i];
  local[0][1] = stored[2][i];
  local[0][2] = stored[3][i];
  local[1][0] = stored[4][i];
  local[1][1] = stored[5][i];
  local[1][2] = stored[6][i];
  local[2][0] = stored[7][i];
  local[2][1] = stored[8][i];
  local[2][2] = stored[9][i];
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Unpack qdata at quadrature point

  @param[in]   Q       Number of quadrature points
  @param[in]   i       Current quadrature point
  @param[in]   wdetJ   Quadrature weight
  @param[in]   local   Qdata for quadrature point i
  @param[out]  stored  All qdata

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelQdataPack(CeedInt Q, CeedInt i, CeedScalar wdetJ, const CeedScalar local[3][3], CeedScalar stored[10][CEED_Q_VLA]) {
  stored[0][i] = wdetJ;
  stored[1][i] = local[0][0];
  stored[2][i] = local[0][1];
  stored[3][i] = local[0][2];
  stored[4][i] = local[1][0];
  stored[5][i] = local[1][1];
  stored[6][i] = local[1][2];
  stored[7][i] = local[2][0];
  stored[8][i] = local[2][1];
  stored[9][i] = local[2][2];
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Unpack gradient at quadrature point

  @param[in]   Q       Number of quadrature points
  @param[in]   i       Current quadrature point
  @param[in]   grad    Gradient of u, x (or du in jacobian) w.r.t  X, X is ref coordinate [-1, 1]^3
  @param[out]  local   dudX, dxdX (or ddudX in jacobian) for quadrature point i

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelGradUnpack(CeedInt Q, CeedInt i, const CeedScalar grad[3][3][CEED_Q_VLA], CeedScalar local[3][3]) {
  local[0][0] = grad[0][0][i];
  local[0][1] = grad[1][0][i];
  local[0][2] = grad[2][0][i];
  local[1][0] = grad[0][1][i];
  local[1][1] = grad[1][1][i];
  local[1][2] = grad[2][1][i];
  local[2][0] = grad[0][2][i];
  local[2][1] = grad[1][2][i];
  local[2][2] = grad[2][2][i];
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute `C = alpha A * B` for 3x3 matrices at a quadrature point

  @param[in]   Q      Number of quadrature points
  @param[in]   i      Current quadrature point
  @param[in]   alpha  Scaling factor
  @param[in]   A      First input matrix
  @param[in]   B      Second input matrix
  @param[out]  C      Output matrix, stored for quadrature point i

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelMatMatMultAtQuadraturePoint(CeedInt Q, CeedInt i, CeedScalar alpha, const CeedScalar A[3][3], const CeedScalar B[3][3],
                                                           CeedScalar C[3][3][CEED_Q_VLA]) {
  for (CeedInt j = 0; j < 3; j++) {
    for (CeedInt k = 0; k < 3; k++) {
      C[j][k][i] = 0;
      for (CeedInt m = 0; m < 3; m++) {
        C[j][k][i] += alpha * A[j][m] * B[m][k];
      }
    }
  }
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute `C = alpha A * B^T` for 3x3 matrices at a quadrature point

  @param[in]   Q      Number of quadrature points
  @param[in]   i      Current quadrature point
  @param[in]   alpha  Scaling factor
  @param[in]   A      First input matrix
  @param[in]   B      Second input matrix
  @param[out]  C      Output matrix, stored for quadrature point i

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelMatMatTransposeMultAtQuadraturePoint(CeedInt Q, CeedInt i, CeedScalar alpha, const CeedScalar A[3][3],
                                                                    const CeedScalar B[3][3], CeedScalar C[3][3][CEED_Q_VLA]) {
  for (CeedInt j = 0; j < 3; j++) {
    for (CeedInt k = 0; k < 3; k++) {
      C[j][k][i] = 0;
      for (CeedInt m = 0; m < 3; m++) {
        C[j][k][i] += alpha * A[j][m] * B[k][m];
      }
    }
  }
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute `b = alpha A x` for a length 3 vector and a 3x3 matrix at a quadrature point

  @param[in]   Q      Number of quadrature points
  @param[in]   i      Current quadrature point
  @param[in]   alpha  Scaling factor
  @param[in]   A      First input matrix
  @param[in]   x      First input vector
  @param[out]  b      Output vector, stored for quadrature point i

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelMatVecMultAtQuadraturePoint(CeedInt Q, CeedInt i, CeedScalar alpha, const CeedScalar A[3][3], const CeedScalar x[3],
                                                           CeedScalar b[3][CEED_Q_VLA]) {
  for (CeedInt j = 0; j < 3; j++) {
    b[j][i] = 0;
    for (CeedInt m = 0; m < 3; m++) {
      b[j][i] += alpha * A[j][m] * x[m];
    }
  }
  return CEED_ERROR_SUCCESS;
}

/// @}
