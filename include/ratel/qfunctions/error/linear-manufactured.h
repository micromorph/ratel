/// @file
/// Ratel error computation linear elasticity manufactured solution QFunction source
#include <ceed/types.h>

#include "../mms-solution/linear.h"  // IWYU pragma: export

/// @addtogroup RatelInternal
/// @{

/**
  @brief Compute error from true solution for linear elasticity MMS

  @details Uses an MMS adapted from doi:10.1016/j.compstruc.2019.106175

  @param[in]   ctx  QFunction context, unused
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - quadrature point coordinates
                      - 2 - computed solution
  @param[out]  out  Output array
                      - 0 - error from true solution

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(MMSError_Linear)(void *ctx, const CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*coords)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[1];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[2];

  // Outputs
  CeedScalar(*error)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Setup
    const CeedScalar wdetJ            = q_data[0][i];
    const CeedScalar X[3]             = {coords[0][i], coords[1][i], coords[2][i]};
    CeedScalar       true_solution[3] = {0., 0., 0.};

    // Error
    RatelLinearElasticityMMSTrueSolution(ctx, X, true_solution);
    error[0][i] = (true_solution[0] - u[0][i]) * (true_solution[0] - u[0][i]) * wdetJ;
    error[1][i] = (true_solution[1] - u[1][i]) * (true_solution[1] - u[1][i]) * wdetJ;
    error[2][i] = (true_solution[2] - u[2][i]) * (true_solution[2] - u[2][i]) * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
