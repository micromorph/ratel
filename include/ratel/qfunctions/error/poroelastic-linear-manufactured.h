/// @file
/// Ratel Dirichlet boundary computation mixed linear poroelasticity manufactured solution QFunction source
#include <ceed/types.h>

#include "../mms-solution/poroelastic-linear.h"  // IWYU pragma: export

/// @addtogroup RatelBoundary
/// @{

/**
  @brief Compute error from true solution for mixed linear poroelasticity MMS

  @param[in]   ctx  QFunction context, holding `RatelMMSPoroElasticityLinearParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - quadrature point coordinates
                      - 2 - computed solution, displacement field
                      - 3 - computed solution, pressure field
  @param[out]  out  Output array
                      - 0 - error from true solution, displacement field
                      - 1 - error from true solution, pressure field

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(MMSError_PoroElasticityLinear)(void *ctx, const CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*coords)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[1];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[2];
  const CeedScalar(*p)                  = in[3];

  // Outputs
  CeedScalar(*error_u)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];
  CeedScalar(*error_p)             = out[1];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Setup
    const CeedScalar wdetJ              = q_data[0][i];
    const CeedScalar X[3]               = {coords[0][i], coords[1][i], coords[2][i]};
    CeedScalar       true_solution_u[3] = {0., 0., 0.}, true_solution_p;

    // Error
    RatelPoroElasticityMMSTrueSolution(ctx, X, true_solution_u, &true_solution_p);
    error_u[0][i] = (true_solution_u[0] - u[0][i]) * (true_solution_u[0] - u[0][i]) * wdetJ;
    error_u[1][i] = (true_solution_u[1] - u[1][i]) * (true_solution_u[1] - u[1][i]) * wdetJ;
    error_u[2][i] = (true_solution_u[2] - u[2][i]) * (true_solution_u[2] - u[2][i]) * wdetJ;
    error_p[i]    = (true_solution_p - p[i]) * (true_solution_p - p[i]) * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
