/// @file
/// Ratel error computation CEED vector BPs manufactured solution QFunction source
#include <ceed/types.h>

#include "../mms-solution/ceed-vector-bps.h"  // IWYU pragma: export

/// @addtogroup RatelInternal
/// @{

/**
  @brief Compute error from true solution for CEED vector BPs MMS

  @param[in]   ctx  QFunction context, holding `RatelMMSCEEDBPsParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - quadrature point coordinates
                      - 2 - computed solution
  @param[out]  out  Output array
                      - 0 - error from true solution

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(MMSError_CEED_VectorBPs)(void *ctx, const CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*coords)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[1];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[2];

  // Outputs
  CeedScalar(*error)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Setup
    const CeedScalar wdetJ            = q_data[0][i];
    const CeedScalar X[3]             = {coords[0][i], coords[1][i], coords[2][i]};
    CeedScalar       true_solution[3] = {0., 0., 0.};

    RatelVectorBPsMMSTrueSolution(ctx, X, true_solution);
    for (CeedInt j = 0; j < 3; j++) error[j][i] = (u[j][i] - true_solution[j]) * (u[j][i] - true_solution[j]) * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
