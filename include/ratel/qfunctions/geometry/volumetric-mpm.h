/// @file
/// Ratel volumetric geometric factors computation QFunction source
#pragma once

#include <ceed/types.h>

#include "../utils.h"  // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

#define Q_DATA_VOLUMETRIC_GEOMETRY_MPM_SIZE 10

/**
  @brief Update swarm volume based on previous converged solution for hyperelastic materials.

  @param[in]   ctx  QFunction context, unused
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - gradient of u with respect to reference coordinates
                      - 2 - Volume in the previous converged solution
  @param[out]  out  Output array
                      - 0 - Updated volume J*V_prev

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(UpdateVolume_MPM)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];
  const CeedScalar *volume_prev         = (const CeedScalar *)in[2];

  // Outputs
  CeedScalar *volume = out[0];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar dXdx[3][3], grad_u[3][3];

    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Read spatial derivatives of u; du/dX
    CeedScalar dudX[3][3];
    RatelGradUnpack(Q, i, ug, dudX);

    // Compute grad_u = du/dX * dX/dx
    // X is ref coordinate [-1,1]^3; x is physical coordinate
    RatelMatMatMult(1.0, dudX, dXdx, grad_u);

    // Compute J-1
    const CeedScalar Jm1 = RatelMatDetAM1(grad_u);
    const CeedScalar J   = Jm1 + 1.0;

    volume[i] = volume_prev[i] * J;
  }
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Update swarm volume based on previous converged solution for 4 component materials.

  @param[in]   ctx  QFunction context, unused
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - gradient of u_tilde with respect to previous coordinates X_previous
                      - 2 - volume in the previous converged solution
  @param[out]  out  Output array
                      - 0 - Updated volume J*V_prev

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(UpdateVolume_Damage_MPM)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA]       = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u_tilde_g)[4][CEED_Q_VLA] = (const CeedScalar(*)[4][CEED_Q_VLA])in[1];
  const CeedScalar *volume_prev               = (const CeedScalar *)in[2];

  // Outputs
  CeedScalar *volume = out[0];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar dXdx_previous[3][3], Grad_u_tilde[3][3];

    RatelQdataUnpack(Q, i, q_data, dXdx_previous);

    // Read derivatives of u_tilde; du_tilde/dX
    const CeedScalar du_tildedX[3][3] = {
        {u_tilde_g[0][0][i], u_tilde_g[1][0][i], u_tilde_g[2][0][i]},
        {u_tilde_g[0][1][i], u_tilde_g[1][1][i], u_tilde_g[2][1][i]},
        {u_tilde_g[0][2][i], u_tilde_g[1][2][i], u_tilde_g[2][2][i]}
    };

    // Compute Grad_u_tilde = du_tilde/dX * dX/dx_previous
    // X is ref coordinate [-1,1]^3; x_previous is physical coordinate at previous converged solution
    RatelMatMatMult(1.0, du_tildedX, dXdx_previous, Grad_u_tilde);

    // Compute J-1
    const CeedScalar Jm1 = RatelMatDetAM1(Grad_u_tilde);
    const CeedScalar J   = Jm1 + 1.0;

    volume[i] = volume_prev[i] * J;
  }
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute geometric factors for integration, gradient transformations, and coordinate transformations in elements.

  Reference (parent) coordinates are given by `X` and physical (previous converged) coordinates are given by `x`.
  Change of coordinate matrix is given by `dxdX_{i,j} = x_{i,j} (indicial notation)`.
  Inverse of change of coordinate matrix is given by `dXdx_{i,j} = (detJ^-1) * X_{i,j}`.

  We require the transpose of the inverse of the Jacobian to properly compute integrals of the form `int( gradv u )`.

  @param[in]   ctx  QFunction context, unused
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - Jacobian of cell coordinates
                      - 1 - quadrature weights
                      - 2 - point volumes
  @param[out]  out  Output array
                      - 0 - qdata, `w V_p` and `dXdx`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(SetupVolumeGeometryMPM)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*J)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[0];
  const CeedScalar(*w)                = in[1];
  const CeedScalar(*V)                = in[2];

  // Outputs
  CeedScalar *q_data = out[0];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Setup
    CeedScalar dxdX[3][3];
    RatelGradUnpack(Q, i, J, dxdX);

    const CeedScalar detJ = RatelMatDetA(dxdX);
    const CeedScalar wV   = w[i] * V[i];

    CeedScalar dXdx[3][3];
    RatelMatInverse(dxdX, detJ, dXdx);
    RatelStoredValuesPack(Q, i, 0, 1, &wV, q_data);
    RatelStoredValuesPack(Q, i, 1, 9, (const CeedScalar *)dXdx, q_data);
  }  // End of Quadrature Point Loop
  (void)UpdateVolume_MPM;
  (void)UpdateVolume_Damage_MPM;
  return CEED_ERROR_SUCCESS;
}

/// @}
