/// @file
/// Ratel surface diagnostic geometric factors computation QFunction source
#include <ceed/types.h>

#include "../utils.h"  // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

#define Q_DATA_SURFACE_FORCE_GEOMETRY_SIZE 13

/**
  @brief Compute geometric factors for integration, gradient transformations, and coordinate transformations on element faces.

  Reference (parent) 2D coordinates are given by `X` and physical (current) 3D coordinates are given by `x`.
  The change of coordinate matrix is given by`dxdX_{i,j} = dx_i/dX_j (indicial notation) [3 * 2]`.

  `(N_1, N_2, N_3)` is given by the cross product of the columns of `dxdX_{i,j}`.

  `detNb` is the magnitude of `(N_1, N_2, N_3)`.

  @param[in]   ctx  QFunction context, unused
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - Jacobian of cell coordinates
                      - 1 - Jacobian of face coordinates
                      - 2 - quadrature weights
  @param[out]  out  Output array
                      - 0 - qdata, `w detNb`, `dXdx`, and `N`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(SetupSurfaceForceGeometry)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*J_cell)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[0];
  const CeedScalar(*J_face)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];
  const CeedScalar(*w)                     = in[2];

  // Outputs
  CeedScalar(*q_data)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  const CeedInt dim = 3;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Setup
    // N_1, N_2, and N_3 are given by the cross product of the columns of dxdX
    CeedScalar normal[3];

    for (CeedInt j = 0; j < dim; j++) {
      // Equivalent code with no mod operations:
      // normal[j] = J_face[0][j+1]*J_face[1][j+2] - J_face[0][j+2]*J_face[1][j+1]
      normal[j] = J_face[0][(j + 1) % dim][i] * J_face[1][(j + 2) % dim][i] - J_face[0][(j + 2) % dim][i] * J_face[1][(j + 1) % dim][i];
    }
    const CeedScalar detJ_face = RatelNorm3(normal);
    // Gradient
    CeedScalar dxdX[3][3];
    RatelGradUnpack(Q, i, J_cell, dxdX);

    const CeedScalar detJ_cell = RatelMatDetA(dxdX);
    CeedScalar       dXdx[3][3];

    RatelMatInverse(dxdX, detJ_cell, dXdx);

    // Qdata
    // -- Interp-to-Interp q_data
    q_data[0][i] = w[i] * detJ_face;
    for (CeedInt j = 0; j < 3; j++) {
      for (CeedInt k = 0; k < 3; k++) {
        q_data[j * 3 + k + 1][i] = dXdx[j][k];
      }
    }
    // -- Normal vector
    for (CeedInt j = 0; j < 3; j++) q_data[j + 10][i] = normal[j] / detJ_face;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
