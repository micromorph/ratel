/// @file
/// Ratel surface geometric factors computation QFunction source
#include <ceed/types.h>

#include "../../models/bounding-box.h"  // IWYU pragma: export
#include "../utils.h"                   // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

typedef struct {
  RatelBoundingBoxParams box;
  CeedInt                num_comp;
} RatelBoundingBoxParamsRestrictionContext;

/**
  @brief Restrict the bounding box of the domain to a given bounding box.



  @param[in]   ctx  QFunction context, `RatelBoundingBoxParamsContext`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - x, physical (initial) coordinates
                      - 1 - scale, multiplicity scaling factor
                      - 2 - r, current timestep residual
  @param[out]  out  Output array
                      - 0 - v, current timestep residual restricted to bounding box

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(RestrictBoundingBox)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*x)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar *scale          = (const CeedScalar *)in[1];
  const CeedScalar(*r)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[2];

  // Outputs
  CeedScalar(*v)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Context
  RatelBoundingBoxParamsRestrictionContext *bounding = (RatelBoundingBoxParamsRestrictionContext *)ctx;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    const CeedScalar xi[3]  = {x[0][i], x[1][i], x[2][i]};
    const bool       inside = RatelCoordinatesInBoundingBox(xi, bounding->box.min, bounding->box.max);

    for (CeedInt j = 0; j < bounding->num_comp; j++) v[j][i] = inside ? scale[i] * r[j][i] : 0.0;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}
/// @}
