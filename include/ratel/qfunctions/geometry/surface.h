/// @file
/// Ratel surface geometric factors computation QFunction source
#include <ceed/types.h>

#include "../utils.h"  // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

#define Q_DATA_SURFACE_GEOMETRY_SIZE 4

/**
  @brief Compute geometric factors for integration, gradient transformations, and coordinate transformations on element faces.

  Reference (parent) 2D coordinates are given by `X` and physical (current) 3D coordinates are given by `x`.
  The change of coordinate matrix is given by`dxdX_{i,j} = dx_i/dX_j (indicial notation) [3 * 2]`.

  `(N_1, N_2, N_3)` is given by the cross product of the columns of `dxdX_{i,j}`.

  `detNb` is the magnitude of `(N_1, N_2, N_3)`.

  @param[in]   ctx  QFunction context, unused
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - Jacobian of face coordinates
                      - 1 - quadrature weights
  @param[out]  out  Output array
                      - 0 - qdata, `w detNb` and `N`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(SetupSurfaceGeometry)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*J)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[0];
  const CeedScalar(*w)                = in[1];

  // Outputs
  CeedScalar(*q_data)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  const CeedInt dim = 3;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Setup
    // N_1, N_2, and N_3 are given by the cross product of the columns of dxdX
    CeedScalar normal[3];

    for (CeedInt j = 0; j < dim; j++) {
      // Equivalent code with no mod operations:
      // normal[j] = J[0][j+1]*J[1][j+2] - J[0][j+2]*J[1][j+1]
      normal[j] = J[0][(j + 1) % dim][i] * J[1][(j + 2) % dim][i] - J[0][(j + 2) % dim][i] * J[1][(j + 1) % dim][i];
    }
    const CeedScalar detJb = RatelNorm3(normal);

    // Qdata
    // -- Interp-to-Interp q_data
    q_data[0][i] = w[i] * detJb;
    // -- Normal vector
    for (CeedInt j = 0; j < dim; j++) q_data[j + 1][i] = normal[j] / detJb;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
