/// @file
/// Ratel volumetric geometric factors computation QFunction source
#include <ceed/types.h>

#include "../utils.h"  // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

#define Q_DATA_VOLUMETRIC_GEOMETRY_SIZE 10

/**
  @brief Compute geometric factors for integration, gradient transformations, and coordinate transformations in elements.

  Reference (parent) coordinates are given by `X` and physical (current) coordinates are given by `x`.
  Change of coordinate matrix is given by `dxdX_{i,j} = x_{i,j} (indicial notation)`.
  Inverse of change of coordinate matrix is given by `dXdx_{i,j} = (detJ^-1) * X_{i,j}`.

  We require the transpose of the inverse of the Jacobian to properly compute integrals of the form `int( gradv u )`.

  @param[in]   ctx  QFunction context, unused
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - Jacobian of cell coordinates
                      - 1 - quadrature weights
  @param[out]  out  Output array
                      - 0 - qdata, `w detJ` and `dXdx`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(SetupVolumeGeometry)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*J)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[0];
  const CeedScalar(*w)                = in[1];

  // Outputs
  CeedScalar(*q_data)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Setup
    CeedScalar dxdX[3][3], wdetJ;
    RatelGradUnpack(Q, i, J, dxdX);

    const CeedScalar detJ = RatelMatDetA(dxdX);
    CeedScalar       dXdx[3][3];

    RatelMatInverse(dxdX, detJ, dXdx);
    wdetJ = w[i] * detJ;
    RatelQdataPack(Q, i, wdetJ, dXdx, q_data);
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
