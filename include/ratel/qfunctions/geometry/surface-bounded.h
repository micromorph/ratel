/// @file
/// Ratel bounded surface geometric factors computation QFunction source
#include <ceed/types.h>

#include "../../models/bounding-box.h"  // IWYU pragma: export
#include "../utils.h"                   // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

/**
@brief Compute geometric factors for integration, gradient transformations, and coordinate transformations on element faces, restricted by a
bounding box.

Reference (parent) 2D coordinates are given by `X` and physical (current) 3D coordinates are given by `x`.
The change of coordinate matrix is given by`dxdX_{i,j} = dx_i/dX_j (indicial notation) [3 * 2]`.

`(N_1, N_2, N_3)` is given by the cross product of the columns of `dxdX_{i,j}`.

`detNb` is the magnitude of `(N_1, N_2, N_3)`.

@param[in]   ctx  QFunction context, `RatelBoundingBoxParams`
@param[in]   Q    Number of quadrature points
@param[in]   in   Input arrays
                    - 0 - Face coordinates
                    - 1 - Jacobian of face coordinates
                    - 2 - quadrature weights
@param[out]  out  Output array
                    - 0 - qdata, `w detNb` and `N`

@return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(SetupSurfaceGeometryBounded)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*x)[CEED_Q_VLA]    = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*J)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];
  const CeedScalar(*w)                = in[2];

  // Outputs
  CeedScalar(*q_data)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Context
  const RatelBoundingBoxParams *box = (const RatelBoundingBoxParams *)ctx;

  const CeedInt dim = 3;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Setup
    // N_1, N_2, and N_3 are given by the cross product of the columns of dxdX
    CeedScalar       normal[3];
    const CeedScalar xi[3]  = {x[0][i], x[1][i], x[2][i]};
    const bool       inside = RatelCoordinatesInBoundingBox(xi, box->min, box->max);

    for (CeedInt j = 0; j < dim; j++) {
      // Equivalent code with no mod operations:
      // normal[j] = J[0][j+1]*J[1][j+2] - J[0][j+2]*J[1][j+1]
      normal[j] = J[0][(j + 1) % dim][i] * J[1][(j + 2) % dim][i] - J[0][(j + 2) % dim][i] * J[1][(j + 1) % dim][i];
    }
    const CeedScalar detJb = RatelNorm3(normal);

    // Qdata
    // -- Interp-to-Interp q_data, set to zero if outside bounding box
    q_data[0][i] = inside ? w[i] * detJb : 0.0;
    // -- Normal vector
    for (CeedInt j = 0; j < dim; j++) q_data[j + 1][i] = normal[j] / detJb;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
