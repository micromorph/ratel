/// @file
/// Ratel flexible loading for boundary conditions utilities
#pragma once

#include <ceed/types.h>

#include "../../types.h"  // IWYU pragma: export

/// @addtogroup RatelBoundary
/// @{

/**
  @brief Compute the index `j \in [0, num_knots]` such that `knots[j] <= t < knots[j+1]`.

  If `t < knots[0]`, returns -1.

  @param[in]   t          Current time
  @param[in]   num_knots  Number of knots
  @param[in]   knots      Values of knots

  @return A scalar value: the index of the previous knot
**/
CEED_QFUNCTION_HELPER CeedInt RatelBCInterpGetPreviousKnotIndex(CeedScalar t, CeedInt num_knots, const CeedScalar* knots) {
  CeedInt prev_index = -1;
  for (CeedInt k = 0; k < num_knots; k++) {
    prev_index += (knots[k] < t && fabs(knots[k] - t) > CEED_EPSILON);
  }
  return prev_index;
}

/**
  @brief Compute rescaled time between knots, \f$s \in [0,1)\f$.

  If `0 <= prev_knot_index < num_knots-1`, returns `s = (t - knots[prev_knot_index]) / (knots[prev_knot_index + 1])`.
  If `t` is before the first knot, i.e. prev_knot_index == -1, implicitly inserts a knot at time 0 and returns `s = t / knots[0]`.
  If `t` is past the final knot, returns `s = 1`.
  If `t > knots[num_knots - 1]`, returns `points[num_knots - 1]`.

  @param[in]   t                Current time
  @param[in]   prev_knot_index  Index of last knot
  @param[in]   num_knots        Number of knots
  @param[in]   knots            Values of knots

  @return A scalar value: the rescaled time between knots
**/
CEED_QFUNCTION_HELPER CeedScalar RatelBCInterpScaleTime(CeedScalar t, CeedInt prev_knot_index, CeedInt num_knots, const CeedScalar* knots) {
  const bool is_first  = prev_knot_index < 0;
  const bool is_last   = prev_knot_index == num_knots - 1;
  CeedScalar prev_knot = is_first ? 0 : knots[prev_knot_index];
  return is_last ? 1 : (t - prev_knot) / (knots[prev_knot_index + 1] - prev_knot);
}

/**
  @brief Compute the interpolated value between the given points and knots.

  For `t > knots[num_knots-1]`, returns `points[num_knots-1]`.

  @param[in]   type             If type is
                                  - `RATEL_BC_INTERP_NONE` - returns the point corresponding to the first knot after time t.
                                  - `RATEL_BC_INTERP_LINEAR` - returns the result of linearly interpolating between adjacent points.
  @param[in]   t                Current time
  @param[in]   prev_knot_index  Index of last knot
  @param[in]   num_knots        Number of knots
  @param[in]   knots            Values of knots
  @param[in]   point_dim        Dimension of coordinates
  @param[in]   points           Coordinates of knots
  @param[out]  out_point        Interpolated value

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelBCInterpolate(RatelBCInterpolationType type, CeedScalar t, CeedInt prev_knot_index, CeedInt num_knots,
                                             const CeedScalar* knots, CeedInt point_dim, const CeedScalar* points, CeedScalar* out_point) {
  const bool is_first = prev_knot_index < 0;
  const bool is_last  = prev_knot_index == num_knots - 1;

  switch (type) {
    case RATEL_BC_INTERP_NONE: {
      for (CeedInt j = 0; j < point_dim; j++) {
        out_point[j] = is_last ? 0 : points[(prev_knot_index + 1) * point_dim + j];
      }
      break;
    }
    case RATEL_BC_INTERP_LINEAR: {
      const CeedScalar s           = RatelBCInterpScaleTime(t, prev_knot_index, num_knots, knots);
      // If time is before first knot, we should implicitly start from zero
      const CeedScalar  zero[3]    = {0.};
      const CeedScalar* prev_point = is_first ? zero : &points[prev_knot_index * point_dim];
      // If time is past last knot, we should hold final value
      const CeedScalar* next_point = is_last ? &points[prev_knot_index * point_dim] : &points[(prev_knot_index + 1) * point_dim];

      for (CeedInt j = 0; j < point_dim; j++) {
        out_point[j] = (1 - s) * prev_point[j] + s * next_point[j];
      }
      break;
    }
  }
  return CEED_ERROR_SUCCESS;
}

/// @}
