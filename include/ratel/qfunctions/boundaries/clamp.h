/// @file
/// Ratel clamp boundary condition QFunction source
#include <ceed/types.h>
#ifndef CEED_RUNNING_JIT_PASS
#include <math.h>
#endif

#include "../../models/boundary.h"  // IWYU pragma: export
#include "../utils.h"               // IWYU pragma: export
#include "flexible-loading.h"       // IWYU pragma: export

/// @addtogroup RatelBoundary
/// @{

/**
  @brief Compute Dirichlet (clamp) boundary values

  @param[in]   ctx  QFunction context, holding `RatelBCClampParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - stored coordinates
                      - 1 - stored multiplicity scaling factor
  @param[out]  out  Output array
                      - 0 - Dirichlet values

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ClampBCs)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*coords)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*scale)              = (const CeedScalar(*))in[1];

  // Outputs
  CeedScalar(*u)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // User context
  const RatelBCClampParams *context         = (RatelBCClampParams *)ctx;
  const CeedScalar          dt              = context->dt;
  const CeedInt             prev_time_index = RatelBCInterpGetPreviousKnotIndex(context->time, context->num_times, context->times);
  CeedScalar                translation[3]  = {0.};

  RatelBCInterpolate(context->interpolation_type, context->time, prev_time_index, context->num_times, context->times, 3, context->translation,
                     translation);
  CeedScalar rotation_polynomial[2] = {0.};

  RatelBCInterpolate(context->interpolation_type, context->time, prev_time_index, context->num_times, context->times, 2, context->rotation_polynomial,
                     rotation_polynomial);
  // We do not interpolate the rotation axis
  CeedScalar rotation_axis[3] = {0.};

  RatelBCInterpolate(RATEL_BC_INTERP_NONE, context->time, prev_time_index, context->num_times, context->times, 3, context->rotation_axis,
                     rotation_axis);

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Coordinates
    const CeedScalar x  = coords[0][i];
    const CeedScalar y  = coords[1][i];
    const CeedScalar z  = coords[2][i];
    // Translation vector
    const CeedScalar lx = translation[0], ly = translation[1], lz = translation[2];
    // Normalized rotation axis
    const CeedScalar kx = rotation_axis[0], ky = rotation_axis[1], kz = rotation_axis[2];
    // Rotation polynomial
    const CeedScalar c_0 = rotation_polynomial[0] * RATEL_PI_DOUBLE, c_1 = rotation_polynomial[1] * RATEL_PI_DOUBLE;
    const CeedScalar cx        = kx * x + ky * y + kz * z;
    // Rotation magnitude
    const CeedScalar theta     = c_0 + cx * c_1;
    const CeedScalar cos_theta = cos(theta), sin_theta = sin(theta);

    // Compute new boundary displacement
    u[0][i] = lx + sin_theta * (-kz * y + ky * z) + (1 - cos_theta) * (-(ky * ky + kz * kz) * x + kx * ky * y + kx * kz * z);
    u[1][i] = ly + sin_theta * (kz * x + -kx * z) + (1 - cos_theta) * (kx * ky * x + -(kx * kx + kz * kz) * y + ky * kz * z);
    u[2][i] = lz + sin_theta * (-ky * x + kx * y) + (1 - cos_theta) * (kx * kz * x + ky * kz * y + -(kx * kx + ky * ky) * z);
    for (CeedInt j = 0; j < 3; j++) u[j][i] *= dt * scale[i];
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
