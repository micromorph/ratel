/// @file
/// Ratel commoon Dirichlet boundary condition QFunction source
#include <ceed/types.h>

/// @addtogroup RatelBoundary
/// @{

/**
  @brief Setup coordinate and scaling data for Dirichlet boundary

  @param[in]   ctx  QFunction context, unused
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - nodal coordinates
                      - 1 - nodal multiplicity
  @param[out]  out  Output array
                      - 0 - stored coordinates
                      - 1 - stored multiplicity scaling factor

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(SetupDirichletBCs)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*coords)[CEED_Q_VLA]       = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*multiplicity)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*coords_stored)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];
  CeedScalar(*scale_stored)              = (CeedScalar(*))out[1];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Stored values
    for (CeedInt j = 0; j < 3; j++) coords_stored[j][i] = coords[j][i];
    scale_stored[i] = 1.0 / multiplicity[0][i];
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
