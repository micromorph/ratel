/// @file
/// Ratel slip boundary condition QFunction source
#include <ceed/types.h>

#include "../../models/boundary.h"  // IWYU pragma: export
#include "../utils.h"               // IWYU pragma: export
#include "flexible-loading.h"       // IWYU pragma: export

/// @addtogroup RatelBoundary
/// @{

/**
  @brief Setup scaling data for Dirichlet (clamp) boundary

  @param[in]   ctx  QFunction context, unused
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - nodal multiplicity
  @param[out]  out  Output array
                      - 0 - stored multiplicity scaling factor

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(SetupSlipBCs)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*multiplicity)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];

  // Outputs
  CeedScalar(*scale_stored) = (CeedScalar(*))out[0];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Stored values
    scale_stored[i] = 1.0 / multiplicity[0][i];
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute Dirichlet (slip) boundary values

  @param[in]   ctx  QFunction context, holding `RatelBCSlipParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - stored multiplicity scaling factor
  @param[out]  out  Output array
                      - 0 - Dirichlet values

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(SlipBCs)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*scale) = (const CeedScalar(*))in[0];

  // Outputs
  CeedScalar(*u)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // User context
  const RatelBCSlipParams *context         = (RatelBCSlipParams *)ctx;
  const CeedScalar         dt              = context->dt;
  const CeedInt            prev_time_index = RatelBCInterpGetPreviousKnotIndex(context->time, context->num_times, context->times);
  CeedScalar               translation[3]  = {0.};

  RatelBCInterpolate(context->interpolation_type, context->time, prev_time_index, context->num_times, context->times, context->num_comp_slip,
                     context->translation, translation);
  // Store translation for all components, including zeros for unconstrained
  CeedScalar constrained_values[4] = {0};
  for (CeedInt j = 0; j < context->num_comp_slip; j++) {
    constrained_values[context->components[j]] = translation[j];
  }

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Compute new boundary displacement
    for (CeedInt j = 0; j < context->num_comp; j++) {
      u[j][i] = dt * scale[i] * constrained_values[j];
    }
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
