/// @file
/// Ratel Dirichlet boundary computation CEED scalar BPs manufactured solution QFunction source
#include <ceed/types.h>

#include "../mms-solution/ceed-scalar-bps.h"  // IWYU pragma: export

/// @addtogroup RatelBoundary
/// @{

/**
  @brief Compute Dirichlet MMS boundary values for CEED scalar BPs

  @param[in]   ctx  QFunction context, holding `RatelMMSCEEDBPsParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - stored coordinates
                      - 1 - stored multiplicity scaling factor
  @param[out]  out  Output array
                      - 0 - Dirichlet values

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(MMSBCs_CEED_ScalarBPs)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*coords)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*scale)              = (const CeedScalar(*))in[1];

  // Outputs
  CeedScalar(*u) = (CeedScalar(*))out[0];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Setup
    const CeedScalar X[3] = {coords[0][i], coords[1][i], coords[2][i]};
    CeedScalar       true_solution;

    RatelScalarBPsMMSTrueSolution(ctx, X, &true_solution);
    u[i] = true_solution * scale[i];
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
