/// @file
/// Ratel traction boundary condition QFunction source
#include <ceed/types.h>

#include "../../models/boundary.h"  // IWYU pragma: export
#include "flexible-loading.h"       // IWYU pragma: export

/// @addtogroup RatelBoundary
/// @{

/**
  @brief Compute surface integral of the traction vector on the constrained faces

  @param[in]   ctx  QFunction context, holding `RatelBCTractionParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - surface qdata
  @param[out]  out  Output array
                      - 0 - `v = t * (w detNb)`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(TractionBCs)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];

  // Outputs
  CeedScalar(*v)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // User context
  const RatelBCTractionParams *context         = (RatelBCTractionParams *)ctx;
  const CeedInt                prev_time_index = RatelBCInterpGetPreviousKnotIndex(context->time, context->num_times, context->times);
  CeedScalar                   direction[3]    = {0.};

  RatelBCInterpolate(context->interpolation_type, context->time, prev_time_index, context->num_times, context->times, 3, context->direction,
                     direction);

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Qdata
    const CeedScalar wdetNb = q_data[0][i];

    // Traction surface integral
    for (CeedInt j = 0; j < 3; j++) {
      v[j][i] = -direction[j] * wdetNb;
    }
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute external energy cause by the traction vector on the constrained faces

  @param[in]   ctx  QFunction context, holding `RatelBCTractionParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - surface qdata
                      - 1 - displacement u
  @param[out]  out  Output array
                      - 0 - traction energy `u t * (w detNb)`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(TractionEnergy)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*traction_energy) = (CeedScalar(*))out[0];

  // User context
  const RatelBCTractionParams *context         = (RatelBCTractionParams *)ctx;
  const CeedInt                prev_time_index = RatelBCInterpGetPreviousKnotIndex(context->time, context->num_times, context->times);
  CeedScalar                   direction[3]    = {0.};

  RatelBCInterpolate(context->interpolation_type, context->time, prev_time_index, context->num_times, context->times, 3, context->direction,
                     direction);

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Qdata
    const CeedScalar wdetNb  = q_data[0][i];
    CeedScalar       u_dot_t = 0.0;

    // Traction surface integral
    for (CeedInt j = 0; j < 3; j++) {
      u_dot_t += u[j][i] * direction[j];
    }
    traction_energy[i] = -u_dot_t * wdetNb;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
