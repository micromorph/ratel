/// @file
/// Ratel platen boundary condition QFunction source
#pragma once

#include <ceed/types.h>

#include "../../models/platen.h"          // IWYU pragma: export
#include "../models/elasticity-common.h"  // IWYU pragma: export
#include "friction.h"                     // IWYU pragma: export

/// @addtogroup RatelBoundary
/// @{

#define FLOPS_Platen_without_df1 (7 * FLOPS_Dot3 + 64)

/**
  @brief Compute the surface integral of the platen contact boundary condition.

  Updated to support initial gap using https://hal.archives-ouvertes.fr/hal-00722114/document.

  In the frictionless case, compute
  ```
  (f1_gamma < 0) ? (f1_gamma * n_platen * (w detNb)) : 0
  ```
  where
  - `f1_gamma = f1_n + gamma*gap`
  - `f1_n = (f1·n_platen)·n_platen` is the contact pressure between the surface and platen
  - `gap = (X + u - C)·n_platen` is the gap between the surface and the platen in the current configuration

  For linear elasticity `f1 = sigma`, where `sigma` is Cauchy stress tensor.
  For hyperelastic in initial configuration `f1 = P`, where `P` is First Piola Kirchhoff stress.
  For hyperelastic in current configuration `f1 = tau`, where tau is Kirchhoff stress.

  For the frictional case, see the theory section of the documentation

  @param[in]   ctx                          QFunction platen, holding `RatelBCPlatenParams`
  @param[in]   Q                            Number of quadrature points
  @param[in]   compute_f1                   Function to compute f1
  @param[in]   has_state_values             Boolean flag indicating model state values in residual evaluation
  @param[in]   has_stored_values            Boolean flag indicating model stores values in residual evaluation
  @param[in]   num_active_field_eval_modes  Number of active field evaluation modes
  @param[in]   in                           Input arrays
                                              - 0                       - qdata
                                              - `input_data_offset`     - `u`
                                              - `input_data_offset + 1` - `du/dt` (or `u` in static case)
                                              - `input_data_offset + 2` - quadrature point coordinates
  @param[out]  out                          Output array
                                              - `output_data_offset`    - action of QFunction on `u`
                                              - `output_data_offset + ` - stored `f1_n + gamma*gap` and `f1_gamma_T - gamma*(du/dt)_T`

  @return An error code: 0 - success, otherwise - failure
**/
int CEED_QFUNCTION_HELPER PlatenBCs(void *ctx, CeedInt Q, RatelComputef1 compute_f1, bool has_state_values, bool has_stored_values,
                                    CeedInt num_active_field_eval_modes, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedInt input_data_offset       = 1 + has_state_values + num_active_field_eval_modes;
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[input_data_offset];
  const CeedScalar(*u_dot)[CEED_Q_VLA]  = (const CeedScalar(*)[CEED_Q_VLA])in[input_data_offset + 1];
  const CeedScalar(*x)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[input_data_offset + 2];

  // Outputs
  const CeedInt output_data_offset              = has_state_values + has_stored_values;
  CeedScalar(*v)[CEED_Q_VLA]                    = (CeedScalar(*)[CEED_Q_VLA])out[output_data_offset];
  CeedScalar(*stored_values_platen)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[output_data_offset + 1];

  // User context
  const RatelBCPlatenParams       *context             = (RatelBCPlatenParams *)ctx;
  const RatelBCPlatenParamsCommon *platen              = &context->platen;
  const RatelFrictionParams       *friction            = &platen->friction;
  const CeedScalar                 viscous_coefficient = friction->viscous_coefficient;
  const CeedScalar                 gamma               = platen->gamma;
  const CeedScalar                 platen_normal[3]    = {platen->normal[0], platen->normal[1], platen->normal[2]};

  // Compute current center
  const CeedInt prev_time_index = RatelBCInterpGetPreviousKnotIndex(context->time, platen->num_times, platen->times);
  CeedScalar    distance        = 0;
  RatelBCInterpolate(platen->interpolation_type, context->time, prev_time_index, platen->num_times, platen->times, 1, platen->distance, &distance);
  CeedScalar current_center[3];
  for (CeedInt j = 0; j < 3; j++) {
    current_center[j] = platen->center[j] + distance * platen_normal[j];
  }

  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    const CeedScalar wdetJb = q_data[0][i];
    CeedScalar       dXdx[3][3], f1[3][3];

    compute_f1((void *)&context->material, Q, i, in, out, dXdx, f1);

    const CeedScalar n[3]  = {q_data[10][i], q_data[11][i], q_data[12][i]};
    const CeedScalar ui[3] = {u[0][i], u[1][i], u[2][i]};

    // Compute overlap in current configuration
    const CeedScalar overlap_vec[3] = {
        ui[0] + x[0][i] - current_center[0],
        ui[1] + x[1][i] - current_center[1],
        ui[2] + x[2][i] - current_center[2],
    };

    // And compute gap as magnitude of projection of above vector onto face normal
    const CeedScalar gap = RatelDot3(overlap_vec, platen_normal);

    // Compute boundary traction
    CeedScalar f1_dot_n[3] = {0., 0., 0.};

    for (CeedInt j = 0; j < 3; j++) {
      f1_dot_n[j] = -RatelDot3(f1[j], n);
    }
    const CeedScalar f1_N       = RatelDot3(f1_dot_n, platen_normal);
    const CeedScalar f1_gamma_n = f1_N + gamma * gap;

    stored_values_platen[0][i] = f1_gamma_n;
    for (CeedInt j = 0; j < 3; j++) {
      stored_values_platen[j + 1][i] = f1_dot_n[j] - f1_N * platen_normal[j];
    }

    // Normal traction component
    for (CeedInt j = 0; j < 3; j++) {
      v[j][i] = (f1_gamma_n < 0) * (f1_gamma_n * platen_normal[j] * wdetJb);
    }
  }  // End of Quadrature Point Loop

  // Quadrature Point Loop
  switch (friction->model) {
    case RATEL_FRICTION_COULOMB:
      CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
        // QData
        const CeedScalar wdetJb = q_data[0][i];

        // Compute tangent velocity
        CeedScalar       velocity_T[3] = {0};
        const CeedScalar velocity[3]   = {u_dot[0][i], u_dot[1][i], u_dot[2][i]};
        const CeedScalar u_dot_n       = RatelDot3(velocity, platen_normal);
        for (CeedInt j = 0; j < 3; j++) {
          velocity_T[j] = velocity[j] - u_dot_n * platen_normal[j];
        }

        // Compute projection of f1 - gamma*u_dot onto tangent plane to n_platen
        // This is the tangent Nitsche traction
        for (CeedInt j = 0; j < 3; j++) {
          stored_values_platen[j + 1][i] -= gamma * velocity_T[j];
        }
        CeedScalar       f1_gamma_T[3] = {stored_values_platen[1][i], stored_values_platen[2][i], stored_values_platen[3][i]};
        const CeedScalar f1_gamma_n    = stored_values_platen[0][i];

        // Apply friction model
        CeedScalar friction_traction[3] = {0.};
        FrictionCoulomb(friction, f1_gamma_T, RatelMin(f1_gamma_n, 0), friction_traction);

        // Add viscous term
        for (CeedInt j = 0; j < 3; j++) {
          v[j][i] += (f1_gamma_n < 0) * (friction_traction[j] + viscous_coefficient * velocity_T[j]) * wdetJb;
        }
      }  // End of Quadrature Point Loop
      break;
    case RATEL_FRICTION_THRELFALL:
      CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
        // QData
        const CeedScalar wdetJb = q_data[0][i];

        // Compute tangent velocity
        CeedScalar       velocity_T[3] = {0};
        const CeedScalar velocity[3]   = {u_dot[0][i], u_dot[1][i], u_dot[2][i]};
        const CeedScalar u_dot_n       = RatelDot3(velocity, platen_normal);
        for (CeedInt j = 0; j < 3; j++) {
          velocity_T[j]                  = velocity[j] - u_dot_n * platen_normal[j];
          stored_values_platen[j + 1][i] = velocity_T[j];
        }
        const CeedScalar f1_gamma_n = stored_values_platen[0][i];

        // Apply friction model
        CeedScalar friction_traction[3] = {0.};
        FrictionThrelfall(friction, velocity_T, RatelMin(f1_gamma_n, 0), friction_traction);

        // Add to output
        for (CeedInt j = 0; j < 3; j++) {
          v[j][i] += friction_traction[j] * wdetJb;
        }
      }  // End of Quadrature Point Loop
      break;
    case RATEL_FRICTION_NONE:
      CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
        for (CeedInt j = 1; j < 4; j++) {
          stored_values_platen[j][i] = 0;
        }
      }  // End of Quadrature Point Loop
      break;
  }
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute the incremental traction due to platen contact boundary condition.

  In the frictionless case compute
  ```
  (f1_gamma < 0) ? ((df1_N + gamma * du_N) * n_platen * (w detNb)) : 0
  ```
  where
  - `df1_N = (df1·n_platen)·n_platen` is the incremental surface force away from platen.
  - `du_N = du·n_platen` is the incremental displacement away from platen.

  For linear elasticity `df1 = dsigma`.
  For hyperelastic in initial configuration `df1 = dP`.
  For hyperelastic in current configuration `df1 = dtau - tau * grad_du^T`.

  For the frictional case, see the theory section of the documentation

  @param[in]   ctx                          QFunction platen, holding `RatelBCPlatenParams`
  @param[in]   Q                            Number of quadrature points
  @param[in]   compute_df1                  Function to compute f1
  @param[in]   has_state_values             Boolean flag indicating model state values in residual evaluation
  @param[in]   has_stored_values            Boolean flag indicating model stores values in residual evaluation
  @param[in]   num_active_field_eval_modes  Number of active fields
  @param[in]   in                           Input arrays
                                              - 0                       - qdata
                                              - `input_data_offset`     - `du`, incremental change to u
                                              - `input_data_offset + 1` - stored values from residual
  @param[out]  out                          Output array
                                              - 0 - action of QFunction

  @return An error code: 0 - success, otherwise - failure
**/
int CEED_QFUNCTION_HELPER PlatenBCs_Jacobian(void *ctx, CeedInt Q, RatelComputef1_fwd compute_df1, bool has_state_values, bool has_stored_values,
                                             CeedInt num_active_field_eval_modes, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedInt input_data_offset                     = 1 + has_state_values + has_stored_values + num_active_field_eval_modes;
  const CeedScalar(*q_data)[CEED_Q_VLA]               = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*du)[CEED_Q_VLA]                   = (const CeedScalar(*)[CEED_Q_VLA])in[input_data_offset];
  const CeedScalar(*stored_values_platen)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[input_data_offset + 1];

  // Outputs
  CeedScalar(*dv)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Temp data
  CeedScalar df1_T[3][CEED_Q_VLA], df1_gamma_n[CEED_Q_VLA];

  // Context
  const RatelBCPlatenParams       *context             = (RatelBCPlatenParams *)ctx;
  const RatelBCPlatenParamsCommon *platen              = &context->platen;
  const CeedScalar                *material_context    = (CeedScalar *)&context->material;
  const RatelFrictionParams       *friction            = &platen->friction;
  const CeedScalar                 viscous_coefficient = friction->viscous_coefficient;
  const CeedScalar                 gamma               = platen->gamma;
  const CeedScalar                 platen_normal[3]    = {platen->normal[0], platen->normal[1], platen->normal[2]};

  // If shift_v is not set, we're running in static mode, so we used u instead of u_t in the traction calculation
  const CeedScalar shift_v = material_context[RATEL_COMMON_PARAMETER_SHIFT_V] != 0 ? material_context[RATEL_COMMON_PARAMETER_SHIFT_V] : 1;

  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // QData
    const CeedScalar wdetJb = q_data[0][i];
    const CeedScalar n[3]   = {q_data[10][i], q_data[11][i], q_data[12][i]};

    // Stored platen values
    const CeedScalar f1_gamma_n = stored_values_platen[0][i];

    CeedScalar dXdx[3][3], df1[3][3];

    compute_df1((void *)material_context, Q, i, in, out, dXdx, df1);

    // Compute du_N = du . n_platen
    const CeedScalar dui[3] = {du[0][i], du[1][i], du[2][i]};
    const CeedScalar du_N   = RatelDot3(dui, platen_normal);

    // Compute linearization of boundary traction
    CeedScalar df1_dot_n[3];
    for (CeedInt j = 0; j < 3; j++) {
      df1_dot_n[j] = -RatelDot3(df1[j], n);
    }
    const CeedScalar df1_N = RatelDot3(df1_dot_n, platen_normal);
    df1_gamma_n[i]         = (f1_gamma_n < 0) * (df1_N + gamma * du_N);

    // Cache for friction
    for (CeedInt j = 0; j < 3; j++) {
      df1_T[j][i] = df1_dot_n[j] - df1_N * platen_normal[j];
    }

    // Normal traction component
    for (CeedInt j = 0; j < 3; j++) {
      dv[j][i] = df1_gamma_n[i] * platen_normal[j] * wdetJb;
    }
  }  // End of Quadrature Point Loop

  switch (friction->model) {
    case RATEL_FRICTION_COULOMB:
      CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
        // QData
        const CeedScalar wdetJb = q_data[0][i];

        // Compute tangent dvelocity
        const CeedScalar dui[3]         = {du[0][i], du[1][i], du[2][i]};
        const CeedScalar du_N           = RatelDot3(dui, platen_normal);
        CeedScalar       dvelocity_T[3] = {0};
        for (CeedInt j = 0; j < 3; j++) {
          dvelocity_T[j] = shift_v * (dui[j] - du_N * platen_normal[j]);
        }

        // Friction traction linearization
        CeedScalar dfriction_traction[3] = {0};

        // Compute tangent traction linearization with Nitsche term
        const CeedScalar f1_gamma_T[3] = {stored_values_platen[1][i], stored_values_platen[2][i], stored_values_platen[3][i]};
        CeedScalar       df1_gamma_T[3];
        for (CeedInt j = 0; j < 3; j++) {
          df1_gamma_T[j] = df1_T[j][i] - gamma * dvelocity_T[j];
        }
        const CeedScalar f1_gamma_n = stored_values_platen[0][i];

        // Apply friction model
        FrictionCoulomb_fwd(friction, f1_gamma_T, df1_gamma_T, RatelMin(f1_gamma_n, 0), df1_gamma_n[i], dfriction_traction);

        // Add contribution of friction traction linearization
        for (CeedInt j = 0; j < 3; j++) {
          dv[j][i] += (f1_gamma_n < 0) * (dfriction_traction[j] + viscous_coefficient * dvelocity_T[j]) * wdetJb;
        }
      }  // End of Quadrature Point Loop
      break;
    case RATEL_FRICTION_THRELFALL:
      CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
        // QData
        const CeedScalar wdetJb = q_data[0][i];

        // Compute tangent dvelocity
        const CeedScalar dui[3]         = {du[0][i], du[1][i], du[2][i]};
        const CeedScalar du_N           = RatelDot3(dui, platen_normal);
        CeedScalar       dvelocity_T[3] = {0};
        for (CeedInt j = 0; j < 3; j++) {
          dvelocity_T[j] = shift_v * (dui[j] - du_N * platen_normal[j]);
        }
        const CeedScalar velocity_T[3] = {stored_values_platen[1][i], stored_values_platen[2][i], stored_values_platen[3][i]};
        const CeedScalar f1_gamma_n    = stored_values_platen[0][i];

        // Apply friction model
        CeedScalar dfriction_traction[3] = {0};
        FrictionThrelfall_fwd(friction, velocity_T, dvelocity_T, RatelMin(f1_gamma_n, 0), df1_gamma_n[i], dfriction_traction);

        // Add contribution of friction traction linearization
        for (CeedInt j = 0; j < 3; j++) {
          dv[j][i] += dfriction_traction[j] * wdetJb;
        }
      }  // End of Quadrature Point Loop
      break;
    case RATEL_FRICTION_NONE:
      break;
  }
  return CEED_ERROR_SUCCESS;
}

/// @}
