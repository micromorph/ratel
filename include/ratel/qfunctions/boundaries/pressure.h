/// @file
/// Ratel pressure boundary condition QFunction source
#include <ceed/types.h>

#include "../../models/boundary.h"  // IWYU pragma: export
#include "flexible-loading.h"       // IWYU pragma: export

/// @addtogroup RatelBoundary
/// @{

#define FLOPS_Jacobian_PressureBC 30

/**
  @brief Compute the surface integral for pressure on the constrained faces

  @param[in]   ctx  QFunction context, holding `RatelBCPressureParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - gradient of coordinates
                      - 1 - quadrature weights
                      - 2 - gradient of u with respect to reference coordinates
  @param[out]  out  Output array
                      - 0 - `\int v^T . (pn) ds`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(PressureBCs)(void *ctx, const CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*J_face)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[0];
  const CeedScalar(*w)                     = in[1];
  const CeedScalar(*ug)[3][CEED_Q_VLA]     = (const CeedScalar(*)[3][CEED_Q_VLA])in[2];

  // Outputs
  CeedScalar(*v)[CEED_Q_VLA]         = (CeedScalar(*)[CEED_Q_VLA])out[0];
  CeedScalar(*dxdX_save)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[1];

  // User context
  const RatelBCPressureParams *context         = (RatelBCPressureParams *)ctx;
  const CeedInt                prev_time_index = RatelBCInterpGetPreviousKnotIndex(context->time, context->num_times, context->times);
  CeedScalar                   pressure        = 0;
  const CeedInt                dim             = 3;

  RatelBCInterpolate(context->interpolation_type, context->time, prev_time_index, context->num_times, context->times, 1, context->pressure,
                     &pressure);

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // dxdX = dx/dX = dx_initial/dX + du/dX
    const CeedScalar dxdX[3][2] = {
        {J_face[0][0][i] + ug[0][0][i], J_face[1][0][i] + ug[1][0][i]},
        {J_face[0][1][i] + ug[0][1][i], J_face[1][1][i] + ug[1][1][i]},
        {J_face[0][2][i] + ug[0][2][i], J_face[1][2][i] + ug[1][2][i]}
    };

    // normal in current configuration are given by the cross product of the columns of dxdX
    CeedScalar normal[3];

    for (CeedInt j = 0; j < dim; j++) {
      // Equivalent code with no mod operations:
      // normal[j] = dxdX[j+1][0]*dxdX[j+2][1] - dxdX[j+2][0]*dxdX[j+1][1]
      normal[j] = dxdX[(j + 1) % dim][0] * dxdX[(j + 2) % dim][1] - dxdX[(j + 2) % dim][0] * dxdX[(j + 1) % dim][1];
    }

    // Pressure surface integral
    for (CeedInt j = 0; j < 3; j++) {
      v[j][i] = pressure * normal[j] * w[i];
    }
    // Save dxdX for Jacobian
    dxdX_save[0][i] = dxdX[0][0];
    dxdX_save[1][i] = dxdX[1][0];
    dxdX_save[2][i] = dxdX[2][0];
    dxdX_save[3][i] = dxdX[0][1];
    dxdX_save[4][i] = dxdX[1][1];
    dxdX_save[5][i] = dxdX[2][1];
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute the Jacobian of the surface integral for pressure on the constrained faces

  @param[in]   ctx  QFunction context, holding `RatelBCPressureParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - gradient of coordinates
                      - 1 - quadrature weights
                      - 2 - gradient of incremental change in `u`
  @param[out]  out  Output array
                      - 0 - action of QFunction on `dug`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(PressureBCsJacobian)(void *ctx, const CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*dxdX)[CEED_Q_VLA]   = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*w)                  = in[1];
  const CeedScalar(*dug)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[2];

  // Outputs
  CeedScalar(*v)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // User context
  const RatelBCPressureParams *context         = (RatelBCPressureParams *)ctx;
  const CeedInt                prev_time_index = RatelBCInterpGetPreviousKnotIndex(context->time, context->num_times, context->times);
  CeedScalar                   pressure        = 0;
  const CeedInt                dim             = 3;

  RatelBCInterpolate(context->interpolation_type, context->time, prev_time_index, context->num_times, context->times, 1, context->pressure,
                     &pressure);

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Read dxdX
    const CeedScalar temp_dxdX[3][2] = {
        {dxdX[0][i], dxdX[3][i]},
        {dxdX[1][i], dxdX[4][i]},
        {dxdX[2][i], dxdX[5][i]}
    };
    // Read spatial derivatives of increment u; d(du)/dX = d(du)/dX
    const CeedScalar ddudX[3][2] = {
        {dug[0][0][i], dug[1][0][i]},
        {dug[0][1][i], dug[1][1][i]},
        {dug[0][2][i], dug[1][2][i]}
    };

    // Linearization of the normal in current configuration
    CeedScalar dnormal[3][2];
    for (CeedInt j = 0; j < dim; j++) {
      dnormal[j][0] = ddudX[(j + 1) % dim][0] * temp_dxdX[(j + 2) % dim][1] - ddudX[(j + 2) % dim][0] * temp_dxdX[(j + 1) % dim][1];
      dnormal[j][1] = temp_dxdX[(j + 1) % dim][0] * ddudX[(j + 2) % dim][1] - temp_dxdX[(j + 2) % dim][0] * ddudX[(j + 1) % dim][1];
    }

    // Pressure surface integral
    for (CeedInt j = 0; j < 3; j++) {
      v[j][i] = pressure * (dnormal[j][0] + dnormal[j][1]) * w[i];
    }
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
