/// @file
/// Ratel Dirichlet boundary computation CEED vector BPs manufactured solution QFunction source
#include <ceed/types.h>

#include "../mms-solution/ceed-vector-bps.h"  // IWYU pragma: export

/// @addtogroup RatelBoundary
/// @{

/**
  @brief Compute Dirichlet MMS boundary values for CEED vector BPs

  @param[in]   ctx  QFunction context, holding `RatelMMSCEEDBPsParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - stored coordinates
                      - 1 - stored multiplicity scaling factor
  @param[out]  out  Output array
                      - 0 - Dirichlet values

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(MMSBCs_CEED_VectorBPs)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*coords)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*scale)              = (const CeedScalar(*))in[1];

  // Outputs
  CeedScalar(*u)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Setup
    const CeedScalar X[3]             = {coords[0][i], coords[1][i], coords[2][i]};
    CeedScalar       true_solution[3] = {0., 0., 0.};

    RatelVectorBPsMMSTrueSolution(ctx, X, true_solution);
    for (CeedInt j = 0; j < 3; j++) u[j][i] = true_solution[j] * scale[i];
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
