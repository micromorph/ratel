/// @file
/// Ratel friction models QFunction source
#pragma once

#include <ceed/types.h>
#ifndef CEED_RUNNING_JIT_PASS
#include <math.h>
#endif

#include "../../models/friction.h"
#include "../utils.h"

/// @addtogroup RatelBoundary
/// @{

/**
  @brief Compute friction traction using Coulomb model.

  The Coulomb friction model is given by
  \f[
    F = \begin{cases}
      F_C \ \sgn(\bm v_t), & \text{if } \|\bm v_t\| > 0, \\
      \min(F_C, \|\bm \sigma_t\|) \sgn(\bm \sigma_t), & \text{if } \|\bm v_t\| = 0,
    \end{cases}
  \f]
  where \f$ F_C = -\mu_k \sigma_n \f$ is the Coulomb friction force, \f$ \mu_k \f$ is the kinetic friction coefficient,
  \f$ \bm v_t \f$ is the tangential velocity, and \f$ \bm \sigma_t \f$ is the tangential force.
  See `RatelVecSignum()` for the definition of \f$ \sgn(\cdot) \f$.

  The Coulomb model is replaced by the following non-smooth operator for the Nitsche method:
  \f[
    F = -\min(\max(0, -\mu_k (\sigma_n + \gamma g(\bm u)), \|\bm \sigma_t - \gamma \bm v_t\|) \sgn(\bm \sigma_t - \gamma \bm v_t),
  \f]
  where \f$ \gamma \f$ is the penalty parameter, \f$ g(\bm u) \f$ is the gap function, and \f$ \bm u \f$ is the displacement.

  @note If `force_n` is non-negative, set `traction` to zero. Otherwise, clamp magnitude of `traction` to be less than or equal to `-f * force_n`
  @note Uses `RatelFrictionParams::kinetic_coefficient` as \f$ \mu_k \f$.

  @param[in]   friction  Friction parameters
  @param[in]   force_T   Tangential force
  @param[in]   force_n   Normal force, possibly positive if Nitsche method is used
  @param[out]  traction  Computed traction force

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int FrictionCoulomb(const RatelFrictionParams *friction, const CeedScalar force_T[3], const CeedScalar force_n,
                                          CeedScalar traction[3]) {
  // Compute friction traction using Coulomb model
  // If normal force is non-negative, set friction traction to zero
  // Otherwise, clamp magnitude of friction traction to -f * force_n
  const CeedScalar Fc = -friction->kinetic_coefficient * force_n;
  CeedScalar       sgn_force_T[3];
  CeedScalar       norm_force_T;

  RatelVecSignum(force_T, sgn_force_T, &norm_force_T);
  const CeedScalar scale = RatelMax(RatelMin(norm_force_T, Fc), 0.0);

  for (CeedInt j = 0; j < 3; j++) {
    traction[j] = -scale * sgn_force_T[j];
  }
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_FrictionCoulomb (1 + FLOPS_VecSignum + 3 * (2))

/**
  @brief Compute the linearization of the friction traction using Coulomb model.

  @note Uses `RatelFrictionParams::kinetic_coefficient` as \f$ \mu_k \f$.

  @param[in]   friction   Friction parameters
  @param[in]   force_T    Tangential force on contact surface
  @param[in]   dforce_T   Linearization of tangential force on contact surface
  @param[in]   force_n    Normal force on contact surface
  @param[in]   dforce_n   Linearization of normal force on contact surface
  @param[out]  dtraction  Linearization of the computed friction traction

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int FrictionCoulomb_fwd(const RatelFrictionParams *friction, const CeedScalar force_T[3], const CeedScalar dforce_T[3],
                                              const CeedScalar force_n, const CeedScalar dforce_n, CeedScalar dtraction[3]) {
  const CeedScalar Fc  = -friction->kinetic_coefficient * force_n;
  const CeedScalar dFc = -friction->kinetic_coefficient * dforce_n;

  CeedScalar sgn_force_T[3];
  CeedScalar dsgn_force_T[3];
  CeedScalar norm_force_T;

  RatelVecSignum_fwd(force_T, dforce_T, sgn_force_T, dsgn_force_T, &norm_force_T);

  for (CeedInt i = 0; i < 3; i++) {
    // If norm_force_T < Fc, only have to differentiate -||force_T|| * sgn(force_T) = -force_T => dtraction = -dforce_T
    // Otherwise, differentiate -Fc * sgn(force_T) => dtraction = -dFc * sgn(force_T) - Fc * d(sgn(force_T))
    dtraction[i] = (Fc > 0) * (norm_force_T < Fc ? -dforce_T[i] : -(dFc * sgn_force_T[i] + Fc * dsgn_force_T[i]));
  }
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_FrictionCoulomb_fwd (2 + 3 * (5) + FLOPS_VecSignum_fwd)

/**
  @brief Compute friction traction using Threlfall model.

  The Threlfall model is given by
  \f[
    F = \begin{cases}
      F_C \left(\frac{1 - \exp\left(\frac{-3 ||\bm v_t||}{v_0}\right)}{1 - \exp(-3)}\right) \sgn(\bm v_t), & \text{if } ||\bm v_t|| \leq v_0, \\
      F_C \ \sgn(\bm v_t), & \text{otherwise},
    \end{cases}
  \f]
  where \f$ F_C = -\mu_k (\hat\sigma_n) \f$ is the Coulomb friction force, \f$ \bm v_t \f$ is the tangential velocity, \f$ v_0 \f$ is the
  tolerance velocity, and \f$ \mu_k \f$ is the kinetic friction coefficient. See `RatelVecSignum()` for the definition of \f$ \sgn(\cdot) \f$.

  @note Uses `RatelFrictionParams::kinetic_coefficient` as \f$ \mu_k \f$ and `RatelFrictionParams::tolerance_velocity` as \f$ v_0 \f$.

  @param[in]   friction          Friction parameters
  @param[in]   tangent_velocity  Velocity in the tangent plane to the contact surface
  @param[in]   force_n           Normal force, possibly positive if Nitsche method is used
  @param[out]  traction          Computed friction traction force

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int FrictionThrelfall(const RatelFrictionParams *friction, const CeedScalar tangent_velocity[3], const CeedScalar force_n,
                                            CeedScalar traction[3]) {
  // Compute friction traction using Threlfall model
  const CeedScalar Fc = -friction->kinetic_coefficient * force_n;
  const CeedScalar v0 = friction->tolerance_velocity;
  const CeedScalar Fv = friction->viscous_coefficient;
  CeedScalar       sgn_velocity_T[3];
  CeedScalar       norm_velocity_T;

  RatelVecSignum(tangent_velocity, sgn_velocity_T, &norm_velocity_T);

  const CeedScalar threlfall_func = (1 - exp(-3.0 * norm_velocity_T / v0)) / (1 - exp(-3.0));
  const CeedScalar scale          = (Fc > 0.) * ((norm_velocity_T <= v0) ? Fc * threlfall_func : Fc);
  for (CeedInt j = 0; j < 3; j++) {
    traction[j] = scale * sgn_velocity_T[j] + (Fc > 0.) * (norm_velocity_T > v0) * Fv * (norm_velocity_T - v0) * sgn_velocity_T[j];
  }
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_FrictionThrelfall (1 + FLOPS_VecSignum + 7 + 2 + 3 * (7))

/**
  @brief Compute the derivative of the friction traction using Threlfall model.

  @note Uses `RatelFrictionParams::kinetic_coefficient` as \f$ \mu_k \f$ and `RatelFrictionParams::tolerance_velocity` as \f$ v_0 \f$.

  @param[in]   friction     Friction parameters
  @param[in]   velocity_T   Tangential velocity
  @param[in]   dvelocity_T  Linearization of tangential velocity
  @param[in]   force_n      Normal force
  @param[in]   dforce_n     Linearization of normal force
  @param[out]  dtraction    Linearization of the computed friction traction

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int FrictionThrelfall_fwd(const RatelFrictionParams *friction, const CeedScalar velocity_T[3], const CeedScalar dvelocity_T[3],
                                                const CeedScalar force_n, const CeedScalar dforce_n, CeedScalar dtraction[3]) {
  // Compute friction traction linearization using Threlfall model
  const CeedScalar Fc  = -friction->kinetic_coefficient * force_n;
  const CeedScalar dFc = -friction->kinetic_coefficient * dforce_n;
  const CeedScalar Fv  = friction->viscous_coefficient;
  const CeedScalar v0  = friction->tolerance_velocity;

  CeedScalar sgn_velocity_T[3];
  CeedScalar dsgn_velocity_T[3];
  CeedScalar norm_velocity_T;
  RatelVecSignum_fwd(velocity_T, dvelocity_T, sgn_velocity_T, dsgn_velocity_T, &norm_velocity_T);

  // d(||x||)/dx = x⋅dx / ||x|| = sgn(x)⋅dx
  const CeedScalar dnorm_velocity_T = RatelDot3(sgn_velocity_T, dvelocity_T);
  const CeedScalar threlfall_func   = (1 - exp(-3.0 * norm_velocity_T / v0)) / (1 - exp(-3.0));
  const CeedScalar dthrelfall_func  = (3.0 * dnorm_velocity_T / v0) * exp(-3.0 * norm_velocity_T / v0) / (1 - exp(-3.0));

  for (CeedInt i = 0; i < 3; i++) {
    // If norm_velocity_T <= v0, have to differentiate Fc * threlfall_func * sgn_velocity_T
    // Otherwise, only have to differentiate Fc * sgn(velocity_T):
    const CeedScalar d_Fc_sgn_v_T_i = Fc * dsgn_velocity_T[i] + dFc * sgn_velocity_T[i];

    dtraction[i] = (Fc > 0.) * ((norm_velocity_T <= v0) ? d_Fc_sgn_v_T_i * threlfall_func + Fc * sgn_velocity_T[i] * dthrelfall_func
                                                        : d_Fc_sgn_v_T_i + Fv * (dvelocity_T[i] - v0 * dsgn_velocity_T[i]));
  }
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_FrictionThrelfall_fwd (2 + FLOPS_VecSignum_fwd + FLOPS_Dot3 + 7 + 9 + 3 * (3 + 4))

/// @}
