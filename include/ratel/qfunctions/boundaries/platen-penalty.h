/// @file
/// Ratel penalty method platen boundary condition QFunction source
#include <ceed/types.h>

#include "../../models/platen.h"          // IWYU pragma: export
#include "../models/elasticity-common.h"  // IWYU pragma: export
#include "../utils.h"                     // IWYU pragma: export
#include "friction.h"                     // IWYU pragma: export

/// @addtogroup RatelBoundary
/// @{

#define FLOPS_Jacobian_PlatenPenaltyBC (3 * FLOPS_Dot3 + FLOPS_Norm3 + 57)

/**
  @brief Compute geometric factors for integration, gradient transformations, and coordinate transformations on element faces.

  Reference (parent) 2D coordinates are given by `X` and physical (current) 3D coordinates are given by `x`.
  The change of coordinate matrix is given by`dxdX_{i,j} = dx_i/dX_j (indicial notation) [3 * 2]`.

  `(N_1, N_2, N_3)` is given by the cross product of the columns of `dxdX_{i,j}`.

  `detNb` is the magnitude of `(N_1, N_2, N_3)`.

  @param[in]   ctx  QFunction context, unused
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - Jacobian of face coordinates
                      - 1 - quadrature weights
  @param[out]  out  Output array
                      - 0 - qdata, `w`, `detNb`, and `N`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(SetupPenaltyPlatens)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*J)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[0];
  const CeedScalar(*w)                = in[1];

  // Outputs
  CeedScalar(*q_data)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  const CeedInt dim = 3;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Setup
    // N_1, N_2, and N_3 are given by the cross product of the columns of dxdX
    CeedScalar normal[3];

    for (CeedInt j = 0; j < dim; j++) {
      // Equivalent code with no mod operations:
      // normal[j] = J[0][j+1]*J[1][j+2] - J[0][j+2]*J[1][j+1]
      normal[j] = J[0][(j + 1) % dim][i] * J[1][(j + 2) % dim][i] - J[0][(j + 2) % dim][i] * J[1][(j + 1) % dim][i];
    }
    const CeedScalar detJb = RatelNorm3(normal);

    // Qdata
    // -- Interp-to-Interp q_data
    q_data[0][i] = w[i];
    q_data[1][i] = detJb;
    // -- Normal vector
    for (CeedInt j = 0; j < dim; j++) q_data[j + 2][i] = normal[j] / detJb;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute the surface integral for penalty method based contact on the constrained faces

  In the frictionless case, compute
  ```
  (gap < 0) ? (w detNb) * gap / epsilon * N : 0
  ```
  where `epsilon = sqrt(detNb) / platen->gamma` is the penalty parameter

  @param[in]   ctx  QFunction context, holding `RatelBCPlatenParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - quadrature data
                      - 1 - `u`
                      - 2 - `du/dt` (or `u` in static case)
                      - 3 - quadrature point coordinates
  @param[out]  out  Output array
                      - 0 - `v`
                      - 1 - stored values from residual

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(PlatenPenaltyBCs)(void *ctx, const CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[1];
  const CeedScalar(*u_dot)[CEED_Q_VLA]  = (const CeedScalar(*)[CEED_Q_VLA])in[2];
  const CeedScalar(*x)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[3];

  // Outputs
  CeedScalar(*v)[CEED_Q_VLA]                    = (CeedScalar(*)[CEED_Q_VLA])out[0];
  CeedScalar(*stored_values_platen)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[1];

  // User context
  const RatelBCPlatenParams       *context             = (RatelBCPlatenParams *)ctx;
  const RatelBCPlatenParamsCommon *platen              = &context->platen;
  const RatelFrictionParams       *friction            = &platen->friction;
  const CeedScalar                 viscous_coefficient = friction->viscous_coefficient;
  const CeedScalar                 gamma               = platen->gamma;
  const CeedScalar                 platen_normal[3]    = {platen->normal[0], platen->normal[1], platen->normal[2]};

  // Compute current center
  const CeedInt prev_time_index = RatelBCInterpGetPreviousKnotIndex(context->time, platen->num_times, platen->times);
  CeedScalar    distance        = 0;
  RatelBCInterpolate(platen->interpolation_type, context->time, prev_time_index, platen->num_times, platen->times, 1, platen->distance, &distance);
  CeedScalar current_center[3];
  for (CeedInt j = 0; j < 3; j++) {
    current_center[j] = platen->center[j] + distance * platen_normal[j];
  }

  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    const CeedScalar w     = q_data[0][i];
    const CeedScalar detJb = q_data[1][i];

    /*\epsilon ~ h = sqrt(detJb)*/
    const CeedScalar epsilon = fabs(sqrt(detJb)) / gamma;

    /* Compute overlap in current configuration */
    const CeedScalar ui[3]          = {u[0][i], u[1][i], u[2][i]};
    const CeedScalar overlap_vec[3] = {
        ui[0] + x[0][i] - current_center[0],
        ui[1] + x[1][i] - current_center[1],
        ui[2] + x[2][i] - current_center[2],
    }; /* And compute gap as magnitude of projection of above vector onto face normal */
    const CeedScalar gap = RatelDot3(overlap_vec, platen_normal);

    const CeedScalar penalty = gap / epsilon;

    stored_values_platen[0][i] = penalty;
    stored_values_platen[1][i] = gap;
    stored_values_platen[2][i] = epsilon;

    /* Normal traction component */
    for (CeedInt j = 0; j < 3; j++) {
      v[j][i] = (penalty < 0) * (w * detJb * penalty * platen_normal[j]);
    }
  }

  switch (friction->model) {
    case RATEL_FRICTION_COULOMB:
      // Quadrature Point Loop
      CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
        // QData
        const CeedScalar w       = q_data[0][i];
        const CeedScalar detJb   = q_data[1][i];
        const CeedScalar gap     = stored_values_platen[1][i];
        const CeedScalar epsilon = stored_values_platen[2][i];

        // Compute tangent velocity
        CeedScalar       velocity_T[3] = {0};
        const CeedScalar velocity[3]   = {u_dot[0][i], u_dot[1][i], u_dot[2][i]};
        const CeedScalar u_dot_n       = RatelDot3(velocity, platen_normal);
        for (CeedInt j = 0; j < 3; j++) {
          velocity_T[j] = velocity[j] - u_dot_n * platen_normal[j];
        }

        CeedScalar f1_T[3] = {-velocity_T[0], -velocity_T[1], -velocity_T[2]};
        for (CeedInt j = 0; j < 3; j++) {
          stored_values_platen[j + 1][i] = f1_T[j];
        }

        // Apply friction model
        CeedScalar friction_traction[3] = {0.};
        FrictionCoulomb(friction, f1_T, RatelMin(gap, 0), friction_traction);

        // Add viscous term
        for (CeedInt j = 0; j < 3; j++) {
          v[j][i] += (friction_traction[j] + (gap < 0) * viscous_coefficient * velocity_T[j]) * w * detJb / epsilon;
        }
      }  // End of Quadrature Point Loop
      break;
    case RATEL_FRICTION_THRELFALL:
      // Quadrature Point Loop
      CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
        // QData
        const CeedScalar w       = q_data[0][i];
        const CeedScalar detJb   = q_data[1][i];
        const CeedScalar gap     = stored_values_platen[1][i];
        const CeedScalar epsilon = stored_values_platen[2][i];

        // Compute tangent velocity
        CeedScalar       velocity_T[3] = {0};
        const CeedScalar velocity[3]   = {u_dot[0][i], u_dot[1][i], u_dot[2][i]};
        const CeedScalar u_dot_n       = RatelDot3(velocity, platen_normal);
        for (CeedInt j = 0; j < 3; j++) {
          stored_values_platen[j + 1][i] = velocity_T[j] = velocity[j] - u_dot_n * platen_normal[j];
        }

        // Apply friction model
        CeedScalar friction_traction[3] = {0.};
        FrictionThrelfall(friction, velocity_T, RatelMin(gap, 0), friction_traction);

        // Add to output
        for (CeedInt j = 0; j < 3; j++) {
          v[j][i] += friction_traction[j] * w * detJb / epsilon;
        }
      }  // End of Quadrature Point Loop
      break;
    case RATEL_FRICTION_NONE:
      // Quadrature Point Loop
      CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
        for (CeedInt j = 1; j < 4; j++) {
          stored_values_platen[j][i] = 0;
        }
      }  // End of Quadrature Point Loop
      break;
  }
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute the Jacobian of the surface integral for penalty method based contact on the constrained faces

  @param[in]   ctx  QFunction context, holding `RatelBCPlatenParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - quadrature data
                      - 1 - incremental change in `u`
                      - 2 - stored values from residual
  @param[out]  out  Output array
                      - 0 - action of QFunction on `du`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(PlatenPenaltyBCsJacobian)(void *ctx, const CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA]               = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*du)[CEED_Q_VLA]                   = (const CeedScalar(*)[CEED_Q_VLA])in[1];
  const CeedScalar(*stored_values_platen)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[2];

  // Outputs
  CeedScalar(*dv)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // User context
  const RatelBCPlatenParams       *context             = (RatelBCPlatenParams *)ctx;
  const RatelBCPlatenParamsCommon *platen              = &context->platen;
  const CeedScalar                *material_context    = (CeedScalar *)&context->material;
  const RatelFrictionParams       *friction            = &platen->friction;
  const CeedScalar                 viscous_coefficient = friction->viscous_coefficient;
  const CeedScalar                 gamma               = platen->gamma;
  const CeedScalar                 platen_normal[3]    = {platen->normal[0], platen->normal[1], platen->normal[2]};
  // If shift_v is not set, we're running in static mode, so we used u instead of u_t in the traction calculation
  const CeedScalar shift_v = material_context[RATEL_COMMON_PARAMETER_SHIFT_V] != 0 ? material_context[RATEL_COMMON_PARAMETER_SHIFT_V] : 1;

  switch (friction->model) {
    case RATEL_FRICTION_COULOMB:
      // Quadrature Point Loop
      CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
        // Stored platen values
        const CeedScalar penalty = stored_values_platen[0][i];

        // Qdata
        const CeedScalar w       = q_data[0][i];
        const CeedScalar detJb   = q_data[1][i];
        const CeedScalar epsilon = fabs(sqrt(detJb)) / gamma;

        // Compute du_N = du . n_platen
        const CeedScalar dui[3] = {du[0][i], du[1][i], du[2][i]};
        const CeedScalar du_N   = RatelDot3(dui, platen_normal);

        // Compute linearization of boundary traction
        const CeedScalar dpenalty = (du_N / epsilon);

        // Normal traction linearization
        for (CeedInt j = 0; j < 3; j++) {
          // only evaluate where f1_gamma was negative, otherwise uniformly zero
          dv[j][i] = (penalty < 0) * w * detJb * dpenalty * platen_normal[j];
        }

        // Compute tangent dvelocity
        CeedScalar dvelocity_T[3] = {0};
        for (CeedInt j = 0; j < 3; j++) {
          dvelocity_T[j] = shift_v * (dui[j] - du_N * platen_normal[j]);
        }

        // Normal force
        const CeedScalar penalized_f_normal  = penalty * epsilon;
        const CeedScalar dpenalized_f_normal = dpenalty * epsilon;

        // Friction traction linearization
        CeedScalar dfriction_traction[3] = {0};
        // Coulomb friction model expects negative of tangential velocity
        const CeedScalar f1_T[3]         = {stored_values_platen[1][i], stored_values_platen[2][i], stored_values_platen[3][i]};
        const CeedScalar df1_T[3]        = {-dvelocity_T[0], -dvelocity_T[1], -dvelocity_T[2]};

        // Apply friction model
        FrictionCoulomb_fwd(friction, f1_T, df1_T, RatelMin(penalized_f_normal, 0), dpenalized_f_normal, dfriction_traction);
        // Add viscous term
        for (CeedInt j = 0; j < 3; j++) {
          dv[j][i] += (dfriction_traction[j] + (penalty < 0) * viscous_coefficient * dvelocity_T[j]) * w * detJb / epsilon;
        }
      }  // End of Quadrature Point Loop
      break;
    case RATEL_FRICTION_THRELFALL:
      // Quadrature Point Loop
      CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
        // Stored platen values
        const CeedScalar penalty = stored_values_platen[0][i];

        // Qdata
        const CeedScalar w       = q_data[0][i];
        const CeedScalar detJb   = q_data[1][i];
        const CeedScalar epsilon = fabs(sqrt(detJb)) / gamma;

        // Compute du_N = du . n_platen
        const CeedScalar dui[3] = {du[0][i], du[1][i], du[2][i]};
        const CeedScalar du_N   = RatelDot3(dui, platen_normal);

        // Compute linearization of boundary traction
        const CeedScalar dpenalty = (du_N / epsilon);

        // Normal traction linearization
        for (CeedInt j = 0; j < 3; j++) {
          // only evaluate where f1_gamma was negative, otherwise uniformly zero
          dv[j][i] = (penalty < 0) * w * detJb * dpenalty * platen_normal[j];
        }

        // Compute tangent dvelocity
        const CeedScalar velocity_T[3]  = {stored_values_platen[1][i], stored_values_platen[2][i], stored_values_platen[3][i]};
        CeedScalar       dvelocity_T[3] = {0};
        for (CeedInt j = 0; j < 3; j++) {
          dvelocity_T[j] = shift_v * (dui[j] - du_N * platen_normal[j]);
        }

        // Normal force
        const CeedScalar penalized_f_normal  = penalty * epsilon;
        const CeedScalar dpenalized_f_normal = dpenalty * epsilon;

        // Friction traction linearization
        CeedScalar dfriction_traction[3] = {0};

        // Apply friction model
        FrictionThrelfall_fwd(friction, velocity_T, dvelocity_T, RatelMin(penalized_f_normal, 0), dpenalized_f_normal, dfriction_traction);
        for (CeedInt j = 0; j < 3; j++) {
          dv[j][i] += dfriction_traction[j] * w * detJb / epsilon;
        }
      }  // End of Quadrature Point Loop
      break;
    case RATEL_FRICTION_NONE:
      // Quadrature Point Loop
      CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
        // Stored platen values
        const CeedScalar penalty = stored_values_platen[0][i];

        // Qdata
        const CeedScalar w       = q_data[0][i];
        const CeedScalar detJb   = q_data[1][i];
        const CeedScalar epsilon = fabs(sqrt(detJb)) / gamma;

        // Compute du_N = du . n_platen
        const CeedScalar dui[3] = {du[0][i], du[1][i], du[2][i]};
        const CeedScalar du_N   = RatelDot3(dui, platen_normal);

        // Compute linearization of boundary traction
        const CeedScalar dpenalty = (du_N / epsilon);

        // Normal traction linearization
        for (CeedInt j = 0; j < 3; j++) {
          // only evaluate where f1_gamma was negative, otherwise uniformly zero
          dv[j][i] = (penalty < 0) * w * detJb * dpenalty * platen_normal[j];
        }
      }  // End of Quadrature Point Loop
      break;
  }
  return CEED_ERROR_SUCCESS;
}

/// @}
