/// @file
/// Ratel utility helpers QFunction source for CPP
#pragma once

#include <ceed/types.h>

#include "utils-common.h"

#define RATEL_VLA_PARAM3(name, d1, d2, d3) CeedScalar name##_vla[]
#define RATEL_VLA_VAR3(name, d1, d2, d3) \
  typedef CeedScalar(*name##_t)[d2][d3]; \
  name##_t name = (name##_t)name##_vla
#define RATEL_VLA_PARAM2(name, d1, d2) CeedScalar name##_vla[]
#define RATEL_VLA_VAR2(name, d1, d2) \
  typedef CeedScalar(*name##_t)[d2]; \
  name##_t name = (name##_t)name##_vla
#define RATEL_VLA_PARAM3_CONST(name, d1, d2, d3) const CeedScalar name##_vla[]
#define RATEL_VLA_VAR3_CONST(name, d1, d2, d3) \
  typedef const CeedScalar(*name##_t)[d2][d3]; \
  name##_t name = (name##_t)name##_vla
#define RATEL_VLA_PARAM2_CONST(name, d1, d2) const CeedScalar name##_vla[]
#define RATEL_VLA_VAR2_CONST(name, d1, d2) \
  typedef const CeedScalar(*name##_t)[d2]; \
  name##_t name = (name##_t)name##_vla
#define RATEL_VLA_PASS2(name) name[0]
#define RATEL_VLA_PASS3(name) name[0][0]

CEED_QFUNCTION_HELPER int RatelQdataUnpack_cpp(CeedInt Q, CeedInt i, RATEL_VLA_PARAM2_CONST(stored, 10, CEED_Q_VLA), CeedScalar local[3][3]) {
  RATEL_VLA_VAR2_CONST(stored, 10, CEED_Q_VLA);
  local[0][0] = stored[1][i];
  local[0][1] = stored[2][i];
  local[0][2] = stored[3][i];
  local[1][0] = stored[4][i];
  local[1][1] = stored[5][i];
  local[1][2] = stored[6][i];
  local[2][0] = stored[7][i];
  local[2][1] = stored[8][i];
  local[2][2] = stored[9][i];
  return CEED_ERROR_SUCCESS;
}

CEED_QFUNCTION_HELPER int RatelGradUnpack_cpp(CeedInt Q, CeedInt i, RATEL_VLA_PARAM3_CONST(grad, 3, 3, CEED_Q_VLA), CeedScalar local[3][3]) {
  RATEL_VLA_VAR3_CONST(grad, 3, 3, CEED_Q_VLA);
  local[0][0] = grad[0][0][i];
  local[0][1] = grad[1][0][i];
  local[0][2] = grad[2][0][i];
  local[1][0] = grad[0][1][i];
  local[1][1] = grad[1][1][i];
  local[1][2] = grad[2][1][i];
  local[2][0] = grad[0][2][i];
  local[2][1] = grad[1][2][i];
  local[2][2] = grad[2][2][i];
  return CEED_ERROR_SUCCESS;
}

CEED_QFUNCTION_HELPER int RatelMatMatTransposeMultAtQuadraturePoint_cpp(CeedInt Q, CeedInt i, CeedScalar alpha, const CeedScalar A[3][3],
                                                                        const CeedScalar B[3][3], RATEL_VLA_PARAM3(C, 3, 3, CEED_Q_VLA)) {
  RATEL_VLA_VAR3(C, 3, 3, CEED_Q_VLA);
  for (CeedInt j = 0; j < 3; j++) {
    for (CeedInt k = 0; k < 3; k++) {
      C[j][k][i] = 0;
      for (CeedInt m = 0; m < 3; m++) {
        C[j][k][i] += alpha * A[j][m] * B[k][m];
      }
    }
  }
  return CEED_ERROR_SUCCESS;
}
