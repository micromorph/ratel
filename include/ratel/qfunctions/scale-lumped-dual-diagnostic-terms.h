/// @file
/// Ratel compute lumped diagnostics operator QFunction source
#include <ceed/types.h>

#include "utils.h"  // IWYU pragma: export

/// @addtogroup RatelInternal
/// @{

/**
  @brief Apply inverse of lumped mass operator to nodal values

  @param[in]   ctx  QFunction context, holding the number of components
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - element restriction multiplicity
                      - 1 - composite operator nodal multiplicity
                      - 2 - dual diagnostics: nodal volume, other values
  @param[out]  out  Output array
                      - 0 - lumped dual diagnostics: nodal volume, other values divided by nodal volume

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ScaleLumpedDualDiagnosticTerms)(void *ctx, const CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*multiplicity)[CEED_Q_VLA]       = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*nodal_multiplicity)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[1];
  const CeedScalar(*u)[CEED_Q_VLA]                  = (const CeedScalar(*)[CEED_Q_VLA])in[2];

  // Outputs
  CeedScalar(*v)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  const CeedInt num_comp = *(CeedInt *)ctx;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Compute multiplicity scaling factor
    CeedScalar scale = 1.0 / (nodal_multiplicity[0][i] * multiplicity[0][i]);

    // Copy nodal volume
    v[0][i] = scale * u[0][i];
    // Pointwise divide by nodal volume
    for (CeedInt j = 1; j < num_comp; j++) {
      v[j][i] = scale * u[j][i] / u[0][i];
    }
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
