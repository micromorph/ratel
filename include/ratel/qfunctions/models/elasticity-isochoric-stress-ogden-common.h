/// @file
/// Ratel isochoric stress Ogden for mixed/single fields hyperelasticity at finite strain common QFunction source
#pragma once

#include <ceed/types.h>
#ifndef CEED_RUNNING_JIT_PASS
#include <math.h>
#endif

#include "../../models/ogden.h"  // IWYU pragma: export
#include "../utils.h"            // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

#define FLOPS_Tau_iso_Ogden (9 + FLOPS_EigenVectorOuterMult + 36)
#define FLOPS_S_iso_Ogden                                                                                                   \
  (FLOPS_MatComputeEigensystemSymmetric + FLOPS_PrincipalStretch + 3 * FLOPS_Log1pSeries + 6 + 9 * FLOPS_Expm1Series + 42 + \
   FLOPS_EigenVectorOuterMult + 36)
#define FLOPS_dS_iso_Ogden                                                                                                                       \
  (FLOPS_GreenLagrangeStrain_fwd + FLOPS_CInverse_fwd + FLOPS_MatMatContractSymmetric + FLOPS_MatComputeEigensystemSymmetric +                   \
   FLOPS_PrincipalStretch + FLOPS_PrincipalStretch_fwd + 3 * FLOPS_Log1pSeries + 9 + 18 * FLOPS_Expm1Series + 142 + FLOPS_EigenVectorOuterMult + \
   FLOPS_EigenVectorOuterMult_fwd + 105)
#define FLOPS_FdSFTranspose_iso_Ogden                                                                                                      \
  (6 + FLOPS_MatTrace + 2 * FLOPS_MatMatMult + 12 + FLOPS_GreenEulerStrain_fwd + FLOPS_PrincipalStretch + FLOPS_PrincipalStretch_fwd +     \
   3 * FLOPS_Log1pSeries + 9 + FLOPS_EigenVectorOuterMult + FLOPS_EigenVectorOuterMult_fwd + +6 * FLOPS_MatMatMult + 3 * FLOPS_MatMatAdd + \
   3 * FLOPS_MatMatMatAddSymmetric + 21 + 57 + 126)

/**
  @brief Compute isochoric part of Kirchoff tau for Ogden hyperelasticity.

  @param[in]   N             Number of Ogden parameters
  @param[in]   m             Array of first Ogden material model
  @param[in]   alpha         Array of second Ogden material model
  @param[in]   series_terms  Series terms
  @param[in]   e_vals        Eigenvalues of Green Euler strain tensor
  @param[in]   e_vecs        Eigenvectors of Green Euler strain tensor
  @param[out]  tau_iso_sym   Isochoric Kirchoff tau, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelIsochoricKirchhoffTau_Ogden(CeedInt N, const CeedScalar *m, const CeedScalar *alpha, const CeedScalar series_terms[9],
                                                           const CeedScalar e_vals[3], const CeedScalar e_vecs[3][3], CeedScalar tau_iso_sym[6]) {
  CeedScalar s[3] = {0., 0., 0.}, e_vecs_product[3][6];

  // s[i] = sum_{j=0:N} m[j]/3 [...] J^{-alpha[j]/3}
  for (CeedInt i = 0; i < N; i++) {
    s[0] += series_terms[i];
    s[1] += series_terms[i + 3];
    s[2] += series_terms[i + 6];
  }

  // tau_iso = sum_{i=0:3} (s[i]) n[i] n[i]^T
  RatelEigenVectorOuterMult(e_vals, e_vecs, e_vecs_product);  // n[i] n[i]^T
  for (CeedInt k = 0; k < 6; k++) tau_iso_sym[k] = 0.;
  for (CeedInt i = 0; i < 3; i++) {
    for (CeedInt k = 0; k < 6; k++) {
      tau_iso_sym[k] += s[i] * e_vecs_product[i][k];
    }
  }
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute isochoric part of second Kirchoff stress for Ogden hyperelasticity.

  @param[in]   N          Number of Ogden parameters
  @param[in]   m          Array of first Ogden material model
  @param[in]   alpha      Array of second Ogden material model
  @param[in]   J          Determinant of deformation gradient
  @param[in]   C_inv_sym  Inverse of right Cauchy-Green tensor, in symmetric representation
  @param[in]   E_sym      Green Lagrange strain, in symmetric representation
  @param[out]  S_iso_sym  Isochoric second Kirchoff stress, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelIsochoricSecondKirchhoffStress_Ogden(CeedInt N, const CeedScalar *m, const CeedScalar *alpha, CeedScalar J,
                                                                    const CeedScalar C_inv_sym[6], const CeedScalar E_sym[6],
                                                                    CeedScalar S_iso_sym[6]) {
  CeedScalar pr_str[3], e_vals[3], e_vecs[3][3], l[3], s[3] = {0., 0., 0.}, e_vecs_product[3][6];

  RatelMatComputeEigensystemSymmetric(E_sym, e_vals, e_vecs);
  // Compute principal stretch from strain's eigenvalues
  RatelPrincipalStretch(e_vals, pr_str);
  // Compute l[i] = log(pr_str[i]) = 0.5 * log1p(2*e_vals[i])
  for (CeedInt i = 0; i < 3; i++) l[i] = 0.5 * RatelLog1pSeries(2 * e_vals[i]);

  // s[i] = sum_{j=0:N} m[j]/3 [...] J^{-alpha[j]/3}
  for (CeedInt j = 0; j < N; j++) {
    CeedScalar J_pow_alpha_m = pow(J, -alpha[j] / 3.) * (m[j] / 3.);
    CeedScalar expm1_term_0 = RatelExpm1Series(alpha[j] * l[0]), expm1_term_1 = RatelExpm1Series(alpha[j] * l[1]),
               expm1_term_2 = RatelExpm1Series(alpha[j] * l[2]);

    s[0] += J_pow_alpha_m * (2 * expm1_term_0 - expm1_term_1 - expm1_term_2);
    s[1] += J_pow_alpha_m * (-expm1_term_0 + 2 * expm1_term_1 - expm1_term_2);
    s[2] += J_pow_alpha_m * (-expm1_term_0 - expm1_term_1 + 2 * expm1_term_2);
  }

  // S_iso = sum_{i=0:3} (s_i) N[i] N[i]^T
  RatelEigenVectorOuterMult(e_vals, e_vecs, e_vecs_product);  // N[i] N[i]^T
  for (CeedInt k = 0; k < 6; k++) S_iso_sym[k] = 0.;
  for (CeedInt i = 0; i < 3; i++) {
    s[i] /= pr_str[i] * pr_str[i];
    for (CeedInt k = 0; k < 6; k++) {
      S_iso_sym[k] += s[i] * e_vecs_product[i][k];
    }
  }
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute derivative of isochoric second Kirchoff stress for Ogden hyperelasticity.

  @param[in]   N                 Number of Ogden parameters
  @param[in]   m                 Array of first Ogden material model
  @param[in]   alpha             Array of second Ogden material model
  @param[in]   J                 Determinant of deformation gradient
  @param[in]   F                 Deformation gradient
  @param[in]   grad_du           Gradient of incremental change in u
  @param[in]   C_inv_sym         Inverse of right Cauchy-Green tensor, in symmetric representation
  @param[in]   E_sym             Green Lagrange strain, in symmetric representation
  @param[out]  dC_inv_sym        Derivative of C^{-1}, in symmetric representation
  @param[out]  dS_iso_sym        Derivative of isochoric second Kirchoff stress, in symmetric representation
  @param[out]  Cinv_contract_dE  `C_inv : dE`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelIsochoricSecondKirchhoffStress_Ogden_fwd(CeedInt N, const CeedScalar *m, const CeedScalar *alpha, CeedScalar J,
                                                                        const CeedScalar F[3][3], const CeedScalar grad_du[3][3],
                                                                        const CeedScalar C_inv_sym[6], const CeedScalar E_sym[6],
                                                                        CeedScalar dC_inv_sym[6], CeedScalar dS_iso_sym[6],
                                                                        CeedScalar *Cinv_contract_dE) {
  CeedScalar dE_sym[6], e_vals[3], e_vecs[3][3], pr_str[3], dpr_str[3], l[3], dl[3], e_vecs_product[3][6], e_vecs_product_fwd[3][6];

  // dE - Green-Lagrange strain tensor increment
  RatelGreenLagrangeStrain_fwd(grad_du, F, dE_sym);
  // dC_inv = -2*C_inv*dE*C_inv
  RatelCInverse_fwd(C_inv_sym, dE_sym, dC_inv_sym);
  // C_inv:dE
  *Cinv_contract_dE = RatelMatMatContractSymmetric(1.0, C_inv_sym, dE_sym);

  // 1) S_iso = sum_{i=0:3} (s[i]) N[i] N[i]^T
  // dS_iso = sum_{i=0:3} ds[i] N[i] N[i]^T + s[i] (dN[i] N[i]^T + N[i] dN[i]^T)
  RatelMatComputeEigensystemSymmetric(E_sym, e_vals, e_vecs);
  RatelPrincipalStretch(e_vals, pr_str);
  RatelPrincipalStretch_fwd(dE_sym, e_vecs, pr_str, dpr_str);
  // Compute l[i] = log(pr_str[i]) = 0.5 * log1p(2*e_vals[i]) and dl[i]
  for (CeedInt i = 0; i < 3; i++) {
    l[i]  = 0.5 * RatelLog1pSeries(2 * e_vals[i]);
    dl[i] = dpr_str[i] / pr_str[i];
  }

  CeedScalar s[3] = {0., 0., 0.}, s1[3] = {0., 0., 0.}, ds[3] = {0., 0., 0.}, ds1[3] = {0., 0., 0.};
  for (CeedInt j = 0; j < N; j++) {
    CeedScalar J_pow_alpha_m = pow(J, -alpha[j] / 3.) * (m[j] / 3.);
    CeedScalar expm1_term_0 = RatelExpm1Series(alpha[j] * l[0]), expm1_term_1 = RatelExpm1Series(alpha[j] * l[1]),
               expm1_term_2        = RatelExpm1Series(alpha[j] * l[2]);
    CeedScalar J_pow_alpha_m_alpha = J_pow_alpha_m * alpha[j];
    CeedScalar exp_term_0 = exp(alpha[j] * l[0]), exp_term_1 = exp(alpha[j] * l[1]), exp_term_2 = exp(alpha[j] * l[2]);

    s[0] += J_pow_alpha_m * (2 * expm1_term_0 - expm1_term_1 - expm1_term_2);
    s[1] += J_pow_alpha_m * (-expm1_term_0 + 2 * expm1_term_1 - expm1_term_2);
    s[2] += J_pow_alpha_m * (-expm1_term_0 - expm1_term_1 + 2 * expm1_term_2);

    ds1[0] += J_pow_alpha_m_alpha * (2 * dl[0] * exp_term_0 - dl[1] * exp_term_1 - dl[2] * exp_term_2);
    ds1[1] += J_pow_alpha_m_alpha * (-dl[0] * exp_term_0 + 2 * dl[1] * exp_term_1 - dl[2] * exp_term_2);
    ds1[2] += J_pow_alpha_m_alpha * (-dl[0] * exp_term_0 - dl[1] * exp_term_1 + 2 * dl[2] * exp_term_2);

    s1[0] += (J_pow_alpha_m_alpha / 3.) * (2 * expm1_term_0 - expm1_term_1 - expm1_term_2);
    s1[1] += (J_pow_alpha_m_alpha / 3.) * (-expm1_term_0 + 2 * expm1_term_1 - expm1_term_2);
    s1[2] += (J_pow_alpha_m_alpha / 3.) * (-expm1_term_0 - expm1_term_1 + 2 * expm1_term_2);
  }

  // dS_iso = sum_{i=0:3} ds[i] N[i] N[i]^T + s[i] (dN[i] N[i]^T + N[i] dN[i]^T)
  RatelEigenVectorOuterMult(e_vals, e_vecs, e_vecs_product);                  // N[i] N[i]^T
  RatelEigenVectorOuterMult_fwd(dE_sym, e_vals, e_vecs, e_vecs_product_fwd);  // dN[i] N[i]^T + N[i] dN[i]^T
  for (CeedInt k = 0; k < 6; k++) dS_iso_sym[k] = 0.;
  for (CeedInt i = 0; i < 3; i++) {
    s[i] /= (pr_str[i] * pr_str[i]);
    ds[i] = -2 * dl[i] * s[i];
    ds[i] += ds1[i] / (pr_str[i] * pr_str[i]);
    ds[i] += -(*Cinv_contract_dE) * s1[i] / (pr_str[i] * pr_str[i]);
    for (CeedInt k = 0; k < 6; k++) {
      dS_iso_sym[k] += ds[i] * e_vecs_product[i][k];
      dS_iso_sym[k] += s[i] * e_vecs_product_fwd[i][k];
    }
  }
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute F*dS_iso*F^T for Ogden hyperelasticity in current configuration.

  @param[in]   N                      Number of Ogden parameters
  @param[in]   m                      Array of first Ogden material model
  @param[in]   alpha                  Array of second Ogden material model
  @param[in]   J_pow_alpha            Array of J^{-alpha[j]/3}, j=1..3
  @param[in]   series_terms           Series terms
  @param[in]   grad_du                Gradient of incremental change in u
  @param[in]   e_vals                 Eigenvalues of Green Euler strain tensor
  @param[in]   e_vecs                 Eigenvectors of Green Euler strain tensor
  @param[out]  depsilon_sym           depsilon = (grad_du + grad_du^T)/2
  @param[out]  FdSFTranspose_iso_sym  F*dS_iso*F^T needed for computing df1 in current configuration
  @param[out]  trace_depsilon         `trace(depsilon)`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelComputeFdSFTransposeIsochoric_Ogden(CeedInt N, const CeedScalar *m, const CeedScalar *alpha,
                                                                   const CeedScalar J_pow_alpha[3], const CeedScalar series_terms[9],
                                                                   const CeedScalar grad_du[3][3], const CeedScalar e_vals[3],
                                                                   const CeedScalar e_vecs[3][3], CeedScalar depsilon_sym[6],
                                                                   CeedScalar FdSFTranspose_iso_sym[6], CeedScalar *trace_depsilon) {
  CeedScalar e[3][3], e_sym[6], B_e_vecs[3][3], b_sym[6], b[3][3], de_sym[6], pr_str[3], dpr_str[3], l[3], dl[3], e_vecs_product[3][6],
      e_vecs_product_fwd[3][6], A[3][6];
  // Compute depsilon = (grad_du + grad_du^T)/2
  depsilon_sym[0] = grad_du[0][0];
  depsilon_sym[1] = grad_du[1][1];
  depsilon_sym[2] = grad_du[2][2];
  depsilon_sym[3] = (grad_du[1][2] + grad_du[2][1]) / 2.;
  depsilon_sym[4] = (grad_du[0][2] + grad_du[2][0]) / 2.;
  depsilon_sym[5] = (grad_du[0][1] + grad_du[1][0]) / 2.;

  *trace_depsilon = RatelMatTraceSymmetric(depsilon_sym);

  CeedScalar B[3][3] = {
      {e_vals[0], 0.0,       0.0      },
      {0.0,       e_vals[1], 0.0      },
      {0.0,       0.0,       e_vals[2]}
  };
  RatelMatTransposeMatMult(1.0, e_vecs, B, B_e_vecs);
  RatelMatMatMult(1.0, B_e_vecs, e_vecs, e);
  RatelSymmetricMatPack(e, e_sym);
  // F*dS_iso*F^T ...
  for (CeedInt j = 0; j < 6; j++) b_sym[j] = 2 * e_sym[j] + (j < 3);
  RatelSymmetricMatUnpack(b_sym, b);
  RatelGreenEulerStrain_fwd(grad_du, b, de_sym);

  RatelPrincipalStretch(e_vals, pr_str);
  RatelPrincipalStretch_fwd(de_sym, e_vecs, pr_str, dpr_str);
  // Compute l[i] = log(pr_str[i]) = 0.5 * log1p(2*e_vals[i]) and dl[i]
  for (CeedInt i = 0; i < 3; i++) {
    l[i]  = 0.5 * RatelLog1pSeries(2 * e_vals[i]);
    dl[i] = dpr_str[i] / pr_str[i];
  }

  // Compute n[i] n[i]^T and (dn[i] n[i]^T + n[i] dn[i]^T)
  RatelEigenVectorOuterMult(e_vals, e_vecs, e_vecs_product);
  RatelEigenVectorOuterMult_fwd(de_sym, e_vals, e_vecs, e_vecs_product_fwd);

  for (CeedInt i = 0; i < 3; i++) {
    CeedScalar nn[3][3], grad_du_nn[3][3], nn_grad_duT[3][3], B[3][3], B_sym[6];
    RatelSymmetricMatUnpack(e_vecs_product[i], nn);
    RatelMatMatMult(1.0, grad_du, nn, grad_du_nn);
    RatelMatMatTransposeMult(1.0, nn, grad_du, nn_grad_duT);
    RatelMatMatAdd(1., grad_du_nn, 1., nn_grad_duT, B);
    RatelSymmetricMatPack(B, B_sym);

    RatelMatMatMatAddSymmetric(2 * dl[i], e_vecs_product[i], 1.0, e_vecs_product_fwd[i], -1.0, B_sym, A[i]);
  }

  CeedScalar s[3] = {0., 0., 0.}, ds[3] = {0., 0., 0.};
  for (CeedInt j = 0; j < N; j++) {
    s[0] += series_terms[j];
    s[1] += series_terms[j + 3];
    s[2] += series_terms[j + 6];

    ds[0] +=
        (m[j] * alpha[j] / 3.) * (2 * dl[0] * exp(alpha[j] * l[0]) - dl[1] * exp(alpha[j] * l[1]) - dl[2] * exp(alpha[j] * l[2])) * J_pow_alpha[j];
    ds[1] +=
        (m[j] * alpha[j] / 3.) * (-dl[0] * exp(alpha[j] * l[0]) + 2 * dl[1] * exp(alpha[j] * l[1]) - dl[2] * exp(alpha[j] * l[2])) * J_pow_alpha[j];
    ds[2] +=
        (m[j] * alpha[j] / 3.) * (-dl[0] * exp(alpha[j] * l[0]) - dl[1] * exp(alpha[j] * l[1]) + 2 * dl[2] * exp(alpha[j] * l[2])) * J_pow_alpha[j];

    ds[0] += -(*trace_depsilon) * (alpha[j] / 3.) * series_terms[j];
    ds[1] += -(*trace_depsilon) * (alpha[j] / 3.) * series_terms[j + 3];
    ds[2] += -(*trace_depsilon) * (alpha[j] / 3.) * series_terms[j + 6];
  }

  // F*dS_iso*F^T = sum_{i=0:3} [ds[i] - (2 * dpr_str[i] / pr_str[i]) * s[i]] n[i] n[i]^T + s[i] A[i]
  for (CeedInt k = 0; k < 6; k++) FdSFTranspose_iso_sym[k] = 0.;
  for (CeedInt i = 0; i < 3; i++) {
    for (CeedInt k = 0; k < 6; k++) {
      FdSFTranspose_iso_sym[k] += (ds[i] - 2 * dl[i] * s[i]) * e_vecs_product[i][k];
      FdSFTranspose_iso_sym[k] += s[i] * A[i][k];
    }
  }
  return CEED_ERROR_SUCCESS;
}

/// @}
