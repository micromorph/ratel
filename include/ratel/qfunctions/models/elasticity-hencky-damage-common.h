/// @file
/// Ratel elasticity in logarithmic strain space
#pragma once

#include <ceed/types.h>

#include "../../models/elasticity-damage.h"  // IWYU pragma: export
#include "../utils.h"                        // IWYU pragma: export
#include "elasticity-hencky-common.h"        // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

#define NUM_COMPONENTS_DIAGNOSTIC_ElasticityHenckyDamage 19
#define NUM_COMPONENTS_DIAGNOSTIC_ElasticityHenckyDamage_Dual 2

/**
  @brief Compute Psi_plus for Hencky hyperelasticity with damage

  @param[in]   mu                 shear modulus
  @param[in]   bulk               bulk modulus
  @param[in]   trace_e_log        trace log train tensor
  @param[in]   e_log_eigenvalues  eigenvalues log strain tensor
  @param[out]  Psi_plus           Psi^+

  @return Computed Psi_plus for Hencky hyperelasticity
**/
CEED_QFUNCTION_HELPER int PsiPlus_Hencky_Principal(CeedScalar mu, CeedScalar bulk, CeedScalar trace_e_log, const CeedScalar e_log_eigenvalues[3],
                                                   CeedScalar *Psi_plus) {
  // Compute Psi_dev
  const CeedScalar Psi_dev =
      mu * (e_log_eigenvalues[0] * e_log_eigenvalues[0] + e_log_eigenvalues[1] * e_log_eigenvalues[1] + e_log_eigenvalues[2] * e_log_eigenvalues[2]) -
      mu * trace_e_log * trace_e_log / 3.;

  // Compute Psi_vol = 0.5*K*tr(e)^2
  const CeedScalar Psi_vol = 0.5 * (trace_e_log > 0) * bulk * trace_e_log * trace_e_log;

  // Compute Psi_plus
  *Psi_plus = Psi_dev + Psi_vol;
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_PsiPlus_Hencky 15

/**
  @brief Compute Linearization of Psi_plus for Hencky hyperelasticity with damage

  @param[in]   mu                  Shear modulus
  @param[in]   bulk                Bulk modulus
  @param[in]   trace_e_log         trace log train tensor
  @param[in]   trace_de_log        trace log train tensor
  @param[in]   e_log_eigenvalues   eigenvalues log strain tensor
  @param[in]   de_log_eigenvalues  eigenvalues log strain tensor
  @param[out]  dPsi_plus           dPsi^+

  @return Computed linearization of Psi_plus for Hencky hyperelasticity
**/
CEED_QFUNCTION_HELPER int PsiPlus_Hencky_Principal_fwd(const CeedScalar mu, const CeedScalar bulk, const CeedScalar trace_e_log,
                                                       const CeedScalar trace_de_log, const CeedScalar e_log_eigenvalues[3],
                                                       const CeedScalar de_log_eigenvalues[3], CeedScalar *dPsi_plus) {
  // Compute dPsi_vol
  const CeedScalar dPsi_vol = (trace_e_log > 0) * bulk * trace_e_log * trace_de_log;

  // Compute Psi_dev
  const CeedScalar dPsi_dev = 2 * mu *
                                  (e_log_eigenvalues[0] * de_log_eigenvalues[0] + e_log_eigenvalues[1] * de_log_eigenvalues[1] +
                                   e_log_eigenvalues[2] * de_log_eigenvalues[2]) -
                              (2 / 3.) * mu * trace_e_log * trace_de_log;

  // Compute dPsi_plus
  *dPsi_plus = dPsi_dev + dPsi_vol;
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_PsiPlus_Hencky_fwd 16

/**
  @brief Compute diagnostics for hencky hyperelasticity + damage in log space

  @param[in]   ctx  QFunction context, holding `ElastoElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - volumetric qdata
                      - 1 - state
                      - 2 - u
                      - 3 - u_g gradient of u with respect to reference coordinates
  @param[out]  out  Diagnostics

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(Diagnostic_ElasticityHenckyDamage)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[2];
  const CeedScalar(*u_g)[4][CEED_Q_VLA] = (const CeedScalar(*)[4][CEED_Q_VLA])in[3];

  // Outputs
  CeedScalar(*diagnostic)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Context ElasticityDamage
  const RatelElasticityDamageParams *context = (RatelElasticityDamageParams *)ctx;
  const CeedScalar                   mu      = context->mu;
  const CeedScalar                   bulk    = context->bulk;
  const CeedScalar                   eta     = context->residual_stiffness;
  const CeedScalar                   rho     = context->common_parameters[RATEL_COMMON_PARAMETER_RHO];
  const CeedScalar                   Gc      = context->fracture_toughness;
  const CeedScalar                   l0      = context->characteristic_length;
  const bool                         use_AT1 = context->use_AT1;

  // Set c0 based on AT1 or AT2
  const CeedScalar c0 = use_AT1 ? 8. / 3. : 2.;

  // Quadrature point loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar Grad_u[3][3], b_eigenvalues[3], b_eigenvectors[3][3];
    CeedScalar tau_degr_eigenvalues[3], tau_degr_sym[6], sigma_degr_sym[6];
    CeedScalar e_log_eigenvalues[3], n_outer3x6[3][6];
    CeedScalar dXdx_initial[3][3];
    CeedScalar Grad_phi[3], Psi_plus, sigma_dev_degr_sym[6];

    // dX/dx from Q_data
    const CeedScalar wdetJ = q_data[0][i];

    //-- Compute Grad_u = du/dX * dX/dx
    RatelQdataUnpack(Q, i, q_data, dXdx_initial);

    // Spatial derivatives of u
    const CeedScalar dudX[3][3] = {
        {u_g[0][0][i], u_g[1][0][i], u_g[2][0][i]},
        {u_g[0][1][i], u_g[1][1][i], u_g[2][1][i]},
        {u_g[0][2][i], u_g[1][2][i], u_g[2][2][i]}
    };

    //-- Compute Grad_u = du/dX * dX/dx
    RatelMatMatMult(1, dudX, dXdx_initial, Grad_u);

    // Compute Jm1
    const CeedScalar Jm1 = RatelMatDetAM1(Grad_u);
    RatelMatMatMult(1, dudX, dXdx_initial, Grad_u);

    // Compute e_log = 0.5 ln(b) eigenvalues using log1p of e eigenvalues for numerical stability
    RatelHenckyStrainEigenvalues(Grad_u, b_eigenvalues, b_eigenvectors, e_log_eigenvalues);

    // Compute trace e_log
    CeedScalar trace_e_log = e_log_eigenvalues[0] + e_log_eigenvalues[1] + e_log_eigenvalues[2];

    // Get degraded mu and bulk
    const CeedScalar phi           = u[3][i];
    const CeedScalar one_minus_phi = 1 - phi;
    const CeedScalar g             = one_minus_phi * one_minus_phi * (1 - eta) + eta;
    const CeedScalar mu_degr       = g * mu;
    const CeedScalar bulk_degr     = (trace_e_log > 0 ? g : 1) * bulk;
    const CeedScalar phi_g[3]      = {u_g[0][3][i], u_g[1][3][i], u_g[2][3][i]};
    const CeedScalar alpha         = use_AT1 ? phi : phi * phi;

    // Compute sigma_degr
    for (CeedInt j = 0; j < 3; j++) {
      tau_degr_eigenvalues[j] = mu_degr * (e_log_eigenvalues[j] - 1 / 3. * trace_e_log) + bulk_degr * trace_e_log;
    }
    RatelEigenVectorOuterMult(b_eigenvalues, b_eigenvectors, n_outer3x6);
    RatelMatFromEigensystemSymmetric(tau_degr_eigenvalues, n_outer3x6, tau_degr_sym);
    RatelScalarMatMultSymmetric(1. / (Jm1 + 1), tau_degr_sym, sigma_degr_sym);

    // Compute Grad_phi = dX/dx^T dphi/dX
    RatelMatTransposeVecMult(1.0, dXdx_initial, phi_g, Grad_phi);
    const CeedScalar norm_Grad_phi2 = RatelDot3(Grad_phi, Grad_phi);

    //- Compute W_bar(u, phi)
    const CeedScalar Psi_vol = 0.5 * bulk * trace_e_log * trace_e_log;
    PsiPlus_Hencky_Principal(mu, bulk, trace_e_log, e_log_eigenvalues, &Psi_plus);
    const CeedScalar Psi_minus          = (trace_e_log > 0) ? 0 : Psi_vol;
    const CeedScalar degr_strain_energy = g * Psi_plus + Psi_minus;
    const CeedScalar fracture_energy    = Gc * (alpha + l0 * l0 * norm_Grad_phi2) / (c0 * l0);

    //--Store diagnostics
    // Displacement
    diagnostic[0][i] = u[0][i];
    diagnostic[1][i] = u[1][i];
    diagnostic[2][i] = u[2][i];

    // Cauchy stress tensor
    diagnostic[3][i] = sigma_degr_sym[0];
    diagnostic[4][i] = sigma_degr_sym[5];
    diagnostic[5][i] = sigma_degr_sym[4];
    diagnostic[6][i] = sigma_degr_sym[1];
    diagnostic[7][i] = sigma_degr_sym[3];
    diagnostic[8][i] = sigma_degr_sym[2];

    // Pressure
    diagnostic[9][i] = -bulk_degr * trace_e_log;

    // Strain tensor invariants
    diagnostic[10][i] = trace_e_log;

    const CeedScalar trace_e_log2 =
        e_log_eigenvalues[0] * e_log_eigenvalues[1] + e_log_eigenvalues[1] * e_log_eigenvalues[2] + e_log_eigenvalues[2] * e_log_eigenvalues[0];

    diagnostic[11][i] = trace_e_log2;
    diagnostic[12][i] = Jm1 + 1;

    // Degraded strain energy + fracture energy
    diagnostic[13][i] = degr_strain_energy + fracture_energy;

    // von Mises stress
    const CeedScalar trace_sigma_degr = RatelMatTraceSymmetric(sigma_degr_sym);

    RatelMatDeviatoricSymmetric(trace_sigma_degr, sigma_degr_sym, sigma_dev_degr_sym);

    const CeedScalar sigma_dev_degr_contract = RatelMatMatContractSymmetric(1.0, sigma_dev_degr_sym, sigma_dev_degr_sym);

    diagnostic[14][i] = sqrt(3 * sigma_dev_degr_contract / 2);

    // Compute mass density values
    // -- Set to 0 as default (rho should b 0.0 as defualt anyways)
    diagnostic[15][i] = 0.0;

    // -- if mass density is requested then compute mass density: rho*J
    if (rho > 0.0) diagnostic[15][i] = rho / diagnostic[12][i];

    // Additional diagnostic quantities for damage model
    // Damage
    diagnostic[16][i] = phi;

    // Psi_vol (undegraded)
    diagnostic[17][i] = Psi_vol;

    // Compute Psi_dev
    const CeedScalar Psi_dev = mu * (e_log_eigenvalues[0] * e_log_eigenvalues[0] + e_log_eigenvalues[1] * e_log_eigenvalues[1] +
                                     e_log_eigenvalues[2] * e_log_eigenvalues[2]) -
                               mu * trace_e_log * trace_e_log / 3.;

    // Psi_dev (undegraded)
    diagnostic[18][i] = Psi_dev;
    for (CeedInt j = 0; j < NUM_COMPONENTS_DIAGNOSTIC_ElasticityHenckyDamage; j++) diagnostic[j][i] *= wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute strain energy for log strains elasticity

  @param[in]   ctx  QFunction context holding RatelElastoElasticityParams
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - state
                      - 3 - gradient of u with respect to reference coordinates
  @param[out]  out  Output arrays
                      - 0 - strain energy

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(StrainEnergy_ElasticityHenckyDamage)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[2];
  const CeedScalar(*u_g)[4][CEED_Q_VLA] = (const CeedScalar(*)[4][CEED_Q_VLA])in[3];

  // Outputs
  CeedScalar(*energy) = (CeedScalar(*))out[0];

  // Context Elastoplaticity
  const RatelElasticityDamageParams *context = (RatelElasticityDamageParams *)ctx;
  const CeedScalar                   mu      = context->mu;
  const CeedScalar                   bulk    = context->bulk;
  const CeedScalar                   eta     = context->residual_stiffness;
  const CeedScalar                   Gc      = context->fracture_toughness;
  const CeedScalar                   l0      = context->characteristic_length;
  const bool                         use_AT1 = context->use_AT1;

  // Set c0 based on AT1 or AT2
  const CeedScalar c0 = use_AT1 ? 8. / 3. : 2.;

  // Quadrature point loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar Grad_u[3][3], dXdx_initial[3][3], Psi_plus = 0.0;
    CeedScalar b_eigenvectors[3][3];
    CeedScalar e_log_eigenvalues[3], b_eigenvalues[3];
    CeedScalar Grad_phi[3];

    // dX/dx from Q_data
    const CeedScalar wdetJ = q_data[0][i];

    //-- Compute Grad_u = du/dX * dX/dx
    RatelQdataUnpack(Q, i, q_data, dXdx_initial);
    // Spatial derivatives of u
    const CeedScalar dudX[3][3] = {
        {u_g[0][0][i], u_g[1][0][i], u_g[2][0][i]},
        {u_g[0][1][i], u_g[1][1][i], u_g[2][1][i]},
        {u_g[0][2][i], u_g[1][2][i], u_g[2][2][i]}
    };

    // Compute e_log = 0.5 ln(b) eigenvalues using log1p of e eigenvalues for numerical stability
    RatelMatMatMult(1, dudX, dXdx_initial, Grad_u);
    RatelHenckyStrainEigenvalues(Grad_u, b_eigenvalues, b_eigenvectors, e_log_eigenvalues);

    // Compute Psi
    const CeedScalar trace_e_log = e_log_eigenvalues[0] + e_log_eigenvalues[1] + e_log_eigenvalues[2];
    const CeedScalar phi         = u[3][i];
    const CeedScalar g           = (1 - phi) * (1 - phi) * (1 - eta) + eta;
    const CeedScalar Psi_vol     = 0.5 * bulk * trace_e_log * trace_e_log;
    const CeedScalar phi_g[3]    = {u_g[0][3][i], u_g[1][3][i], u_g[2][3][i]};
    const CeedScalar alpha       = use_AT1 ? phi : phi * phi;

    // Compute grad_phi = dX/dx^T dphi/dX
    RatelMatTransposeVecMult(1.0, dXdx_initial, phi_g, Grad_phi);
    const CeedScalar norm_Grad_phi2 = RatelDot3(Grad_phi, Grad_phi);

    //- Compute Hencky strain energy
    PsiPlus_Hencky_Principal(mu, bulk, trace_e_log, e_log_eigenvalues, &Psi_plus);
    const CeedScalar Psi_minus          = (trace_e_log > 0) ? 0 : Psi_vol;
    const CeedScalar degr_strain_energy = g * Psi_plus + Psi_minus;
    const CeedScalar fracture_energy    = Gc * (alpha + l0 * l0 * norm_Grad_phi2) / (c0 * l0);

    // Strain energy + fracture energy W_bar(u, phi)
    energy[i] = (degr_strain_energy + fracture_energy) * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute dual space diagnostic values for NeoHookean hyperelasticity with damage

  @param[in]   ctx  QFunction context, unused
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - volumetric qdata
  @param[out]  out  Output array
                      - 0 - nodal volume

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityDamageDualDiagnostic_Hencky)(void *ctx, const CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u_g)[4][CEED_Q_VLA] = (const CeedScalar(*)[4][CEED_Q_VLA])in[2];

  // Outputs
  CeedScalar(*v)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Context
  const CeedScalar *context = (const CeedScalar *)ctx;
  const CeedScalar  rho_0   = context[RATEL_COMMON_PARAMETER_RHO];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar dXdx[3][3], grad_u[3][3];

    // Read spatial derivatives of u
    const CeedScalar dudX[3][3] = {
        {u_g[0][0][i], u_g[1][0][i], u_g[2][0][i]},
        {u_g[0][1][i], u_g[1][1][i], u_g[2][1][i]},
        {u_g[0][2][i], u_g[1][2][i], u_g[2][2][i]}
    };
    // -- Qdata
    const CeedScalar wdetJ = q_data[0][i];
    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Compute grad_u
    RatelMatMatMult(1.0, dudX, dXdx, grad_u);

    // Compute Jm1
    const CeedScalar Jm1 = RatelMatDetAM1(grad_u);

    // Nodal volume
    v[0][i] = 1.0 * wdetJ;

    // Nodal density: rho_0 = rho / J
    v[1][i] = wdetJ * rho_0 / (Jm1 + 1);
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}
/// @}
