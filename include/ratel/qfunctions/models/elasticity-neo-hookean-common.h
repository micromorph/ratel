/// @file
/// Ratel neo-Hookean hyperelasticity at finite strain common QFunction source
#pragma once

#include <ceed/types.h>

#include "../../models/elasticity-linear.h"       // IWYU pragma: export
#include "../../models/neo-hookean.h"             // IWYU pragma: export
#include "../utils.h"                             // IWYU pragma: export
#include "elasticity-diagnostic-common.h"         // IWYU pragma: export
#include "elasticity-volumetric-energy-common.h"  // IWYU pragma: export
#include "elasticity-volumetric-stress-common.h"  // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

#define FLOPS_Tau_NeoHookean (FLOPS_Tau_vol + 9)
#define FLOPS_S_NeoHookean (FLOPS_S_vol + FLOPS_MatMatMultSymmetric + FLOPS_MatMatAddSymmetric + 6)
#define FLOPS_dS_NeoHookean \
  (FLOPS_GreenLagrangeStrain_fwd + FLOPS_CInverse_fwd + FLOPS_MatMatContractSymmetric + FLOPS_dS_vol + FLOPS_MatMatAddSymmetric + 6)
#define FLOPS_FdSFTranspose_NeoHookean (6 + FLOPS_MatTrace + FLOPS_FdSFTranspose_vol + FLOPS_MatMatAddSymmetric + 7)

/**
  @brief Compute Kirchoff tau for neo-Hookean hyperelasticity.

 `tau = (lambda * J dV/dJ) I + 2 mu * e`

  Ref https://ratel.micromorph.org/doc/modeling/materials/neo-hookean/#equation-eq-tau-neo-hookean

  @param[in]   J_dVdJ    J dV/dJ
  @param[in]   lambda    Lamé parameter
  @param[in]   two_mu    Two times the shear modulus
  @param[in]   e_sym     Green Euler strain, in symmetric representation
  @param[out]  tau_sym   Kirchoff tau, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelKirchhoffTau_NeoHookean(CeedScalar J_dVdJ, CeedScalar lambda, CeedScalar two_mu, const CeedScalar e_sym[6],
                                                       CeedScalar tau_sym[6]) {
  CeedScalar tau_vol_sym;

  RatelVolumetricKirchhoffTau(J_dVdJ, lambda, &tau_vol_sym);

  tau_sym[0] = two_mu * e_sym[0] + tau_vol_sym;
  tau_sym[1] = two_mu * e_sym[1] + tau_vol_sym;
  tau_sym[2] = two_mu * e_sym[2] + tau_vol_sym;
  tau_sym[3] = two_mu * e_sym[3];
  tau_sym[4] = two_mu * e_sym[4];
  tau_sym[5] = two_mu * e_sym[5];
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute second Kirchoff stress for neo-Hookean hyperelasticity.

 `S = (lambda * J dV/dJ) * C_inv + 2 mu C_inv * E`

  Ref https://ratel.micromorph.org/doc/modeling/materials/neo-hookean/#equation-eq-tau-neo-hookean

  @param[in]   J_dVdJ     J dV/dJ
  @param[in]   lambda     Lamé parameter
  @param[in]   two_mu     Two times the shear modulus
  @param[in]   C_inv_sym  Inverse of right Cauchy-Green tensor, in symmetric representation
  @param[in]   E_sym      Green Lagrange strain, in symmetric representation
  @param[out]  S_sym      Second Kirchoff stress, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelSecondKirchhoffStress_NeoHookean(CeedScalar J_dVdJ, CeedScalar lambda, CeedScalar two_mu,
                                                                const CeedScalar C_inv_sym[6], const CeedScalar E_sym[6], CeedScalar S_sym[6]) {
  CeedScalar C_inv_E_sym[6], S_vol_sym[6];

  // Compute S_vol
  RatelVolumetricSecondKirchhoffStress(J_dVdJ, lambda, C_inv_sym, S_vol_sym);

  // Compute the Second Piola-Kirchhoff (S)
  // -- C_inv * E
  RatelMatMatMultSymmetric(1.0, C_inv_sym, E_sym, C_inv_E_sym);

  // S = S_vol + 2*mu*C_inv*E
  RatelMatMatAddSymmetric(1., S_vol_sym, two_mu, C_inv_E_sym, S_sym);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute derivative of second Kirchoff stress for neo-Hookean hyperelasticity.

 `dS = dS_vol - mu * dC_inv`
 `dS_vol = ((lambda J^2 * d2V/dJ2 + lambda * J dV/dJ) * (C_inv:dE))  C_inv + (lambda * J dV/dJ) dC_inv`

  Ref https://ratel.micromorph.org/doc/modeling/materials/neo-hookean/#equation-eq-tau-neo-hookean

  @param[in]   J_dVdJ     J dV/dJ
  @param[in]   J2_d2VdJ2  J^2 d^2V/dJ^2
  @param[in]   lambda     Lamé parameter
  @param[in]   mu         Shear modulus
  @param[in]   F          Deformation gradient
  @param[in]   grad_du    Gradient of incremental change in u
  @param[in]   C_inv_sym  Inverse of right Cauchy-Green tensor, in symmetric representation
  @param[out]  dS_sym     Derivative of second Kirchoff stress, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelSecondKirchhoffStress_NeoHookean_fwd(CeedScalar J_dVdJ, CeedScalar J2_d2VdJ2, CeedScalar lambda, CeedScalar mu,
                                                                    const CeedScalar F[3][3], const CeedScalar grad_du[3][3],
                                                                    const CeedScalar C_inv_sym[6], CeedScalar dS_sym[6]) {
  CeedScalar dE_sym[6], dC_inv_sym[6], dS_vol_sym[6];

  // dE - Green-Lagrange strain tensor increment
  RatelGreenLagrangeStrain_fwd(grad_du, F, dE_sym);
  // dC_inv = -2*C_inv*dE*C_inv
  RatelCInverse_fwd(C_inv_sym, dE_sym, dC_inv_sym);
  // C_inv:dE
  CeedScalar Cinv_contract_dE = RatelMatMatContractSymmetric(1.0, C_inv_sym, dE_sym);

  // dS_vol
  RatelVolumetricSecondKirchhoffStress_fwd(J_dVdJ, J2_d2VdJ2, lambda, Cinv_contract_dE, C_inv_sym, dC_inv_sym, dS_vol_sym);

  // dS = dS_vol - mu * dC_inv
  RatelMatMatAddSymmetric(1., dS_vol_sym, -mu, dC_inv_sym, dS_sym);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute F*dS*F^T for neo-Hookean hyperelasticity in current configuration.

  Ref https://ratel.micromorph.org/doc/modeling/methods/hyperelasticity/#equation-cur-simp-jac

  @param[in]   J_dVdJ        J dV/dJ
  @param[in]   J2_d2VdJ2     J^2 d^2V/dJ^2
  @param[in]   lambda        Lamé parameter
  @param[in]   mu            Shear modulus
  @param[in]   grad_du       Gradient of incremental change in u
  @param[out]  FdSFTranspose F*dS*F^T needed for computing df1 in current configuration

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelComputeFdSFTranspose_NeoHookean(CeedScalar J_dVdJ, CeedScalar J2_d2VdJ2, CeedScalar lambda, CeedScalar mu,
                                                               const CeedScalar grad_du[3][3], CeedScalar FdSFTranspose[3][3]) {
  CeedScalar depsilon_sym[6], FdSFTranspose_vol_sym[6], FdSFTranspose_sym[6];
  // Compute depsilon = (grad_du + grad_du^T)/2
  depsilon_sym[0] = grad_du[0][0];
  depsilon_sym[1] = grad_du[1][1];
  depsilon_sym[2] = grad_du[2][2];
  depsilon_sym[3] = (grad_du[1][2] + grad_du[2][1]) / 2.;
  depsilon_sym[4] = (grad_du[0][2] + grad_du[2][0]) / 2.;
  depsilon_sym[5] = (grad_du[0][1] + grad_du[1][0]) / 2.;

  const CeedScalar trace_depsilon = RatelMatTraceSymmetric(depsilon_sym);

  // F*dS_vol*F^T
  RatelComputeFdSFTransposeVolumetric(J_dVdJ, J2_d2VdJ2, lambda, trace_depsilon, depsilon_sym, FdSFTranspose_vol_sym);

  // F*dS*F^T = F*dS_vol*F^T + 2 mu depsilon
  RatelMatMatAddSymmetric(1.0, FdSFTranspose_vol_sym, 2 * mu, depsilon_sym, FdSFTranspose_sym);
  RatelSymmetricMatUnpack(FdSFTranspose_sym, FdSFTranspose);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute strain energy for neo-Hookean model.

 `psi = lambda * V(J) - mu logJ + mu I1(E)`

  @param[in]   V              V(J)
  @param[in]   lambda         Lamé parameter
  @param[in]   mu             Shear modulus
  @param[in]   Jm1            Determinant of deformation gradient - 1
  @param[in]   trace_strain   Trace of Green Lagrange or Green euler strain tensor (E or e)
  @param[out]  strain_energy  Strain energy for Neo-Hookean model

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelStrainEnergy_NeoHookean(CeedScalar V, CeedScalar lambda, CeedScalar mu, CeedScalar Jm1, CeedScalar trace_strain,
                                                       CeedScalar *strain_energy) {
  const CeedScalar logJ = RatelLog1pSeries(Jm1);

  // Strain energy psi for Neo-Hookean
  *strain_energy = (lambda * V - mu * logJ + mu * trace_strain);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute strain energy for neo-Hookean hyperelasticity

  @param[in]   ctx  QFunction context, holding `RatelNeoHookeanElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - gradient of u with respect to reference coordinates
  @param[out]  out  Output arrays
                      - 0 - strain energy

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(StrainEnergy_NeoHookean)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*energy) = (CeedScalar(*))out[0];

  // Context
  const RatelNeoHookeanElasticityParams *context = (RatelNeoHookeanElasticityParams *)ctx;
  const CeedScalar                       mu      = context->mu;
  const CeedScalar                       lambda  = context->lambda;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar dXdx[3][3], grad_u[3][3], e_sym[6], V, strain_energy;

    // Qdata
    const CeedScalar wdetJ = q_data[0][i];

    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Read spatial derivatives of u; du/dX
    CeedScalar dudX[3][3];
    RatelGradUnpack(Q, i, ug, dudX);

    // Compute grad_u = du/dX * dX/dx
    // X is ref coordinate [-1,1]^3; x is physical coordinate
    RatelMatMatMult(1.0, dudX, dXdx, grad_u);

    // e - Euler strain tensor
    RatelGreenEulerStrain(grad_u, e_sym);
    const CeedScalar trace_e = RatelMatTraceSymmetric(e_sym);
    const CeedScalar Jm1     = RatelMatDetAM1(grad_u);

    VolumetricFunctionAndDerivatives(Jm1, &V, NULL, NULL);
    RatelStrainEnergy_NeoHookean(V, lambda, mu, Jm1, trace_e, &strain_energy);

    // Strain energy psi(e) for compressible Neo-Hookean
    energy[i] = strain_energy * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute projected diagnostic stress, and strain energy invariants values for Neo-hookean hyperelasticity

  @param[in]   ctx            QFunction context, holding `RatelNeoHookeanElasticityParams`
  @param[in]   Jm1            Determinant of deformation gradient - 1
  @param[in]   V              V(J); volumetric energy
  @param[in]   J_dVdJ         J dV/dJ
  @param[in]   grad_u         Gradient of incremental change in u
  @param[out]  sigma_sym      Cauchy stress tensor in symmetric representation
  @param[out]  strain_energy  Strain energy
  @param[out]  trace_e        Trace of strain tensor e
  @param[out]  trace_e2       Trace of strain tensor e*e

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int DiagnosticStress_NeoHookean(void *ctx, const CeedScalar Jm1, const CeedScalar V, const CeedScalar J_dVdJ,
                                                      CeedScalar grad_u[3][3], CeedScalar sigma_sym[6], CeedScalar *strain_energy,
                                                      CeedScalar *trace_e, CeedScalar *trace_e2) {
  // Context
  const RatelNeoHookeanElasticityParams *context = (RatelNeoHookeanElasticityParams *)ctx;
  const CeedScalar                       mu      = context->mu;
  const CeedScalar                       lambda  = context->lambda;
  const CeedScalar                       two_mu  = context->two_mu;

  CeedScalar e_sym[6], tau_sym[6];

  RatelGreenEulerStrain(grad_u, e_sym);
  RatelKirchhoffTau_NeoHookean(J_dVdJ, lambda, two_mu, e_sym, tau_sym);
  for (CeedInt j = 0; j < 6; j++) sigma_sym[j] = tau_sym[j] / (Jm1 + 1.);

  // Strain tensor invariants
  *trace_e  = RatelMatTraceSymmetric(e_sym);
  *trace_e2 = RatelMatMatContractSymmetric(1.0, e_sym, e_sym);

  // Strain energy
  RatelStrainEnergy_NeoHookean(V, lambda, mu, Jm1, *trace_e, strain_energy);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute projected diagnostic values for neo-Hookean hyperelasticity

  @param[in]   ctx  QFunction context, holding `RatelNeoHookeanElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - u
                      - 2 - gradient of u with respect to reference coordinates
  @param[out]  out  Output arrays
                      - 0 - projected diagnostic values

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(Diagnostic_NeoHookean)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return ElasticityDiagnostic(ctx, Q, DiagnosticStress_NeoHookean, in, out);
}

/// @}
