/// @file
/// Ratel CEED BP3 QFunction source
#include <ceed/types.h>

#include "../utils.h"  // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

/**
  @brief Compute the residual for the CEED scalar BP problem

  @param[in]   ctx  QFunction context, unused
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - volumetric qdata
                      - 1 - gradient of u
  @param[out]  out  Output array
                      - 0 - action of the QFunction

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(Residual_CEED_BP3)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[CEED_Q_VLA]     = (const CeedScalar(*)[CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*vg)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Read spatial derivatives of u; du/dX
    const CeedScalar dudX[3] = {ug[0][i], ug[1][i], ug[2][i]};

    // Read q_data (dXdxdXdx_T symmetric matrix)
    const CeedScalar dXdxdXdx_T[3][3] = {
        {q_data[1][i], q_data[6][i], q_data[5][i]},
        {q_data[6][i], q_data[2][i], q_data[4][i]},
        {q_data[5][i], q_data[4][i], q_data[3][i]}
    };

    for (CeedInt j = 0; j < 3; j++) {  // j = direction of vg
      vg[j][i] = (dudX[0] * dXdxdXdx_T[0][j] + dudX[1] * dXdxdXdx_T[1][j] + dudX[2] * dXdxdXdx_T[2][j]);
    }
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute the Jacobian for the CEED scalar BP problem

  @param[in]   ctx  QFunction context, unused
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - volumetric qdata
                      - 1 - gradient of du
  @param[out]  out  Output array
                      - 0 - action of the QFunction

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(Jacobian_CEED_BP3)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*dug)[CEED_Q_VLA]    = (const CeedScalar(*)[CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*dvg)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Read spatial derivatives of increment u; d(du)/dX = d(du)/dX
    const CeedScalar ddudX[3] = {dug[0][i], dug[1][i], dug[2][i]};

    // Read q_data (dXdxdXdx_T symmetric matrix)
    const CeedScalar dXdxdXdx_T[3][3] = {
        {q_data[1][i], q_data[6][i], q_data[5][i]},
        {q_data[6][i], q_data[2][i], q_data[4][i]},
        {q_data[5][i], q_data[4][i], q_data[3][i]}
    };

    for (CeedInt j = 0; j < 3; j++) {  // j = direction of vg
      dvg[j][i] = (ddudX[0] * dXdxdXdx_T[0][j] + ddudX[1] * dXdxdXdx_T[1][j] + ddudX[2] * dXdxdXdx_T[2][j]);
    }
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute the diagnostic quantities for the CEED scalar BP problem

  @param[in]   ctx  QFunction context, unused
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - volumetric qdata
                      - 1 - u
  @param[out]  out  Output array
                      - 0 - diagnostic values

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(Diagnostic_CEED_BP3)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar *u                   = in[1];

  // Outputs
  CeedScalar *diagnostic = out[0];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Qdata
    const CeedScalar wdetJ = q_data[0][i];

    // Displacement
    diagnostic[i] = u[i] * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
