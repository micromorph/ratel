/// @file
/// Ratel isochoric Ogden hyperelasticity at finite strain common QFunction source
#pragma once

#include <ceed/types.h>
#ifndef CEED_RUNNING_JIT_PASS
#include <math.h>
#endif

#include "../../models/ogden.h"                        // IWYU pragma: export
#include "../utils.h"                                  // IWYU pragma: export
#include "elasticity-diagnostic-common.h"              // IWYU pragma: export
#include "elasticity-isochoric-stress-ogden-common.h"  // IWYU pragma: export
#include "elasticity-volumetric-energy-common.h"       // IWYU pragma: export
#include "elasticity-volumetric-stress-common.h"       // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

#define FLOPS_Tau_IsochoricOgden (FLOPS_Tau_vol + FLOPS_Tau_iso_Ogden + 3)
#define FLOPS_S_IsochoricOgden (FLOPS_S_iso_Ogden + FLOPS_S_vol)
#define FLOPS_dS_IsochoricOgden (1 + FLOPS_dS_vol + FLOPS_dS_iso_Ogden + FLOPS_MatMatMatAddSymmetric)
#define FLOPS_FdSFTranspose_IsochoricOgden (FLOPS_FdSFTranspose_iso_Ogden + FLOPS_FdSFTranspose_vol + FLOPS_MatMatAddSymmetric)

/**
  @brief Compute Kirchoff tau for isochoric Ogden hyperelasticity.

  @param[in]   J_dVdJ        J dV/dJ
  @param[in]   bulk          Bulk modulus
  @param[in]   N             Number of Ogden parameters
  @param[in]   m             Array of first Ogden material model
  @param[in]   alpha         Array of second Ogden material model
  @param[in]   series_terms  Series terms
  @param[in]   e_vals        Eigenvalues of Green Euler strain tensor
  @param[in]   e_vecs        Eigenvectors of Green Euler strain tensor
  @param[out]  tau_sym       Kirchoff tau, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelKirchhoffTau_IsochoricOgden(CeedScalar J_dVdJ, CeedScalar bulk, CeedInt N, const CeedScalar *m,
                                                           const CeedScalar *alpha, const CeedScalar series_terms[9], const CeedScalar e_vals[3],
                                                           const CeedScalar e_vecs[3][3], CeedScalar tau_sym[6]) {
  CeedScalar tau_vol_sym;

  RatelVolumetricKirchhoffTau(J_dVdJ, bulk, &tau_vol_sym);

  RatelIsochoricKirchhoffTau_Ogden(N, m, alpha, series_terms, e_vals, e_vecs, tau_sym);

  tau_sym[0] += tau_vol_sym;
  tau_sym[1] += tau_vol_sym;
  tau_sym[2] += tau_vol_sym;
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute second Kirchoff stress for isochoric Ogden hyperelasticity.

  @param[in]   J_dVdJ     J dV/dJ
  @param[in]   bulk       Bulk modulus
  @param[in]   N          Number of Ogden parameters
  @param[in]   m          Array of first Ogden material model
  @param[in]   alpha      Array of second Ogden material model
  @param[in]   Jm1        Determinant of deformation gradient - 1
  @param[in]   C_inv_sym  Inverse of right Cauchy-Green tensor, in symmetric representation
  @param[in]   E_sym      Green Lagrange strain, in symmetric representation
  @param[out]  S_sym      Second Kirchoff stress, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int SecondKirchhoffStress_IsochoricOgden(CeedScalar J_dVdJ, CeedScalar bulk, CeedInt N, const CeedScalar *m,
                                                               const CeedScalar *alpha, CeedScalar Jm1, const CeedScalar C_inv_sym[6],
                                                               const CeedScalar E_sym[6], CeedScalar S_sym[6]) {
  CeedScalar       S_iso_sym[6], S_vol_sym[6];
  const CeedScalar J = Jm1 + 1.;

  // Compute S_iso
  RatelIsochoricSecondKirchhoffStress_Ogden(N, m, alpha, J, C_inv_sym, E_sym, S_iso_sym);

  // Compute S_vol
  RatelVolumetricSecondKirchhoffStress(J_dVdJ, bulk, C_inv_sym, S_vol_sym);

  // S = S_vol + S_iso
  RatelMatMatAddSymmetric(1.0, S_vol_sym, 1.0, S_iso_sym, S_sym);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute derivative of second Kirchoff stress for isochoric Ogden hyperelasticity.

  @param[in]   J_dVdJ     J dV/dJ
  @param[in]   J2_d2VdJ2  J^2 d^2V/dJ^2
  @param[in]   bulk       Bulk modulus
  @param[in]   N          Number of Ogden parameters
  @param[in]   m          Array of first Ogden material model
  @param[in]   alpha      Array of second Ogden material model
  @param[in]   Jm1        Determinant of deformation gradient - 1
  @param[in]   F          Deformation gradient
  @param[in]   grad_du    Gradient of incremental change in u
  @param[in]   C_inv_sym  Inverse of right Cauchy-Green tensor, in symmetric representation
  @param[in]   E_sym      Green Lagrange strain, in symmetric representation
  @param[out]  dS_sym     Derivative of second Kirchoff stress, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int SecondKirchhoffStress_IsochoricOgden_fwd(CeedScalar J_dVdJ, CeedScalar J2_d2VdJ2, CeedScalar bulk, CeedInt N,
                                                                   const CeedScalar *m, const CeedScalar *alpha, CeedScalar Jm1,
                                                                   const CeedScalar F[3][3], const CeedScalar grad_du[3][3],
                                                                   const CeedScalar C_inv_sym[6], const CeedScalar E_sym[6], CeedScalar dS_sym[6]) {
  CeedScalar dC_inv_sym[6], dS_vol_sym[6], dS_iso_sym[6], Cinv_contract_dE;

  // dS_iso
  RatelIsochoricSecondKirchhoffStress_Ogden_fwd(N, m, alpha, Jm1 + 1., F, grad_du, C_inv_sym, E_sym, dC_inv_sym, dS_iso_sym, &Cinv_contract_dE);

  // dS_vol
  RatelVolumetricSecondKirchhoffStress_fwd(J_dVdJ, J2_d2VdJ2, bulk, Cinv_contract_dE, C_inv_sym, dC_inv_sym, dS_vol_sym);

  // dS = dS_iso + dS_vol;
  RatelMatMatAddSymmetric(1, dS_vol_sym, 1.0, dS_iso_sym, dS_sym);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute F*dS*F^T for isochoric Ogden hyperelasticity in current configuration.

  @param[in]   J_dVdJ        J dV/dJ
  @param[in]   J2_d2VdJ2     J^2 d^2V/dJ^2
  @param[in]   bulk          Bulk modulus
  @param[in]   N             Number of Ogden parameters
  @param[in]   m             Array of first Ogden material model
  @param[in]   alpha         Array of second Ogden material model
  @param[in]   J_pow_alpha   Array of J^{-alpha[j]/3}, j=1..3
  @param[in]   series_terms  Series terms
  @param[in]   grad_du       Gradient of incremental change in u
  @param[in]   e_vals        Eigenvalues of Green Euler strain tensor
  @param[in]   e_vecs        Eigenvectors of Green Euler strain tensor
  @param[out]  FdSFTranspose F*dS*F^T needed for computing df1 in current configuration

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelComputeFdSFTranspose_IsochoricOgden(CeedScalar J_dVdJ, CeedScalar J2_d2VdJ2, CeedScalar bulk, CeedInt N,
                                                                   const CeedScalar *m, const CeedScalar *alpha, const CeedScalar J_pow_alpha[3],
                                                                   const CeedScalar series_terms[9], const CeedScalar grad_du[3][3],
                                                                   const CeedScalar e_vals[3], const CeedScalar e_vecs[3][3],
                                                                   CeedScalar FdSFTranspose[3][3]) {
  CeedScalar depsilon_sym[6], FdSFTranspose_vol_sym[6], FdSFTranspose_iso_sym[6], FdSFTranspose_sym[6], trace_depsilon;

  RatelComputeFdSFTransposeIsochoric_Ogden(N, m, alpha, J_pow_alpha, series_terms, grad_du, e_vals, e_vecs, depsilon_sym, FdSFTranspose_iso_sym,
                                           &trace_depsilon);
  RatelComputeFdSFTransposeVolumetric(J_dVdJ, J2_d2VdJ2, bulk, trace_depsilon, depsilon_sym, FdSFTranspose_vol_sym);

  // F*dS*F^T = F*dS_vol*F^T + F*dS_iso*F^T
  RatelMatMatAddSymmetric(1.0, FdSFTranspose_vol_sym, 1.0, FdSFTranspose_iso_sym, FdSFTranspose_sym);
  RatelSymmetricMatUnpack(FdSFTranspose_sym, FdSFTranspose);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute strain energy for isochoric Ogden model.

 `psi = sum_{j=1:3} (sum_{k=1:N} m_k/alpha_k (pr_bar_j^alpha_k - 1)) + bulk * V(J)`

  @param[in]   V              V(J)
  @param[in]   bulk           Bulk modulus
  @param[in]   N              Number of Ogden parameters
  @param[in]   m              Array of first Ogden material model
  @param[in]   alpha          Array of second Ogden material model
  @param[in]   pr_str_bar     Modified principal stretch
  @param[out]  strain_energy  Strain energy for isochoric Ogden model

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelStrainEnergy_IsochoricOgden(CeedScalar V, CeedScalar bulk, CeedInt N, const CeedScalar *m, const CeedScalar *alpha,
                                                           const CeedScalar pr_str_bar[3], CeedScalar *strain_energy) {
  CeedScalar omega[3], energy_iso = 0.;

  // energy_iso = sum_{j=1:3} omega[j] = sum_{j=1:3} (sum_{k=1:N} m_k/alpha_k (pr_bar_j^alpha_k - 1))
  for (CeedInt j = 0; j < 3; j++) {
    omega[j] = 0;
    for (CeedInt k = 0; k < N; k++) {
      omega[j] += (m[k] / alpha[k]) * (pow(pr_str_bar[j], alpha[k]) - 1.);
    }
    energy_iso += omega[j];
  }

  // Strain energy psi(e) for Ogden
  *strain_energy = bulk * V + energy_iso;
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute strain energy for isochoric Ogden hyperelasticity

  @param[in]   ctx  QFunction context, holding `RatelOgdenElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - gradient of u with respect to reference coordinates
  @param[out]  out  Output arrays
                      - 0 - strain energy

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(StrainEnergy_IsochoricOgden)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*energy) = (CeedScalar(*))out[0];

  // Context
  const RatelOgdenElasticityParams *context = (RatelOgdenElasticityParams *)ctx;
  const CeedInt                     N       = context->num_ogden_parameters;
  const CeedScalar                  bulk    = context->bulk;
  const CeedScalar                 *m       = context->m;
  const CeedScalar                 *alpha   = context->alpha;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar dXdx[3][3], grad_u[3][3], e_sym[6], e_vals[3], pr_str[3], pr_str_bar[3], V, strain_energy;

    // Qdata
    const CeedScalar wdetJ = q_data[0][i];

    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Read spatial derivatives of u; du/dX
    CeedScalar dudX[3][3];
    RatelGradUnpack(Q, i, ug, dudX);

    // Compute grad_u = du/dX * dX/dx
    // X is ref coordinate [-1,1]^3; x is physical coordinate
    RatelMatMatMult(1.0, dudX, dXdx, grad_u);

    // e - Euler strain tensor
    RatelGreenEulerStrain(grad_u, e_sym);
    const CeedScalar Jm1 = RatelMatDetAM1(grad_u);

    RatelMatComputeEigensystemSymmetric(e_sym, e_vals, NULL);
    RatelPrincipalStretch(e_vals, pr_str);
    const CeedScalar J_pow = pow(Jm1 + 1., -1. / 3.);

    RatelScalarVecMult(J_pow, pr_str, pr_str_bar);
    VolumetricFunctionAndDerivatives(Jm1, &V, NULL, NULL);
    RatelStrainEnergy_IsochoricOgden(V, bulk, N, m, alpha, pr_str_bar, &strain_energy);

    // Strain energy psi(e) for IsochoricOgden
    energy[i] = strain_energy * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute projected diagnostic stress, and strain energy invariants values for isochoric Ogden hyperelasticity

  @param[in]   ctx            QFunction context, holding `RatelOgdenElasticityParams`
  @param[in]   Jm1            Determinant of deformation gradient - 1
  @param[in]   V              V(J); volumetric energy
  @param[in]   J_dVdJ         J dV/dJ
  @param[in]   grad_u         Gradient of incremental change in u
  @param[out]  sigma_sym      Cauchy stress tensor in symmetric representation
  @param[out]  strain_energy  Strain energy
  @param[out]  trace_e        Trace of strain tensor e
  @param[out]  trace_e2       Trace of strain tensor e*e

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int DiagnosticStress_IsochoricOgden(void *ctx, const CeedScalar Jm1, const CeedScalar V, const CeedScalar J_dVdJ,
                                                          CeedScalar grad_u[3][3], CeedScalar sigma_sym[6], CeedScalar *strain_energy,
                                                          CeedScalar *trace_e, CeedScalar *trace_e2) {
  // Context
  const RatelOgdenElasticityParams *context = (RatelOgdenElasticityParams *)ctx;
  const CeedInt                     N       = context->num_ogden_parameters;
  const CeedScalar                  bulk    = context->bulk;
  const CeedScalar                 *m       = context->m;
  const CeedScalar                 *alpha   = context->alpha;

  CeedScalar e_sym[6], tau_sym[6], e_vals[3], e_vecs[3][3], pr_str[3], pr_str_bar[3], l[3], series_terms[9];

  RatelGreenEulerStrain(grad_u, e_sym);
  RatelMatComputeEigensystemSymmetric(e_sym, e_vals, e_vecs);
  // Compute l[j] = log(pr_str[j]) = 0.5 * log1p(2*e_vals[j])
  for (CeedInt j = 0; j < 3; j++) l[j] = 0.5 * RatelLog1pSeries(2 * e_vals[j]);
  for (CeedInt j = 0; j < N; j++) {
    CeedScalar J_pow_alpha_m = pow(Jm1 + 1., -alpha[j] / 3.) * (m[j] / 3.);
    CeedScalar expm1_term_0 = RatelExpm1Series(alpha[j] * l[0]), expm1_term_1 = RatelExpm1Series(alpha[j] * l[1]),
               expm1_term_2 = RatelExpm1Series(alpha[j] * l[2]);

    series_terms[j]     = J_pow_alpha_m * (2 * expm1_term_0 - expm1_term_1 - expm1_term_2);
    series_terms[j + 3] = J_pow_alpha_m * (-expm1_term_0 + 2 * expm1_term_1 - expm1_term_2);
    series_terms[j + 6] = J_pow_alpha_m * (-expm1_term_0 - expm1_term_1 + 2 * expm1_term_2);
  }
  RatelKirchhoffTau_IsochoricOgden(J_dVdJ, bulk, N, m, alpha, series_terms, e_vals, e_vecs, tau_sym);
  for (CeedInt j = 0; j < 6; j++) sigma_sym[j] = tau_sym[j] / (Jm1 + 1.);

  // Strain tensor invariants
  *trace_e  = RatelMatTraceSymmetric(e_sym);
  *trace_e2 = RatelMatMatContractSymmetric(1.0, e_sym, e_sym);

  // Strain energy
  RatelPrincipalStretch(e_vals, pr_str);
  const CeedScalar J_pow = pow(Jm1 + 1., -1. / 3.);

  RatelScalarVecMult(J_pow, pr_str, pr_str_bar);
  RatelStrainEnergy_IsochoricOgden(V, bulk, N, m, alpha, pr_str_bar, strain_energy);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute projected diagnostic values for isochoric Ogden hyperelasticity

  @param[in]   ctx  QFunction context, holding `RatelOgdenElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(Diagnostic_IsochoricOgden)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return ElasticityDiagnostic(ctx, Q, DiagnosticStress_IsochoricOgden, in, out);
}

/// @}
