/// @file
/// Ratel elasticity in logarithmic strain space, InitialPrincipal configuration
#include <ceed/types.h>
#ifndef CEED_RUNNING_JIT_PASS
#include <math.h>
#endif

#include "../../models/elasticity-damage.h"   // IWYU pragma: export
#include "../boundaries/platen-nitsche.h"     // IWYU pragma: export
#include "elasticity-hencky-damage-common.h"  // IWYU pragma: export

#define NUM_COMPONENTS_STORED_ElasticityHenckyDamageInitialPrincipal 21
#define NUM_COMPONENTS_STATE_ElasticityHenckyDamageInitialPrincipal 1
#define NUM_ACTIVE_FIELD_EVAL_MODES_ElasticityHenckyDamageInitialPrincipal 2
#define NUM_U_t_FIELD_EVAL_MODES_ElasticityHenckyDamageInitialPrincipal 1
#define FLOPS_JACOBIAN_ElasticityHenckyDamageInitialPrincipal                                                                     \
  (3 + 3 * FLOPS_MatMatMult + FLOPS_EigenVectorOuterMult + FLOPS_Hencky_dbedF_Symmetric + FLOPS_Hencky_dedb_Eigenvalues + 4 + 7 + \
   FLOPS_EigenVectorOuterMult_fwd + FLOPS_KirchhoffStressSymmetric_fwd + 4 + 2 * FLOPS_MatMatAddSymmetric + FLOPS_MatInverse +    \
   3 * FLOPS_MatMatMult + FLOPS_MatMatAdd)

/// @addtogroup RatelMaterials
/// @{

/**
  @brief Compute u_t term of damage residual

  @param[in]   ctx  QFunction context, holding `RatelElasticityDamageParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - volumetric qdata
                      - 1 - u_t (four components: 3 displacement components, 1 scalar damage field)
  @param[out]  out  Output arrays
                      - 0 - action on u_t

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityDamageResidual_ut_HenckyInitial)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u_t)[CEED_Q_VLA]    = (const CeedScalar(*)[CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*v_t)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Context Elasticity+Damage
  const RatelElasticityDamageParams *context        = (RatelElasticityDamageParams *)ctx;
  const CeedScalar                   xi             = context->damage_viscosity;
  const CeedScalar                   damage_scaling = context->damage_scaling;

  // Quadrature point loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    const CeedScalar wdetJ = q_data[0][i];

    // Update values
    v_t[0][i] = 0;
    v_t[1][i] = 0;
    v_t[2][i] = 0;
    v_t[3][i] = damage_scaling * xi * u_t[3][i] * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Helper for residual, elasticity in log space - current configuration
cat
  @param[in]   ctx   QFunction context, holding `RatelElastoElasticityParams`
  @param[in]   Q     Number of quadrature points
  @param[in]   in    Input arrays
                       - 0 - volumetric qdata
                       - 1 - state values
                       - 2 - u
                       - 3 - gradient of u with respect to reference coordinates
  @param[out]  out   Output arrays
                       - 0 - currently computed state components
                       - 1 - stored values
                       - 2 - v
                       - 3 - dv

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityHenckyDamageInitialPrincipal_Residual)
(void *ctx, const CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*state)              = in[1];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[2];
  const CeedScalar(*u_g)[4][CEED_Q_VLA] = (const CeedScalar(*)[4][CEED_Q_VLA])in[3];

  // Outputs
  CeedScalar(*current_state)       = out[0];
  CeedScalar(*stored_values)       = out[1];
  CeedScalar(*v)[CEED_Q_VLA]       = (CeedScalar(*)[CEED_Q_VLA])out[2];
  CeedScalar(*dvdX)[4][CEED_Q_VLA] = (CeedScalar(*)[4][CEED_Q_VLA])out[3];

  // Context Elastoplaticity
  const RatelElasticityDamageParams *context        = (RatelElasticityDamageParams *)ctx;
  const CeedScalar                   mu             = context->mu;
  const CeedScalar                   bulk           = context->bulk;
  const CeedScalar                   Gc             = context->fracture_toughness;
  const CeedScalar                   l0             = context->characteristic_length;
  const CeedScalar                   eta            = context->residual_stiffness;
  const CeedScalar                   damage_scaling = context->damage_scaling;
  const bool                         use_AT1        = context->use_AT1;

  // Get Psi_plus_critical and c0 term
  const CeedScalar Psi_plus_critical = use_AT1 ? 3 * Gc / (8 * 2 * l0) : 0;
  const CeedScalar c0                = use_AT1 ? 8. / 3. : 2.;

  // Quadrature point loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar Grad_u[3][3], dXdx_initial[3][3];
    CeedScalar b_eigenvectors[3][3], b_eigenvalues[3], e_log_eigenvalues[3], s_eigenvalues[3];
    CeedScalar tau_degr[3][3], tau_degr_sym[6], tau_degr_eigenvalues[3];
    CeedScalar F_inv[3][3], b_eigenvectors_outer3x6[3][6];
    CeedScalar Psi_plus = 0.0, Psi_plus_history_flag, H, Grad_phi[3];
    CeedScalar s_sym[6], P_degr[3][3];

    // Unpack Q_data
    const CeedScalar wdetJ = q_data[0][i];
    RatelQdataUnpack(Q, i, q_data, dXdx_initial);

    //-- Compute Grad_u = du/dX * dX/dx
    const CeedScalar dudX[3][3] = {
        {u_g[0][0][i], u_g[1][0][i], u_g[2][0][i]},
        {u_g[0][1][i], u_g[1][1][i], u_g[2][1][i]},
        {u_g[0][2][i], u_g[1][2][i], u_g[2][2][i]}
    };

    RatelMatMatMult(1, dudX, dXdx_initial, Grad_u);

    // Compute F = I + Grad_u and F_inv
    const CeedScalar F[3][3] = {
        {Grad_u[0][0] + 1, Grad_u[0][1],     Grad_u[0][2]    },
        {Grad_u[1][0],     Grad_u[1][1] + 1, Grad_u[1][2]    },
        {Grad_u[2][0],     Grad_u[2][1],     Grad_u[2][2] + 1}
    };
    CeedScalar Jm1 = RatelMatDetAM1(Grad_u);

    // Compute e_log = 0.5 ln(b) eigenvalues using log1p of e eigenvalues for numerical stability
    RatelHenckyStrainEigenvalues(Grad_u, b_eigenvalues, b_eigenvectors, e_log_eigenvalues);

    // Compute trace e_log
    CeedScalar trace_e_log = e_log_eigenvalues[0] + e_log_eigenvalues[1] + e_log_eigenvalues[2];

    // Get degraded mu and bulk
    const CeedScalar phi           = u[3][i];
    const CeedScalar one_minus_phi = 1 - phi;
    const CeedScalar g             = one_minus_phi * one_minus_phi * (1 - eta) + eta;
    const CeedScalar bulk_degr     = (trace_e_log > 0 ? g : 1) * bulk;

    // Compute eigenvalues of s, tau, and tau_degr
    for (CeedInt j = 0; j < 3; j++) {
      s_eigenvalues[j]        = 2 * mu * (e_log_eigenvalues[j] - 1 / 3. * trace_e_log);
      tau_degr_eigenvalues[j] = g * s_eigenvalues[j] + bulk_degr * trace_e_log;
    }

    // Compute P_degr
    RatelMatInverse(F, Jm1 + 1, F_inv);
    RatelEigenVectorOuterMult(b_eigenvalues, b_eigenvectors, b_eigenvectors_outer3x6);
    RatelMatFromEigensystemSymmetric(s_eigenvalues, b_eigenvectors_outer3x6, s_sym);
    RatelMatFromEigensystemSymmetric(tau_degr_eigenvalues, b_eigenvectors_outer3x6, tau_degr_sym);
    RatelSymmetricMatUnpack(tau_degr_sym, tau_degr);
    RatelMatMatTransposeMult(1, tau_degr, F_inv, P_degr);

    // Compute (grad(v), tau_degr)
    for (CeedInt j = 0; j < 3; j++) {
      for (CeedInt k = 0; k < 3; k++) {
        dvdX[j][k][i] = 0;
        for (CeedInt m = 0; m < 3; m++) {
          dvdX[j][k][i] += wdetJ * dXdx_initial[j][m] * P_degr[k][m];
        }
      }
    }

    //----- Damage problem
    // Compute Psi_plus
    PsiPlus_Hencky_Principal(mu, bulk, trace_e_log, e_log_eigenvalues, &Psi_plus);

    // Treat Psi_plus as a history variable and ensure that increases monotonically for crack irreversibility
    const CeedScalar Psi_plus_old = state[i];
    Psi_plus_history_flag         = Psi_plus > Psi_plus_old;
    Psi_plus                      = RatelMax(Psi_plus, Psi_plus_old);
    Psi_plus                      = RatelMax(Psi_plus_critical, Psi_plus);

    // Spatial derivative of phi
    const CeedScalar phi_g[3] = {u_g[0][3][i], u_g[1][3][i], u_g[2][3][i]};

    // Compute grad_phi = dX/dx^T dphi/dX
    RatelMatTransposeVecMult(1.0, dXdx_initial, phi_g, Grad_phi);
    const CeedScalar dg_dphi = -2 * (1 - eta) * (1 - phi);

    // If AT2, substitute appropiate values for c0 and alpha (hence dalpha)
    const CeedScalar dalpha_dphi = use_AT1 ? 1. : 2. * phi;

    // Compute H and update v values
    H       = dg_dphi * Psi_plus + (Gc / (c0 * l0)) * dalpha_dphi;
    v[0][i] = 0;
    v[1][i] = 0;
    v[2][i] = 0;
    v[3][i] = damage_scaling * H * wdetJ;

    // Update fourth component of dvdX via (grad(q), 2 Gc l0 / c0 grad(phi))
    for (CeedInt j = 0; j < 3; j++) {
      dvdX[j][3][i] = damage_scaling * (Grad_phi[0] * dXdx_initial[j][0] + Grad_phi[1] * dXdx_initial[j][1] + Grad_phi[2] * dXdx_initial[j][2]) * 2. *
                      Gc * l0 * wdetJ / c0;
    }

    // Save updated values for state fields
    RatelStoredValuesPack(Q, i, 0, 1, &Psi_plus, current_state);

    // Store values
    RatelStoredValuesPack(Q, i, 0, 6, (CeedScalar *)s_sym, stored_values);
    RatelStoredValuesPack(Q, i, 6, 9, (CeedScalar *)Grad_u, stored_values);
    RatelStoredValuesPack(Q, i, 15, 3, (CeedScalar *)tau_degr_eigenvalues, stored_values);
    RatelStoredValuesPack(Q, i, 18, 1, &Jm1, stored_values);
    RatelStoredValuesPack(Q, i, 19, 1, &phi, stored_values);
    RatelStoredValuesPack(Q, i, 20, 1, &Psi_plus_history_flag, stored_values);
  }
  (void)(Diagnostic_ElasticityHencky);
  (void)(StrainEnergy_ElasticityHencky);
  (void)(Diagnostic_ElasticityHenckyDamage);
  (void)(StrainEnergy_ElasticityHenckyDamage);
  (void)(ElasticityDamageDualDiagnostic_Hencky);
  return CEED_ERROR_SUCCESS;
}
/**
  @brief Helper for residual, elasticity in log space - current configuration

  @param[in]   ctx   QFunction context, holding `RatelElastoElasticityParams`
  @param[in]   Q     Number of quadrature points
  @param[in]   in    Input arrays
                       - 0 - volumetric qdata
                       - 1 - previously accepted plastic state components
                       - 2 - gradient of u with respect to reference coordinates
  @param[out]  out   Output arrays
                       - 0 - currently computed plastic state components

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityHenckyDamageInitialPrincipal_Jacobian)
(void *ctx, const CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA]  = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*current_state)       = in[1];
  const CeedScalar(*stored_values)       = in[2];
  const CeedScalar(*du)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[3];
  const CeedScalar(*du_g)[4][CEED_Q_VLA] = (const CeedScalar(*)[4][CEED_Q_VLA])in[4];

  // Outputs
  CeedScalar(*dv)[CEED_Q_VLA]       = (CeedScalar(*)[CEED_Q_VLA])out[0];
  CeedScalar(*ddvdX)[4][CEED_Q_VLA] = (CeedScalar(*)[4][CEED_Q_VLA])out[1];

  // Context Elastoplasticity
  const RatelElasticityDamageParams *context        = (RatelElasticityDamageParams *)ctx;
  const CeedScalar                   mu             = context->mu;
  const CeedScalar                   bulk           = context->bulk;
  const CeedScalar                   Gc             = context->fracture_toughness;
  const CeedScalar                   l0             = context->characteristic_length;
  const CeedScalar                   eta            = context->residual_stiffness;
  const CeedScalar                   damage_scaling = context->damage_scaling;
  const bool                         use_AT1        = context->use_AT1;
  const CeedScalar                   xi             = context->damage_viscosity;
  const bool                         use_offdiag    = context->use_offdiagonal;
  const CeedScalar                   shift_v        = context->common_parameters[1];

  // Set d2alpha_dphi2 and c0 based on AT1 or AT2
  const CeedScalar c0            = use_AT1 ? 8. / 3. : 2.;
  const CeedScalar d2alpha_dphi2 = use_AT1 ? 0. : 2.;
  const CeedScalar d2g_dphi2     = 2 * (1 - eta);

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar phi = 0.0, Jm1 = 0.0;
    CeedScalar Grad_u[3][3], grad_du[3][3], db_sym[6];
    CeedScalar dF[3][3], dXdx_initial[3][3], dXdx[3][3];
    CeedScalar tau_degr_sym[6], dtau_degr_sym[6], tau_degr[3][3], dtau_degr[3][3], F_inv[3][3];
    CeedScalar tau_degr_F_invT_dFT[3][3], F_invT_dFT[3][3], dtau_degr_minus_tau_degr_F_invT_dFT[3][3];
    CeedScalar b_eigenvalues[3], b_eigenvectors[3][3], de_log_eigenvalues[3], tau_degr_eigenvalues[3], e_log_eigenvalues[3];
    CeedScalar b_eigenvectors_outer3x6[3][6], db_eigenvectors_outer3x6[3][6];
    CeedScalar Psi_plus = 0.0, Psi_plus_history_flag, dPsi_plus = 0.0, dH, Grad_dphi[3], dP_degr[3][3];
    CeedScalar s_sym[6], tau_sym[6];

    // Unpack state
    RatelStoredValuesUnpack(Q, i, 0, 1, current_state, &Psi_plus);

    // Unpack stored
    RatelStoredValuesUnpack(Q, i, 0, 6, stored_values, (CeedScalar *)s_sym);
    RatelStoredValuesUnpack(Q, i, 6, 9, stored_values, (CeedScalar *)Grad_u);
    RatelStoredValuesUnpack(Q, i, 15, 3, stored_values, (CeedScalar *)tau_degr_eigenvalues);
    RatelStoredValuesUnpack(Q, i, 18, 1, stored_values, &Jm1);
    RatelStoredValuesUnpack(Q, i, 19, 1, stored_values, &phi);
    RatelStoredValuesUnpack(Q, i, 20, 1, stored_values, &Psi_plus_history_flag);

    // Compute e_log = 0.5 ln(b) eigenvalues using log1p of e eigenvalues for numerical stability
    RatelHenckyStrainEigenvalues(Grad_u, b_eigenvalues, b_eigenvectors, e_log_eigenvalues);

    //- Compute dF = grad_du = du * dX/dx
    RatelQdataUnpack(Q, i, q_data, dXdx_initial);

    // Read spatial derivatives of increment u; d(du)/dX = d(du)/dX
    const CeedScalar ddudX[3][3] = {
        {du_g[0][0][i], du_g[1][0][i], du_g[2][0][i]},
        {du_g[0][1][i], du_g[1][1][i], du_g[2][1][i]},
        {du_g[0][2][i], du_g[1][2][i], du_g[2][2][i]}
    };

    RatelMatMatMult(1.0, ddudX, dXdx_initial, dF);

    // Retrieve F = I + Grad_u
    const CeedScalar F[3][3] = {
        {Grad_u[0][0] + 1, Grad_u[0][1],     Grad_u[0][2]    },
        {Grad_u[1][0],     Grad_u[1][1] + 1, Grad_u[1][2]    },
        {Grad_u[2][0],     Grad_u[2][1],     Grad_u[2][2] + 1}
    };

    // Compute F_inv and grad_du
    RatelMatInverse(F, Jm1 + 1, F_inv);
    RatelMatMatMult(1.0, dXdx_initial, F_inv, dXdx);
    RatelMatMatMult(1.0, ddudX, dXdx, grad_du);

    // Compute b_eigenvectors_outer3x6
    RatelEigenVectorOuterMult(b_eigenvalues, b_eigenvectors, b_eigenvectors_outer3x6);

    // Compute db/dF
    RatelHencky_dbedF_Symmetric(grad_du, b_eigenvalues, b_eigenvectors_outer3x6, db_sym);

    // Compute de/db eigenvalues
    RatelHencky_dedb_Eigenvalues(db_sym, b_eigenvalues, b_eigenvectors, de_log_eigenvalues);

    // Compute trace de_log and trace e_log
    const CeedScalar trace_de_log = de_log_eigenvalues[0] + de_log_eigenvalues[1] + de_log_eigenvalues[2];
    const CeedScalar trace_e_log  = e_log_eigenvalues[0] + e_log_eigenvalues[1] + e_log_eigenvalues[2];

    // Get value of degradation function g(phi)
    const CeedScalar one_minus_phi = 1 - phi;
    const CeedScalar g             = one_minus_phi * one_minus_phi * (1 - eta) + eta;
    const CeedScalar mu_degr       = g * mu;
    const CeedScalar bulk_degr     = (trace_e_log > 0. ? g : 1.) * bulk;

    // Compute dtau_degr
    RatelEigenVectorOuterMult_fwd(db_sym, b_eigenvalues, b_eigenvectors, db_eigenvectors_outer3x6);
    RatelHenckyKirchhoffStressSymmetric_fwd(mu_degr, bulk_degr, trace_de_log, de_log_eigenvalues, tau_degr_eigenvalues, b_eigenvectors_outer3x6,
                                            db_eigenvectors_outer3x6, dtau_degr_sym);

    // Add diagonal term
    const CeedScalar dphi        = du[3][i];
    const CeedScalar Identity[6] = {1., 1., 1., 0., 0., 0.};

    // Retrieve tau_degr
    if (trace_e_log > 0) {
      RatelMatMatAddSymmetric(mu_degr / mu, s_sym, bulk_degr * trace_e_log, Identity, tau_degr_sym);
    } else {
      RatelMatMatAddSymmetric(mu_degr / mu, s_sym, bulk * trace_e_log, Identity, tau_degr_sym);
    }
    RatelSymmetricMatUnpack(tau_degr_sym, tau_degr);

    if (use_offdiag) {
      // Add dtau_degr/dphi = dg/dphi * dphi * tau_degr (if offdiagonal term are considered)
      const CeedScalar dg_dphi = -2 * one_minus_phi * (1 - eta);

      if (trace_e_log > 0) {
        RatelMatMatAddSymmetric(1., s_sym, bulk * trace_e_log, Identity, tau_sym);
        RatelMatMatAddSymmetric(1., dtau_degr_sym, dg_dphi * dphi, tau_sym, dtau_degr_sym);
      } else {
        RatelMatMatAddSymmetric(1., dtau_degr_sym, dg_dphi * dphi, s_sym, dtau_degr_sym);
      }
    }
    RatelSymmetricMatUnpack(dtau_degr_sym, dtau_degr);

    //- Compute df1 = dP = dtau * F^-T - tau * (F^-T * dF^T * F^-T) = (dtau - tau * F^-T * dF^T) * F^-T
    RatelMatInverse(F, Jm1 + 1, F_inv);
    RatelMatTransposeMatTransposeMult(1, F_inv, dF, F_invT_dFT);
    RatelMatMatMult(1., tau_degr, F_invT_dFT, tau_degr_F_invT_dFT);
    RatelMatMatAdd(1., dtau_degr, -1., tau_degr_F_invT_dFT, dtau_degr_minus_tau_degr_F_invT_dFT);
    RatelMatMatTransposeMult(1, dtau_degr_minus_tau_degr_F_invT_dFT, F_inv, dP_degr);

    const CeedScalar wdetJ = q_data[0][i];

    for (CeedInt j = 0; j < 3; j++) {
      for (CeedInt k = 0; k < 3; k++) {
        ddvdX[j][k][i] = 0;
        for (CeedInt m = 0; m < 3; m++) {
          ddvdX[j][k][i] += wdetJ * dXdx_initial[j][m] * dP_degr[k][m];
        }
      }
    }

    //--Compute l0*Gc*(grad(dphi), grad(q))
    // Spatial derivative of phi
    const CeedScalar dphi_g[3] = {du_g[0][3][i], du_g[1][3][i], du_g[2][3][i]};

    // Grad_dphi = dX/dx^T * d(dphi)/dX
    RatelMatTransposeVecMult(1.0, dXdx_initial, dphi_g, Grad_dphi);

    // Get dg_dphi
    const CeedScalar dg_dphi = -2 * (1 - eta) * (1 - phi);

    // Compute d_phi_t
    // Retrieve dphi
    const CeedScalar dphi_t = shift_v * dphi;

    // Compute dPsi_plus
    const bool add_offdiag = !(Psi_plus_history_flag == 0. || !use_offdiag);

    PsiPlus_Hencky_Principal_fwd(mu, bulk, trace_e_log, trace_de_log, e_log_eigenvalues, de_log_eigenvalues, &dPsi_plus);
    dPsi_plus = add_offdiag * dPsi_plus;

    // Compute dH and dv values
    dH       = d2g_dphi2 * dphi * Psi_plus + dg_dphi * dPsi_plus + (Gc / (c0 * l0)) * d2alpha_dphi2 * dphi + xi * dphi_t;
    dv[0][i] = 0;
    dv[1][i] = 0;
    dv[2][i] = 0;
    dv[3][i] = damage_scaling * dH * wdetJ;

    // Update fourth component of dvdX via (grad(dq), 2 Gc l0 / c0 grad(dphi))
    for (CeedInt j = 0; j < 3; j++) {
      ddvdX[j][3][i] = damage_scaling * (Grad_dphi[0] * dXdx_initial[j][0] + Grad_dphi[1] * dXdx_initial[j][1] + Grad_dphi[2] * dXdx_initial[j][2]) *
                       2. * Gc * l0 * wdetJ / c0;
    }
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute platens residual for linear elasticity with damage

  @param[in]   ctx       QFunction context, holding `RatelElasticityDamageParams`
  @param[in]   Q         Number of quadrature points
  @param[in]   i         Index
  @param[in]   in        Input arrays
                         - 0 - volumetric qdata
                         - 2 - u (four components: 3 displacement components, 1 scalar damage field)
                         - 3 - u_g gradient of u with respect to reference coordinates
  @param[out]  out       Output arrays
                         - 1 - stored vector
                         - 2 - initializing v
  @param[out]  dXdx_initial      dXdx_initial
  @param[out]  P_degr  Degraded stress tensor

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int ElasticityDamagePlatenResidual_HenckyInitialPrincipal(void *ctx, const CeedInt Q, const CeedInt i,
                                                                                const CeedScalar *const *in, CeedScalar *const *out,
                                                                                CeedScalar dXdx_initial[3][3], CeedScalar P_degr[3][3]) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[2];
  const CeedScalar(*u_g)[4][CEED_Q_VLA] = (const CeedScalar(*)[4][CEED_Q_VLA])in[3];

  // Outputs
  CeedScalar(*state_out)     = out[0];
  CeedScalar(*stored_values) = out[1];
  CeedScalar(*v)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[2];

  // Context
  const RatelElasticityDamageParams *context = (RatelElasticityDamageParams *)ctx;
  const CeedScalar                   mu      = context->mu;
  const CeedScalar                   bulk    = context->bulk;
  const CeedScalar                   eta     = context->residual_stiffness;

  CeedScalar Grad_u[3][3];
  CeedScalar b_eigenvectors[3][3], b_eigenvalues[3], e_log_eigenvalues[3], s_eigenvalues[3];
  CeedScalar tau_degr_sym[6], tau_degr_eigenvalues[3];
  CeedScalar F_inv[3][3], n_outer3x6[3][6];
  CeedScalar s_sym[6], tau_degr[3][3];

  //-- Compute Grad_u = du/dX * dX/dx
  RatelQdataUnpack(Q, i, q_data, dXdx_initial);
  const CeedScalar dudX[3][3] = {
      {u_g[0][0][i], u_g[1][0][i], u_g[2][0][i]},
      {u_g[0][1][i], u_g[1][1][i], u_g[2][1][i]},
      {u_g[0][2][i], u_g[1][2][i], u_g[2][2][i]}
  };

  RatelMatMatMult(1, dudX, dXdx_initial, Grad_u);

  // Compute F = I + Grad_u
  const CeedScalar F[3][3] = {
      {Grad_u[0][0] + 1, Grad_u[0][1],     Grad_u[0][2]    },
      {Grad_u[1][0],     Grad_u[1][1] + 1, Grad_u[1][2]    },
      {Grad_u[2][0],     Grad_u[2][1],     Grad_u[2][2] + 1}
  };
  const CeedScalar Jm1 = RatelMatDetAM1(Grad_u);

  // Compute e_log = 0.5 ln(b) eigenvalues using log1p of e eigenvalues for numerical stability
  RatelHenckyStrainEigenvalues(Grad_u, b_eigenvalues, b_eigenvectors, e_log_eigenvalues);

  // Compute trace e_log
  CeedScalar trace_e_log = e_log_eigenvalues[0] + e_log_eigenvalues[1] + e_log_eigenvalues[2];

  // Get degraded mu and bulk
  const CeedScalar phi           = u[3][i];
  const CeedScalar one_minus_phi = 1 - phi;
  const CeedScalar g             = one_minus_phi * one_minus_phi * (1 - eta) + eta;
  const CeedScalar bulk_degr     = (trace_e_log > 0 ? g : 1) * bulk;

  // Compute eigenvalues of s, tau, and tau_degr
  for (CeedInt j = 0; j < 3; j++) {
    s_eigenvalues[j]        = 2 * mu * (e_log_eigenvalues[j] - 1 / 3. * trace_e_log);
    tau_degr_eigenvalues[j] = g * s_eigenvalues[j] + bulk_degr * trace_e_log;
  }

  // Compute P_degr
  RatelMatInverse(F, Jm1 + 1, F_inv);
  RatelEigenVectorOuterMult(b_eigenvalues, b_eigenvectors, n_outer3x6);
  RatelMatFromEigensystemSymmetric(s_eigenvalues, n_outer3x6, s_sym);
  RatelMatFromEigensystemSymmetric(tau_degr_eigenvalues, n_outer3x6, tau_degr_sym);
  RatelSymmetricMatUnpack(tau_degr_sym, tau_degr);
  RatelMatMatTransposeMult(1, tau_degr, F_inv, P_degr);

  // Store values
  RatelStoredValuesPack(Q, i, 0, 6, (CeedScalar *)s_sym, stored_values);
  RatelStoredValuesPack(Q, i, 6, 9, (CeedScalar *)Grad_u, stored_values);
  RatelStoredValuesPack(Q, i, 15, 3, (CeedScalar *)tau_degr_eigenvalues, stored_values);
  RatelStoredValuesPack(Q, i, 18, 1, &Jm1, stored_values);
  RatelStoredValuesPack(Q, i, 19, 1, &phi, stored_values);
  CeedScalar value_for_init = 0;
  RatelStoredValuesPack(Q, i, 20, 1, &value_for_init, stored_values);

  // Initialize state variable and damage component to zero
  state_out[i] = value_for_init;
  v[3][i]      = 0;
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute platens jacobian for linear elasticity with damage

  @param[in]   ctx           QFunction context, holding `RatelElasticityDamageParams`
  @param[in]   Q             Number of quadrature points
  @param[in]   i             Index
  @param[in]   in            Input arrays
                             - 0 - volumetric qdata
                             - 2 - Stored values
                             - 3 - du (four components: 3 displacement components, 1 damage field)
                             - 4 - du_g gradient of du with respect to reference coordinates
  @param[out]  out           Output arrays
                             - 0 - initializing dv
  @param[out]  dXdx_initial  dXdx_initial
  @param[out]  dP_degr       Linearization of degraded stress tensor

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int ElasticityDamagePlatenJacobian_HenckyInitialPrincipal(void *ctx, const CeedInt Q, const CeedInt i,
                                                                                const CeedScalar *const *in, CeedScalar *const *out,
                                                                                CeedScalar dXdx_initial[3][3], CeedScalar dP_degr[3][3]) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA]  = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*stored_values)       = in[2];
  const CeedScalar(*du)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[3];
  const CeedScalar(*du_g)[4][CEED_Q_VLA] = (const CeedScalar(*)[4][CEED_Q_VLA])in[4];

  // Outputs
  CeedScalar(*dv)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Context
  const RatelElasticityDamageParams *context     = (RatelElasticityDamageParams *)ctx;
  const CeedScalar                   mu          = context->mu;
  const CeedScalar                   bulk        = context->bulk;
  const CeedScalar                   eta         = context->residual_stiffness;
  const bool                         use_offdiag = context->use_offdiagonal;

  CeedScalar phi = 0.0, Jm1 = 0.0, init_value = 0.0;
  CeedScalar Grad_u[3][3], grad_du[3][3], db_sym[6];
  CeedScalar dF[3][3], dXdx[3][3];
  CeedScalar tau_degr_sym[6], dtau_degr_sym[6], tau_degr[3][3], dtau_degr[3][3], F_inv[3][3];
  CeedScalar tau_degr_F_invT_dFT[3][3], F_invT_dFT[3][3], dtau_degr_minus_tau_degr_F_invT_dFT[3][3];
  CeedScalar b_eigenvalues[3], b_eigenvectors[3][3], tau_degr_eigenvalues[3], e_log_eigenvalues[3], de_log_eigenvalues[3];
  CeedScalar b_eigenvectors_outer3x6[3][6], db_eigenvectors_outer3x6[3][6];
  CeedScalar s_sym[6], tau_sym[6];

  // Unpack stored
  RatelStoredValuesUnpack(Q, i, 0, 6, stored_values, (CeedScalar *)s_sym);
  RatelStoredValuesUnpack(Q, i, 6, 9, stored_values, (CeedScalar *)Grad_u);
  RatelStoredValuesUnpack(Q, i, 15, 3, stored_values, (CeedScalar *)tau_degr_eigenvalues);
  RatelStoredValuesUnpack(Q, i, 18, 1, stored_values, &Jm1);
  RatelStoredValuesUnpack(Q, i, 19, 1, stored_values, &phi);
  RatelStoredValuesUnpack(Q, i, 20, 1, stored_values, &init_value);

  // Compute e_log = 0.5 ln(b) eigenvalues using log1p of e eigenvalues for numerical stability
  RatelHenckyStrainEigenvalues(Grad_u, b_eigenvalues, b_eigenvectors, e_log_eigenvalues);

  //- Compute dF = grad_du = du * dX/dx
  RatelQdataUnpack(Q, i, q_data, dXdx_initial);
  const CeedScalar ddudX[3][3] = {
      {du_g[0][0][i], du_g[1][0][i], du_g[2][0][i]},
      {du_g[0][1][i], du_g[1][1][i], du_g[2][1][i]},
      {du_g[0][2][i], du_g[1][2][i], du_g[2][2][i]}
  };

  RatelMatMatMult(1.0, ddudX, dXdx_initial, dF);

  // Retrieve F = I + Grad_u
  const CeedScalar F[3][3] = {
      {Grad_u[0][0] + 1, Grad_u[0][1],     Grad_u[0][2]    },
      {Grad_u[1][0],     Grad_u[1][1] + 1, Grad_u[1][2]    },
      {Grad_u[2][0],     Grad_u[2][1],     Grad_u[2][2] + 1}
  };

  // Compute grad_du
  RatelMatInverse(F, Jm1 + 1, F_inv);
  RatelMatMatMult(1.0, dXdx_initial, F_inv, dXdx);
  RatelMatMatMult(1.0, ddudX, dXdx, grad_du);

  // Compute b_eigenvectors_outer3x6
  RatelEigenVectorOuterMult(b_eigenvalues, b_eigenvectors, b_eigenvectors_outer3x6);

  // Compute db/dF
  RatelHencky_dbedF_Symmetric(grad_du, b_eigenvalues, b_eigenvectors_outer3x6, db_sym);

  // Compute de/db eigenvalues
  RatelHencky_dedb_Eigenvalues(db_sym, b_eigenvalues, b_eigenvectors, de_log_eigenvalues);

  // Compute trace de_log and trace e_log
  const CeedScalar trace_de_log = de_log_eigenvalues[0] + de_log_eigenvalues[1] + de_log_eigenvalues[2];
  const CeedScalar trace_e_log  = e_log_eigenvalues[0] + e_log_eigenvalues[1] + e_log_eigenvalues[2];

  // Get value of degradation function g(phi)
  const CeedScalar one_minus_phi = 1 - phi;
  const CeedScalar g             = one_minus_phi * one_minus_phi * (1 - eta) + eta;
  const CeedScalar mu_degr       = g * mu;
  const CeedScalar bulk_degr     = (trace_e_log > 0. ? g : 1.) * bulk;

  // Compute dtau_degr
  RatelEigenVectorOuterMult_fwd(db_sym, b_eigenvalues, b_eigenvectors, db_eigenvectors_outer3x6);
  RatelHenckyKirchhoffStressSymmetric_fwd(mu_degr, bulk_degr, trace_de_log, de_log_eigenvalues, tau_degr_eigenvalues, b_eigenvectors_outer3x6,
                                          db_eigenvectors_outer3x6, dtau_degr_sym);

  // Add diagonal term
  const CeedScalar dphi        = du[3][i];
  const CeedScalar Identity[6] = {1., 1., 1., 0., 0., 0.};

  if (trace_e_log > 0) {
    RatelMatMatAddSymmetric(g, s_sym, bulk_degr * trace_e_log, Identity, tau_degr_sym);
  } else {
    RatelMatMatAddSymmetric(g, s_sym, bulk * trace_e_log, Identity, tau_degr_sym);
  }
  RatelSymmetricMatUnpack(tau_degr_sym, tau_degr);

  if (use_offdiag) {
    // Add dtau_degr/dphi = dg/dphi * dphi * tau_degr (if offdiagonal term are considered)
    const CeedScalar dg_dphi = -2 * one_minus_phi * (1 - eta);

    if (trace_e_log > 0) {
      RatelMatMatAddSymmetric(1., s_sym, bulk * trace_e_log, Identity, tau_sym);
      RatelMatMatAddSymmetric(1., dtau_degr_sym, dg_dphi * dphi, tau_sym, dtau_degr_sym);
    } else {
      RatelMatMatAddSymmetric(1., dtau_degr_sym, dg_dphi * dphi, s_sym, dtau_degr_sym);
    }
  }
  RatelSymmetricMatUnpack(dtau_degr_sym, dtau_degr);

  //- Compute df1 = dP = dtau * F^-T - tau * (F^-T * dF^T * F^-T) = (dtau - tau * F^-T * dF^T) * F^-T
  RatelMatTransposeMatTransposeMult(1, F_inv, dF, F_invT_dFT);
  RatelMatMatMult(1., tau_degr, F_invT_dFT, tau_degr_F_invT_dFT);
  RatelMatMatAdd(1., dtau_degr, -1., tau_degr_F_invT_dFT, dtau_degr_minus_tau_degr_F_invT_dFT);
  RatelMatMatTransposeMult(1, dtau_degr_minus_tau_degr_F_invT_dFT, F_inv, dP_degr);
  // Initialize damage component to zero
  dv[3][i] = 0;
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute platen residual for Hencky hyperelasticity initial conf.

  @param[in]   ctx  QFunction context, holding `RatelBCPlatenParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(PlatenBCsResidualElasticityDamage_HenckyInitialPrincipal)
(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return PlatenBCs(ctx, Q, ElasticityDamagePlatenResidual_HenckyInitialPrincipal, !!NUM_COMPONENTS_STATE_ElasticityHenckyDamageInitialPrincipal,
                   !!NUM_COMPONENTS_STORED_ElasticityHenckyDamageInitialPrincipal, NUM_ACTIVE_FIELD_EVAL_MODES_ElasticityHenckyDamageInitialPrincipal,
                   in, out);
}

/**
  @brief Evaluate platen Jacobian for Hencky hyperelasticity initial conf.

  @param[in]   ctx  QFunction context, holding `RatelBCPlatenParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(PlatenBCsJacobianElasticityDamage_HenckyInitialPrincipal)
(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return PlatenBCs_Jacobian(
      ctx, Q, ElasticityDamagePlatenJacobian_HenckyInitialPrincipal, !!NUM_COMPONENTS_STATE_ElasticityHenckyDamageInitialPrincipal,
      !!NUM_COMPONENTS_STORED_ElasticityHenckyDamageInitialPrincipal, NUM_ACTIVE_FIELD_EVAL_MODES_ElasticityHenckyDamageInitialPrincipal, in, out);
}

/// @}
