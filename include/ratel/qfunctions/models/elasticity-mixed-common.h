/// @file
/// Common components for mixed elasticity QFunctions
#pragma once

#include <ceed/types.h>

#include "../../models/common-parameters.h"  // IWYU pragma: export
#include "../utils.h"                        // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

typedef int (*RatelComputef1_Mixed)(void *, CeedInt, CeedInt, const CeedScalar *const *, CeedScalar *const *, CeedScalar[3][3], CeedScalar[3][3],
                                    CeedScalar *);
typedef int (*RatelComputedf1_fwd_Mixed)(void *, CeedInt, CeedInt, const CeedScalar *const *, CeedScalar *const *, CeedScalar[3][3], CeedScalar[3][3],
                                         CeedScalar *, CeedScalar *);
typedef int (*RatelComputeg0_Mixed)(void *, CeedInt, CeedInt, const CeedScalar *const *, const CeedScalar, CeedScalar *const *, CeedScalar *);
typedef int (*RatelComputedg0_fwd_Mixed)(void *, CeedInt, CeedInt, const CeedScalar *const *, const CeedScalar, const CeedScalar, CeedScalar *const *,
                                         CeedScalar *);

/**
  @brief Compute mixed elasticity residual evaluation

  @param[in]   ctx                          QFunction context, unused
  @param[in]   Q                            Number of quadrature points
  @param[in]   compute_f1_mixed             Function to compute action of f1
  @param[in]   compute_g0_mixed             Function to compute action of g0
  @param[in]   has_state_values             Boolean flag indicating model state values in residual evaluation
  @param[in]   has_stored_values            Boolean flag indicating model stores values in residual evaluation
  @param[in]   num_active_field_eval_modes  Number of active field evaluation modes
  @param[in]   in                           Input arrays
                                              - 0 - volumetric qdata
  @param[out]  out                          Output array
                                              - `output_data_offset`     - action of QFunction on displacement
                                              - `output_data_offset + 1` - action of QFunction on pressure

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int MixedElasticityResidual(void *ctx, CeedInt Q, RatelComputef1_Mixed compute_f1_mixed, RatelComputeg0_Mixed compute_g0_mixed,
                                                  bool has_state_values, bool has_stored_values, CeedInt num_active_field_eval_modes,
                                                  const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];

  // Outputs
  const CeedInt output_data_offset = has_state_values + has_stored_values;
  CeedScalar(*dvdX)[3][CEED_Q_VLA] = (CeedScalar(*)[3][CEED_Q_VLA])out[output_data_offset];
  CeedScalar(*q)                   = out[output_data_offset + 1];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar dXdx[3][3], f1[3][3], g0, scaler_1;

    // Qdata
    const CeedScalar wdetJ = q_data[0][i];

    // Coordinate transformation f1, and g0
    // For linear elasticity f1 = sigma, where sigma is Cauchy stress tensor
    // For hyperelastic in initial configuration f1 = P, where P is First Piola Kirchhoff stress
    // For hyperelastic in current configuration f1 = tau, where tau is Kirchhoff stress
    // For mixed linear scalar_1 = div(u) = trace_e
    // For mixed hyperelastic scalar_1 = Jm1
    compute_f1_mixed(ctx, Q, i, in, out, dXdx, f1, &scaler_1);
    compute_g0_mixed(ctx, Q, i, in, scaler_1, out, &g0);

    // Apply dXdx^T and weight
    RatelMatMatTransposeMultAtQuadraturePoint(Q, i, wdetJ, dXdx, f1, dvdX);
    q[i] = g0 * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute mixed elasticity Jacobian evaluation

  @param[in]   ctx                          QFunction context, holding common parameters and model parameters
  @param[in]   Q                            Number of quadrature points
  @param[in]   compute_df1_mixed            Function to compute action of df1
  @param[in]   compute_dg0_mixed            Function to compute action of dg0
  @param[in]   has_state_values             Boolean flag indicating model state values in residual evaluation
  @param[in]   has_stored_values            Boolean flag indicating model stores values in residual evaluation
  @param[in]   num_active_field_eval_modes  Number of active field evaluation modes
  @param[in]   in                           Input arrays
                                              - 0                   - volumetric qdata
                                              - `input_data_offset` - incremental change in u
  @param[out]  out                          Output array
                                              - 0 - action of QFunction on displacement
                                              - 1 - action of QFunction on pressure
                                              - 2 - action of QFunction dynamic solver term

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int MixedElasticityJacobian(void *ctx, CeedInt Q, RatelComputedf1_fwd_Mixed compute_df1_mixed,
                                                  RatelComputedg0_fwd_Mixed compute_dg0_mixed, bool has_state_values, bool has_stored_values,
                                                  CeedInt num_active_field_eval_modes, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedInt input_data_offset       = 1 + has_state_values + has_stored_values + num_active_field_eval_modes;
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar *du                  = in[input_data_offset];

  // Outputs
  CeedScalar(*ddvdX)[3][CEED_Q_VLA] = (CeedScalar(*)[3][CEED_Q_VLA])out[0];
  CeedScalar *dq                    = out[1];
  CeedScalar *dv                    = out[2];

  // Context
  const CeedScalar *context     = (CeedScalar *)ctx;
  const CeedScalar  rho         = context[RATEL_COMMON_PARAMETER_RHO];
  const CeedScalar  shift_a     = context[RATEL_COMMON_PARAMETER_SHIFT_A];
  const bool        has_shift_a = shift_a != 0.0;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar dXdx[3][3], df1[3][3], dg0, scalar_1, scalar_2;

    // Qdata
    const CeedScalar wdetJ = q_data[0][i];

    // Coordinate transformation and linearized of f1 and g0
    // For linear elasticity df1 = dsigma;
    // For hyperelastic in initial configuration df1 = dP
    // For hyperelastic in current configuration df1 = dtau - tau * grad_du^T
    // For mixed linear scalar_1 = div(du) = trace_de and scalar_2 = 0.
    // For mixed hyperelastic scalar_1 = Jm1 and scalar_2 = C_inv:dE
    compute_df1_mixed(ctx, Q, i, in, out, dXdx, df1, &scalar_1, &scalar_2);
    compute_dg0_mixed(ctx, Q, i, in, scalar_1, scalar_2, out, &dg0);

    // Apply dXdx^T and weight
    RatelMatMatTransposeMultAtQuadraturePoint(Q, i, wdetJ, dXdx, df1, ddvdX);
    dq[i] = dg0 * wdetJ;
  }  // End of Quadrature Point Loop

  // Second Quadrature Point Loop for dynamic solver
  if (has_shift_a) {
    CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
      // Qdata
      const CeedScalar wdetJ = q_data[0][i];

      // Apply scaled mass matrix
      RatelScaledMassApplyAtQuadraturePoint(Q, i, 3, shift_a * rho * wdetJ, du, dv);
    }  // End of Quadrature Point Loop
  }
  return CEED_ERROR_SUCCESS;
}

/// @}
