
/// @file
/// Ratel plasticity return mapping algorithm(s) QFunction source
#pragma once

#include <ceed/types.h>

#include "../utils.h"  // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

/**
  @brief return mapping update of deviatoric elastic strain

  @param[in]   mu                   mu
  @param[in]   q_tr                 trial von Mises stress
  @param[in]   delta_gamma          increment plastic multiplier
  @param[out]  e_el_dev_tr_sym      deviatoric elastic strain in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelReturnMapping_vonMises(const CeedScalar mu, const CeedScalar q_tr, const CeedScalar delta_gamma,
                                                      CeedScalar e_el_dev_tr_sym[6]) {
  // Update elastic deviatoric component of strain
  for (CeedInt j = 0; j < 6; j++) {
    e_el_dev_tr_sym[j] -= (3. * mu * delta_gamma / q_tr) * e_el_dev_tr_sym[j];
  }

  return CEED_ERROR_SUCCESS;
}

/**
  @brief Increment in updated deviatoric elastic strain

  @param[in]  mu               mu
  @param[in]  hardening_slope  strain hardening slope
  @param[in]  q_tr             trial von Mises stress
  @param[in]  delta_gamma      increment plastic multiplier
  @param[in]  dq_tr            trial dphi = dq_tr
  @param[in]  e_el_dev_tr      deviatoric elastic strain in symmetric representation
  @param[out] de_el_dev_tr     variation of deviatoric elastic strain in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelReturnMapping_vonMises_fwd(const CeedScalar mu, const CeedScalar hardening_slope, const CeedScalar q_tr,
                                                          const CeedScalar delta_gamma, const CeedScalar dq_tr, const CeedScalar e_el_dev_tr[6],
                                                          CeedScalar de_el_dev_tr[6]) {
  const CeedScalar factor           = 1. / (3. * mu + hardening_slope);
  const CeedScalar dq_tr_q_tr       = dq_tr / q_tr;
  const CeedScalar delta_gamma_q_tr = delta_gamma / q_tr;

  for (CeedInt j = 0; j < 6; j++) {
    // Update the elastic part of dE
    de_el_dev_tr[j] = (1 - 3 * mu * delta_gamma_q_tr) * de_el_dev_tr[j] - 3 * mu * dq_tr_q_tr * (factor - delta_gamma_q_tr) * e_el_dev_tr[j];
  }
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_ReturnMapping_vonMisesLinear_fwd 65

/**
  @brief return mapping update of deviatoric stress in principal directions

  @param[in]   mu                   mu
  @param[in]   q_tr                 trial von Mises stress
  @param[in]   delta_gamma          increment plastic multiplier
  @param[out]  s_tr_eigenvalues     eigenvalues of deviatoric stress

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelReturnMapping_vonMises_Principal(const CeedScalar mu, const CeedScalar q_tr, const CeedScalar delta_gamma,
                                                                CeedScalar s_tr_eigenvalues[3]) {
  // Update s_tr_eigenvalues
  for (CeedInt j = 0; j < 3; j++) {
    s_tr_eigenvalues[j] -= (3. * mu * delta_gamma / q_tr) * s_tr_eigenvalues[j];
  }

  return CEED_ERROR_SUCCESS;
}

/**
  @brief Variation of updated deviatoric stress in principal directions

  @param[in]   mu                 mu
  @param[in]   hardening_slope    hardening slope
  @param[in]   delta_gamma        delta gamma
  @param[in]   s_tr_eigenvalues   eigenvalues of trial deviatoric stress
  @param[out]  ds_tr_eigenvalues  increment of eigenvalues of deviatoric stress

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelReturnMapping_vonMises_Principal_fwd(const CeedScalar mu, const CeedScalar hardening_slope,
                                                                    const CeedScalar delta_gamma, const CeedScalar s_tr_eigenvalues[3],
                                                                    CeedScalar ds_tr_eigenvalues[3]) {
  CeedScalar s_tr_contract = 0.0, s_tr_ds_tr_contract = 0.0;

  // Compute quantities needed for update
  for (int i = 0; i < 3; i++) {
    s_tr_contract += s_tr_eigenvalues[i] * s_tr_eigenvalues[i];
    s_tr_ds_tr_contract += s_tr_eigenvalues[i] * ds_tr_eigenvalues[i];
  }
  const CeedScalar q_tr             = sqrt(3. / 2. * s_tr_contract);
  const CeedScalar factor           = 1. / (3. * mu + hardening_slope);
  const CeedScalar dq_tr            = 1.5 * s_tr_ds_tr_contract / q_tr;
  const CeedScalar dq_tr_q_tr       = dq_tr / q_tr;
  const CeedScalar delta_gamma_q_tr = delta_gamma / q_tr;

  // Update ds_tr_eigenvalues
  for (CeedInt j = 0; j < 3; j++) {
    ds_tr_eigenvalues[j] =
        (1 - 3 * mu * delta_gamma_q_tr) * ds_tr_eigenvalues[j] - 3 * mu * dq_tr_q_tr * (factor - delta_gamma_q_tr) * s_tr_eigenvalues[j];
  }
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_ReturnMapping_vonMisesLinearPricipal_fwd 54

/**
  @brief Compute stress required to sustain plastic yielding (flow stress)

  @param[in]   yield_stress          yield stress
  @param[in]   hardening_parameters  strain hardening parameter H
  @param[in]   accumulated_plastic   accumulated plastic strain
  @param[out]  flow_stress           deviatoric elastic strain in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelComputeFlowStress(const CeedScalar yield_stress, const CeedScalar hardening_parameters[3],
                                                 const CeedScalar accumulated_plastic, CeedScalar *flow_stress) {
  *flow_stress = yield_stress + hardening_parameters[0] * accumulated_plastic +
                 (hardening_parameters[1] - yield_stress) * (-expm1(-hardening_parameters[2] * accumulated_plastic));

  return CEED_ERROR_SUCCESS;
}

/**
  @brief Increment in flow stress, i.e. hardening slope

  @param[in]   yield_stress          yield stress
  @param[in]   hardening_parameters  array of strain hardening parameters
  @param[in]   accumulated_plastic   accumulated plastic strain
  @param[out]  hardening_slope       hardening slope

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelComputeFlowStress_fwd(const CeedScalar yield_stress, const CeedScalar hardening_parameters[3],
                                                     const CeedScalar accumulated_plastic, CeedScalar *hardening_slope) {
  *hardening_slope = hardening_parameters[0] +
                     (hardening_parameters[1] - yield_stress) * hardening_parameters[2] * (expm1(-hardening_parameters[2] * accumulated_plastic) + 1);

  return CEED_ERROR_SUCCESS;
}

/**
  @brief compute delta gamma and hardening slope as part of the return mapping

  @param[in]    yield_stress          yield_stress
  @param[in]    hardening_parameters  array of strain hardening parameters
  @param[in]    mu                    mu
  @param[in]    q_tr                  trial von Mises stress
  @param[in]    phi_tr                trial phi = q_tr - sigma_y
  @param[in]    accumulated_plastic   accumulated plastic strain
  @param[in]    phi_tol               tolerance for return mapping
  @param[out]   hardening_slope       hardening slope
  @param[out]   delta_gamma           increment plastic multiplier

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelComputeDeltaGamma_vonMises(const CeedScalar yield_stress, const CeedScalar hardening_parameters[3],
                                                          const CeedScalar mu, const CeedScalar q_tr, CeedScalar phi_tr,
                                                          const CeedScalar accumulated_plastic, const CeedScalar phi_tol, CeedScalar *hardening_slope,
                                                          CeedScalar *delta_gamma) {
  const CeedInt max_iter    = 10;
  CeedScalar    flow_stress = 0.0;

  for (CeedInt iter = 0; iter < max_iter; ++iter) {
    // Compute trial hardening_slope
    RatelComputeFlowStress_fwd(yield_stress, hardening_parameters, accumulated_plastic, hardening_slope);

    // Compute trial delta_gamma
    *delta_gamma += phi_tr / (3 * mu + *hardening_slope);

    // Update flow stress
    RatelComputeFlowStress(yield_stress, hardening_parameters, accumulated_plastic + *delta_gamma, &flow_stress);

    // Update phi_tr
    phi_tr = q_tr - 3 * mu * *delta_gamma - flow_stress;

    // Check yield condition
    if (fabs(phi_tr) <= phi_tol * flow_stress) {
      break;
    }
  }
  return CEED_ERROR_SUCCESS;
}

/// @}
