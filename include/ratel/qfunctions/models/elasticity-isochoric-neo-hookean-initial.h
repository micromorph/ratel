/// @file
/// Ratel isochoric neo-Hookean hyperelasticity at finite strain in initial configuration QFunction source
#include <ceed/types.h>

#include "../../models/neo-hookean.h"                 // IWYU pragma: export
#include "../boundaries/platen-nitsche.h"             // IWYU pragma: export
#include "../utils.h"                                 // IWYU pragma: export
#include "elasticity-common.h"                        // IWYU pragma: export
#include "elasticity-isochoric-neo-hookean-common.h"  // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

#define NUM_COMPONENTS_STATE_IsochoricNeoHookeanInitial 0
#define NUM_COMPONENTS_STORED_IsochoricNeoHookeanInitial 9
#define NUM_ACTIVE_FIELD_EVAL_MODES_IsochoricNeoHookeanInitial 1

// FLOPS_grad_du = FLOPS_MatMatMult
// 3 flops for creating F
#define FLOPS_df1_IsochoricNH_Initial                                                                                                              \
  (FLOPS_MatMatMult + 3 + FLOPS_MatDetAM1 + FLOPS_GreenEulerStrain + FLOPS_CInverse + FLOPS_S_IsochoricNeoHookean + FLOPS_dS_IsochoricNeoHookean + \
   FLOPS_MatMatMultPlusMatMatMult)
#define FLOPS_JACOBIAN_IsochoricNeoHookeanInitial (FLOPS_df1_IsochoricNH_Initial + FLOPS_dXdxwdetJ)

/**
  @brief Compute `P` for isochoric neo-Hookean hyperelasticity in initial configuration

  @param[in]   ctx                   QFunction context, holding `RatelNeoHookeanElasticityParams`
  @param[in]   Q                     Number of quadrature points
  @param[in]   i                     Current quadrature point
  @param[in]   in                    Input arrays
                                       - 0 - volumetric qdata
                                       - 1 - gradient of u with respect to reference coordinates
  @param[out]  out                   Output arrays
                                       - 0 - stored gradient of u with respect to physical coordinates
  @param[out]  dXdx                  Coordinate transformation
  @param[out]  f1                    `f1 = P`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int f1_IsochoricNeoHookeanInitial(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in,
                                                        CeedScalar *const *out, CeedScalar dXdx[3][3], CeedScalar f1[3][3]) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*grad_u)[3][CEED_Q_VLA] = (CeedScalar(*)[3][CEED_Q_VLA])out[0];

  // Context
  const RatelNeoHookeanElasticityParams *context = (RatelNeoHookeanElasticityParams *)ctx;
  const CeedScalar                       bulk    = context->bulk;
  const CeedScalar                       two_mu  = context->two_mu;

  CeedScalar E_sym[6], C_inv_sym[6], S_sym[6], S[3][3], J_dVdJ;

  // Qdata
  RatelQdataUnpack(Q, i, q_data, dXdx);

  // Read spatial derivatives of u; du/dX
  CeedScalar dudX[3][3];
  RatelGradUnpack(Q, i, ug, dudX);

  // Compute grad_u = du/dX * dX/dx
  // X is ref coordinate [-1,1]^3; x is physical coordinate in initial configuration
  RatelMatMatMultAtQuadraturePoint(Q, i, 1.0, dudX, dXdx, grad_u);

  // Compute the Deformation Gradient : F = I + grad_u
  const CeedScalar F[3][3] = {
      {grad_u[0][0][i] + 1, grad_u[0][1][i],     grad_u[0][2][i]    },
      {grad_u[1][0][i],     grad_u[1][1][i] + 1, grad_u[1][2][i]    },
      {grad_u[2][0][i],     grad_u[2][1][i],     grad_u[2][2][i] + 1}
  };
  const CeedScalar temp_grad_u[3][3] = {
      {grad_u[0][0][i], grad_u[0][1][i], grad_u[0][2][i]},
      {grad_u[1][0][i], grad_u[1][1][i], grad_u[1][2][i]},
      {grad_u[2][0][i], grad_u[2][1][i], grad_u[2][2][i]}
  };

  // Compute J - 1
  const CeedScalar Jm1 = RatelMatDetAM1(temp_grad_u);

  // Compute E, C^{-1}, S
  RatelGreenLagrangeStrain(temp_grad_u, E_sym);
  RatelCInverse(E_sym, Jm1, C_inv_sym);
  VolumetricFunctionAndDerivatives(Jm1, NULL, &J_dVdJ, NULL);
  RatelSecondKirchhoffStress_IsochoricNeoHookean(J_dVdJ, bulk, two_mu, Jm1, C_inv_sym, E_sym, S_sym);
  RatelSymmetricMatUnpack(S_sym, S);

  // Compute the First Piola-Kirchhoff f1 = P = F*S
  RatelMatMatMult(1.0, F, S, f1);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute linearization of `P` for isochoric neo-Hookean hyperelasticity in initial configuration

  @param[in]   ctx                   QFunction context, holding `RatelNeoHookeanElasticityParams`
  @param[in]   Q                     Number of quadrature points
  @param[in]   i                     Current quadrature point
  @param[in]   in                    Input arrays
                                       - 0 - volumetric qdata
                                       - 1 - stored gradient of u with respect to physical coordinates
                                       - 2 - gradient of incremental change to u with respect to reference coordinates
  @param[out]  out                   Output arrays, unused
  @param[out]  dXdx                  Coordinate transformation
  @param[out]  df1                   `df1 = dP`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int df1_IsochoricNeoHookeanInitial(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in,
                                                         CeedScalar *const *out, CeedScalar dXdx[3][3], CeedScalar df1[3][3]) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA]    = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*grad_u)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];
  const CeedScalar(*dug)[3][CEED_Q_VLA]    = (const CeedScalar(*)[3][CEED_Q_VLA])in[2];

  // Context
  const RatelNeoHookeanElasticityParams *context = (RatelNeoHookeanElasticityParams *)ctx;
  const CeedScalar                       mu      = context->mu;
  const CeedScalar                       bulk    = context->bulk;
  const CeedScalar                       two_mu  = context->two_mu;

  CeedScalar grad_du[3][3], E_sym[6], C_inv_sym[6], S_sym[6], S[3][3], dS_sym[6], dS[3][3], J_dVdJ, J2_d2VdJ2;

  // Qdata
  RatelQdataUnpack(Q, i, q_data, dXdx);

  // Read spatial derivatives of increment u; d(du)/dX = d(du)/dX
  CeedScalar ddudX[3][3];
  RatelGradUnpack(Q, i, dug, ddudX);

  // Compute grad_du = ddu/dX * dX/dx
  // X is ref coordinate [-1,1]^3; x is physical coordinate
  RatelMatMatMult(1.0, ddudX, dXdx, grad_du);

  // Deformation Gradient : F = I + grad_u
  const CeedScalar F[3][3] = {
      {grad_u[0][0][i] + 1, grad_u[0][1][i],     grad_u[0][2][i]    },
      {grad_u[1][0][i],     grad_u[1][1][i] + 1, grad_u[1][2][i]    },
      {grad_u[2][0][i],     grad_u[2][1][i],     grad_u[2][2][i] + 1}
  };
  const CeedScalar temp_grad_u[3][3] = {
      {grad_u[0][0][i], grad_u[0][1][i], grad_u[0][2][i]},
      {grad_u[1][0][i], grad_u[1][1][i], grad_u[1][2][i]},
      {grad_u[2][0][i], grad_u[2][1][i], grad_u[2][2][i]}
  };

  // Compute J - 1
  const CeedScalar Jm1 = RatelMatDetAM1(temp_grad_u);

  // Compute E, C^{-1}, S
  RatelGreenLagrangeStrain(temp_grad_u, E_sym);
  RatelCInverse(E_sym, Jm1, C_inv_sym);
  VolumetricFunctionAndDerivatives(Jm1, NULL, &J_dVdJ, &J2_d2VdJ2);
  RatelSecondKirchhoffStress_IsochoricNeoHookean(J_dVdJ, bulk, two_mu, Jm1, C_inv_sym, E_sym, S_sym);
  RatelSymmetricMatUnpack(S_sym, S);

  // dS = dS/dE:dE
  RatelSecondKirchhoffStress_IsochoricNeoHookean_fwd(J_dVdJ, J2_d2VdJ2, bulk, mu, Jm1, F, grad_du, C_inv_sym, E_sym, dS_sym);
  RatelSymmetricMatUnpack(dS_sym, dS);

  // df1 = dP = dP/dF:dF = dF*S + F*dS; note dF = grad_du
  RatelMatMatMultPlusMatMatMult(grad_du, S, F, dS, df1);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute residual for isochoric neo-Hookean hyperelasticity in initial configuration

  @param[in]   ctx  QFunction context, holding `RatelNeoHookeanElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityResidual_IsochoricNeoHookeanInitial)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return ElasticityResidual(ctx, Q, f1_IsochoricNeoHookeanInitial, !!NUM_COMPONENTS_STATE_IsochoricNeoHookeanInitial,
                            !!NUM_COMPONENTS_STORED_IsochoricNeoHookeanInitial, NUM_ACTIVE_FIELD_EVAL_MODES_IsochoricNeoHookeanInitial, in, out);
}

/**
  @brief Evaluate Jacobian for isochoric neo-Hookean hyperelasticity in initial configuration

  @param[in]   ctx  QFunction context, holding `RatelNeoHookeanElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityJacobian_IsochoricNeoHookeanInitial)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return ElasticityJacobian(ctx, Q, df1_IsochoricNeoHookeanInitial, !!NUM_COMPONENTS_STATE_IsochoricNeoHookeanInitial,
                            !!NUM_COMPONENTS_STORED_IsochoricNeoHookeanInitial, NUM_ACTIVE_FIELD_EVAL_MODES_IsochoricNeoHookeanInitial, in, out);
}

/**
  @brief Compute platen residual for isochoric neo-Hookean hyperelasticity in initial configuration

  @param[in]   ctx  QFunction context, holding `RatelBCPlatenParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(PlatenBCsResidual_IsochoricNeoHookeanInitial)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return PlatenBCs(ctx, Q, f1_IsochoricNeoHookeanInitial, !!NUM_COMPONENTS_STATE_IsochoricNeoHookeanInitial,
                   !!NUM_COMPONENTS_STORED_IsochoricNeoHookeanInitial, NUM_ACTIVE_FIELD_EVAL_MODES_IsochoricNeoHookeanInitial, in, out);
}

/**
  @brief Evaluate platen Jacobian for isochoric neo-Hookean hyperelasticity in initial configuration

  @param[in]   ctx  QFunction context, holding `RatelBCPlatenParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(PlatenBCsJacobian_IsochoricNeoHookeanInitial)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return PlatenBCs_Jacobian(ctx, Q, df1_IsochoricNeoHookeanInitial, !!NUM_COMPONENTS_STATE_IsochoricNeoHookeanInitial,
                            !!NUM_COMPONENTS_STORED_IsochoricNeoHookeanInitial, NUM_ACTIVE_FIELD_EVAL_MODES_IsochoricNeoHookeanInitial, in, out);
}

/// @}
