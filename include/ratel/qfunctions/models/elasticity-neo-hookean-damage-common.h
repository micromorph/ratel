/// @file
/// Ratel Neo-Hookean (initial configuration) with damage phase field
#pragma once

#include <ceed/types.h>
#ifndef CEED_RUNNING_JIT_PASS
#include <math.h>
#endif

#include "../../models/elasticity-damage.h"           // IWYU pragma: export
#include "../utils.h"                                 // IWYU pragma: export
#include "elasticity-isochoric-neo-hookean-common.h"  // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

#define NUM_COMPONENTS_DIAGNOSTIC_NeoHookean_Damage_Dual 2
#define NUM_COMPONENTS_DIAGNOSTIC_NeoHookean_Damage 19
#define NUM_COMPONENTS_STATE_NeoHookean_Damage 1  // 1 Psi_plus
#define NUM_ACTIVE_FIELD_EVAL_MODES_NeoHookean_Damage 2
#define NUM_U_t_FIELD_EVAL_MODES_NeoHookean_Damage 1

/**
  @brief Compute Psi_plus for Neo-hookean hyperelasticity

  @param[in]   V             Volumetric term
  @param[in]   mu            shear modulus
  @param[in]   bulk          bulk modulus
  @param[in]   Jm1           J - 1
  @param[in]   trace_strain  Trace of Green Lagrange or Green euler strain tensor (E or e)
  @param[out]  Psi_plus      Psi^+

  @return Psi_plus
**/
CEED_QFUNCTION_HELPER int PsiPlus_NeoHookean(CeedScalar V, CeedScalar mu, CeedScalar bulk, CeedScalar Jm1, CeedScalar trace_strain,
                                             CeedScalar *Psi_plus) {
  // Compute Psi_vol. If J < 1, Ps_vol = 0
  const CeedScalar Psi_vol = (Jm1 > 0) * bulk * V;

  // Compute Psi_iso
  CeedScalar Psi_iso;
  RatelStrainEnergy_IsochoricNeoHookean(0.0, bulk, mu, Jm1, trace_strain, &Psi_iso);

  // Compute Psi_plus = Psi_iso + Psi_vol
  *Psi_plus = Psi_iso + Psi_vol;
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_PsiPlus_NeoHookean (FLOPS_MatTraceSymmetric + FLOPS_StrainEnergy_IsochoricNeoHookean + 2)

/**
  @brief Compute Linearization of Psi_plus for Neo-hookean hyperelasticity initial conf.

  @param[in]   J_dVdJ    J dV/dJ
  @param[in]   two_mu    2 mu (shear modulus)
  @param[in]   bulk      bulk modulus
  @param[in]   Jm1       J - 1
  @param[in]   E_sym     (Green-Lagrange) strain tensor in symmetric representation
  @param[in]   dE_sym    strain tensor increment in symmetric representation
  @param[out]  dPsi_plus dPsi^+

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int PsiPlus_NeoHookeanInitial_fwd(CeedScalar J_dVdJ, CeedScalar two_mu, CeedScalar bulk, CeedScalar Jm1,
                                                        const CeedScalar E_sym[6], const CeedScalar dE_sym[6], CeedScalar *dPsi_plus) {
  CeedScalar C_inv_sym[6], S_iso_sym[6], S_vol_sym[6], S_sym[6];

  // C^{-1}
  RatelCInverse(E_sym, Jm1, C_inv_sym);

  // Compute S_iso
  RatelIsochoricSecondKirchhoffStress_NeoHookean(two_mu, Jm1 + 1, C_inv_sym, E_sym, S_iso_sym);

  // Compute dPsi_plus (offdiagonal term) = S : dE if J >=1 , else S_iso : dE
  RatelVolumetricSecondKirchhoffStress(J_dVdJ, bulk, C_inv_sym, S_vol_sym);
  RatelMatMatAddSymmetric(Jm1 > 0, S_vol_sym, 1, S_iso_sym, S_sym);
  *dPsi_plus = RatelMatMatContractSymmetric(1., S_sym, dE_sym);
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_PsiPlus_NeoHookeanInitial_fwd \
  (FLOPS_GreenLagrangeStrain + FLOPS_CInverse + FLOPS_S_IsochoricNeoHookean + FLOPS_S_vol + FLOPS_MatMatAddSymmetric + FLOPS_MatMatContractSymmetric)

/**
  @brief Compute degraded second Kirchhoff stress (S_degr) for Neo-hookean hyperelasticity initial conf.

  @param[in]   J_dVdJ      J dV/dJ
  @param[in]   mu          shear modulus
  @param[in]   bulk        bulk modulus
  @param[in]   eta         residual stiffness
  @param[in]   phi         damage field
  @param[in]   Jm1         trace strain tensor
  @param[in]   E_sym       (Green-Lagrange) strain tensor in symmetric representation
  @param[out]  S_degr_sym  degraded second Kirchhoff stress in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int ComputeDegradedSecondKirchhoffStress_NeoHookean(CeedScalar J_dVdJ, CeedScalar mu, CeedScalar bulk, CeedScalar eta,
                                                                          CeedScalar phi, CeedScalar Jm1, const CeedScalar E_sym[6],
                                                                          CeedScalar S_degr_sym[6]) {
  CeedScalar C_inv_sym[6], S_iso_sym[6], S_vol_sym[6];

  // Get value of degradation function g(phi) and degraded bulk and mu (depending on Jm1 sign)
  const CeedScalar one_minus_phi = 1 - phi;
  const CeedScalar g             = one_minus_phi * one_minus_phi * (1 - eta) + eta;
  const bool       J_positive    = Jm1 > 0;
  const CeedScalar bulk_deg      = (J_positive ? g : 1) * bulk;
  const CeedScalar mu_degr       = mu * g;

  // C^{-1}
  RatelCInverse(E_sym, Jm1, C_inv_sym);

  // Compute g(phi) * S_iso and (Jm1>0 ? g(phi) : 1) * S_vol (degraded)
  RatelIsochoricSecondKirchhoffStress_NeoHookean(2 * mu_degr, Jm1 + 1, C_inv_sym, E_sym, S_iso_sym);
  RatelVolumetricSecondKirchhoffStress(J_dVdJ, bulk_deg, C_inv_sym, S_vol_sym);

  // Compute S_degr
  RatelMatMatAddSymmetric(1, S_vol_sym, 1, S_iso_sym, S_degr_sym);
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_Degraded_S_NeoHookean (7 + FLOPS_CInverse + FLOPS_S_IsochoricNeoHookean + FLOPS_S_vol + FLOPS_MatMatAddSymmetric)

/**
  @brief Compute linearization of Kirchhoff stress (S_degr) for Neo-hookean hyperelasticity initial conf.

  @param[in]   J_dVdJ       J dV/dJ
  @param[in]   J2_d2VdJ2    J^2 d^2V/dJ^2
  @param[in]   two_mu       2 mu
  @param[in]   bulk         bulk modulus
  @param[in]   eta          residual stiffness
  @param[in]   use_offdiag  offdiagonal term (bool)
  @param[in]   phi          damage field
  @param[in]   Jm1          J - 1
  @param[in]   F            deformation gradient
  @param[in]   grad_du      gradient of u increment
  @param[in]   E_sym        (Green-Lagrange) strain tensor in symmetric representation
  @param[in]   dphi         increment of phi
  @param[out]  dS_degr_sym  linearization of degraded stress in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int ComputeDegradedSecondKirchhoffStress_NeoHookean_fwd(CeedScalar J_dVdJ, CeedScalar J2_d2VdJ2, CeedScalar two_mu,
                                                                              CeedScalar bulk, CeedScalar eta, bool use_offdiag, CeedScalar phi,
                                                                              CeedScalar Jm1, const CeedScalar F[3][3],
                                                                              const CeedScalar grad_du[3][3], const CeedScalar E_sym[6],
                                                                              CeedScalar dphi, CeedScalar dS_degr_sym[6]) {
  CeedScalar dE_sym[6], dC_inv_sym[6], C_inv_sym[6], S_iso_sym[6], S_vol_sym[6];
  CeedScalar dS_iso_sym[6], dS_vol_sym[6], Cinv_contract_dE, dS_degr_sym2[6];

  // Compute degradation function g(phi) and degraded bulk and mu (depending on Jm1 sign)
  const CeedScalar one_minus_phi = 1 - phi;
  const CeedScalar g             = one_minus_phi * one_minus_phi * (1 - eta) + eta;
  const bool       J_positive    = Jm1 > 0;
  const CeedScalar bulk_degr     = (J_positive ? g : 1) * bulk;
  const CeedScalar two_mu_degr   = two_mu * g;
  const CeedScalar bulk_factor   = (J_positive ? 1 : 0) * bulk;

  // Compute C^{-1} and dC_inv = -2*C_inv*dE*C_inv
  RatelCInverse(E_sym, Jm1, C_inv_sym);
  RatelGreenLagrangeStrain_fwd(grad_du, F, dE_sym);
  RatelCInverse_fwd(C_inv_sym, dE_sym, dC_inv_sym);

  // Compute S_iso and (Jm1>0 ? 1 : 0) * S_vol
  RatelIsochoricSecondKirchhoffStress_NeoHookean(two_mu, Jm1 + 1, C_inv_sym, E_sym, S_iso_sym);
  RatelVolumetricSecondKirchhoffStress(bulk_factor, Jm1, C_inv_sym, S_vol_sym);

  // Compute g(phi) * dS_iso and (Jm1>0 ? g(phi) : 1) * dS_vol
  RatelIsochoricSecondKirchhoffStress_NeoHookean_fwd(0.5 * two_mu_degr, Jm1 + 1., F, grad_du, C_inv_sym, E_sym, dC_inv_sym, dS_iso_sym,
                                                     &Cinv_contract_dE);
  RatelVolumetricSecondKirchhoffStress_fwd(J_dVdJ, J2_d2VdJ2, bulk_degr, Cinv_contract_dE, C_inv_sym, dC_inv_sym, dS_vol_sym);
  // Compute dS_degr
  RatelMatMatAddSymmetric(1, dS_vol_sym, 1, dS_iso_sym, dS_degr_sym);

  // Add offdiagonal term dS_degr_sym2 = dg_dphi * dphi * S if J>=1 , else dg_dphi * dphi * S_iso
  if (use_offdiag) {
    // Compute dg_dphi
    const CeedScalar dg_dphi = -2 * one_minus_phi * (1 - eta);

    // Compute dS_degr_sym2 and add to dS_degr_sym
    RatelMatMatAddSymmetric(dg_dphi * dphi, S_vol_sym, dg_dphi * dphi, S_iso_sym, dS_degr_sym2);
    RatelMatMatAddSymmetric(1., dS_degr_sym, 1., dS_degr_sym2, dS_degr_sym);
  }
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_Degraded_dS_NeoHookean                                                                                     \
  (7 + FLOPS_CInverse + FLOPS_GreenLagrangeStrain_fwd + FLOPS_CInverse_fwd + FLOPS_S_IsochoricNeoHookean + FLOPS_S_vol + \
   FLOPS_dS_IsochoricNeoHookean + FLOPS_dS_vol + 3 * FLOPS_MatMatAddSymmetric)

/**
  @brief Compute degraded Kirchhoff stress (tau) for diagnostic Qfunction

  @param[in]   J_dVdJ        J dV/dJ
  @param[in]   bulk          Bulk modulus
  @param[in]   two_mu        Two times the shear modulus
  @param[in]   Jm1           J - 1
  @param[in]   J_pow         J^{-2/3}
  @param[in]   eta           eta
  @param[in]   phi           damage field
  @param[in]   e_sym         Green Euler strain, in symmetric representation
  @param[out]  tau_degr_sym  degraded second Kirchhoff stress in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int ComputeDegradedKirchhoffTau_NeoHookean(CeedScalar J_dVdJ, CeedScalar bulk, CeedScalar two_mu, CeedScalar Jm1,
                                                                 CeedScalar J_pow, CeedScalar eta, CeedScalar phi, const CeedScalar e_sym[6],
                                                                 CeedScalar tau_degr_sym[6]) {
  CeedScalar tau_vol_diag;

  // Get value of degradation function g(phi)
  const CeedScalar one_minus_phi = 1 - phi;
  const CeedScalar g             = one_minus_phi * one_minus_phi * (1 - eta) + eta;

  // Compute g(phi) * tau_iso and tau_vol
  RatelIsochoricKirchhoffTau_NeoHookean(two_mu * g, J_pow, e_sym, tau_degr_sym);
  RatelVolumetricKirchhoffTau(J_dVdJ, bulk, &tau_vol_diag);

  // Fill diagonal entries for volumetric term
  const CeedScalar tau_vol_diag_degr = (Jm1 > 0. ? g : 1.) * tau_vol_diag;

  // Add volumetric term
  for (int i = 0; i < 3; ++i) tau_degr_sym[i] += tau_vol_diag_degr;
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_Degraded_Tau_NeoHookean (FLOPS_Tau_IsochoricNeoHookean + FLOPS_Tau_vol + 6)

/**
  @brief Compute Linearization of Psi_plus for Neo-hookean hyperelasticity current conf.

  @param[in]   J_dVdJ        J dV/dJ
  @param[in]   two_mu        2 mu (shear modulus)
  @param[in]   bulk          bulk modulus
  @param[in]   Jm1           J - 1
  @param[in]   J_pow         J^{-2/3}
  @param[in]   e_sym         (Green-Euler) strain tensor in symmetric representation
  @param[in]   depsilon_sym  depsilon = (grad_du + grad_du^T)/2
  @param[out]  dPsi_plus     dPsi^+

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int PsiPlus_NeoHookeanCurrent_fwd(CeedScalar J_dVdJ, CeedScalar two_mu, CeedScalar bulk, CeedScalar Jm1, CeedScalar J_pow,
                                                        const CeedScalar e_sym[6], const CeedScalar depsilon_sym[6], CeedScalar *dPsi_plus) {
  CeedScalar tau_sym[6], tau_vol_diag;

  // Compute tau_iso and tau_vol
  RatelIsochoricKirchhoffTau_NeoHookean(two_mu, J_pow, e_sym, tau_sym);
  RatelVolumetricKirchhoffTau(J_dVdJ, bulk, &tau_vol_diag);

  // Fill diagonal entries for volumetric term
  const CeedScalar tau_vol_diag_degr = (Jm1 > 0. ? 1. : 0.) * tau_vol_diag;

  // Add volumetric term
  for (int i = 0; i < 3; ++i) tau_sym[i] += tau_vol_diag_degr;

  // Compute dPsi_plus (offdiagonal term) = tau : depsilon if J >=1 , else tau_iso : depsilon
  *dPsi_plus = RatelMatMatContractSymmetric(1., tau_sym, depsilon_sym);
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_PsiPlus_NeoHookeanCurrent_fwd (FLOPS_Tau_IsochoricNeoHookean + FLOPS_Tau_vol + 3 + FLOPS_MatMatContractSymmetric)

/**
  @brief Compute F*dS_degr*F^T for neo-Hookean damage hyperelasticity in current configuration.

  @param[in]   J_dVdJ              J dV/dJ
  @param[in]   J2_d2VdJ2           J^2 d^2V/dJ^2
  @param[in]   bulk                Bulk modulus
  @param[in]   mu                  Shear modulus
  @param[in]   Jm1                 J - 1
  @param[in]   J_pow               J^{-2/3}
  @param[in]   eta                 eta
  @param[in]   use_offdiag         offdiagonal term (bool)
  @param[in]   phi                 damage field
  @param[in]   dphi                increment of phi
  @param[in]   e_sym               Green Euler strain, in symmetric representation
  @param[in]   grad_du             Gradient of incremental change in u
  @param[out]  depsilon_sym        depsilon = (grad_du + grad_du^T)/2
  @param[out]  FdSFTranspose_degr  F*dS_degr*F^T needed for computing df1 in current configuration

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelComputeDegradedFdSFTranspose_NeoHookean(CeedScalar J_dVdJ, CeedScalar J2_d2VdJ2, CeedScalar bulk, CeedScalar mu,
                                                                       CeedScalar Jm1, CeedScalar J_pow, CeedScalar eta, bool use_offdiag,
                                                                       CeedScalar phi, CeedScalar dphi, const CeedScalar e_sym[6],
                                                                       const CeedScalar grad_du[3][3], CeedScalar depsilon_sym[6],
                                                                       CeedScalar FdSFTranspose_degr[3][3]) {
  CeedScalar FdSFTranspose_vol_sym[6], FdSFTranspose_iso_sym[6], FdSFTranspose_degr_sym[6], tau_sym[6], tau_vol_diag, trace_depsilon;

  // Compute degradation function g(phi) and degraded bulk and mu (depending on Jm1 sign)
  const CeedScalar one_minus_phi = 1 - phi;
  const CeedScalar g             = one_minus_phi * one_minus_phi * (1 - eta) + eta;
  const bool       J_positive    = Jm1 > 0;
  const CeedScalar bulk_degr     = (J_positive ? g : 1) * bulk;
  const CeedScalar mu_degr       = mu * g;

  // Compute g(phi) * dS_iso and (Jm1>0 ? g(phi) : 1) * dS_vol
  RatelComputeFdSFTransposeIsochoric_NeoHookean(mu_degr, J_pow, grad_du, e_sym, depsilon_sym, FdSFTranspose_iso_sym, &trace_depsilon);
  RatelComputeFdSFTransposeVolumetric(J_dVdJ, J2_d2VdJ2, bulk_degr, trace_depsilon, depsilon_sym, FdSFTranspose_vol_sym);

  // F*dS_degr*F^T = F*dS_vol*F^T + F*dS_iso*F^T
  RatelMatMatAddSymmetric(1.0, FdSFTranspose_vol_sym, 1.0, FdSFTranspose_iso_sym, FdSFTranspose_degr_sym);

  // Add offdiagonal term dg_dphi * dphi * tau if J>=1 , else dg_dphi * dphi * tau_iso
  if (use_offdiag) {
    // Compute dg_dphi
    const CeedScalar dg_dphi     = -2 * one_minus_phi * (1 - eta);
    const CeedScalar bulk_factor = (J_positive ? 1 : 0) * bulk;

    RatelIsochoricKirchhoffTau_NeoHookean(dg_dphi * dphi * mu * 2, J_pow, e_sym, tau_sym);
    RatelVolumetricKirchhoffTau(J_dVdJ, dg_dphi * dphi * bulk_factor, &tau_vol_diag);

    // Add volumetric term
    for (int i = 0; i < 3; ++i) tau_sym[i] += tau_vol_diag;

    RatelMatMatAddSymmetric(1., FdSFTranspose_degr_sym, 1., tau_sym, FdSFTranspose_degr_sym);
  }
  RatelSymmetricMatUnpack(FdSFTranspose_degr_sym, FdSFTranspose_degr);
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_Degraded_FdSFTranspose_NeoHookean \
  (FLOPS_FdSFTranspose_iso_NeoHookean + FLOPS_FdSFTranspose_vol + 2 * FLOPS_MatMatAddSymmetric + FLOPS_Tau_IsochoricNeoHookean + FLOPS_Tau_vol + 18)

/**
  @brief Compute (degraded) strain energy for Neo-hookean hyperelasticity

  @param[in]   ctx  QFunction context holding RatelElasticityDamageParams
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 2 - u (4 components)
                      - 3 - gradient of u with respect to reference coordinates
  @param[out]  out  Output arrays
                      - 0 - (degraded) strain energy

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityDamageStrainEnergy_NeoHookean)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[2];
  const CeedScalar(*ug)[4][CEED_Q_VLA]  = (const CeedScalar(*)[4][CEED_Q_VLA])in[3];

  // Outputs
  CeedScalar(*degr_energy) = (CeedScalar(*))out[0];

  // Context
  const RatelElasticityDamageParams *context = (RatelElasticityDamageParams *)ctx;
  const CeedScalar                   mu      = context->mu;
  const CeedScalar                   bulk    = context->bulk;
  const CeedScalar                   eta     = context->residual_stiffness;
  const CeedScalar                   Gc      = context->fracture_toughness;
  const CeedScalar                   l0      = context->characteristic_length;
  const bool                         use_AT1 = context->use_AT1;

  // Set c0 based on AT1 or AT2
  const CeedScalar c0 = use_AT1 ? 8. / 3. : 2.;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar grad_phi[3], E_sym[6], dXdx[3][3], grad_u[3][3], Psi_plus, V = 0.;

    // Read spatial derivatives of u
    const CeedScalar dudX[3][3] = {
        {ug[0][0][i], ug[1][0][i], ug[2][0][i]},
        {ug[0][1][i], ug[1][1][i], ug[2][1][i]},
        {ug[0][2][i], ug[1][2][i], ug[2][2][i]}
    };
    // -- Qdata
    const CeedScalar wdetJ = q_data[0][i];
    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Compute grad_u
    RatelMatMatMult(1.0, dudX, dXdx, grad_u);

    // Compute grad_u , Jm1, and E
    RatelMatMatMult(1.0, dudX, dXdx, grad_u);
    const CeedScalar Jm1 = RatelMatDetAM1(grad_u);
    RatelGreenLagrangeStrain(grad_u, E_sym);

    // Compute volumetric function and derivatives
    VolumetricFunctionAndDerivatives(Jm1, &V, NULL, NULL);

    // Compute Psi
    const CeedScalar phi      = u[3][i];
    const CeedScalar g        = (1 - phi) * (1 - phi) * (1 - eta) + eta;
    const CeedScalar phi_g[3] = {ug[0][3][i], ug[1][3][i], ug[2][3][i]};
    const CeedScalar alpha    = use_AT1 ? phi : phi * phi;

    // Compute grad_phi = dX/dx^T dphi/dX
    RatelMatTransposeVecMult(1.0, dXdx, phi_g, grad_phi);
    const CeedScalar norm_grad_phi2 = RatelDot3(grad_phi, grad_phi);

    // Compute psi_plus and phi_minus
    const CeedScalar trace_E = RatelMatTraceSymmetric(E_sym);
    PsiPlus_NeoHookean(V, mu, bulk, Jm1, trace_E, &Psi_plus);
    const CeedScalar Psi_minus = (Jm1 > 0) ? 0 : bulk * V;

    // Strain energy W_bar(u, phi)
    const CeedScalar strain_energy = g * Psi_plus + Psi_minus + Gc * (alpha + l0 * l0 * norm_grad_phi2) / (c0 * l0);

    degr_energy[i] = strain_energy * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute projected diagnostic values for Neo-hookean hyperelasticity

  @param[in]   ctx  QFunction context holding RatelElasticityDamageParams
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 2 - u (four components: 3 displacement components, 1 scalar damage field)
                      - 3 - gradient of u with respect to reference coordinates
  @param[out]  out  Output arrays
                      - 0 - projected diagnostic values

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityDamageDiagnostic_NeoHookean)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[2];
  const CeedScalar(*ug)[4][CEED_Q_VLA]  = (const CeedScalar(*)[4][CEED_Q_VLA])in[3];

  // Outputs
  CeedScalar(*diagnostic)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Context
  const RatelElasticityDamageParams *context = (RatelElasticityDamageParams *)ctx;
  const CeedScalar                   mu      = context->mu;
  const CeedScalar                   bulk    = context->bulk;
  const CeedScalar                   eta     = context->residual_stiffness;
  const CeedScalar                   rho     = context->common_parameters[RATEL_COMMON_PARAMETER_RHO];
  const CeedScalar                   Gc      = context->fracture_toughness;
  const CeedScalar                   l0      = context->characteristic_length;
  const bool                         use_AT1 = context->use_AT1;

  // Set c0 based on AT1 or AT2
  const CeedScalar c0 = use_AT1 ? 8. / 3. : 2.;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar grad_phi[3], e_sym[6], dXdx[3][3], grad_u[3][3], sigma_degr_sym[6], sigma_dev_degr_sym[6], tau_degr_sym[6], Psi_iso, Psi_plus, V,
        J_dVdJ;

    // Read spatial derivatives of u
    const CeedScalar dudX[3][3] = {
        {ug[0][0][i], ug[1][0][i], ug[2][0][i]},
        {ug[0][1][i], ug[1][1][i], ug[2][1][i]},
        {ug[0][2][i], ug[1][2][i], ug[2][2][i]}
    };
    // -- Qdata
    const CeedScalar wdetJ = q_data[0][i];
    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Compute grad_u , Jm1, E, and trace_E
    RatelMatMatMult(1.0, dudX, dXdx, grad_u);
    const CeedScalar Jm1 = RatelMatDetAM1(grad_u);
    RatelGreenEulerStrain(grad_u, e_sym);
    const CeedScalar trace_e = RatelMatTraceSymmetric(e_sym);

    // Volumetric function and derivatives
    VolumetricFunctionAndDerivatives(Jm1, &V, &J_dVdJ, NULL);

    // Compute psi_iso and psi_vol
    RatelStrainEnergy_IsochoricNeoHookean(0., bulk, mu, Jm1, trace_e, &Psi_iso);
    const CeedScalar Psi_vol = bulk * V;

    // Compute Psi
    const CeedScalar phi      = u[3][i];
    const CeedScalar g        = (1 - phi) * (1 - phi) * (1 - eta) + eta;
    const CeedScalar phi_g[3] = {ug[0][3][i], ug[1][3][i], ug[2][3][i]};
    const CeedScalar alpha    = use_AT1 ? phi : phi * phi;

    // Compute grad_phi = dX/dx^T dphi/dX
    RatelMatTransposeVecMult(1.0, dXdx, phi_g, grad_phi);
    const CeedScalar norm_grad_phi2 = RatelDot3(grad_phi, grad_phi);

    // Compute psi_plus and psi_minus
    PsiPlus_NeoHookean(V, mu, bulk, Jm1, trace_e, &Psi_plus);
    const CeedScalar Psi_minus = (Jm1 > 0) ? 0 : Psi_vol;

    // Strain energy W_bar(u, phi)
    const CeedScalar strain_energy = g * Psi_plus + Psi_minus + Gc * (alpha + l0 * l0 * norm_grad_phi2) / (c0 * l0);

    // Compute sigma = J^-1 * tau (tau being the Kirchhoff stress)
    const CeedScalar J_pow = pow(Jm1 + 1., -2. / 3.);
    ComputeDegradedKirchhoffTau_NeoHookean(J_dVdJ, bulk, 2 * mu, Jm1, J_pow, eta, phi, e_sym, tau_degr_sym);
    for (CeedInt j = 0; j < 6; j++) sigma_degr_sym[j] = tau_degr_sym[j] / (Jm1 + 1);

    //--Store diagnostics
    // Displacement
    diagnostic[0][i] = u[0][i];
    diagnostic[1][i] = u[1][i];
    diagnostic[2][i] = u[2][i];

    // Cauchy stress tensor
    diagnostic[3][i] = sigma_degr_sym[0];
    diagnostic[4][i] = sigma_degr_sym[1];
    diagnostic[5][i] = sigma_degr_sym[2];
    diagnostic[6][i] = sigma_degr_sym[3];
    diagnostic[7][i] = sigma_degr_sym[4];
    diagnostic[8][i] = sigma_degr_sym[5];

    // Hydrostatic pressure; p_h = -trace(sigma) / 3
    diagnostic[9][i] = -RatelMatTraceSymmetric(sigma_degr_sym) / 3.;

    // Strain tensor invariants
    const CeedScalar trace_e2 = RatelMatMatContractSymmetric(1.0, e_sym, e_sym);

    diagnostic[10][i] = trace_e;
    diagnostic[11][i] = trace_e2;

    diagnostic[12][i] = Jm1 + 1;

    // Strain energy W_bar(u, phi)
    diagnostic[13][i] = strain_energy;

    // von Mises stress
    const CeedScalar trace_sigma_degr = RatelMatTraceSymmetric(sigma_degr_sym);
    RatelMatDeviatoricSymmetric(trace_sigma_degr, sigma_degr_sym, sigma_dev_degr_sym);
    const CeedScalar sigma_dev_degr_contract = RatelMatMatContractSymmetric(1.0, sigma_dev_degr_sym, sigma_dev_degr_sym);

    diagnostic[14][i] = sqrt(3 * sigma_dev_degr_contract / 2);

    // Compute mass density values
    // -- Set to 0 as default (rho should be 0.0 as defualt anyways)
    diagnostic[15][i] = 0.0;

    // -- if mass density is requested then compute mass density: rho*J
    if (rho > 0.0) diagnostic[15][i] = rho / diagnostic[12][i];

    // Additional diagnostic quantities for damage model
    // Damage
    diagnostic[16][i] = phi;

    // Psi_vol (undegraded)
    diagnostic[17][i] = Psi_vol;

    // Psi_dev (undegraded)
    diagnostic[18][i] = Psi_iso;

    for (CeedInt j = 0; j < NUM_COMPONENTS_DIAGNOSTIC_NeoHookean_Damage; j++) diagnostic[j][i] *= wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute dual space diagnostic values for NeoHookean hyperelasticity with damage

  @param[in]   ctx  QFunction context, unused
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - volumetric qdata
  @param[out]  out  Output array
                      - 0 - nodal volume

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityDamageDualDiagnostic_NeoHookean)(void *ctx, const CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[4][CEED_Q_VLA]  = (const CeedScalar(*)[4][CEED_Q_VLA])in[2];

  // Outputs
  CeedScalar(*v)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Context
  const CeedScalar *context = (const CeedScalar *)ctx;
  const CeedScalar  rho_0   = context[RATEL_COMMON_PARAMETER_RHO];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar dXdx[3][3], grad_u[3][3];

    // Read spatial derivatives of u
    const CeedScalar dudX[3][3] = {
        {ug[0][0][i], ug[1][0][i], ug[2][0][i]},
        {ug[0][1][i], ug[1][1][i], ug[2][1][i]},
        {ug[0][2][i], ug[1][2][i], ug[2][2][i]}
    };
    // -- Qdata
    const CeedScalar wdetJ = q_data[0][i];
    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Compute grad_u
    RatelMatMatMult(1.0, dudX, dXdx, grad_u);

    // Compute Jm1
    const CeedScalar Jm1 = RatelMatDetAM1(grad_u);

    // Nodal volume
    v[0][i] = 1.0 * wdetJ;

    // Nodal density: rho_0 = rho / J
    v[1][i] = wdetJ * rho_0 / (Jm1 + 1);
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
