/// @file
/// Ratel isochoric stress neo-Hookean for mixed/single fields hyperelasticity at finite strain common QFunction source
#pragma once

#include <ceed/types.h>
#ifndef CEED_RUNNING_JIT_PASS
#include <math.h>
#endif

#include "../../models/neo-hookean.h"  // IWYU pragma: export
#include "../utils.h"                  // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

#define FLOPS_J_pow 3
#define FLOPS_Tau_iso_NeoHookean (FLOPS_MatTrace + FLOPS_MatDeviatoricSymmetric + 12)
#define FLOPS_S_iso_NeoHookean \
  (FLOPS_MatTrace + FLOPS_MatDeviatoricSymmetric + FLOPS_MatMatMultSymmetric + FLOPS_J_pow + 2 + FLOPS_ScalarMatMultSymmetric)
#define FLOPS_dS_iso_NeoHookean                                                                                                             \
  (FLOPS_GreenLagrangeStrain_fwd + FLOPS_CInverse_fwd + FLOPS_MatMatContractSymmetric + 2 * FLOPS_MatTrace + FLOPS_MatDeviatoricSymmetric + \
   FLOPS_MatMatMultSymmetric + 3 + 2 * FLOPS_MatMatAddSymmetric + 24 + FLOPS_J_pow + 7)
#define FLOPS_FdSFTranspose_iso_NeoHookean                                                                                     \
  (6 + 3 * FLOPS_MatTrace + FLOPS_ScalarMatMultSymmetric + 9 + FLOPS_GreenEulerStrain_fwd + 6 + FLOPS_MatDeviatoricSymmetric + \
   FLOPS_MatMatAddSymmetric + 15)

/**
  @brief Compute isochoric part of Kirchoff tau for neo-Hookean hyperelasticity.

 `tau_iso = 2 mu J^{-2/3} e_dev`

  @param[in]   two_mu       Two times the shear modulus
  @param[in]   J_pow        J^{-2/3}
  @param[in]   e_sym        Green Euler strain, in symmetric representation
  @param[out]  tau_iso_sym  Isochoric Kirchoff tau, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelIsochoricKirchhoffTau_NeoHookean(CeedScalar two_mu, CeedScalar J_pow, const CeedScalar e_sym[6],
                                                                CeedScalar tau_iso_sym[6]) {
  CeedScalar e_dev_sym[6];

  // Compute Deviatoric Strain
  // e_dev = e - 1/3 * trace(e) * I
  const CeedScalar trace_e = RatelMatTraceSymmetric(e_sym);

  RatelMatDeviatoricSymmetric(trace_e, e_sym, e_dev_sym);

  tau_iso_sym[0] = two_mu * J_pow * e_dev_sym[0];
  tau_iso_sym[1] = two_mu * J_pow * e_dev_sym[1];
  tau_iso_sym[2] = two_mu * J_pow * e_dev_sym[2];
  tau_iso_sym[3] = two_mu * J_pow * e_dev_sym[3];
  tau_iso_sym[4] = two_mu * J_pow * e_dev_sym[4];
  tau_iso_sym[5] = two_mu * J_pow * e_dev_sym[5];
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute isochoric part of second Kirchoff stress for neo-Hookean hyperelasticity.

 `S_iso = 2 mu J^{-2/3} C_inv * E_dev`

  @param[in]   two_mu     Two times the shear modulus
  @param[in]   J          Determinant of deformation gradient
  @param[in]   C_inv_sym  Inverse of right Cauchy-Green tensor, in symmetric representation
  @param[in]   E_sym      Green Lagrange strain, in symmetric representation
  @param[out]  S_iso_sym  Isochoric second Kirchoff stress, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelIsochoricSecondKirchhoffStress_NeoHookean(CeedScalar two_mu, CeedScalar J, const CeedScalar C_inv_sym[6],
                                                                         const CeedScalar E_sym[6], CeedScalar S_iso_sym[6]) {
  CeedScalar E_dev_sym[6], C_inv_E_dev_sym[6];

  // Compute Deviatoric Strain
  // E_dev = E - 1/3 * trace(E) * I
  const CeedScalar trace_E = RatelMatTraceSymmetric(E_sym);

  RatelMatDeviatoricSymmetric(trace_E, E_sym, E_dev_sym);

  // -- C_inv * E_dev
  RatelMatMatMultSymmetric(1.0, C_inv_sym, E_dev_sym, C_inv_E_dev_sym);

  // -- 2*mu*J^{-2/3}
  const CeedScalar coeff_1 = two_mu * pow(J, -2. / 3.);

  // S_iso = 2*mu*J^{-2/3}*C_inv*E_dev
  RatelScalarMatMultSymmetric(coeff_1, C_inv_E_dev_sym, S_iso_sym);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute derivative of isochoric second Kirchoff stress for neo-Hookean hyperelasticity.

 `dS_iso = -4/3 mu J^(-2/3) (C_inv:dE) C_inv * E_dev - 1/3 mu J^(-2/3) dS_iso1`
 `dS_iso1 = 2 tr(dE) C_inv + I1(C) dC_inv`

  @param[in]   mu                Shear modulus
  @param[in]   J                 Determinant of deformation gradient
  @param[in]   F                 Deformation gradient
  @param[in]   grad_du           Gradient of incremental change in u
  @param[in]   C_inv_sym         Inverse of right Cauchy-Green tensor, in symmetric representation
  @param[in]   E_sym             Green Lagrange strain, in symmetric representation
  @param[out]  dC_inv_sym        Derivative of C^{-1}, in symmetric representation
  @param[out]  dS_iso_sym        Derivative of isochoric second Kirchoff stress, in symmetric representation
  @param[out]  Cinv_contract_dE  `C_inv : dE`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelIsochoricSecondKirchhoffStress_NeoHookean_fwd(CeedScalar mu, CeedScalar J, const CeedScalar F[3][3],
                                                                             const CeedScalar grad_du[3][3], const CeedScalar C_inv_sym[6],
                                                                             const CeedScalar E_sym[6], CeedScalar dC_inv_sym[6],
                                                                             CeedScalar dS_iso_sym[6], CeedScalar *Cinv_contract_dE) {
  CeedScalar dE_sym[6], E_dev_sym[6], C_inv_E_dev_sym[6], dS_iso1_sym[6];

  // dE - Green-Lagrange strain tensor increment
  RatelGreenLagrangeStrain_fwd(grad_du, F, dE_sym);
  // dC_inv = -2*C_inv*dE*C_inv
  RatelCInverse_fwd(C_inv_sym, dE_sym, dC_inv_sym);
  // C_inv:dE
  *Cinv_contract_dE = RatelMatMatContractSymmetric(1.0, C_inv_sym, dE_sym);

  // dS_iso = -4/3 * mu * J^(-2/3) * (C_inv:dE) * C_inv * E_dev - 1/3 mu J^(-2/3) * dS_iso1
  // Compute Deviatoric Strain
  // E_dev = E - 1/3 * trace(E) * I
  const CeedScalar trace_E = RatelMatTraceSymmetric(E_sym);

  RatelMatDeviatoricSymmetric(trace_E, E_sym, E_dev_sym);
  // -- C_inv * E_dev
  RatelMatMatMultSymmetric(1.0, C_inv_sym, E_dev_sym, C_inv_E_dev_sym);

  // -- dS_iso1 = 2*tr(dE)*C_inv + I1(C) dC_inv
  const CeedScalar I1_C     = 2 * trace_E + 3.;
  const CeedScalar trace_dE = RatelMatTraceSymmetric(dE_sym);

  RatelMatMatAddSymmetric(2 * trace_dE, C_inv_sym, I1_C, dC_inv_sym, dS_iso1_sym);

  // dS_iso
  const CeedScalar J_pow   = pow(J, -2. / 3.);
  const CeedScalar coeff_1 = (-4. / 3.) * mu * J_pow * (*Cinv_contract_dE);
  const CeedScalar coeff_2 = (-1. / 3.) * mu * J_pow;

  RatelMatMatAddSymmetric(coeff_1, C_inv_E_dev_sym, coeff_2, dS_iso1_sym, dS_iso_sym);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute F*dS_iso*F^T for neo-Hookean hyperelasticity in current configuration.

  @param[in]   mu                     Shear modulus
  @param[in]   J_pow                  J^{-2/3}
  @param[in]   grad_du                Gradient of incremental change in u
  @param[in]   e_sym                  Green Euler strain, in symmetric representation
  @param[out]  depsilon_sym           depsilon = (grad_du + grad_du^T)/2
  @param[out]  FdSFTranspose_iso_sym  F*dS_iso*F^T needed for computing df1 in current configuration
  @param[out]  trace_depsilon         `trace(depsilon)`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelComputeFdSFTransposeIsochoric_NeoHookean(CeedScalar mu, CeedScalar J_pow, const CeedScalar grad_du[3][3],
                                                                        const CeedScalar e_sym[6], CeedScalar depsilon_sym[6],
                                                                        CeedScalar FdSFTranspose_iso_sym[6], CeedScalar *trace_depsilon) {
  CeedScalar b_sym[6], b[3][3], e_dev_sym[6], de_sym[6];

  depsilon_sym[0] = grad_du[0][0];
  depsilon_sym[1] = grad_du[1][1];
  depsilon_sym[2] = grad_du[2][2];
  depsilon_sym[3] = (grad_du[1][2] + grad_du[2][1]) / 2.;
  depsilon_sym[4] = (grad_du[0][2] + grad_du[2][0]) / 2.;
  depsilon_sym[5] = (grad_du[0][1] + grad_du[1][0]) / 2.;

  *trace_depsilon = RatelMatTraceSymmetric(depsilon_sym);

  // F*dS_iso*F^T = (-2/3)*mu*J^{-2/3} * [2*tr(depsilon)*e_dev + tr(de)*I - I1_b*depsilon]
  for (CeedInt j = 0; j < 6; j++) b_sym[j] = 2 * e_sym[j] + (j < 3);
  RatelSymmetricMatUnpack(b_sym, b);
  RatelGreenEulerStrain_fwd(grad_du, b, de_sym);
  const CeedScalar trace_e  = RatelMatTraceSymmetric(e_sym);
  const CeedScalar I1_b     = 2 * trace_e + 3.;
  const CeedScalar trace_de = RatelMatTraceSymmetric(de_sym);
  const CeedScalar coeff_1  = (-2. / 3.) * mu * J_pow;

  RatelMatDeviatoricSymmetric(trace_e, e_sym, e_dev_sym);
  RatelMatMatAddSymmetric(2.0 * (*trace_depsilon), e_dev_sym, -I1_b, depsilon_sym, FdSFTranspose_iso_sym);
  // Add trace(de)I
  FdSFTranspose_iso_sym[0] += trace_de;
  FdSFTranspose_iso_sym[1] += trace_de;
  FdSFTranspose_iso_sym[2] += trace_de;
  RatelScalarMatMultSymmetric(coeff_1, FdSFTranspose_iso_sym, FdSFTranspose_iso_sym);
  return CEED_ERROR_SUCCESS;
}

/// @}
