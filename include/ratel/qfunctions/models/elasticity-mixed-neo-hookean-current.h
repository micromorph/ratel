/// @file
/// Ratel mixed neo-Hookean hyperelasticity at finite strain in current configuration QFunction source
#include <ceed/types.h>
#ifndef CEED_RUNNING_JIT_PASS
#include <math.h>
#endif

#include "../../models/mixed-neo-hookean.h"       // IWYU pragma: export
#include "../boundaries/platen-nitsche.h"         // IWYU pragma: export
#include "../utils.h"                             // IWYU pragma: export
#include "elasticity-mixed-common.h"              // IWYU pragma: export
#include "elasticity-mixed-neo-hookean-common.h"  // IWYU pragma: export
#include "elasticity-mixed-neo-hookean-initial.h"

/// @addtogroup RatelMaterials
/// @{

#define NUM_COMPONENTS_STATE_MixedNeoHookeanCurrent 0
#define NUM_COMPONENTS_STORED_MixedNeoHookeanCurrent 18
#define NUM_ACTIVE_FIELD_EVAL_MODES_MixedNeoHookeanCurrent_u 1
#define NUM_ACTIVE_FIELD_EVAL_MODES_MixedNeoHookeanCurrent_p 1
#define NUM_ACTIVE_FIELD_EVAL_MODES_MixedNeoHookeanCurrent \
  (NUM_ACTIVE_FIELD_EVAL_MODES_MixedNeoHookeanCurrent_u + NUM_ACTIVE_FIELD_EVAL_MODES_MixedNeoHookeanCurrent_p)

// FLOPS_grad_du = FLOPS_MatMatMult
// 3 flops for creating F
#define FLOPS_df1_MixedNH_Current (2 * FLOPS_MatMatMult + FLOPS_Tau_MixedNeoHookean + FLOPS_FdSFTranspose_MixedNeoHookean + FLOPS_MatMatAdd)
#define FLOPS_dg0_MixedNH_Current (FLOPS_J_dVdJ + FLOPS_J2_d2VdJ2 + 10)
#define FLOPS_JACOBIAN_MixedNeoHookeanCurrent (FLOPS_df1_MixedNH_Current + FLOPS_dXdxwdetJ + FLOPS_dg0_MixedNH_Current + 1)
#define FLOPS_JACOBIAN_Block_uu_MixedNeoHookeanCurrent \
  (2 * FLOPS_MatMatMult + FLOPS_Tau_MixedNeoHookean + FLOPS_FdSFTranspose_MixedNeoHookean + FLOPS_MatMatAdd + FLOPS_dXdxwdetJ)
#define FLOPS_JACOBIAN_Block_pp_MixedNeoHookeanCurrent 8

// -----------------------------------------------------------------------------
// Strong form:
//  div(P)  +  f       = 0   in \Omega
// -p  -  d\phi_vol/dJ = 0   in \Omega
//
// p is pressure, P is First Piola-Kirchhoff tensor
// Weak form: Find (u,p) \in VxQ (V=H1, Q=L^2) on \Omega
//  (grad(v), tau)                     = (v, f)
// -(q, J * p) - (q, J * d\phi_vol/dJ) = 0
// -----------------------------------------------------------------------------
/**
  @brief Compute `P` for mixed neo-Hookean hyperelasticity in current configuration

  @param[in]   ctx   QFunction context, holding `RatelMixedNeoHookeanElasticityParams`
  @param[in]   Q     Number of quadrature points
  @param[in]   i     Current quadrature point
  @param[in]   in    Input arrays
                       - 0 - volumetric qdata
                       - 1 - gradient of u with respect to reference coordinates
                       - 2 - pressure
  @param[out]  out   Output arrays
                       - 0 - stored values: dXdx, e, J - 1, J^{-2/3} and p
  @param[out]  dXdx  Coordinate transformation
  @param[out]  f1    `f1 = tau`
  @param[out]  Jm1   Determinant of deformation gradient - 1 needed for computing g0 function

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int f1_MixedNeoHookeanCurrent(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in, CeedScalar *const *out,
                                                    CeedScalar dXdx[3][3], CeedScalar f1[3][3], CeedScalar *Jm1) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];
  const CeedScalar(*p)                  = in[2];

  // Outputs
  CeedScalar(*stored_values) = out[0];

  // Context
  const RatelMixedNeoHookeanElasticityParams *context     = (RatelMixedNeoHookeanElasticityParams *)ctx;
  const CeedScalar                            two_mu      = context->two_mu;
  const CeedScalar                            bulk_primal = context->bulk_primal;

  CeedScalar dXdx_initial[3][3], Grad_u[3][3], e_sym[6], tau_sym[6], F_inv[3][3], J_dVdJ;

  // -- Qdata
  // dXdx_initial = dX/dx_initial
  // X is natural coordinate sys OR Reference [-1, 1]^dim
  // x_initial is initial config coordinate system
  RatelQdataUnpack(Q, i, q_data, dXdx_initial);

  // Read spatial derivatives of u; du/dX
  CeedScalar dudX[3][3];
  RatelGradUnpack(Q, i, ug, dudX);

  // X is natural coordinate sys OR Reference system
  // x_initial is initial config coordinate system
  // Grad_u =du/dx_initial= du/dX * dX/dx_initial
  RatelMatMatMult(1.0, dudX, dXdx_initial, Grad_u);

  // Compute the Deformation Gradient : F = I + grad_u
  const CeedScalar F[3][3] = {
      {Grad_u[0][0] + 1, Grad_u[0][1],     Grad_u[0][2]    },
      {Grad_u[1][0],     Grad_u[1][1] + 1, Grad_u[1][2]    },
      {Grad_u[2][0],     Grad_u[2][1],     Grad_u[2][2] + 1}
  };

  // J - 1
  *Jm1                   = RatelMatDetAM1(Grad_u);
  const CeedScalar J     = *Jm1 + 1.;
  const CeedScalar J_pow = pow(J, -2. / 3.);

  // Compute e, tau
  RatelGreenEulerStrain(Grad_u, e_sym);
  VolumetricFunctionAndDerivatives(*Jm1, NULL, &J_dVdJ, NULL);
  RatelKirchhoffTau_MixedNeoHookean(J_dVdJ, bulk_primal, two_mu, p[i], *Jm1, J_pow, e_sym, tau_sym);
  RatelSymmetricMatUnpack(tau_sym, f1);

  // Compute F^{-1}
  RatelMatInverse(F, J, F_inv);

  // x is current config coordinate system
  // dXdx = dX/dx = dX/dx_initial * F^{-1}
  // Note that F^{-1} = dx_initial/dx
  RatelMatMatMult(1.0, dXdx_initial, F_inv, dXdx);

  // Store values
  RatelStoredValuesPack(Q, i, 0, 9, (CeedScalar *)dXdx, stored_values);
  RatelStoredValuesPack(Q, i, 9, 6, (CeedScalar *)e_sym, stored_values);
  RatelStoredValuesPack(Q, i, 15, 1, Jm1, stored_values);
  RatelStoredValuesPack(Q, i, 16, 1, &J_pow, stored_values);
  RatelStoredValuesPack(Q, i, 17, 1, &p[i], stored_values);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute `g0` for mixed neo-Hookean hyperelasticity in current configuration

  @param[in]   ctx  QFunction context, holding `RatelMixedNeoHookeanElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   i    Current quadrature point
  @param[in]   in   Input arrays
                      - 2 - p
  @param[in]   Jm1  Determinant of deformation gradient - 1 computed in f1 function
  @param[out]  out  Output arrays, unused
  @param[out]  g0   `g0 = -J dV/dJ - p * J / (bulk - bulk_primal)`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int g0_MixedNeoHookeanCurrent(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in, const CeedScalar Jm1,
                                                    CeedScalar *const *out, CeedScalar *g0) {
  // Inputs
  const CeedScalar(*p) = in[2];

  // Context
  const RatelMixedNeoHookeanElasticityParams *context     = (RatelMixedNeoHookeanElasticityParams *)ctx;
  const CeedScalar                            bulk        = context->bulk;
  const CeedScalar                            bulk_primal = context->bulk_primal;
  const CeedScalar                            inv_bulk    = (bulk == 0.) ? 0. : 1. / (bulk - bulk_primal);

  CeedScalar J_dVdJ, J = Jm1 + 1.;

  VolumetricFunctionAndDerivatives(Jm1, NULL, &J_dVdJ, NULL);

  // q * (-J dV/dJ - p * J / (bulk - bulk_primal))
  *g0 = (-J_dVdJ - (p[i] * J) * inv_bulk);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute linearization of `f1` for mixed neo-Hookean hyperelasticity in current configuration

  @param[in]   ctx             QFunction context, holding `RatelMixedNeoHookeanElasticityParams`
  @param[in]   Q               Number of quadrature points
  @param[in]   i               Current quadrature point
  @param[in]   in              Input arrays
                                 - 0 - volumetric qdata
                                 - 1 - stored dXdx, e, J - 1, J^{-2/3} and p
                                 - 2 - gradient of incremental change to u with respect to reference coordinates
                                 - 3 - dp
  @param[out]  out             Output arrays, unused
  @param[out]  dXdx            Coordinate transformation
  @param[out]  df1             `df1 = dtau - tau * grad_du^T`
  @param[out]  Jm1             Determinant of deformation gradient - 1 needed for computing dg0 function
  @param[out]  trace_depsilon  `trace(depsilon)` needed for computing dg0 function

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int df1_MixedNeoHookeanCurrent(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in, CeedScalar *const *out,
                                                     CeedScalar dXdx[3][3], CeedScalar df1[3][3], CeedScalar *Jm1, CeedScalar *trace_depsilon) {
  // Inputs
  const CeedScalar(*stored_values)      = in[1];
  const CeedScalar(*dug)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[2];
  const CeedScalar(*dp)                 = in[3];

  // Context
  const RatelMixedNeoHookeanElasticityParams *context     = (RatelMixedNeoHookeanElasticityParams *)ctx;
  const CeedScalar                            mu          = context->mu;
  const CeedScalar                            two_mu      = context->two_mu;
  const CeedScalar                            bulk_primal = context->bulk_primal;

  CeedScalar grad_du[3][3], e_sym[6], tau_sym[6], tau[3][3], grad_du_tau[3][3], FdSFTranspose[3][3], J_pow, p, J_dVdJ, J2_d2VdJ2;

  // Read spatial derivatives of increment u; d(du)/dX = d(du)/dX
  CeedScalar ddudX[3][3];
  RatelGradUnpack(Q, i, dug, ddudX);

  // Retrieve dXdx
  RatelStoredValuesUnpack(Q, i, 0, 9, stored_values, (CeedScalar *)dXdx);

  // Compute grad_du = ddu/dX * dX/dx
  // X is ref coordinate [-1,1]^3; x is physical coordinate in current configuration
  RatelMatMatMult(1.0, ddudX, dXdx, grad_du);

  // Retrieve e_sym, J1, J^{-2/3} and p
  RatelStoredValuesUnpack(Q, i, 9, 6, stored_values, e_sym);
  RatelStoredValuesUnpack(Q, i, 15, 1, stored_values, Jm1);
  RatelStoredValuesUnpack(Q, i, 16, 1, stored_values, &J_pow);
  RatelStoredValuesUnpack(Q, i, 17, 1, stored_values, &p);
  VolumetricFunctionAndDerivatives(*Jm1, NULL, &J_dVdJ, &J2_d2VdJ2);
  RatelKirchhoffTau_MixedNeoHookean(J_dVdJ, bulk_primal, two_mu, p, *Jm1, J_pow, e_sym, tau_sym);
  RatelSymmetricMatUnpack(tau_sym, tau);

  // Compute grad_du_tau = grad_du*tau
  RatelMatMatMult(1.0, grad_du, tau, grad_du_tau);

  // Compute F*dS*F^T
  RatelComputeFdSFTranspose_MixedNeoHookean(J_dVdJ, J2_d2VdJ2, bulk_primal, mu, p, dp[i], *Jm1, J_pow, grad_du, e_sym, FdSFTranspose, trace_depsilon);

  // df1 = dtau - tau * grad_du^T
  //     = grad_du*tau + F*dS*F^T
  RatelMatMatAdd(1.0, grad_du_tau, 1., FdSFTranspose, df1);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute linearization of `g0` for mixed neo-Hookean hyperelasticity in current configuration

  @param[in]   ctx             QFunction context, holding `RatelMixedNeoHookeanElasticityParams`
  @param[in]   Q               Number of quadrature points
  @param[in]   i               Current quadrature point
  @param[in]   in              Input arrays
                                 - 1 - stored J and p
                                 - 3 - dp
  @param[in]   Jm1             Determinant of deformation gradient - 1 computed in df1 function
  @param[in]   trace_depsilon  `trace(depsilon)` computed in df1 function
  @param[out]  out             Output arrays, unused
  @param[out]  dg0             `dg0 = ([-J^2 d2V/dJ2 - J dV/dJ - J*p/(bulk-bulk_primal)]trace(depsilon) - dp*J/(bulk-bulk_primal) )`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int dg0_MixedNeoHookeanCurrent(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in, const CeedScalar Jm1,
                                                     const CeedScalar trace_depsilon, CeedScalar *const *out, CeedScalar *dg0) {
  // Inputs
  const CeedScalar(*stored_values) = in[1];
  const CeedScalar(*dp)            = in[3];

  // Context
  const RatelMixedNeoHookeanElasticityParams *context     = (RatelMixedNeoHookeanElasticityParams *)ctx;
  const CeedScalar                            bulk        = context->bulk;
  const CeedScalar                            bulk_primal = context->bulk_primal;
  const CeedScalar                            inv_bulk    = (bulk == 0.) ? 0. : 1. / (bulk - bulk_primal);

  CeedScalar J_dVdJ, J2_d2VdJ2, p, J = Jm1 + 1.;

  VolumetricFunctionAndDerivatives(Jm1, NULL, &J_dVdJ, &J2_d2VdJ2);

  RatelStoredValuesUnpack(Q, i, 17, 1, stored_values, &p);
  // q * dg0
  *dg0 = (-J2_d2VdJ2 - J_dVdJ - (J * p) * inv_bulk) * trace_depsilon - (J * dp[i]) * inv_bulk;
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute residual for mixed neo-Hookean hyperelasticity in current configuration

  @param[in]   ctx  QFunction context, holding `RatelMixedNeoHookeanElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityResidual_MixedNeoHookeanCurrent)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  (void)ElasticityResidual_MixedNeoHookeanInitial;
  return MixedElasticityResidual(ctx, Q, f1_MixedNeoHookeanCurrent, g0_MixedNeoHookeanCurrent, !!NUM_COMPONENTS_STATE_MixedNeoHookeanCurrent,
                                 !!NUM_COMPONENTS_STORED_MixedNeoHookeanCurrent, NUM_ACTIVE_FIELD_EVAL_MODES_MixedNeoHookeanCurrent, in, out);
}

/**
  @brief Evaluate Jacobian for mixed neo-Hookean hyperelasticity in current configuration

  @param[in]   ctx  QFunction context, holding `RatelMixedNeoHookeanElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityJacobian_MixedNeoHookeanCurrent)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  (void)ElasticityJacobian_MixedNeoHookeanInitial;
  return MixedElasticityJacobian(ctx, Q, df1_MixedNeoHookeanCurrent, dg0_MixedNeoHookeanCurrent, !!NUM_COMPONENTS_STATE_MixedNeoHookeanCurrent,
                                 !!NUM_COMPONENTS_STORED_MixedNeoHookeanCurrent, NUM_ACTIVE_FIELD_EVAL_MODES_MixedNeoHookeanCurrent, in, out);
}

/**
  @brief Compute displacement block for pMG preconditioner for mixed neo-Hookean hyperelasticity in current configuration

  @param[in]   ctx  QFunction context, holding `RatelMixedNeoHookeanElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      0 - qdata
                      1 - gradient of incremental change of u with respect to reference coordinates
  @param[out]  out  Output arrays
                      0 - action of QFunction for displacement field

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityPC_uu_MixedNeoHookeanCurrent)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  (void)ElasticityPC_uu_MixedNeoHookeanInitial;
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*stored_values)      = in[1];
  const CeedScalar(*dug)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[2];

  // Outputs
  CeedScalar(*ddvdX)[3][CEED_Q_VLA] = (CeedScalar(*)[3][CEED_Q_VLA])out[0];

  // Context
  const RatelMixedNeoHookeanElasticityParams *context        = (RatelMixedNeoHookeanElasticityParams *)ctx;
  const CeedScalar                            mu             = context->mu;
  const CeedScalar                            two_mu         = context->two_mu;
  const CeedScalar                            bulk_primal_pc = context->bulk_primal_pc;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar ddudX[3][3], dXdx[3][3], grad_du[3][3], e_sym[6], tau_sym[6], tau[3][3], grad_du_tau[3][3], FdSFTranspose[3][3], df1_uu[3][3], Jm1,
        J_pow, p, trace_depsilon, J_dVdJ, J2_d2VdJ2;
    // -- Qdata
    const CeedScalar wdetJ = q_data[0][i];

    // Read spatial derivatives of increment u; d(du)/dX = d(du)/dX
    RatelGradUnpack(Q, i, dug, ddudX);

    // Retrieve dXdx
    RatelStoredValuesUnpack(Q, i, 0, 9, stored_values, (CeedScalar *)dXdx);

    // Compute grad_du = ddu/dX * dX/dx
    // X is ref coordinate [-1,1]^3; x is physical coordinate in current configuration
    RatelMatMatMult(1.0, ddudX, dXdx, grad_du);

    // Retrieve e_sym, J1, J^{-2/3} and p
    RatelStoredValuesUnpack(Q, i, 9, 6, stored_values, e_sym);
    RatelStoredValuesUnpack(Q, i, 15, 1, stored_values, &Jm1);
    RatelStoredValuesUnpack(Q, i, 16, 1, stored_values, &J_pow);
    RatelStoredValuesUnpack(Q, i, 17, 1, stored_values, &p);
    VolumetricFunctionAndDerivatives(Jm1, NULL, &J_dVdJ, &J2_d2VdJ2);
    RatelKirchhoffTau_MixedNeoHookean(J_dVdJ, bulk_primal_pc, two_mu, p, Jm1, J_pow, e_sym, tau_sym);
    RatelSymmetricMatUnpack(tau_sym, tau);

    // Compute grad_du_tau = grad_du*tau
    RatelMatMatMult(1.0, grad_du, tau, grad_du_tau);

    // Compute F*dS*F^T
    RatelComputeFdSFTranspose_MixedNeoHookean(J_dVdJ, J2_d2VdJ2, bulk_primal_pc, mu, p, 0.0, Jm1, J_pow, grad_du, e_sym, FdSFTranspose,
                                              &trace_depsilon);

    // df1 = dtau - tau * grad_du^T
    //     = grad_du*tau + F*dS*F^T
    RatelMatMatAdd(1.0, grad_du_tau, 1., FdSFTranspose, df1_uu);

    // (grad(v), df1_uu)
    RatelMatMatTransposeMultAtQuadraturePoint(Q, i, wdetJ, dXdx, df1_uu, ddvdX);
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute pressure block for pMG preconditioner for mixed neo-Hookean hyperelasticity in current configuration

  @param[in]   ctx  QFunction context, holding `RatelMixedNeoHookeanElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      0 - qdata
                      1 - incremental change of p
  @param[out]  out  Output arrays
                      0 - action of QFunction for pressure field

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityPC_pp_MixedNeoHookeanCurrent)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  (void)ElasticityPC_pp_MixedNeoHookeanInitial;
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*stored_values)      = in[1];
  const CeedScalar(*dp)                 = in[2];

  // Outputs
  CeedScalar(*dq) = out[0];

  // Context
  const RatelMixedNeoHookeanElasticityParams *context        = (RatelMixedNeoHookeanElasticityParams *)ctx;
  const CeedScalar                            mu             = context->mu;
  const CeedScalar                            bulk           = context->bulk;
  const CeedScalar                            bulk_primal_pc = context->bulk_primal_pc;
  const CeedInt                               sign_pp        = context->sign_pp;
  const CeedScalar                            inv_bulk_pc    = (bulk == 0.) ? 0. : 1. / (bulk - bulk_primal_pc);

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // -- Qdata
    const CeedScalar wdetJ = q_data[0][i];
    // J - 1
    CeedScalar Jm1;
    RatelStoredValuesUnpack(Q, i, 15, 1, stored_values, &Jm1);
    const CeedScalar J = Jm1 + 1.;
    // (q, -J*dp/k)
    dq[i]              = sign_pp * (J * inv_bulk_pc + J / mu) * dp[i] * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
