/// @file
/// Ratel Neo-Hookean (initial configuration) with damage phase field
#include <ceed/types.h>
#ifndef CEED_RUNNING_JIT_PASS
#include <math.h>
#endif

#include "../../models/elasticity-damage.h"           // IWYU pragma: export
#include "../utils.h"                                 // IWYU pragma: export
#include "elasticity-isochoric-neo-hookean-common.h"  // IWYU pragma: export
#include "elasticity-neo-hookean-damage-common.h"     // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

/**
  @brief Compute projected diagnostic values for Neo-hookean hyperelasticity initial conf.

  @param[in]   ctx  QFunction context holding RatelElasticityDamageParams
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata with respect to X_previous
                      - 1 - state (11 components: Psi_plus, 9 Grad_u)
                      - 2 - u (four components: 3 displacement components (unused), 1 scalar damage field)
                      - 3 - gradient of u with respect to previous coordinates X_previous (unused)
                      - 4 - rho
                      - 5 - elastic parameters `RatelNeoHookeanElasticityPointFields`
                      - 6 - damage parameters `RatelElasticityDamagePointFields`
  @param[out]  out  Output arrays
                      - 0 - projected diagnostic values

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityDamageDiagnostic_NeoHookean_MPM)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar *state               = in[1];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[2];
  const CeedScalar(*ug)[4][CEED_Q_VLA]  = (const CeedScalar(*)[4][CEED_Q_VLA])in[3];
  const CeedScalar *rho                 = in[4];
  const CeedScalar *elastic_parameters  = in[5];
  const CeedScalar *damage_parameters   = in[6];

  // Outputs
  CeedScalar(*diagnostic)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Context
  const RatelElasticityDamageParams *context = (RatelElasticityDamageParams *)ctx;
  const bool                         use_AT1 = context->use_AT1;
  const CeedScalar                   c0      = use_AT1 ? 8. / 3. : 2.;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar grad_phi[3], grad_u[3][3], e_sym[6], dXdx[3][3], sigma_degr_sym[6], sigma_dev_degr_sym[6], tau_degr_sym[6], Psi_iso, Psi_plus, J_dVdJ,
        V;

    // Parameters
    RatelNeoHookeanElasticityPointFields elastic;
    RatelElasticityDamagePointFields     damage;

    RatelStoredValuesUnpack(Q, i, 0, sizeof(elastic) / sizeof(CeedScalar), elastic_parameters, (CeedScalar *)&elastic);
    RatelStoredValuesUnpack(Q, i, 0, sizeof(damage) / sizeof(CeedScalar), damage_parameters, (CeedScalar *)&damage);

    // State
    RatelStoredValuesUnpack(Q, i, 1, 9, state, (CeedScalar *)grad_u);

    // -- Qdata
    const CeedScalar wdetJ = q_data[0][i];
    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Compute grad_u , Jm1, E, and trace_E
    const CeedScalar Jm1 = RatelMatDetAM1(grad_u);
    RatelGreenEulerStrain(grad_u, e_sym);
    const CeedScalar trace_e = RatelMatTraceSymmetric(e_sym);

    // Volumetric function and derivatives
    VolumetricFunctionAndDerivatives(Jm1, &V, &J_dVdJ, NULL);

    // Compute psi_iso and psi_vol
    RatelStrainEnergy_IsochoricNeoHookean(0, elastic.bulk, elastic.mu, Jm1, trace_e, &Psi_iso);
    const CeedScalar Psi_vol = elastic.bulk * V;

    // Compute Psi
    const CeedScalar phi      = u[3][i];
    const CeedScalar g        = (1 - phi) * (1 - phi) * (1 - damage.residual_stiffness) + damage.residual_stiffness;
    const CeedScalar phi_g[3] = {ug[0][3][i], ug[1][3][i], ug[2][3][i]};
    const CeedScalar alpha    = use_AT1 ? phi : phi * phi;

    // Compute grad_phi = dX/dx^T dphi/dX
    RatelMatTransposeVecMult(1.0, dXdx, phi_g, grad_phi);
    const CeedScalar norm_grad_phi2 = RatelDot3(grad_phi, grad_phi);

    // Compute psi_plus and psi_minus
    PsiPlus_NeoHookean(V, elastic.mu, elastic.bulk, Jm1, trace_e, &Psi_plus);
    const CeedScalar Psi_minus = (Jm1 > 0) ? 0 : Psi_vol;

    // Strain energy W_bar(u, phi)
    const CeedScalar strain_energy = g * Psi_plus + Psi_minus +
                                     damage.fracture_toughness *
                                         (alpha + damage.characteristic_length * damage.characteristic_length * norm_grad_phi2) /
                                         (c0 * damage.characteristic_length);

    // Compute sigma = J^-1 * tau (tau being the Kirchhoff stress)
    const CeedScalar J_pow = pow(Jm1 + 1., -2. / 3.);
    ComputeDegradedKirchhoffTau_NeoHookean(J_dVdJ, elastic.bulk, 2 * elastic.mu, Jm1, J_pow, damage.residual_stiffness, phi, e_sym, tau_degr_sym);
    for (CeedInt j = 0; j < 6; j++) sigma_degr_sym[j] = tau_degr_sym[j] / (Jm1 + 1);

    //--Store diagnostics
    // Displacement
    diagnostic[0][i] = u[0][i];
    diagnostic[1][i] = u[1][i];
    diagnostic[2][i] = u[2][i];

    // Cauchy stress tensor
    diagnostic[3][i] = sigma_degr_sym[0];
    diagnostic[4][i] = sigma_degr_sym[1];
    diagnostic[5][i] = sigma_degr_sym[2];
    diagnostic[6][i] = sigma_degr_sym[3];
    diagnostic[7][i] = sigma_degr_sym[4];
    diagnostic[8][i] = sigma_degr_sym[5];

    // Hydrostatic pressure; p_h = -trace(sigma) / 3
    diagnostic[9][i] = -RatelMatTraceSymmetric(sigma_degr_sym) / 3.;

    // Strain tensor invariants
    const CeedScalar trace_e2 = RatelMatMatContractSymmetric(1.0, e_sym, e_sym);

    diagnostic[10][i] = trace_e;
    diagnostic[11][i] = trace_e2;

    diagnostic[12][i] = Jm1 + 1;

    // Strain energy (degraded)
    diagnostic[13][i] = strain_energy;

    // von Mises stress
    const CeedScalar trace_sigma_degr = RatelMatTraceSymmetric(sigma_degr_sym);
    RatelMatDeviatoricSymmetric(trace_sigma_degr, sigma_degr_sym, sigma_dev_degr_sym);
    const CeedScalar sigma_dev_degr_contract = RatelMatMatContractSymmetric(1.0, sigma_dev_degr_sym, sigma_dev_degr_sym);

    diagnostic[14][i] = sqrt(3 * sigma_dev_degr_contract / 2);

    // Compute mass density values
    // -- Set to 0 as default (rho should be 0.0 as defualt anyways)
    diagnostic[15][i] = 0.0;

    // -- if mass density is requested then compute mass density: rho*J
    if (rho[i] > 0.0) diagnostic[15][i] = rho[i] / diagnostic[12][i];

    // Additional diagnostic quantities for damage model
    // Damage
    diagnostic[16][i] = phi;

    // Psi_vol (undegraded)
    diagnostic[17][i] = Psi_vol;

    // Psi_dev (undegraded)
    diagnostic[18][i] = Psi_iso;

    for (CeedInt j = 0; j < NUM_COMPONENTS_DIAGNOSTIC_NeoHookean_Damage; j++) diagnostic[j][i] *= wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute (degraded) strain energy for Neo-hookean hyperelasticity

  @param[in]   ctx  QFunction context holding RatelElasticityDamageParams
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - state (10 components: Psi_plus, 9 Grad_u)
                      - 2 - u (4 components: 3 displacement components (unused), 1 scalar damage field)
                      - 3 - gradient of u with respect to reference coordinates (unused)
                      - 4 - rho (unused)
                      - 5 - elastic parameters `RatelNeoHookeanElasticityPointFields`
                      - 6 - damage parameters `RatelElasticityDamagePointFields`
  @param[out]  out  Output arrays
                      - 0 - (degraded) strain energy

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityDamageStrainEnergy_NeoHookean_MPM)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar *state               = in[1];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[2];
  const CeedScalar(*ug)[4][CEED_Q_VLA]  = (const CeedScalar(*)[4][CEED_Q_VLA])in[3];
  const CeedScalar *elastic_paramters   = in[5];
  const CeedScalar *damage_parameters   = in[6];

  // Outputs
  CeedScalar(*degr_energy) = (CeedScalar(*))out[0];

  // Context
  const RatelElasticityDamageParams *context = (RatelElasticityDamageParams *)ctx;
  const bool                         use_AT1 = context->use_AT1;

  // Set c0 based on AT1 or AT2
  const CeedScalar c0 = use_AT1 ? 8. / 3. : 2.;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar V, grad_phi[3], dXdx[3][3], E_sym[6], grad_u[3][3], Psi_plus;

    // Parameters
    RatelNeoHookeanElasticityPointFields elastic;
    RatelElasticityDamagePointFields     damage;

    RatelStoredValuesUnpack(Q, i, 0, sizeof(elastic) / sizeof(CeedScalar), elastic_paramters, (CeedScalar *)&elastic);
    RatelStoredValuesUnpack(Q, i, 0, sizeof(damage) / sizeof(CeedScalar), damage_parameters, (CeedScalar *)&damage);

    // State (Psi_plus and grad_u)
    RatelStoredValuesUnpack(Q, i, 1, 9, state, (CeedScalar *)grad_u);

    // Qdata
    const CeedScalar wdetJ = q_data[0][i];
    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Compute Jm1, and E
    const CeedScalar Jm1 = RatelMatDetAM1(grad_u);
    RatelGreenLagrangeStrain(grad_u, E_sym);

    // Compute volumetric function and derivatives
    VolumetricFunctionAndDerivatives(Jm1, &V, NULL, NULL);

    // Compute Psi
    const CeedScalar phi      = u[3][i];
    const CeedScalar g        = (1 - phi) * (1 - phi) * (1 - damage.residual_stiffness) + damage.residual_stiffness;
    const CeedScalar phi_g[3] = {ug[0][3][i], ug[1][3][i], ug[2][3][i]};
    const CeedScalar alpha    = use_AT1 ? phi : phi * phi;

    // Compute grad_phi = dX/dx^T dphi/dX
    RatelMatTransposeVecMult(1.0, dXdx, phi_g, grad_phi);
    const CeedScalar norm_grad_phi2 = RatelDot3(grad_phi, grad_phi);

    // Compute psi_plus and phi_minus
    const CeedScalar trace_E = RatelMatTraceSymmetric(E_sym);
    PsiPlus_NeoHookean(V, elastic.mu, elastic.bulk, Jm1, trace_E, &Psi_plus);
    const CeedScalar Psi_minus = (Jm1 > 0) ? 0 : elastic.bulk * V;

    // Strain energy W_bar(u, phi)
    const CeedScalar strain_energy = g * Psi_plus + Psi_minus +
                                     damage.fracture_toughness *
                                         (alpha + damage.characteristic_length * damage.characteristic_length * norm_grad_phi2) /
                                         (c0 * damage.characteristic_length);

    degr_energy[i] = strain_energy * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute dual space diagnostic values for NeoHookean hyperelasticity with damage

  @param[in]   ctx  QFunction context, unused
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - volumetric qdata with respect to X
                      - 1 - state (10 components: Psi_plus, 9 Grad_u)
                      - 3 - rho
  @param[out]  out  Output array
                      - 0 - nodal volume

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityDamageDualDiagnostic_NeoHookean_MPM)(void *ctx, const CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar *state               = in[1];
  const CeedScalar *rho                 = in[3];

  // Outputs
  CeedScalar(*v)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar grad_u[3][3];

    // State
    RatelStoredValuesUnpack(Q, i, 1, 9, state, (CeedScalar *)grad_u);

    // Qdata
    const CeedScalar wdetJ = q_data[0][i];

    // Compute Jm1
    const CeedScalar Jm1 = RatelMatDetAM1(grad_u);

    // Nodal volume
    v[0][i] = 1.0 * wdetJ;

    // Nodal density: rho_0 = rho / J
    v[1][i] = wdetJ * rho[i] / (Jm1 + 1);
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
