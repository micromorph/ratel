/// @file
/// Ratel neo-Hookean hyperelasticity at finite strain in current configuration with autodifferentiation (ADOL-C) QFunction source
#include <ceed/types.h>
#ifndef CEED_RUNNING_JIT_PASS
#include <math.h>
#endif

#include "../../models/elasticity-linear.h"          // IWYU pragma: export
#include "../../models/neo-hookean.h"                // IWYU pragma: export
#include "../utils-cpp.h"                            // IWYU pragma: export
#include "elasticity-common-cpp.h"                   // IWYU pragma: export
#include "elasticity-neo-hookean-ad-adolc-common.h"  // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

#define NUM_COMPONENTS_STATE_NeoHookeanCurrent_AD_ADOLC 0
#define NUM_COMPONENTS_STORED_NeoHookeanCurrent_AD_ADOLC 21
#define NUM_ACTIVE_FIELD_EVAL_MODES_NeoHookeanCurrent_AD_ADOLC 1

/**
  @brief Compute `P` for neo-Hookean hyperelasticity in current configuration with ADOL-C

  @param[in]   ctx                   QFunction context, holding `RatelNeoHookeanElasticityParams`
  @param[in]   Q                     Number of quadrature points
  @param[in]   i                     Current quadrature point
  @param[in]   in                    Input arrays
                                       - 0 - volumetric qdata
                                       - 1 - gradient of u with respect to reference coordinates
  @param[out]  out                   Output arrays
                                       - 0 - stored gradient of u with respect to physical coordinates, Green Euler strain,
                                             and gradient of strain energy
  @param[out]  dXdx                  Coordinate transformation
  @param[out]  f1                    `f1 = P = F * S`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int f1_NeoHookeanCurrent_AD_ADOLC(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in,
                                                        CeedScalar *const *out, CeedScalar dXdx[3][3], CeedScalar f1[3][3]) {
  // Inputs
  typedef CeedScalar(*VLATy)[CEED_Q_VLA];
  const VLATy q_data = (VLATy)in[0];
  typedef CeedScalar(*VLATy3)[3][CEED_Q_VLA];
  const VLATy3 ug = (VLATy3)in[1];

  // Outputs
  CeedScalar *stored_values = out[0];

  // Context
  const RatelNeoHookeanElasticityParams *context = (RatelNeoHookeanElasticityParams *)ctx;
  const CeedScalar                       mu      = context->mu;
  const CeedScalar                       lambda  = context->lambda;

  CeedScalar dXdx_initial[3][3], dudX[3][3], Grad_u[3][3], F_inv[3][3], e_sym[6], tau_sym[6], tau[3][3], b_sym[6], b[3][3], gradPsi_sym[6],
      gradPsi[3][3];

  // Qdata
  // dXdx_initial = dX/dx_initial
  // X is natural coordinate sys OR Reference [-1, 1]^dim
  // x_initial is initial config coordinate system
  RatelQdataUnpack_cpp(Q, i, RATEL_VLA_PASS2(q_data), dXdx_initial);

  // Read spatial derivatives of u; du/dX
  RatelGradUnpack_cpp(Q, i, RATEL_VLA_PASS3(ug), dudX);

  // X is natural coordinate sys OR Reference system
  // x_initial is initial config coordinate system
  // Grad_u = du/dx_initial = du/dX * dX/dx_initial
  RatelMatMatMult(1.0, dudX, dXdx_initial, Grad_u);

  // Compute the Deformation Gradient : F = I + Grad_u
  const CeedScalar F[3][3] = {
      {Grad_u[0][0] + 1, Grad_u[0][1],     Grad_u[0][2]    },
      {Grad_u[1][0],     Grad_u[1][1] + 1, Grad_u[1][2]    },
      {Grad_u[2][0],     Grad_u[2][1],     Grad_u[2][2] + 1}
  };

  // Compute F^{-1}
  const CeedScalar Jm1  = RatelMatDetAM1(Grad_u);
  const CeedScalar detF = Jm1 + 1.;
  RatelMatInverse(F, detF, F_inv);

  // x is current config coordinate system
  // dXdx = dX/dx = dX/dx_initial * F^{-1}
  // Note that F^{-1} = dx_initial/dx
  RatelMatMatMult(1.0, dXdx_initial, F_inv, dXdx);

  // Compute Green Euler Strain tensor (e)
  RatelGreenEulerStrain(Grad_u, e_sym);

  // Compute the gradient of Psi (dPsi/de) with ADOL-C
  GradientPsi_ADOLC(gradPsi_sym, e_sym, lambda, mu);
  RatelSymmetricMatUnpack(gradPsi_sym, gradPsi);

  // b = 2 e + I
  for (CeedInt j = 0; j < 6; j++) b_sym[j] = 2. * e_sym[j] + (j < 3);
  RatelSymmetricMatUnpack(b_sym, b);

  // Compute Kirchhoff tau = dPsi/de * b
  RatelMatMatMult(1.0, gradPsi, b, tau);
  RatelSymmetricMatPack(tau, tau_sym);

  RatelSymmetricMatUnpack(tau_sym, f1);

  // Store
  RatelStoredValuesPack(Q, i, 0, 9, (CeedScalar *)dXdx, stored_values);
  RatelStoredValuesPack(Q, i, 9, 6, (CeedScalar *)e_sym, stored_values);
  RatelStoredValuesPack(Q, i, 15, 6, (CeedScalar *)gradPsi_sym, stored_values);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute linearization of `P` for neo-Hookean hyperelasticity in current configuration with ADOL-C

  @param[in]   ctx                   QFunction context, holding `RatelNeoHookeanElasticityParams`
  @param[in]   Q                     Number of quadrature points
  @param[in]   i                     Current quadrature point
  @param[in]   in                    Input arrays
                                       - 0 - volumetric qdata
                                       - 1 - stored gradient of u with respect to physical coordinates, Green Euler strain,
                                             and gradient of strain energy
                                       - 2 - gradient of incremental change to u with respect to reference coordinates
  @param[out]  out                   Output arrays, unused
  @param[out]  dXdx                  Coordinate transformation
  @param[out]  df1                   `df1 = dtau - tau * (nabla_x du)^T`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int df1_NeoHookeanCurrent_AD_ADOLC(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in,
                                                         CeedScalar *const *out, CeedScalar dXdx[3][3], CeedScalar df1[3][3]) {
  // Inputs
  typedef CeedScalar(*VLATy)[CEED_Q_VLA];
  const VLATy       q_data        = (VLATy)in[0];
  const CeedScalar *stored_values = in[1];
  typedef CeedScalar(*VLATy3)[3][CEED_Q_VLA];
  const VLATy3 dug = (VLATy3)in[2];

  // Context
  const RatelNeoHookeanElasticityParams *context = (RatelNeoHookeanElasticityParams *)ctx;
  const CeedScalar                       mu      = context->mu;
  const CeedScalar                       lambda  = context->lambda;

  CeedScalar grad_du[3][3], ddudX[3][3], e_sym[6], de_sym[6], de[3][3], b_sym[6], b[3][3], dtau_1[3][3], dtau_2[3][3], dtau[3][3], tau_grad_du[3][3],
      tau[3][3], gradPsi_sym[6], gradPsi[3][3], hessPsi[6][6], dGradPsi[3][3], dGradPsi_sym[6];

  // Qdata
  RatelQdataUnpack_cpp(Q, i, RATEL_VLA_PASS2(q_data), dXdx);

  // Read spatial derivatives of increment u; d(du)/dX = d(du)/dX
  RatelGradUnpack_cpp(Q, i, RATEL_VLA_PASS3(dug), ddudX);

  // Retrieve dXdx
  RatelStoredValuesUnpack(Q, i, 0, 9, stored_values, (CeedScalar *)dXdx);

  // Compute grad_du = ddu/dX * dX/dx
  // X is ref coordinate [-1,1]^3; x is physical coordinate in current configuration
  RatelMatMatMult(1.0, ddudX, dXdx, grad_du);

  // Compute b = 2 e + I
  RatelStoredValuesUnpack(Q, i, 9, 6, stored_values, (CeedScalar *)e_sym);
  for (CeedInt j = 0; j < 6; j++) b_sym[j] = 2 * e_sym[j] + (j < 3);
  RatelSymmetricMatUnpack(b_sym, b);

  // Compute de = db / 2 = (grad_du b + b (grad_du)^T) / 2
  RatelGreenEulerStrain_fwd(grad_du, b, de_sym);
  RatelSymmetricMatUnpack(de_sym, de);

  // Retrieve gradient of strain energy
  RatelStoredValuesUnpack(Q, i, 15, 6, stored_values, (CeedScalar *)gradPsi_sym);
  RatelSymmetricMatUnpack(gradPsi_sym, gradPsi);

  // Compute Kirchhoff tau = dPsi/de * b
  RatelMatMatMult(1.0, gradPsi, b, tau);

  // Compute the hessian of strain energy with ADOL-C
  HessianPsi_ADOLC(hessPsi, e_sym, lambda, mu);

  //-------------------------------------------------------------------------
  // Compute dtau
  //
  // dtau = (hessPsi : de) b + 2 gradPsi (I_4 : de)
  //      = dGradPsi b + 2 gradPsi de
  //      = dtau_1 + dtau_2
  //-------------------------------------------------------------------------
  // Compute dGradPsi = hessPsi : de
  for (CeedInt i = 0; i < 6; i++) {
    dGradPsi_sym[i] = 0.;
    for (CeedInt j = 0; j < 6; j++) dGradPsi_sym[i] += hessPsi[i][j] * de_sym[j];
  }
  RatelSymmetricMatUnpack(dGradPsi_sym, dGradPsi);

  // Compute dtau_1 = dGradPsi b
  RatelMatMatMult(1.0, dGradPsi, b, dtau_1);

  // Compute dtau_2 = 2 gradPsi de
  RatelSymmetricMatUnpack(gradPsi_sym, gradPsi);
  RatelMatMatMult(2., gradPsi, de, dtau_2);

  // Compute dtau = dtau_1 + dtau_2
  RatelMatMatAdd(1., dtau_1, 1., dtau_2, dtau);
  //-------------------------------------------------------------------------

  // Compute tau_grad_du = tau * grad_du^T
  RatelMatMatTransposeMult(1.0, tau, grad_du, tau_grad_du);

  // Compute df1 = dtau - tau * grad_du^T
  for (CeedInt j = 0; j < 3; j++) {
    for (CeedInt k = 0; k < 3; k++) {
      df1[j][k] = dtau[j][k] - tau_grad_du[j][k];
    }
  }
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute residual for neo-Hookean hyperelasticity in current configuration with ADOL-C

  @param[in]   ctx  QFunction context, holding `RatelNeoHookeanElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityResidual_NeoHookeanCurrent_AD_ADOLC)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return ElasticityResidual(ctx, Q, f1_NeoHookeanCurrent_AD_ADOLC, !!NUM_COMPONENTS_STATE_NeoHookeanCurrent_AD_ADOLC,
                            !!NUM_COMPONENTS_STORED_NeoHookeanCurrent_AD_ADOLC, NUM_ACTIVE_FIELD_EVAL_MODES_NeoHookeanCurrent_AD_ADOLC, in, out);
}

/**
  @brief Evaluate Jacobian for neo-Hookean hyperelasticity in current configuration with ADOL-C

  @param[in]   ctx  QFunction context, holding `RatelNeoHookeanElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityJacobian_NeoHookeanCurrent_AD_ADOLC)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return ElasticityJacobian(ctx, Q, df1_NeoHookeanCurrent_AD_ADOLC, !!NUM_COMPONENTS_STATE_NeoHookeanCurrent_AD_ADOLC,
                            !!NUM_COMPONENTS_STORED_NeoHookeanCurrent_AD_ADOLC, NUM_ACTIVE_FIELD_EVAL_MODES_NeoHookeanCurrent_AD_ADOLC, in, out);
}

/// @}
