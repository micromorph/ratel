/// @file
/// Ratel neo-Hookean poroelasticity at finite strain common QFunction source
#pragma once

#include <ceed/types.h>

#include "../../models/poroelasticity-linear.h"       // IWYU pragma: export
#include "../../models/poroelasticity-neo-hookean.h"  // IWYU pragma: export
#include "../utils.h"                                 // IWYU pragma: export
#include "elasticity-diagnostic-common.h"             // IWYU pragma: export
#include "elasticity-neo-hookean-common.h"            // IWYU pragma: export
#include "elasticity-volumetric-energy-common.h"      // IWYU pragma: export
#include "elasticity-volumetric-stress-common.h"      // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

#define FLOPS_PorosityFunction 7
#define FLOPS_PorosityFunctionDerivative 11
#define FLOPS_Tau_PoroElasticityNeoHookean (FLOPS_Tau_NeoHookean + 9)
#define FLOPS_FdSFTranspose_PoroElasticityNeoHookean (FLOPS_FdSFTranspose_NeoHookean + 3 + FLOPS_MatMatAddSymmetric + 21)
#define NUM_COMPONENTS_DIAGNOSTIC_PoroElasticityNeoHookean 17

/**
  @brief Compute porosity function and its first derivative for poroelasticity.
  For example, Kozeny-Carman relation is
  `F(phi) = phi^3 / (1 - phi^2), dF/dphi = phi^2 (3 - phi^2) / (1 - phi^2)^2`

  @param[in]   phi           Fluid porosity.
  @param[out]  F            `F(phi)`
  @param[out]  dFdphi       `dF/dphi`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int PorosityFunctionAndDerivative(CeedScalar phi, CeedScalar *F, CeedScalar *dFdphi) {
  const CeedScalar phi2 = pow(phi, 2.0);
  const CeedScalar phi3 = pow(phi, 3.0);

  if (F) *F = phi3 / (1. - phi2);
  if (dFdphi) *dFdphi = phi2 * (3. - phi2) / pow(1. - phi2, 2.);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute total Kirchoff tau for neo-Hookean poroelasticity.

  `tau = tau_effective - J B p I`
  `tau_effective` is similar to single field Neo-Hookean

  @param[in]   J         Determinant of deformation gradient
  @param[in]   J_dVdJ    J dV/dJ
  @param[in]   lambda_d  Lamé parameter (drained)
  @param[in]   two_mu_d  Shear modulus (drained) multiplied by 2
  @param[in]   B         Biot's coefficient
  @param[in]   p         Fluid pressure
  @param[in]   e_sym     Green Euler strain, in symmetric representation
  @param[out]  tau_sym   Kirchoff tau, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelKirchhoffTau_PoroElasticityNeoHookean(CeedScalar J, CeedScalar J_dVdJ, CeedScalar lambda_d, CeedScalar two_mu_d,
                                                                     CeedScalar B, CeedScalar p, const CeedScalar e_sym[6], CeedScalar tau_sym[6]) {
  // Compute effective Kirchhoff stress
  RatelKirchhoffTau_NeoHookean(J_dVdJ, lambda_d, two_mu_d, e_sym, tau_sym);

  tau_sym[0] -= J * B * p;
  tau_sym[1] -= J * B * p;
  tau_sym[2] -= J * B * p;
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute F*dS*F^T for neo-Hookean poroelasticity in current configuration.

  @param[in]   J               Determinant of deformation gradient
  @param[in]   J_dVdJ          J dV/dJ
  @param[in]   J2_d2VdJ2       J^2 d^2V/dJ^2
  @param[in]   lambda_d        Lamé parameter (drained)
  @param[in]   mu_d            Shear modulus (drained)
  @param[in]   B               Biot's coefficient
  @param[in]   p               Fluid pressure
  @param[in]   dp              Fluid pressure increment
  @param[in]   grad_du         Gradient of incremental change in u
  @param[out]  depsilon        depsilon = (grad_du + grad_du^T)/2
  @param[out]  FdSFTranspose   F*dS*F^T needed for computing df1 in current configuration
  @param[out]  trace_depsilon  `trace(depsilon)`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelComputeFdSFTranspose_PoroElasticityNeoHookean(CeedScalar J, CeedScalar J_dVdJ, CeedScalar J2_d2VdJ2,
                                                                             CeedScalar lambda_d, CeedScalar mu_d, CeedScalar B, CeedScalar p,
                                                                             CeedScalar dp, const CeedScalar grad_du[3][3], CeedScalar depsilon[3][3],
                                                                             CeedScalar FdSFTranspose[3][3], CeedScalar *trace_depsilon) {
  CeedScalar depsilon_sym[6], FdSFTranspose_vol_sym[6], FdS_effectiveFTranspose_sym[6], FdSFTranspose_sym[6];
  // Compute depsilon = (grad_du + grad_du^T)/2
  depsilon_sym[0] = grad_du[0][0];
  depsilon_sym[1] = grad_du[1][1];
  depsilon_sym[2] = grad_du[2][2];
  depsilon_sym[3] = (grad_du[1][2] + grad_du[2][1]) / 2.;
  depsilon_sym[4] = (grad_du[0][2] + grad_du[2][0]) / 2.;
  depsilon_sym[5] = (grad_du[0][1] + grad_du[1][0]) / 2.;
  RatelSymmetricMatUnpack(depsilon_sym, depsilon);

  *trace_depsilon = RatelMatTraceSymmetric(depsilon_sym);

  // F*dS_vol*F^T
  RatelComputeFdSFTransposeVolumetric(J_dVdJ, J2_d2VdJ2, lambda_d, *trace_depsilon, depsilon_sym, FdSFTranspose_vol_sym);

  // For Neo-Hookean model we have F*dS_effective*F^T = F*dS_vol*F^T + 2 mu depsilon
  RatelMatMatAddSymmetric(1.0, FdSFTranspose_vol_sym, 2 * mu_d, depsilon_sym, FdS_effectiveFTranspose_sym);

  // F*dS*F^T = F*dS_effective*F^T + 2 B*J*p*depsilon - B*J*(trace_depsilon*p + dp)*I
  RatelMatMatAddSymmetric(1.0, FdS_effectiveFTranspose_sym, 2 * B * J * p, depsilon_sym, FdSFTranspose_sym);
  FdSFTranspose_sym[0] -= B * J * (*trace_depsilon * p + dp);
  FdSFTranspose_sym[1] -= B * J * (*trace_depsilon * p + dp);
  FdSFTranspose_sym[2] -= B * J * (*trace_depsilon * p + dp);
  RatelSymmetricMatUnpack(FdSFTranspose_sym, FdSFTranspose);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute strain energy for Neo-Hookean poroelasticity

  @param[in]   ctx  QFunction context, holding `RatelNeoHookeanPoroElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - gradient of u with respect to reference coordinates
                      - 2 - p
  @param[out]  out  Output arrays
                      - 0 - strain energy

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(StrainEnergy_PoroElasticityNeoHookean)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*energy) = (CeedScalar(*))out[0];

  // Context
  const RatelNeoHookeanPoroElasticityParams *context  = (RatelNeoHookeanPoroElasticityParams *)ctx;
  const CeedScalar                           lambda_d = context->lambda_d;
  const CeedScalar                           mu_d     = context->mu_d;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar dXdx[3][3], grad_u[3][3], e_sym[6], V, strain_energy;

    // Qdata
    const CeedScalar wdetJ = q_data[0][i];

    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Read spatial derivatives of u; du/dX
    CeedScalar dudX[3][3];
    RatelGradUnpack(Q, i, ug, dudX);

    // Compute grad_u = du/dX * dX/dx
    // X is ref coordinate [-1,1]^3; x is physical coordinate
    RatelMatMatMult(1.0, dudX, dXdx, grad_u);

    // e - Euler strain tensor
    RatelGreenEulerStrain(grad_u, e_sym);
    const CeedScalar trace_e = RatelMatTraceSymmetric(e_sym);
    const CeedScalar Jm1     = RatelMatDetAM1(grad_u);

    VolumetricFunctionAndDerivatives(Jm1, &V, NULL, NULL);
    RatelStrainEnergy_NeoHookean(V, lambda_d, mu_d, Jm1, trace_e, &strain_energy);

    // Strain energy psi(e) for Neo-Hookean poroelasticity
    energy[i] = strain_energy * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
  (void)StrainEnergy_NeoHookean;
}

/**
  @brief Compute projected diagnostic values for Neo-Hookean poroelasticity

  @param[in]   ctx  QFunction context, holding `RatelNeoHookeanPoroElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - u
                      - 2 - gradient of u with respect to reference coordinates
                      - 3 - p
  @param[out]  out  Output arrays
                      - 0 - projected diagnostic values

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(Diagnostic_PoroElasticityNeoHookean)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[1];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[2];
  const CeedScalar(*p)                  = in[3];

  // Outputs
  CeedScalar(*diagnostic)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Context
  const RatelNeoHookeanPoroElasticityParams *context  = (RatelNeoHookeanPoroElasticityParams *)ctx;
  const CeedScalar                           lambda_d = context->lambda_d;
  const CeedScalar                           mu_d     = context->mu_d;
  const CeedScalar                           B        = context->B;
  const CeedScalar                           two_mu_d = context->two_mu_d;
  const CeedScalar                           phi_0    = context->phi_0;
  const CeedScalar                           rho      = context->common_parameters[RATEL_COMMON_PARAMETER_RHO];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar dXdx[3][3], dudX[3][3], grad_u[3][3], e_sym[6], tau_sym[6], sigma_sym[6], sigma_dev_sym[6], V, J_dVdJ, strain_energy;

    // Qdata
    const CeedScalar wdetJ = q_data[0][i];
    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Read spatial derivatives of u; du/dX
    RatelGradUnpack(Q, i, ug, dudX);

    // Compute grad_u = du/dX * dX/dx
    // X is ref coordinate [-1,1]^3; x is physical coordinate
    RatelMatMatMult(1.0, dudX, dXdx, grad_u);

    // Compute J-1 and volumetric energy and its first derivative
    const CeedScalar Jm1 = RatelMatDetAM1(grad_u);
    VolumetricFunctionAndDerivatives(Jm1, &V, &J_dVdJ, NULL);

    RatelGreenEulerStrain(grad_u, e_sym);
    RatelKirchhoffTau_PoroElasticityNeoHookean(Jm1 + 1., J_dVdJ, lambda_d, two_mu_d, B, p[i], e_sym, tau_sym);
    for (CeedInt j = 0; j < 6; j++) sigma_sym[j] = tau_sym[j] / (Jm1 + 1.);

    // Strain tensor invariants
    const CeedScalar trace_e  = RatelMatTraceSymmetric(e_sym);
    const CeedScalar trace_e2 = RatelMatMatContractSymmetric(1.0, e_sym, e_sym);

    // Strain energy Neo-Hookean
    RatelStrainEnergy_NeoHookean(V, lambda_d, mu_d, Jm1, trace_e, &strain_energy);

    // Displacement
    diagnostic[0][i] = u[0][i];
    diagnostic[1][i] = u[1][i];
    diagnostic[2][i] = u[2][i];

    // Cauchy stress tensor
    diagnostic[3][i] = sigma_sym[0];
    diagnostic[4][i] = sigma_sym[5];
    diagnostic[5][i] = sigma_sym[4];
    diagnostic[6][i] = sigma_sym[1];
    diagnostic[7][i] = sigma_sym[3];
    diagnostic[8][i] = sigma_sym[2];

    // Pressure; as computed by model
    diagnostic[9][i] = p[i];

    // Strain tensor invariants
    diagnostic[10][i] = trace_e;
    diagnostic[11][i] = trace_e2;

    diagnostic[12][i] = Jm1 + 1.;

    // strain energy Neo-Hookean poroelasticity
    diagnostic[13][i] = strain_energy;

    // Compute von-Mises stress

    // -- Compute the the deviatoric part of Cauchy stress: sigma_dev = sigma - trace(sigma)/3
    const CeedScalar trace_sigma = RatelMatTraceSymmetric(sigma_sym);

    RatelMatDeviatoricSymmetric(trace_sigma, sigma_sym, sigma_dev_sym);

    // -- sigma_dev:sigma_dev
    const CeedScalar sigma_dev_contract = RatelMatMatContractSymmetric(1.0, sigma_dev_sym, sigma_dev_sym);

    // -- Compute von-Mises stress: sigma_von = sqrt(3/2 sigma_dev:sigma_dev)
    diagnostic[14][i] = sqrt(3. * sigma_dev_contract / 2.);

    // Compute mass density values

    // -- Set to 0 as default (rho should be 0.0 as defualt anyways)
    diagnostic[15][i] = 0.0;

    // -- if mass density is requested then compute mass density: rho/J
    if (rho > 0.0) diagnostic[15][i] = rho / diagnostic[12][i];

    // Porosity
    const CeedScalar phi_0s = 1 - phi_0;
    diagnostic[16][i]       = 1 - phi_0s / (Jm1 + 1.);

    // Quadrature weight
    for (CeedInt j = 0; j < NUM_COMPONENTS_DIAGNOSTIC_PoroElasticityNeoHookean; j++) diagnostic[j][i] *= wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
  (void)Diagnostic_NeoHookean;
}

/// @}
