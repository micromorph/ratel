/// @file
/// Ratel isochoric stress Mooney-Rivlin for mixed/single fields hyperelasticity at finite strain common QFunction source
#pragma once

#include <ceed/types.h>
#ifndef CEED_RUNNING_JIT_PASS
#include <math.h>
#endif

#include "../../models/mooney-rivlin.h"  // IWYU pragma: export
#include "../utils.h"                    // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

#define FLOPS_Tau_iso_MooneyRivlin                                                                                     \
  (FLOPS_MatTrace + FLOPS_MatDeviatoricSymmetric + 9 + FLOPS_MatMatMultSymmetric + 2 * FLOPS_MatMatAddSymmetric + 24 + \
   FLOPS_MatMatContractSymmetric + 15)
#define FLOPS_S_iso_MooneyRivlin                                                                                        \
  (FLOPS_MatTrace + FLOPS_MatDeviatoricSymmetric + FLOPS_MatMatMultSymmetric + 16 + 2 * FLOPS_MatMatAddSymmetric + 18 + \
   FLOPS_MatMatContractSymmetric + 7)
#define FLOPS_dS1_iso_MooneyRivlin                                                                                                          \
  (FLOPS_GreenLagrangeStrain_fwd + FLOPS_CInverse_fwd + FLOPS_MatMatContractSymmetric + 2 * FLOPS_MatTrace + FLOPS_MatDeviatoricSymmetric + \
   FLOPS_MatMatMultSymmetric + 3 + 2 * FLOPS_MatMatAddSymmetric + 24 + 20)
#define FLOPS_dS2_iso_MooneyRivlin (24 + FLOPS_MatMatAddSymmetric + 12 + 5)
#define FLOPS_dS3_iso_MooneyRivlin (2 * FLOPS_MatMatContractSymmetric + FLOPS_MatMatAddSymmetric + 12 + 22)
#define FLOPS_dS_iso_MooneyRivlin (FLOPS_dS1_iso_MooneyRivlin + FLOPS_dS2_iso_MooneyRivlin + FLOPS_dS3_iso_MooneyRivlin + 18)
#define FLOPS_FdSFTranspose_iso_MooneyRivlin                                                                                                        \
  (6 + 3 * FLOPS_MatTrace + 4 + 3 * FLOPS_ScalarMatMultSymmetric + 12 + 8 + FLOPS_MatDeviatoricSymmetric + 2 * FLOPS_MatMatContractSymmetric + 16 + \
   12 + FLOPS_MatMatMultSymmetric + FLOPS_MatMatMatMultSymmetric + FLOPS_GreenEulerStrain_fwd + 23 + 2 * FLOPS_MatMatMatAddSymmetric + 18 + 12 +    \
   FLOPS_MatMatAddSymmetric)

/**
  @brief Compute isochoric part of Kirchoff tau for Mooney-Rivlin hyperelasticity.

  `tau_iso = 2 (mu_1 * J^{-2/3} + 2 mu_2 * J^{-4/3}) e_dev + 2 mu_2 * J^{-4/3} (trace(e) b - e*b) - 4/3 mu_2 * J^{-4/3}*(I1(e) + 2* I2(e)) * I`

  @param[in]   mu_1         First Mooney-Rivlin parameter
  @param[in]   two_mu_2     Two times the second Mooney-Rivlin parameter
  @param[in]   J_pow        J^{-2/3}
  @param[in]   e_sym        Green Euler strain, in symmetric representation
  @param[out]  tau_iso_sym  Isochoric Kirchoff tau, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelIsochoricKirchhoffTau_MooneyRivlin(CeedScalar mu_1, CeedScalar two_mu_2, CeedScalar J_pow, const CeedScalar e_sym[6],
                                                                  CeedScalar tau_iso_sym[6]) {
  CeedScalar e_dev_sym[6], b_sym[6], eb_sym[6], bMeb_sym[6];

  // Compute Deviatoric Strain
  // e_dev = e - 1/3 * trace(e) * I
  const CeedScalar trace_e = RatelMatTraceSymmetric(e_sym);

  RatelMatDeviatoricSymmetric(trace_e, e_sym, e_dev_sym);

  // b = 2 e + I
  for (CeedInt j = 0; j < 6; j++) b_sym[j] = 2 * e_sym[j] + (j < 3);

  RatelMatMatMultSymmetric(1.0, e_sym, b_sym, eb_sym);
  // bMeb_sym = (trace(e)b - e*b)
  RatelMatMatAddSymmetric(trace_e, b_sym, -1.0, eb_sym, bMeb_sym);
  // -- intermediate tau = 2 (mu_1 * J^{-2/3} + 2 mu_2 * J^{-4/3}) e_dev + 2 mu_2 * J^{-4/3} (trace(e) b - e*b)
  const CeedScalar J_pow2  = J_pow * J_pow;
  const CeedScalar coeff_1 = 2 * (mu_1 * J_pow + two_mu_2 * J_pow2);

  RatelMatMatAddSymmetric(coeff_1, e_dev_sym, two_mu_2 * J_pow2, bMeb_sym, tau_iso_sym);
  // -- Add `- 4/3 mu_2 * J^{-4/3}*(I1(e) + 2* I2(e)) * I`
  const CeedScalar trace_e2 = RatelMatMatContractSymmetric(1.0, e_sym, e_sym);
  const CeedScalar two_I_2  = trace_e * trace_e - trace_e2;
  const CeedScalar coeff_2  = 2 * two_mu_2 * J_pow2 * (trace_e + two_I_2) / 3;

  tau_iso_sym[0] -= coeff_2;
  tau_iso_sym[1] -= coeff_2;
  tau_iso_sym[2] -= coeff_2;
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute isochoric part of second Kirchoff stress for Mooney-Rivlin hyperelasticity.

  @param[in]   mu_1       First Mooney-Rivlin parameter
  @param[in]   two_mu_2   Two times the second Mooney-Rivlin parameter
  @param[in]   J          Determinant of deformation gradient
  @param[in]   C_inv_sym  Inverse of right Cauchy-Green tensor, in symmetric representation
  @param[in]   E_sym      Green Lagrange strain, in symmetric representation
  @param[out]  S_iso_sym  Isochoric second Kirchoff stress, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelIsochoricSecondKirchhoffStress_MooneyRivlin(CeedScalar mu_1, CeedScalar two_mu_2, CeedScalar J,
                                                                           const CeedScalar C_inv_sym[6], const CeedScalar E_sym[6],
                                                                           CeedScalar S_iso_sym[6]) {
  CeedScalar E_dev_sym[6], C_inv_E_dev_sym[6], S1_sym[6];

  // Compute Deviatoric Strain
  // E_dev = E - 1/3 * trace(E) * I
  const CeedScalar trace_E = RatelMatTraceSymmetric(E_sym);

  RatelMatDeviatoricSymmetric(trace_E, E_sym, E_dev_sym);

  // -- C_inv * E_dev
  RatelMatMatMultSymmetric(1.0, C_inv_sym, E_dev_sym, C_inv_E_dev_sym);
  // trace(E)I - E
  CeedScalar tr_EME[6] = {trace_E - E_sym[0], trace_E - E_sym[1], trace_E - E_sym[2], -E_sym[3], -E_sym[4], -E_sym[5]};

  // -- intermediate S = 2 (mu_1 * J^{-2/3} + 2 mu_2 * J^{-4/3}) C_inv * E_dev + 2 mu_2 * J^{-4/3} (trace(E) I - E)
  const CeedScalar J_23    = pow(J, -2. / 3.);
  const CeedScalar J_43    = pow(J, -4. / 3.);
  const CeedScalar coeff_1 = 2 * (mu_1 * J_23 + two_mu_2 * J_43);

  RatelMatMatAddSymmetric(coeff_1, C_inv_E_dev_sym, two_mu_2 * J_43, tr_EME, S1_sym);

  // Add `- 4/3 mu_2 * J^{-4/3}*(I1(E) + 2* I2(E)) * C_inv`
  const CeedScalar trace_E2 = RatelMatMatContractSymmetric(1.0, E_sym, E_sym);
  const CeedScalar two_I_2  = trace_E * trace_E - trace_E2;
  const CeedScalar coeff_2  = -2 * two_mu_2 * J_43 * (trace_E + two_I_2) / 3;

  RatelMatMatAddSymmetric(1.0, S1_sym, coeff_2, C_inv_sym, S_iso_sym);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute derivative of isochoric second Kirchoff stress for Mooney-Rivlin hyperelasticity.

  @param[in]   mu_1              First Mooney-Rivlin parameter
  @param[in]   two_mu_2          Two times the second Mooney-Rivlin parameter
  @param[in]   J                 Determinant of deformation gradient
  @param[in]   F                 Deformation gradient
  @param[in]   grad_du           Gradient of incremental change in u
  @param[in]   C_inv_sym         Inverse of right Cauchy-Green tensor, in symmetric representation
  @param[in]   E_sym             Green Lagrange strain, in symmetric representation
  @param[out]  dC_inv_sym        Derivative of C^{-1}, in symmetric representation
  @param[out]  dS_iso_sym        Derivative of isochoric second Kirchoff stress, in symmetric representation
  @param[out]  Cinv_contract_dE  `C_inv : dE`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelIsochoricSecondKirchhoffStress_MooneyRivlin_fwd(CeedScalar mu_1, CeedScalar two_mu_2, CeedScalar J,
                                                                               const CeedScalar F[3][3], const CeedScalar grad_du[3][3],
                                                                               const CeedScalar C_inv_sym[6], const CeedScalar E_sym[6],
                                                                               CeedScalar dC_inv_sym[6], CeedScalar dS_iso_sym[6],
                                                                               CeedScalar *Cinv_contract_dE) {
  CeedScalar dE_sym[6], E_dev_sym[6], C_inv_E_dev_sym[6], tr_EME[6], tr_dEMdE[6], dS_iso2_sym[6], dS_iso1_sym[6], dS_isoa_sym[6], dS_iso3_sym[6];

  // dE - Green-Lagrange strain tensor increment
  RatelGreenLagrangeStrain_fwd(grad_du, F, dE_sym);
  // dC_inv = -2*C_inv*dE*C_inv
  RatelCInverse_fwd(C_inv_sym, dE_sym, dC_inv_sym);
  // C_inv:dE
  *Cinv_contract_dE = RatelMatMatContractSymmetric(1.0, C_inv_sym, dE_sym);

  // 2) dS_iso = dS_iso1 + dS_iso2 + dS_iso3
  // 2a) dS_iso1 = -4/3 (C_inv:dE) (mu_1 J^(-2/3) + 4 mu_2 J^(-4/3)) C_inv E_dev
  // - 1/3 (mu_1 J^(-2/3) + 2 mu_2 J^(-4/3)) * dS_iso_a
  // Compute Deviatoric Strain
  // E_dev = E - 1/3 * trace(E) * I
  const CeedScalar trace_E = RatelMatTraceSymmetric(E_sym);

  RatelMatDeviatoricSymmetric(trace_E, E_sym, E_dev_sym);
  // -- C_inv * E_dev
  RatelMatMatMultSymmetric(1.0, C_inv_sym, E_dev_sym, C_inv_E_dev_sym);

  // -- dS_iso_a = 2*tr(dE)*C_inv + I1(C) dC_inv
  CeedScalar       I1_C     = 2 * trace_E + 3.;
  const CeedScalar trace_dE = RatelMatTraceSymmetric(dE_sym);

  RatelMatMatAddSymmetric(2 * trace_dE, C_inv_sym, I1_C, dC_inv_sym, dS_isoa_sym);

  // dS_iso1
  const CeedScalar J_23    = pow(J, -2. / 3.);
  const CeedScalar J_43    = pow(J, -4. / 3.);
  const CeedScalar coeff_1 = (-4. / 3.) * (*Cinv_contract_dE) * (mu_1 * J_23 + 2 * two_mu_2 * J_43);
  const CeedScalar coeff_2 = (-1. / 3.) * (mu_1 * J_23 + two_mu_2 * J_43);

  RatelMatMatAddSymmetric(coeff_1, C_inv_E_dev_sym, coeff_2, dS_isoa_sym, dS_iso1_sym);

  // 2b) dS_iso2 = -8/3 (C_inv:dE) mu_2 J^(-4/3) (I1(E)*I - E) + 2 mu_2 J^(-4/3) (tr(dE)*I - dE)
  for (CeedInt j = 0; j < 6; j++) {
    tr_EME[j]   = (j < 3) * trace_E - E_sym[j];
    tr_dEMdE[j] = (j < 3) * trace_dE - dE_sym[j];
  }
  const CeedScalar coeff_3 = (-4. / 3.) * (*Cinv_contract_dE) * (two_mu_2 * J_43);
  const CeedScalar coeff_4 = two_mu_2 * J_43;

  RatelMatMatAddSymmetric(coeff_3, tr_EME, coeff_4, tr_dEMdE, dS_iso2_sym);

  // 2c) dS_iso3 = 16/9 (C_inv:dE) mu_2 J^(-4/3) (I1(E) + 2*I2(E)) C_inv - 4/3 mu_2 J^(-4/3) (I1(E) + 2*I2(E)) dC_inv
  // - 4/3 mu_2 J^(-4/3) (tr(dE) + 2*I1(E)*tr(dE) - 2*E:dE) C_inv
  const CeedScalar trace_E2      = RatelMatMatContractSymmetric(1.0, E_sym, E_sym);
  const CeedScalar two_I_2       = trace_E * trace_E - trace_E2;
  // E:dE
  const CeedScalar E_contract_dE = RatelMatMatContractSymmetric(1.0, E_sym, dE_sym);
  const CeedScalar coeff_5       = (8. / 9.) * (*Cinv_contract_dE) * (two_mu_2 * J_43) * (trace_E + two_I_2);
  const CeedScalar coeff_6       = (-2. / 3.) * (two_mu_2 * J_43) * (trace_E + two_I_2);
  const CeedScalar coeff_7       = (-2. / 3.) * (two_mu_2 * J_43) * (trace_dE + 2 * trace_E * trace_dE - 2 * E_contract_dE);

  RatelMatMatAddSymmetric(coeff_5 + coeff_7, C_inv_sym, coeff_6, dC_inv_sym, dS_iso3_sym);

  for (CeedInt j = 0; j < 6; j++) {
    dS_iso_sym[j] = 0.0;
    dS_iso_sym[j] += dS_iso1_sym[j] + dS_iso2_sym[j] + dS_iso3_sym[j];
  }
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute F*dS_iso*F^T for isochoric Mooney-Rivlin hyperelasticity in current configuration.

  @param[in]   mu_1                   First Mooney-Rivlin parameter
  @param[in]   two_mu_2               Two times the second Mooney-Rivlin parameter
  @param[in]   J_pow                  J^{-2/3}
  @param[in]   grad_du                Gradient of incremental change in u
  @param[in]   e_sym                  Green Euler strain, in symmetric representation
  @param[out]  depsilon_sym           depsilon = (grad_du + grad_du^T)/2
  @param[out]  FdSFTranspose_iso_sym  F*dS_iso*F^T needed for computing df1 in current configuration
  @param[out]  trace_depsilon         `trace(depsilon)`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelComputeFdSFTransposeIsochoric_MooneyRivlin(CeedScalar mu_1, CeedScalar two_mu_2, CeedScalar J_pow,
                                                                          const CeedScalar grad_du[3][3], const CeedScalar e_sym[6],
                                                                          CeedScalar depsilon_sym[6], CeedScalar FdSFTranspose_iso_sym[6],
                                                                          CeedScalar *trace_depsilon) {
  CeedScalar b_sym[6], b[3][3], be_sym[6], e_dev_sym[6], e_dev_sym_scaled[6], depsilon_sym_scaled[6], de_sym[6], b_depsilon_b_sym[6],
      FdSFTranspose_iso_sym1[6];
  // Compute depsilon = (grad_du + grad_du^T)/2
  depsilon_sym[0] = grad_du[0][0];
  depsilon_sym[1] = grad_du[1][1];
  depsilon_sym[2] = grad_du[2][2];
  depsilon_sym[3] = (grad_du[1][2] + grad_du[2][1]) / 2.;
  depsilon_sym[4] = (grad_du[0][2] + grad_du[2][0]) / 2.;
  depsilon_sym[5] = (grad_du[0][1] + grad_du[1][0]) / 2.;

  *trace_depsilon = RatelMatTraceSymmetric(depsilon_sym);

  // F*dS_iso*F^T
  // -- `-4/3 (tr(depsilon)) (mu_1 J^(-2/3) + 4 mu_2 J^(-4/3)) e_dev`
  const CeedScalar trace_e = RatelMatTraceSymmetric(e_sym);
  const CeedScalar J_pow2  = J_pow * J_pow;  // J^{-4/3}
  const CeedScalar coeff_2 = (-4. / 3.) * (*trace_depsilon) * (mu_1 * J_pow + 2 * two_mu_2 * J_pow2);

  RatelMatDeviatoricSymmetric(trace_e, e_sym, e_dev_sym);
  RatelScalarMatMultSymmetric(coeff_2, e_dev_sym, e_dev_sym_scaled);

  // -- `(coeff_3*tr(b) + coeff_7) * depsilon`
  const CeedScalar trace_e2 = RatelMatMatContractSymmetric(1.0, e_sym, e_sym);
  const CeedScalar I1_b     = 2 * trace_e + 3.;
  const CeedScalar two_I_2  = trace_e * trace_e - trace_e2;
  const CeedScalar coeff_3  = (2. / 3.) * (mu_1 * J_pow + two_mu_2 * J_pow2);
  const CeedScalar coeff_4  = (4. / 3.) * (two_mu_2 * J_pow2) * (trace_e + two_I_2);

  RatelScalarMatMultSymmetric(coeff_3 * I1_b + coeff_4, depsilon_sym, depsilon_sym_scaled);

  // -- `b*e, b*depsilon*b, de`
  for (CeedInt j = 0; j < 6; j++) b_sym[j] = 2 * e_sym[j] + (j < 3);
  RatelMatMatMultSymmetric(1.0, b_sym, e_sym, be_sym);
  RatelMatMatMatMultSymmetric(1.0, b_sym, depsilon_sym, b_depsilon_b_sym);
  RatelSymmetricMatUnpack(b_sym, b);
  RatelGreenEulerStrain_fwd(grad_du, b, de_sym);

  // F*dS_iso*F^T
  const CeedScalar trace_de             = RatelMatTraceSymmetric(de_sym);
  const CeedScalar be_contract_depsilon = RatelMatMatContractSymmetric(1.0, be_sym, depsilon_sym);
  const CeedScalar coeff_5              = (-4. / 3.) * (*trace_depsilon) * two_mu_2 * J_pow2;
  const CeedScalar coeff_6              = two_mu_2 * J_pow2;
  const CeedScalar coeff_7              = (8. / 9.) * (*trace_depsilon) * (two_mu_2 * J_pow2) * (trace_e + two_I_2);
  const CeedScalar coeff_8              = (-2. / 3.) * (two_mu_2 * J_pow2) * (trace_de + 2 * trace_e * trace_de - 2 * be_contract_depsilon);

  RatelMatMatMatAddSymmetric(coeff_5 * trace_e + coeff_6 * trace_de, b_sym, -coeff_5, be_sym, -coeff_6, b_depsilon_b_sym, FdSFTranspose_iso_sym1);
  RatelMatMatMatAddSymmetric(1.0, e_dev_sym_scaled, 1.0, depsilon_sym_scaled, 1.0, FdSFTranspose_iso_sym1, FdSFTranspose_iso_sym);
  // Add `(coeff_7 + coeff_8 - coeff_3*tr(de))*I`
  FdSFTranspose_iso_sym[0] += coeff_7 + coeff_8 - coeff_3 * trace_de;
  FdSFTranspose_iso_sym[1] += coeff_7 + coeff_8 - coeff_3 * trace_de;
  FdSFTranspose_iso_sym[2] += coeff_7 + coeff_8 - coeff_3 * trace_de;
  return CEED_ERROR_SUCCESS;
}

/// @}
