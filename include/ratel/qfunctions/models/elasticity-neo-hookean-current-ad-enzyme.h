/// @file
/// Ratel neo-Hookean hyperelasticity at finite strain in current configuration QFunction source
#include <ceed/types.h>
#ifndef CEED_RUNNING_JIT_PASS
#include <math.h>
#endif

#include "../../models/elasticity-linear.h"  // IWYU pragma: export
#include "../../models/neo-hookean.h"        // IWYU pragma: export
#include "../utils.h"                        // IWYU pragma: export
#include "elasticity-common.h"               // IWYU pragma: export
#include "elasticity-neo-hookean-common.h"   // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

#define NUM_COMPONENTS_STATE_NeoHookeanCurrentAD_Enzyme 0
#define NUM_COMPONENTS_STORED_NeoHookeanCurrentAD_Enzyme 15
#define NUM_ACTIVE_FIELD_EVAL_MODES_NeoHookeanCurrentAD_Enzyme 1

/**
  @brief Compute strain energy for neo-Hookean hyperelasticity

  @param[in]   e_sym   Green Euler strain, in symmetric representation
  @param[in]   lambda  Lamé parameter
  @param[in]   mu      Shear modulus

  @return A scalar value: strain energy
**/
CEED_QFUNCTION_HELPER CeedScalar RatelStrainEnergy_NeoHookeanCurrentAD_Enzyme(CeedScalar e_sym[6], CeedScalar lambda, CeedScalar mu) {
  CeedScalar e2_sym[6];

  // J and log(J)
  for (CeedInt i = 0; i < 6; i++) e2_sym[i] = 2 * e_sym[i];
  const CeedScalar detbm1 = RatelMatDetAM1Symmetric(e2_sym);
  const CeedScalar J      = sqrt(detbm1 + 1);
  const CeedScalar logJ   = RatelLog1pSeries(detbm1) / 2.;

  // trace(e)
  const CeedScalar trace_e = RatelMatTraceSymmetric(e_sym);

  return lambda * (J * J - 1) / 4 - lambda * logJ / 2 + mu * (-logJ + trace_e);
}

/// @}

// -----------------------------------------------------------------------------
//  Compute Kirchhoff stress
// -----------------------------------------------------------------------------
// -- Enzyme-AD
void       __enzyme_autodiff(void *, ...);
void       __enzyme_fwddiff(void *, ...);
extern int enzyme_const;

void *__enzyme_function_like[2] = {(void *)RatelLog1pSeries, (void *)"log1p"};

/// @addtogroup RatelMaterials
/// @{

/**
  @brief Compute Kirchoff tau for neo-Hookean hyperelasticity in current configuration with Enzyme

  @param[in]   lambda    Lamé parameter
  @param[in]   mu        Shear modulus
  @param[in]   e_sym     Green Euler strain, in symmetric representation
  @param[out]  tau_sym   Kirchoff tau, in symmetric representation
**/
CEED_QFUNCTION_HELPER void RatelKirchhofftau_sym_NeoHookean_AD(const CeedScalar lambda, const CeedScalar mu, CeedScalar e_sym[6],
                                                               CeedScalar tau_sym[6]) {
  CeedScalar dPsi_sym[6] = {0.}, b_sym[6], dPsi[3][3], b[3][3], tau[3][3];

  // dPsi / de
  __enzyme_autodiff((void *)RatelStrainEnergy_NeoHookeanCurrentAD_Enzyme, e_sym, dPsi_sym, enzyme_const, lambda, enzyme_const, mu);
  for (CeedInt i = 3; i < 6; i++) dPsi_sym[i] /= 2.;

  // b = 2 e + I
  for (CeedInt j = 0; j < 6; j++) b_sym[j] = 2 * e_sym[j] + (j < 3);

  // tau = (dPsi / de) b
  RatelSymmetricMatUnpack(dPsi_sym, dPsi);
  RatelSymmetricMatUnpack(b_sym, b);
  RatelMatMatMult(1., dPsi, b, tau);
  RatelSymmetricMatPack(tau, tau_sym);
}

/**
  @brief Compute derivative of Kirchoff tau for neo-Hookean hyperelasticity in current configuration with Enzyme

  @param[in]   lambda    Lamé parameter
  @param[in]   mu        Shear modulus
  @param[in]   e_sym     Green Euler strain, in symmetric representation
  @param[out]  de_sym    Derivative of Green Euler strain, in symmetric representation
  @param[in]   tau_sym   Kirchoff tau, in symmetric representation
  @param[out]  dtau_sym  Derivative of Kirchoff tau, in symmetric representation
**/
CEED_QFUNCTION_HELPER void Rateldtau_fwd(const CeedScalar lambda, const CeedScalar mu, CeedScalar e_sym[6], CeedScalar de_sym[6],
                                         CeedScalar tau_sym[6], CeedScalar dtau_sym[6]) {
  __enzyme_fwddiff((void *)RatelKirchhofftau_sym_NeoHookean_AD, enzyme_const, lambda, enzyme_const, mu, e_sym, de_sym, tau_sym, dtau_sym);
}

/**
  @brief Compute `tau` for neo-Hookean hyperelasticity in current configuration with Enzyme

  @param[in]   ctx                   QFunction context, holding `RatelNeoHookeanElasticityParams`
  @param[in]   Q                     Number of quadrature points
  @param[in]   i                     Current quadrature point
  @param[in]   in                    Input arrays
                                       - 0 - volumetric qdata
                                       - 1 - gradient of u with respect to reference coordinates
  @param[out]  out                   Output arrays
                                       - 0 - stored dXdx and Green Euler strain
  @param[out]  dXdx                  Coordinate transformation
  @param[out]  f1                    `f1 = tau`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int f1_NeoHookeanCurrentAD_Enzyme(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in,
                                                        CeedScalar *const *out, CeedScalar dXdx[3][3], CeedScalar f1[3][3]) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*stored_values) = out[0];

  // Context
  const RatelNeoHookeanElasticityParams *context = (RatelNeoHookeanElasticityParams *)ctx;
  const CeedScalar                       mu      = context->mu;
  const CeedScalar                       lambda  = context->lambda;

  CeedScalar dXdx_initial[3][3], Grad_u[3][3], F_inv[3][3], e_sym[6], tau_sym[6];

  // Qdata
  // dXdx_initial = dX/dx_initial
  // X is natural coordinate sys OR Reference [-1, 1]^dim
  // x_initial is initial config coordinate system
  RatelQdataUnpack(Q, i, q_data, dXdx_initial);

  // Read spatial derivatives of u; du/dX
  CeedScalar dudX[3][3];
  RatelGradUnpack(Q, i, ug, dudX);

  // X is natural coordinate sys OR Reference system
  // x_initial is initial config coordinate system
  // Grad_u = du/dx_initial = du/dX * dX/dx_initial
  RatelMatMatMult(1.0, dudX, dXdx_initial, Grad_u);

  // Compute the Deformation Gradient : F = I + Grad_u
  const CeedScalar F[3][3] = {
      {Grad_u[0][0] + 1, Grad_u[0][1],     Grad_u[0][2]    },
      {Grad_u[1][0],     Grad_u[1][1] + 1, Grad_u[1][2]    },
      {Grad_u[2][0],     Grad_u[2][1],     Grad_u[2][2] + 1}
  };

  // Compute F^{-1}
  const CeedScalar Jm1  = RatelMatDetAM1(Grad_u);
  const CeedScalar detF = Jm1 + 1.;

  RatelMatInverse(F, detF, F_inv);

  // x is current config coordinate system
  // dXdx = dX/dx = dX/dx_initial * F^{-1}
  // Note that F^{-1} = dx_initial/dx
  RatelMatMatMult(1.0, dXdx_initial, F_inv, dXdx);

  // Compute Green Euler Strain tensor (e)
  RatelGreenEulerStrain(Grad_u, e_sym);

  // Compute Kirchhoff (tau) with Enzyme-AD
  RatelKirchhofftau_sym_NeoHookean_AD(lambda, mu, e_sym, tau_sym);
  RatelSymmetricMatUnpack(tau_sym, f1);

  // Store
  RatelStoredValuesPack(Q, i, 0, 9, (CeedScalar *)dXdx, stored_values);
  RatelStoredValuesPack(Q, i, 9, 6, (CeedScalar *)e_sym, stored_values);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute linearization of `tau` for neo-Hookean hyperelasticity in current configuration with Enzyme

  @param[in]   ctx                   QFunction context, holding `RatelNeoHookeanElasticityParams`
  @param[in]   Q                     Number of quadrature points
  @param[in]   i                     Current quadrature point
  @param[in]   in                    Input arrays
                                       - 0 - volumetric qdata
                                       - 1 - stored dXdx and Green Euler strain
                                       - 2 - gradient of incremental change to u with respect to reference coordinates
  @param[out]  out                   Output arrays, unused
  @param[out]  dXdx                  Coordinate transformation
  @param[out]  df1                   `df1 = dtau - tau * (nabla_x du)^T`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int df1_NeoHookeanCurrentAD_Enzyme(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in,
                                                         CeedScalar *const *out, CeedScalar dXdx[3][3], CeedScalar df1[3][3]) {
  // Inputs
  const CeedScalar(*stored_values)      = in[1];
  const CeedScalar(*dug)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[2];

  // Context
  const RatelNeoHookeanElasticityParams *context = (RatelNeoHookeanElasticityParams *)ctx;
  const CeedScalar                       mu      = context->mu;
  const CeedScalar                       lambda  = context->lambda;

  CeedScalar grad_du[3][3], e_sym[6], b_sym[6], b[3][3], de_sym[6], tau_sym[6], dtau_sym[6], tau[3][3], dtau[3][3], tau_grad_du[3][3];

  // Read spatial derivatives of increment u; d(du)/dX = d(du)/dX
  CeedScalar ddudX[3][3];
  RatelGradUnpack(Q, i, dug, ddudX);

  // Retrieve dXdx
  RatelStoredValuesUnpack(Q, i, 0, 9, stored_values, (CeedScalar *)dXdx);

  // Compute grad_du = ddu/dX * dX/dx
  // X is ref coordinate [-1,1]^3; x is physical coordinate in current configuration
  RatelMatMatMult(1.0, ddudX, dXdx, grad_du);

  // Compute b = 2 e + I
  RatelStoredValuesUnpack(Q, i, 9, 6, stored_values, (CeedScalar *)e_sym);
  for (CeedInt j = 0; j < 6; j++) b_sym[j] = 2 * e_sym[j] + (j < 3);
  RatelSymmetricMatUnpack(b_sym, b);

  // Compute de = db / 2 = (grad_du b + b (grad_du)^T) / 2
  RatelGreenEulerStrain_fwd(grad_du, b, de_sym);

  // Compute dtau with Enzyme-AD
  Rateldtau_fwd(lambda, mu, e_sym, de_sym, tau_sym, dtau_sym);
  RatelSymmetricMatUnpack(tau_sym, tau);
  RatelSymmetricMatUnpack(dtau_sym, dtau);

  // Compute tau_grad_du = tau * grad_du^T
  RatelMatMatTransposeMult(1.0, tau, grad_du, tau_grad_du);

  // Compute df1 = dtau - tau * grad_du^T
  for (CeedInt j = 0; j < 3; j++) {
    for (CeedInt k = 0; k < 3; k++) {
      df1[j][k] = dtau[j][k] - tau_grad_du[j][k];
    }
  }
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute residual for neo-Hookean hyperelasticity in current configuration with Enzyme

  @param[in]   ctx  QFunction context, holding `RatelNeoHookeanElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityResidual_NeoHookeanCurrentAD_Enzyme)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return ElasticityResidual(ctx, Q, f1_NeoHookeanCurrentAD_Enzyme, !!NUM_COMPONENTS_STATE_NeoHookeanCurrentAD_Enzyme,
                            !!NUM_COMPONENTS_STORED_NeoHookeanCurrentAD_Enzyme, NUM_ACTIVE_FIELD_EVAL_MODES_NeoHookeanCurrentAD_Enzyme, in, out);
}

/**
  @brief Evaluate Jacobian for neo-Hookean hyperelasticity in current configuration with Enzyme

  @param[in]   ctx  QFunction context, holding `RatelNeoHookeanElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityJacobian_NeoHookeanCurrentAD_Enzyme)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return ElasticityJacobian(ctx, Q, df1_NeoHookeanCurrentAD_Enzyme, !!NUM_COMPONENTS_STATE_NeoHookeanCurrentAD_Enzyme,
                            !!NUM_COMPONENTS_STORED_NeoHookeanCurrentAD_Enzyme, NUM_ACTIVE_FIELD_EVAL_MODES_NeoHookeanCurrentAD_Enzyme, in, out);
}

/// @}
