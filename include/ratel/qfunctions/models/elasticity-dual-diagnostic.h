/// @file
/// Ratel common dual space diagnostic values for elasticity
#include <ceed/types.h>

#include "../../models/common-parameters.h"  // IWYU pragma: export
#include "../utils.h"                        // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

#define NUM_COMPONENTS_ELASTICITY_DIAGNOSTIC_Dual 2

/**
  @brief Compute dual space diagnostic values

  @param[in]   ctx  QFunction context, unused
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - volumetric qdata
  @param[out]  out  Output array
                      - 0 - nodal volume

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityDualDiagnostic)(void *ctx, const CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[2];

  // Outputs
  CeedScalar(*v)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Context
  const CeedScalar *context = (const CeedScalar *)ctx;
  const CeedScalar  rho_0   = context[RATEL_COMMON_PARAMETER_RHO];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar grad_u[3][3], dXdx[3][3];

    // Qdata
    const CeedScalar wdetJ = q_data[0][i];

    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Read spatial derivatives of u; du/dX
    CeedScalar dudX[3][3];
    RatelGradUnpack(Q, i, ug, dudX);

    // Compute grad_u = du/dX * dX/dx
    // X is ref coordinate [-1,1]^3; x is physical coordinate
    RatelMatMatMult(1.0, dudX, dXdx, grad_u);

    // Compute J-1 and logJ
    const CeedScalar Jm1 = RatelMatDetAM1(grad_u);
    const CeedScalar J   = Jm1 + 1.0;

    // Nodal volume
    v[0][i] = 1.0 * wdetJ;

    // Nodal density: rho_0 = rho / J
    v[1][i] = rho_0 / J * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
