/// @file
/// Ratel linear poroelasticity  QFunction source
#include <ceed/types.h>
#ifndef CEED_RUNNING_JIT_PASS
#include <math.h>
#endif

#include "../../models/poroelasticity-linear.h"  // IWYU pragma: export
#include "../boundaries/platen-nitsche.h"        // IWYU pragma: export
#include "../utils.h"                            // IWYU pragma: export
#include "poroelasticity-common.h"               // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

#define NUM_COMPONENTS_STATE_PoroElasticityLinear 0
#define NUM_COMPONENTS_STORED_PoroElasticityLinear 0
#define NUM_ACTIVE_FIELD_EVAL_MODES_PoroElasticityLinear_u 1
#define NUM_ACTIVE_FIELD_EVAL_MODES_PoroElasticityLinear_p 2
#define NUM_ACTIVE_FIELD_EVAL_MODES_PoroElasticityLinear \
  (NUM_ACTIVE_FIELD_EVAL_MODES_PoroElasticityLinear_u + NUM_ACTIVE_FIELD_EVAL_MODES_PoroElasticityLinear_p)
#define NUM_U_t_FIELD_EVAL_MODES_PoroElasticityLinear_u 1
#define NUM_U_t_FIELD_EVAL_MODES_PoroElasticityLinear_p 1

#define NUM_COMPONENTS_DIAGNOSTIC_PoroElasticityLinear 17

// FLOPS_grad_du = FLOPS_MatMatMult
#define FLOPS_df1_PoroElasticityLinear (FLOPS_MatMatMult + FLOPS_LinearStrain_fwd + FLOPS_MatTrace + FLOPS_PoroElasticityLinearStress_fwd)
#define FLOPS_dg0_PoroElasticityLinear (4)
#define FLOPS_dg1_PoroElasticityLinear (FLOPS_MatTransposeVecMult + 4)
#define FLOPS_JACOBIAN_PoroElasticityLinear \
  (FLOPS_df1_PoroElasticityLinear + FLOPS_dXdxwdetJ + FLOPS_dg0_PoroElasticityLinear + FLOPS_dg1_PoroElasticityLinear + FLOPS_MatVecMult + 3 + 1)
#define FLOPS_JACOBIAN_Block_uu_PoroElasticityLinear \
  (FLOPS_MatMatMult + FLOPS_LinearStrain_fwd + FLOPS_PoroElasticityLinearStress_fwd + FLOPS_MatTrace + FLOPS_MatMatTransposeMult + 9)
#define FLOPS_JACOBIAN_Block_pp_PoroElasticityLinear (FLOPS_MatTransposeVecMult + FLOPS_MatVecMult + 4)

/**
  @brief Compute `Sigma` for linear poroelasticity

  @param[in]   ctx      QFunction context, holding `RatelLinearPoroElasticityParams`
  @param[in]   Q        Number of quadrature points
  @param[in]   i        Current quadrature point
  @param[in]   in       Input arrays
                          - 0 - volumetric qdata
                          - 1 - gradient of u with respect to reference coordinates
                          - 2 - p
  @param[out]  out      Output arrays, unused
  @param[out]  dXdx     Coordinate transformation
  @param[out]  f1       `f1 = Sigma = (lambda_d trace_e I) + (2 mu_d e_sym) - (B p I)`
  @param[out]  trace_e  Divergence of u = trace_e needed for computing g0 function

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int f1_PoroElasticityLinear(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in, CeedScalar *const *out,
                                                  CeedScalar dXdx[3][3], CeedScalar f1[3][3], CeedScalar *trace_e) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];
  const CeedScalar(*p)                  = in[2];

  // Context
  const RatelLinearPoroElasticityParams *context  = (RatelLinearPoroElasticityParams *)ctx;
  const CeedScalar                       lambda_d = context->lambda_d;
  const CeedScalar                       B        = context->B;
  const CeedScalar                       two_mu_d = context->two_mu_d;

  CeedScalar grad_u[3][3], e_sym[6], sigma_sym[6];

  // Qdata
  RatelQdataUnpack(Q, i, q_data, dXdx);

  // Read spatial derivatives of u; du/dX
  CeedScalar dudX[3][3];
  RatelGradUnpack(Q, i, ug, dudX);

  // Compute grad_u = du/dX * dX/dx
  // X is ref coordinate [-1,1]^3; x is physical coordinate
  RatelMatMatMult(1.0, dudX, dXdx, grad_u);

  // Compute strain : e (epsilon)
  // e = 1/2 (grad u + (grad u)^T)
  RatelLinearStrain(grad_u, e_sym);
  *trace_e = RatelMatTraceSymmetric(e_sym);

  // Compute sigma = (lambda_d trace_e I) + (2_mu_d e_sym) - (B p I)
  RatelPoroElasticityLinearStress(two_mu_d, lambda_d, B, *trace_e, p[i], e_sym, sigma_sym);

  RatelSymmetricMatUnpack(sigma_sym, f1);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute `g1` for linear poroelasticity

  @param[in]   ctx       QFunction context, holding `RatelLinearPoroElasticityParams`
  @param[in]   Q         Number of quadrature points
  @param[in]   i         Current quadrature point
  @param[in]   in        Input arrays
                           - 3 - gradient of p with respect to reference coordinates
  @param[in]   dXdx      Coordinate transformation
  @param[in]   Jm1       Unused in linear poroelasticity
  @param[out]  out       Output arrays, unused
  @param[out]  grad_p    Unused in linear poroelasticity
  @param[out]  g1        `g1 = varkappa grad_p`
  @param[out]  varkappa  Unused in linear poroelasticity

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int g1_PoroElasticityLinear(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in, CeedScalar dXdx[3][3],
                                                  const CeedScalar Jm1, CeedScalar *const *out, CeedScalar grad_p[3], CeedScalar g1[3],
                                                  CeedScalar *varkappa) {
  // Inputs
  const CeedScalar(*pg)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[3];

  // Context
  const RatelLinearPoroElasticityParams *context    = (RatelLinearPoroElasticityParams *)ctx;
  const CeedScalar                       varkappa_0 = context->varkappa_0;
  const CeedScalar                       eta_f      = context->eta_f;

  // Read spatial derivatives of p; dp/dX
  const CeedScalar dpdX[3] = {pg[0][i], pg[1][i], pg[2][i]};

  // g1 = (varkappa_0 / eta_f) * grad_p = (varkappa_0 / eta_f) * dXdx^T * dpdX
  RatelMatTransposeVecMult((varkappa_0 / eta_f), dXdx, dpdX, g1);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute `g0` for for linear poroelasticity

  `g0 = pt /M + B div(ut)`

  Since g0 is a function of ut and pt we write it in residual_ut Qfunction and here we return 0

  @param[in]   ctx               QFunction context, holding `RatelLinearPoroElasticityParams`
  @param[in]   Q                 Number of quadrature points
  @param[in]   i                 Current quadrature point
  @param[in]   in                Input arrays, unused
  @param[in]   grad_p            Gradient of pressure in current configuration computed in g1 function
  @param[in]   Jm1               Determinant of deformation gradient - 1 computed in f1 function
  @param[in]   varkappa          Permeability coefficient computed in g1 function
  @param[out]  out               Output arrays, unused
  @param[out]  g0               `g0 = 0.0`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int g0_PoroElasticityLinear(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in, CeedScalar grad_p[3],
                                                  const CeedScalar Jm1, const CeedScalar varkappa, CeedScalar *const *out, CeedScalar *g0) {
  *g0 = 0.0;
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute `g0` with u_t and p_t for linear poroelasticity

  @param[in]   ctx  QFunction context, holding `RatelLinearPoroElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - volumetric qdata
                      - 1 - gradient of u_t with respect to reference coordinates
                      - 2 - p_t
  @param[out]  out  Output arrays
                      - 0 - action on displacement field (it is 0)
                      - 1 - action on pressure field (q, g0) where `g0 = pt /M + B div(ut)`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(PoroElasticityResidual_Linear_ut)(void *ctx, const CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA]  = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug_t)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];
  const CeedScalar(*p_t)                 = in[2];

  // Outputs
  CeedScalar(*dvtdX)[3][CEED_Q_VLA] = (CeedScalar(*)[3][CEED_Q_VLA])out[0];
  CeedScalar(*g0)                   = out[1];

  // Context
  const RatelLinearPoroElasticityParams *context = (RatelLinearPoroElasticityParams *)ctx;
  const CeedScalar                       B       = context->B;
  const CeedScalar                       M       = context->M;

  // Quadrature point loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    const CeedScalar wdetJ = q_data[0][i];

    // Read spatial derivatives of ut; dut/dX
    CeedScalar dutdX[3][3], dXdx[3][3], grad_ut[3][3];
    RatelGradUnpack(Q, i, ug_t, dutdX);
    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Compute grad_ut = dut/dX * dX/dx
    // X is ref coordinate [-1,1]^3; x is physical coordinate
    RatelMatMatMult(1.0, dutdX, dXdx, grad_ut);
    // Compute div(u_t) = trace(grad_ut)
    CeedScalar trace_grad_ut = RatelMatTrace(grad_ut);

    // Update values
    // g0 = pt/M + B grad_ut
    g0[i] = ((p_t[i] / M) + (B * trace_grad_ut)) * wdetJ;

    // Set dvtdX = 0
    for (CeedInt j = 0; j < 3; j++) {
      for (CeedInt k = 0; k < 3; k++) {
        dvtdX[j][k][i] = 0;
      }
    }
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute linearization of `Sigma` for linear poroelasticity

  @param[in]   ctx               QFunction context, holding `RatelLinearPoroElasticityParams`
  @param[in]   Q                 Number of quadrature points
  @param[in]   i                 Current quadrature point
  @param[in]   in                Input arrays
                                   - 0 - volumetric qdata
                                   - 1 - gradient of incremental change to u with respect to reference coordinates
                                   - 2 - dp
  @param[out]  out               Output arrays, unused
  @param[out]  dXdx              Coordinate transformation
  @param[out]  grad_du           Gradient of incremental change in u
  @param[out]  depsilon          depsilon = (grad_du + grad_du^T)/2
  @param[out]  df1               `df1 = dSigma = (lambda_d trace_de I) + (2 mu_d de_sym) - (B dp I)`
  @param[out]  trace_de          Linearization of grad_u: div_du = trace_de
  @param[out]  trace_depsilon    `trace(depsilon)` unused in linear poroelasticity

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int df1_PoroElasticityLinear(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in, CeedScalar *const *out,
                                                   CeedScalar dXdx[3][3], CeedScalar grad_du[3][3], CeedScalar depsilon[3][3], CeedScalar df1[3][3],
                                                   CeedScalar *trace_de, CeedScalar *trace_depsilon) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*dug)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];
  const CeedScalar(*dp)                 = in[2];

  // Context
  const RatelLinearPoroElasticityParams *context  = (RatelLinearPoroElasticityParams *)ctx;
  const CeedScalar                       lambda_d = context->lambda_d;
  const CeedScalar                       B        = context->B;
  const CeedScalar                       two_mu_d = context->two_mu_d;

  CeedScalar de_sym[6], dsigma_sym[6];

  // Qdata
  RatelQdataUnpack(Q, i, q_data, dXdx);

  // Read spatial derivatives of increment u; d(du)/dX = d(du)/dX
  CeedScalar ddudX[3][3];
  RatelGradUnpack(Q, i, dug, ddudX);

  // Compute grad_du = ddu/dX * dX/dx
  // X is ref coordinate [-1,1]^3; x is physical coordinate
  RatelMatMatMult(1.0, ddudX, dXdx, grad_du);

  // Compute strain : de (epsilon)
  // de = 1/2 (grad_du + (grad_du)^T)
  RatelLinearStrain_fwd(grad_du, de_sym);

  // Compute trace of de
  *trace_de = RatelMatTraceSymmetric(de_sym);

  // Compute dsigma = ((lambda_d trace_de I)) + (2 mu_d de) - (B dp I)
  RatelPoroElasticityLinearStress_fwd(two_mu_d, lambda_d, B, *trace_de, dp[i], de_sym, dsigma_sym);

  RatelSymmetricMatUnpack(dsigma_sym, df1);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute linearization of `g1` for linear poroelasticity

  @param[in]   ctx               QFunction context, holding `RatelLinearPoroElasticityParams`
  @param[in]   Q                 Number of quadrature points
  @param[in]   i                 Current quadrature point
  @param[in]   in                Input arrays
                                   - 3 - gradient of incremental change to p with respect to reference coordinates
  @param[in]   dXdx              Coordinate transformation
  @param[in]   depsilon          depsilon = (grad_du + grad_du^T)/2
  @param[in]   trace_de          Linearization of divergence of u: div(du) = trace_de computed in df1 function
  @param[in]   trace_depsilon    `trace(depsilon)` unused in linear poroelasticity
  @param[out]  out               Output arrays, unused
  @param[out]  grad_dp           Gradient of incremental change in p, unused in linear porelasticity
  @param[out]  dg1               `dg1 = varkappa grad_dp`
  @param[out]  varkappa          Permeability coefficient, unused in linear porelasticity
  @param[out]  varkappa_fwd      Linearization of permeability coefficient, unused in linear porelasticity

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int dg1_PoroElasticityLinear(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in, CeedScalar dXdx[3][3],
                                                   CeedScalar depsilon[3][3], const CeedScalar trace_de, const CeedScalar trace_depsilon,
                                                   CeedScalar *const *out, CeedScalar grad_dp[3], CeedScalar dg1[3], CeedScalar *varkappa,
                                                   CeedScalar *varkappa_fwd) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*dpg)[CEED_Q_VLA]    = (const CeedScalar(*)[CEED_Q_VLA])in[3];

  // Context
  const RatelLinearPoroElasticityParams *context    = (RatelLinearPoroElasticityParams *)ctx;
  const CeedScalar                       varkappa_0 = context->varkappa_0;
  const CeedScalar                       eta_f      = context->eta_f;

  // Qdata
  RatelQdataUnpack(Q, i, q_data, dXdx);

  // Read spatial derivatives of p; d(dp)/dX
  const CeedScalar ddpdX[3] = {dpg[0][i], dpg[1][i], dpg[2][i]};

  // dg1 = (varkappa_0 / eta_f) * grad_dp = (varkappa_0 / eta_f) * dXdx^T * dpdX
  RatelMatTransposeVecMult((varkappa_0 / eta_f), dXdx, ddpdX, dg1);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute linearization of `g0` for linear poroelasticity

  @param[in]   ctx               QFunction context, holding `RatelLinearPoroElasticityParams`
  @param[in]   Q                 Number of quadrature points
  @param[in]   i                 Current quadrature point
  @param[in]   in                Input arrays
                                   - 2 - dp
  @param[in]   grad_du           Gradient of incremental change in u, unused in linear porelasticity
  @param[in]   grad_dp           Gradient of incremental change in p, unused in linear porelasticity
  @param[in]   trace_de          Linearization of divergence of u: div(du) = trace_de computed in df1 function
  @param[in]   trace_depsilon    `trace(depsilon)`, unused in linear porelasticity
  @param[in]   varkappa          Permeability coefficient, unused in linear porelasticity
  @param[in]   varkappa_fwd      Linearization of permeability coefficient, unused in linear porelasticity
  @param[out]  out               Output arrays, unused
  @param[out]  dg0               `dg0 = shift_v(dp/M + B trace_de )`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int dg0_PoroElasticityLinear(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in, CeedScalar grad_du[3][3],
                                                   CeedScalar grad_dp[3], const CeedScalar trace_de, const CeedScalar trace_depsilon,
                                                   const CeedScalar varkappa, const CeedScalar varkappa_fwd, CeedScalar *const *out,
                                                   CeedScalar *dg0) {
  // Inputs
  const CeedScalar(*dp) = in[2];

  // Context
  const RatelLinearPoroElasticityParams *context = (RatelLinearPoroElasticityParams *)ctx;
  const CeedScalar                       B       = context->B;
  const CeedScalar                       M       = context->M;
  const CeedScalar                       shift_v = context->common_parameters[1];

  // dg0 = shift_v(dp/M + B trace_de )
  *dg0 = shift_v * (dp[i] / M + B * trace_de);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute residual for linear poroelasticity

  @param[in]   ctx  QFunction context, holding `RatelLinearPoroElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(PoroElasticityResidual_Linear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return PoroElasticityResidual(ctx, Q, f1_PoroElasticityLinear, g1_PoroElasticityLinear, g0_PoroElasticityLinear,
                                !!NUM_COMPONENTS_STATE_PoroElasticityLinear, !!NUM_COMPONENTS_STORED_PoroElasticityLinear,
                                NUM_ACTIVE_FIELD_EVAL_MODES_PoroElasticityLinear, in, out);
}

/**
  @brief Evaluate Jacobian for linear poroelasticity

  @param[in]   ctx  QFunction context, holding `RatelLinearPoroElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(PoroElasticityJacobian_Linear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return PoroElasticityJacobian(ctx, Q, df1_PoroElasticityLinear, dg1_PoroElasticityLinear, dg0_PoroElasticityLinear,
                                !!NUM_COMPONENTS_STATE_PoroElasticityLinear, !!NUM_COMPONENTS_STORED_PoroElasticityLinear,
                                NUM_ACTIVE_FIELD_EVAL_MODES_PoroElasticityLinear, in, out);
}

/**
  @brief Wrapper to compute `Sigma` for linear poroelasticity platen BCs

  @param[in]   ctx   QFunction context, holding `RatelLinearPoroElasticityParams`
  @param[in]   Q     Number of quadrature points
  @param[in]   i     Current quadrature point
  @param[in]   in    Input arrays
                       - 0 - volumetric qdata
                       - 1 - gradient of u with respect to reference coordinates
                       - 2 - p
  @param[out]  out   Output arrays, unused
  @param[out]  dXdx  Coordinate transformation
  @param[out]  f1    `f1 = Sigma = (lambda_d trace_e I) + (2 mu_d e_sym) - (B p I)`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int f1_PoroElasticityLinear_Platen(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in,
                                                         CeedScalar *const *out, CeedScalar dXdx[3][3], CeedScalar f1[3][3]) {
  CeedScalar trace_e;

  return f1_PoroElasticityLinear(ctx, Q, i, in, out, dXdx, f1, &trace_e);
}

/**
  @brief Wrapper to compute linearization of `Sigma` for linear poroelasticity

  @param[in]   ctx   QFunction context, holding `RatelLinearPoroElasticityParams`
  @param[in]   Q     Number of quadrature points
  @param[in]   i     Current quadrature point
  @param[in]   in    Input arrays
                       - 0 - volumetric qdata
                       - 1 - gradient of incremental change to u with respect to reference coordinates
                       - 2 - dp
  @param[out]  out   Output arrays, unused
  @param[out]  dXdx  Coordinate transformation
  @param[out]  df1   `df1 = dSigma = (lambda_d trace_de I) + (2 mu_d de_sym) - (B dp I)`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int df1_PoroElasticityLinear_Platen(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in,
                                                          CeedScalar *const *out, CeedScalar dXdx[3][3], CeedScalar df1[3][3]) {
  CeedScalar grad_du[3][3], depsilon[3][3], trace_de, trace_depsilon;

  return df1_PoroElasticityLinear(ctx, Q, i, in, out, dXdx, grad_du, depsilon, df1, &trace_de, &trace_depsilon);
}

/**
  @brief Compute platen residual for linear poroelasticity

  @param[in]   ctx  QFunction context, holding `RatelBCPlatenParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(PlatenBCsResidual_PoroElasticityLinear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return PlatenBCs(ctx, Q, f1_PoroElasticityLinear_Platen, !!NUM_COMPONENTS_STATE_PoroElasticityLinear, !!NUM_COMPONENTS_STORED_PoroElasticityLinear,
                   NUM_ACTIVE_FIELD_EVAL_MODES_PoroElasticityLinear, in, out);
}

/**
  @brief Evaluate platen Jacobian for linear poroelasticity

  @param[in]   ctx  QFunction context, holding `RatelBCPlatenParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(PlatenBCsJacobian_PoroElasticityLinear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return PlatenBCs_Jacobian(ctx, Q, df1_PoroElasticityLinear_Platen, !!NUM_COMPONENTS_STATE_PoroElasticityLinear,
                            !!NUM_COMPONENTS_STORED_PoroElasticityLinear, NUM_ACTIVE_FIELD_EVAL_MODES_PoroElasticityLinear, in, out);
}

/**
  @brief Compute strain energy for linear poroelasticity

  @param[in]   ctx  QFunction context, holding `RatelLinearPoroElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - gradient of u with respect to reference coordinates
                      - 2 - p
  @param[out]  out  Output arrays
                      - 0 - strain energy

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(StrainEnergy_PoroElasticityLinear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];
  const CeedScalar(*p)                  = in[2];

  // Outputs
  CeedScalar(*energy) = (CeedScalar(*))out[0];

  // Context
  const RatelLinearPoroElasticityParams *context  = (RatelLinearPoroElasticityParams *)ctx;
  const CeedScalar                       lambda_d = context->lambda_d;
  const CeedScalar                       mu_d     = context->mu_d;
  const CeedScalar                       B        = context->B;
  const CeedScalar                       M        = context->M;
  const CeedScalar                       lambda_u = lambda_d + pow(B, 2.0) * M;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar dXdx[3][3], grad_u[3][3], e_sym[6];

    // Qdata
    const CeedScalar wdetJ = q_data[0][i];
    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Read spatial derivatives of u; du/dX
    CeedScalar dudX[3][3];
    RatelGradUnpack(Q, i, ug, dudX);

    // Compute grad_u = du/dX * dX/dx
    // X is ref coordinate [-1,1]^3; x is physical coordinate
    RatelMatMatMult(1.0, dudX, dXdx, grad_u);

    // Compute Strain : e (epsilon)
    // e = 1/2 (grad u + (grad u)^T)
    RatelLinearStrain(grad_u, e_sym);

    // Strain energy = lambda/2 (trace(e))^2 + mu e:e
    // -- trace(e) = div(u) = Volumetric strain
    const CeedScalar trace_e = RatelMatTraceSymmetric(e_sym);
    CeedScalar       zeta    = (p[i] / M) + (B * trace_e);

    // -- e:e = trace(e^2)
    const CeedScalar trace_e2 = RatelMatMatContractSymmetric(1.0, e_sym, e_sym);

    // psi = lambda_u * 0.5 * trace_e^2 + mu_d * trace_e2 - B * M * trace_e * zeta + 0.5 * M * zeta^2
    CeedScalar psi = 0.5 * lambda_u * pow(trace_e, 2.0) + mu_d * trace_e2 - B * M * trace_e * zeta + 0.5 * M * pow(zeta, 2.0);
    energy[i]      = psi * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute projected diagnostic values for linear poroelasticity

  @param[in]   ctx  QFunction context, holding `RatelLinearPoroElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - u
                      - 2 - gradient of u with respect to reference coordinates
                      - 3 - p
  @param[out]  out  Output arrays
                      - 0 - projected diagnostic values

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(Diagnostic_PoroElasticityLinear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[1];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[2];
  const CeedScalar(*p)                  = in[3];

  // Outputs
  CeedScalar(*diagnostic)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Context
  const RatelLinearPoroElasticityParams *context  = (RatelLinearPoroElasticityParams *)ctx;
  const CeedScalar                       lambda_d = context->lambda_d;
  const CeedScalar                       mu_d     = context->mu_d;
  const CeedScalar                       B        = context->B;
  const CeedScalar                       M        = context->M;
  const CeedScalar                       two_mu_d = context->two_mu_d;
  const CeedScalar                       phi_0    = context->phi_0;
  const CeedScalar                       rho      = context->common_parameters[RATEL_COMMON_PARAMETER_RHO];
  const CeedScalar                       lambda_u = lambda_d + pow(B, 2.0) * M;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar dXdx[3][3], grad_u[3][3], e_sym[6], e_dev_sym[6], sigma_sym[6], sigma_dev_sym[6];

    // Qdata
    const CeedScalar wdetJ = q_data[0][i];
    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Read spatial derivatives of u; du/dX
    CeedScalar dudX[3][3];
    RatelGradUnpack(Q, i, ug, dudX);

    // Compute grad_u = du/dX * dX/dx
    // X is ref coordinate [-1,1]^3; x is physical coordinate
    RatelMatMatMult(1.0, dudX, dXdx, grad_u);

    // Compute strain : e (epsilon)
    // e = 1/2 (grad u + (grad u)^T)
    RatelLinearStrain(grad_u, e_sym);

    // Compute Deviatoric Strain
    // e_dev = e - 1/3 * trace(e) * I
    const CeedScalar trace_e = RatelMatTraceSymmetric(e_sym);
    CeedScalar       zeta    = (p[i] / M) + (B * trace_e);

    RatelMatDeviatoricSymmetric(trace_e, e_sym, e_dev_sym);

    // Compute sigma = (lambda_d trace_e I) + (2_mu_d e_sym) - (B p I)
    RatelPoroElasticityLinearStress(two_mu_d, lambda_d, B, trace_e, p[i], e_sym, sigma_sym);

    // Displacement
    diagnostic[0][i] = u[0][i];
    diagnostic[1][i] = u[1][i];
    diagnostic[2][i] = u[2][i];

    // Cauchy stress tensor
    diagnostic[3][i] = sigma_sym[0];
    diagnostic[4][i] = sigma_sym[5];
    diagnostic[5][i] = sigma_sym[4];
    diagnostic[6][i] = sigma_sym[1];
    diagnostic[7][i] = sigma_sym[3];
    diagnostic[8][i] = sigma_sym[2];

    // Pressure; as computed by model
    diagnostic[9][i] = p[i];

    // Strain tensor invariants; trace(e)
    diagnostic[10][i] = trace_e;

    // trace(e^2) = e:e
    const CeedScalar trace_e2 = RatelMatMatContractSymmetric(1.0, e_sym, e_sym);
    diagnostic[11][i]         = trace_e2;
    diagnostic[12][i]         = 1 + trace_e;

    // linear strain for now
    diagnostic[13][i] = 0.5 * lambda_u * pow(trace_e, 2.0) + mu_d * trace_e2 - B * M * trace_e * zeta + 0.5 * M * pow(zeta, 2.0);

    // Compute von-Mises stress

    // -- Compute the the deviatoric part of Cauchy stress: sigma_dev = sigma - trace(sigma)/3
    const CeedScalar trace_sigma = RatelMatTraceSymmetric(sigma_sym);

    RatelMatDeviatoricSymmetric(trace_sigma, sigma_sym, sigma_dev_sym);

    // -- sigma_dev:sigma_dev
    const CeedScalar sigma_dev_contract = RatelMatMatContractSymmetric(1.0, sigma_dev_sym, sigma_dev_sym);

    // -- Compute von-Mises stress: sigma_von = sqrt(3/2 sigma_dev:sigma_dev)
    diagnostic[14][i] = sqrt(3. * sigma_dev_contract / 2.);

    // Compute mass density values

    // -- Set to 0 as default (rho should be 0.0 as defualt anyways)
    diagnostic[15][i] = 0.0;

    // -- if mass density is requested then compute mass density: rho/J
    if (rho > 0.0) diagnostic[15][i] = rho / diagnostic[12][i];

    // Porosity
    diagnostic[16][i] = phi_0;

    // Quadrature weight
    for (CeedInt j = 0; j < NUM_COMPONENTS_DIAGNOSTIC_PoroElasticityLinear; j++) diagnostic[j][i] *= wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute displacement block for pMG preconditioner for linear poroelasticity

  @param[in]   ctx  QFunction context, holding `RatelLinearPoroElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - gradient of incremental change of u with respect to reference coordinates
  @param[out]  out  Output arrays
                      - 0 - action of QFunction for displacement field

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(PoroElasticityPC_uu_Linear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*dug)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*ddvdX)[3][CEED_Q_VLA] = (CeedScalar(*)[3][CEED_Q_VLA])out[0];

  // Context
  const RatelLinearPoroElasticityParams *context  = (RatelLinearPoroElasticityParams *)ctx;
  const CeedScalar                       lambda_d = context->lambda_d;
  const CeedScalar                       two_mu_d = context->two_mu_d;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Read spatial derivatives of increment u; d(du)/dX = d(du)/dX
    CeedScalar ddudX[3][3], dXdx[3][3];
    RatelGradUnpack(Q, i, dug, ddudX);

    // -- Qdata
    const CeedScalar wdetJ = q_data[0][i];
    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Compute grad_du = ddu/dX * dX/dx
    // X is ref coordinate [-1,1]^3; x is physical coordinate
    CeedScalar grad_du[3][3];
    RatelMatMatMult(1.0, ddudX, dXdx, grad_du);

    // Compute strain : de (epsilon)
    // de = 1/2 (grad_du + (grad_du)^T)
    CeedScalar de_sym[6];
    RatelLinearStrain_fwd(grad_du, de_sym);

    // Compute trace of strain
    const CeedScalar trace_de = RatelMatTraceSymmetric(de_sym);
    CeedScalar       dsigma_pc_sym[6], dsigma_pc[3][3];

    // Compute dsigma_pc = ((lambda_d trace_de I)) + (2 mu_d de)
    RatelPoroElasticityLinearStress_fwd(two_mu_d, lambda_d, 0, trace_de, 0, de_sym, dsigma_pc_sym);

    RatelSymmetricMatUnpack(dsigma_pc_sym, dsigma_pc);

    // (grad(v), dsigma_pc)
    RatelMatMatTransposeMultAtQuadraturePoint(Q, i, wdetJ, dXdx, dsigma_pc, ddvdX);
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute pressure block for pMG preconditioner for linear poroelasticity

  @param[in]   ctx  QFunction context, holding `RatelLinearPoroElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - incremental change of p
                      - 2 - gradient of p
  @param[out]  out  Output arrays
                      - 0 - action of QFunction for pressure field (dg0)
                      - 1 - action of QFunction for pressure field (dg1)

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(PoroElasticityPC_pp_Linear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*dp)                 = in[1];
  const CeedScalar(*dpg)[CEED_Q_VLA]    = (const CeedScalar(*)[CEED_Q_VLA])in[2];

  // Outputs
  CeedScalar(*dq)               = out[0];
  CeedScalar(*dqdX)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[1];

  // Context
  const RatelLinearPoroElasticityParams *context    = (RatelLinearPoroElasticityParams *)ctx;
  const CeedScalar                       varkappa_0 = context->varkappa_0;
  const CeedScalar                       M          = context->M;
  const CeedScalar                       shift_v    = context->common_parameters[1];
  const CeedInt                          sign_pp    = context->sign_pp;
  const CeedScalar                       eta_f      = context->eta_f;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // -- Qdata
    const CeedScalar wdetJ = q_data[0][i];
    CeedScalar       dXdx[3][3];
    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Read spatial derivatives of p; d(dp)/dX
    const CeedScalar ddpdX[3] = {dpg[0][i], dpg[1][i], dpg[2][i]};

    // dg0(dp) = shift_v(dp/M)
    CeedScalar dg0 = shift_v * (dp[i] / M);
    // dg1 = (varkappa_0 / eta_f) * grad_dp = (varkappa_0 / eta_f) * dXdx^T * dpdX
    CeedScalar dg1[3];
    RatelMatTransposeVecMult((varkappa_0 / eta_f), dXdx, ddpdX, dg1);

    // (q, dp, dqdX)
    RatelMatVecMultAtQuadraturePoint(Q, i, sign_pp * wdetJ, dXdx, dg1, dqdX);
    dq[i] = sign_pp * dg0 * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
