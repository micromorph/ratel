/// @file
/// Ratel neo-Hookean hyperelasticity at finite strain in initial configuration with autodifferentiation (ADOL-C) QFunction source
#include <ceed/types.h>
#ifndef CEED_RUNNING_JIT_PASS
#include <math.h>
#endif

#include "../../models/elasticity-linear.h"          // IWYU pragma: export
#include "../../models/neo-hookean.h"                // IWYU pragma: export
#include "../utils-cpp.h"                            // IWYU pragma: export
#include "elasticity-common-cpp.h"                   // IWYU pragma: export
#include "elasticity-neo-hookean-ad-adolc-common.h"  // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

#define NUM_COMPONENTS_STATE_NeoHookeanInitial_AD_ADOLC 0
#define NUM_COMPONENTS_STORED_NeoHookeanInitial_AD_ADOLC 21
#define NUM_ACTIVE_FIELD_EVAL_MODES_NeoHookeanInitial_AD_ADOLC 1

/**
  @brief Compute `P` for neo-Hookean hyperelasticity in initial configuration with ADOL-C

  @param[in]   ctx                   QFunction context, holding `RatelNeoHookeanElasticityParams`
  @param[in]   Q                     Number of quadrature points
  @param[in]   i                     Current quadrature point
  @param[in]   in                    Input arrays
                                       - 0 - volumetric qdata
                                       - 1 - gradient of u with respect to reference coordinates
  @param[out]  out                   Output arrays
                                       - 0 - stored gradient of u with respect to physical coordinates, Green Lagrange strain,
                                             and Second Piola-Kirchhoff
  @param[out]  dXdx                  Coordinate transformation
  @param[out]  f1                    `f1 = P = F * S`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int f1_NeoHookeanInitial_AD_ADOLC(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in,
                                                        CeedScalar *const *out, CeedScalar dXdx[3][3], CeedScalar f1[3][3]) {
  // Inputs
  typedef CeedScalar(*VLATy)[CEED_Q_VLA];
  const VLATy q_data = (VLATy)in[0];
  typedef CeedScalar(*VLATy3)[3][CEED_Q_VLA];
  const VLATy3 ug = (VLATy3)in[1];

  // Outputs
  CeedScalar *stored_values = out[0];

  // Context
  const RatelNeoHookeanElasticityParams *context = (RatelNeoHookeanElasticityParams *)ctx;
  const CeedScalar                       mu      = context->mu;
  const CeedScalar                       lambda  = context->lambda;

  CeedScalar grad_u[3][3], dudX[3][3], E_sym[6], S_sym[6], S[3][3];

  // Qdata
  RatelQdataUnpack_cpp(Q, i, RATEL_VLA_PASS2(q_data), dXdx);

  // Read spatial derivatives of u; du/dX
  RatelGradUnpack_cpp(Q, i, RATEL_VLA_PASS3(ug), dudX);

  // Compute grad_u = du/dX * dX/dx
  // X is ref coordinate [-1,1]^3; x is physical coordinate in initial configuration
  RatelMatMatMult(1.0, dudX, dXdx, grad_u);

  // Compute the Deformation Gradient : F = I + grad_u
  const CeedScalar F[3][3] = {
      {grad_u[0][0] + 1, grad_u[0][1],     grad_u[0][2]    },
      {grad_u[1][0],     grad_u[1][1] + 1, grad_u[1][2]    },
      {grad_u[2][0],     grad_u[2][1],     grad_u[2][2] + 1}
  };

  // E - Green-Lagrange strain tensor
  RatelGreenLagrangeStrain(grad_u, E_sym);

  // Compute Second Piola-Kirchhoff (S) with ADOL-C
  GradientPsi_ADOLC(S_sym, E_sym, lambda, mu);
  RatelSymmetricMatUnpack(S_sym, S);

  // Compute the First Piola-Kirchhoff f1 = P = F*S
  RatelMatMatMult(1.0, F, S, f1);

  // Store values
  RatelStoredValuesPack(Q, i, 0, 9, (CeedScalar *)grad_u, stored_values);
  RatelStoredValuesPack(Q, i, 9, 6, (CeedScalar *)E_sym, stored_values);
  RatelStoredValuesPack(Q, i, 15, 6, (CeedScalar *)S_sym, stored_values);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute linearization of `P` for neo-Hookean hyperelasticity in initial configuration with ADOL-C

  @param[in]   ctx                   QFunction context, holding `RatelNeoHookeanElasticityParams`
  @param[in]   Q                     Number of quadrature points
  @param[in]   i                     Current quadrature point
  @param[in]   in                    Input arrays
                                       - 0 - volumetric qdata
                                       - 1 - stored gradient of u with respect to physical coordinates, Green Lagrange strain,
                                             and Second Piola-Kirchhoff
                                       - 2 - gradient of incremental change to u with respect to reference coordinates
  @param[out]  out                   Output arrays, unused
  @param[out]  dXdx                  Coordinate transformation
  @param[out]  df1                   `df1 = dP =  dF * S + F * dS`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int df1_NeoHookeanInitial_AD_ADOLC(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in,
                                                         CeedScalar *const *out, CeedScalar dXdx[3][3], CeedScalar df1[3][3]) {
  // Inputs
  typedef CeedScalar(*VLATy)[CEED_Q_VLA];
  const VLATy       q_data        = (VLATy)in[0];
  const CeedScalar *stored_values = in[1];
  typedef CeedScalar(*VLATy3)[3][CEED_Q_VLA];
  const VLATy3 dug = (VLATy3)in[2];

  // Context
  const RatelNeoHookeanElasticityParams *context = (RatelNeoHookeanElasticityParams *)ctx;
  const CeedScalar                       mu      = context->mu;
  const CeedScalar                       lambda  = context->lambda;

  CeedScalar grad_du[3][3], grad_u[3][3], ddudX[3][3], E_sym[6], dE_sym[6], dS_sym[6], dS[3][3], S[3][3], S_sym[6], hess[6][6];

  // Qdata
  RatelQdataUnpack_cpp(Q, i, RATEL_VLA_PASS2(q_data), dXdx);

  // Read spatial derivatives of increment u; d(du)/dX = d(du)/dX
  RatelGradUnpack_cpp(Q, i, RATEL_VLA_PASS3(dug), ddudX);

  // Compute grad_du = ddu/dX * dX/dx
  // X is ref coordinate [-1,1]^3; x is physical coordinate in initial configuration
  RatelMatMatMult(1.0, ddudX, dXdx, grad_du);

  // Deformation Gradient : F = I + grad_u
  RatelStoredValuesUnpack(Q, i, 0, 9, stored_values, (CeedScalar *)grad_u);
  const CeedScalar F[3][3] = {
      {grad_u[0][0] + 1, grad_u[0][1],     grad_u[0][2]    },
      {grad_u[1][0],     grad_u[1][1] + 1, grad_u[1][2]    },
      {grad_u[2][0],     grad_u[2][1],     grad_u[2][2] + 1}
  };

  // dE - Green-Lagrange strain tensor increment
  RatelGreenLagrangeStrain_fwd(grad_du, F, dE_sym);

  // Compute dS with ADOL-C
  RatelStoredValuesUnpack(Q, i, 9, 6, stored_values, (CeedScalar *)E_sym);
  HessianPsi_ADOLC(hess, E_sym, lambda, mu);
  for (CeedInt i = 0; i < 6; i++) {
    dS_sym[i] = 0.;
    for (CeedInt j = 0; j < 6; j++) dS_sym[i] += hess[i][j] * dE_sym[j];
  }
  RatelSymmetricMatUnpack(dS_sym, dS);

  // Second Piola-Kirchhoff (S)
  RatelStoredValuesUnpack(Q, i, 15, 6, stored_values, (CeedScalar *)S_sym);
  RatelSymmetricMatUnpack(S_sym, S);

  // df1 = dP = dP/dF:dF = dF*S + F*dS; note dF = grad_du
  RatelMatMatMultPlusMatMatMult(grad_du, S, F, dS, df1);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute residual for neo-Hookean hyperelasticity in initial configuration with ADOL-C

  @param[in]   ctx  QFunction context, holding `RatelNeoHookeanElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityResidual_NeoHookeanInitial_AD_ADOLC)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return ElasticityResidual(ctx, Q, f1_NeoHookeanInitial_AD_ADOLC, !!NUM_COMPONENTS_STATE_NeoHookeanInitial_AD_ADOLC,
                            !!NUM_COMPONENTS_STORED_NeoHookeanInitial_AD_ADOLC, NUM_ACTIVE_FIELD_EVAL_MODES_NeoHookeanInitial_AD_ADOLC, in, out);
}

/**
  @brief Evaluate Jacobian for neo-Hookean hyperelasticity in initial configuration with ADOL-C

  @param[in]   ctx  QFunction context, holding `RatelNeoHookeanElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityJacobian_NeoHookeanInitial_AD_ADOLC)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return ElasticityJacobian(ctx, Q, df1_NeoHookeanInitial_AD_ADOLC, !!NUM_COMPONENTS_STATE_NeoHookeanInitial_AD_ADOLC,
                            !!NUM_COMPONENTS_STORED_NeoHookeanInitial_AD_ADOLC, NUM_ACTIVE_FIELD_EVAL_MODES_NeoHookeanInitial_AD_ADOLC, in, out);
}

/// @}
