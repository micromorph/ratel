/// @file
/// Common components for elasticity QFunctions
#pragma once

#include <ceed/types.h>

#include "../../models/common-parameters.h"  // IWYU pragma: export
#include "../utils-cpp.h"                    // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

typedef int (*RatelComputef1_cpp)(void *, CeedInt, CeedInt, const CeedScalar *const *, CeedScalar *const *, CeedScalar[3][3], CeedScalar[3][3]);
typedef int (*RatelComputef1_fwd_cpp)(void *, CeedInt, CeedInt, const CeedScalar *const *, CeedScalar *const *, CeedScalar[3][3], CeedScalar[3][3]);

/**
  @brief Compute elasticity residual evaluation

  @param[in]   ctx                          QFunction context, unused
  @param[in]   Q                            Number of quadrature points
  @param[in]   compute_f1                   Function to compute action of f1
  @param[in]   has_state_values             Boolean flag indicating model state values in residual evaluation
  @param[in]   has_stored_values            Boolean flag indicating model stores values in residual evaluation
  @param[in]   num_active_field_eval_modes  Number of active field evaluation modes
  @param[in]   in                           Input arrays
                                              - 0 - volumetric qdata
  @param[out]  out                          Output array
                                              - `output_data_offset` - action of QFunction

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int ElasticityResidual(void *ctx, CeedInt Q, RatelComputef1_cpp compute_f1, bool has_state_values, bool has_stored_values,
                                             CeedInt num_active_field_eval_modes, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  typedef CeedScalar(*VLATy)[CEED_Q_VLA];
  const VLATy q_data = (VLATy)in[0];

  // Outputs
  const CeedInt output_data_offset = has_state_values + has_stored_values;
  typedef CeedScalar(*VLATy3)[3][CEED_Q_VLA];
  VLATy3 dvdX = (VLATy3)out[output_data_offset];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Qdata
    const CeedScalar wdetJ = q_data[0][i];

    // Coordinate transformation and f1
    // For linear elasticity f1 = sigma, where sigma is Cauchy stress tensor
    // For hyperelastic in initial configuration f1 = P, where P is First Piola Kirchhoff stress
    // For hyperelastic in current configuration f1 = tau, where tau is Kirchhoff stress
    CeedScalar dXdx[3][3], f1[3][3];

    compute_f1(ctx, Q, i, in, out, dXdx, f1);

    // Apply dXdx^T and weight
    RatelMatMatTransposeMultAtQuadraturePoint_cpp(Q, i, wdetJ, dXdx, f1, RATEL_VLA_PASS3(dvdX));
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute elasticity Jacobian evaluation

  @param[in]   ctx                          QFunction context, holding common parameters and model parameters
  @param[in]   Q                            Number of quadrature points
  @param[in]   compute_df1                  Function to compute action of df1
  @param[in]   has_state_values             Boolean flag indicating model state values in residual evaluation
  @param[in]   has_stored_values            Boolean flag indicating model stores values in residual evaluation
  @param[in]   num_active_field_eval_modes  Number of active field evaluation modes
  @param[in]   in                           Input arrays
                                              - 0                   - volumetric qdata
                                              - `input_data_offset` - incremental change in u
  @param[out]  out                          Output array
                                              - 0 - action of QFunction
                                              - 1 - action of QFunction dynamic solver term

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int ElasticityJacobian(void *ctx, CeedInt Q, RatelComputef1_fwd_cpp compute_df1, bool has_stored_values, bool has_state_values,
                                             CeedInt num_active_field_eval_modes, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedInt input_data_offset = 1 + has_state_values + has_stored_values + num_active_field_eval_modes;
  typedef CeedScalar(*VLATy)[CEED_Q_VLA];
  const VLATy       q_data = (VLATy)in[0];
  const CeedScalar *du     = in[input_data_offset];

  // Outputs
  typedef CeedScalar(*VLATy3)[3][CEED_Q_VLA];
  VLATy3      ddvdX = (VLATy3)out[0];
  CeedScalar *dv    = out[1];

  // Context
  const CeedScalar *context     = (CeedScalar *)ctx;
  const CeedScalar  rho         = context[RATEL_COMMON_PARAMETER_RHO];
  const CeedScalar  shift_a     = context[RATEL_COMMON_PARAMETER_SHIFT_A];
  const bool        has_shift_a = shift_a != 0.0;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Qdata
    const CeedScalar wdetJ = q_data[0][i];

    // Coordinate transformation and linearized f1
    // For linear elasticity df1 = dsigma;
    // For hyperelastic in initial configuration df1 = dP
    // For hyperelastic in current configuration df1 = dtau - tau * grad_du^T
    CeedScalar dXdx[3][3], df1[3][3];

    compute_df1(ctx, Q, i, in, out, dXdx, df1);

    // Apply dXdx^T and weight
    RatelMatMatTransposeMultAtQuadraturePoint_cpp(Q, i, wdetJ, dXdx, df1, RATEL_VLA_PASS3(ddvdX));
  }  // End of Quadrature Point Loop

  // Second Quadrature Point Loop for dynamic solver
  if (has_shift_a) {
    CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
      // Qdata
      const CeedScalar wdetJ = q_data[0][i];

      // Apply scaled mass matrix
      RatelScaledMassApplyAtQuadraturePoint(Q, i, 3, shift_a * rho * wdetJ, du, dv);
    }  // End of Quadrature Point Loop
  }
  return CEED_ERROR_SUCCESS;
}

/// @}
