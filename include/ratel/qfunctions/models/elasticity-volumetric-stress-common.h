/// @file
/// Ratel volumetric stress common term for single field isochoric-volumetric split at finite strain QFunction source
#pragma once

#include <ceed/types.h>

#include "../../models/neo-hookean.h"             // IWYU pragma: export
#include "../utils.h"                             // IWYU pragma: export
#include "elasticity-volumetric-energy-common.h"  // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

#define FLOPS_Tau_vol (FLOPS_J_dVdJ + 1)
#define FLOPS_S_vol (FLOPS_J_dVdJ + 1 + FLOPS_ScalarMatMultSymmetric)
#define FLOPS_dS_vol (FLOPS_J_dVdJ + FLOPS_J2_d2VdJ2 + 5 + FLOPS_MatMatAddSymmetric + 12)
#define FLOPS_FdSFTranspose_vol (FLOPS_J_dVdJ + FLOPS_J2_d2VdJ2 + 5 + FLOPS_ScalarMatMultSymmetric + 4)

/**
  @brief Compute volumetric Kirchoff tau for single field isochoric hyperelasticity.

 `tau_vol = (bulk * J dV/dJ) I`

  @param[in]   J_dVdJ       J dV/dJ
  @param[in]   bulk         Bulk modulus
  @param[out]  tau_vol_sym  Volumetric Kirchoff tau

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelVolumetricKirchhoffTau(CeedScalar J_dVdJ, CeedScalar bulk, CeedScalar *tau_vol_sym) {
  // -- [bulk * J dV/dJ]
  *tau_vol_sym = bulk * J_dVdJ;
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute volumetric part of second Kirchoff stress for single field isochoric hyperelasticity.

 `S_vol = (bulk * J dV/dJ) * C_inv`

  @param[in]   J_dVdJ      J dV/dJ
  @param[in]   bulk        Bulk modulus
  @param[in]   C_inv_sym   Inverse of right Cauchy-Green tensor, in symmetric representation
  @param[out]  S_vol_sym   Volumetric second Kirchoff stress, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelVolumetricSecondKirchhoffStress(CeedScalar J_dVdJ, CeedScalar bulk, const CeedScalar C_inv_sym[6],
                                                               CeedScalar S_vol_sym[6]) {
  // -- bulk * J dV/dJ
  const CeedScalar coeff_1 = bulk * J_dVdJ;

  // S_vol = coeff_1 * C_inv
  RatelScalarMatMultSymmetric(coeff_1, C_inv_sym, S_vol_sym);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute derivative of volumetric second Kirchoff stress for single field isochoric hyperelasticity.

 `dS_vol = ((bulk J^2 * d2V/dJ2 + bulk * J dV/dJ) (C_inv:dE)) C_inv + (bulk * J dV/dJ) dC_inv`

  @param[in]   J_dVdJ            J dV/dJ
  @param[in]   J2_d2VdJ2         J^2 d^2V/dJ^2
  @param[in]   bulk              Bulk modulus
  @param[in]   Cinv_contract_dE  `C_inv : dE`
  @param[in]   C_inv_sym         Inverse of right Cauchy-Green tensor, in symmetric representation
  @param[in]   dC_inv_sym        Derivative of C^{-1}, in symmetric representation
  @param[out]  dS_vol_sym        Derivative of primal second Kirchoff stress, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelVolumetricSecondKirchhoffStress_fwd(CeedScalar J_dVdJ, CeedScalar J2_d2VdJ2, CeedScalar bulk,
                                                                   CeedScalar Cinv_contract_dE, const CeedScalar C_inv_sym[6],
                                                                   const CeedScalar dC_inv_sym[6], CeedScalar dS_vol_sym[6]) {
  const CeedScalar coeff_1 = (bulk * J2_d2VdJ2 + bulk * J_dVdJ) * Cinv_contract_dE;
  const CeedScalar coeff_2 = bulk * J_dVdJ;

  // dS_vol = coeff_1 C_inv + coeff_2 dC_inv
  RatelMatMatAddSymmetric(coeff_1, C_inv_sym, coeff_2, dC_inv_sym, dS_vol_sym);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute F*dS_vol*F^T for for single field isochoric hyperelasticity in current configuration.

  @param[in]   J_dVdJ                  J dV/dJ
  @param[in]   J2_d2VdJ2               J^2 d^2V/dJ^2
  @param[in]   bulk                    Bulk modulus
  @param[in]   trace_depsilon          `trace(depsilon)`
  @param[in]   depsilon_sym            depsilon = (grad_du + grad_du^T)/2
  @param[out]  FdSFTranspose_vol_sym   F*dS_vol*F^T needed for computing df1 in current configuration

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelComputeFdSFTransposeVolumetric(CeedScalar J_dVdJ, CeedScalar J2_d2VdJ2, CeedScalar bulk, CeedScalar trace_depsilon,
                                                              const CeedScalar depsilon_sym[6], CeedScalar FdSFTranspose_vol_sym[6]) {
  const CeedScalar coeff_1 = (bulk * J2_d2VdJ2 + bulk * J_dVdJ) * trace_depsilon;
  const CeedScalar coeff_2 = bulk * J_dVdJ;

  // F*dS_vol*F^T = -2 * coeff_2 * depsilon_sym
  RatelScalarMatMultSymmetric(-2. * coeff_2, depsilon_sym, FdSFTranspose_vol_sym);

  // Add coeff_1 * I to FdSFTranspose_vol_sym
  FdSFTranspose_vol_sym[0] += coeff_1;
  FdSFTranspose_vol_sym[1] += coeff_1;
  FdSFTranspose_vol_sym[2] += coeff_1;
  return CEED_ERROR_SUCCESS;
}

/// @}
