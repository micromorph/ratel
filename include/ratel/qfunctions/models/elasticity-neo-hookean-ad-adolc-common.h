/// @file
/// Ratel utility helpers QFunction source when using ADOL-C
#pragma once

#include <adolc/adolc.h>

/// @addtogroup RatelInternal
/// @{

/**
  @brief Compute `det(A_sym) - 1` for a 3x3 matrix, in symmetric representation

  @param[in]  A_sym  Input matrix, in symmetric representation

  @return A scalar value: `det(A_sym) - 1`
**/
CEED_QFUNCTION_HELPER adouble RatelMatDetAM1Symmetric_ADOLC(const adouble A_sym[6]) {
  return A_sym[0] * (A_sym[1] * A_sym[2] - A_sym[3] * A_sym[3]) +          /* *NOPAD* */
         A_sym[5] * (A_sym[3] * A_sym[4] - A_sym[5] * A_sym[2]) +          /* *NOPAD* */
         A_sym[4] * (A_sym[5] * A_sym[3] - A_sym[4] * A_sym[1]) +          /* *NOPAD* */
         A_sym[0] + A_sym[1] + A_sym[2] +                                  /* *NOPAD* */
         A_sym[0] * A_sym[1] + A_sym[0] * A_sym[2] + A_sym[1] * A_sym[2] - /* *NOPAD* */
         A_sym[5] * A_sym[5] - A_sym[4] * A_sym[4] - A_sym[3] * A_sym[3];  /* *NOPAD* */
}

/**
  @brief Series approximation of `log1p()`

  `log1p()` is not vectorized in libc.
  The series expansion up to the sixth term is accurate to 1e-10 relative error in the range `2/3 < J < 3/2`.
  This is accurate enough for many hyperelastic applications, but perhaps not for materials like foams that may encounter volumetric strains on the
  order of `1/5 < J < 5`.

  @param[in]  x  Scalar value

  @return A scalar value: `log1p(x)`
**/
CEED_QFUNCTION_HELPER adouble RatelLog1pSeries_ADOLC(adouble x) {
  adouble sum = 0;
  adouble y   = x / (2. + x);
  adouble y2  = y * y;
  sum += y;
  for (CeedInt i = 0; i < 5; i++) {
    y *= y2;
    sum += y / (2 * i + 3);
  }
  return 2 * sum;
};

/**
  @brief Compute the trace of a 3x3 matrix

  @param[in]  A_sym  Input 3x3 matrix, in symmetric representation

  @return A scalar value: `trace(A)`
**/
CEED_QFUNCTION_HELPER adouble RatelMatTraceSymmetric_ADOLC(adouble A_sym[6]) { return A_sym[0] + A_sym[1] + A_sym[2]; }

/**
  @brief Compute strain energy for neo-Hookean hyperelasticity

  @param[in]   strain_sym  Green Lagrange/Euler strain, in symmetric representation
  @param[in]   lambda      Lamé parameter
  @param[in]   mu          Shear modulus

  @return A scalar value: strain energy
**/
CEED_QFUNCTION_HELPER adouble RatelStrainEnergy_NeoHookean_AD_ADOLC(adouble strain_sym[6], const CeedScalar lambda, const CeedScalar mu) {
  adouble strain2_sym[6];

  // Calculate 2*E
  for (CeedInt i = 0; i < 6; i++) strain2_sym[i] = strain_sym[i] * 2;

  // log(J)
  const adouble detCm1 = RatelMatDetAM1Symmetric_ADOLC(strain2_sym);
  const adouble J      = sqrt(detCm1 + 1);
  const adouble logJ   = RatelLog1pSeries_ADOLC(detCm1) / 2.;

  // trace(E)
  adouble strain_trace = RatelMatTraceSymmetric_ADOLC(strain_sym);
  return lambda * (J * J - 1) / 4 - lambda * logJ / 2 + mu * (-logJ + strain_trace);
}

/**
  @brief Compute the gradient of strain energy for neo-Hookean hyperelasticity with ADOL-C

  @param[out]  grad_sym    Gradient of strain energy, in symmetric representation
  @param[in]   strain_sym  Green Lagrange/Euler strain, in symmetric representation
  @param[in]   lambda      Lamé parameter
  @param[in]   mu          Shear modulus
**/
CEED_QFUNCTION_HELPER void GradientPsi_ADOLC(CeedScalar grad_sym[6], CeedScalar strain_sym[6], const CeedScalar lambda, const CeedScalar mu) {
  CeedScalar Fp[1];
  adouble    Fa[1], Ea[6];
  CeedInt    tag = 1;

  // Active section for AD
  trace_on(tag);
  for (CeedInt i = 0; i < 6; i++) Ea[i] <<= strain_sym[i];
  Fa[0] = RatelStrainEnergy_NeoHookean_AD_ADOLC(Ea, lambda, mu);
  Fa[0] >>= Fp[0];
  trace_off();

  // Compute the gradient
  gradient(tag, 6, strain_sym, grad_sym);
  for (CeedInt i = 3; i < 6; i++) grad_sym[i] /= 2.;
}

/**
  @brief Compute the hessian of strain energy for neo-Hookean hyperelasticity with ADOL-C

  @param[out]  hess        Hessian of strain energy
  @param[in]   strain_sym  Green Lagrange strain
  @param[in]   lambda      Lamé parameter
  @param[in]   mu          Shear modulus
**/
CEED_QFUNCTION_HELPER void HessianPsi_ADOLC(CeedScalar hess[6][6], CeedScalar strain_sym[6], const CeedScalar lambda, const CeedScalar mu) {
  adouble     Fa[1], Ea[6];
  CeedInt     tag = 1;
  CeedScalar  Fp[1], buf[21];
  CeedScalar *H[6] = {&buf[0], &buf[1], &buf[3], &buf[6], &buf[10], &buf[15]};

  // Active section for AD
  trace_on(tag);
  for (CeedInt i = 0; i < 6; i++) Ea[i] <<= strain_sym[i];
  Fa[0] = RatelStrainEnergy_NeoHookean_AD_ADOLC(Ea, lambda, mu);
  Fa[0] >>= Fp[0];
  trace_off();

  // Compute the hessian
  hessian(tag, 6, strain_sym, H);

  // Populate hess[6][6]
  for (CeedInt i = 0; i < 6; i++) {
    for (CeedInt j = 0; j < i + 1; j++) {
      hess[i][j] = H[i][j];
      if (i != j) hess[j][i] = hess[i][j];
    }
  }
  for (CeedInt i = 3; i < 6; i++) {
    for (CeedInt j = 0; j < 6; j++) hess[i][j] /= 2.;
  }
}

/// @}
