/// @file
/// Common components for poroelasticity QFunctions
#pragma once

#include <ceed/types.h>

#include "../../models/common-parameters.h"  // IWYU pragma: export
#include "../utils.h"                        // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{
typedef int (*RatelComputef1_Poroelastic)(void *, CeedInt, CeedInt, const CeedScalar *const *, CeedScalar *const *, CeedScalar[3][3],
                                          CeedScalar[3][3], CeedScalar *);
typedef int (*RatelComputedf1_fwd_Poroelastic)(void *, CeedInt, CeedInt, const CeedScalar *const *, CeedScalar *const *, CeedScalar[3][3],
                                               CeedScalar[3][3], CeedScalar[3][3], CeedScalar[3][3], CeedScalar *, CeedScalar *);
typedef int (*RatelComputeg0_Poroelastic)(void *, CeedInt, CeedInt, const CeedScalar *const *, CeedScalar[3], const CeedScalar, const CeedScalar,
                                          CeedScalar *const *, CeedScalar *);
typedef int (*RatelComputedg0_fwd_Poroelastic)(void *, CeedInt, CeedInt, const CeedScalar *const *, CeedScalar[3][3], CeedScalar[3], const CeedScalar,
                                               const CeedScalar, const CeedScalar, const CeedScalar, CeedScalar *const *, CeedScalar *);
typedef int (*RatelComputeg1_Poroelastic)(void *, CeedInt, CeedInt, const CeedScalar *const *, CeedScalar[3][3], const CeedScalar,
                                          CeedScalar *const *, CeedScalar[3], CeedScalar[3], CeedScalar *);
typedef int (*RatelComputedg1_fwd_Poroelastic)(void *, CeedInt, CeedInt, const CeedScalar *const *, CeedScalar[3][3], CeedScalar[3][3],
                                               const CeedScalar, const CeedScalar, CeedScalar *const *, CeedScalar[3], CeedScalar[3], CeedScalar *,
                                               CeedScalar *);

/**
  @brief Compute poroelasticity residual evaluation

  @param[in]   ctx                          QFunction context, unused
  @param[in]   Q                            Number of quadrature points
  @param[in]   compute_f1_poroelastic       Function to compute action of f1
  @param[in]   compute_g1_poroelastic       Function to compute action of g1
  @param[in]   compute_g0_poroelastic       Function to compute action of g0
  @param[in]   has_state_values             Boolean flag indicating model state values in residual evaluation
  @param[in]   has_stored_values            Boolean flag indicating model stores values in residual evaluation
  @param[in]   num_active_field_eval_modes  Number of active field evaluation modes
  @param[in]   in                           Input arrays
                                              - 0 - volumetric qdata
  @param[out]  out                          Output array
                                              - `output_data_offset`     - action of QFunction on f1 term (displacement field)
                                              - `output_data_offset + 1` - action of QFunction on g0 term (pressure field)
                                              - `output_data_offset + 2` - action of QFunction on g1 term (pressure field)

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int PoroElasticityResidual(void *ctx, CeedInt Q, RatelComputef1_Poroelastic compute_f1_poroelastic,
                                                 RatelComputeg1_Poroelastic compute_g1_poroelastic, RatelComputeg0_Poroelastic compute_g0_poroelastic,
                                                 bool has_state_values, bool has_stored_values, CeedInt num_active_field_eval_modes,
                                                 const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];

  // Outputs
  const CeedInt output_data_offset = has_state_values + has_stored_values;
  CeedScalar(*dvdX)[3][CEED_Q_VLA] = (CeedScalar(*)[3][CEED_Q_VLA])out[output_data_offset];
  CeedScalar(*q)                   = out[output_data_offset + 1];
  CeedScalar(*dqdX)[CEED_Q_VLA]    = (CeedScalar(*)[CEED_Q_VLA])out[output_data_offset + 2];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar dXdx[3][3], f1[3][3], grad_p[3], g1[3], scaler_1, scalar_2, g0;

    // Qdata
    const CeedScalar wdetJ = q_data[0][i];
    // Coordinate transformation f1 and g1
    // For linear poroelasticity f1 = sigma, where sigma is Cauchy stress tensor
    // For linear poroelastic scalar_1 = div(u) = trace_e and finite strain scalar_1 = Jm1
    // For finite strain poroelastic in current f1 = tau, where tau is total Kirchhoff stress
    // For finite strain poroelastic scalar_2 = varkappa, permeability coefficient
    compute_f1_poroelastic(ctx, Q, i, in, out, dXdx, f1, &scaler_1);
    compute_g1_poroelastic(ctx, Q, i, in, dXdx, scaler_1, out, grad_p, g1, &scalar_2);
    compute_g0_poroelastic(ctx, Q, i, in, grad_p, scaler_1, scalar_2, out, &g0);

    // Apply dXdx^T and weight
    RatelMatMatTransposeMultAtQuadraturePoint(Q, i, wdetJ, dXdx, f1, dvdX);
    RatelMatVecMultAtQuadraturePoint(Q, i, wdetJ, dXdx, g1, dqdX);
    // g0 = 0 for linear poroelasticity and only contributes in residual_ut
    q[i] = g0 * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute poroelasticity Jacobian evaluation

  @param[in]   ctx                          QFunction context, holding common parameters and model parameters
  @param[in]   Q                            Number of quadrature points
  @param[in]   compute_df1_poroelastic      Function to compute action of df1
  @param[in]   compute_dg1_poroelastic      Function to compute action of dg1
  @param[in]   compute_dg0_poroelastic      Function to compute action of dg0
  @param[in]   has_state_values             Boolean flag indicating model state values in residual evaluation
  @param[in]   has_stored_values            Boolean flag indicating model stores values in residual evaluation
  @param[in]   num_active_field_eval_modes  Number of active field evaluation modes
  @param[in]   in                           Input arrays
                                              - 0                   - volumetric qdata
                                              - `input_data_offset` - incremental change in u
  @param[out]  out                          Output array
                                              - 0 - action of QFunction on displacement
                                              - 1 - action of QFunction on pressure

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int PoroElasticityJacobian(void *ctx, CeedInt Q, RatelComputedf1_fwd_Poroelastic compute_df1_poroelastic,
                                                 RatelComputedg1_fwd_Poroelastic compute_dg1_poroelastic,
                                                 RatelComputedg0_fwd_Poroelastic compute_dg0_poroelastic, bool has_state_values,
                                                 bool has_stored_values, CeedInt num_active_field_eval_modes, const CeedScalar *const *in,
                                                 CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];

  // Outputs
  CeedScalar(*ddvdX)[3][CEED_Q_VLA] = (CeedScalar(*)[3][CEED_Q_VLA])out[0];
  CeedScalar *dq                    = out[1];
  CeedScalar(*dqdX)[CEED_Q_VLA]     = (CeedScalar(*)[CEED_Q_VLA])out[2];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar dXdx[3][3], grad_du[3][3], depsilon[3][3], df1[3][3], scalar_1, scalar_2, grad_dp[3], dg1[3], scalar_3, scalar_4, dg0;

    // Qdata
    const CeedScalar wdetJ = q_data[0][i];

    // Coordinate transformation and linearized of f1, g0, and g1
    // For linear poroelasticity df1 = dsigma; for finite strain in current df1 = dtau - tau * grad_du^T
    // For linear poroelasticity scalar_1 = trace_de for finite strain scalar_1 = Jm1
    // For finte strain poroelasticity scalar_2 = trace(depsilon) and unused in linear poroelasticity
    // For finte strain poroelasticity scalar_3 = varkappa and scalar_4 = varkappa_fwd
    compute_df1_poroelastic(ctx, Q, i, in, out, dXdx, grad_du, depsilon, df1, &scalar_1, &scalar_2);
    compute_dg1_poroelastic(ctx, Q, i, in, dXdx, depsilon, scalar_1, scalar_2, out, grad_dp, dg1, &scalar_3, &scalar_4);
    compute_dg0_poroelastic(ctx, Q, i, in, grad_du, grad_dp, scalar_1, scalar_2, scalar_3, scalar_4, out, &dg0);

    // Apply dXdx^T and weight
    RatelMatMatTransposeMultAtQuadraturePoint(Q, i, wdetJ, dXdx, df1, ddvdX);
    RatelMatVecMultAtQuadraturePoint(Q, i, wdetJ, dXdx, dg1, dqdX);
    dq[i] = dg0 * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
