/// @file
/// Ratel Mooney-Rivlin hyperelasticity at finite strain common QFunction source
#pragma once

#include <ceed/types.h>

#include "../../models/mooney-rivlin.h"           // IWYU pragma: export
#include "../utils.h"                             // IWYU pragma: export
#include "elasticity-diagnostic-common.h"         // IWYU pragma: export
#include "elasticity-volumetric-energy-common.h"  // IWYU pragma: export
#include "elasticity-volumetric-stress-common.h"  // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

#define FLOPS_Tau_MooneyRivlin (FLOPS_MatTrace + 9 + FLOPS_MatMatMultSymmetric + 2 * FLOPS_MatMatAddSymmetric + 18 + FLOPS_Tau_vol + 5)
#define FLOPS_S_MooneyRivlin (FLOPS_S_vol + FLOPS_MatMatMultSymmetric + FLOPS_MatTrace + 2 * FLOPS_MatMatAddSymmetric + 12 + 5)
#define FLOPS_dS_MooneyRivlin                                                                                           \
  (FLOPS_GreenLagrangeStrain_fwd + FLOPS_CInverse_fwd + FLOPS_MatMatContractSymmetric + FLOPS_dS_vol + FLOPS_MatTrace + \
   2 * FLOPS_MatMatAddSymmetric + 12 + 4)
#define FLOPS_FdSFTranspose_MooneyRivlin \
  (6 + 2 * FLOPS_MatTrace + 8 + 9 + FLOPS_MatMatMatMultSymmetric + FLOPS_GreenEulerStrain_fwd + FLOPS_MatMatMatAddSymmetric + 19 + 12)

/**
  @brief Compute Kirchoff tau for Mooney-Rivlin hyperelasticity.

 `tau = 2 (mu_1 + 2 mu_2) e + 2 mu_2 (trace(e) b - e*b) + (lambda * J dV/dJ) I`

  Ref https://ratel.micromorph.org/doc/modeling/materials/mooney-rivlin/#equation-mooney-rivlin-tau-stable

  @param[in]   J_dVdJ    J dV/dJ
  @param[in]   lambda    Lamé parameter
  @param[in]   mu_1      First Mooney-Rivlin parameter
  @param[in]   two_mu_2  Two times the second Mooney-Rivlin parameter
  @param[in]   e_sym     Green Euler strain, in symmetric representation
  @param[out]  tau_sym   Kirchoff tau, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelKirchhoffTau_MooneyRivlin(CeedScalar J_dVdJ, CeedScalar lambda, CeedScalar mu_1, CeedScalar two_mu_2,
                                                         const CeedScalar e_sym[6], CeedScalar tau_sym[6]) {
  CeedScalar       b_sym[6], eb_sym[6], bMeb_sym[6], tau_vol_sym;
  const CeedScalar trace_e = RatelMatTraceSymmetric(e_sym);

  // b = 2 e + I
  for (CeedInt j = 0; j < 6; j++) b_sym[j] = 2 * e_sym[j] + (j < 3);
  RatelMatMatMultSymmetric(1.0, e_sym, b_sym, eb_sym);
  // (trace(e)b - e*b)
  RatelMatMatAddSymmetric(trace_e, b_sym, -1.0, eb_sym, bMeb_sym);
  // -- intermediate tau = 2 (mu_1 + 2 mu_2) e + 2 mu_2 (trace(e) b - e*b)
  RatelMatMatAddSymmetric(2 * (mu_1 + two_mu_2), e_sym, two_mu_2, bMeb_sym, tau_sym);

  // Add `(lambda * J dV/dJ) I`
  RatelVolumetricKirchhoffTau(J_dVdJ, lambda, &tau_vol_sym);

  tau_sym[0] += tau_vol_sym;
  tau_sym[1] += tau_vol_sym;
  tau_sym[2] += tau_vol_sym;
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute second Kirchoff stress for Mooney-Rivlin hyperelasticity.

 `S = (lambda * J dV/dJ) * C_inv + 2 (mu_1 + 2 mu_2) C_inv * E + 2 mu_2 (tr(E) I - E)`

  Ref https://ratel.micromorph.org/doc/modeling/materials/mooney-rivlin/#equation-mooney-rivlin-tau-stable

  @param[in]   J_dVdJ     J dV/dJ
  @param[in]   lambda     Lamé parameter
  @param[in]   mu_1       First Mooney-Rivlin parameter
  @param[in]   two_mu_2   Two times the second Mooney-Rivlin parameter
  @param[in]   Jm1        Determinant of deformation gradient - 1
  @param[in]   C_inv_sym  Inverse of right Cauchy-Green tensor, in symmetric representation
  @param[in]   E_sym      Green Lagrange strain, in symmetric representation
  @param[out]  S_sym      Second Kirchoff stress, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelSecondKirchhoffStress_MooneyRivlin(CeedScalar J_dVdJ, CeedScalar lambda, CeedScalar mu_1, CeedScalar two_mu_2,
                                                                  CeedScalar Jm1, const CeedScalar C_inv_sym[6], const CeedScalar E_sym[6],
                                                                  CeedScalar S_sym[6]) {
  CeedScalar C_inv_E_sym[6], S_vol_sym[6];

  // Compute S_vol
  RatelVolumetricSecondKirchhoffStress(J_dVdJ, lambda, C_inv_sym, S_vol_sym);

  // Compute the Second Piola-Kirchhoff (S)
  // -- C_inv * E
  RatelMatMatMultSymmetric(1.0, C_inv_sym, E_sym, C_inv_E_sym);

  // -- intermediate S = S_vol + 2*(mu_1+2*mu_2)*C_inv*E
  RatelMatMatAddSymmetric(1., S_vol_sym, 2 * (mu_1 + two_mu_2), C_inv_E_sym, S_sym);
  const CeedScalar trace_E = RatelMatTraceSymmetric(E_sym);

  // trace(E)I - E
  const CeedScalar tr_EME[6] = {trace_E - E_sym[0], trace_E - E_sym[1], trace_E - E_sym[2], -E_sym[3], -E_sym[4], -E_sym[5]};

  // S += S + 2*mu_2*(tr(E)*I-E)
  RatelMatMatAddSymmetric(1.0, S_sym, two_mu_2, tr_EME, S_sym);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute derivative of second Kirchoff stress for Mooney-Rivlin hyperelasticity.

 `dS = dS_vol - (mu_1 + 2 mu_2) * dC_inv + 2 mu_2 (tr(dE)*I - dE)`
 `dS_vol = ((lambda J^2 * d2V/dJ2 + lambda * J dV/dJ) * (C_inv:dE))  C_inv + (lambda * J dV/dJ) dC_inv`

  Ref https://ratel.micromorph.org/doc/modeling/materials/mooney-rivlin/#equation-mooney-rivlin-tau-stable

  @param[in]   J_dVdJ     J dV/dJ
  @param[in]   J2_d2VdJ2  J^2 d^2V/dJ^2
  @param[in]   lambda     Lamé parameter
  @param[in]   mu_1       First Mooney-Rivlin parameter
  @param[in]   two_mu_2   Two times the second Mooney-Rivlin parameter
  @param[in]   Jm1        Determinant of deformation gradient - 1
  @param[in]   F          Deformation gradient
  @param[in]   grad_du    Gradient of incremental change in u
  @param[in]   C_inv_sym  Inverse of right Cauchy-Green tensor, in symmetric representation
  @param[out]  dS_sym     Derivative of second Kirchoff stress, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelSecondKirchhoffStress_MooneyRivlin_fwd(CeedScalar J_dVdJ, CeedScalar J2_d2VdJ2, CeedScalar lambda, CeedScalar mu_1,
                                                                      CeedScalar two_mu_2, CeedScalar Jm1, const CeedScalar F[3][3],
                                                                      const CeedScalar grad_du[3][3], const CeedScalar C_inv_sym[6],
                                                                      CeedScalar dS_sym[6]) {
  CeedScalar dE_sym[6], dC_inv_sym[6], dS_vol_sym[6];

  // dE - Green-Lagrange strain tensor increment
  RatelGreenLagrangeStrain_fwd(grad_du, F, dE_sym);
  // dC_inv = -2*C_inv*dE*C_inv
  RatelCInverse_fwd(C_inv_sym, dE_sym, dC_inv_sym);
  // -- C_inv:dE
  const CeedScalar Cinv_contract_dE = RatelMatMatContractSymmetric(1.0, C_inv_sym, dE_sym);

  // dS_vol
  RatelVolumetricSecondKirchhoffStress_fwd(J_dVdJ, J2_d2VdJ2, lambda, Cinv_contract_dE, C_inv_sym, dC_inv_sym, dS_vol_sym);

  // -- intermediate dS = dS_vol - (mu_1 + 2 mu_2) * dC_inv
  RatelMatMatAddSymmetric(1., dS_vol_sym, -(mu_1 + two_mu_2), dC_inv_sym, dS_sym);

  // dS += dS + 2*mu_2*(tr(dE)*I - dE)
  // -- trace(dE)I - dE
  const CeedScalar trace_dE    = RatelMatTraceSymmetric(dE_sym);
  const CeedScalar tr_dEMdE[6] = {trace_dE - dE_sym[0], trace_dE - dE_sym[1], trace_dE - dE_sym[2], -dE_sym[3], -dE_sym[4], -dE_sym[5]};

  RatelMatMatAddSymmetric(1.0, dS_sym, two_mu_2, tr_dEMdE, dS_sym);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute F*dS*F^T for Mooney-Rivlin hyperelasticity in current configuration.

  Ref https://ratel.micromorph.org/doc/modeling/methods/hyperelasticity/#equation-cur-simp-jac-mooney-rivlin

  @param[in]   J_dVdJ        J dV/dJ
  @param[in]   J2_d2VdJ2     J^2 d^2V/dJ^2
  @param[in]   lambda        Lamé parameter
  @param[in]   mu_1          First Mooney-Rivlin parameter
  @param[in]   two_mu_2      Two times the second Mooney-Rivlin parameter
  @param[in]   grad_du       Gradient of incremental change in u
  @param[in]   e_sym         Green Euler strain, in symmetric representation
  @param[out]  FdSFTranspose F*dS*F^T needed for computing df1 in current configuration

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelComputeFdSFTranspose_MooneyRivlin(CeedScalar J_dVdJ, CeedScalar J2_d2VdJ2, CeedScalar lambda, CeedScalar mu_1,
                                                                 CeedScalar two_mu_2, const CeedScalar grad_du[3][3], const CeedScalar e_sym[6],
                                                                 CeedScalar FdSFTranspose[3][3]) {
  CeedScalar depsilon_sym[6], b_sym[6], b[3][3], de_sym[6], b_depsilon_b_sym[6], FdSFTranspose_vol_sym[6], FdSFTranspose_sym[6];
  // Compute depsilon = (grad_du + grad_du^T)/2
  depsilon_sym[0] = grad_du[0][0];
  depsilon_sym[1] = grad_du[1][1];
  depsilon_sym[2] = grad_du[2][2];
  depsilon_sym[3] = (grad_du[1][2] + grad_du[2][1]) / 2.;
  depsilon_sym[4] = (grad_du[0][2] + grad_du[2][0]) / 2.;
  depsilon_sym[5] = (grad_du[0][1] + grad_du[1][0]) / 2.;

  const CeedScalar trace_depsilon = RatelMatTraceSymmetric(depsilon_sym);

  // F*dS_vol*F^T
  RatelComputeFdSFTransposeVolumetric(J_dVdJ, J2_d2VdJ2, lambda, trace_depsilon, depsilon_sym, FdSFTranspose_vol_sym);

  // F*dS*F^T = F*dS_vol*F^T + 2 (mu_1 + 2*mu_2) depsilon
  RatelMatMatAddSymmetric(1.0, FdSFTranspose_vol_sym, 2. * (mu_1 + two_mu_2), depsilon_sym, FdSFTranspose_sym);

  // -- b*depsilon*b
  for (CeedInt j = 0; j < 6; j++) b_sym[j] = 2 * e_sym[j] + (j < 3);
  RatelMatMatMatMultSymmetric(1.0, b_sym, depsilon_sym, b_depsilon_b_sym);

  RatelSymmetricMatUnpack(b_sym, b);
  RatelGreenEulerStrain_fwd(grad_du, b, de_sym);
  const CeedScalar trace_de = RatelMatTraceSymmetric(de_sym);
  // -- F*dS*F^T += F*dS*F^T + 2*mu_2*trace(de)*b - 2*mu_2*b*depsilon*b
  RatelMatMatMatAddSymmetric(1., FdSFTranspose_sym, two_mu_2 * trace_de, b_sym, -two_mu_2, b_depsilon_b_sym, FdSFTranspose_sym);
  RatelSymmetricMatUnpack(FdSFTranspose_sym, FdSFTranspose);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute strain energy for Mooney-Rivlin model.

 `psi = lambda * V(J) - (mu_1 + 2 mu_2) logJ + mu_1 I1(E) + 2 mu_2 II(E)`

  @param[in]   V              V(J)
  @param[in]   lambda         Lamé parameter
  @param[in]   mu_1           First Mooney-Rivlin parameter
  @param[in]   two_mu_2       Two times the second Mooney-Rivlin parameter
  @param[in]   Jm1            Determinant of deformation gradient - 1
  @param[in]   trace_strain   Trace of Green Lagrange or Green euler strain tensor (E or e)
  @param[in]   trace_strain2  Trace of E^2 (or e^2)
  @param[out]  strain_energy  Strain energy for Mooney-Rivlin model

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelStrainEnergy_MooneyRivlin(CeedScalar V, CeedScalar lambda, CeedScalar mu_1, CeedScalar two_mu_2, CeedScalar Jm1,
                                                         CeedScalar trace_strain, CeedScalar trace_strain2, CeedScalar *strain_energy) {
  const CeedScalar logJ = RatelLog1pSeries(Jm1);
  const CeedScalar I_2  = 0.5 * (trace_strain * trace_strain - trace_strain2);
  const CeedScalar II   = trace_strain + I_2;

  // Strain energy psi for Neo-Hookean
  *strain_energy = (lambda * V - (mu_1 + two_mu_2) * logJ + mu_1 * trace_strain + two_mu_2 * II);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute strain energy for Mooney-Rivlin hyperelasticity

  @param[in]   ctx  QFunction context, holding `RatelMooneyRivlinElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - gradient of u with respect to reference coordinates
  @param[out]  out  Output arrays
                      - 0 - strain energy

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(StrainEnergy_MooneyRivlin)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*energy) = (CeedScalar(*))out[0];

  // Context
  const RatelMooneyRivlinElasticityParams *context  = (RatelMooneyRivlinElasticityParams *)ctx;
  const CeedScalar                         lambda   = context->lambda;
  const CeedScalar                         mu_1     = context->mu_1;
  const CeedScalar                         two_mu_2 = context->two_mu_2;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar dXdx[3][3], grad_u[3][3], e_sym[6], V, strain_energy;

    // Qdata
    const CeedScalar wdetJ = q_data[0][i];

    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Read spatial derivatives of u; du/dX
    CeedScalar dudX[3][3];
    RatelGradUnpack(Q, i, ug, dudX);

    // Compute grad_u = du/dX * dX/dx
    // X is ref coordinate [-1,1]^3; x is physical coordinate
    RatelMatMatMult(1.0, dudX, dXdx, grad_u);

    // e - Euler strain tensor
    RatelGreenEulerStrain(grad_u, e_sym);
    const CeedScalar trace_e  = RatelMatTraceSymmetric(e_sym);
    const CeedScalar trace_e2 = RatelMatMatContractSymmetric(1.0, e_sym, e_sym);
    const CeedScalar Jm1      = RatelMatDetAM1(grad_u);

    VolumetricFunctionAndDerivatives(Jm1, &V, NULL, NULL);
    RatelStrainEnergy_MooneyRivlin(V, lambda, mu_1, two_mu_2, Jm1, trace_e, trace_e2, &strain_energy);

    // Strain energy Phi(E) for Mooney-Rivlin
    energy[i] = strain_energy * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute projected diagnostic stress, and strain energy invariants values for Mooney-Rivlin hyperelasticity

  @param[in]   ctx            QFunction context, holding `RatelMooneyRivlinElasticityParams`
  @param[in]   Jm1            Determinant of deformation gradient - 1
  @param[in]   V              V(J); volumetric energy
  @param[in]   J_dVdJ         J dV/dJ
  @param[in]   grad_u         Gradient of incremental change in u
  @param[out]  sigma_sym      Cauchy stress tensor in symmetric representation
  @param[out]  strain_energy  Strain energy
  @param[out]  trace_e        Trace of strain tensor e
  @param[out]  trace_e2       Trace of strain tensor e*e

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int DiagnosticStress_MooneyRivlin(void *ctx, const CeedScalar Jm1, const CeedScalar V, const CeedScalar J_dVdJ,
                                                        CeedScalar grad_u[3][3], CeedScalar sigma_sym[6], CeedScalar *strain_energy,
                                                        CeedScalar *trace_e, CeedScalar *trace_e2) {
  // Context
  const RatelMooneyRivlinElasticityParams *context  = (RatelMooneyRivlinElasticityParams *)ctx;
  const CeedScalar                         lambda   = context->lambda;
  const CeedScalar                         mu_1     = context->mu_1;
  const CeedScalar                         two_mu_2 = context->two_mu_2;

  CeedScalar e_sym[6], tau_sym[6];

  RatelGreenEulerStrain(grad_u, e_sym);
  RatelKirchhoffTau_MooneyRivlin(J_dVdJ, lambda, mu_1, two_mu_2, e_sym, tau_sym);
  for (CeedInt j = 0; j < 6; j++) sigma_sym[j] = tau_sym[j] / (Jm1 + 1.);

  // Strain tensor invariants
  *trace_e  = RatelMatTraceSymmetric(e_sym);
  *trace_e2 = RatelMatMatContractSymmetric(1.0, e_sym, e_sym);

  // Strain energy
  RatelStrainEnergy_MooneyRivlin(V, lambda, mu_1, two_mu_2, Jm1, *trace_e, *trace_e2, strain_energy);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute projected diagnostic values for Mooney-Rivlin hyperelasticity

  @param[in]   ctx  QFunction context, holding `RatelMooneyRivlinElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - u
                      - 2 - gradient of u with respect to reference coordinates
  @param[out]  out  Output arrays
                      - 0 - projected diagnostic values

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(Diagnostic_MooneyRivlin)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return ElasticityDiagnostic(ctx, Q, DiagnosticStress_MooneyRivlin, in, out);
}

/// @}
