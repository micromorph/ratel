/// @file
/// Ratel Neo-Hookean (initial configuration) with damage phase field
#include <ceed/types.h>
#ifndef CEED_RUNNING_JIT_PASS
#include <math.h>
#endif

#include "../../models/elasticity-damage.h"           // IWYU pragma: export
#include "../boundaries/platen-nitsche.h"             // IWYU pragma: export
#include "../utils.h"                                 // IWYU pragma: export
#include "elasticity-isochoric-neo-hookean-common.h"  // IWYU pragma: export
#include "elasticity-neo-hookean-damage-common.h"     // IWYU pragma: export

#define NUM_COMPONENTS_STATE_MPM_NeoHookeanCurrent_Damage 10
#define NUM_COMPONENTS_STORED_MPM_NeoHookeanCurrent_Damage 20

/// @addtogroup RatelMaterials
/// @{

/**
  @brief Set point fields for Neo-Hookean hyperelasticity with damage phase field

  @param[in]   ctx  QFunction context holding RatelElasticityDamageParams
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays, empty
  @param[out]  out  Output arrays
                      - 0 - density
                      - 1 - elastic parameters `RatelNeoHookeanElasticityPointFields`
                      - 2 - damage parameters `RatelElasticityDamagePointFields`
                      - 3 - initial state
                      - 4 - current state

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(SetPointFields_MPM_ElasticityDamageNeoHookeanCurrent)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Outputs
  CeedScalar *rho                = out[0];
  CeedScalar *elastic_parameters = out[1];
  CeedScalar *damage_parameters  = out[2];
  CeedScalar *initial_state      = out[3];
  CeedScalar *current_state      = out[4];

  // Context
  const RatelElasticityDamageParams         *context = (RatelElasticityDamageParams *)ctx;
  const RatelNeoHookeanElasticityPointFields elastic = {context->mu, context->bulk};
  const RatelElasticityDamagePointFields     damage  = {
      context->fracture_toughness, context->characteristic_length, context->residual_stiffness, context->damage_scaling, context->damage_viscosity,
  };
  const CeedScalar init_phi       = 0;
  const CeedScalar init_grad_u[9] = {0};

  // Quadrature Point Loop
  for (CeedInt i = 0; i < Q; i++) {
    rho[i] = context->common_parameters[RATEL_COMMON_PARAMETER_RHO];
    RatelStoredValuesPack(Q, i, 0, sizeof(elastic) / sizeof(CeedScalar), (const CeedScalar *)&elastic, elastic_parameters);
    RatelStoredValuesPack(Q, i, 0, sizeof(damage) / sizeof(CeedScalar), (const CeedScalar *)&damage, damage_parameters);
    RatelStoredValuesPack(Q, i, 0, 1, &init_phi, initial_state);
    RatelStoredValuesPack(Q, i, 1, 9, init_grad_u, initial_state);
    RatelStoredValuesPack(Q, i, 0, 1, &init_phi, current_state);
    RatelStoredValuesPack(Q, i, 1, 9, init_grad_u, current_state);
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute u_t term of damage residual

  @param[in]   ctx  QFunction context, holding `RatelElasticityDamageParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - volumetric qdata
                      - 1 - u_tilde_t (four components: 3 displacement components, 1 scalar damage field)
                      - 2 - rho (unused)
                      - 3 - elastic parameters (unused)
                      - 4 - damage parameters

  @param[out]  out  Output arrays
                      - 0 - action on u_t

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityDamageResidual_ut_NeoHookeanCurrent_MPM)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u_t)[CEED_Q_VLA]    = (const CeedScalar(*)[CEED_Q_VLA])in[1];
  const CeedScalar *damage_parameters   = in[4];

  // Outputs
  CeedScalar(*v_t)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Quadrature point loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    const CeedScalar                 wdetJ = q_data[0][i];
    RatelElasticityDamagePointFields damage;

    // Unpack damage parameters
    RatelStoredValuesUnpack(Q, i, 0, sizeof(damage) / sizeof(CeedScalar), damage_parameters, (CeedScalar *)&damage);

    // Update values
    v_t[0][i] = 0;
    v_t[1][i] = 0;
    v_t[2][i] = 0;
    v_t[3][i] = damage.damage_scaling * damage.damage_viscosity * u_t[3][i] * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute residual for Neo-hookean hyperelasticity initial conf.

  @param[in]   ctx  QFunction context, holding `RatelElasticityDamageParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - volumetric qdata with respect to X_previous
                      - 1 - state (10 components:  Psi_plus, 9 Grad_u_previous)
                      - 2 - u_tilde (four components: 3 displacement components, 1 scalar damage field)
                      - 3 - u_tilde_g gradient of u_tilde with respect to previous coordinates X_previous
                      - 4 - rho (unused)
                      - 5 - elastic parameters `RatelNeoHookeanElasticityPointFields`
                      - 6 - damage parameters `RatelElasticityDamagePointFields`
  @param[out]  out  Output arrays
                      - 0 - updated state (10 components: Psi_plus, 9 Grad_u)
                      - 1 - stored values (20 components: phi, Psi_plus_history_flag, 9 dXdx, Jn, Jm1, J_pow, 6 e_sym)
                      - 2 - action on u_tilde
                      - 3 - action on u_tilde_g

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityDamageResidual_NeoHookeanCurrent_MPM)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA]       = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*state)                    = in[1];
  const CeedScalar(*u_tilde)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[2];
  const CeedScalar(*u_tilde_g)[4][CEED_Q_VLA] = (const CeedScalar(*)[4][CEED_Q_VLA])in[3];
  const CeedScalar *elastic_parameters        = in[5];
  const CeedScalar *damage_parameters         = in[6];

  // Outputs
  CeedScalar(*current_state)       = out[0];
  CeedScalar(*stored)              = out[1];
  CeedScalar(*v)[CEED_Q_VLA]       = (CeedScalar(*)[CEED_Q_VLA])out[2];
  CeedScalar(*dvdX)[4][CEED_Q_VLA] = (CeedScalar(*)[4][CEED_Q_VLA])out[3];

  // Context
  const RatelElasticityDamageParams *context = (RatelElasticityDamageParams *)ctx;
  const bool                         use_AT1 = context->use_AT1;

  // Quadrature point loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar V, J_dVdJ, Psi_plus_history_flag, H, Psi_plus, Psi_plus_old;
    CeedScalar Grad_u[3][3], Grad_u_prev[3][3], Grad_u_tilde[3][3], F_tilde_inv[3][3], grad_phi[3], e_sym[6], b_sym[6], b[3][3], b_grad_phi[3],
        tau_degr_sym[6], dXdx_previous[3][3], dXdx[3][3], tau_degr[3][3];

    // Parameters
    RatelNeoHookeanElasticityPointFields elastic;
    RatelElasticityDamagePointFields     damage;

    RatelStoredValuesUnpack(Q, i, 0, sizeof(elastic) / sizeof(CeedScalar), elastic_parameters, (CeedScalar *)&elastic);
    RatelStoredValuesUnpack(Q, i, 0, sizeof(damage) / sizeof(CeedScalar), damage_parameters, (CeedScalar *)&damage);

    const CeedScalar Psi_plus_critical = use_AT1 ? 3 * damage.fracture_toughness / (8 * 2 * damage.characteristic_length) : 0;
    const CeedScalar c0                = use_AT1 ? 8. / 3. : 2.;

    // State
    RatelStoredValuesUnpack(Q, i, 0, 1, state, &Psi_plus_old);
    RatelStoredValuesUnpack(Q, i, 1, 9, state, (CeedScalar *)Grad_u_prev);

    // dX/dx from Q_data
    const CeedScalar wdetJ = q_data[0][i];
    RatelQdataUnpack(Q, i, q_data, dXdx_previous);

    //----- Displacement problem
    // Spatial derivatives of u_tilde
    const CeedScalar du_tildedX[3][3] = {
        {u_tilde_g[0][0][i], u_tilde_g[1][0][i], u_tilde_g[2][0][i]},
        {u_tilde_g[0][1][i], u_tilde_g[1][1][i], u_tilde_g[2][1][i]},
        {u_tilde_g[0][2][i], u_tilde_g[1][2][i], u_tilde_g[2][2][i]}
    };

    // Compute grad_delta_u = du/dX * dX/dx
    RatelMatMatMult(1.0, du_tildedX, dXdx_previous, Grad_u_tilde);

    // Compute Grad_u = Grad_u_prev + Grad_u_tilde + Grad_u_tilde * Grad_u_prev
    CeedScalar Grad_u_temp[3][3];
    RatelMatMatMult(1.0, Grad_u_tilde, Grad_u_prev, Grad_u_temp);
    RatelMatMatMatAdd(1.0, Grad_u_prev, 1.0, Grad_u_tilde, 1.0, Grad_u_temp, Grad_u);

    const CeedScalar F_tilde[3][3] = {
        {Grad_u_tilde[0][0] + 1, Grad_u_tilde[0][1],     Grad_u_tilde[0][2]    },
        {Grad_u_tilde[1][0],     Grad_u_tilde[1][1] + 1, Grad_u_tilde[1][2]    },
        {Grad_u_tilde[2][0],     Grad_u_tilde[2][1],     Grad_u_tilde[2][2] + 1}
    };

    // Read damage (forth component)
    const CeedScalar phi = u_tilde[3][i];

    // Compute J - 1
    const CeedScalar Jn        = RatelMatDetAM1(Grad_u_prev) + 1;
    const CeedScalar Jm1       = RatelMatDetAM1(Grad_u);
    const CeedScalar Jm1_tilde = RatelMatDetAM1(Grad_u_tilde);
    const CeedScalar J_pow     = pow(Jm1 + 1., -2. / 3.);

    // Volumetric function and derivatives
    VolumetricFunctionAndDerivatives(Jm1, &V, &J_dVdJ, NULL);

    // Compute degraded kirchhoff stress tau_degr
    RatelGreenEulerStrain(Grad_u, e_sym);
    ComputeDegradedKirchhoffTau_NeoHookean(J_dVdJ, elastic.bulk, 2 * elastic.mu, Jm1, J_pow, damage.residual_stiffness, phi, e_sym, tau_degr_sym);
    RatelSymmetricMatUnpack(tau_degr_sym, tau_degr);

    // Compute F_tilde^{-1}
    RatelMatInverse(F_tilde, Jm1_tilde + 1, F_tilde_inv);

    // Compute dXdx = dX/dx = dX/dx_previous * F_tilde^{-1}
    RatelMatMatMult(1.0, dXdx_previous, F_tilde_inv, dXdx);

    // Compute (grad(v), tau_degr)
    for (CeedInt j = 0; j < 3; j++) {
      for (CeedInt k = 0; k < 3; k++) {
        dvdX[j][k][i] = 0;
        for (CeedInt m = 0; m < 3; m++) {
          dvdX[j][k][i] += wdetJ * dXdx[j][m] * tau_degr[k][m] / Jn;
        }
      }
    }

    //----- Damage problem
    // Compute Psi_plus
    const CeedScalar trace_e = RatelMatTraceSymmetric(e_sym);
    PsiPlus_NeoHookean(V, elastic.mu, elastic.bulk, Jm1, trace_e, &Psi_plus);

    // Treat Psi_plus as a history variable and ensure that increases monotonically for crack irreversibility
    Psi_plus_history_flag = Psi_plus > Psi_plus_old;
    Psi_plus              = RatelMax(Psi_plus, Psi_plus_old);
    Psi_plus              = RatelMax(Psi_plus_critical, Psi_plus);

    // Spatial derivative of phi
    const CeedScalar phi_g[3] = {u_tilde_g[0][3][i], u_tilde_g[1][3][i], u_tilde_g[2][3][i]};

    // Compute grad_phi = dX/dx^T dphi/dX
    RatelMatTransposeVecMult(1.0, dXdx, phi_g, grad_phi);
    const CeedScalar dg_dphi = -2 * (1 - damage.residual_stiffness) * (1 - phi);

    // If AT2, substitute appropiate values for c0 and alpha (hence dalpha)
    const CeedScalar dalpha_dphi = use_AT1 ? 1. : 2. * phi;

    // Compute H and update v values
    H       = dg_dphi * Psi_plus + (damage.fracture_toughness / (c0 * damage.characteristic_length)) * dalpha_dphi;
    v[0][i] = 0;
    v[1][i] = 0;
    v[2][i] = 0;
    v[3][i] = damage.damage_scaling * H * wdetJ;

    // Compute b = 2 e + I
    for (CeedInt j = 0; j < 6; j++) b_sym[j] = 2 * e_sym[j] + (j < 3);
    RatelSymmetricMatUnpack(b_sym, b);
    RatelMatVecMult(1.0, b, grad_phi, b_grad_phi);

    // Update fourth component of dvdX via (grad(q), (2 Gc l0 / c0) b * grad(phi))
    const CeedScalar scale = damage.damage_scaling * 2. * damage.fracture_toughness * damage.characteristic_length * wdetJ / c0;
    for (CeedInt j = 0; j < 3; j++) {
      dvdX[j][3][i] = scale * (b_grad_phi[0] * dXdx[j][0] + b_grad_phi[1] * dXdx[j][1] + b_grad_phi[2] * dXdx[j][2]);
    }

    // Save updated values for state fields
    RatelStoredValuesPack(Q, i, 0, 1, &Psi_plus, current_state);
    RatelStoredValuesPack(Q, i, 1, 9, (CeedScalar *)Grad_u, current_state);

    // Store values
    RatelStoredValuesPack(Q, i, 0, 1, &phi, stored);
    RatelStoredValuesPack(Q, i, 1, 1, &Psi_plus_history_flag, stored);
    RatelStoredValuesPack(Q, i, 2, 9, (CeedScalar *)dXdx, stored);
    RatelStoredValuesPack(Q, i, 11, 1, &Jn, stored);
    RatelStoredValuesPack(Q, i, 12, 1, &Jm1, stored);
    RatelStoredValuesPack(Q, i, 13, 1, &J_pow, stored);
    RatelStoredValuesPack(Q, i, 14, 6, (CeedScalar *)e_sym, stored);
  }  // End of Quadrature Point Loop

  // Prevent unused functions warnings
  (void)StrainEnergy_IsochoricNeoHookean;
  (void)Diagnostic_IsochoricNeoHookean;
  (void)ElasticityDamageStrainEnergy_NeoHookean;
  (void)ElasticityDamageDiagnostic_NeoHookean;
  (void)ElasticityDamageDualDiagnostic_NeoHookean;
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute jacobian for Neo-hookean hyperelasticity initial conf.

  @param[in]   ctx  QFunction context, holding `RatelElasticityDamageParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - volumetric qdata with respect to X_previous
                      - 1 - current state values (10 components: Psi_plus, 9 Grad_u)
                      - 2 - Stored values (20 components: phi, Psi_plus_history_flag, 9 dXdx, Jn, Jm1, J_pow, 6 e_sym)
                      - 3 - du (four components: 3 displacement components, 1 damage field)
                      - 4 - du_g gradient of du with respect to previous coordinates X_previous
                      - 5 - rho (unused)
                      - 6 - elastic parameters `RatelNeoHookeanElasticityPointFields`
                      - 7 - damage parameters `RatelElasticityDamagePointFields`
  @param[out]  out  Output arrays
                      - 0 - action on du
                      - 1 - action on du_g

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityDamageJacobian_NeoHookeanCurrent_MPM)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA]  = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*current_state)       = in[1];
  const CeedScalar(*stored)              = in[2];
  const CeedScalar(*du)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[3];
  const CeedScalar(*du_g)[4][CEED_Q_VLA] = (const CeedScalar(*)[4][CEED_Q_VLA])in[4];
  const CeedScalar *elastic_parameters   = in[6];
  const CeedScalar *damage_parameters    = in[7];

  // Outputs
  CeedScalar(*dv)[CEED_Q_VLA]       = (CeedScalar(*)[CEED_Q_VLA])out[0];
  CeedScalar(*ddvdX)[4][CEED_Q_VLA] = (CeedScalar(*)[4][CEED_Q_VLA])out[1];

  // Context Elasticity+Damage
  const RatelElasticityDamageParams *context     = (RatelElasticityDamageParams *)ctx;
  const bool                         use_AT1     = context->use_AT1;
  const bool                         use_offdiag = context->use_offdiagonal;
  const CeedScalar                   shift_v     = context->common_parameters[1];

  // Set d2alpha_dphi2 and c0 based on AT1 or AT2
  const CeedScalar c0            = use_AT1 ? 8. / 3. : 2.;
  const CeedScalar d2alpha_dphi2 = use_AT1 ? 0. : 2.;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar J_dVdJ, J2_d2VdJ2, Psi_plus_history_flag, dPsi_plus, dH, Jm1, J_pow, Jn, Psi_plus, phi;
    CeedScalar grad_dphi[3], e_sym[6], b_sym[6], b[3][3], b_grad_dphi[3], dXdx[3][3], grad_du[3][3], depsilon_sym[6], tau_degr_sym[6], tau_degr[3][3],
        grad_du_tau_degr[3][3], FdSFTranspose_degr[3][3], df1[3][3];

    // Parameters
    RatelNeoHookeanElasticityPointFields elastic;
    RatelElasticityDamagePointFields     damage;

    RatelStoredValuesUnpack(Q, i, 0, sizeof(elastic) / sizeof(CeedScalar), elastic_parameters, (CeedScalar *)&elastic);
    RatelStoredValuesUnpack(Q, i, 0, sizeof(damage) / sizeof(CeedScalar), damage_parameters, (CeedScalar *)&damage);

    const CeedScalar d2g_dphi2 = 2 * (1 - damage.residual_stiffness);

    // State
    RatelStoredValuesUnpack(Q, i, 0, 1, current_state, &Psi_plus);

    // Read spatial derivatives of increment u; d(du)/dX = d(du)/dX
    const CeedScalar ddudX[3][3] = {
        {du_g[0][0][i], du_g[1][0][i], du_g[2][0][i]},
        {du_g[0][1][i], du_g[1][1][i], du_g[2][1][i]},
        {du_g[0][2][i], du_g[1][2][i], du_g[2][2][i]}
    };

    // Extract Qdata
    const CeedScalar wdetJ = q_data[0][i];

    // Unpack stored values
    RatelStoredValuesUnpack(Q, i, 0, 1, stored, &phi);
    RatelStoredValuesUnpack(Q, i, 1, 1, stored, &Psi_plus_history_flag);
    RatelStoredValuesUnpack(Q, i, 2, 9, stored, (CeedScalar *)dXdx);
    RatelStoredValuesUnpack(Q, i, 11, 1, stored, &Jn);
    RatelStoredValuesUnpack(Q, i, 12, 1, stored, &Jm1);
    RatelStoredValuesUnpack(Q, i, 13, 1, stored, &J_pow);
    RatelStoredValuesUnpack(Q, i, 14, 6, stored, e_sym);

    // Compute grad_du = ddu/dX * dX/dx
    // X is ref coordinate [-1,1]^3; x is physical coordinate in current configuration
    RatelMatMatMult(1.0, ddudX, dXdx, grad_du);

    // Volumetric function and derivatives
    VolumetricFunctionAndDerivatives(Jm1, NULL, &J_dVdJ, &J2_d2VdJ2);

    // Compute tau_degr
    ComputeDegradedKirchhoffTau_NeoHookean(J_dVdJ, elastic.bulk, 2 * elastic.mu, Jm1, J_pow, damage.residual_stiffness, phi, e_sym, tau_degr_sym);
    RatelSymmetricMatUnpack(tau_degr_sym, tau_degr);

    // Compute grad_du_tau = grad_du*tau
    RatelMatMatMult(1.0, grad_du, tau_degr, grad_du_tau_degr);

    // Compute F*dS*F^T
    const CeedScalar dphi = du[3][i];
    RatelComputeDegradedFdSFTranspose_NeoHookean(J_dVdJ, J2_d2VdJ2, elastic.bulk, elastic.mu, Jm1, J_pow, damage.residual_stiffness, use_offdiag, phi,
                                                 dphi, e_sym, grad_du, depsilon_sym, FdSFTranspose_degr);
    RatelMatMatAdd(1.0, grad_du_tau_degr, 1.0, FdSFTranspose_degr, df1);

    for (CeedInt j = 0; j < 3; j++) {
      for (CeedInt k = 0; k < 3; k++) {
        ddvdX[j][k][i] = 0;
        for (CeedInt m = 0; m < 3; m++) {
          ddvdX[j][k][i] += dXdx[j][m] * df1[k][m];
        }
        ddvdX[j][k][i] *= wdetJ / Jn;
      }
    }

    //--Compute l0*Gc*(grad(dphi), grad(q))
    // Spatial derivative of phi
    const CeedScalar dphi_g[3] = {du_g[0][3][i], du_g[1][3][i], du_g[2][3][i]};

    // grad_dphi = dX/dx^T * d(dphi)/dX
    RatelMatTransposeVecMult(1.0, dXdx, dphi_g, grad_dphi);

    // Get dg_dphi
    const CeedScalar dg_dphi = -2 * (1 - damage.residual_stiffness) * (1 - phi);

    // Compute d_phi_t
    const CeedScalar dphi_t = shift_v * dphi;

    // Compute dPsi_plus
    const bool add_offdiag = !(Psi_plus_history_flag == 0. || !use_offdiag);

    PsiPlus_NeoHookeanCurrent_fwd(J_dVdJ, 2 * elastic.mu, elastic.bulk, Jm1, J_pow, e_sym, depsilon_sym, &dPsi_plus);
    dPsi_plus = add_offdiag * dPsi_plus;

    // Compute dH and dv values
    dH = d2g_dphi2 * dphi * Psi_plus + dg_dphi * dPsi_plus +
         (damage.fracture_toughness / (c0 * damage.characteristic_length)) * d2alpha_dphi2 * dphi + damage.damage_viscosity * dphi_t;
    dv[0][i] = 0;
    dv[1][i] = 0;
    dv[2][i] = 0;
    dv[3][i] = damage.damage_scaling * dH * wdetJ;

    // Compute b = 2 e + I
    for (CeedInt j = 0; j < 6; j++) b_sym[j] = 2 * e_sym[j] + (j < 3);
    RatelSymmetricMatUnpack(b_sym, b);
    RatelMatVecMult(1.0, b, grad_dphi, b_grad_dphi);

    // Update fourth component of dvdX via (grad(dq), (2 Gc l0 / c0) b * grad(dphi))
    const CeedScalar scale = damage.damage_scaling * 2. * damage.fracture_toughness * damage.characteristic_length * wdetJ / c0;
    for (CeedInt j = 0; j < 3; j++) {
      ddvdX[j][3][i] = scale * (b_grad_dphi[0] * dXdx[j][0] + b_grad_dphi[1] * dXdx[j][1] + b_grad_dphi[2] * dXdx[j][2]);
    }
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_JACOBIAN_MPM_NeoHookeanCurrent_Damage                                                               \
  (2 + FLOPS_Degraded_Tau_NeoHookean + FLOPS_MatMatMult + FLOPS_J_dVdJ + FLOPS_J2_d2VdJ2 + FLOPS_MatMatMult +     \
   FLOPS_Degraded_FdSFTranspose_NeoHookean + FLOPS_MatMatAdd + FLOPS_MatMatMult + 10 + FLOPS_MatVecMult + 4 + 1 + \
   FLOPS_PsiPlus_NeoHookeanCurrent_fwd + 1 + 11 + 2 + 5 + 6)

/// @}
