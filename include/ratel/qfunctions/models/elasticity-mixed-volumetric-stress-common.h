/// @file
/// Ratel mixed volumetric common stress at finite strain QFunction source
#pragma once

#include <ceed/types.h>

#include "../../models/mixed-neo-hookean.h"       // IWYU pragma: export
#include "../utils.h"                             // IWYU pragma: export
#include "elasticity-volumetric-energy-common.h"  // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

#define FLOPS_Tau_vol_mixed (FLOPS_J_dVdJ + 4)
#define FLOPS_S_vol_mixed (FLOPS_J_dVdJ + 4 + FLOPS_ScalarMatMultSymmetric)
#define FLOPS_dS_vol_mixed (FLOPS_J_dVdJ + FLOPS_J2_d2VdJ2 + 12 + FLOPS_MatMatAddSymmetric + 12)
#define FLOPS_FdSFTranspose_vol_mixed (FLOPS_J_dVdJ + FLOPS_J2_d2VdJ2 + 12 + FLOPS_ScalarMatMultSymmetric + 4)

/**
  @brief Compute volumetric Kirchoff tau for mixed fields hyperelasticity.

 `tau_vol = [bulk_primal * J dV/dJ - p J] I`

  @param[in]   J_dVdJ       J dV/dJ
  @param[in]   bulk_primal  Primal bulk modulus
  @param[in]   Jm1          Determinant of deformation gradient - 1
  @param[in]   p            Pressure
  @param[out]  tau_vol_sym  Volumetric Kirchoff tau

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelVolumetricKirchhoffTau_Mixed(CeedScalar J_dVdJ, CeedScalar bulk_primal, CeedScalar Jm1, CeedScalar p,
                                                            CeedScalar *tau_vol_sym) {
  // -- [bulk_primal * J dV/dJ - p J]
  *tau_vol_sym = bulk_primal * J_dVdJ - p * (Jm1 + 1.);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute volumetric part of second Kirchoff stress for mixed fields hyperelasticity.

 `S_vol = [bulk_primal * J dV/dJ - p J] * C_inv`

  @param[in]   J_dVdJ        J dV/dJ
  @param[in]   bulk_primal   Primal bulk modulus
  @param[in]   p             Pressure
  @param[in]   Jm1           Determinant of deformation gradient - 1
  @param[in]   C_inv_sym     Inverse of right Cauchy-Green tensor, in symmetric representation
  @param[out]  S_vol_sym     Volumetric second Kirchoff stress, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelVolumetricSecondKirchhoffStress_Mixed(CeedScalar J_dVdJ, CeedScalar bulk_primal, CeedScalar p, CeedScalar Jm1,
                                                                     const CeedScalar C_inv_sym[6], CeedScalar S_vol_sym[6]) {
  // -- [bulk_primal * J dV/dJ - p J]
  const CeedScalar coeff_1 = bulk_primal * J_dVdJ - p * (Jm1 + 1.);

  // S_vol = coeff_1 * C_inv
  RatelScalarMatMultSymmetric(coeff_1, C_inv_sym, S_vol_sym);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute derivative of volumetric second Kirchoff stress for mixed fields hyperelasticity.

 `dS_vol = [(bulk_primal J^2 * d2V/dJ2 + (bulk_primal * J dV/dJ - p J)) (C_inv:dE) - dp J]  C_inv + [bulk_primal * J dV/dJ - p J ] dC_inv`

  @param[in]   J_dVdJ            J dV/dJ
  @param[in]   J2_d2VdJ2         J^2 d^2V/dJ^2
  @param[in]   bulk_primal       Primal bulk modulus
  @param[in]   p                 Pressure
  @param[in]   dp                Increment of Pressure
  @param[in]   Jm1               Determinant of deformation gradient - 1
  @param[in]   Cinv_contract_dE  `C_inv : dE`
  @param[in]   C_inv_sym         Inverse of right Cauchy-Green tensor, in symmetric representation
  @param[in]   dC_inv_sym        Derivative of C^{-1}, in symmetric representation
  @param[out]  dS_vol_sym        Derivative of primal second Kirchoff stress, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelVolumetricSecondKirchhoffStress_Mixed_fwd(CeedScalar J_dVdJ, CeedScalar J2_d2VdJ2, CeedScalar bulk_primal,
                                                                         CeedScalar p, CeedScalar dp, CeedScalar Jm1, CeedScalar Cinv_contract_dE,
                                                                         const CeedScalar C_inv_sym[6], const CeedScalar dC_inv_sym[6],
                                                                         CeedScalar dS_vol_sym[6]) {
  const CeedScalar J       = Jm1 + 1.;
  const CeedScalar coeff_1 = (bulk_primal * J2_d2VdJ2 + (bulk_primal * J_dVdJ - p * J)) * Cinv_contract_dE - dp * J;
  const CeedScalar coeff_2 = bulk_primal * J_dVdJ - p * J;

  // dS_vol = coeff_1 C_inv + coeff_2 dC_inv
  RatelMatMatAddSymmetric(coeff_1, C_inv_sym, coeff_2, dC_inv_sym, dS_vol_sym);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute F*dS_vol*F^T for mixed fields hyperelasticity in current configuration.

  @param[in]   J_dVdJ                  J dV/dJ
  @param[in]   J2_d2VdJ2               J^2 d^2V/dJ^2
  @param[in]   bulk_primal             Primal bulk modulus
  @param[in]   p                       Pressure
  @param[in]   dp                      Increment of Pressure
  @param[in]   Jm1                     Determinant of deformation gradient - 1
  @param[in]   trace_depsilon          `trace(depsilon)`
  @param[in]   depsilon_sym            depsilon = (grad_du + grad_du^T)/2
  @param[out]  FdSFTranspose_vol_sym   F*dS_vol*F^T needed for computing df1 in current configuration

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelComputeFdSFTransposeVolumetric_Mixed(CeedScalar J_dVdJ, CeedScalar J2_d2VdJ2, CeedScalar bulk_primal, CeedScalar p,
                                                                    CeedScalar dp, CeedScalar Jm1, CeedScalar trace_depsilon,
                                                                    const CeedScalar depsilon_sym[6], CeedScalar FdSFTranspose_vol_sym[6]) {
  const CeedScalar J       = Jm1 + 1.;
  const CeedScalar coeff_1 = (bulk_primal * J2_d2VdJ2 + (bulk_primal * J_dVdJ - p * J)) * trace_depsilon - dp * J;
  const CeedScalar coeff_2 = bulk_primal * J_dVdJ - p * J;

  // F*dS_vol*F^T = -2 * coeff_2 * depsilon_sym
  RatelScalarMatMultSymmetric(-2. * coeff_2, depsilon_sym, FdSFTranspose_vol_sym);

  // Add coeff_1 * I to FdSFTranspose_vol_sym
  FdSFTranspose_vol_sym[0] += coeff_1;
  FdSFTranspose_vol_sym[1] += coeff_1;
  FdSFTranspose_vol_sym[2] += coeff_1;
  return CEED_ERROR_SUCCESS;
}

/// @}
