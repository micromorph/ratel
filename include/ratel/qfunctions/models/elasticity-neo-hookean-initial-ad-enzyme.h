/// @file
/// Ratel neo-Hookean hyperelasticity at finite strain in initial configuration with autodifferentiation QFunction source
#include <ceed/types.h>
#ifndef CEED_RUNNING_JIT_PASS
#include <math.h>
#endif

#include "../../models/elasticity-linear.h"  // IWYU pragma: export
#include "../../models/neo-hookean.h"        // IWYU pragma: export
#include "../utils.h"                        // IWYU pragma: export
#include "elasticity-common.h"               // IWYU pragma: export
#include "elasticity-neo-hookean-common.h"   // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

#define RATEL_ELASTICITY_NEO_HOOKEAN_INITIAL_AD_TAPE_SIZE 6

#define NUM_COMPONENTS_STATE_NeoHookeanInitial_AD_Enzyme 0
#define NUM_COMPONENTS_STORED_NeoHookeanInitial_AD_Enzyme (15 + RATEL_ELASTICITY_NEO_HOOKEAN_INITIAL_AD_TAPE_SIZE)
#define NUM_ACTIVE_FIELD_EVAL_MODES_NeoHookeanInitial_AD_Enzyme 1

/**
  @brief Compute strain energy for neo-Hookean hyperelasticity

  @param[in]   E_sym   Green Lagrange strain, in symmetric representation
  @param[in]   lambda  Lamé parameter
  @param[in]   mu      Shear modulus

  @return A scalar value: strain energy
**/
CEED_QFUNCTION_HELPER CeedScalar RatelStrainEnergy_NeoHookeanInitial_AD_Enzyme(CeedScalar E_sym[6], CeedScalar lambda, CeedScalar mu) {
  CeedScalar E2_sym[6];

  // Calculate 2*E
  for (CeedInt i = 0; i < 6; i++) E2_sym[i] = E_sym[i] * 2;

  // log(J)
  const CeedScalar detCm1 = RatelMatDetAM1Symmetric(E2_sym);
  const CeedScalar J      = sqrt(detCm1 + 1);
  const CeedScalar logJ   = RatelLog1pSeries(detCm1) / 2.;

  // trace(E)
  CeedScalar traceE = RatelMatTraceSymmetric(E_sym);
  return lambda * (J * J - 1) / 4 - lambda * logJ / 2 + mu * (-logJ + traceE);
}

/// @}

// -----------------------------------------------------------------------------
//  Compute Second Piola Kirchhoff stress:
// -----------------------------------------------------------------------------
// -- Enzyme-AD
void       __enzyme_autodiff(void *, ...);
void       __enzyme_augmentfwd(void *, ...);
void       __enzyme_fwdsplit(void *, ...);
int        __enzyme_augmentsize(void *, ...);
extern int enzyme_tape, enzyme_const, enzyme_dup, enzyme_nofree, enzyme_allocated;

void *__enzyme_function_like[2] = {(void *)RatelLog1pSeries, (void *)"log1p"};

/// @addtogroup RatelMaterials
/// @{

/**
  @brief Compute second Piola-Kirchhoff for neo-Hookean hyperelasticity in initial configuration with Enzyme

  @param[in]   lambda  Lamé parameter
  @param[in]   mu      Shear modulus
  @param[in]   E_sym   Green Lagrange strain, in symmetric representation
  @param[out]  S_sym   Second Piola-Kirchhoff, in symmetric representation
**/
CEED_QFUNCTION_HELPER void SecondPiolaKirchhoffStress_NeoHookean_AD(const CeedScalar lambda, const CeedScalar mu, CeedScalar E_sym[6],
                                                                    CeedScalar S_sym[6]) {
  for (CeedInt i = 0; i < 6; i++) S_sym[i] = 0.;
  __enzyme_autodiff((void *)RatelStrainEnergy_NeoHookeanInitial_AD_Enzyme, E_sym, S_sym, enzyme_const, lambda, enzyme_const, mu);
  for (CeedInt i = 3; i < 6; i++) S_sym[i] /= 2.;
}

/**
  @brief Compute first Piola-Kirchhoff for neo-Hookean hyperelasticity in initial configuration with Enzyme

  @param[in]   E       Green Lagrange strain
  @param[in]   S       Second Piola-Kirchhoff
  @param[in]   lambda  Lamé parameter
  @param[in]   mu      Shear modulus
  @param[out]  tape    Enzyme tape
**/
CEED_QFUNCTION_HELPER void S_augmentfwd(double *S, double *E, const double lambda, const double mu, double *tape) {
  int tape_bytes = __enzyme_augmentsize((void *)SecondPiolaKirchhoffStress_NeoHookean_AD, enzyme_const, enzyme_const, enzyme_dup, enzyme_dup);

  __enzyme_augmentfwd((void *)SecondPiolaKirchhoffStress_NeoHookean_AD, enzyme_allocated, tape_bytes, enzyme_tape, tape, enzyme_nofree, enzyme_const,
                      lambda, enzyme_const, mu, E, (double *)NULL, S, (double *)NULL);
}

/**
  @brief Compute derivative of first Piola-Kirchhoff for neo-Hookean hyperelasticity in initial configuration with Enzyme

  @param[out]  dS      Derivative of first Piola-Kirchoff
  @param[out]  dE      Derivative of Green Lagrange strain
  @param[in]   lambda  Lamé parameter
  @param[in]   mu      Shear modulus
  @param[in]   tape    Enzyme tape
**/
CEED_QFUNCTION_HELPER void dS_fwd(double *dS, double *dE, const double lambda, const double mu, const double *tape) {
  int tape_bytes = __enzyme_augmentsize((void *)SecondPiolaKirchhoffStress_NeoHookean_AD, enzyme_const, enzyme_const, enzyme_dup, enzyme_dup);

  for (CeedInt i = 0; i < 6; i++) dS[i] = 0.;
  __enzyme_fwdsplit((void *)SecondPiolaKirchhoffStress_NeoHookean_AD, enzyme_allocated, tape_bytes, enzyme_tape, tape, enzyme_const, lambda,
                    enzyme_const, mu, (double *)NULL, dE, (double *)NULL, dS);
}

/**
  @brief Compute `P` for neo-Hookean hyperelasticity in initial configuration with Enzyme

  @param[in]   ctx                   QFunction context, holding `RatelNeoHookeanElasticityParams`
  @param[in]   Q                     Number of quadrature points
  @param[in]   i                     Current quadrature point
  @param[in]   in                    Input arrays
                                       - 0 - volumetric qdata
                                       - 1 - gradient of u with respect to reference coordinates
  @param[out]  out                   Output arrays
                                       - 0 - stored gradient of u with respect to physical coordinates, Second Piola-Kirchhoff, and tape
  @param[out]  dXdx                  Coordinate transformation
  @param[out]  f1                    `f1 = P = F * S`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int f1_NeoHookeanInitial_AD_Enzyme(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in,
                                                         CeedScalar *const *out, CeedScalar dXdx[3][3], CeedScalar f1[3][3]) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*stored_values) = out[0];

  // Context
  const RatelNeoHookeanElasticityParams *context = (RatelNeoHookeanElasticityParams *)ctx;
  const CeedScalar                       mu      = context->mu;
  const CeedScalar                       lambda  = context->lambda;

  CeedScalar grad_u[3][3], E_sym[6], C_inv_sym[6], tape[RATEL_ELASTICITY_NEO_HOOKEAN_INITIAL_AD_TAPE_SIZE], S_sym[6], S[3][3];

  // Qdata
  RatelQdataUnpack(Q, i, q_data, dXdx);

  // Read spatial derivatives of u; du/dX
  CeedScalar dudX[3][3];
  RatelGradUnpack(Q, i, ug, dudX);

  // Compute grad_u = du/dX * dX/dx
  // X is ref coordinate [-1,1]^3; x is physical coordinate in initial configuration
  RatelMatMatMult(1.0, dudX, dXdx, grad_u);

  // Compute the Deformation Gradient : F = I + grad_u
  const CeedScalar F[3][3] = {
      {grad_u[0][0] + 1, grad_u[0][1],     grad_u[0][2]    },
      {grad_u[1][0],     grad_u[1][1] + 1, grad_u[1][2]    },
      {grad_u[2][0],     grad_u[2][1],     grad_u[2][2] + 1}
  };

  // Compute J-1
  const CeedScalar Jm1 = RatelMatDetAM1(grad_u);

  RatelGreenLagrangeStrain(grad_u, E_sym);
  RatelCInverse(E_sym, Jm1, C_inv_sym);

  // Compute Second Piola-Kirchhoff (S) with Enzyme-AD
  S_augmentfwd(S_sym, E_sym, lambda, mu, tape);
  RatelSymmetricMatUnpack(S_sym, S);

  // Compute the First Piola-Kirchhoff f1 = P = F*S
  RatelMatMatMult(1.0, F, S, f1);

  // Store values
  RatelStoredValuesPack(Q, i, 0, 9, (CeedScalar *)grad_u, stored_values);
  RatelStoredValuesPack(Q, i, 9, 6, (CeedScalar *)S_sym, stored_values);
  RatelStoredValuesPack(Q, i, 15, RATEL_ELASTICITY_NEO_HOOKEAN_INITIAL_AD_TAPE_SIZE, (CeedScalar *)tape, stored_values);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute linearization of `P` for neo-Hookean hyperelasticity in initial configuration with Enzyme

  @param[in]   ctx                   QFunction context, holding `RatelNeoHookeanElasticityParams`
  @param[in]   Q                     Number of quadrature points
  @param[in]   i                     Current quadrature point
  @param[in]   in                    Input arrays
                                       - 0 - volumetric qdata
                                       - 1 - stored gradient of u with respect to physical coordinates, Second Piola-Kirchhoff, and tape
                                       - 2 - gradient of incremental change to u with respect to reference coordinates
  @param[out]  out                   Output arrays, unused
  @param[out]  dXdx                  Coordinate transformation
  @param[out]  df1                   `df1 = dP =  dF * S + F * dS`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int df1_NeoHookeanInitial_AD_Enzyme(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in,
                                                          CeedScalar *const *out, CeedScalar dXdx[3][3], CeedScalar df1[3][3]) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*stored_values)      = in[1];
  const CeedScalar(*dug)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[2];

  // Context
  const RatelNeoHookeanElasticityParams *context = (RatelNeoHookeanElasticityParams *)ctx;
  const CeedScalar                       mu      = context->mu;
  const CeedScalar                       lambda  = context->lambda;

  CeedScalar grad_du[3][3], dE_sym[6], tape[RATEL_ELASTICITY_NEO_HOOKEAN_INITIAL_AD_TAPE_SIZE], dS_sym[6], dS[3][3], S[3][3], S_sym[6];

  // Qdata
  RatelQdataUnpack(Q, i, q_data, dXdx);

  // Read spatial derivatives of increment u; d(du)/dX = d(du)/dX
  CeedScalar ddudX[3][3];
  RatelGradUnpack(Q, i, dug, ddudX);

  // Compute grad_du = ddu/dX * dX/dx
  // X is ref coordinate [-1,1]^3; x is physical coordinate in initial configuration
  RatelMatMatMult(1.0, ddudX, dXdx, grad_du);

  // Deformation Gradient : F = I + grad_u
  CeedScalar grad_u[3][3];
  RatelStoredValuesUnpack(Q, i, 0, 9, stored_values, (CeedScalar *)grad_u);
  const CeedScalar F[3][3] = {
      {grad_u[0][0] + 1, grad_u[0][1],     grad_u[0][2]    },
      {grad_u[1][0],     grad_u[1][1] + 1, grad_u[1][2]    },
      {grad_u[2][0],     grad_u[2][1],     grad_u[2][2] + 1}
  };

  // dE - Green-Lagrange strain tensor increment
  RatelGreenLagrangeStrain_fwd(grad_du, F, dE_sym);
  // Compute dS with Enzyme-AD
  RatelStoredValuesUnpack(Q, i, 15, RATEL_ELASTICITY_NEO_HOOKEAN_INITIAL_AD_TAPE_SIZE, stored_values, (CeedScalar *)tape);
  dS_fwd(dS_sym, dE_sym, lambda, mu, tape);
  RatelSymmetricMatUnpack(dS_sym, dS);

  // Second Piola-Kirchhoff (S)
  RatelStoredValuesUnpack(Q, i, 9, 6, stored_values, (CeedScalar *)S_sym);
  RatelSymmetricMatUnpack(S_sym, S);

  // df1 = dP = dP/dF:dF = dF*S + F*dS; note dF = grad_du
  RatelMatMatMultPlusMatMatMult(grad_du, S, F, dS, df1);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute residual for neo-Hookean hyperelasticity in initial configuration with Enzyme

  @param[in]   ctx  QFunction context, holding `RatelNeoHookeanElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityResidual_NeoHookeanInitial_AD_Enzyme)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return ElasticityResidual(ctx, Q, f1_NeoHookeanInitial_AD_Enzyme, !!NUM_COMPONENTS_STATE_NeoHookeanInitial_AD_Enzyme,
                            !!NUM_COMPONENTS_STORED_NeoHookeanInitial_AD_Enzyme, NUM_ACTIVE_FIELD_EVAL_MODES_NeoHookeanInitial_AD_Enzyme, in, out);
}

/**
  @brief Evaluate Jacobian for neo-Hookean hyperelasticity in initial configuration with Enzyme

  @param[in]   ctx  QFunction context, holding `RatelNeoHookeanElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityJacobian_NeoHookeanInitial_AD_Enzyme)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return ElasticityJacobian(ctx, Q, df1_NeoHookeanInitial_AD_Enzyme, !!NUM_COMPONENTS_STATE_NeoHookeanInitial_AD_Enzyme,
                            !!NUM_COMPONENTS_STORED_NeoHookeanInitial_AD_Enzyme, NUM_ACTIVE_FIELD_EVAL_MODES_NeoHookeanInitial_AD_Enzyme, in, out);
}

/// @}
