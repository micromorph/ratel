/// @file
/// Ratel elasticity in logarithmic strain space, initial configuration in pricipal directions
#include <ceed/types.h>
#ifndef CEED_RUNNING_JIT_PASS
#include <math.h>
#endif

#include "../../models/hencky.h"           // IWYU pragma: export
#include "../boundaries/platen-nitsche.h"  // IWYU pragma: export
#include "elasticity-hencky-common.h"      // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

#define NUM_COMPONENTS_STORED_ElasticityHenckyInitialPrincipal 31
#define NUM_COMPONENTS_STATE_ElasticityHenckyInitialPrincipal 0
#define NUM_ACTIVE_FIELD_EVAL_MODES_ElasticityHenckyInitialPrincipal 1
#define FLOPS_JACOBIAN_ElasticityHenckyInitialPrincipal                                                                                          \
  (3 + 3 * FLOPS_MatMatMult + FLOPS_MatInverse + FLOPS_EigenVectorOuterMult + FLOPS_Hencky_dbedF_Symmetric + FLOPS_Hencky_dedb_Eigenvalues + 3 + \
   FLOPS_EigenVectorOuterMult_fwd + FLOPS_KirchhoffStressSymmetric_fwd + 3 * FLOPS_MatMatMult + FLOPS_MatMatAdd)

/**
  @brief Helper for residual, elasticity in log space - initial configuration, principal directions

  @param[in]   ctx           QFunction context, holding `RatelHenckyElasticityParams`
  @param[in]   Q             Number of quadrature points
  @param[in]   i             Current quadrature point
  @param[in]   in            Input arrays
                             - 0 - volumetric qdata
                             - 1 - gradient of u with respect to reference coordinates
  @param[out]  out           Output arrays
  @param[out]  dXdx_initial  Coordinate transformation
  @param[out]  f1            P

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int f1_ElasticityHenckyInitialPrincipal(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in,
                                                              CeedScalar *const *out, CeedScalar dXdx_initial[3][3], CeedScalar f1[3][3]) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u_g)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*stored_values) = out[0];

  // Context elasticity
  const RatelHenckyElasticityParams *context = (RatelHenckyElasticityParams *)ctx;
  const CeedScalar                   two_mu  = context->two_mu;
  const CeedScalar                   lambda  = context->lambda;

  CeedScalar Grad_u[3][3], dudX[3][3];
  CeedScalar b_eigenvectors[3][3], b_eigenvalues[3], e_log_eigenvalues[3];
  CeedScalar tau_sym[6], tau[3][3];
  CeedScalar F_inv[3][3], tau_eigenvalues[3], b_eigenvectors_outer3x6[3][6];

  //-- Compute Grad_u = du/dX * dX/dx
  RatelQdataUnpack(Q, i, q_data, dXdx_initial);
  RatelGradUnpack(Q, i, u_g, dudX);
  RatelMatMatMult(1, dudX, dXdx_initial, Grad_u);

  // Compute F = I + Grad_u
  const CeedScalar F[3][3] = {
      {Grad_u[0][0] + 1, Grad_u[0][1],     Grad_u[0][2]    },
      {Grad_u[1][0],     Grad_u[1][1] + 1, Grad_u[1][2]    },
      {Grad_u[2][0],     Grad_u[2][1],     Grad_u[2][2] + 1}
  };
  const CeedScalar Jm1 = RatelMatDetAM1(Grad_u);

  // Compute e_log = 0.5 ln(b) eigenvalues using log1p of e eigenvalues for numerical stability
  RatelHenckyStrainEigenvalues(Grad_u, b_eigenvalues, b_eigenvectors, e_log_eigenvalues);

  // Compute trace_e_log
  const CeedScalar trace_e_log = e_log_eigenvalues[0] + e_log_eigenvalues[1] + e_log_eigenvalues[2];

  // Compute tau_eigenvalues
  RatelLinearStressEigenvalues(two_mu, lambda, trace_e_log, e_log_eigenvalues, tau_eigenvalues);

  // Coumpute f1 = P = tau F^-1
  RatelMatInverse(F, Jm1 + 1, F_inv);
  RatelEigenVectorOuterMult(b_eigenvalues, b_eigenvectors, b_eigenvectors_outer3x6);
  RatelMatFromEigensystemSymmetric(tau_eigenvalues, b_eigenvectors_outer3x6, tau_sym);
  RatelSymmetricMatUnpack(tau_sym, tau);
  RatelMatMatTransposeMult(1, tau, F_inv, f1);

  // Store values
  RatelStoredValuesPack(Q, i, 0, 6, (CeedScalar *)tau_sym, stored_values);
  RatelStoredValuesPack(Q, i, 6, 9, (CeedScalar *)Grad_u, stored_values);
  RatelStoredValuesPack(Q, i, 15, 3, (CeedScalar *)b_eigenvalues, stored_values);
  RatelStoredValuesPack(Q, i, 18, 9, (CeedScalar *)b_eigenvectors, stored_values);
  RatelStoredValuesPack(Q, i, 27, 3, (CeedScalar *)tau_eigenvalues, stored_values);
  RatelStoredValuesPack(Q, i, 30, 1, &Jm1, stored_values);

  return CEED_ERROR_SUCCESS;
}

/**
  @brief Helper for residual, elasticity in log space - initial configuration, principal directions

  @param[in]   ctx           QFunction context, holding `RatelHenckyElasticityParams`
  @param[in]   Q             Number of quadrature points
  @param[in]   i             Current quadrature point
  @param[in]   in            Input arrays
                             - 0 - volumetric qdata
                             - 1 - stored values
                             - 2 - gradient of u with respect to reference coordinates
  @param[out]  out           Output arrays, unused
  @param[out]  dXdx_initial  Coordinate transformation
  @param[out]  df1           df1

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int df1_ElasticityHenckyInitialPrincipal(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in,
                                                               CeedScalar *const *out, CeedScalar dXdx_initial[3][3], CeedScalar df1[3][3]) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA]  = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*stored_values)       = in[1];
  const CeedScalar(*du_g)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[2];

  // Context elasticity
  const RatelHenckyElasticityParams *context = (RatelHenckyElasticityParams *)ctx;
  const CeedScalar                   mu      = context->mu;
  const CeedScalar                   bulk    = context->bulk;

  CeedScalar Grad_u[3][3], grad_du[3][3], Jm1 = 0.0;
  CeedScalar dF[3][3], ddudX[3][3], dXdx[3][3];
  CeedScalar tau_sym[6], dtau_sym[6], tau[3][3], dtau[3][3], F_inv[3][3];
  CeedScalar b_eigenvalues[3], b_eigenvectors[3][3], db_sym[6], de_log_eigenvalues[3], tau_eigenvalues[3];
  CeedScalar b_eigenvectors_outer3x6[3][6], db_eigenvectors_outer3x6[3][6];
  CeedScalar tau_F_invT_dFT[3][3], F_invT_dFT[3][3], dtau_minus_tau_F_invT_dFT[3][3];

  // Unpack stored
  RatelStoredValuesUnpack(Q, i, 0, 6, stored_values, (CeedScalar *)tau_sym);
  RatelSymmetricMatUnpack(tau_sym, tau);
  RatelStoredValuesUnpack(Q, i, 6, 9, stored_values, (CeedScalar *)Grad_u);
  RatelStoredValuesUnpack(Q, i, 15, 3, stored_values, (CeedScalar *)b_eigenvalues);
  RatelStoredValuesUnpack(Q, i, 18, 9, stored_values, (CeedScalar *)b_eigenvectors);
  RatelStoredValuesUnpack(Q, i, 27, 3, stored_values, (CeedScalar *)tau_eigenvalues);
  RatelStoredValuesUnpack(Q, i, 30, 1, stored_values, &Jm1);

  //- Compute dF = Grad_du = du * dX/dx_initial
  RatelQdataUnpack(Q, i, q_data, dXdx_initial);
  RatelGradUnpack(Q, i, du_g, ddudX);
  RatelMatMatMult(1.0, ddudX, dXdx_initial, dF);

  // Retrieve F = I + Grad_u
  const CeedScalar F[3][3] = {
      {Grad_u[0][0] + 1, Grad_u[0][1],     Grad_u[0][2]    },
      {Grad_u[1][0],     Grad_u[1][1] + 1, Grad_u[1][2]    },
      {Grad_u[2][0],     Grad_u[2][1],     Grad_u[2][2] + 1}
  };

  // Compute F_inv and grad_du
  RatelMatInverse(F, Jm1 + 1, F_inv);
  RatelMatMatMult(1.0, dXdx_initial, F_inv, dXdx);
  RatelMatMatMult(1.0, ddudX, dXdx, grad_du);

  // Compute b_eigenvectors_outer3x6
  RatelEigenVectorOuterMult(b_eigenvalues, b_eigenvectors, b_eigenvectors_outer3x6);

  // Compute db/dF
  RatelHencky_dbedF_Symmetric(grad_du, b_eigenvalues, b_eigenvectors_outer3x6, db_sym);

  // Compute de/db eigenvalues
  RatelHencky_dedb_Eigenvalues(db_sym, b_eigenvalues, b_eigenvectors, de_log_eigenvalues);

  // Compute trace de_log
  const CeedScalar trace_de_log = de_log_eigenvalues[0] + de_log_eigenvalues[1] + de_log_eigenvalues[2];

  // Compute dtau
  RatelEigenVectorOuterMult_fwd(db_sym, b_eigenvalues, b_eigenvectors, db_eigenvectors_outer3x6);
  RatelHenckyKirchhoffStressSymmetric_fwd(mu, bulk, trace_de_log, de_log_eigenvalues, tau_eigenvalues, b_eigenvectors_outer3x6,
                                          db_eigenvectors_outer3x6, dtau_sym);

  //- Compute df1 = dP = dtau * F^-T - tau * (F^-T * dF^T * F^-T) = (dtau - tau * F^-T * dF^T) * F^-T
  RatelSymmetricMatUnpack(dtau_sym, dtau);
  RatelMatTransposeMatTransposeMult(1, F_inv, dF, F_invT_dFT);
  RatelMatMatMult(1., tau, F_invT_dFT, tau_F_invT_dFT);
  RatelMatMatAdd(1., dtau, -1., tau_F_invT_dFT, dtau_minus_tau_F_invT_dFT);
  RatelMatMatTransposeMult(1, dtau_minus_tau_F_invT_dFT, F_inv, df1);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute residual elasticity in log space - initial configuration

  @param[in]   ctx  QFunction context, holding `ElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityHenckyInitialPrincipal_Residual)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return ElasticityResidual(ctx, Q, f1_ElasticityHenckyInitialPrincipal, !!NUM_COMPONENTS_STATE_ElasticityHenckyInitialPrincipal,
                            !!NUM_COMPONENTS_STORED_ElasticityHenckyInitialPrincipal, NUM_ACTIVE_FIELD_EVAL_MODES_ElasticityHenckyInitialPrincipal,
                            in, out);
}

/**
  @brief Compute Jacobian elasticity in log space - initial configuration

  @param[in]   ctx  QFunction context, holding `ElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityHenckyInitialPrincipal_Jacobian)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return ElasticityJacobian(ctx, Q, df1_ElasticityHenckyInitialPrincipal, !!NUM_COMPONENTS_STATE_ElasticityHenckyInitialPrincipal,
                            !!NUM_COMPONENTS_STORED_ElasticityHenckyInitialPrincipal, NUM_ACTIVE_FIELD_EVAL_MODES_ElasticityHenckyInitialPrincipal,
                            in, out);
}

/**
  @brief Compute platen residual, elasticity in log space - initial configuration - principal directions

  @param[in]   ctx  QFunction context, holding `ElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityHenckyInitialPrincipalPlaten_Residual)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return PlatenBCs(ctx, Q, f1_ElasticityHenckyInitialPrincipal, !!NUM_COMPONENTS_STATE_ElasticityHenckyInitialPrincipal,
                   !!NUM_COMPONENTS_STORED_ElasticityHenckyInitialPrincipal, NUM_ACTIVE_FIELD_EVAL_MODES_ElasticityHenckyInitialPrincipal, in, out);
}

/**
  @brief Compute platen Jacobian elasticity in log space - initial configuration - principal directions

  @param[in]   ctx  QFunction context, holding `ElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityHenckyInitialPrincipalPlaten_Jacobian)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return PlatenBCs_Jacobian(ctx, Q, df1_ElasticityHenckyInitialPrincipal, !!NUM_COMPONENTS_STATE_ElasticityHenckyInitialPrincipal,
                            !!NUM_COMPONENTS_STORED_ElasticityHenckyInitialPrincipal, NUM_ACTIVE_FIELD_EVAL_MODES_ElasticityHenckyInitialPrincipal,
                            in, out);
}

/// @}
