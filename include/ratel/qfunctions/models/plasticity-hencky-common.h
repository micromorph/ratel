/// @file
/// Ratel plasticity in logarithmic strain space
#pragma once

#include <ceed/types.h>
#ifndef CEED_RUNNING_JIT_PASS
#include <math.h>
#endif

#include "../../models/plasticity.h"  // IWYU pragma: export
#include "../utils.h"                 // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

#define NUM_COMPONENTS_DIAGNOSTIC_PlasticityHencky 17

/**
  @brief Compute dbe/dF

  @param[in]   Cp_n_inv    inverse of the right Cauchy-Green strain tensor from t_n
  @param[in]   F           deformation gradient
  @param[in]   dF          variation deformation gradient
  @param[out]  dbe_tr_sym  variation of trial left Cauchy-Green strain tensor

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelCompute_dbedF_Symmetric(const CeedScalar Cp_n_inv[3][3], const CeedScalar F[3][3], const CeedScalar dF[3][3],
                                                       CeedScalar dbe_tr_sym[6]) {
  //
  CeedScalar Cp_n_inv_FT[3][3], Cp_n_inv_dFT[3][3], dF_Cp_n_inv_FT[3][3], F_Cp_n_inv_dFT[3][3], dbe_tr[3][3];

  //- Compute dbe = dF * C_n^{p-1} * FT + F * C_n{p^-1} * dFT
  // dF * Cp_n^-1 * FT
  RatelMatMatTransposeMult(1, Cp_n_inv, F, Cp_n_inv_FT);
  RatelMatMatMult(1, dF, Cp_n_inv_FT, dF_Cp_n_inv_FT);

  // F * Cp_n^-1 * dFT
  RatelMatMatTransposeMult(1, Cp_n_inv, dF, Cp_n_inv_dFT);
  RatelMatMatMult(1, F, Cp_n_inv_dFT, F_Cp_n_inv_dFT);

  // dbe
  RatelMatMatAdd(1, dF_Cp_n_inv_FT, 1, F_Cp_n_inv_dFT, dbe_tr);
  RatelSymmetricMatPack(dbe_tr, dbe_tr_sym);
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_dbedF_Symmetric (4 * FLOPS_MatMatMult + FLOPS_MatMatAdd)

/**
  @brief Compute dlogA/A as sum_{i=1}^3 [1/lambda_i * dlambda_i * N_i + ln(lambda_i) * dN_i]

  @param[in]   alpha         scalar
  @param[in]   A_sym         A_sym
  @param[in]   dA_sym        dA_sym
  @param[out]  dlogA_dA_sym  dlogA_dA_sym

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelCompute_dlogAdA_Symmetric(const CeedScalar alpha, const CeedScalar A_sym[6], const CeedScalar dA_sym[6],
                                                         CeedScalar dlogA_dA_sym[6]) {
  CeedScalar aux[3][6], A_eigenvalues[3], A_eigenvectors[3][3], B_eigenvalues[3], B_sym[6], A_d_eigenvalues[3];
  CeedScalar A_eigenvectors_outer3x6[3][6], dA_eigenvectors_outer3x6[3][6];
  CeedScalar A_eigenvalues_rec[3], A_eigenvalues_log[3];

  // Decomposition
  // Compute log_e_el_tr = 0.5 ln(be_tr) using log1p of e eigenvalues for numerical stability
  CeedScalar Identity[6] = {1.0, 1.0, 1.0, 0.0, 0.0, 0.0};
  RatelMatMatAddSymmetric(1.0, A_sym, -1.0, Identity, B_sym);
  RatelMatComputeEigensystemSymmetric(B_sym, B_eigenvalues, A_eigenvectors);
  for (CeedInt j = 0; j < 3; j++) {
    A_eigenvalues[j] = B_eigenvalues[j] + 1;
  }

  // 1/lambda_i and log(lambda_i)
  for (CeedInt j = 0; j < 3; j++) {
    A_eigenvalues_rec[j] = 1 / A_eigenvalues[j];
    A_eigenvalues_log[j] = RatelLog1pSeries(B_eigenvalues[j]);
  }

  // sum_{i=1}^3 [1\lambda_i * dlambda_i * N_i + ln(lambda_i) * dN_i]
  RatelEigenVectorOuterMult(A_eigenvalues, A_eigenvectors, A_eigenvectors_outer3x6);
  RatelEigenVectorOuterMult_fwd(dA_sym, A_eigenvalues, A_eigenvectors, dA_eigenvectors_outer3x6);
  RatelEigenValue_fwd(dA_sym, A_eigenvectors, A_d_eigenvalues);

  for (CeedInt i = 0; i < 3; i++) {
    RatelMatMatAddSymmetric(A_eigenvalues_rec[i] * A_d_eigenvalues[i], A_eigenvectors_outer3x6[i], A_eigenvalues_log[i], dA_eigenvectors_outer3x6[i],
                            aux[i]);
  }
  RatelMatMatMatAddSymmetric(alpha, aux[0], alpha, aux[1], alpha, aux[2], dlogA_dA_sym);
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_dlogAdA_Symmetric                                                                                         \
  (6 + 3 * FLOPS_Log1pSeries + FLOPS_EigenVectorOuterMult + FLOPS_EigenVectorOuterMult_fwd + FLOPS_EigenValue_fwd + 3 + \
   3 * FLOPS_MatMatAddSymmetric + FLOPS_MatMatMatAddSymmetric)

/**
  @brief Compute diagnostics for plasticity in log space

  @param[in]   ctx  QFunction context, holding `ElastoPlasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - volumetric qdata
                      - 1 - state
                      - 2 - u
                      - 3 - u_g gradient of u with respect to reference coordinates
  @param[out]  out  Diagnostics

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(Diagnostic_PlasticityHencky)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*state)              = in[1];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[2];
  const CeedScalar(*u_g)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[3];

  // Outputs
  CeedScalar(*diagnostic)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Context Elastoplaticity
  const RatelElastoPlasticityParams *context = (RatelElastoPlasticityParams *)ctx;
  const CeedScalar                   mu      = context->elasticity_params.mu;
  const CeedScalar                   bulk    = context->elasticity_params.bulk;
  const CeedScalar                   rho     = context->elasticity_params.common_parameters[RATEL_COMMON_PARAMETER_RHO];

  CeedScalar accumulated_plastic = 0.0;
  CeedScalar Grad_u[3][3], dudX[3][3];
  CeedScalar Cp_n_inv_sym[6], Cp_n_inv[3][3], Cp_n_inv_FT[3][3];
  CeedScalar be[3][3], be_sym[6], be_tr_eigenvalues[3], be_tr_eigenvectors[3][3];
  CeedScalar e_el_sym[6], log_e_el_sym[6], log_e_el_dev_sym[6], tau_sym[6], sigma_sym[6];
  CeedScalar e_el_eigenvalues[3], log_e_el_eigenvalues[3], n_outer3x6[3][6];
  CeedScalar dXdx_initial[3][3];

  // Quadrature point loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // dX/dx from Q_data
    const CeedScalar wdetJ = q_data[0][i];

    // State
    RatelStoredValuesUnpack(Q, i, 0, 1, state, &accumulated_plastic);
    RatelStoredValuesUnpack(Q, i, 1, 6, state, Cp_n_inv_sym);
    RatelSymmetricMatUnpack(Cp_n_inv_sym, Cp_n_inv);

    //-- Compute Grad_u = du/dX * dX/dx
    RatelQdataUnpack(Q, i, q_data, dXdx_initial);
    RatelGradUnpack(Q, i, u_g, dudX);
    RatelMatMatMult(1, dudX, dXdx_initial, Grad_u);

    // Compute F = I + Grad_u
    const CeedScalar F[3][3] = {
        {Grad_u[0][0] + 1, Grad_u[0][1],     Grad_u[0][2]    },
        {Grad_u[1][0],     Grad_u[1][1] + 1, Grad_u[1][2]    },
        {Grad_u[2][0],     Grad_u[2][1],     Grad_u[2][2] + 1}
    };
    const CeedScalar Jm1 = RatelMatDetAM1(Grad_u);

    //- Compute be = F * C^p-1_n *F^T
    RatelMatMatTransposeMult(1, Cp_n_inv, F, Cp_n_inv_FT);
    RatelMatMatMult(1, F, Cp_n_inv_FT, be);
    RatelSymmetricMatPack(be, be_sym);

    // Compute log_e_el = 0.5 ln(be)
    CeedScalar Identity[6] = {1.0, 1.0, 1.0, 0.0, 0.0, 0.0};
    RatelMatMatAddSymmetric(0.5, be_sym, -0.5, Identity, e_el_sym);
    RatelMatComputeEigensystemSymmetric(e_el_sym, e_el_eigenvalues, be_tr_eigenvectors);
    for (CeedInt j = 0; j < 3; j++) {
      be_tr_eigenvalues[j] = 2 * e_el_eigenvalues[j] + 1;
    }
    RatelEigenVectorOuterMult(be_tr_eigenvalues, be_tr_eigenvectors, n_outer3x6);
    for (CeedInt j = 0; j < 3; j++) {
      log_e_el_eigenvalues[j] = 0.5 * RatelLog1pSeries(2 * e_el_eigenvalues[j]);
    }
    RatelMatFromEigensystemSymmetric(log_e_el_eigenvalues, n_outer3x6, log_e_el_sym);

    //- Compute sigma = J * tau
    const CeedScalar trace_log_e = RatelMatTraceSymmetric(log_e_el_sym);
    RatelMatDeviatoricSymmetric(trace_log_e, log_e_el_sym, log_e_el_dev_sym);
    RatelLinearStress(2 * mu, bulk, trace_log_e, log_e_el_dev_sym, tau_sym);
    RatelScalarMatMultSymmetric(1. / (Jm1 + 1), tau_sym, sigma_sym);

    //--Store diagnostics
    // Displacement
    diagnostic[0][i] = u[0][i];
    diagnostic[1][i] = u[1][i];
    diagnostic[2][i] = u[2][i];

    // Cauchy stress tensor
    diagnostic[3][i] = sigma_sym[0];
    diagnostic[4][i] = sigma_sym[5];
    diagnostic[5][i] = sigma_sym[4];
    diagnostic[6][i] = sigma_sym[1];
    diagnostic[7][i] = sigma_sym[3];
    diagnostic[8][i] = sigma_sym[2];

    // Pressure
    diagnostic[9][i] = -bulk * trace_log_e;

    // Strain tensor invariants
    diagnostic[10][i] = trace_log_e;

    const CeedScalar trace_log_e2 = RatelMatMatContractSymmetric(1.0, log_e_el_sym, log_e_el_sym);

    diagnostic[11][i] = trace_log_e2;
    diagnostic[12][i] = Jm1 + 1;

    CeedScalar Phi_dev = 0.5 * mu * RatelMatMatContractSymmetric(1.0, log_e_el_dev_sym, log_e_el_dev_sym);

    // Hencky strain energy
    diagnostic[13][i] = 0.5 * bulk * trace_log_e * trace_log_e + Phi_dev;

    // Additional diagnostic quantities for plasticity
    // von Mises stress
    diagnostic[14][i] = sqrt(3 * Phi_dev / mu);

    // -- if mass density is requested then compute mass density: rho*J
    if (rho > 0.0) diagnostic[15][i] = rho / diagnostic[12][i];

    // Accumulated plastic strain
    diagnostic[16][i] = accumulated_plastic;

    for (CeedInt j = 0; j < NUM_COMPONENTS_DIAGNOSTIC_PlasticityHencky; j++) diagnostic[j][i] *= wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute strain energy for log strains plasticity

  @param[in]   ctx  QFunction context holding RatelElastoPlasticityParams
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - state
                      - 3 - gradient of u with respect to reference coordinates
  @param[out]  out  Output arrays
                      - 0 - strain energy

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(StrainEnergy_PlasticityHencky)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*state)              = in[1];
  const CeedScalar(*u_g)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[2];

  // Outputs
  CeedScalar(*energy) = (CeedScalar(*))out[0];

  // Context Elastoplaticity
  const RatelElastoPlasticityParams *context = (RatelElastoPlasticityParams *)ctx;
  const CeedScalar                   mu      = context->elasticity_params.mu;
  const CeedScalar                   bulk    = context->elasticity_params.bulk;

  CeedScalar Grad_u[3][3], dudX[3][3];
  CeedScalar Cp_n_inv_sym[6], Cp_n_inv[3][3], Cp_n_inv_FT[3][3];
  CeedScalar be[3][3], be_sym[6], be_tr_eigenvalues[3], be_tr_eigenvectors[3][3];
  CeedScalar e_el_sym[6], log_e_el_sym[6], log_e_el_dev_sym[6];
  CeedScalar log_e_el_eigenvalues[3], e_el_eigenvalues[3], n_outer3x6[3][6];
  CeedScalar dXdx_initial[3][3];

  // Quadrature point loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Qdata
    const CeedScalar wdetJ = q_data[0][i];

    // Get Cp^-1
    RatelStoredValuesUnpack(Q, i, 1, 6, state, Cp_n_inv_sym);
    RatelSymmetricMatUnpack(Cp_n_inv_sym, Cp_n_inv);

    //-- Compute Grad_u = du/dX * dX/dx
    RatelQdataUnpack(Q, i, q_data, dXdx_initial);
    RatelGradUnpack(Q, i, u_g, dudX);
    RatelMatMatMult(1, dudX, dXdx_initial, Grad_u);

    // Compute F = I + Grad_u
    const CeedScalar F[3][3] = {
        {Grad_u[0][0] + 1, Grad_u[0][1],     Grad_u[0][2]    },
        {Grad_u[1][0],     Grad_u[1][1] + 1, Grad_u[1][2]    },
        {Grad_u[2][0],     Grad_u[2][1],     Grad_u[2][2] + 1}
    };

    //- Compute be_tr = F * C^p-1_n *F^T
    RatelMatMatTransposeMult(1, Cp_n_inv, F, Cp_n_inv_FT);
    RatelMatMatMult(1, F, Cp_n_inv_FT, be);
    RatelSymmetricMatPack(be, be_sym);

    // Compute log_e_el = 0.5 ln(be)
    CeedScalar Identity[6] = {1.0, 1.0, 1.0, 0.0, 0.0, 0.0};
    RatelMatMatAddSymmetric(0.5, be_sym, -0.5, Identity, e_el_sym);
    RatelMatComputeEigensystemSymmetric(e_el_sym, e_el_eigenvalues, be_tr_eigenvectors);
    for (CeedInt j = 0; j < 3; j++) {
      be_tr_eigenvalues[j] = 2 * e_el_eigenvalues[j] + 1;
    }
    RatelEigenVectorOuterMult(be_tr_eigenvalues, be_tr_eigenvectors, n_outer3x6);
    for (CeedInt j = 0; j < 3; j++) {
      log_e_el_eigenvalues[j] = 0.5 * RatelLog1pSeries(2 * e_el_eigenvalues[j]);
    }
    RatelMatFromEigensystemSymmetric(log_e_el_eigenvalues, n_outer3x6, log_e_el_sym);

    //- Compute Hencky strain energy
    const CeedScalar trace_log_e = RatelMatTraceSymmetric(log_e_el_sym);
    RatelMatDeviatoricSymmetric(trace_log_e, log_e_el_sym, log_e_el_dev_sym);
    CeedScalar Phi_dev = mu * RatelMatMatContractSymmetric(1.0, log_e_el_dev_sym, log_e_el_dev_sym);
    energy[i]          = (0.5 * bulk * trace_log_e * trace_log_e + Phi_dev) * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}
/// @}
