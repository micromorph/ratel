/// @file
/// Ratel mass operator QFunction source
#include <ceed/types.h>

#include "../../models/common-parameters.h"  // IWYU pragma: export
#include "../utils.h"                        // IWYU pragma: export

/// @addtogroup RatelInternal
/// @{

/**
  @brief Apply scaled mass operator for elasticity damage u_tt residual

  @param[in]   ctx  QFunction context, holding common parameters and model parameters
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - volumetric qdata
                      - 1 - u
  @param[out]  out  Output array
                      - 0 - v

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityDamageResidual_utt)(void *ctx, const CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];

  const CeedScalar *context = (CeedScalar *)ctx;
  const CeedScalar  rho     = context[RATEL_COMMON_PARAMETER_RHO];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Scaled mass operator
    RatelScaledMassApplyAtQuadraturePoint(Q, i, 3, rho * q_data[0][i], in[1], out[0]);
    // Zero damage component
    out[0][4 * Q + i] = 0.0;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
