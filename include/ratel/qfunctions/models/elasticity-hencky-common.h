/// @file
/// Ratel elasticity in logarithmic strain space
#pragma once

#include <ceed/types.h>

#include "../../models/hencky.h"           // IWYU pragma: export
#include "../utils.h"                      // IWYU pragma: export
#include "elasticity-diagnostic-common.h"  // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

#define NUM_COMPONENTS_DIAGNOSTIC_ElasticityHencky 16

/**
  @brief Compute Eulerian log strain for Hencky hyperelasticity

  @param[in]   Grad_u             gradient of u initial configuration
  @param[in]   b_eigenvalues      left Cauchy strain eigenvalues
  @param[in]   b_eigenvectors     left Cauchy strain eigenvectors
  @param[out]  e_log_eigenvalues  eigenvalues log strain tensor

  @return Computed Eulerian log strain for Hencky hyperelasticity
**/
CEED_QFUNCTION_HELPER int RatelHenckyStrainEigenvalues(const CeedScalar Grad_u[3][3], CeedScalar b_eigenvalues[3], CeedScalar b_eigenvectors[3][3],
                                                       CeedScalar e_log_eigenvalues[3]) {
  CeedScalar e_sym[6], e_eigenvalues[3];
  RatelGreenEulerStrain(Grad_u, e_sym);
  RatelMatComputeEigensystemSymmetric(e_sym, e_eigenvalues, b_eigenvectors);

  // Compute e_log and b eigenvalues
  for (CeedInt j = 0; j < 3; j++) {
    e_log_eigenvalues[j] = 0.5 * RatelLog1pSeries(2 * e_eigenvalues[j]);
    b_eigenvalues[j]     = 2 * e_eigenvalues[j] + 1;
  }
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute dbe/dF

  @param[in]   grad_du                  gradient in du increment
  @param[in]   b_eigenvalues            left Cauchy strain tensor eigenvalues
  @param[in]   b_eigenvectors_outer3x6  outer products between left Cauchy strain tensor eigenvectors
  @param[out]  db_sym                   increment in left Cauchy strain tensor (symmetric representation)

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelHencky_dbedF_Symmetric(const CeedScalar grad_du[3][3], const CeedScalar b_eigenvalues[3],
                                                      const CeedScalar b_eigenvectors_outer3x6[3][6], CeedScalar db_sym[6]) {
  CeedScalar b[3][3], b_sym[6], de_sym[6];

  //- Compute db
  RatelMatFromEigensystemSymmetric(b_eigenvalues, b_eigenvectors_outer3x6, b_sym);
  RatelSymmetricMatUnpack(b_sym, b);
  RatelGreenEulerStrain_fwd(grad_du, b, de_sym);
  RatelScalarMatMultSymmetric(2.0, de_sym, db_sym);
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_Hencky_dbedF_Symmetric (FLOPS_MatComputeEigensystemSymmetric + FLOPS_GreenEulerStrain_fwd + FLOPS_ScalarMatMultSymmetric)

/**
  @brief Compute de/db eigenvalues

  @param[in]   db_sym              increment in left Cauchy strain tensor (symmetric representation)
  @param[in]   b_eigenvalues       left Cauchy strain tensor eigenvalues
  @param[in]   b_eigenvectors      left Cauchy strain tensor eigenvectors
  @param[out]  de_log_eigenvalues  increment in e_log eigenvalues

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelHencky_dedb_Eigenvalues(const CeedScalar db_sym[6], const CeedScalar b_eigenvalues[3],
                                                       const CeedScalar b_eigenvectors[3][3], CeedScalar de_log_eigenvalues[3]) {
  CeedScalar db_eigenvalues[3];

  // Compute db_eigenvalues
  RatelEigenValue_fwd(db_sym, b_eigenvectors, db_eigenvalues);

  // Compute de_log_eigenvalues
  for (CeedInt j = 0; j < 3; j++) {
    de_log_eigenvalues[j] = 0.5 * db_eigenvalues[j] / b_eigenvalues[j];  // b_eigenvalues never 0
  }
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_Hencky_dedb_Eigenvalues (FLOPS_EigenValue_fwd + 6)

/**
  @brief Compute dtau/de_log

  @param[in]   mu                        shear modulus
  @param[in]   bulk                      bulk modulus
  @param[in]   trace_de_log              trace of de_log
  @param[in]   de_log_eigenvalues        inverse of the right Cauchy-Green strain tensor from t_n
  @param[in]   tau_eigenvalues           Kirchhoff stress eigenvalues
  @param[in]   b_eigenvectors_outer3x6   outer products between left Cauchy strain tensor eigenvectors
  @param[in]   db_eigenvectors_outer3x6  increment of b_eigenvectors_outer3x6
  @param[out]  dtau_sym                  increment of Kirchhoff stress (symmetric representation)

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelHenckyKirchhoffStressSymmetric_fwd(const CeedScalar mu, const CeedScalar bulk, const CeedScalar trace_de_log,
                                                                  const CeedScalar de_log_eigenvalues[3], const CeedScalar tau_eigenvalues[3],
                                                                  const CeedScalar b_eigenvectors_outer3x6[3][6],
                                                                  const CeedScalar db_eigenvectors_outer3x6[3][6], CeedScalar dtau_sym[6]) {
  CeedScalar dtau_eigenvalues[3], aux[3][6];

  // Compute dtau
  for (CeedInt j = 0; j < 3; j++) {
    dtau_eigenvalues[j] = 2 * mu * (de_log_eigenvalues[j] - 1 / 3. * trace_de_log) + bulk * trace_de_log;
    RatelMatMatAddSymmetric(dtau_eigenvalues[j], b_eigenvectors_outer3x6[j], tau_eigenvalues[j], db_eigenvectors_outer3x6[j], aux[j]);
  }
  RatelMatMatMatAddSymmetric(1.0, aux[0], 1.0, aux[1], 1.0, aux[2], dtau_sym);

  return CEED_ERROR_SUCCESS;
}
#define FLOPS_KirchhoffStressSymmetric_fwd (21 + 3 * FLOPS_MatMatAddSymmetric + FLOPS_MatMatMatAddSymmetric)

/**
  @brief Compute diagnostics for elasticity in log space

  @param[in]   ctx  QFunction context, holding `ElastoElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - volumetric qdata
                      - 1 - state
                      - 2 - u
                      - 3 - u_g gradient of u with respect to reference coordinates
  @param[out]  out  Diagnostics

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(Diagnostic_ElasticityHencky)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[1];
  const CeedScalar(*u_g)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[2];

  // Outputs
  CeedScalar(*diagnostic)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Context Elasticity
  const RatelHenckyElasticityParams *context = (RatelHenckyElasticityParams *)ctx;
  const CeedScalar                   mu      = context->mu;
  const CeedScalar                   bulk    = context->bulk;
  const CeedScalar                   rho     = context->common_parameters[RATEL_COMMON_PARAMETER_RHO];

  CeedScalar Grad_u[3][3], dudX[3][3], dXdx_initial[3][3];
  CeedScalar b_eigenvectors[3][3], b_eigenvalues[3], e_log_eigenvalues[3];
  CeedScalar tau_sym[6], sigma_sym[6], sigma_dev_sym[6];
  CeedScalar tau_eigenvalues[3], n_outer3x6[3][6];

  // Quadrature point loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Unpack Q_data
    const CeedScalar wdetJ = q_data[0][i];
    RatelQdataUnpack(Q, i, q_data, dXdx_initial);

    //-- Compute Grad_u = du/dX * dX/dx
    RatelGradUnpack(Q, i, u_g, dudX);
    RatelMatMatMult(1, dudX, dXdx_initial, Grad_u);

    // Compute e_log = 0.5 ln(b) eigenvalues using log1p of e eigenvalues for numerical stability
    RatelHenckyStrainEigenvalues(Grad_u, b_eigenvalues, b_eigenvectors, e_log_eigenvalues);

    // Compute trace_e_log
    const CeedScalar trace_e_log = e_log_eigenvalues[0] + e_log_eigenvalues[1] + e_log_eigenvalues[2];

    // Compute tau_eigenvalues
    for (CeedInt j = 0; j < 3; j++) {
      tau_eigenvalues[j] = 2 * mu * (e_log_eigenvalues[j] - 1 / 3. * trace_e_log) + bulk * trace_e_log;
    }

    // Compute sigma
    const CeedScalar Jm1 = RatelMatDetAM1(Grad_u);
    RatelEigenVectorOuterMult(b_eigenvalues, b_eigenvectors, n_outer3x6);
    RatelMatFromEigensystemSymmetric(tau_eigenvalues, n_outer3x6, tau_sym);
    RatelScalarMatMultSymmetric(1. / (Jm1 + 1), tau_sym, sigma_sym);

    //--Store diagnostics
    // Displacement
    diagnostic[0][i] = u[0][i];
    diagnostic[1][i] = u[1][i];
    diagnostic[2][i] = u[2][i];

    // Cauchy stress tensor
    diagnostic[3][i] = sigma_sym[0];
    diagnostic[4][i] = sigma_sym[5];
    diagnostic[5][i] = sigma_sym[4];
    diagnostic[6][i] = sigma_sym[1];
    diagnostic[7][i] = sigma_sym[3];
    diagnostic[8][i] = sigma_sym[2];

    // Pressure
    diagnostic[9][i] = -bulk * trace_e_log;

    // Strain tensor invariants
    diagnostic[10][i] = trace_e_log;

    const CeedScalar trace_e_log2 =
        e_log_eigenvalues[0] * e_log_eigenvalues[1] + e_log_eigenvalues[1] * e_log_eigenvalues[2] + e_log_eigenvalues[2] * e_log_eigenvalues[0];

    diagnostic[11][i] = trace_e_log2;
    diagnostic[12][i] = Jm1 + 1;

    const CeedScalar Phi_dev = mu * (e_log_eigenvalues[0] * e_log_eigenvalues[0] + e_log_eigenvalues[1] * e_log_eigenvalues[1] +
                                     e_log_eigenvalues[2] * e_log_eigenvalues[2]) -
                               mu * trace_e_log * trace_e_log / 3.;

    // Hencky strain energy
    diagnostic[13][i] = 0.5 * bulk * trace_e_log * trace_e_log + Phi_dev;

    // von Mises stress
    const CeedScalar trace_sigma_degr = RatelMatTraceSymmetric(sigma_sym);
    RatelMatDeviatoricSymmetric(trace_sigma_degr, sigma_sym, sigma_dev_sym);
    const CeedScalar sigma_dev_contract = RatelMatMatContractSymmetric(1.0, sigma_dev_sym, sigma_dev_sym);

    diagnostic[14][i] = sqrt(3 * sigma_dev_contract / 2);

    // Compute mass density values
    // -- Set to 0 as default
    diagnostic[15][i] = 0.0;

    // -- if mass density is requested then compute mass density: rho/J
    if (rho > 0.0) diagnostic[15][i] = rho / diagnostic[12][i];

    for (CeedInt j = 0; j < NUM_COMPONENTS_DIAGNOSTIC_ElasticityHencky; j++) diagnostic[j][i] *= wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute strain energy for log strains elasticity

  @param[in]   ctx  QFunction context holding RatelElastoElasticityParams
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - state
                      - 3 - gradient of u with respect to reference coordinates
  @param[out]  out  Output arrays
                      - 0 - strain energy

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(StrainEnergy_ElasticityHencky)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u_g)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*energy) = (CeedScalar(*))out[0];

  // Context Elastoplaticity
  const RatelHenckyElasticityParams *context = (RatelHenckyElasticityParams *)ctx;
  const CeedScalar                   mu      = context->mu;
  const CeedScalar                   bulk    = context->bulk;

  CeedScalar Grad_u[3][3], dudX[3][3];
  CeedScalar b_eigenvectors[3][3];
  CeedScalar e_log_eigenvalues[3], b_eigenvalues[3];
  CeedScalar dXdx_initial[3][3];

  // Quadrature point loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Qdata
    const CeedScalar wdetJ = q_data[0][i];

    //-- Compute Grad_u = du/dX * dX/dx
    RatelQdataUnpack(Q, i, q_data, dXdx_initial);
    RatelGradUnpack(Q, i, u_g, dudX);
    RatelMatMatMult(1, dudX, dXdx_initial, Grad_u);

    // Compute e_log = 0.5 ln(b) eigenvalues using log1p of e eigenvalues for numerical stability
    RatelHenckyStrainEigenvalues(Grad_u, b_eigenvalues, b_eigenvectors, e_log_eigenvalues);

    // Compute trace_e_log
    const CeedScalar trace_e_log = e_log_eigenvalues[0] + e_log_eigenvalues[1] + e_log_eigenvalues[2];

    //- Compute Hencky strain energy
    const CeedScalar Phi_dev = mu * (e_log_eigenvalues[0] * e_log_eigenvalues[0] + e_log_eigenvalues[1] * e_log_eigenvalues[1] +
                                     e_log_eigenvalues[2] * e_log_eigenvalues[2]) -
                               mu * trace_e_log * trace_e_log / 3.;

    energy[i] = (0.5 * bulk * trace_e_log * trace_e_log + Phi_dev) * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}
/// @}
