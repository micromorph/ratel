/// @file
/// Ratel mixed linear elasticity QFunction source
#include <ceed/types.h>

#include "../../models/elasticity-mixed-linear.h"  // IWYU pragma: export
#include "../boundaries/platen-nitsche.h"          // IWYU pragma: export
#include "../utils.h"                              // IWYU pragma: export
#include "elasticity-mixed-common.h"               // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

#define NUM_COMPONENTS_STATE_MixedLinear 0
#define NUM_COMPONENTS_STORED_MixedLinear 0
#define NUM_ACTIVE_FIELD_EVAL_MODES_MixedLinear_u 1
#define NUM_ACTIVE_FIELD_EVAL_MODES_MixedLinear_p 1
#define NUM_ACTIVE_FIELD_EVAL_MODES_MixedLinear (NUM_ACTIVE_FIELD_EVAL_MODES_MixedLinear_u + NUM_ACTIVE_FIELD_EVAL_MODES_MixedLinear_p)

#define NUM_COMPONENTS_DIAGNOSTIC_MixedLinear 16

// FLOPS_grad_du = FLOPS_MatMatMult
#define FLOPS_df1_MixedLinear \
  (FLOPS_MatMatMult + FLOPS_LinearStrain_fwd + FLOPS_MatTrace + FLOPS_MatDeviatoricSymmetric + FLOPS_MixedLinearStress_fwd)
#define FLOPS_JACOBIAN_MixedLinear (FLOPS_df1_MixedLinear + FLOPS_dXdxwdetJ + 3)
#define FLOPS_JACOBIAN_Block_uu_MixedLinear \
  (FLOPS_MatMatMult + FLOPS_LinearStrain_fwd + FLOPS_MatTrace + FLOPS_MatDeviatoricSymmetric + FLOPS_dXdxwdetJ + 1)
#define FLOPS_JACOBIAN_Block_pp_MixedLinear (3)

// -----------------------------------------------------------------------------
// Strong form:
//  div(sigma)  +  f                = 0   in \Omega
// -div(u)      -  p/(bulk - bulk_primal) = 0   in \Omega
//
//  where sigma_ij = (bulk_primal * tr(e) - p) * I + 2 * mu * e_dev; e_dev = e - (1 / 3) tr(e) * I
//  and e = 0.5*(grad(u) + (grad(u))^T )
//
// Weak form: Find (u,p) \in VxQ (V=H1, Q=L^2) on \Omega
//  (grad(v), sigma)                   = (v, f)
// -(q, div(u)) - (q,p/(bulk - bulk_primal)) = 0
// -----------------------------------------------------------------------------

/**
  @brief Compute `Sigma` for linear mixed elasticity

  @param[in]   ctx      QFunction context, holding `RatelMixedLinearElasticityParams`
  @param[in]   Q        Number of quadrature points
  @param[in]   i        Current quadrature point
  @param[in]   in       Input arrays
                          - 0 - volumetric qdata
                          - 1 - gradient of u with respect to reference coordinates
                          - 2 - p
  @param[out]  out      Output arrays, unused
  @param[out]  dXdx     Coordinate transformation
  @param[out]  f1       `f1 = Sigma = (bulk_primal * tr(e) - p) * I + 2 * mu * e_dev`
  @param[out]  trace_e  Divergence of u = trace_e needed for computing g0 function

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int f1_MixedLinear(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in, CeedScalar *const *out,
                                         CeedScalar dXdx[3][3], CeedScalar f1[3][3], CeedScalar *trace_e) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];
  const CeedScalar(*p)                  = in[2];

  // Context
  const RatelMixedLinearElasticityParams *context     = (RatelMixedLinearElasticityParams *)ctx;
  const CeedScalar                        two_mu      = context->two_mu;
  const CeedScalar                        bulk_primal = context->bulk_primal;

  CeedScalar grad_u[3][3], e_sym[6], e_dev_sym[6], sigma_sym[6];

  // Qdata
  RatelQdataUnpack(Q, i, q_data, dXdx);

  // Read spatial derivatives of u; du/dX
  CeedScalar dudX[3][3];
  RatelGradUnpack(Q, i, ug, dudX);

  // Compute grad_u = du/dX * dX/dx
  // X is ref coordinate [-1,1]^3; x is physical coordinate
  RatelMatMatMult(1.0, dudX, dXdx, grad_u);

  // Compute strain : e (epsilon)
  // e = 1/2 (grad u + (grad u)^T)
  RatelLinearStrain(grad_u, e_sym);

  // Compute Deviatoric Strain
  // e_dev = e - 1/3 * trace(e) * I
  *trace_e = RatelMatTraceSymmetric(e_sym);
  RatelMatDeviatoricSymmetric(*trace_e, e_sym, e_dev_sym);

  // Compute sigma = (bulk_primal * tr(e) - p) * I + 2 * mu * e_dev
  RatelMixedLinearStress(two_mu, bulk_primal, *trace_e, p[i], e_dev_sym, sigma_sym);

  RatelSymmetricMatUnpack(sigma_sym, f1);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute `g0` for mixed linear elasticity

  @param[in]   ctx      QFunction context, holding `RatelMixedLinearElasticityParams`
  @param[in]   Q        Number of quadrature points
  @param[in]   i        Current quadrature point
  @param[in]   in       Input arrays
                          - 2 - p
  @param[in]   trace_e  Divergence of u = trace_e computed in f1 function
  @param[out]  out      Output arrays, unused
  @param[out]  g0       `g0 = -div(u) - p/(bulk - bulk_primal)`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int g0_MixedLinear(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in, const CeedScalar trace_e,
                                         CeedScalar *const *out, CeedScalar *g0) {
  // Inputs
  const CeedScalar(*p) = in[2];

  // Context
  const RatelMixedLinearElasticityParams *context     = (RatelMixedLinearElasticityParams *)ctx;
  const CeedScalar                        bulk        = context->bulk;
  const CeedScalar                        bulk_primal = context->bulk_primal;
  const CeedScalar                        inv_bulk    = (bulk == 0.) ? 0. : 1. / (bulk - bulk_primal);

  // -(q, div(u)) - (q, p/(bulk - bulk_primal)); trace(e) = div(u)
  *g0 = -trace_e - p[i] * inv_bulk;
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute linearization of `Sigma` for mixed linear elasticity

  @param[in]   ctx               QFunction context, holding `RatelMixedLinearElasticityParams`
  @param[in]   Q                 Number of quadrature points
  @param[in]   i                 Current quadrature point
  @param[in]   in                Input arrays
                                   - 0 - volumetric qdata
                                   - 1 - gradient of incremental change to u with respect to reference coordinates
                                   - 2 - dp
  @param[out]  out               Output arrays, unused
  @param[out]  dXdx              Coordinate transformation
  @param[out]  df1               `df1 = dSigma = -dp*I + 2*mu*de_dev`
  @param[out]  trace_de          Linearization of div(u): div_du = trace_de
  @param[out]  Cinv_contract_dE  `C_inv:dE`, unused for mixed linear elasticity

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int df1_MixedLinear(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in, CeedScalar *const *out,
                                          CeedScalar dXdx[3][3], CeedScalar df1[3][3], CeedScalar *trace_de, CeedScalar *Cinv_contract_dE) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*dug)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];
  const CeedScalar(*dp)                 = in[2];

  // Context
  const RatelMixedLinearElasticityParams *context     = (RatelMixedLinearElasticityParams *)ctx;
  const CeedScalar                        two_mu      = context->two_mu;
  const CeedScalar                        bulk_primal = context->bulk_primal;

  CeedScalar grad_du[3][3], de_sym[6], de_dev_sym[6], dsigma_sym[6];

  // Qdata
  RatelQdataUnpack(Q, i, q_data, dXdx);

  // Read spatial derivatives of increment u; d(du)/dX = d(du)/dX
  CeedScalar ddudX[3][3];
  RatelGradUnpack(Q, i, dug, ddudX);

  // Compute grad_du = ddu/dX * dX/dx
  // X is ref coordinate [-1,1]^3; x is physical coordinate
  RatelMatMatMult(1.0, ddudX, dXdx, grad_du);

  // Compute strain : de (epsilon)
  // de = 1/2 (grad_du + (grad_du)^T)
  RatelLinearStrain_fwd(grad_du, de_sym);

  // Compute Deviatoric Strain
  // de_dev = de - 1/3 * trace(de) * I
  *trace_de = RatelMatTraceSymmetric(de_sym);
  RatelMatDeviatoricSymmetric(*trace_de, de_sym, de_dev_sym);

  // Compute dsigma = (bulk_primal * tr(de) - dp) * I + 2 * mu * de_dev
  RatelMixedLinearStress_fwd(two_mu, bulk_primal, *trace_de, dp[i], de_dev_sym, dsigma_sym);

  RatelSymmetricMatUnpack(dsigma_sym, df1);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute linearization of `g0` for mixed linear elasticity

  @param[in]   ctx               QFunction context, holding `RatelMixedLinearElasticityParams`
  @param[in]   Q                 Number of quadrature points
  @param[in]   i                 Current quadrature point
  @param[in]   in                Input arrays
                                   - 2 - dp
  @param[in]   trace_de          Linearization of divergence of u: div(du) = trace_de computed in df1 function
  @param[in]   Cinv_contract_dE  `C_inv:dE` unused for mixed linear elasticity
  @param[out]  out               Output arrays, unused
  @param[out]  dg0               `dg0 = -div(du) - dp/(bulk - bulk_primal)`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int dg0_MixedLinear(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in, const CeedScalar trace_de,
                                          const CeedScalar Cinv_contract_dE, CeedScalar *const *out, CeedScalar *dg0) {
  // Inputs
  const CeedScalar(*dp) = in[2];

  // Context
  const RatelMixedLinearElasticityParams *context     = (RatelMixedLinearElasticityParams *)ctx;
  const CeedScalar                        bulk        = context->bulk;
  const CeedScalar                        bulk_primal = context->bulk_primal;
  const CeedScalar                        inv_bulk    = (bulk == 0.) ? 0. : 1. / (bulk - bulk_primal);

  // -(q, div(du)) - (q, dp/(bulk - bulk_primal))
  *dg0 = -trace_de - dp[i] * inv_bulk;
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute residual for mixed linear elasticity

  @param[in]   ctx  QFunction context, holding `RatelMixedLinearElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityResidual_MixedLinear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return MixedElasticityResidual(ctx, Q, f1_MixedLinear, g0_MixedLinear, !!NUM_COMPONENTS_STATE_MixedLinear, !!NUM_COMPONENTS_STORED_MixedLinear,
                                 NUM_ACTIVE_FIELD_EVAL_MODES_MixedLinear, in, out);
}

/**
  @brief Evaluate Jacobian for linear mixed elasticity

  @param[in]   ctx  QFunction context, holding `RatelMixedLinearElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityJacobian_MixedLinear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return MixedElasticityJacobian(ctx, Q, df1_MixedLinear, dg0_MixedLinear, !!NUM_COMPONENTS_STATE_MixedLinear, !!NUM_COMPONENTS_STORED_MixedLinear,
                                 NUM_ACTIVE_FIELD_EVAL_MODES_MixedLinear, in, out);
}

/**
  @brief Wrapper to compute `Sigma` for linear mixed elasticity platen BCs

  @param[in]   ctx   QFunction context, holding `RatelMixedLinearElasticityParams`
  @param[in]   Q     Number of quadrature points
  @param[in]   i     Current quadrature point
  @param[in]   in    Input arrays
                       - 0 - volumetric qdata
                       - 1 - gradient of u with respect to reference coordinates
                       - 2 - p
  @param[out]  out   Output arrays, unused
  @param[out]  dXdx  Coordinate transformation
  @param[out]  f1    `f1 = Sigma = -p*I + 2*mu*e_dev`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int f1_MixedLinear_Platen(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in, CeedScalar *const *out,
                                                CeedScalar dXdx[3][3], CeedScalar f1[3][3]) {
  CeedScalar trace_e;

  return f1_MixedLinear(ctx, Q, i, in, out, dXdx, f1, &trace_e);
}

/**
  @brief Wrapper to compute linearization of `Sigma` for mixed linear elasticity

  @param[in]   ctx   QFunction context, holding `RatelMixedLinearElasticityParams`
  @param[in]   Q     Number of quadrature points
  @param[in]   i     Current quadrature point
  @param[in]   in    Input arrays
                       - 0 - volumetric qdata
                       - 1 - gradient of incremental change to u with respect to reference coordinates
                       - 2 - dp
  @param[out]  out   Output arrays, unused
  @param[out]  dXdx  Coordinate transformation
  @param[out]  df1   `df1 = dSigma = -dp*I + 2*mu*de_dev`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int df1_MixedLinear_Platen(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in, CeedScalar *const *out,
                                                 CeedScalar dXdx[3][3], CeedScalar df1[3][3]) {
  CeedScalar trace_e, Cinv_contract_dE;

  return df1_MixedLinear(ctx, Q, i, in, out, dXdx, df1, &trace_e, &Cinv_contract_dE);
}

/**
  @brief Compute platen residual for linear mixed elasticity

  @param[in]   ctx  QFunction context, holding `RatelBCPlatenParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(PlatenBCsResidual_MixedLinear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return PlatenBCs(ctx, Q, f1_MixedLinear_Platen, !!NUM_COMPONENTS_STATE_MixedLinear, !!NUM_COMPONENTS_STORED_MixedLinear,
                   NUM_ACTIVE_FIELD_EVAL_MODES_MixedLinear, in, out);
}

/**
  @brief Evaluate platen Jacobian for linear mixed elasticity

  @param[in]   ctx  QFunction context, holding `RatelBCPlatenParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(PlatenBCsJacobian_MixedLinear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return PlatenBCs_Jacobian(ctx, Q, df1_MixedLinear_Platen, !!NUM_COMPONENTS_STATE_MixedLinear, !!NUM_COMPONENTS_STORED_MixedLinear,
                            NUM_ACTIVE_FIELD_EVAL_MODES_MixedLinear, in, out);
}

/**
  @brief Compute strain energy for mixed linear elasticity

  @param[in]   ctx  QFunction context, holding `RatelMixedLinearElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - gradient of u with respect to reference coordinates
                      - 2 - p
  @param[out]  out  Output arrays
                      - 0 - strain energy

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(StrainEnergy_MixedLinear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];
  const CeedScalar(*p)                  = in[2];

  // Outputs
  CeedScalar(*energy) = (CeedScalar(*))out[0];

  // Context
  const RatelMixedLinearElasticityParams *context     = (RatelMixedLinearElasticityParams *)ctx;
  const CeedScalar                        mu          = context->mu;
  const CeedScalar                        bulk        = context->bulk;
  const CeedScalar                        bulk_primal = context->bulk_primal;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar dXdx[3][3], grad_u[3][3], e_sym[6], e_dev_sym[6];

    // Qdata
    const CeedScalar wdetJ = q_data[0][i];

    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Read spatial derivatives of u; du/dX
    CeedScalar dudX[3][3];
    RatelGradUnpack(Q, i, ug, dudX);

    // Compute grad_u = du/dX * dX/dx
    // X is ref coordinate [-1,1]^3; x is physical coordinate
    RatelMatMatMult(1.0, dudX, dXdx, grad_u);

    // Compute Strain : e (epsilon)
    // e = 1/2 (grad u + (grad u)^T)
    RatelLinearStrain(grad_u, e_sym);

    // Compute Deviatoric Strain
    // e_dev = e - 1/3 * trace(e) * I
    const CeedScalar trace_e = RatelMatTraceSymmetric(e_sym);

    RatelMatDeviatoricSymmetric(trace_e, e_sym, e_dev_sym);

    // Strain energy = -p trace(e) + mu e_d:e_d  + 0.5 bulk_primal trace(e)^2 - 0.5 p^2 / (bulk - bulk_primal)
    // -- e_dev:e_dev = trace(e_dev^2)
    const CeedScalar trace_e_dev2 = RatelMatMatContractSymmetric(1.0, e_dev_sym, e_dev_sym);

    energy[i] = (-p[i] * trace_e + mu * trace_e_dev2 + 0.5 * bulk_primal * trace_e * trace_e - 0.5 * p[i] * p[i] / (bulk - bulk_primal)) * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute projected diagnostic values for mixed linear elasticity

  @param[in]   ctx  QFunction context, holding `RatelMixedLinearElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - u
                      - 2 - gradient of u with respect to reference coordinates
                      - 3 - p
  @param[out]  out  Output arrays
                      - 0 - projected diagnostic values

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(Diagnostic_MixedLinear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[1];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[2];
  const CeedScalar(*p)                  = in[3];

  // Outputs
  CeedScalar(*diagnostic)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Context
  const RatelMixedLinearElasticityParams *context     = (RatelMixedLinearElasticityParams *)ctx;
  const CeedScalar                        mu          = context->mu;
  const CeedScalar                        two_mu      = context->two_mu;
  const CeedScalar                        bulk        = context->bulk;
  const CeedScalar                        bulk_primal = context->bulk_primal;
  const CeedScalar                        rho         = context->common_parameters[RATEL_COMMON_PARAMETER_RHO];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar dXdx[3][3], grad_u[3][3], e_sym[6], e_dev_sym[6], sigma_sym[6], sigma_dev_sym[6];

    // Qdata
    const CeedScalar wdetJ = q_data[0][i];
    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Read spatial derivatives of u; du/dX
    CeedScalar dudX[3][3];
    RatelGradUnpack(Q, i, ug, dudX);

    // Compute grad_u = du/dX * dX/dx
    // X is ref coordinate [-1,1]^3; x is physical coordinate
    RatelMatMatMult(1.0, dudX, dXdx, grad_u);

    // Compute strain : e (epsilon)
    // e = 1/2 (grad u + (grad u)^T)
    RatelLinearStrain(grad_u, e_sym);

    // Compute Deviatoric Strain
    // e_dev = e - 1/3 * trace(e) * I
    const CeedScalar trace_e = RatelMatTraceSymmetric(e_sym);

    RatelMatDeviatoricSymmetric(trace_e, e_sym, e_dev_sym);

    // Compute sigma = (bulk_primal * tr(e) - p) * I + 2 * mu * e_dev
    RatelMixedLinearStress(two_mu, bulk_primal, trace_e, p[i], e_dev_sym, sigma_sym);

    // Displacement
    diagnostic[0][i] = u[0][i];
    diagnostic[1][i] = u[1][i];
    diagnostic[2][i] = u[2][i];

    // Cauchy stress tensor
    diagnostic[3][i] = sigma_sym[0];
    diagnostic[4][i] = sigma_sym[5];
    diagnostic[5][i] = sigma_sym[4];
    diagnostic[6][i] = sigma_sym[1];
    diagnostic[7][i] = sigma_sym[3];
    diagnostic[8][i] = sigma_sym[2];

    // Hydrostatic pressure; p_h = -trace(sigma) / 3
    diagnostic[9][i] = -RatelMatTraceSymmetric(sigma_sym) / 3.;

    // Strain tensor invariants; trace(e)
    diagnostic[10][i] = trace_e;

    // trace(e^2) = e:e
    const CeedScalar trace_e2 = RatelMatMatContractSymmetric(1.0, e_sym, e_sym);
    diagnostic[11][i]         = trace_e2;
    diagnostic[12][i]         = 1 + trace_e;

    // Strain energy
    // -- e_dev:e_dev = trace(e_dev^2)
    const CeedScalar trace_e_dev2 = RatelMatMatContractSymmetric(1.0, e_dev_sym, e_dev_sym);

    diagnostic[13][i] = -p[i] * trace_e + mu * trace_e_dev2 + 0.5 * bulk_primal * trace_e * trace_e - 0.5 * p[i] * p[i] / (bulk - bulk_primal);

    // Compute von-Mises stress

    // -- Compute the the deviatoric part of Cauchy stress: sigma_dev = sigma - trace(sigma)/3
    const CeedScalar trace_sigma = RatelMatTraceSymmetric(sigma_sym);

    RatelMatDeviatoricSymmetric(trace_sigma, sigma_sym, sigma_dev_sym);

    // -- sigma_dev:sigma_dev
    const CeedScalar sigma_dev_contract = RatelMatMatContractSymmetric(1.0, sigma_dev_sym, sigma_dev_sym);

    // -- Compute von-Mises stress: sigma_von = sqrt(3/2 sigma_dev:sigma_dev)
    diagnostic[14][i] = sqrt(3. * sigma_dev_contract / 2.);

    // Compute mass density values

    // -- Set to 0 as default (rho should be 0.0 as defualt anyways)
    diagnostic[15][i] = 0.0;

    // -- if mass density is requested then compute mass density: rho/J
    if (rho > 0.0) diagnostic[15][i] = rho / diagnostic[12][i];

    // Quadrature weight
    for (CeedInt j = 0; j < NUM_COMPONENTS_DIAGNOSTIC_MixedLinear; j++) diagnostic[j][i] *= wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute displacement block for pMG preconditioner for mixed linear elasticity

  @param[in]   ctx  QFunction context, holding `RatelNeoHookeanElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - gradient of incremental change of u with respect to reference coordinates
  @param[out]  out  Output arrays
                      - 0 - action of QFunction for displacement field

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityPC_uu_MixedLinear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*dug)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*ddvdX)[3][CEED_Q_VLA] = (CeedScalar(*)[3][CEED_Q_VLA])out[0];

  // Context
  const RatelMixedLinearElasticityParams *context        = (RatelMixedLinearElasticityParams *)ctx;
  const CeedScalar                        two_mu         = context->two_mu;
  const CeedScalar                        bulk_primal_pc = context->bulk_primal_pc;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Read spatial derivatives of increment u; d(du)/dX = d(du)/dX
    CeedScalar ddudX[3][3];
    RatelGradUnpack(Q, i, dug, ddudX);

    // -- Qdata
    const CeedScalar wdetJ = q_data[0][i];
    CeedScalar       dXdx[3][3];
    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Compute grad_du = ddu/dX * dX/dx
    // X is ref coordinate [-1,1]^3; x is physical coordinate
    CeedScalar grad_du[3][3];
    RatelMatMatMult(1.0, ddudX, dXdx, grad_du);

    // Compute strain : de (epsilon)
    // de = 1/2 (grad_du + (grad_du)^T)
    CeedScalar de_sym[6];
    RatelLinearStrain_fwd(grad_du, de_sym);

    // Compute Deviatoric Strain
    // de_dev = de - 1/3 * trace(de) * I
    const CeedScalar trace_de = RatelMatTraceSymmetric(de_sym);
    CeedScalar       de_dev_sym[6], dsigma_pc_sym[6], dsigma_pc[3][3];
    RatelMatDeviatoricSymmetric(trace_de, de_sym, de_dev_sym);

    // Compute dsigma_pc = bulk_primal_pc * tr(de) * I + 2 * mu * de_dev
    RatelMixedLinearStress_fwd(two_mu, bulk_primal_pc, trace_de, 0.0, de_dev_sym, dsigma_pc_sym);

    RatelSymmetricMatUnpack(dsigma_pc_sym, dsigma_pc);

    // (grad(v), dsigma_pc)
    RatelMatMatTransposeMultAtQuadraturePoint(Q, i, wdetJ, dXdx, dsigma_pc, ddvdX);
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute pressure block for pMG preconditioner for mixed linear elasticity

  @param[in]   ctx  QFunction context, holding `RatelNeoHookeanElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - incremental change of p
  @param[out]  out  Output arrays
                      - 0 - action of QFunction for pressure field

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityPC_pp_MixedLinear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*dp)                 = in[1];

  // Outputs
  CeedScalar(*dq) = out[0];

  // Context
  const RatelMixedLinearElasticityParams *context        = (RatelMixedLinearElasticityParams *)ctx;
  const CeedScalar                        mu             = context->mu;
  const CeedScalar                        bulk           = context->bulk;
  const CeedScalar                        bulk_primal_pc = context->bulk_primal_pc;
  const CeedInt                           sign_pp        = context->sign_pp;
  const CeedScalar                        inv_bulk_pc    = (bulk == 0.) ? 0. : 1. / (bulk - bulk_primal_pc);

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // -- Qdata
    const CeedScalar wdetJ = q_data[0][i];
    // (q, dp)
    dq[i]                  = sign_pp * (inv_bulk_pc + 1. / mu) * dp[i] * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
