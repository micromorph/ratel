/// @file
/// Ratel plasticity in logarithmic strain space, initial configuration
#include <ceed/types.h>
#ifndef CEED_RUNNING_JIT_PASS
#include <math.h>
#endif

#include "../../models/elasticity-linear.h"  // IWYU pragma: export
#include "../../models/plasticity.h"         // IWYU pragma: export
#include "../boundaries/platen-nitsche.h"    // IWYU pragma: export
#include "elasticity-common.h"               // IWYU pragma: export
#include "plasticity-common.h"               // IWYU pragma: export
#include "plasticity-hencky-common.h"        // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

#define NUM_COMPONENTS_STORED_PlasticityHenckyInitial 31
#define NUM_COMPONENTS_STATE_PlasticityHenckyInitial 7
#define NUM_ACTIVE_FIELD_EVAL_MODES_PlasticityHenckyInitial 1
#define FLOPS_JACOBIAN_PlasticityHenckyInitial                                                                                       \
  (7 * FLOPS_MatMatMult + FLOPS_dbedF_Symmetric + FLOPS_dlogAdA_Symmetric + FLOPS_MatTrace + FLOPS_MatDeviatoricSymmetric + 12 + 2 + \
   2 * FLOPS_MatMatContractSymmetric + 5 + FLOPS_ReturnMapping_vonMisesLinear_fwd + FLOPS_LinearStress_fwd + FLOPS_MatInverse + FLOPS_MatMatAdd)

/**
  @brief Helper for residual, plasticity in log space - initial configuration

  @param[in]   ctx           QFunction context, holding `RatelElastoPlasticityParams`
  @param[in]   Q             Number of quadrature points
  @param[in]   i             Current quadrature point
  @param[in]   in            Input arrays
                               - 0 - volumetric qdata
                               - 1 - previously accepted plastic state components
                               - 2 - gradient of u with respect to reference coordinates
  @param[out]  out           Output arrays
                               - 0 - currently computed plastic state components
  @param[out]  dXdx_initial  Coordinate transformation
  @param[out]  f1            P

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int f1_PlasticityHenckyInitial(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in, CeedScalar *const *out,
                                                     CeedScalar dXdx_initial[3][3], CeedScalar f1[3][3]) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*state)              = in[1];
  const CeedScalar(*u_g)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[2];

  // Outputs
  CeedScalar(*current_state) = out[0];
  CeedScalar(*stored)        = out[1];

  // Context Elastoplaticity
  const RatelElastoPlasticityParams *context                 = (RatelElastoPlasticityParams *)ctx;
  const CeedScalar                   mu                      = context->elasticity_params.mu;
  const CeedScalar                   bulk                    = context->elasticity_params.bulk;
  const CeedScalar                   sigma_0                 = context->plasticity_params.yield_stress;
  const CeedScalar                   linear_hardening        = context->plasticity_params.linear_hardening;
  const CeedScalar                   saturation_stress       = context->plasticity_params.saturation_stress;
  const CeedScalar                   hardening_decay         = context->plasticity_params.hardening_decay;
  const CeedScalar                   hardening_parameters[3] = {linear_hardening, saturation_stress, hardening_decay};

  CeedScalar yield_flag = 0.0, accumulated_plastic = 0.0, delta_gamma = 0.0, hardening_slope = 0.0, sigma_y = 0.0, phi_tol = 1e-5;
  CeedScalar Grad_u[3][3], dudX[3][3];
  CeedScalar F_values[9], Cp_n_inv_sym[6], Cp_n_inv[3][3], Cp_n_inv_FT[3][3];
  CeedScalar be_tr[3][3], be_tr_sym[6], be_tr_eigenvalues[3], be_tr_eigenvectors[3][3];
  CeedScalar s_tr_sym[6], e_el_tr_sym[6], log_e_el_tr_sym[6], log_e_el_dev_tr_sym[6], log_e_el_dev_sym[6], tau_sym[6];
  CeedScalar F_inv[3][3], e_el_tr_eigenvalues[3], log_e_el_tr_eigenvalues[3], be_tr_F_invT[3][3];
  CeedScalar N_ep_eigenvalues[3], n_outer3x6[3][6], be_tr_eigenvectors_values[9];
  CeedScalar tau[3][3];

  // Unpack state fields
  RatelStoredValuesUnpack(Q, i, 0, 1, state, &accumulated_plastic);
  RatelStoredValuesUnpack(Q, i, 1, 6, state, Cp_n_inv_sym);
  if (Cp_n_inv_sym[0] == 0) {  // at time t0, Fn is zero matrix (more elegant solution is desirable)
    Cp_n_inv_sym[0] = 1;
    Cp_n_inv_sym[1] = 1;
    Cp_n_inv_sym[2] = 1;
  }
  RatelSymmetricMatUnpack(Cp_n_inv_sym, Cp_n_inv);

  //-- Compute Grad_u = du/dX * dX/dx
  RatelQdataUnpack(Q, i, q_data, dXdx_initial);
  RatelGradUnpack(Q, i, u_g, dudX);
  RatelMatMatMult(1, dudX, dXdx_initial, Grad_u);

  // Compute F = I + Grad_u
  const CeedScalar F[3][3] = {
      {Grad_u[0][0] + 1, Grad_u[0][1],     Grad_u[0][2]    },
      {Grad_u[1][0],     Grad_u[1][1] + 1, Grad_u[1][2]    },
      {Grad_u[2][0],     Grad_u[2][1],     Grad_u[2][2] + 1}
  };
  const CeedScalar Jm1 = RatelMatDetAM1(Grad_u);

  //- Compute be_tr = F * C^p-1_n *F^T
  RatelMatMatTransposeMult(1, Cp_n_inv, F, Cp_n_inv_FT);
  RatelMatMatMult(1, F, Cp_n_inv_FT, be_tr);
  RatelSymmetricMatPack(be_tr, be_tr_sym);

  // Compute log_e_el_tr = 0.5 ln(be_tr) using log1p of e eigenvalues for numerical stability
  CeedScalar Identity[6] = {1.0, 1.0, 1.0, 0.0, 0.0, 0.0};
  RatelMatMatAddSymmetric(0.5, be_tr_sym, -0.5, Identity, e_el_tr_sym);
  RatelMatComputeEigensystemSymmetric(e_el_tr_sym, e_el_tr_eigenvalues, be_tr_eigenvectors);
  for (CeedInt j = 0; j < 3; j++) {
    be_tr_eigenvalues[j] = 2 * e_el_tr_eigenvalues[j] + 1;
  }
  RatelEigenVectorOuterMult(be_tr_eigenvalues, be_tr_eigenvectors, n_outer3x6);
  for (CeedInt j = 0; j < 3; j++) {
    log_e_el_tr_eigenvalues[j] = 0.5 * RatelLog1pSeries(2 * e_el_tr_eigenvalues[j]);
  }
  RatelMatFromEigensystemSymmetric(log_e_el_tr_eigenvalues, n_outer3x6, log_e_el_tr_sym);

  //- Compute s_trial = 2*mu * e_el_dev_tr
  const CeedScalar trace_log_e = RatelMatTraceSymmetric(log_e_el_tr_sym);
  RatelMatDeviatoricSymmetric(trace_log_e, log_e_el_tr_sym, log_e_el_dev_tr_sym);
  for (CeedInt j = 0; j < 6; j++) {
    s_tr_sym[j]         = 2 * mu * log_e_el_dev_tr_sym[j];
    log_e_el_dev_sym[j] = log_e_el_dev_tr_sym[j];
  }

  // Compute phi_trial = q_trial - sigma_y
  RatelComputeFlowStress(sigma_0, hardening_parameters, accumulated_plastic, &sigma_y);
  const CeedScalar s_tr_contract = RatelMatMatContractSymmetric(1.0, s_tr_sym, s_tr_sym);
  const CeedScalar q_tr          = sqrt(3. / 2. * s_tr_contract);
  const CeedScalar phi_tr        = q_tr - sigma_y;

  // Check admissibility --> if false, return mapping
  RatelMatInverse(F, Jm1 + 1, F_inv);
  if (phi_tr > phi_tol * sigma_y) {
    yield_flag = 1;
    RatelComputeDeltaGamma_vonMises(sigma_0, hardening_parameters, mu, q_tr, phi_tr, accumulated_plastic, phi_tol, &hardening_slope, &delta_gamma);
    RatelReturnMapping_vonMises(mu, q_tr, delta_gamma, log_e_el_dev_sym);

    // Update C^p-1 = F-1 * be * F^p-T
    for (CeedInt j = 0; j < 3; j++) {
      N_ep_eigenvalues[j] = 2 * mu * (log_e_el_tr_eigenvalues[j] - 1 / 3. * RatelLog1pSeries(Jm1)) / sqrt(2 / 3. * s_tr_contract);
      be_tr_eigenvalues[j] *= RatelExpm1Series(-2 * delta_gamma * N_ep_eigenvalues[j]) + 1;
    }
    RatelMatFromEigensystemSymmetric(be_tr_eigenvalues, n_outer3x6, be_tr_sym);
    RatelSymmetricMatUnpack(be_tr_sym, be_tr);
    RatelMatMatTransposeMult(1, be_tr, F_inv, be_tr_F_invT);
    RatelMatMatMult(1, F_inv, be_tr_F_invT, Cp_n_inv);

    // Update accumulated plastic strain
    accumulated_plastic += delta_gamma;
  }

  //- Compute P = tau * F^-T
  RatelLinearStress(2 * mu, bulk, trace_log_e, log_e_el_dev_sym, tau_sym);
  RatelSymmetricMatUnpack(tau_sym, tau);
  RatelMatMatTransposeMult(1, tau, F_inv, f1);

  // Save updated values for state fields
  RatelSymmetricMatPack(Cp_n_inv, Cp_n_inv_sym);
  RatelStoredValuesPack(Q, i, 0, 1, &accumulated_plastic, current_state);
  RatelStoredValuesPack(Q, i, 1, 6, (CeedScalar *)Cp_n_inv_sym, current_state);

  // Store values
  RatelMatPack(F, F_values);
  RatelMatPack(be_tr_eigenvectors, be_tr_eigenvectors_values);
  RatelStoredValuesPack(Q, i, 0, 6, (CeedScalar *)log_e_el_dev_tr_sym, stored);
  RatelStoredValuesPack(Q, i, 6, 6, (CeedScalar *)s_tr_sym, stored);
  RatelStoredValuesPack(Q, i, 12, 6, (CeedScalar *)tau_sym, stored);
  RatelStoredValuesPack(Q, i, 18, 9, (CeedScalar *)F_values, stored);
  RatelStoredValuesPack(Q, i, 27, 1, &Jm1, stored);
  RatelStoredValuesPack(Q, i, 28, 1, &yield_flag, stored);
  RatelStoredValuesPack(Q, i, 29, 1, &delta_gamma, stored);
  RatelStoredValuesPack(Q, i, 30, 1, &hardening_slope, stored);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Helper for residual, plasticity in log space - initial configuration

  @param[in]   ctx           QFunction context, holding `RatelElastoPlasticityParams`
  @param[in]   Q             Number of quadrature points
  @param[in]   i             Current quadrature point
  @param[in]   in            Input arrays
                               - 0 - volumetric qdata
                               - 1 - previously accepted plastic state components
                               - 2 - gradient of u with respect to reference coordinates
  @param[out]  out           Output arrays
                               - 0 - currently computed plastic state components
  @param[out]  dXdx_initial  Coordinate transformation
  @param[out]  df1           `f1 = Sigma = lambda*trace(e)I + 2 mu e`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int df1_PlasticityHenckyInitial(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in,
                                                      CeedScalar *const *out, CeedScalar dXdx_initial[3][3], CeedScalar df1[3][3]) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA]  = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*state)               = in[1];
  const CeedScalar(*stored)              = in[2];
  const CeedScalar(*du_g)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[3];

  // Context Elastoplasticity
  const RatelElastoPlasticityParams *context = (RatelElastoPlasticityParams *)ctx;
  const CeedScalar                   mu      = context->elasticity_params.mu;
  const CeedScalar                   bulk    = context->elasticity_params.bulk;

  CeedScalar yield_flag = 0.0, accumulated_plastic = 0.0, hardening_slope = 0.0, delta_gamma = 0.0, Jm1 = 0.0, s_tr_sym[6], F_values[9], F[3][3];
  CeedScalar dF[3][3], ddudX[3][3];
  CeedScalar ds_tr_sym[6], dlog_e_el_dev_tr_sym[6], log_e_el_dev_tr_sym[6];
  CeedScalar dbe_tr_sym[6], be_tr[3][3], be_tr_sym[6], dlog_e_el_tr_sym[6];
  CeedScalar Cp_n_inv_sym[6], Cp_n_inv[3][3], F_inv[3][3], Cp_n_inv_FT[3][3];
  CeedScalar F_invT_dFT[3][3], tau_F_invT_dFT[3][3], dtau_minus_tau_F_invT_dFT[3][3];
  CeedScalar tau_sym[6], tau[3][3], dtau[3][3], dtau_sym[6];

  // Unpack state
  RatelStoredValuesUnpack(Q, i, 0, 1, state, &accumulated_plastic);
  RatelStoredValuesUnpack(Q, i, 1, 6, state, Cp_n_inv_sym);
  if (Cp_n_inv_sym[0] == 0) {  // needed for platens
    Cp_n_inv_sym[0] = 1;
    Cp_n_inv_sym[1] = 1;
    Cp_n_inv_sym[2] = 1;
  }
  RatelSymmetricMatUnpack(Cp_n_inv_sym, Cp_n_inv);

  // Unpack stored
  RatelStoredValuesUnpack(Q, i, 0, 6, stored, (CeedScalar *)log_e_el_dev_tr_sym);
  RatelStoredValuesUnpack(Q, i, 6, 6, stored, (CeedScalar *)s_tr_sym);
  RatelStoredValuesUnpack(Q, i, 12, 6, stored, (CeedScalar *)tau_sym);
  RatelSymmetricMatUnpack(tau_sym, tau);
  RatelStoredValuesUnpack(Q, i, 18, 9, stored, (CeedScalar *)F_values);
  RatelStoredValuesUnpack(Q, i, 27, 1, stored, &Jm1);
  RatelStoredValuesUnpack(Q, i, 28, 1, stored, &yield_flag);
  RatelStoredValuesUnpack(Q, i, 29, 1, stored, &delta_gamma);
  RatelStoredValuesUnpack(Q, i, 30, 1, stored, &hardening_slope);
  if (F_values[0] == 0) {  // needed for platens
    F_values[0] = 1;
    F_values[1] = 1;
    F_values[2] = 1;
  }
  RatelMatUnpack(F_values, F);

  //- Compute dF = grad_du = du * dX/dx
  RatelQdataUnpack(Q, i, q_data, dXdx_initial);
  RatelGradUnpack(Q, i, du_g, ddudX);
  RatelMatMatMult(1.0, ddudX, dXdx_initial, dF);

  //- Compute dBe/dF
  RatelMatMatTransposeMult(1, Cp_n_inv, F, Cp_n_inv_FT);
  RatelMatMatMult(1, F, Cp_n_inv_FT, be_tr);
  RatelSymmetricMatPack(be_tr, be_tr_sym);
  RatelCompute_dbedF_Symmetric(Cp_n_inv, F, dF, dbe_tr_sym);

  //- Compute de/dBe
  RatelCompute_dlogAdA_Symmetric(0.5, be_tr_sym, dbe_tr_sym, dlog_e_el_tr_sym);

  //- Compute dq_trial = sqrt(3/2) * (s_tr : ds_tr) / sqrt(tr(s_tr : s_tr))
  // ds_tr = 2 * mu * de_el_dev_tr
  const CeedScalar trace_dlog_e = RatelMatTraceSymmetric(dlog_e_el_tr_sym);
  RatelMatDeviatoricSymmetric(trace_dlog_e, dlog_e_el_tr_sym, dlog_e_el_dev_tr_sym);
  for (CeedInt j = 0; j < 6; j++) ds_tr_sym[j] = 2 * mu * dlog_e_el_dev_tr_sym[j];

  // If yielding --> D^ep
  if (yield_flag == 1) {
    // Compute ds_tr_contract and dq_tr
    const CeedScalar s_tr_contract  = RatelMatMatContractSymmetric(1.0, s_tr_sym, s_tr_sym);
    const CeedScalar q_tr           = sqrt(3. / 2. * s_tr_contract);
    const CeedScalar ds_tr_contract = RatelMatMatContractSymmetric(1.0, s_tr_sym, ds_tr_sym);
    const CeedScalar dq_tr          = 1.5 * ds_tr_contract / q_tr;

    // Update de_el_dev_tr_sym
    RatelReturnMapping_vonMises_fwd(mu, hardening_slope, q_tr, delta_gamma, dq_tr, log_e_el_dev_tr_sym, dlog_e_el_dev_tr_sym);
  }

  RatelLinearStress_fwd(2 * mu, bulk, trace_dlog_e, dlog_e_el_dev_tr_sym, dtau_sym);
  RatelSymmetricMatUnpack(dtau_sym, dtau);

  //- Compute df1 = dP = dtau * F^-T - tau * (F^-T * dF^T * F^-T) = (dtau - tau * F^-T * dF^T) * F^-T
  RatelMatInverse(F, Jm1 + 1, F_inv);
  RatelMatTransposeMatTransposeMult(1, F_inv, dF, F_invT_dFT);
  RatelMatMatMult(1., tau, F_invT_dFT, tau_F_invT_dFT);
  RatelMatMatAdd(1., dtau, -1., tau_F_invT_dFT, dtau_minus_tau_F_invT_dFT);
  RatelMatMatTransposeMult(1, dtau_minus_tau_F_invT_dFT, F_inv, df1);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute residual plasticity in log space - initial configuration

  @param[in]   ctx  QFunction context, holding `ElastoPlasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(PlasticityHenckyInitial_Residual)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return ElasticityResidual(ctx, Q, f1_PlasticityHenckyInitial, !!NUM_COMPONENTS_STATE_PlasticityHenckyInitial,
                            !!NUM_COMPONENTS_STORED_PlasticityHenckyInitial, NUM_ACTIVE_FIELD_EVAL_MODES_PlasticityHenckyInitial, in, out);
}

/**
  @brief Compute Jacobian plasticity in log space - initial configuration

  @param[in]   ctx  QFunction context, holding `ElastoPlasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(PlasticityHenckyInitial_Jacobian)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return ElasticityJacobian(ctx, Q, df1_PlasticityHenckyInitial, !!NUM_COMPONENTS_STATE_PlasticityHenckyInitial,
                            !!NUM_COMPONENTS_STORED_PlasticityHenckyInitial, NUM_ACTIVE_FIELD_EVAL_MODES_PlasticityHenckyInitial, in, out);
}

/**
  @brief Compute platen residual, plasticity in log space - initial configuration

  @param[in]   ctx  QFunction context, holding `ElastoPlasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(PlasticityHenckyInitialPlaten_Residual)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return PlatenBCs(ctx, Q, f1_PlasticityHenckyInitial, !!NUM_COMPONENTS_STATE_PlasticityHenckyInitial,
                   !!NUM_COMPONENTS_STORED_PlasticityHenckyInitial, NUM_ACTIVE_FIELD_EVAL_MODES_PlasticityHenckyInitial, in, out);
}

/**
  @brief Compute platen Jacobian plasticity in log space - initial configuration

  @param[in]   ctx  QFunction context, holding `ElastoPlasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(PlasticityHenckyInitialPlaten_Jacobian)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return PlatenBCs_Jacobian(ctx, Q, df1_PlasticityHenckyInitial, !!NUM_COMPONENTS_STATE_PlasticityHenckyInitial,
                            !!NUM_COMPONENTS_STORED_PlasticityHenckyInitial, NUM_ACTIVE_FIELD_EVAL_MODES_PlasticityHenckyInitial, in, out);
}

/// @}
