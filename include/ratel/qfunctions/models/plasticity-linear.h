/// @file
/// Ratel linear plasticity QFunction source
#include <ceed/types.h>
#ifndef CEED_RUNNING_JIT_PASS
#include <math.h>
#endif

#include "../../models/elasticity-linear.h"  // IWYU pragma: export
#include "../../models/plasticity.h"         // IWYU pragma: export
#include "../boundaries/platen-nitsche.h"    // IWYU pragma: export
#include "../utils.h"                        // IWYU pragma: export
#include "elasticity-common.h"               // IWYU pragma: export
#include "plasticity-common.h"               // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

#define NUM_COMPONENTS_STATE_PlasticityLinear 7
#define NUM_COMPONENTS_STORED_PlasticityLinear 14
#define NUM_ACTIVE_FIELD_EVAL_MODES_PlasticityLinear 1

#define NUM_COMPONENTS_DIAGNOSTIC_PlasticityLinear 23

/**
  @brief Compute `Sigma` for linear plasticity

  @param[in]   ctx                   QFunction context, holding `RatelElastoPlasticityParams`
  @param[in]   Q                     Number of quadrature points
  @param[in]   i                     Current quadrature point
  @param[in]   in                    Input arrays
                                       - 0 - volumetric qdata
                                       - 1 - previously accepted plastic state components
                                       - 2 - gradient of u with respect to reference coordinates
  @param[out]  out                   Output arrays
                                       - 0 - currently computed plastic state components
  @param[out]  dXdx                  Coordinate transformation
  @param[out]  f1                    `f1 = Sigma = lambda*trace(e)I + 2 mu e`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int f1_PlasticityLinear(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in, CeedScalar *const *out,
                                              CeedScalar dXdx[3][3], CeedScalar f1[3][3]) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*state)              = in[1];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[2];

  // Outputs
  CeedScalar(*current_state) = out[0];
  CeedScalar(*stored_values) = out[1];

  // Context
  const RatelElastoPlasticityParams *context                 = (RatelElastoPlasticityParams *)ctx;
  const CeedScalar                   bulk                    = context->elasticity_params.bulk;
  const CeedScalar                   mu                      = context->elasticity_params.mu;
  const CeedScalar                   two_mu                  = context->elasticity_params.two_mu;
  const CeedScalar                   sigma_0                 = context->plasticity_params.yield_stress;
  const CeedScalar                   linear_hardening        = context->plasticity_params.linear_hardening;
  const CeedScalar                   saturation_stress       = context->plasticity_params.saturation_stress;
  const CeedScalar                   hardening_decay         = context->plasticity_params.hardening_decay;
  const CeedScalar                   hardening_parameters[3] = {linear_hardening, saturation_stress, hardening_decay};

  CeedScalar grad_u[3][3], E_plastic[6], E_total[6], E_elastic[6], s_trial[6], E_dev[6], E_dev_tr[6];
  CeedScalar accumulated_plastic = 0.0, delta_gamma = 0.0, hardening_slope = 0.0, sigma_y = 0.0, phi_tol = 1e-5;

  // Qdata
  RatelQdataUnpack(Q, i, q_data, dXdx);

  // Read spatial derivatives of u; du/dX
  CeedScalar dudX[3][3];
  RatelGradUnpack(Q, i, ug, dudX);

  // Compute grad_u = du/dX * dX/dx
  // X is ref coordinate [-1,1]^3; x is physical coordinate
  RatelMatMatMult(1.0, dudX, dXdx, grad_u);

  // Unpack state fields
  RatelStoredValuesUnpack(Q, i, 0, 6, state, E_plastic);
  RatelStoredValuesUnpack(Q, i, 6, 1, state, &accumulated_plastic);

  // Compute E
  RatelLinearStrain(grad_u, E_total);

  // Subtract off any previous/existing plastic strain
  for (CeedInt j = 0; j < 6; j++) E_elastic[j] = E_total[j] - E_plastic[j];

  // // -- s_trial = 2*mu * E^e_dev
  const CeedScalar trace_e = RatelMatTraceSymmetric(E_elastic);

  RatelMatDeviatoricSymmetric(trace_e, E_elastic, E_dev_tr);
  for (CeedInt j = 0; j < 6; j++) {
    s_trial[j] = two_mu * E_dev_tr[j];
    E_dev[j]   = E_dev_tr[j];
  }

  // Compute values needed for determining if we are in the plastic regime
  RatelComputeFlowStress(sigma_0, hardening_parameters, accumulated_plastic, &sigma_y);
  const CeedScalar s_trial_contract = RatelMatMatContractSymmetric(1.0, s_trial, s_trial);
  const CeedScalar q_trial          = sqrt(3. / 2. * s_trial_contract);
  const CeedScalar phi_trial        = q_trial - sigma_y;

  // If we are in plastic regime: compute elastic and plastic parts of E
  if (phi_trial > phi_tol * sigma_y) {
    RatelComputeDeltaGamma_vonMises(sigma_0, hardening_parameters, mu, q_trial, phi_trial, accumulated_plastic, phi_tol, &hardening_slope,
                                    &delta_gamma);
    RatelReturnMapping_vonMises(mu, q_trial, delta_gamma, E_dev);

    // Update the plastic part of E
    for (CeedInt j = 0; j < 6; j++) {
      E_plastic[j] += delta_gamma * 1.5 * s_trial[j] / q_trial;
    }

    // Update accumulated plastic strain
    accumulated_plastic += delta_gamma;
  }

  // Compute SKP (S_sym) with just elastic part
  const CeedScalar S_sym[6] = {bulk * trace_e + two_mu * E_dev[0],
                               bulk * trace_e + two_mu * E_dev[1],
                               bulk * trace_e + two_mu * E_dev[2],
                               two_mu * E_dev[3],
                               two_mu * E_dev[4],
                               two_mu * E_dev[5]};

  // Save updated values for state fields
  RatelStoredValuesPack(Q, i, 0, 6, (CeedScalar *)E_plastic, current_state);
  RatelStoredValuesPack(Q, i, 6, 1, &accumulated_plastic, current_state);

  // Store values
  RatelStoredValuesPack(Q, i, 0, 6, (CeedScalar *)E_dev_tr, stored_values);
  RatelStoredValuesPack(Q, i, 6, 6, (CeedScalar *)s_trial, stored_values);
  RatelStoredValuesPack(Q, i, 12, 1, &delta_gamma, stored_values);
  RatelStoredValuesPack(Q, i, 13, 1, &hardening_slope, stored_values);

  RatelSymmetricMatUnpack(S_sym, f1);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute linearization of `Sigma` for linear plasticity

  @param[in]   ctx                   QFunction context, holding `RatelElastoPlasticityParams`
  @param[in]   Q                     Number of quadrature points
  @param[in]   i                     Current quadrature point
  @param[in]   in                    Input arrays
                                       - 0 - volumetric qdata
                                       - 1 - previously accepted plastic state components
                                       - 2 - stored values for total strain and s_trial
                                       - 3 - gradient of incremental change to u with respect to reference coordinates
  @param[out]  out                   Output arrays, unused
  @param[out]  dXdx                  Coordinate transformation
  @param[out]  df1                   `df1 = dSigma = lambda tr(de) I + 2 mu de`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int df1_PlasticityLinear(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in, CeedScalar *const *out,
                                               CeedScalar dXdx[3][3], CeedScalar df1[3][3]) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*current_state)      = in[1];
  const CeedScalar(*stored_values)      = in[2];
  const CeedScalar(*dug)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[3];

  // Context
  const RatelElastoPlasticityParams *context                 = (RatelElastoPlasticityParams *)ctx;
  const CeedScalar                   bulk                    = context->elasticity_params.bulk;
  const CeedScalar                   mu                      = context->elasticity_params.mu;
  const CeedScalar                   two_mu                  = context->elasticity_params.two_mu;
  const CeedScalar                   sigma_0                 = context->plasticity_params.yield_stress;
  const CeedScalar                   linear_hardening        = context->plasticity_params.linear_hardening;
  const CeedScalar                   saturation_stress       = context->plasticity_params.saturation_stress;
  const CeedScalar                   hardening_decay         = context->plasticity_params.hardening_decay;
  const CeedScalar                   hardening_parameters[3] = {linear_hardening, saturation_stress, hardening_decay};

  CeedScalar grad_du[3][3], de_elastic[6], s_trial[6], E_dev_tr[6], ds_trial[6], dE_dev[6];
  CeedScalar accumulated_plastic = 0.0, delta_gamma = 0.0, hardening_slope = 0.0, sigma_y = 0.0, phi_tol = 1e-5;

  // Qdata
  RatelQdataUnpack(Q, i, q_data, dXdx);

  // Read spatial derivatives of increment u; d(du)/dX = d(du)/dX
  CeedScalar ddudX[3][3];
  RatelGradUnpack(Q, i, dug, ddudX);

  // Compute grad_du = ddu/dX * dX/dx
  // X is ref coordinate [-1,1]^3; x is physical coordinate
  RatelMatMatMult(1.0, ddudX, dXdx, grad_du);

  // Compute Strain : de (epsilon)
  // de = 1/2 (grad_du + (grad_du)^T)
  RatelLinearStrain_fwd(grad_du, de_elastic);

  // Unpack state fields
  RatelStoredValuesUnpack(Q, i, 6, 1, current_state, &accumulated_plastic);
  // Unpack stored values
  RatelStoredValuesUnpack(Q, i, 0, 6, stored_values, E_dev_tr);
  RatelStoredValuesUnpack(Q, i, 6, 6, stored_values, s_trial);
  RatelStoredValuesUnpack(Q, i, 12, 1, stored_values, &delta_gamma);
  RatelStoredValuesUnpack(Q, i, 13, 1, stored_values, &hardening_slope);

  // -- ds_trial = 2*mu * dE^e_dev
  const CeedScalar trace_de = RatelMatTraceSymmetric(de_elastic);

  RatelMatDeviatoricSymmetric(trace_de, de_elastic, dE_dev);
  for (CeedInt j = 0; j < 6; j++) ds_trial[j] = two_mu * dE_dev[j];

  // Compute values needed for determining if we are in the plastic regime and their derivatives
  RatelComputeFlowStress(sigma_0, hardening_parameters, accumulated_plastic, &sigma_y);
  const CeedScalar s_trial_contract  = RatelMatMatContractSymmetric(1.0, s_trial, s_trial);
  const CeedScalar ds_trial_contract = RatelMatMatContractSymmetric(2.0, s_trial, ds_trial);
  const CeedScalar q_trial           = sqrt(3. / 2. * s_trial_contract);
  const CeedScalar dq_trial          = 0.5 * sqrt(3. / 2.) * (ds_trial_contract / sqrt(s_trial_contract));
  const CeedScalar phi_trial         = q_trial - sigma_y;

  // Check if we are in the plastic regime
  if (phi_trial > phi_tol * sigma_y) {
    // Update de_el_dev_tr_sym
    RatelReturnMapping_vonMises_fwd(mu, hardening_slope, q_trial, delta_gamma, dq_trial, E_dev_tr, dE_dev);
  }
  // Compute dS
  const CeedScalar dsigma_sym[6] = {bulk * trace_de + two_mu * dE_dev[0],
                                    bulk * trace_de + two_mu * dE_dev[1],
                                    bulk * trace_de + two_mu * dE_dev[2],
                                    two_mu * dE_dev[3],
                                    two_mu * dE_dev[4],
                                    two_mu * dE_dev[5]};

  RatelSymmetricMatUnpack(dsigma_sym, df1);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute residual for linear plasticity

  @param[in]   ctx  QFunction context, holding `RatelElastoPlasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(PlasticityResidual_PlasticityLinear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return ElasticityResidual(ctx, Q, f1_PlasticityLinear, !!NUM_COMPONENTS_STATE_PlasticityLinear, !!NUM_COMPONENTS_STORED_PlasticityLinear,
                            NUM_ACTIVE_FIELD_EVAL_MODES_PlasticityLinear, in, out);
}

/**
  @brief Evaluate Jacobian for linear plasticity

  @param[in]   ctx  QFunction context, holding `RatelElastoPlasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(PlasticityJacobian_PlasticityLinear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return ElasticityJacobian(ctx, Q, df1_PlasticityLinear, !!NUM_COMPONENTS_STATE_PlasticityLinear, !!NUM_COMPONENTS_STORED_PlasticityLinear,
                            NUM_ACTIVE_FIELD_EVAL_MODES_PlasticityLinear, in, out);
}

/**
  @brief Compute platen residual for linear plasticity

  @param[in]   ctx  QFunction context, holding `RatelBCPlatenParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(PlatenBCsResidual_PlasticityLinear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return PlatenBCs(ctx, Q, f1_PlasticityLinear, !!NUM_COMPONENTS_STATE_PlasticityLinear, !!NUM_COMPONENTS_STORED_PlasticityLinear,
                   NUM_ACTIVE_FIELD_EVAL_MODES_PlasticityLinear, in, out);
}

/**
  @brief Evaluate platen Jacobian for linear plasticity

  @param[in]   ctx  QFunction context, holding `RatelBCPlatenParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(PlatenBCsJacobian_PlasticityLinear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return PlatenBCs_Jacobian(ctx, Q, df1_PlasticityLinear, !!NUM_COMPONENTS_STATE_PlasticityLinear, !!NUM_COMPONENTS_STORED_PlasticityLinear,
                            NUM_ACTIVE_FIELD_EVAL_MODES_PlasticityLinear, in, out);
}

/**
  @brief Compute strain energy for linear plasticity

  @param[in]   ctx  QFunction context, holding `RatelElastoPlasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - plastic state
                      - 2 - gradient of u with respect to reference coordinates
  @param[out]  out  Output arrays
                      - 0 - strain energy

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(StrainEnergy_PlasticityLinear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*state)              = in[1];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[2];

  // Outputs
  CeedScalar(*energy) = (CeedScalar(*))out[0];

  // Context
  const RatelElastoPlasticityParams *context = (RatelElastoPlasticityParams *)ctx;
  const CeedScalar                   lambda  = context->elasticity_params.lambda;
  const CeedScalar                   mu      = context->elasticity_params.mu;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar dXdx[3][3], grad_u[3][3], E_total[6], E_elastic[6], E_plastic[6];

    // Qdata
    const CeedScalar wdetJ = q_data[0][i];

    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Read spatial derivatives of u; du/dX
    CeedScalar dudX[3][3];
    RatelGradUnpack(Q, i, ug, dudX);

    // Compute grad_u = du/dX * dX/dx
    // X is ref coordinate [-1,1]^3; x is physical coordinate
    RatelMatMatMult(1.0, dudX, dXdx, grad_u);

    // Compute Strain : e (epsilon)
    // e = 1/2 (grad u + (grad u)^T)
    RatelLinearStrain(grad_u, E_total);

    // Unpack state field for plastic strain
    RatelStoredValuesUnpack(Q, i, 0, 6, state, E_plastic);
    // Subtract off any previous/existing plastic strain
    for (CeedInt j = 0; j < 6; j++) E_elastic[j] = E_total[j] - E_plastic[j];

    // Strain energy = lambda/2 (trace(e))^2 + mu e:e
    // -- trace(e) = div(u) = Volumetric strain
    const CeedScalar trace_e = RatelMatTraceSymmetric(E_elastic);

    // -- e:e = trace(e^2)
    const CeedScalar trace_e2 = RatelMatMatContractSymmetric(1.0, E_elastic, E_elastic);

    energy[i] = (lambda * trace_e * trace_e / 2. + mu * trace_e2) * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute projected diagnostic values for linear plasticity

  @param[in]   ctx  QFunction context, holding `RatelElastoPlasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - plastic state
                      - 2 - u
                      - 3 - gradient of u with respect to reference coordinates
  @param[out]  out  Output arrays
                      - 0 - projected diagnostic values

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(Diagnostic_PlasticityLinear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*state)              = in[1];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[2];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[3];

  // Outputs
  CeedScalar(*diagnostic)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Context
  const RatelElastoPlasticityParams *context = (RatelElastoPlasticityParams *)ctx;
  const CeedScalar                   lambda  = context->elasticity_params.lambda;
  const CeedScalar                   mu      = context->elasticity_params.mu;
  const CeedScalar                   two_mu  = context->elasticity_params.two_mu;
  const CeedScalar                   rho     = context->elasticity_params.common_parameters[RATEL_COMMON_PARAMETER_RHO];

  CeedScalar e_plastic[6], e_total[6], e_elastic[6], accumulated_plastic = 0.0;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar dXdx[3][3], grad_u[3][3], s_trial_sym[6];

    // Qdata
    const CeedScalar wdetJ = q_data[0][i];
    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Read spatial derivatives of u; du/dX
    CeedScalar dudX[3][3];
    RatelGradUnpack(Q, i, ug, dudX);

    // Compute grad_u = du/dX * dX/dx
    // X is ref coordinate [-1,1]^3; x is physical coordinate
    RatelMatMatMult(1.0, dudX, dXdx, grad_u);

    // Read E_plastic
    RatelStoredValuesUnpack(Q, i, 0, 6, state, e_plastic);
    RatelStoredValuesUnpack(Q, i, 6, 1, state, &accumulated_plastic);

    // Compute E
    RatelLinearStrain(grad_u, e_total);

    // Subtract off plastic strain
    for (CeedInt j = 0; j < 6; j++) e_elastic[j] = e_total[j] - e_plastic[j];

    // // -- Cauchy stress
    const CeedScalar trace_e      = RatelMatTraceSymmetric(e_elastic);
    const CeedScalar sigma_sym[6] = {lambda * trace_e + two_mu * e_elastic[0],
                                     lambda * trace_e + two_mu * e_elastic[1],
                                     lambda * trace_e + two_mu * e_elastic[2],
                                     two_mu * e_elastic[3],
                                     two_mu * e_elastic[4],
                                     two_mu * e_elastic[5]};

    // Displacement
    diagnostic[0][i] = u[0][i];
    diagnostic[1][i] = u[1][i];
    diagnostic[2][i] = u[2][i];

    // Cauchy stress tensor
    diagnostic[3][i] = sigma_sym[0];
    diagnostic[4][i] = sigma_sym[5];
    diagnostic[5][i] = sigma_sym[4];
    diagnostic[6][i] = sigma_sym[1];
    diagnostic[7][i] = sigma_sym[3];
    diagnostic[8][i] = sigma_sym[2];

    // Hydrostatic pressure; p_h = -trace(sigma) / 3
    diagnostic[9][i] = -RatelMatTraceSymmetric(sigma_sym) / 3.;

    // Strain tensor invariants; trace(e)
    diagnostic[10][i]         = trace_e;
    // trace(e^2) = e:e
    const CeedScalar trace_e2 = RatelMatMatContractSymmetric(1.0, e_elastic, e_elastic);

    diagnostic[11][i] = trace_e2;
    diagnostic[12][i] = 1 + trace_e;

    // Strain energy
    diagnostic[13][i] = lambda * trace_e * trace_e / 2. + mu * trace_e2;

    // Compute von-Mises stress

    // -- Compute the the deviatoric part of Cauchy stress: s_trial = sigma - trace(sigma)/3
    const CeedScalar trace_sigma = RatelMatTraceSymmetric(sigma_sym);

    RatelMatDeviatoricSymmetric(trace_sigma, sigma_sym, s_trial_sym);

    // -- s_trial:s_trial
    const CeedScalar s_trial_contract = RatelMatMatContractSymmetric(1.0, s_trial_sym, s_trial_sym);

    // -- Compute von-Mises stress: sigma_von = sqrt(3/2 s_trial:s_trial)
    diagnostic[14][i] = sqrt(3. * s_trial_contract / 2.);

    // Compute mass density values

    // -- Set to 0 as default (rho should be 0.0 as defualt anyways)
    diagnostic[15][i] = 0.0;

    // -- if mass density is requested then compute mass density: rho/J
    if (rho > 0.0) diagnostic[15][i] = rho / diagnostic[12][i];

    // Plastic strain tensor
    diagnostic[16][i] = e_plastic[0];
    diagnostic[17][i] = e_plastic[5];
    diagnostic[18][i] = e_plastic[4];
    diagnostic[19][i] = e_plastic[1];
    diagnostic[20][i] = e_plastic[3];
    diagnostic[21][i] = e_plastic[2];

    // Accumulated plastic strain
    diagnostic[22][i] = accumulated_plastic;

    // Quadrature weight
    for (CeedInt j = 0; j < NUM_COMPONENTS_DIAGNOSTIC_PlasticityLinear; j++) diagnostic[j][i] *= wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
