/// @file
/// Ratel common dual space diagnostic values
#include <ceed/types.h>

/// @addtogroup RatelMaterials
/// @{

#define NUM_COMPONENTS_DIAGNOSTIC_Dual 1

/**
  @brief Compute dual space diagnostic values

  @param[in]   ctx  QFunction context, unused
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - volumetric qdata
  @param[out]  out  Output array
                      - 0 - nodal volume

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(DualDiagnostic)(void *ctx, const CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];

  // Outputs
  CeedScalar *v = (CeedScalar(*))out[0];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) { v[i] = 1.0 * q_data[0][i]; }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
