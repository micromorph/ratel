/// @file
/// Ratel Neo-Hookean (initial configuration) with damage phase field
#include <ceed/types.h>

#include "../../models/elasticity-damage.h"           // IWYU pragma: export
#include "../boundaries/platen-nitsche.h"             // IWYU pragma: export
#include "../utils.h"                                 // IWYU pragma: export
#include "elasticity-isochoric-neo-hookean-common.h"  // IWYU pragma: export
#include "elasticity-neo-hookean-damage-common.h"     // IWYU pragma: export

#define NUM_COMPONENTS_STORED_NeoHookeanInitial_Damage 17  // 1 damage + 6 strain + 1 Psi_plus_history_flag + 9 grad_u

/// @addtogroup RatelMaterials
/// @{

/**
  @brief Compute u_t term of damage residual

  @param[in]   ctx  QFunction context, holding `RatelElasticityDamageParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - volumetric qdata
                      - 1 - u_t (four components: 3 displacement components, 1 scalar damage field)
  @param[out]  out  Output arrays
                      - 0 - action on u_t

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityDamageResidual_ut_NeoHookeanInitial)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u_t)[CEED_Q_VLA]    = (const CeedScalar(*)[CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*v_t)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Context Elasticity+Damage
  const RatelElasticityDamageParams *context        = (RatelElasticityDamageParams *)ctx;
  const CeedScalar                   xi             = context->damage_viscosity;
  const CeedScalar                   damage_scaling = context->damage_scaling;

  // Quadrature point loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    const CeedScalar wdetJ = q_data[0][i];

    // Update values
    v_t[0][i] = 0;
    v_t[1][i] = 0;
    v_t[2][i] = 0;
    v_t[3][i] = damage_scaling * xi * u_t[3][i] * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute residual for Neo-hookean hyperelasticity initial conf.

  @param[in]   ctx  QFunction context, holding `RatelElasticityDamageParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - volumetric qdata
                      - 1 - Psi_plus and damage state
                      - 2 - u (four components: 3 displacement components, 1 scalar damage field)
                      - 3 - u_g gradient of u with respect to reference coordinates
  @param[out]  out  Output arrays
                      - 0 - Updated Psi_plus and damage state
                      - 1 - stored vector
                      - 2 - action on u
                      - 3 - action on u_g

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityDamageResidual_NeoHookeanInitial)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*state)              = in[1];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[2];
  const CeedScalar(*u_g)[4][CEED_Q_VLA] = (const CeedScalar(*)[4][CEED_Q_VLA])in[3];

  // Outputs
  CeedScalar(*current_state)       = out[0];
  CeedScalar(*stored)              = out[1];
  CeedScalar(*v)[CEED_Q_VLA]       = (CeedScalar(*)[CEED_Q_VLA])out[2];
  CeedScalar(*dvdX)[4][CEED_Q_VLA] = (CeedScalar(*)[4][CEED_Q_VLA])out[3];

  // Context
  const RatelElasticityDamageParams *context        = (RatelElasticityDamageParams *)ctx;
  const CeedScalar                   mu             = context->mu;
  const CeedScalar                   bulk           = context->bulk;
  const CeedScalar                   Gc             = context->fracture_toughness;
  const CeedScalar                   l0             = context->characteristic_length;
  const CeedScalar                   eta            = context->residual_stiffness;
  const CeedScalar                   damage_scaling = context->damage_scaling;
  const bool                         use_AT1        = context->use_AT1;

  const CeedScalar Psi_plus_critical = use_AT1 ? 3 * Gc / (8 * 2 * l0) : 0;
  const CeedScalar c0                = use_AT1 ? 8. / 3. : 2.;

  // Quadrature point loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar V, J_dVdJ, Psi_plus_history_flag, H, Psi_plus;
    CeedScalar grad_phi[3], E_sym[6], S_degr_sym[6], S_degr[3][3], P_degr[3][3], dXdx[3][3], grad_u[3][3];

    // dX/dx from Q_data
    const CeedScalar wdetJ = q_data[0][i];
    RatelQdataUnpack(Q, i, q_data, dXdx);

    //----- Displacement problem
    // Spatial derivatives of u
    const CeedScalar dudX[3][3] = {
        {u_g[0][0][i], u_g[1][0][i], u_g[2][0][i]},
        {u_g[0][1][i], u_g[1][1][i], u_g[2][1][i]},
        {u_g[0][2][i], u_g[1][2][i], u_g[2][2][i]}
    };

    // Compute grad_u = du/dX * dX/dx
    RatelMatMatMult(1.0, dudX, dXdx, grad_u);

    // Compute the Deformation Gradient : F = I + grad_u
    const CeedScalar F[3][3] = {
        {grad_u[0][0] + 1, grad_u[0][1],     grad_u[0][2]    },
        {grad_u[1][0],     grad_u[1][1] + 1, grad_u[1][2]    },
        {grad_u[2][0],     grad_u[2][1],     grad_u[2][2] + 1}
    };

    // Read damage (forth component)
    const CeedScalar phi = u[3][i];

    // Compute J - 1
    const CeedScalar Jm1 = RatelMatDetAM1(grad_u);

    // Volumetric function and derivatives
    VolumetricFunctionAndDerivatives(Jm1, &V, &J_dVdJ, NULL);

    // Compute degraded first Piola-kirchhoff stress P_degr = F * S_degr
    RatelGreenLagrangeStrain(grad_u, E_sym);
    ComputeDegradedSecondKirchhoffStress_NeoHookean(J_dVdJ, mu, bulk, eta, phi, Jm1, E_sym, S_degr_sym);
    RatelSymmetricMatUnpack(S_degr_sym, S_degr);
    RatelMatMatMult(1.0, F, S_degr, P_degr);

    // Compute (grad(v), P_degr)
    for (CeedInt j = 0; j < 3; j++) {
      for (CeedInt k = 0; k < 3; k++) {
        dvdX[j][k][i] = 0;
        for (CeedInt m = 0; m < 3; m++) {
          dvdX[j][k][i] += wdetJ * dXdx[j][m] * P_degr[k][m];
        }
      }
    }

    //----- Damage problem
    // Compute Psi_plus
    const CeedScalar trace_E = RatelMatTraceSymmetric(E_sym);
    PsiPlus_NeoHookean(V, mu, bulk, Jm1, trace_E, &Psi_plus);

    // Treat Psi_plus as a history variable and ensure that increases monotonically for crack irreversibility
    const CeedScalar Psi_plus_old = state[i];
    Psi_plus_history_flag         = Psi_plus > Psi_plus_old;
    Psi_plus                      = RatelMax(Psi_plus, Psi_plus_old);
    Psi_plus                      = RatelMax(Psi_plus_critical, Psi_plus);

    // Spatial derivative of phi
    const CeedScalar phi_g[3] = {u_g[0][3][i], u_g[1][3][i], u_g[2][3][i]};

    // Compute grad_phi = dX/dx^T dphi/dX
    RatelMatTransposeVecMult(1.0, dXdx, phi_g, grad_phi);
    const CeedScalar dg_dphi = -2 * (1 - eta) * (1 - phi);

    // If AT2, substitute appropiate values for c0 and alpha (hence dalpha)
    const CeedScalar dalpha_dphi = use_AT1 ? 1. : 2. * phi;

    // Compute H and update v values
    H       = dg_dphi * Psi_plus + (Gc / (c0 * l0)) * dalpha_dphi;
    v[0][i] = 0;
    v[1][i] = 0;
    v[2][i] = 0;
    v[3][i] = damage_scaling * H * wdetJ;

    // Update fourth component of dvdX via (grad(q), 2 Gc l0 / c0 grad(phi))
    for (CeedInt j = 0; j < 3; j++) {
      dvdX[j][3][i] = damage_scaling * (grad_phi[0] * dXdx[j][0] + grad_phi[1] * dXdx[j][1] + grad_phi[2] * dXdx[j][2]) * 2. * Gc * l0 * wdetJ / c0;
    }

    // Save updated values for state fields
    RatelStoredValuesPack(Q, i, 0, 1, &Psi_plus, current_state);

    // Store values
    RatelStoredValuesPack(Q, i, 0, 1, &phi, stored);
    RatelStoredValuesPack(Q, i, 1, 6, (CeedScalar *)E_sym, stored);
    RatelStoredValuesPack(Q, i, 7, 1, &Psi_plus_history_flag, stored);
    RatelStoredValuesPack(Q, i, 8, 9, (CeedScalar *)grad_u, stored);
  }  // End of Quadrature Point Loop

  // Prevent unused functions warnings
  (void)StrainEnergy_IsochoricNeoHookean;
  (void)Diagnostic_IsochoricNeoHookean;
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute jacobian for Neo-hookean hyperelasticity initial conf.

  @param[in]   ctx  QFunction context, holding `RatelElasticityDamageParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - volumetric qdata
                      - 1 - Current state values
                      - 2 - Stored values
                      - 3 - du (four components: 3 displacement components, 1 damage field)
                      - 4 - du_g gradient of du with respect to reference coordinates
  @param[out]  out  Output arrays
                      - 0 - action on du
                      - 1 - action on du_g

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityDamageJacobian_NeoHookeanInitial)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA]  = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*current_state)       = in[1];
  const CeedScalar(*stored)              = in[2];
  const CeedScalar(*du)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[3];
  const CeedScalar(*du_g)[4][CEED_Q_VLA] = (const CeedScalar(*)[4][CEED_Q_VLA])in[4];

  // Outputs
  CeedScalar(*dv)[CEED_Q_VLA]       = (CeedScalar(*)[CEED_Q_VLA])out[0];
  CeedScalar(*ddvdX)[4][CEED_Q_VLA] = (CeedScalar(*)[4][CEED_Q_VLA])out[1];

  // Context Elasticity+Damage
  const RatelElasticityDamageParams *context        = (RatelElasticityDamageParams *)ctx;
  const CeedScalar                   mu             = context->mu;
  const CeedScalar                   bulk           = context->bulk;
  const CeedScalar                   Gc             = context->fracture_toughness;
  const CeedScalar                   l0             = context->characteristic_length;
  const CeedScalar                   eta            = context->residual_stiffness;
  const CeedScalar                   damage_scaling = context->damage_scaling;
  const CeedScalar                   xi             = context->damage_viscosity;
  const bool                         use_AT1        = context->use_AT1;
  const bool                         use_offdiag    = context->use_offdiagonal;
  const CeedScalar                   shift_v        = context->common_parameters[1];

  // Set d2alpha_dphi2 and c0 based on AT1 or AT2
  const CeedScalar c0            = use_AT1 ? 8. / 3. : 2.;
  const CeedScalar d2alpha_dphi2 = use_AT1 ? 0. : 2.;
  const CeedScalar d2g_dphi2     = 2 * (1 - eta);

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar J_dVdJ, J2_d2VdJ2, Psi_plus_history_flag, dPsi_plus, dH;
    CeedScalar Psi_plus, phi, grad_dphi[3], E_sym[6], dE_sym[6], grad_u[3][3];
    CeedScalar dXdx[3][3], grad_du[3][3], S_degr_sym[6], S_degr[3][3], dS_degr_sym[6], dS_degr[3][3], dP_degr[3][3];

    // Read spatial derivatives of increment u; d(du)/dX = d(du)/dX
    const CeedScalar ddudX[3][3] = {
        {du_g[0][0][i], du_g[1][0][i], du_g[2][0][i]},
        {du_g[0][1][i], du_g[1][1][i], du_g[2][1][i]},
        {du_g[0][2][i], du_g[1][2][i], du_g[2][2][i]}
    };

    // Extract Qdata
    const CeedScalar wdetJ = q_data[0][i];
    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Compute grad_du = ddu/dX * dX/dx
    // X is ref coordinate [-1,1]^3; x is physical coordinate
    RatelMatMatMult(1.0, ddudX, dXdx, grad_du);

    // Unpack stored values
    RatelStoredValuesUnpack(Q, i, 0, 1, stored, &phi);
    RatelStoredValuesUnpack(Q, i, 1, 6, stored, E_sym);
    RatelStoredValuesUnpack(Q, i, 7, 1, stored, &Psi_plus_history_flag);
    RatelStoredValuesUnpack(Q, i, 8, 9, stored, (CeedScalar *)grad_u);

    // Deformation Gradient : F = I + grad_u
    const CeedScalar F[3][3] = {
        {grad_u[0][0] + 1, grad_u[0][1],     grad_u[0][2]    },
        {grad_u[1][0],     grad_u[1][1] + 1, grad_u[1][2]    },
        {grad_u[2][0],     grad_u[2][1],     grad_u[2][2] + 1}
    };

    // Compute J - 1
    const CeedScalar Jm1 = RatelMatDetAM1(grad_u);

    // Volumetric function and derivatives
    VolumetricFunctionAndDerivatives(Jm1, NULL, &J_dVdJ, &J2_d2VdJ2);

    // Compute S_degr
    ComputeDegradedSecondKirchhoffStress_NeoHookean(J_dVdJ, mu, bulk, eta, phi, Jm1, E_sym, S_degr_sym);
    RatelSymmetricMatUnpack(S_degr_sym, S_degr);

    // Retrieve dphi
    const CeedScalar dphi = du[3][i];

    // Compute dS_degr
    ComputeDegradedSecondKirchhoffStress_NeoHookean_fwd(J_dVdJ, J2_d2VdJ2, 2 * mu, bulk, eta, use_offdiag, phi, Jm1, F, grad_du, E_sym, dphi,
                                                        dS_degr_sym);
    RatelSymmetricMatUnpack(dS_degr_sym, dS_degr);

    // dP_degr
    RatelMatMatMultPlusMatMatMult(grad_du, S_degr, F, dS_degr, dP_degr);
    for (CeedInt j = 0; j < 3; j++) {
      for (CeedInt k = 0; k < 3; k++) {
        ddvdX[j][k][i] = 0;
        for (CeedInt m = 0; m < 3; m++) {
          ddvdX[j][k][i] += wdetJ * dXdx[j][m] * dP_degr[k][m];
        }
      }
    }

    //--Compute l0*Gc*(grad(dphi), grad(q))
    // Spatial derivative of phi
    const CeedScalar dphi_g[3] = {du_g[0][3][i], du_g[1][3][i], du_g[2][3][i]};

    // grad_dphi = dX/dx^T * d(dphi)/dX
    RatelMatTransposeVecMult(1.0, dXdx, dphi_g, grad_dphi);

    // Unpack state fields
    RatelStoredValuesUnpack(Q, i, 0, 1, current_state, &Psi_plus);

    // Get dg_dphi
    const CeedScalar dg_dphi = -2 * (1 - eta) * (1 - phi);

    // Compute d_phi_t
    const CeedScalar dphi_t = shift_v * dphi;

    // Compute dE
    RatelGreenLagrangeStrain_fwd(grad_du, F, dE_sym);

    // Compute dPsi_plus
    const bool add_offdiag = !(Psi_plus_history_flag == 0. || !use_offdiag);
    PsiPlus_NeoHookeanInitial_fwd(J_dVdJ, 2 * mu, bulk, Jm1, E_sym, dE_sym, &dPsi_plus);
    dPsi_plus = add_offdiag * dPsi_plus;

    // Compute dH and dv values
    dH       = d2g_dphi2 * dphi * Psi_plus + dg_dphi * dPsi_plus + (Gc / (c0 * l0)) * d2alpha_dphi2 * dphi + xi * dphi_t;
    dv[0][i] = 0;
    dv[1][i] = 0;
    dv[2][i] = 0;
    dv[3][i] = damage_scaling * dH * wdetJ;

    // Update fourth component of dvdX via (grad(dq), 2 Gc l0 / c0 grad(dphi))
    for (CeedInt j = 0; j < 3; j++) {
      ddvdX[j][3][i] =
          damage_scaling * (grad_dphi[0] * dXdx[j][0] + grad_dphi[1] * dXdx[j][1] + grad_dphi[2] * dXdx[j][2]) * 2. * Gc * l0 * wdetJ / c0;
    }
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_JACOBIAN_NeoHookeanInitial_Damage                                                                                               \
  (3 + FLOPS_MatMatMult + 3 + FLOPS_MatDetAM1 + FLOPS_Degraded_S_NeoHookean + FLOPS_Degraded_dS_NeoHookean + FLOPS_MatMatMultPlusMatMatMult + \
   +FLOPS_MatMatMult + FLOPS_MatVecMult + 5 + FLOPS_GreenLagrangeStrain_fwd + FLOPS_PsiPlus_NeoHookeanInitial_fwd + 14 + 33)

/**
  @brief Compute platens residual for Neo-hookean hyperelasticity initial conf.

  @param[in]   ctx          QFunction context, holding `RatelElasticityDamageParams`
  @param[in]   Q            Number of quadrature points
  @param[in]   i            Index
  @param[in]   in           Input arrays
                              - 0 - volumetric qdata
                              - 2 - u (four components: 3 displacement components, 1 scalar damage field)
                              - 3 - u_g gradient of u with respect to reference coordinates
  @param[out]  out          Output arrays
                              - 1 - stored vector
                              - 2 - initializing v
  @param[out]  dXdx         dXdx
  @param[out]  P_degr       Degraded stress tensor

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int ElasticityDamagePlatenResidual_NeoHookeanInitial(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in,
                                                                           CeedScalar *const *out, CeedScalar dXdx[3][3], CeedScalar P_degr[3][3]) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[2];
  const CeedScalar(*ug)[4][CEED_Q_VLA]  = (const CeedScalar(*)[4][CEED_Q_VLA])in[3];

  // Outputs
  CeedScalar(*state_out)     = out[0];
  CeedScalar(*stored)        = out[1];
  CeedScalar(*v)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[2];

  // Context
  const RatelElasticityDamageParams *context = (RatelElasticityDamageParams *)ctx;
  CeedScalar                         mu      = context->mu;
  CeedScalar                         bulk    = context->bulk;
  const CeedScalar                   eta     = context->residual_stiffness;

  CeedScalar J_dVdJ, E_sym[6], S_degr_sym[6], S_degr[3][3], grad_u[3][3];

  // Read spatial derivatives of u
  const CeedScalar dudX[3][3] = {
      {ug[0][0][i], ug[1][0][i], ug[2][0][i]},
      {ug[0][1][i], ug[1][1][i], ug[2][1][i]},
      {ug[0][2][i], ug[1][2][i], ug[2][2][i]}
  };
  // -- Qdata
  RatelQdataUnpack(Q, i, q_data, dXdx);

  // Compute grad_u , Jm1, E
  RatelMatMatMult(1.0, dudX, dXdx, grad_u);
  const CeedScalar Jm1 = RatelMatDetAM1(grad_u);
  RatelGreenLagrangeStrain(grad_u, E_sym);

  // Compute the Deformation Gradient : F = I + grad_u
  const CeedScalar F[3][3] = {
      {grad_u[0][0] + 1, grad_u[0][1],     grad_u[0][2]    },
      {grad_u[1][0],     grad_u[1][1] + 1, grad_u[1][2]    },
      {grad_u[2][0],     grad_u[2][1],     grad_u[2][2] + 1}
  };

  // Read damage (forth component)
  const CeedScalar phi = u[3][i];

  // Volumetric function and derivatives
  VolumetricFunctionAndDerivatives(Jm1, NULL, &J_dVdJ, NULL);

  // Compute P_degr
  ComputeDegradedSecondKirchhoffStress_NeoHookean(J_dVdJ, mu, bulk, eta, phi, Jm1, E_sym, S_degr_sym);
  RatelSymmetricMatUnpack(S_degr_sym, S_degr);
  RatelMatMatMult(1.0, F, S_degr, P_degr);

  //----- Update stored
  RatelStoredValuesPack(Q, i, 0, 1, &phi, stored);
  RatelStoredValuesPack(Q, i, 1, 6, (CeedScalar *)E_sym, stored);
  CeedScalar value_for_init = 0;

  RatelStoredValuesPack(Q, i, 7, 1, &value_for_init, stored);
  RatelStoredValuesPack(Q, i, 8, 9, (CeedScalar *)grad_u, stored);

  // Initialize state variable and damage component to zero
  state_out[i] = value_for_init;
  v[3][i]      = 0;
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute platens jacobian for Neo-hookean hyperelasticity initial conf.

  @param[in]   ctx      QFunction context, holding `RatelElasticityDamageParams`
  @param[in]   Q        Number of quadrature points
  @param[in]   i        Index
  @param[in]   in       Input arrays
                              - 0 - volumetric qdata
                              - 2 - Stored values
                              - 3 - du (four components: 3 displacement components, 1 damage field)
                              - 4 - du_g gradient of du with respect to reference coordinates
  @param[out]  out      Output arrays
                              - 0 - initializing dv
  @param[out]  dXdx     dXdx
  @param[out]  dP_degr  Linearization of degraded stress tensor

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int ElasticityDamagePlatenJacobian_NeoHookeanInitial(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in,
                                                                           CeedScalar *const *out, CeedScalar dXdx[3][3], CeedScalar dP_degr[3][3]) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA]  = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*stored)              = in[2];
  const CeedScalar(*du)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[3];
  const CeedScalar(*du_g)[4][CEED_Q_VLA] = (const CeedScalar(*)[4][CEED_Q_VLA])in[4];

  // Outputs
  CeedScalar(*dv)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Context
  const RatelElasticityDamageParams *context     = (RatelElasticityDamageParams *)ctx;
  const CeedScalar                   mu          = context->mu;
  const CeedScalar                   bulk        = context->bulk;
  const CeedScalar                   eta         = context->residual_stiffness;
  const bool                         use_offdiag = context->use_offdiagonal;

  CeedScalar J_dVdJ, J2_d2VdJ2, phi, E_sym[6], grad_u[3][3];
  CeedScalar grad_du[3][3], S_degr_sym[6], S_degr[3][3], dS_degr_sym[6], dS_degr[3][3];

  // Read spatial derivatives of increment u; d(du)/dX = d(du)/dX
  const CeedScalar ddudX[3][3] = {
      {du_g[0][0][i], du_g[1][0][i], du_g[2][0][i]},
      {du_g[0][1][i], du_g[1][1][i], du_g[2][1][i]},
      {du_g[0][2][i], du_g[1][2][i], du_g[2][2][i]}
  };

  // Extract Qdata
  RatelQdataUnpack(Q, i, q_data, dXdx);

  // Compute grad_du = ddu/dX * dX/dx
  RatelMatMatMult(1.0, ddudX, dXdx, grad_du);

  // Unpack stored values
  RatelStoredValuesUnpack(Q, i, 0, 1, stored, &phi);
  RatelStoredValuesUnpack(Q, i, 1, 6, stored, E_sym);
  RatelStoredValuesUnpack(Q, i, 8, 9, stored, (CeedScalar *)grad_u);

  // Deformation Gradient : F = I + grad_u
  const CeedScalar F[3][3] = {
      {grad_u[0][0] + 1, grad_u[0][1],     grad_u[0][2]    },
      {grad_u[1][0],     grad_u[1][1] + 1, grad_u[1][2]    },
      {grad_u[2][0],     grad_u[2][1],     grad_u[2][2] + 1}
  };

  // Compute J - 1
  const CeedScalar Jm1 = RatelMatDetAM1(grad_u);

  // Volumetric function and derivatives
  VolumetricFunctionAndDerivatives(Jm1, NULL, &J_dVdJ, &J2_d2VdJ2);

  // Compute S_degr
  ComputeDegradedSecondKirchhoffStress_NeoHookean(J_dVdJ, mu, bulk, eta, phi, Jm1, E_sym, S_degr_sym);
  RatelSymmetricMatUnpack(S_degr_sym, S_degr);

  // Retrieve dphi
  const CeedScalar dphi = du[3][i];

  // Compute dS_degr
  ComputeDegradedSecondKirchhoffStress_NeoHookean_fwd(J_dVdJ, J2_d2VdJ2, 2 * mu, bulk, eta, use_offdiag, phi, Jm1, F, grad_du, E_sym, dphi,
                                                      dS_degr_sym);
  RatelSymmetricMatUnpack(dS_degr_sym, dS_degr);

  // dP_degr
  RatelMatMatMultPlusMatMatMult(grad_du, S_degr, F, dS_degr, dP_degr);

  // Initialize damage component to zero
  dv[3][i] = 0;
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute platen residual for Neo-hookean hyperelasticity initial conf.

  @param[in]   ctx  QFunction context, holding `RatelBCPlatenParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(PlatenBCsResidualElasticityDamage_NeoHookeanInitial)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return PlatenBCs(ctx, Q, ElasticityDamagePlatenResidual_NeoHookeanInitial, !!NUM_COMPONENTS_STATE_NeoHookean_Damage,
                   !!NUM_COMPONENTS_STORED_NeoHookeanInitial_Damage, NUM_ACTIVE_FIELD_EVAL_MODES_NeoHookean_Damage, in, out);
}

/**
  @brief Evaluate platen Jacobian for Neo-hookean hyperelasticity initial conf.

  @param[in]   ctx  QFunction context, holding `RatelBCPlatenParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(PlatenBCsJacobianElasticityDamage_NeoHookeanInitial)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return PlatenBCs_Jacobian(ctx, Q, ElasticityDamagePlatenJacobian_NeoHookeanInitial, !!NUM_COMPONENTS_STATE_NeoHookean_Damage,
                            !!NUM_COMPONENTS_STORED_NeoHookeanInitial_Damage, NUM_ACTIVE_FIELD_EVAL_MODES_NeoHookean_Damage, in, out);
}
#define FLOPS_JACOBIAN_NeoHookeanInitial_Damage_Platen \
  (FLOPS_MatMatMult + FLOPS_MatDetAM1 + FLOPS_S_IsochoricNeoHookean + FLOPS_dS_IsochoricNeoHookean + FLOPS_MatMatMultPlusMatMatMult)

/// @}
