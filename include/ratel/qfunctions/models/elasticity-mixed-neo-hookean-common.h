/// @file
/// Ratel mixed neo-Hookean hyperelasticity at finite strain common QFunction source
#pragma once

#include <ceed/types.h>
#ifndef CEED_RUNNING_JIT_PASS
#include <math.h>
#endif

#include "../../models/mixed-neo-hookean.h"                  // IWYU pragma: export
#include "../utils.h"                                        // IWYU pragma: export
#include "elasticity-isochoric-stress-neo-hookean-common.h"  // IWYU pragma: export
#include "elasticity-mixed-diagnostic-common.h"              // IWYU pragma: export
#include "elasticity-mixed-volumetric-stress-common.h"       // IWYU pragma: export
#include "elasticity-volumetric-energy-common.h"             // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

#define FLOPS_Tau_MixedNeoHookean (FLOPS_Tau_vol_mixed + FLOPS_Tau_iso_NeoHookean + 3)
#define FLOPS_S_MixedNeoHookean (FLOPS_S_iso_NeoHookean + FLOPS_S_vol_mixed)
#define FLOPS_dS_MixedNeoHookean (1 + FLOPS_dS_vol_mixed + FLOPS_dS_iso_NeoHookean + FLOPS_MatMatMatAddSymmetric)
#define FLOPS_FdSFTranspose_MixedNeoHookean (FLOPS_FdSFTranspose_iso_NeoHookean + FLOPS_FdSFTranspose_vol_mixed + FLOPS_MatMatAddSymmetric)

/**
  @brief Compute Kirchoff tau for mixed neo-Hookean hyperelasticity.

 `tau = [bulk_primal * J dV/dJ - p J] I + 2 mu J^{-2/3} e_dev`

  @param[in]   J_dVdJ       J dV/dJ
  @param[in]   bulk_primal  Primal bulk modulus
  @param[in]   two_mu       Two times the shear modulus
  @param[in]   p            Pressure
  @param[in]   Jm1          Determinant of deformation gradient - 1
  @param[in]   J_pow        J^{-2/3}
  @param[in]   e_sym        Green Euler strain, in symmetric representation
  @param[out]  tau_sym      Kirchoff tau, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelKirchhoffTau_MixedNeoHookean(CeedScalar J_dVdJ, CeedScalar bulk_primal, CeedScalar two_mu, CeedScalar p,
                                                            CeedScalar Jm1, CeedScalar J_pow, const CeedScalar e_sym[6], CeedScalar tau_sym[6]) {
  CeedScalar tau_vol_sym;

  // -- [bulk_primal * J dV/dJ - p J] I
  RatelVolumetricKirchhoffTau_Mixed(J_dVdJ, bulk_primal, Jm1, p, &tau_vol_sym);

  // -- 2 mu J^{-2/3} e_dev
  RatelIsochoricKirchhoffTau_NeoHookean(two_mu, J_pow, e_sym, tau_sym);

  tau_sym[0] += tau_vol_sym;
  tau_sym[1] += tau_vol_sym;
  tau_sym[2] += tau_vol_sym;
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute second Kirchoff stress for mixed neo-Hookean hyperelasticity.

 `S = S_iso + S_vol`

  @param[in]   J_dVdJ        J dV/dJ
  @param[in]   bulk_primal   Primal bulk modulus
  @param[in]   two_mu        Two times the shear modulus
  @param[in]   p             Pressure
  @param[in]   Jm1           Determinant of deformation gradient - 1
  @param[in]   C_inv_sym     Inverse of right Cauchy-Green tensor, in symmetric representation
  @param[in]   E_sym         Green Lagrange strain, in symmetric representation
  @param[out]  S_sym         Second Kirchoff stress, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelSecondKirchhoffStress_MixedNeoHookean(CeedScalar J_dVdJ, CeedScalar bulk_primal, CeedScalar two_mu, CeedScalar p,
                                                                     CeedScalar Jm1, const CeedScalar C_inv_sym[6], const CeedScalar E_sym[6],
                                                                     CeedScalar S_sym[6]) {
  CeedScalar       S_iso_sym[6], S_vol_sym[6];
  const CeedScalar J = Jm1 + 1.;

  // Compute S_iso
  RatelIsochoricSecondKirchhoffStress_NeoHookean(two_mu, J, C_inv_sym, E_sym, S_iso_sym);

  // Compute S_vol
  RatelVolumetricSecondKirchhoffStress_Mixed(J_dVdJ, bulk_primal, p, Jm1, C_inv_sym, S_vol_sym);

  // S = S_vol + S_iso
  RatelMatMatAddSymmetric(1.0, S_vol_sym, 1.0, S_iso_sym, S_sym);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute derivative of second Kirchoff stress for mixed neo-Hookean hyperelasticity.

 `dS = dS_iso + dS_vol`

  @param[in]   J_dVdJ            J dV/dJ
  @param[in]   J2_d2VdJ2         J^2 d^2V/dJ^2
  @param[in]   bulk_primal       Primal bulk modulus
  @param[in]   mu                Shear modulus
  @param[in]   p                 Pressure
  @param[in]   dp                Increment of Pressure
  @param[in]   Jm1               Determinant of deformation gradient - 1
  @param[in]   F                 Deformation gradient
  @param[in]   grad_du           Gradient of incremental change in u
  @param[in]   C_inv_sym         Inverse of right Cauchy-Green tensor, in symmetric representation
  @param[in]   E_sym             Green Lagrange strain, in symmetric representation
  @param[out]  dS_sym            Derivative of second Kirchoff stress, in symmetric representation
  @param[out]  Cinv_contract_dE  `C_inv : dE`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelSecondKirchhoffStress_MixedNeoHookean_fwd(CeedScalar J_dVdJ, CeedScalar J2_d2VdJ2, CeedScalar bulk_primal,
                                                                         CeedScalar mu, CeedScalar p, CeedScalar dp, CeedScalar Jm1,
                                                                         const CeedScalar F[3][3], const CeedScalar grad_du[3][3],
                                                                         const CeedScalar C_inv_sym[6], const CeedScalar E_sym[6],
                                                                         CeedScalar dS_sym[6], CeedScalar *Cinv_contract_dE) {
  CeedScalar dC_inv_sym[6], dS_vol_sym[6], dS_iso_sym[6];

  // dS_iso
  RatelIsochoricSecondKirchhoffStress_NeoHookean_fwd(mu, Jm1 + 1., F, grad_du, C_inv_sym, E_sym, dC_inv_sym, dS_iso_sym, Cinv_contract_dE);

  // dS_vol
  RatelVolumetricSecondKirchhoffStress_Mixed_fwd(J_dVdJ, J2_d2VdJ2, bulk_primal, p, dp, Jm1, *Cinv_contract_dE, C_inv_sym, dC_inv_sym, dS_vol_sym);

  // dS = dS_iso + dS_vol;
  RatelMatMatAddSymmetric(1, dS_vol_sym, 1.0, dS_iso_sym, dS_sym);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute F*dS*F^T for mixed neo-Hookean hyperelasticity in current configuration.

  @param[in]   J_dVdJ          J dV/dJ
  @param[in]   J2_d2VdJ2       J^2 d^2V/dJ^2
  @param[in]   bulk_primal     Primal bulk modulus
  @param[in]   mu              Shear modulus
  @param[in]   p               Pressure
  @param[in]   dp              Increment of Pressure
  @param[in]   Jm1             Determinant of deformation gradient - 1
  @param[in]   J_pow           J^{-2/3}
  @param[in]   grad_du         Gradient of incremental change in u
  @param[in]   e_sym           Green Euler strain, in symmetric representation
  @param[out]  FdSFTranspose   F*dS*F^T needed for computing df1 in current configuration
  @param[out]  trace_depsilon  `trace(depsilon)`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelComputeFdSFTranspose_MixedNeoHookean(CeedScalar J_dVdJ, CeedScalar J2_d2VdJ2, CeedScalar bulk_primal, CeedScalar mu,
                                                                    CeedScalar p, CeedScalar dp, CeedScalar Jm1, CeedScalar J_pow,
                                                                    const CeedScalar grad_du[3][3], const CeedScalar e_sym[6],
                                                                    CeedScalar FdSFTranspose[3][3], CeedScalar *trace_depsilon) {
  CeedScalar depsilon_sym[6], FdSFTranspose_vol_sym[6], FdSFTranspose_iso_sym[6], FdSFTranspose_sym[6];

  RatelComputeFdSFTransposeIsochoric_NeoHookean(mu, J_pow, grad_du, e_sym, depsilon_sym, FdSFTranspose_iso_sym, trace_depsilon);
  RatelComputeFdSFTransposeVolumetric_Mixed(J_dVdJ, J2_d2VdJ2, bulk_primal, p, dp, Jm1, *trace_depsilon, depsilon_sym, FdSFTranspose_vol_sym);

  // F*dS*F^T = F*dS_vol*F^T + F*dS_iso*F^T
  RatelMatMatAddSymmetric(1.0, FdSFTranspose_vol_sym, 1.0, FdSFTranspose_iso_sym, FdSFTranspose_sym);
  RatelSymmetricMatUnpack(FdSFTranspose_sym, FdSFTranspose);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute strain energy for mixed neo-Hookean model.

 `psi = 0.5 mu (I1_bar - 3) + bulk * V(J)`

  @param[in]   V              V(J)
  @param[in]   bulk           Bulk modulus
  @param[in]   mu             Shear modulus
  @param[in]   Jm1            Determinant of deformation gradient - 1
  @param[in]   trace_strain   Trace of Green Lagrange or Green euler strain tensor (E or e)
  @param[out]  strain_energy  Strain energy for mixed Neo-Hookean model

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelStrainEnergy_MixedNeoHookean(CeedScalar V, CeedScalar bulk, CeedScalar mu, CeedScalar Jm1, CeedScalar trace_strain,
                                                            CeedScalar *strain_energy) {
  const CeedScalar J  = Jm1 + 1.;
  const CeedScalar I1 = 2 * trace_strain + 3., J_pow = pow(J, -2. / 3.);
  const CeedScalar I1_bar = J_pow * I1;

  // Strain energy psi for mixed Neo-Hookean
  *strain_energy = (0.5 * mu * (I1_bar - 3.) + bulk * V);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute strain energy for mixed neo-Hookean hyperelasticity

  @param[in]   ctx  QFunction context, holding `RatelMixedNeoHookeanElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - gradient of u with respect to reference coordinates
  @param[out]  out  Output arrays
                      - 0 - strain energy

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(StrainEnergy_MixedNeoHookean)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*energy) = (CeedScalar(*))out[0];

  // Context
  const RatelMixedNeoHookeanElasticityParams *context = (RatelMixedNeoHookeanElasticityParams *)ctx;
  const CeedScalar                            mu      = context->mu;
  const CeedScalar                            bulk    = context->bulk;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar dXdx[3][3], grad_u[3][3], E_sym[6], V, strain_energy;

    // Qdata
    const CeedScalar wdetJ = q_data[0][i];

    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Read spatial derivatives of u; du/dX
    CeedScalar dudX[3][3];
    RatelGradUnpack(Q, i, ug, dudX);

    // Compute grad_u = du/dX * dX/dx
    // X is ref coordinate [-1,1]^3; x is physical coordinate
    RatelMatMatMult(1.0, dudX, dXdx, grad_u);

    // E - Euler strain tensor
    RatelGreenLagrangeStrain(grad_u, E_sym);
    const CeedScalar Jm1  = RatelMatDetAM1(grad_u);
    const CeedScalar tr_E = RatelMatTraceSymmetric(E_sym);

    VolumetricFunctionAndDerivatives(Jm1, &V, NULL, NULL);
    RatelStrainEnergy_MixedNeoHookean(V, bulk, mu, Jm1, tr_E, &strain_energy);

    // Strain energy psi(e) for mixed Neo-Hookean
    energy[i] = strain_energy * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute projected diagnostic stress, and strain energy invariants values for isochoric Neo-Hookean hyperelasticity

  @param[in]   ctx            QFunction context, holding `RatelNeoHookeanElasticityParams`
  @param[in]   p              Pressure field
  @param[in]   Jm1            Determinant of deformation gradient - 1
  @param[in]   V              V(J); volumetric energy
  @param[in]   J_dVdJ         J dV/dJ
  @param[in]   grad_u         Gradient of incremental change in u
  @param[out]  sigma_sym      Cauchy stress tensor in symmetric representation
  @param[out]  strain_energy  Strain energy
  @param[out]  trace_e        Trace of strain tensor e
  @param[out]  trace_e2       Trace of strain tensor e*e

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int DiagnosticStress_MixedNeoHookean(void *ctx, const CeedScalar p, const CeedScalar Jm1, const CeedScalar V,
                                                           const CeedScalar J_dVdJ, CeedScalar grad_u[3][3], CeedScalar sigma_sym[6],
                                                           CeedScalar *strain_energy, CeedScalar *trace_e, CeedScalar *trace_e2) {
  // Context
  const RatelMixedNeoHookeanElasticityParams *context     = (RatelMixedNeoHookeanElasticityParams *)ctx;
  const CeedScalar                            mu          = context->mu;
  const CeedScalar                            two_mu      = context->two_mu;
  const CeedScalar                            bulk        = context->bulk;
  const CeedScalar                            bulk_primal = context->bulk_primal;

  CeedScalar e_sym[6], tau_sym[6];

  const CeedScalar J_pow = pow(Jm1 + 1., -2. / 3.);

  RatelGreenEulerStrain(grad_u, e_sym);
  RatelKirchhoffTau_MixedNeoHookean(J_dVdJ, bulk_primal, two_mu, p, Jm1, J_pow, e_sym, tau_sym);
  for (CeedInt j = 0; j < 6; j++) sigma_sym[j] = tau_sym[j] / (Jm1 + 1.);

  // Strain tensor invariants
  *trace_e  = RatelMatTraceSymmetric(e_sym);
  *trace_e2 = RatelMatMatContractSymmetric(1.0, e_sym, e_sym);

  // Strain energy
  RatelStrainEnergy_MixedNeoHookean(V, bulk, mu, Jm1, *trace_e, strain_energy);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute projected diagnostic values for mixed neo-Hookean hyperelasticity

  @param[in]   ctx  QFunction context, holding `RatelMixedNeoHookeanElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - u
                      - 2 - gradient of u with respect to reference coordinates
                      - 3 - p
  @param[out]  out  Output arrays
                      - 0 - projected diagnostic values

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(Diagnostic_MixedNeoHookean)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return MixedElasticityDiagnostic(ctx, Q, DiagnosticStress_MixedNeoHookean, in, out);
}

/// @}
