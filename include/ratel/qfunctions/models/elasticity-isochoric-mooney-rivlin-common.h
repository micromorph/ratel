/// @file
/// Ratel isochoric Mooney-Rivlin hyperelasticity at finite strain common QFunction source
#pragma once

#include <ceed/types.h>
#ifndef CEED_RUNNING_JIT_PASS
#include <math.h>
#endif

#include "../../models/elasticity-linear.h"                    // IWYU pragma: export
#include "../../models/mooney-rivlin.h"                        // IWYU pragma: export
#include "../utils.h"                                          // IWYU pragma: export
#include "elasticity-diagnostic-common.h"                      // IWYU pragma: export
#include "elasticity-isochoric-stress-mooney-rivlin-common.h"  // IWYU pragma: export
#include "elasticity-volumetric-energy-common.h"               // IWYU pragma: export
#include "elasticity-volumetric-stress-common.h"               // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

#define FLOPS_Tau_IsochoricMooneyRivlin (FLOPS_Tau_vol + FLOPS_Tau_iso_MooneyRivlin + 3)
#define FLOPS_S_IsochoricMooneyRivlin (FLOPS_S_iso_MooneyRivlin + FLOPS_S_vol)
#define FLOPS_dS_IsochoricMooneyRivlin (1 + FLOPS_dS_vol + FLOPS_dS_iso_MooneyRivlin + FLOPS_MatMatMatAddSymmetric)
#define FLOPS_FdSFTranspose_IsochoricMooneyRivlin (FLOPS_FdSFTranspose_iso_MooneyRivlin + FLOPS_FdSFTranspose_vol + FLOPS_MatMatAddSymmetric)

/**
  @brief Compute Kirchoff tau for isochoric Mooney-Rivlin hyperelasticity.

  @param[in]   J_dVdJ    J dV/dJ
  @param[in]   bulk      Bulk modulus
  @param[in]   mu_1      First Mooney-Rivlin parameter
  @param[in]   two_mu_2  Two times the second Mooney-Rivlin parameter
  @param[in]   J_pow     J^{-2/3}
  @param[in]   e_sym     Green Euler strain, in symmetric representation
  @param[out]  tau_sym   Kirchoff tau, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelKirchhoffTau_IsochoricMooneyRivlin(CeedScalar J_dVdJ, CeedScalar bulk, CeedScalar mu_1, CeedScalar two_mu_2,
                                                                  CeedScalar J_pow, const CeedScalar e_sym[6], CeedScalar tau_sym[6]) {
  CeedScalar tau_vol_sym;

  RatelVolumetricKirchhoffTau(J_dVdJ, bulk, &tau_vol_sym);

  RatelIsochoricKirchhoffTau_MooneyRivlin(mu_1, two_mu_2, J_pow, e_sym, tau_sym);

  tau_sym[0] += tau_vol_sym;
  tau_sym[1] += tau_vol_sym;
  tau_sym[2] += tau_vol_sym;
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute second Kirchoff stress for isochoric Mooney-Rivlin hyperelasticity.

  @param[in]   J_dVdJ     J dV/dJ
  @param[in]   bulk       Bulk modulus
  @param[in]   mu_1       First Mooney-Rivlin parameter
  @param[in]   two_mu_2   Two times the second Mooney-Rivlin parameter
  @param[in]   Jm1        Determinant of deformation gradient - 1
  @param[in]   C_inv_sym  Inverse of right Cauchy-Green tensor, in symmetric representation
  @param[in]   E_sym      Green Lagrange strain, in symmetric representation
  @param[out]  S_sym      Second Kirchoff stress, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelSecondKirchhoffStress_IsochoricMooneyRivlin(CeedScalar J_dVdJ, CeedScalar bulk, CeedScalar mu_1, CeedScalar two_mu_2,
                                                                           CeedScalar Jm1, const CeedScalar C_inv_sym[6], const CeedScalar E_sym[6],
                                                                           CeedScalar S_sym[6]) {
  CeedScalar       S_iso_sym[6], S_vol_sym[6];
  const CeedScalar J = Jm1 + 1.;

  // Compute S_iso
  RatelIsochoricSecondKirchhoffStress_MooneyRivlin(mu_1, two_mu_2, J, C_inv_sym, E_sym, S_iso_sym);

  // Compute S_vol
  RatelVolumetricSecondKirchhoffStress(J_dVdJ, bulk, C_inv_sym, S_vol_sym);

  // S = S_vol + S_iso
  RatelMatMatAddSymmetric(1.0, S_vol_sym, 1.0, S_iso_sym, S_sym);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute derivative of second Kirchoff stress for isochoric Mooney-Rivlin hyperelasticity.

  @param[in]   J_dVdJ      J dV/dJ
  @param[in]   J2_d2VdJ2   J^2 d^2V/dJ^2
  @param[in]   bulk        Bulk modulus
  @param[in]   mu_1        First Mooney-Rivlin parameter
  @param[in]   two_mu_2    Two times the second Mooney-Rivlin parameter
  @param[in]   Jm1         Determinant of deformation gradient - 1
  @param[in]   F           Deformation gradient
  @param[in]   grad_du     Gradient of incremental change in u
  @param[in]   C_inv_sym   Inverse of right Cauchy-Green tensor, in symmetric representation
  @param[in]   E_sym       Green Lagrange strain, in symmetric representation
  @param[out]  dS_sym      Derivative of second Kirchoff stress, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelSecondKirchhoffStress_IsochoricMooneyRivlin_fwd(CeedScalar J_dVdJ, CeedScalar J2_d2VdJ2, CeedScalar bulk,
                                                                               CeedScalar mu_1, CeedScalar two_mu_2, CeedScalar Jm1,
                                                                               const CeedScalar F[3][3], const CeedScalar grad_du[3][3],
                                                                               const CeedScalar C_inv_sym[6], const CeedScalar E_sym[6],
                                                                               CeedScalar dS_sym[6]) {
  CeedScalar dC_inv_sym[6], dS_vol_sym[6], dS_iso_sym[6], Cinv_contract_dE;

  // dS_iso
  RatelIsochoricSecondKirchhoffStress_MooneyRivlin_fwd(mu_1, two_mu_2, Jm1 + 1., F, grad_du, C_inv_sym, E_sym, dC_inv_sym, dS_iso_sym,
                                                       &Cinv_contract_dE);

  // dS_vol
  RatelVolumetricSecondKirchhoffStress_fwd(J_dVdJ, J2_d2VdJ2, bulk, Cinv_contract_dE, C_inv_sym, dC_inv_sym, dS_vol_sym);

  // dS = dS_iso + dS_vol;
  RatelMatMatAddSymmetric(1, dS_vol_sym, 1.0, dS_iso_sym, dS_sym);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute F*dS*F^T for isochoric Mooney-Rivlin hyperelasticity in current configuration.

  @param[in]   J_dVdJ        J dV/dJ
  @param[in]   J2_d2VdJ2     J^2 d^2V/dJ^2
  @param[in]   bulk          Bulk modulus
  @param[in]   mu_1          First Mooney-Rivlin parameter
  @param[in]   two_mu_2      Two times the second Mooney-Rivlin parameter
  @param[in]   J_pow         J^{-2/3}
  @param[in]   grad_du       Gradient of incremental change in u
  @param[in]   e_sym         Green Euler strain, in symmetric representation
  @param[out]  FdSFTranspose F*dS*F^T needed for computing df1 in current configuration

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelComputeFdSFTranspose_IsochoricMooneyRivlin(CeedScalar J_dVdJ, CeedScalar J2_d2VdJ2, CeedScalar bulk, CeedScalar mu_1,
                                                                          CeedScalar two_mu_2, CeedScalar J_pow, const CeedScalar grad_du[3][3],
                                                                          const CeedScalar e_sym[6], CeedScalar FdSFTranspose[3][3]) {
  CeedScalar depsilon_sym[6], FdSFTranspose_vol_sym[6], FdSFTranspose_iso_sym[6], FdSFTranspose_sym[6], trace_depsilon;

  RatelComputeFdSFTransposeIsochoric_MooneyRivlin(mu_1, two_mu_2, J_pow, grad_du, e_sym, depsilon_sym, FdSFTranspose_iso_sym, &trace_depsilon);
  RatelComputeFdSFTransposeVolumetric(J_dVdJ, J2_d2VdJ2, bulk, trace_depsilon, depsilon_sym, FdSFTranspose_vol_sym);

  // F*dS*F^T = F*dS_vol*F^T + F*dS_iso*F^T
  RatelMatMatAddSymmetric(1.0, FdSFTranspose_vol_sym, 1.0, FdSFTranspose_iso_sym, FdSFTranspose_sym);
  RatelSymmetricMatUnpack(FdSFTranspose_sym, FdSFTranspose);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute strain energy for isochoric Mooney-Rivlin model.

 `psi = 0.5 mu_1 (I1_bar - 3) + 0.5 mu_2 (I2_bar - 3) + bulk * V(J)`

  @param[in]   V              V(J)
  @param[in]   bulk           Bulk modulus
  @param[in]   mu_1           First Mooney-Rivlin parameter
  @param[in]   mu_2           Second Mooney-Rivlin parameter
  @param[in]   Jm1            Determinant of deformation gradient - 1
  @param[in]   trace_strain   Trace of Green Lagrange or Green euler strain tensor (E or e)
  @param[in]   trace_strain2  Trace of E^2 (or e^2)
  @param[out]  strain_energy  Strain energy for isochoric Mooney-Rivlin model

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelStrainEnergy_IsochoricMooneyRivlin(CeedScalar V, CeedScalar bulk, CeedScalar mu_1, CeedScalar mu_2, CeedScalar Jm1,
                                                                  CeedScalar trace_strain, CeedScalar trace_strain2, CeedScalar *strain_energy) {
  const CeedScalar J      = Jm1 + 1.;
  const CeedScalar I_2e   = 0.5 * (trace_strain * trace_strain - trace_strain2);
  const CeedScalar I_2b   = 3 + 4 * trace_strain + 4 * I_2e;
  const CeedScalar J_23   = pow(J, -2. / 3.);
  const CeedScalar J_43   = pow(J, -4. / 3.);
  const CeedScalar I1_bar = J_23 * (3 + 2 * trace_strain);
  const CeedScalar I2_bar = J_43 * I_2b;

  // Strain energy psi for isochoric Mooney-Rivlin
  *strain_energy = (0.5 * mu_1 * (I1_bar - 3.) + 0.5 * mu_2 * (I2_bar - 3.) + bulk * V);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute strain energy for isochoric Mooney-Rivlin hyperelasticity

  @param[in]   ctx  QFunction context, holding `RatelMooneyRivlinElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - gradient of u with respect to reference coordinates
  @param[out]  out  Output arrays
                      - 0 - strain energy

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(StrainEnergy_IsochoricMooneyRivlin)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*energy) = (CeedScalar(*))out[0];

  // Context
  const RatelMooneyRivlinElasticityParams *context = (RatelMooneyRivlinElasticityParams *)ctx;
  const CeedScalar                         bulk    = context->bulk;
  const CeedScalar                         mu_1    = context->mu_1;
  const CeedScalar                         mu_2    = context->mu_2;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar dXdx[3][3], grad_u[3][3], e_sym[6], V, strain_energy;

    // Qdata
    const CeedScalar wdetJ = q_data[0][i];

    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Read spatial derivatives of u; du/dX
    CeedScalar dudX[3][3];
    RatelGradUnpack(Q, i, ug, dudX);

    // Compute grad_u = du/dX * dX/dx
    // X is ref coordinate [-1,1]^3; x is physical coordinate
    RatelMatMatMult(1.0, dudX, dXdx, grad_u);

    // e - Euler strain tensor
    RatelGreenEulerStrain(grad_u, e_sym);
    const CeedScalar trace_e  = RatelMatTraceSymmetric(e_sym);
    const CeedScalar trace_e2 = RatelMatMatContractSymmetric(1.0, e_sym, e_sym);
    const CeedScalar Jm1      = RatelMatDetAM1(grad_u);

    VolumetricFunctionAndDerivatives(Jm1, &V, NULL, NULL);
    RatelStrainEnergy_IsochoricMooneyRivlin(V, bulk, mu_1, mu_2, Jm1, trace_e, trace_e2, &strain_energy);

    // Strain energy psi(e) for isochoric mooney-rivlin
    energy[i] = strain_energy * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute projected diagnostic stress, and strain energy invariants values for isochoric Mooney-Rivlin hyperelasticity

  @param[in]   ctx            QFunction context, holding `RatelMooneyRivlinElasticityParams`
  @param[in]   Jm1            Determinant of deformation gradient - 1
  @param[in]   V              V(J); volumetric energy
  @param[in]   J_dVdJ         J dV/dJ
  @param[in]   grad_u         Gradient of incremental change in u
  @param[out]  sigma_sym      Cauchy stress tensor in symmetric representation
  @param[out]  strain_energy  Strain energy
  @param[out]  trace_e        Trace of strain tensor e
  @param[out]  trace_e2       Trace of strain tensor e*e

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int DiagnosticStress_IsochoricMooneyRivlin(void *ctx, const CeedScalar Jm1, const CeedScalar V, const CeedScalar J_dVdJ,
                                                                 CeedScalar grad_u[3][3], CeedScalar sigma_sym[6], CeedScalar *strain_energy,
                                                                 CeedScalar *trace_e, CeedScalar *trace_e2) {
  // Context
  const RatelMooneyRivlinElasticityParams *context  = (RatelMooneyRivlinElasticityParams *)ctx;
  const CeedScalar                         bulk     = context->bulk;
  const CeedScalar                         mu_1     = context->mu_1;
  const CeedScalar                         mu_2     = context->mu_2;
  const CeedScalar                         two_mu_2 = context->two_mu_2;

  CeedScalar e_sym[6], tau_sym[6];

  const CeedScalar J_pow = pow(Jm1 + 1., -2. / 3.);

  RatelGreenEulerStrain(grad_u, e_sym);
  RatelKirchhoffTau_IsochoricMooneyRivlin(J_dVdJ, bulk, mu_1, two_mu_2, J_pow, e_sym, tau_sym);
  for (CeedInt j = 0; j < 6; j++) sigma_sym[j] = tau_sym[j] / (Jm1 + 1.);

  // Strain tensor invariants
  *trace_e  = RatelMatTraceSymmetric(e_sym);
  *trace_e2 = RatelMatMatContractSymmetric(1.0, e_sym, e_sym);

  // Strain energy
  RatelStrainEnergy_IsochoricMooneyRivlin(V, bulk, mu_1, mu_2, Jm1, *trace_e, *trace_e2, strain_energy);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute projected diagnostic values for isochoric Mooney-Rivlin hyperelasticity

  @param[in]   ctx  QFunction context, holding `RatelMooneyRivlinElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - u
                      - 2 - gradient of u with respect to reference coordinates
                      - 3 - p
  @param[out]  out  Output arrays
                      - 0 - projected diagnostic values

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(Diagnostic_IsochoricMooneyRivlin)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return ElasticityDiagnostic(ctx, Q, DiagnosticStress_IsochoricMooneyRivlin, in, out);
}

/// @}
