/// @file
/// Ratel linear elasticity QFunction source
#include <ceed/types.h>

#include "../../models/elasticity-linear.h"  // IWYU pragma: export
#include "../boundaries/platen-nitsche.h"    // IWYU pragma: export
#include "../utils.h"                        // IWYU pragma: export
#include "elasticity-common.h"               // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

#define NUM_COMPONENTS_STATE_Linear 0
#define NUM_COMPONENTS_STORED_Linear 0
#define NUM_ACTIVE_FIELD_EVAL_MODES_Linear 1

#define NUM_COMPONENTS_DIAGNOSTIC_Linear 16

// FLOPS_grad_du = FLOPS_MatMatMult
#define FLOPS_df1_Linear (FLOPS_MatMatMult + FLOPS_LinearStrain_fwd + FLOPS_MatTrace + FLOPS_LinearStress_fwd)
#define FLOPS_JACOBIAN_Linear (FLOPS_df1_Linear + FLOPS_dXdxwdetJ)

/**
  @brief Compute `Sigma` for linear elasticity

  @param[in]   ctx                   QFunction context, holding `RatelLinearElasticityParams`
  @param[in]   Q                     Number of quadrature points
  @param[in]   i                     Current quadrature point
  @param[in]   in                    Input arrays
                                       - 0 - volumetric qdata
                                       - 1 - gradient of u with respect to reference coordinates
  @param[out]  out                   Output arrays, unused
  @param[out]  dXdx                  Coordinate transformation
  @param[out]  f1                    `f1 = Sigma = lambda*trace(e)I + 2 mu e`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int f1_Linear(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in, CeedScalar *const *out,
                                    CeedScalar dXdx[3][3], CeedScalar f1[3][3]) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];

  // Context
  const RatelLinearElasticityParams *context = (RatelLinearElasticityParams *)ctx;
  const CeedScalar                   lambda  = context->lambda;
  const CeedScalar                   two_mu  = context->two_mu;

  CeedScalar grad_u[3][3], e_sym[6], sigma_sym[6];

  //  Qdata
  RatelQdataUnpack(Q, i, q_data, dXdx);

  // Read spatial derivatives of u; du/dX
  CeedScalar dudX[3][3];
  RatelGradUnpack(Q, i, ug, dudX);

  // Compute grad_u = du/dX * dX/dx
  // X is ref coordinate [-1,1]^3; x is physical coordinate
  RatelMatMatMult(1.0, dudX, dXdx, grad_u);

  // Compute Strain : e (epsilon)
  // e = 1/2 (grad u + (grad u)^T)
  RatelLinearStrain(grad_u, e_sym);

  // Compute Sigma = lambda*trace(e)I + 2*mu*e
  const CeedScalar trace_e = RatelMatTraceSymmetric(e_sym);
  RatelLinearStress(two_mu, lambda, trace_e, e_sym, sigma_sym);

  RatelSymmetricMatUnpack(sigma_sym, f1);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute linearization of `Sigma` for linear elasticity

  @param[in]   ctx                   QFunction context, holding `RatelLinearElasticityParams`
  @param[in]   Q                     Number of quadrature points
  @param[in]   i                     Current quadrature point
  @param[in]   in                    Input arrays
                                       - 0 - volumetric qdata
                                       - 1 - gradient of incremental change to u with respect to reference coordinates
  @param[out]  out                   Output arrays, unused
  @param[out]  dXdx                  Coordinate transformation
  @param[out]  df1                   `df1 = dSigma = lambda tr(de) I + 2 mu de`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int df1_Linear(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in, CeedScalar *const *out,
                                     CeedScalar dXdx[3][3], CeedScalar df1[3][3]) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*dug)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];

  // Context
  const RatelLinearElasticityParams *context = (RatelLinearElasticityParams *)ctx;
  const CeedScalar                   lambda  = context->lambda;
  const CeedScalar                   two_mu  = context->two_mu;

  CeedScalar grad_du[3][3], de_sym[6], dsigma_sym[6];

  // Qdata
  RatelQdataUnpack(Q, i, q_data, dXdx);

  // Read spatial derivatives of increment u; d(du)/dX = d(du)/dX
  CeedScalar ddudX[3][3];
  RatelGradUnpack(Q, i, dug, ddudX);

  // Compute grad_du = ddu/dX * dX/dx
  // X is ref coordinate [-1,1]^3; x is physical coordinate
  RatelMatMatMult(1.0, ddudX, dXdx, grad_du);

  // Compute Strain : de (epsilon)
  // de = 1/2 (grad_du + (grad_du)^T)
  RatelLinearStrain_fwd(grad_du, de_sym);

  // Compute dSigma = lambda*trace(de)I + 2*mu*de
  const CeedScalar trace_de = RatelMatTraceSymmetric(de_sym);
  RatelLinearStress_fwd(two_mu, lambda, trace_de, de_sym, dsigma_sym);

  RatelSymmetricMatUnpack(dsigma_sym, df1);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute residual for linear elasticity

  @param[in]   ctx  QFunction context, holding `RatelLinearElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityResidual_Linear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return ElasticityResidual(ctx, Q, f1_Linear, !!NUM_COMPONENTS_STATE_Linear, !!NUM_COMPONENTS_STORED_Linear, NUM_ACTIVE_FIELD_EVAL_MODES_Linear, in,
                            out);
}

/**
  @brief Evaluate Jacobian for linear elasticity

  @param[in]   ctx  QFunction context, holding `RatelLinearElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityJacobian_Linear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return ElasticityJacobian(ctx, Q, df1_Linear, !!NUM_COMPONENTS_STATE_Linear, !!NUM_COMPONENTS_STORED_Linear, NUM_ACTIVE_FIELD_EVAL_MODES_Linear, in,
                            out);
}

/**
  @brief Compute platen residual for linear elasticity

  @param[in]   ctx  QFunction context, holding `RatelBCPlatenParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(PlatenBCsResidual_Linear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return PlatenBCs(ctx, Q, f1_Linear, !!NUM_COMPONENTS_STATE_Linear, !!NUM_COMPONENTS_STORED_Linear, NUM_ACTIVE_FIELD_EVAL_MODES_Linear, in, out);
}

/**
  @brief Evaluate platen Jacobian for linear elasticity

  @param[in]   ctx  QFunction context, holding `RatelBCPlatenParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(PlatenBCsJacobian_Linear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return PlatenBCs_Jacobian(ctx, Q, df1_Linear, !!NUM_COMPONENTS_STATE_Linear, !!NUM_COMPONENTS_STORED_Linear, NUM_ACTIVE_FIELD_EVAL_MODES_Linear, in,
                            out);
}

/**
  @brief Compute strain energy for linear elasticity

  @param[in]   ctx  QFunction context, holding `RatelLinearElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - gradient of u with respect to reference coordinates
  @param[out]  out  Output arrays
                      - 0 - strain energy

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(StrainEnergy_Linear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*energy) = (CeedScalar(*))out[0];

  // Context
  const RatelLinearElasticityParams *context = (RatelLinearElasticityParams *)ctx;
  const CeedScalar                   mu      = context->mu;
  const CeedScalar                   lambda  = context->lambda;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar dXdx[3][3], grad_u[3][3], e_sym[6];

    // Qdata
    const CeedScalar wdetJ = q_data[0][i];
    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Read spatial derivatives of u; du/dX
    CeedScalar dudX[3][3];
    RatelGradUnpack(Q, i, ug, dudX);

    // Compute grad_u = du/dX * dX/dx
    // X is ref coordinate [-1,1]^3; x is physical coordinate
    RatelMatMatMult(1.0, dudX, dXdx, grad_u);

    // Compute Strain : e (epsilon)
    // e = 1/2 (grad u + (grad u)^T)
    RatelLinearStrain(grad_u, e_sym);

    // Strain energy = lambda/2 (trace(e))^2 + mu e:e
    // -- trace(e) = div(u) = Volumetric strain
    const CeedScalar trace_e = RatelMatTraceSymmetric(e_sym);

    // -- e:e = trace(e^2)
    const CeedScalar trace_e2 = RatelMatMatContractSymmetric(1.0, e_sym, e_sym);

    energy[i] = (lambda * trace_e * trace_e / 2. + mu * trace_e2) * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute projected diagnostic values for linear elasticity

  @param[in]   ctx  QFunction context, holding `RatelLinearElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - u
                      - 2 - gradient of u with respect to reference coordinates
  @param[out]  out  Output arrays
                      - 0 - projected diagnostic values

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(Diagnostic_Linear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[1];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[2];

  // Outputs
  CeedScalar(*diagnostic)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Context
  const RatelLinearElasticityParams *context = (RatelLinearElasticityParams *)ctx;
  const CeedScalar                   mu      = context->mu;
  const CeedScalar                   lambda  = context->lambda;
  const CeedScalar                   two_mu  = context->two_mu;
  const CeedScalar                   rho     = context->common_parameters[RATEL_COMMON_PARAMETER_RHO];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar dXdx[3][3], grad_u[3][3], e_sym[6], sigma_sym[6], sigma_dev_sym[6];

    // Qdata
    const CeedScalar wdetJ = q_data[0][i];
    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Read spatial derivatives of u; du/dX
    CeedScalar dudX[3][3];
    RatelGradUnpack(Q, i, ug, dudX);

    // Compute grad_u = du/dX * dX/dx
    // X is ref coordinate [-1,1]^3; x is physical coordinate
    RatelMatMatMult(1.0, dudX, dXdx, grad_u);

    // Compute Strain : e (epsilon)
    // e = 1/2 (grad u + (grad u)^T)
    RatelLinearStrain(grad_u, e_sym);

    // Compute Sigma = lambda*trace(e)I + 2*mu*e
    const CeedScalar trace_e = RatelMatTraceSymmetric(e_sym);
    RatelLinearStress(two_mu, lambda, trace_e, e_sym, sigma_sym);

    // Displacement
    diagnostic[0][i] = u[0][i];
    diagnostic[1][i] = u[1][i];
    diagnostic[2][i] = u[2][i];

    // Cauchy stress tensor
    diagnostic[3][i] = sigma_sym[0];
    diagnostic[4][i] = sigma_sym[5];
    diagnostic[5][i] = sigma_sym[4];
    diagnostic[6][i] = sigma_sym[1];
    diagnostic[7][i] = sigma_sym[3];
    diagnostic[8][i] = sigma_sym[2];

    // Hydrostatic pressure; p_h = -trace(sigma) / 3
    diagnostic[9][i] = -RatelMatTraceSymmetric(sigma_sym) / 3.;

    // Strain tensor invariants; trace(e)
    diagnostic[10][i]         = trace_e;
    // trace(e^2) = e:e
    const CeedScalar trace_e2 = RatelMatMatContractSymmetric(1.0, e_sym, e_sym);
    diagnostic[11][i]         = trace_e2;
    diagnostic[12][i]         = 1 + trace_e;

    // Strain energy
    diagnostic[13][i] = lambda * trace_e * trace_e / 2. + mu * trace_e2;

    // Compute von-Mises stress

    // -- Compute the the deviatoric part of Cauchy stress: sigma_dev = sigma - trace(sigma)/3
    const CeedScalar trace_sigma = RatelMatTraceSymmetric(sigma_sym);
    RatelMatDeviatoricSymmetric(trace_sigma, sigma_sym, sigma_dev_sym);

    // -- sigma_dev:sigma_dev
    const CeedScalar sigma_dev_contract = RatelMatMatContractSymmetric(1.0, sigma_dev_sym, sigma_dev_sym);

    // -- Compute von-Mises stress: sigma_von = sqrt(3/2 sigma_dev:sigma_dev)
    diagnostic[14][i] = sqrt(3. * sigma_dev_contract / 2.);

    // Compute mass density values

    // -- Set to 0 as default (rho should be 0.0 as defualt anyways)
    diagnostic[15][i] = 0.0;

    // -- if mass density is requested then compute mass density: rho/J
    if (rho > 0.0) diagnostic[15][i] = rho / diagnostic[12][i];

    // Quadrature weight
    for (CeedInt j = 0; j < NUM_COMPONENTS_DIAGNOSTIC_Linear; j++) diagnostic[j][i] *= wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute surface forces for linear elasticity

  @param[in]   ctx  QFunction context, holding `RatelLinearElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - gradient of u with respect to reference coordinates
  @param[out]  out  Output arrays
                      - 0 - surface force values

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(SurfaceForceCellToFace_Linear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*diagnostic)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Context
  const RatelLinearElasticityParams *context = (RatelLinearElasticityParams *)ctx;
  const CeedScalar                   lambda  = context->lambda;
  const CeedScalar                   two_mu  = context->two_mu;

  // Quadrature Point Loop
  for (CeedInt i = 0; i < Q; i++) {
    CeedScalar dXdx[3][3], grad_u[3][3], e_sym[6], sigma_sym[6], sigma[3][3];

    // Qdata
    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Normal
    const CeedScalar normal[3] = {q_data[10][i], q_data[11][i], q_data[12][i]};

    // Read spatial derivatives of u; du/dX
    CeedScalar dudX[3][3];
    RatelGradUnpack(Q, i, ug, dudX);

    // Compute grad_u = du/dX * dX/dx
    // X is ref coordinate [-1,1]^3; x is physical coordinate
    RatelMatMatMult(1.0, dudX, dXdx, grad_u);

    // Compute Strain : e (epsilon)
    // e = 1/2 (grad u + (grad u)^T)
    RatelLinearStrain(grad_u, e_sym);

    // Compute Sigma = lambda*trace(e)I + 2*mu*e
    const CeedScalar trace_e = RatelMatTraceSymmetric(e_sym);
    RatelLinearStress(two_mu, lambda, trace_e, e_sym, sigma_sym);

    RatelSymmetricMatUnpack(sigma_sym, sigma);

    // Compute sigma N
    for (CeedInt j = 0; j < 3; j++) diagnostic[j][i] = -q_data[0][i] * RatelDot3(sigma[j], normal);
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
