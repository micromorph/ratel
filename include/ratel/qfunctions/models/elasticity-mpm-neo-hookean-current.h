/// @file
/// Ratel neo-Hookean hyperelasticity at finite strain in current configuration QFunction source
#include <ceed/types.h>

#include "../../models/elasticity-linear.h"     // IWYU pragma: export
#include "../../models/neo-hookean.h"           // IWYU pragma: export
#include "../utils.h"                           // IWYU pragma: export
#include "elasticity-common.h"                  // IWYU pragma: export
#include "elasticity-mpm-neo-hookean-common.h"  // IWYU pragma: export
#include "elasticity-neo-hookean-common.h"      // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

#define NUM_COMPONENTS_STATE_MPM_NeoHookeanCurrent 0
#define NUM_COMPONENTS_STORED_MPM_NeoHookeanCurrent 16
#define NUM_ACTIVE_FIELD_EVAL_MODES_MPM_NeoHookeanCurrent 1
#define NUM_POINTS_FIELDS_MPM_NeoHookeanCurrent 2

// FLOPS_grad_du = FLOPS_MatMatMult
// FLOPS_grad_du_tau = FLOPS_MatMatMult
// FLOPS_grad_du_tau += flops of adding + lambda J^2 trace(depsilon)I to grad_du_tau
// 6 flops for creating coeff1
#define FLOPS_df1_MPM_NH_Current (2 * FLOPS_MatMatMult + FLOPS_FdSFTranspose_NeoHookean + FLOPS_MatMatAdd)
#define FLOPS_JACOBIAN_MPM_NeoHookeanCurrent (FLOPS_df1_MPM_NH_Current + FLOPS_dXdxwdetJ)

/**
  @brief Compute `tau` for neo-Hookean hyperelasticity in current configuration

  @param[in]   ctx                   QFunction context, holding `RatelNeoHookeanElasticityParams`
  @param[in]   Q                     Number of quadrature points
  @param[in]   i                     Current quadrature point
  @param[in]   in                    Input arrays
                                       - 0 - volumetric qdata
                                       - 1 - gradient of u with respect to reference coordinates
  @param[out]  out                   Output arrays
                                       - 0 - stored, dXdx, tau, and J - 1
  @param[out]  dXdx                  Coordinate transformation
  @param[out]  f1                    `f1 = tau`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int f1_MPM_NeoHookeanCurrent(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in, CeedScalar *const *out,
                                                   CeedScalar dXdx[3][3], CeedScalar f1[3][3]) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];
  const CeedScalar *mu                  = in[3];
  const CeedScalar *bulk                = in[4];

  // Outputs
  CeedScalar(*stored_values) = out[0];

  // Material properties
  const CeedScalar two_mu = 2 * mu[i];
  const CeedScalar lambda = bulk[i] - 2 * mu[i] / 3.;

  CeedScalar dXdx_initial[3][3], Grad_u[3][3], e_sym[6], tau_sym[6], F_inv[3][3], J_dVdJ;

  // -- Qdata
  // dXdx_initial = dX/dx_initial
  // X is natural coordinate sys OR Reference [-1, 1]^dim
  // x_initial is initial config coordinate system
  RatelQdataUnpack(Q, i, q_data, dXdx_initial);

  // Read spatial derivatives of u; du/dX
  CeedScalar dudX[3][3];
  RatelGradUnpack(Q, i, ug, dudX);

  // X is natural coordinate sys OR Reference system
  // x_initial is initial config coordinate system
  // Grad_u =du/dx_initial= du/dX * dX/dx_initial
  RatelMatMatMult(1.0, dudX, dXdx_initial, Grad_u);

  // Compute the Deformation Gradient : F = I + Grad_u
  const CeedScalar F[3][3] = {
      {Grad_u[0][0] + 1, Grad_u[0][1],     Grad_u[0][2]    },
      {Grad_u[1][0],     Grad_u[1][1] + 1, Grad_u[1][2]    },
      {Grad_u[2][0],     Grad_u[2][1],     Grad_u[2][2] + 1}
  };

  RatelGreenEulerStrain(Grad_u, e_sym);

  const CeedScalar Jm1 = RatelMatDetAM1(Grad_u);

  VolumetricFunctionAndDerivatives(Jm1, NULL, &J_dVdJ, NULL);
  RatelKirchhoffTau_NeoHookean(J_dVdJ, lambda, two_mu, e_sym, tau_sym);
  RatelSymmetricMatUnpack(tau_sym, f1);

  // Compute F^{-1}
  const CeedScalar detF = Jm1 + 1.;

  RatelMatInverse(F, detF, F_inv);

  // x is current config coordinate system
  // dXdx = dX/dx = dX/dx_initial * F^{-1}
  // Note that F^{-1} = dx_initial/dx
  RatelMatMatMult(1.0, dXdx_initial, F_inv, dXdx);

  // Store values
  RatelStoredValuesPack(Q, i, 0, 9, (CeedScalar *)dXdx, stored_values);
  RatelStoredValuesPack(Q, i, 9, 6, (CeedScalar *)tau_sym, stored_values);
  RatelStoredValuesPack(Q, i, 15, 1, &Jm1, stored_values);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute linearization `df1` for neo-Hookean hyperelasticity in current configuration

  @param[in]   ctx                   QFunction context, holding `RatelNeoHookeanElasticityParams`
  @param[in]   Q                     Number of quadrature points
  @param[in]   i                     Current quadrature point
  @param[in]   in                    Input arrays
                                       - 0 - volumetric qdata
                                       - 1 - stored dXdx, tau, and J - 1
                                       - 2 - gradient of incremental change to u with respect to reference coordinates
  @param[out]  out                   Output arrays, unused
  @param[out]  dXdx                  Coordinate transformation
  @param[out]  df1                   `df1 = dtau - tau * grad_du^T`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int df1_MPM_NeoHookeanCurrent(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in, CeedScalar *const *out,
                                                    CeedScalar dXdx[3][3], CeedScalar df1[3][3]) {
  // Inputs
  const CeedScalar(*stored_values)      = in[1];
  const CeedScalar(*dug)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[2];
  const CeedScalar *mu                  = in[4];
  const CeedScalar *bulk                = in[5];

  // Material properties
  const CeedScalar lambda = bulk[i] - 2 * mu[i] / 3.;

  CeedScalar grad_du[3][3], tau_sym[6], tau[3][3], grad_du_tau[3][3], FdSFTranspose[3][3], Jm1, J_dVdJ, J2_d2VdJ2;

  // Read spatial derivatives of increment u; d(du)/dX = d(du)/dX
  CeedScalar ddudX[3][3];
  RatelGradUnpack(Q, i, dug, ddudX);

  // Retrieve dXdx
  RatelStoredValuesUnpack(Q, i, 0, 9, stored_values, (CeedScalar *)dXdx);

  // Compute grad_du = ddu/dX * dX/dx
  // X is ref coordinate [-1,1]^3; x is physical coordinate in current configuration
  RatelMatMatMult(1.0, ddudX, dXdx, grad_du);
  // Retrieve tau and Jm1
  RatelStoredValuesUnpack(Q, i, 9, 6, stored_values, tau_sym);
  RatelStoredValuesUnpack(Q, i, 15, 1, stored_values, &Jm1);
  RatelSymmetricMatUnpack(tau_sym, tau);

  // Compute grad_du_tau = grad_du*tau
  RatelMatMatMult(1.0, grad_du, tau, grad_du_tau);
  // Compute F*dS*F^T
  VolumetricFunctionAndDerivatives(Jm1, NULL, &J_dVdJ, &J2_d2VdJ2);
  RatelComputeFdSFTranspose_NeoHookean(J_dVdJ, J2_d2VdJ2, lambda, mu[i], grad_du, FdSFTranspose);

  // df1 = dtau - tau * grad_du^T
  //     = grad_du*tau + F*dS*F^T
  RatelMatMatAdd(1.0, grad_du_tau, 1., FdSFTranspose, df1);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute residual for neo-Hookean hyperelasticity in current configuration

  @param[in]   ctx  QFunction context, holding `RatelNeoHookeanElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityResidual_MPM_NeoHookeanCurrent)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return ElasticityResidual(ctx, Q, f1_MPM_NeoHookeanCurrent, !!NUM_COMPONENTS_STATE_MPM_NeoHookeanCurrent,
                            !!NUM_COMPONENTS_STORED_MPM_NeoHookeanCurrent, NUM_ACTIVE_FIELD_EVAL_MODES_MPM_NeoHookeanCurrent, in, out);
}

/**
  @brief Evaluate Jacobian for neo-Hookean hyperelasticity in current configuration

  @param[in]   ctx  QFunction context, holding `RatelNeoHookeanElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityJacobian_MPM_NeoHookeanCurrent)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return ElasticityJacobian(ctx, Q, df1_MPM_NeoHookeanCurrent, !!NUM_COMPONENTS_STATE_MPM_NeoHookeanCurrent,
                            !!NUM_COMPONENTS_STORED_MPM_NeoHookeanCurrent,
                            NUM_ACTIVE_FIELD_EVAL_MODES_MPM_NeoHookeanCurrent + NUM_POINTS_FIELDS_MPM_NeoHookeanCurrent, in, out);
}

/// @}