/// @file
/// Ratel CEED BP1 QFunction source
#include <ceed/types.h>

#include "../utils.h"  // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

/**
  @brief Compute the residual for the CEED scalar BP problem

  @param[in]   ctx  QFunction context, unused
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - volumetric qdata
                      - 1 - interpolated values of u
  @param[out]  out  Output array
                      - 0 - action of the QFunction

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(Residual_CEED_BP1)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u)                  = in[1];

  // Outputs
  CeedScalar(*v) = out[0];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Qdata
    const CeedScalar wdetJ = q_data[0][i];

    v[i] = u[i] * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute the Jacobian for the CEED scalar BP problem

  @param[in]   ctx  QFunction context, unused
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - volumetric qdata
                      - 1 - interpolated values of du
  @param[out]  out  Output array
                      - 0 - action of the QFunction

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(Jacobian_CEED_BP1)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*du)                 = in[1];

  // Outputs
  CeedScalar(*dv) = out[0];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Qdata
    const CeedScalar wdetJ = q_data[0][i];

    dv[i] = du[i] * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute the diagnostic quantities for the CEED scalar BP problem

  @param[in]   ctx  QFunction context, unused
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - volumetric qdata
                      - 1 - u
  @param[out]  out  Output array
                      - 0 - diagnostic values

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(Diagnostic_CEED_BP1)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar *u                   = in[1];

  // Outputs
  CeedScalar *diagnostic = out[0];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Qdata
    const CeedScalar wdetJ = q_data[0][i];

    // Displacement
    diagnostic[i] = u[i] * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
