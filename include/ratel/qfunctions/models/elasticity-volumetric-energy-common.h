/// @file
/// Ratel volumetric energy common term for mixed and single isochoric hyperelastic models at finite strain QFunction source
#pragma once

#include <ceed/types.h>
#ifndef CEED_RUNNING_JIT_PASS
#include <math.h>
#endif

#include "../utils.h"  // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

#define FLOPS_V (FLOPS_Log1pSeries + 5)
#define FLOPS_J_dVdJ 3
#define FLOPS_J2_d2VdJ2 4
#define FLOPS_U (FLOPS_Log1pSeries + 4 + 5)
#define FLOPS_J_dUdJ (FLOPS_Log1pSeries + 4 + 7)
#define FLOPS_J2_d2UdJ2 (FLOPS_Log1pSeries + 4 + 18)

/**
  @brief Compute V, J * dV/dJ, J^2 * d^2V/dJ^2 for mixed/single fields hyperelasticity.
  For isochoric single field and mixed methods we consider the volumetric energy of form
  `\psi_vol = k * V`

 `A = J^2 - 1 - 2 logJ`
 `V = A / 4`
 `J dV/dJ = (J^2 - 1) / 2`
 `J^2 d^2V/dJ^2 = (J^2 + 1) / 2`

  @param[in]   Jm1          Determinant of deformation gradient - 1.
  @param[out]  V            `V`
  @param[out]  J_dVdJ       `J dV/dJ`
  @param[out]  J2_d2VdJ2    `J^2 d^2V/dJ^2`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int VolumetricFunctionAndDerivatives(CeedScalar Jm1, CeedScalar *V, CeedScalar *J_dVdJ, CeedScalar *J2_d2VdJ2) {
  const CeedScalar Jp1  = Jm1 + 2.;
  const CeedScalar logJ = RatelLog1pSeries(Jm1);
  const CeedScalar A    = Jm1 * Jp1 - 2. * logJ;

  if (V) *V = A * 0.25;
  if (J_dVdJ) *J_dVdJ = Jm1 * Jp1 * 0.5;
  if (J2_d2VdJ2) {
    const CeedScalar J = Jm1 + 1.;

    *J2_d2VdJ2 = (J * J + 1) * 0.5;
  }
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute U, J * dU/dJ, J^2 * d^2U/dJ^2 for mixed with perturbed Lagrange-multiplier hyperelasticity.
  For mixed PL method we consider the volumetric energy of form
  `\psi_vol = k/2 * U^2`

 `A = J^2 - 1 - 2 logJ`
 `U = A^{1/2} / sqrt(2)`
 `J dU/dJ = (J^2 - 1) A^{-1/2} / sqrt(2)`
 `J^2 d^2U/dJ^2 = [(J^2 + 1) A^{-1/2} - (J^2 - 1)^2 A^{-3/2}] / sqrt(2)`

  @param[in]   Jm1          Determinant of deformation gradient - 1.
  @param[out]  U            `U`
  @param[out]  J_dUdJ       `J dU/dJ`
  @param[out]  J2_d2UdJ2    `J^2 d^2U/dJ^2`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int VolumetricFunctionAndDerivatives_PL(CeedScalar Jm1, CeedScalar *U, CeedScalar *J_dUdJ, CeedScalar *J2_d2UdJ2) {
  const CeedScalar Jp1  = Jm1 + 2.;
  const CeedScalar logJ = RatelLog1pSeries(Jm1);
  const CeedScalar A    = Jm1 * Jp1 - 2. * logJ;

  if (U) *U = RatelSign(Jm1) * sqrt(A) * sqrt(0.5);
  if (J_dUdJ) *J_dUdJ = Jm1 * Jp1 * RatelSign(Jm1) * sqrt(1. / A) * sqrt(0.5);
  if (J2_d2UdJ2) {
    const CeedScalar J      = Jm1 + 1.;
    const CeedScalar A_pow1 = RatelSign(Jm1) * sqrt(1. / A);
    const CeedScalar A_pow2 = A_pow1 / A;
    *J2_d2UdJ2              = ((J * J + 1) * A_pow1 - (Jm1 * Jp1) * (Jm1 * Jp1) * A_pow2) * sqrt(0.5);
  }
  return CEED_ERROR_SUCCESS;
}

/// @}
