/// @file
/// Ratel neo-Hookean hyperelasticity at finite strain common QFunction source
#pragma once

#include <ceed/types.h>

#include "../../models/elasticity-linear.h"       // IWYU pragma: export
#include "../../models/neo-hookean.h"             // IWYU pragma: export
#include "../utils.h"                             // IWYU pragma: export
#include "elasticity-diagnostic-common.h"         // IWYU pragma: export
#include "elasticity-neo-hookean-common.h"        // IWYU pragma: export
#include "elasticity-volumetric-energy-common.h"  // IWYU pragma: export
#include "elasticity-volumetric-stress-common.h"  // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

/**
  @brief Compute surface forces for neo-Hookean hyperelasticity

  @param[in]   ctx  QFunction context, holding `RatelNeoHookeanElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - gradient of u with respect to reference coordinates
  @param[out]  out  Output arrays
                      - 0 - surface force values

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(SurfaceForceCellToFace_NeoHookean)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*diagnostic)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Context
  const RatelNeoHookeanElasticityParams *context = (RatelNeoHookeanElasticityParams *)ctx;
  const CeedScalar                       lambda  = context->lambda;
  const CeedScalar                       two_mu  = context->two_mu;

  // Quadrature Point Loop
  for (CeedInt i = 0; i < Q; i++) {
    CeedScalar dXdx[3][3], grad_u[3][3], E_sym[6], C_inv_sym[6], S_sym[6], S[3][3], P[3][3], J_dVdJ;

    // Qdata
    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Normal
    const CeedScalar normal[3] = {q_data[10][i], q_data[11][i], q_data[12][i]};

    // Read spatial derivatives of u; du/dX
    CeedScalar dudX[3][3];
    RatelGradUnpack(Q, i, ug, dudX);

    // Compute grad_u = du/dX * dX/dx
    // X is ref coordinate [-1,1]^3; x is physical coordinate
    RatelMatMatMult(1.0, dudX, dXdx, grad_u);

    // Compute the Deformation Gradient : F = I + grad_u
    const CeedScalar F[3][3] = {
        {grad_u[0][0] + 1, grad_u[0][1],     grad_u[0][2]    },
        {grad_u[1][0],     grad_u[1][1] + 1, grad_u[1][2]    },
        {grad_u[2][0],     grad_u[2][1],     grad_u[2][2] + 1}
    };

    // Compute J - 1
    const CeedScalar Jm1 = RatelMatDetAM1(grad_u);

    // Compute E, C^{-1}, S
    RatelGreenLagrangeStrain(grad_u, E_sym);
    RatelCInverse(E_sym, Jm1, C_inv_sym);
    VolumetricFunctionAndDerivatives(Jm1, NULL, &J_dVdJ, NULL);
    RatelSecondKirchhoffStress_NeoHookean(J_dVdJ, lambda, two_mu, C_inv_sym, E_sym, S_sym);
    RatelSymmetricMatUnpack(S_sym, S);

    // Compute the First Piola-Kirchhoff : P = F*S
    RatelMatMatMult(1.0, F, S, P);

    // Compute P N
    for (CeedInt j = 0; j < 3; j++) diagnostic[j][i] = -q_data[0][i] * RatelDot3(P[j], normal);
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
