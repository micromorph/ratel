/// @file
/// Ratel isochoric Ogden hyperelasticity at finite strain in current configuration QFunction source
#include <ceed/types.h>
#ifndef CEED_RUNNING_JIT_PASS
#include <math.h>
#endif

#include "../../models/ogden.h"                 // IWYU pragma: export
#include "../boundaries/platen-nitsche.h"       // IWYU pragma: export
#include "../utils.h"                           // IWYU pragma: export
#include "elasticity-common.h"                  // IWYU pragma: export
#include "elasticity-isochoric-ogden-common.h"  // IWYU pragma: export
#include "elasticity-isochoric-ogden-initial.h"

/// @addtogroup RatelMaterials
/// @{

#define NUM_COMPONENTS_STATE_IsochoricOgdenCurrent 0
#define NUM_COMPONENTS_STORED_IsochoricOgdenCurrent 31
#define NUM_ACTIVE_FIELD_EVAL_MODES_IsochoricOgdenCurrent 1

// FLOPS_grad_du = FLOPS_MatMatMult
// 3 flops for creating F
#define FLOPS_df1_IsochoricOgden_Current \
  (2 * FLOPS_MatMatMult + FLOPS_VecVecCross + FLOPS_Tau_IsochoricOgden + FLOPS_FdSFTranspose_IsochoricOgden + FLOPS_MatMatAdd)
#define FLOPS_JACOBIAN_IsochoricOgdenCurrent (FLOPS_df1_IsochoricOgden_Current + FLOPS_dXdxwdetJ)

/**
  @brief Compute `tau` for isochoric Ogden hyperelasticity in current configuration

  @param[in]   ctx                   QFunction context, holding `RatelOgdenElasticityParams`
  @param[in]   Q                     Number of quadrature points
  @param[in]   i                     Current quadrature point
  @param[in]   in                    Input arrays
                                       - 0 - volumetric qdata
                                       - 1 - gradient of u with respect to reference coordinates
  @param[out]  out                   Output arrays
                                       - 0 - stored, dXdx, e, J - 1 and J^{-alpha[j]/3}
  @param[out]  dXdx                  Coordinate transformation
  @param[out]  f1                    `f1 = tau`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int f1_IsochoricOgdenCurrent(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in, CeedScalar *const *out,
                                                   CeedScalar dXdx[3][3], CeedScalar f1[3][3]) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*stored_values) = out[0];

  // Context
  const RatelOgdenElasticityParams *context = (RatelOgdenElasticityParams *)ctx;
  const CeedInt                     N       = context->num_ogden_parameters;
  const CeedScalar                  bulk    = context->bulk;
  const CeedScalar                 *m       = context->m;
  const CeedScalar                 *alpha   = context->alpha;

  CeedScalar dXdx_initial[3][3], Grad_u[3][3], e_sym[6], e_vals[3], e_vecs[3][3], tau_sym[6], F_inv[3][3], l[3], J_pow_alpha[3], series_terms[9],
      J_dVdJ;

  // -- Qdata
  // dXdx_initial = dX/dx_initial
  // X is natural coordinate sys OR Reference [-1, 1]^dim
  // x_initial is initial config coordinate system
  RatelQdataUnpack(Q, i, q_data, dXdx_initial);

  // Read spatial derivatives of u; du/dX
  CeedScalar dudX[3][3];
  RatelGradUnpack(Q, i, ug, dudX);

  // X is natural coordinate sys OR Reference system
  // x_initial is initial config coordinate system
  // Grad_u =du/dx_initial= du/dX * dX/dx_initial
  RatelMatMatMult(1.0, dudX, dXdx_initial, Grad_u);

  // Compute the Deformation Gradient : F = I + Grad_u
  const CeedScalar F[3][3] = {
      {Grad_u[0][0] + 1, Grad_u[0][1],     Grad_u[0][2]    },
      {Grad_u[1][0],     Grad_u[1][1] + 1, Grad_u[1][2]    },
      {Grad_u[2][0],     Grad_u[2][1],     Grad_u[2][2] + 1}
  };

  RatelGreenEulerStrain(Grad_u, e_sym);

  const CeedScalar Jm1 = RatelMatDetAM1(Grad_u);
  RatelMatComputeEigensystemSymmetric(e_sym, e_vals, e_vecs);
  // Compute l[j] = log(pr_str[j]) = 0.5 * log1p(2*e_vals[j])
  for (CeedInt j = 0; j < 3; j++) l[j] = 0.5 * RatelLog1pSeries(2 * e_vals[j]);
  for (CeedInt j = 0; j < N; j++) {
    J_pow_alpha[j]          = pow(Jm1 + 1., -alpha[j] / 3.);
    CeedScalar expm1_term_0 = RatelExpm1Series(alpha[j] * l[0]), expm1_term_1 = RatelExpm1Series(alpha[j] * l[1]),
               expm1_term_2 = RatelExpm1Series(alpha[j] * l[2]);

    series_terms[j]     = J_pow_alpha[j] * (m[j] / 3.) * (2 * expm1_term_0 - expm1_term_1 - expm1_term_2);
    series_terms[j + 3] = J_pow_alpha[j] * (m[j] / 3.) * (-expm1_term_0 + 2 * expm1_term_1 - expm1_term_2);
    series_terms[j + 6] = J_pow_alpha[j] * (m[j] / 3.) * (-expm1_term_0 - expm1_term_1 + 2 * expm1_term_2);
  }
  VolumetricFunctionAndDerivatives(Jm1, NULL, &J_dVdJ, NULL);
  RatelKirchhoffTau_IsochoricOgden(J_dVdJ, bulk, N, m, alpha, series_terms, e_vals, e_vecs, tau_sym);
  RatelSymmetricMatUnpack(tau_sym, f1);

  // Compute F^{-1}
  const CeedScalar detF = Jm1 + 1.;

  RatelMatInverse(F, detF, F_inv);

  // x is current config coordinate system
  // dXdx = dX/dx = dX/dx_initial * F^{-1}
  // Note that F^{-1} = dx_initial/dx
  RatelMatMatMult(1.0, dXdx_initial, F_inv, dXdx);

  // Store values
  RatelStoredValuesPack(Q, i, 0, 9, (CeedScalar *)dXdx, stored_values);
  RatelStoredValuesPack(Q, i, 9, 1, &Jm1, stored_values);
  RatelStoredValuesPack(Q, i, 10, 3, (CeedScalar *)e_vals, stored_values);
  RatelStoredValuesPack(Q, i, 13, 3, (CeedScalar *)e_vecs[1], stored_values);
  RatelStoredValuesPack(Q, i, 16, 3, (CeedScalar *)e_vecs[2], stored_values);
  RatelStoredValuesPack(Q, i, 19, 3, (CeedScalar *)J_pow_alpha, stored_values);
  RatelStoredValuesPack(Q, i, 22, 9, (CeedScalar *)series_terms, stored_values);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute linearization of `tau` for isochoric Ogden hyperelasticity in current configuration

  @param[in]   ctx                   QFunction context, holding `RatelOgdenElasticityParams`
  @param[in]   Q                     Number of quadrature points
  @param[in]   i                     Current quadrature point
  @param[in]   in                    Input arrays
                                       - 0 - volumetric qdata
                                       - 1 - stored, dXdx, e, J - 1 and J^{-alpha[j]/3}
                                       - 2 - gradient of incremental change to u with respect to reference coordinates
  @param[out]  out                   Output arrays, unused
  @param[out]  dXdx                  Coordinate transformation
  @param[out]  df1                   `df1 = dtau - tau * grad_du^T`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int df1_IsochoricOgdenCurrent(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in, CeedScalar *const *out,
                                                    CeedScalar dXdx[3][3], CeedScalar df1[3][3]) {
  // Inputs
  const CeedScalar(*stored_values)      = in[1];
  const CeedScalar(*dug)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[2];

  // Context
  const RatelOgdenElasticityParams *context = (RatelOgdenElasticityParams *)ctx;
  const CeedInt                     N       = context->num_ogden_parameters;
  const CeedScalar                  bulk    = context->bulk;
  const CeedScalar                 *m       = context->m;
  const CeedScalar                 *alpha   = context->alpha;

  CeedScalar grad_du[3][3], e_vals[3], e_vecs[3][3], tau_sym[6], tau[3][3], grad_du_tau[3][3], FdSFTranspose[3][3], J_pow_alpha[3], series_terms[9],
      Jm1, J_dVdJ, J2_d2VdJ2;

  // Read spatial derivatives of increment u; d(du)/dX = d(du)/dX
  CeedScalar ddudX[3][3];
  RatelGradUnpack(Q, i, dug, ddudX);

  // Retrieve dXdx
  RatelStoredValuesUnpack(Q, i, 0, 9, stored_values, (CeedScalar *)dXdx);

  // Compute grad_du = ddu/dX * dX/dx
  // X is ref coordinate [-1,1]^3; x is physical coordinate in current configuration
  RatelMatMatMult(1.0, ddudX, dXdx, grad_du);
  // Retrieve e_sym, Jm1, e_vals and e_vecs
  RatelStoredValuesUnpack(Q, i, 9, 1, stored_values, &Jm1);
  RatelStoredValuesUnpack(Q, i, 10, 3, stored_values, e_vals);
  RatelStoredValuesUnpack(Q, i, 13, 3, stored_values, e_vecs[1]);
  RatelStoredValuesUnpack(Q, i, 16, 3, stored_values, e_vecs[2]);
  RatelStoredValuesUnpack(Q, i, 19, 3, stored_values, J_pow_alpha);
  RatelStoredValuesUnpack(Q, i, 22, 9, stored_values, series_terms);
  RatelVecVecCross(e_vecs[1], e_vecs[2], e_vecs[0]);
  VolumetricFunctionAndDerivatives(Jm1, NULL, &J_dVdJ, &J2_d2VdJ2);
  RatelKirchhoffTau_IsochoricOgden(J_dVdJ, bulk, N, m, alpha, series_terms, e_vals, e_vecs, tau_sym);
  RatelSymmetricMatUnpack(tau_sym, tau);

  // Compute grad_du_tau = grad_du*tau
  RatelMatMatMult(1.0, grad_du, tau, grad_du_tau);
  // Compute F*dS*F^T
  RatelComputeFdSFTranspose_IsochoricOgden(J_dVdJ, J2_d2VdJ2, bulk, N, m, alpha, J_pow_alpha, series_terms, grad_du, e_vals, e_vecs, FdSFTranspose);
  // df1 = dtau - tau * grad_du^T
  //     = grad_du*tau + F*dS*F^T
  RatelMatMatAdd(1.0, grad_du_tau, 1., FdSFTranspose, df1);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute residual for isochoric Ogden hyperelasticity in current configuration

  @param[in]   ctx  QFunction context, holding `RatelOgdenElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityResidual_IsochoricOgdenCurrent)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  (void)ElasticityResidual_IsochoricOgdenInitial;
  return ElasticityResidual(ctx, Q, f1_IsochoricOgdenCurrent, !!NUM_COMPONENTS_STATE_IsochoricOgdenCurrent,
                            !!NUM_COMPONENTS_STORED_IsochoricOgdenCurrent, NUM_ACTIVE_FIELD_EVAL_MODES_IsochoricOgdenCurrent, in, out);
}

/**
  @brief Evaluate Jacobian for isochoric Ogden hyperelasticity in current configuration

  @param[in]   ctx  QFunction context, holding `RatelOgdenElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityJacobian_IsochoricOgdenCurrent)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  (void)ElasticityJacobian_IsochoricOgdenInitial;
  return ElasticityJacobian(ctx, Q, df1_IsochoricOgdenCurrent, !!NUM_COMPONENTS_STATE_IsochoricOgdenCurrent,
                            !!NUM_COMPONENTS_STORED_IsochoricOgdenCurrent, NUM_ACTIVE_FIELD_EVAL_MODES_IsochoricOgdenCurrent, in, out);
}

/// @}
