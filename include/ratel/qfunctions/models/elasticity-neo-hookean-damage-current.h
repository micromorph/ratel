/// @file
/// Ratel Neo-Hookean (current configuration) with damage phase field
#include <ceed/types.h>
#ifndef CEED_RUNNING_JIT_PASS
#include <math.h>
#endif

#include "../../models/elasticity-damage.h"           // IWYU pragma: export
#include "../boundaries/platen-nitsche.h"             // IWYU pragma: export
#include "../utils.h"                                 // IWYU pragma: export
#include "elasticity-isochoric-neo-hookean-common.h"  // IWYU pragma: export
#include "elasticity-neo-hookean-damage-common.h"     // IWYU pragma: export
#include "elasticity-neo-hookean-damage-initial.h"    // IWYU pragma: export

#define NUM_COMPONENTS_STORED_NeoHookeanCurrent_Damage 19  // 1 damage + 6 strain + 1 Psi_plus_history_flag + 9 grad_u + Jm1 + J^{-2/3}

/// @addtogroup RatelMaterials
/// @{

/**
  @brief Compute u_t term of damage residual

  @param[in]   ctx  QFunction context, holding `RatelElasticityDamageParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - volumetric qdata
                      - 1 - u_t (four components: 3 displacement components, 1 scalar damage field)
  @param[out]  out  Output arrays
                      - 0 - action on u_t

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityDamageResidual_ut_NeoHookeanCurrent)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u_t)[CEED_Q_VLA]    = (const CeedScalar(*)[CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*v_t)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Context Elasticity+Damage
  const RatelElasticityDamageParams *context        = (RatelElasticityDamageParams *)ctx;
  const CeedScalar                   xi             = context->damage_viscosity;
  const CeedScalar                   damage_scaling = context->damage_scaling;

  // Quadrature point loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    const CeedScalar wdetJ = q_data[0][i];

    // Update values
    v_t[0][i] = 0;
    v_t[1][i] = 0;
    v_t[2][i] = 0;
    v_t[3][i] = damage_scaling * xi * u_t[3][i] * wdetJ;
  }  // End of Quadrature Point Loop
  // Prevent unused functions warnings
  (void)ElasticityDamageResidual_ut_NeoHookeanInitial;
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute residual for Neo-hookean hyperelasticity current conf.

  @param[in]   ctx  QFunction context, holding `RatelElasticityDamageParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - volumetric qdata
                      - 1 - Psi_plus and damage state
                      - 2 - u (four components: 3 displacement components, 1 scalar damage field)
                      - 3 - u_g gradient of u with respect to reference coordinates
  @param[out]  out  Output arrays
                      - 0 - Updated Psi_plus and damage state
                      - 1 - stored vector
                      - 2 - action on u
                      - 3 - action on u_g

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityDamageResidual_NeoHookeanCurrent)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*state)              = in[1];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[2];
  const CeedScalar(*u_g)[4][CEED_Q_VLA] = (const CeedScalar(*)[4][CEED_Q_VLA])in[3];

  // Outputs
  CeedScalar(*current_state)       = out[0];
  CeedScalar(*stored)              = out[1];
  CeedScalar(*v)[CEED_Q_VLA]       = (CeedScalar(*)[CEED_Q_VLA])out[2];
  CeedScalar(*dvdX)[4][CEED_Q_VLA] = (CeedScalar(*)[4][CEED_Q_VLA])out[3];

  // Context
  const RatelElasticityDamageParams *context        = (RatelElasticityDamageParams *)ctx;
  const CeedScalar                   mu             = context->mu;
  const CeedScalar                   bulk           = context->bulk;
  const CeedScalar                   Gc             = context->fracture_toughness;
  const CeedScalar                   l0             = context->characteristic_length;
  const CeedScalar                   eta            = context->residual_stiffness;
  const CeedScalar                   damage_scaling = context->damage_scaling;
  const bool                         use_AT1        = context->use_AT1;

  const CeedScalar Psi_plus_critical = use_AT1 ? 3 * Gc / (8 * 2 * l0) : 0;
  const CeedScalar c0                = use_AT1 ? 8. / 3. : 2.;

  // Quadrature point loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar V, J_dVdJ, Psi_plus_history_flag, H, Psi_plus;
    CeedScalar Grad_u[3][3], grad_phi[3], e_sym[6], b_sym[6], b[3][3], b_grad_phi[3], tau_degr_sym[6], tau_degr[3][3], dXdx_initial[3][3],
        F_inv[3][3], dXdx[3][3];

    // dX/dx from Q_data
    const CeedScalar wdetJ = q_data[0][i];
    // dXdx_initial = dX/dx_initial
    // X is natural coordinate sys OR Reference [-1, 1]^dim
    // x_initial is initial config coordinate system
    RatelQdataUnpack(Q, i, q_data, dXdx_initial);

    //----- Displacement problem
    // Spatial derivatives of u
    const CeedScalar dudX[3][3] = {
        {u_g[0][0][i], u_g[1][0][i], u_g[2][0][i]},
        {u_g[0][1][i], u_g[1][1][i], u_g[2][1][i]},
        {u_g[0][2][i], u_g[1][2][i], u_g[2][2][i]}
    };

    // X is natural coordinate sys OR Reference system
    // x_initial is initial config coordinate system
    // Grad_u =du/dx_initial= du/dX * dX/dx_initial
    RatelMatMatMult(1.0, dudX, dXdx_initial, Grad_u);

    // Compute the Deformation Gradient : F = I + grad_u
    const CeedScalar F[3][3] = {
        {Grad_u[0][0] + 1, Grad_u[0][1],     Grad_u[0][2]    },
        {Grad_u[1][0],     Grad_u[1][1] + 1, Grad_u[1][2]    },
        {Grad_u[2][0],     Grad_u[2][1],     Grad_u[2][2] + 1}
    };

    // Read damage (forth component)
    const CeedScalar phi = u[3][i];

    // Compute J - 1
    const CeedScalar Jm1   = RatelMatDetAM1(Grad_u);
    const CeedScalar J_pow = pow(Jm1 + 1., -2. / 3.);

    // Volumetric function and derivatives
    VolumetricFunctionAndDerivatives(Jm1, &V, &J_dVdJ, NULL);

    // Compute degraded kirchhoff stress tau_degr
    RatelGreenEulerStrain(Grad_u, e_sym);
    ComputeDegradedKirchhoffTau_NeoHookean(J_dVdJ, bulk, 2 * mu, Jm1, J_pow, eta, phi, e_sym, tau_degr_sym);
    RatelSymmetricMatUnpack(tau_degr_sym, tau_degr);

    // Compute F^{-1}
    const CeedScalar detF = Jm1 + 1.;

    RatelMatInverse(F, detF, F_inv);

    // x is current config coordinate system
    // dXdx = dX/dx = dX/dx_initial * F^{-1}
    // Note that F^{-1} = dx_initial/dx
    RatelMatMatMult(1.0, dXdx_initial, F_inv, dXdx);

    // Compute (grad(v), tau_degr)
    for (CeedInt j = 0; j < 3; j++) {
      for (CeedInt k = 0; k < 3; k++) {
        dvdX[j][k][i] = 0;
        for (CeedInt m = 0; m < 3; m++) {
          dvdX[j][k][i] += wdetJ * dXdx[j][m] * tau_degr[k][m];
        }
      }
    }

    //----- Damage problem
    // Compute Psi_plus
    const CeedScalar trace_e = RatelMatTraceSymmetric(e_sym);
    PsiPlus_NeoHookean(V, mu, bulk, Jm1, trace_e, &Psi_plus);

    // Treat Psi_plus as a history variable and ensure that increases monotonically for crack irreversibility
    const CeedScalar Psi_plus_old = state[i];
    Psi_plus_history_flag         = Psi_plus > Psi_plus_old;
    Psi_plus                      = RatelMax(Psi_plus, Psi_plus_old);
    Psi_plus                      = RatelMax(Psi_plus_critical, Psi_plus);

    // Spatial derivative of phi
    const CeedScalar phi_g[3] = {u_g[0][3][i], u_g[1][3][i], u_g[2][3][i]};

    // Compute grad_phi = dX/dx^T dphi/dX
    RatelMatTransposeVecMult(1.0, dXdx, phi_g, grad_phi);
    const CeedScalar dg_dphi = -2 * (1 - eta) * (1 - phi);

    // If AT2, substitute appropiate values for c0 and alpha (hence dalpha)
    const CeedScalar dalpha_dphi = use_AT1 ? 1. : 2. * phi;

    // Compute H and update v values
    H       = dg_dphi * Psi_plus + (Gc / (c0 * l0)) * dalpha_dphi;
    v[0][i] = 0;
    v[1][i] = 0;
    v[2][i] = 0;
    v[3][i] = damage_scaling * H * wdetJ;

    // Compute b = 2 e + I
    for (CeedInt j = 0; j < 6; j++) b_sym[j] = 2 * e_sym[j] + (j < 3);
    RatelSymmetricMatUnpack(b_sym, b);
    RatelMatVecMult(1.0, b, grad_phi, b_grad_phi);

    // Update fourth component of dvdX via (grad(q), (2 Gc l0 / c0) b * grad(phi))
    for (CeedInt j = 0; j < 3; j++) {
      dvdX[j][3][i] =
          damage_scaling * (b_grad_phi[0] * dXdx[j][0] + b_grad_phi[1] * dXdx[j][1] + b_grad_phi[2] * dXdx[j][2]) * 2. * Gc * l0 * wdetJ / c0;
    }

    // Save updated values for state fields
    RatelStoredValuesPack(Q, i, 0, 1, &Psi_plus, current_state);

    // Store values
    RatelStoredValuesPack(Q, i, 0, 1, &phi, stored);
    RatelStoredValuesPack(Q, i, 1, 6, (CeedScalar *)e_sym, stored);
    RatelStoredValuesPack(Q, i, 7, 1, &Psi_plus_history_flag, stored);
    RatelStoredValuesPack(Q, i, 8, 9, (CeedScalar *)dXdx, stored);
    RatelStoredValuesPack(Q, i, 17, 1, &Jm1, stored);
    RatelStoredValuesPack(Q, i, 18, 1, &J_pow, stored);
  }  // End of Quadrature Point Loop
  // Prevent unused functions warnings
  (void)ElasticityDamageResidual_NeoHookeanInitial;
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute jacobian for Neo-hookean hyperelasticity initial conf.

  @param[in]   ctx  QFunction context, holding `RatelElasticityDamageParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - volumetric qdata
                      - 1 - Current state values
                      - 2 - Stored values
                      - 3 - du (four components: 3 displacement components, 1 damage field)
                      - 4 - du_g gradient of du with respect to reference coordinates
  @param[out]  out  Output arrays
                      - 0 - action on du
                      - 1 - action on du_g

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityDamageJacobian_NeoHookeanCurrent)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA]  = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*current_state)       = in[1];
  const CeedScalar(*stored)              = in[2];
  const CeedScalar(*du)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[3];
  const CeedScalar(*du_g)[4][CEED_Q_VLA] = (const CeedScalar(*)[4][CEED_Q_VLA])in[4];

  // Outputs
  CeedScalar(*dv)[CEED_Q_VLA]       = (CeedScalar(*)[CEED_Q_VLA])out[0];
  CeedScalar(*ddvdX)[4][CEED_Q_VLA] = (CeedScalar(*)[4][CEED_Q_VLA])out[1];

  // Context Elasticity+Damage
  const RatelElasticityDamageParams *context        = (RatelElasticityDamageParams *)ctx;
  const CeedScalar                   mu             = context->mu;
  const CeedScalar                   bulk           = context->bulk;
  const CeedScalar                   Gc             = context->fracture_toughness;
  const CeedScalar                   l0             = context->characteristic_length;
  const CeedScalar                   eta            = context->residual_stiffness;
  const CeedScalar                   damage_scaling = context->damage_scaling;
  const CeedScalar                   xi             = context->damage_viscosity;
  const bool                         use_AT1        = context->use_AT1;
  const bool                         use_offdiag    = context->use_offdiagonal;
  const CeedScalar                   shift_v        = context->common_parameters[1];

  // Set d2alpha_dphi2 and c0 based on AT1 or AT2
  const CeedScalar c0            = use_AT1 ? 8. / 3. : 2.;
  const CeedScalar d2alpha_dphi2 = use_AT1 ? 0. : 2.;
  const CeedScalar d2g_dphi2     = 2 * (1 - eta);

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar J_dVdJ, J2_d2VdJ2, Psi_plus_history_flag, dPsi_plus, dH, Jm1, J_pow, Psi_plus, phi;
    CeedScalar grad_dphi[3], e_sym[6], b_sym[6], b[3][3], b_grad_dphi[3], dXdx[3][3], grad_du[3][3], depsilon_sym[6], tau_degr_sym[6], tau_degr[3][3],
        grad_du_tau_degr[3][3], FdSFTranspose_degr[3][3], df1[3][3];

    // Read spatial derivatives of increment u; d(du)/dX = d(du)/dX
    const CeedScalar ddudX[3][3] = {
        {du_g[0][0][i], du_g[1][0][i], du_g[2][0][i]},
        {du_g[0][1][i], du_g[1][1][i], du_g[2][1][i]},
        {du_g[0][2][i], du_g[1][2][i], du_g[2][2][i]}
    };

    // Unpack stored values
    RatelStoredValuesUnpack(Q, i, 0, 1, stored, &phi);
    RatelStoredValuesUnpack(Q, i, 1, 6, stored, e_sym);
    RatelStoredValuesUnpack(Q, i, 7, 1, stored, &Psi_plus_history_flag);
    RatelStoredValuesUnpack(Q, i, 8, 9, stored, (CeedScalar *)dXdx);
    RatelStoredValuesUnpack(Q, i, 17, 1, stored, &Jm1);
    RatelStoredValuesUnpack(Q, i, 18, 1, stored, &J_pow);

    // Extract Qdata
    const CeedScalar wdetJ = q_data[0][i];

    // Compute grad_du = ddu/dX * dX/dx
    // X is ref coordinate [-1,1]^3; x is physical coordinate in current configuration
    RatelMatMatMult(1.0, ddudX, dXdx, grad_du);

    // Volumetric function and derivatives
    VolumetricFunctionAndDerivatives(Jm1, NULL, &J_dVdJ, &J2_d2VdJ2);

    // Compute tau_degr
    ComputeDegradedKirchhoffTau_NeoHookean(J_dVdJ, bulk, 2 * mu, Jm1, J_pow, eta, phi, e_sym, tau_degr_sym);
    RatelSymmetricMatUnpack(tau_degr_sym, tau_degr);

    // Compute grad_du_tau = grad_du*tau
    RatelMatMatMult(1.0, grad_du, tau_degr, grad_du_tau_degr);

    // Compute F*dS*F^T
    const CeedScalar dphi = du[3][i];

    RatelComputeDegradedFdSFTranspose_NeoHookean(J_dVdJ, J2_d2VdJ2, bulk, mu, Jm1, J_pow, eta, use_offdiag, phi, dphi, e_sym, grad_du, depsilon_sym,
                                                 FdSFTranspose_degr);
    RatelMatMatAdd(1.0, grad_du_tau_degr, 1., FdSFTranspose_degr, df1);

    for (CeedInt j = 0; j < 3; j++) {
      for (CeedInt k = 0; k < 3; k++) {
        ddvdX[j][k][i] = 0;
        for (CeedInt m = 0; m < 3; m++) {
          ddvdX[j][k][i] += wdetJ * dXdx[j][m] * df1[k][m];
        }
      }
    }

    //--Compute l0*Gc*(grad(dphi), grad(q))
    // Spatial derivative of phi
    const CeedScalar dphi_g[3] = {du_g[0][3][i], du_g[1][3][i], du_g[2][3][i]};

    // grad_dphi = dX/dx^T * d(dphi)/dX
    RatelMatTransposeVecMult(1.0, dXdx, dphi_g, grad_dphi);

    // Unpack state fields
    RatelStoredValuesUnpack(Q, i, 0, 1, current_state, &Psi_plus);

    // Get dg_dphi
    const CeedScalar dg_dphi = -2 * (1 - eta) * (1 - phi);

    // Compute d_phi_t
    const CeedScalar dphi_t = shift_v * dphi;

    // Compute dPsi_plus
    const bool add_offdiag = !(Psi_plus_history_flag == 0. || !use_offdiag);
    PsiPlus_NeoHookeanCurrent_fwd(J_dVdJ, 2 * mu, bulk, Jm1, J_pow, e_sym, depsilon_sym, &dPsi_plus);
    dPsi_plus = add_offdiag * dPsi_plus;

    // Compute dH and dv values
    dH       = d2g_dphi2 * dphi * Psi_plus + dg_dphi * dPsi_plus + (Gc / (c0 * l0)) * d2alpha_dphi2 * dphi + xi * dphi_t;
    dv[0][i] = 0;
    dv[1][i] = 0;
    dv[2][i] = 0;
    dv[3][i] = damage_scaling * dH * wdetJ;

    // Compute b = 2 e + I
    for (CeedInt j = 0; j < 6; j++) b_sym[j] = 2 * e_sym[j] + (j < 3);
    RatelSymmetricMatUnpack(b_sym, b);
    RatelMatVecMult(1.0, b, grad_dphi, b_grad_dphi);

    // Update fourth component of dvdX via (grad(dq), (2 Gc l0 / c0) b * grad(dphi))
    for (CeedInt j = 0; j < 3; j++) {
      ddvdX[j][3][i] =
          damage_scaling * (b_grad_dphi[0] * dXdx[j][0] + b_grad_dphi[1] * dXdx[j][1] + b_grad_dphi[2] * dXdx[j][2]) * 2. * Gc * l0 * wdetJ / c0;
    }
  }  // End of Quadrature Point Loop
  // Prevent unused functions warnings
  (void)ElasticityDamageJacobian_NeoHookeanInitial;
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_JACOBIAN_NeoHookeanCurrent_Damage                                                                                              \
  (2 * FLOPS_MatMatMult + FLOPS_Degraded_Tau_NeoHookean + FLOPS_Degraded_FdSFTranspose_NeoHookean + FLOPS_MatMatAdd + FLOPS_MatMatMult + 9 + \
   FLOPS_MatVecMult + 5 + FLOPS_PsiPlus_NeoHookeanCurrent_fwd + 14 + 33)

/// @}
