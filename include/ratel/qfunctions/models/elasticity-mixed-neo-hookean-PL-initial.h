/// @file
/// Ratel mixed neo-Hookean perturbed Lagrange-multiplier hyperelasticity at finite strain in initial configuration QFunction source
#include <ceed/types.h>

#include "../../models/mixed-neo-hookean.h"          // IWYU pragma: export
#include "../boundaries/platen-nitsche.h"            // IWYU pragma: export
#include "../utils.h"                                // IWYU pragma: export
#include "elasticity-mixed-common.h"                 // IWYU pragma: export
#include "elasticity-mixed-neo-hookean-PL-common.h"  // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

#define NUM_COMPONENTS_STATE_MixedNeoHookeanPLInitial 0
#define NUM_COMPONENTS_STORED_MixedNeoHookeanPLInitial 10
#define NUM_ACTIVE_FIELD_EVAL_MODES_MixedNeoHookeanPLInitial_u 1
#define NUM_ACTIVE_FIELD_EVAL_MODES_MixedNeoHookeanPLInitial_p 1
#define NUM_ACTIVE_FIELD_EVAL_MODES_MixedNeoHookeanPLInitial \
  (NUM_ACTIVE_FIELD_EVAL_MODES_MixedNeoHookeanPLInitial_u + NUM_ACTIVE_FIELD_EVAL_MODES_MixedNeoHookeanPLInitial_p)

// FLOPS_grad_du = FLOPS_MatMatMult
// 3 flops for creating F
#define FLOPS_df1_MixedNHPL_Initial                                                                                                            \
  (FLOPS_MatMatMult + 3 + FLOPS_MatDetAM1 + FLOPS_GreenEulerStrain + FLOPS_CInverse + FLOPS_S_MixedNeoHookeanPL + FLOPS_dS_MixedNeoHookeanPL + \
   FLOPS_MatMatMultPlusMatMatMult)
#define FLOPS_dg0_MixedNHPL_Initial (FLOPS_Log1pSeries + 14)
#define FLOPS_JACOBIAN_MixedNeoHookeanPLInitial (FLOPS_df1_MixedNHPL_Initial + FLOPS_dXdxwdetJ + FLOPS_dg0_MixedNHPL_Initial)
#define FLOPS_JACOBIAN_Block_uu_MixedNeoHookeanPLInitial                                                                                       \
  (FLOPS_MatMatMult + 3 + FLOPS_MatDetAM1 + FLOPS_GreenEulerStrain + FLOPS_CInverse + FLOPS_S_MixedNeoHookeanPL + FLOPS_dS_MixedNeoHookeanPL + \
   FLOPS_MatMatMultPlusMatMatMult + FLOPS_dXdxwdetJ)
#define FLOPS_JACOBIAN_Block_pp_MixedNeoHookeanPLInitial (7)

// -----------------------------------------------------------------------------
//
// p is Lagrange multiplier, P is First Piola-Kirchhoff tensor
// Weak form: Find (u,p) \in VxQ (V=H1, Q=L^2) on \Omega
//  (grad(v), P)                       = (v, f)
//  (q, U(J) - p / bulk) = 0
// -----------------------------------------------------------------------------
/**
  @brief Compute `P` for mixed neo-Hookean perturbed Lagrange-multiplier hyperelasticity in initial configuration

  @param[in]   ctx                   QFunction context, holding `RatelMixedNeoHookeanElasticityParams`
  @param[in]   Q                     Number of quadrature points
  @param[in]   i                     Current quadrature point
  @param[in]   in                    Input arrays
                                       0 - volumetric qdata
                                       1 - gradient of u with respect to reference coordinates
                                       2 - Lagrange multiplier
  @param[out]  out                   Output arrays
                                       0 - stored values: grad_u and p
  @param[out]  dXdx                  Coordinate transformation
  @param[out]  f1                    `f1 = P`
  @param[out]  Jm1                   Determinant of deformation gradient - 1 needed for computing g0 function

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int f1_MixedNeoHookeanPLInitial(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in,
                                                      CeedScalar *const *out, CeedScalar dXdx[3][3], CeedScalar f1[3][3], CeedScalar *Jm1) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];
  const CeedScalar(*p)                  = in[2];

  // Outputs
  CeedScalar(*stored_values) = out[0];

  // Context
  const RatelMixedNeoHookeanElasticityParams *context     = (RatelMixedNeoHookeanElasticityParams *)ctx;
  const CeedScalar                            two_mu      = context->two_mu;
  const CeedScalar                            bulk_primal = context->bulk_primal;

  CeedScalar grad_u[3][3], E_sym[6], C_inv_sym[6], S_sym[6], S[3][3], U, J_dUdJ;

  // Qdata
  RatelQdataUnpack(Q, i, q_data, dXdx);

  // Read spatial derivatives of u; du/dX
  CeedScalar dudX[3][3];
  RatelGradUnpack(Q, i, ug, dudX);

  // Compute grad_u = du/dX * dX/dx
  // X is ref coordinate [-1,1]^3; x is physical coordinate
  RatelMatMatMult(1.0, dudX, dXdx, grad_u);

  // Compute the Deformation Gradient : F = I + grad_u
  const CeedScalar F[3][3] = {
      {grad_u[0][0] + 1, grad_u[0][1],     grad_u[0][2]    },
      {grad_u[1][0],     grad_u[1][1] + 1, grad_u[1][2]    },
      {grad_u[2][0],     grad_u[2][1],     grad_u[2][2] + 1}
  };

  // J - 1
  *Jm1 = RatelMatDetAM1(grad_u);

  // Compute E, C^{-1}, S
  RatelGreenLagrangeStrain(grad_u, E_sym);
  RatelCInverse(E_sym, *Jm1, C_inv_sym);
  VolumetricFunctionAndDerivatives_PL(*Jm1, &U, &J_dUdJ, NULL);
  RatelSecondKirchhoffStress_MixedNeoHookeanPL(U, J_dUdJ, bulk_primal, two_mu, p[i], *Jm1, C_inv_sym, E_sym, S_sym);
  RatelSymmetricMatUnpack(S_sym, S);

  // Compute the First Piola-Kirchhoff : P = F*S
  RatelMatMatMult(1.0, F, S, f1);

  // Store values
  RatelStoredValuesPack(Q, i, 0, 9, (CeedScalar *)grad_u, stored_values);
  RatelStoredValuesPack(Q, i, 9, 1, &p[i], stored_values);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute `g0` for mixed neo-Hookean perturbed Lagrange-multiplier hyperelasticity in initial configuration

  @param[in]   ctx                   QFunction context, holding `RatelMixedNeoHookeanElasticityParams`
  @param[in]   Q                     Number of quadrature points
  @param[in]   i                     Current quadrature point
  @param[in]   in                    Input arrays
                                       2 - Lagrange multiplier
  @param[in]   Jm1                   Determinant of deformation gradient - 1 computed in f1 function
  @param[out]  out                   Output arrays, unused
  @param[out]  g0                    `g0 = (U(J) - p/bulk )`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int g0_MixedNeoHookeanPLInitial(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in, const CeedScalar Jm1,
                                                      CeedScalar *const *out, CeedScalar *g0) {
  // Inputs
  const CeedScalar(*p) = in[2];

  // Context
  const RatelMixedNeoHookeanElasticityParams *context     = (RatelMixedNeoHookeanElasticityParams *)ctx;
  const CeedScalar                            bulk        = context->bulk;
  const CeedScalar                            bulk_primal = context->bulk_primal;
  const CeedScalar                            inv_bulk    = (bulk == 0.) ? 0. : 1. / (bulk - bulk_primal);

  CeedScalar U;
  VolumetricFunctionAndDerivatives_PL(Jm1, &U, NULL, NULL);

  // q * (-U(J) - p/(bulk - bulk_primal) )
  *g0 = (-U - p[i] * inv_bulk);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute linearization of `P` for mixed neo-Hookean perturbed Lagrange-multiplier hyperelasticity in initial configuration

  @param[in]   ctx                   QFunction context, holding `RatelMixedNeoHookeanElasticityParams`
  @param[in]   Q                     Number of quadrature points
  @param[in]   i                     Current quadrature point
  @param[in]   in                    Input arrays
                                       0 - volumetric qdata
                                       1 - stored gradient of u and p
                                       2 - gradient of incremental change to u with respect to reference coordinates
                                       3 - dp
  @param[out]  out                   Output arrays, unused
  @param[out]  dXdx                  Coordinate transformation
  @param[out]  df1                   `df1 = dP`
  @param[out]  Jm1                   Determinant of deformation gradient - 1 needed for computing dg0 function
  @param[out]  Cinv_contract_dE      `C_inv:dE` needed for computing dg0 function

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int df1_MixedNeoHookeanPLInitial(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in,
                                                       CeedScalar *const *out, CeedScalar dXdx[3][3], CeedScalar df1[3][3], CeedScalar *Jm1,
                                                       CeedScalar *Cinv_contract_dE) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*stored_values)      = in[1];
  const CeedScalar(*dug)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[2];
  const CeedScalar(*dp)                 = in[3];

  // Context
  const RatelMixedNeoHookeanElasticityParams *context     = (RatelMixedNeoHookeanElasticityParams *)ctx;
  const CeedScalar                            mu          = context->mu;
  const CeedScalar                            two_mu      = context->two_mu;
  const CeedScalar                            bulk_primal = context->bulk_primal;

  CeedScalar grad_du[3][3], grad_u[3][3], E_sym[6], C_inv_sym[6], S_sym[6], S[3][3], dS_sym[6], dS[3][3], p, U, J_dUdJ, J2_d2UdJ2;

  // Qdata
  RatelQdataUnpack(Q, i, q_data, dXdx);

  // Read spatial derivatives of increment u; d(du)/dX = d(du)/dX
  CeedScalar ddudX[3][3];
  RatelGradUnpack(Q, i, dug, ddudX);

  // Compute grad_du = ddu/dX * dX/dx
  // X is ref coordinate [-1,1]^3; x is physical coordinate
  RatelMatMatMult(1.0, ddudX, dXdx, grad_du);
  // Deformation Gradient : F = I + grad_u
  RatelStoredValuesUnpack(Q, i, 0, 9, stored_values, (CeedScalar *)grad_u);
  const CeedScalar F[3][3] = {
      {grad_u[0][0] + 1, grad_u[0][1],     grad_u[0][2]    },
      {grad_u[1][0],     grad_u[1][1] + 1, grad_u[1][2]    },
      {grad_u[2][0],     grad_u[2][1],     grad_u[2][2] + 1}
  };

  // J - 1
  *Jm1 = RatelMatDetAM1(grad_u);

  // Compute E, C^{-1}, S
  RatelGreenLagrangeStrain(grad_u, E_sym);
  RatelCInverse(E_sym, *Jm1, C_inv_sym);
  RatelStoredValuesUnpack(Q, i, 9, 1, stored_values, &p);
  VolumetricFunctionAndDerivatives_PL(*Jm1, &U, &J_dUdJ, &J2_d2UdJ2);
  RatelSecondKirchhoffStress_MixedNeoHookeanPL(U, J_dUdJ, bulk_primal, two_mu, p, *Jm1, C_inv_sym, E_sym, S_sym);
  RatelSymmetricMatUnpack(S_sym, S);

  // dS = dS/dE:dE
  RatelSecondKirchhoffStress_MixedNeoHookeanPL_fwd(U, J_dUdJ, J2_d2UdJ2, bulk_primal, mu, p, dp[i], *Jm1, F, grad_du, C_inv_sym, E_sym, dS_sym,
                                                   Cinv_contract_dE);
  RatelSymmetricMatUnpack(dS_sym, dS);

  // dP = dF*S + F*dS
  RatelMatMatMultPlusMatMatMult(grad_du, S, F, dS, df1);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute linearization of `g0` for mixed neo-Hookean perturbed Lagrange-multiplier hyperelasticity in initial configuration

  @param[in]   ctx                   QFunction context, holding `RatelMixedNeoHookeanElasticityParams`
  @param[in]   Q                     Number of quadrature points
  @param[in]   i                     Current quadrature point
  @param[in]   in                    Input arrays
                                       1 - stored gradient of u and p
                                       3 - dp
  @param[in]   Jm1                   Determinant of deformation gradient - 1 computed in df1 function
  @param[in]   Cinv_contract_dE      `C_inv:dE` computed in df1 function
  @param[out]  out                   Output arrays, unused
  @param[out]  dg0                   `dg0 = ([-J^2 - J*p/bulk](C_inv:dE) - dp*J/bulk )`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int dg0_MixedNeoHookeanPLInitial(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in, const CeedScalar Jm1,
                                                       const CeedScalar Cinv_contract_dE, CeedScalar *const *out, CeedScalar *dg0) {
  // Inputs
  const CeedScalar(*stored_values) = in[1];
  const CeedScalar(*dp)            = in[3];

  // Context
  const RatelMixedNeoHookeanElasticityParams *context     = (RatelMixedNeoHookeanElasticityParams *)ctx;
  const CeedScalar                            bulk        = context->bulk;
  const CeedScalar                            bulk_primal = context->bulk_primal;
  const CeedScalar                            inv_bulk    = (bulk == 0.) ? 0. : 1. / (bulk - bulk_primal);

  CeedScalar p, J_dUdJ;
  VolumetricFunctionAndDerivatives_PL(Jm1, NULL, &J_dUdJ, NULL);

  RatelStoredValuesUnpack(Q, i, 9, 1, stored_values, &p);
  // q * (dU(J) - dp/(bulk - bulk_primal))
  *dg0 = -J_dUdJ * Cinv_contract_dE - dp[i] * inv_bulk;
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute residual for mixed neo-Hookean perturbed Lagrange-multiplier hyperelasticity in initial configuration

  @param[in]   ctx  QFunction context, holding `RatelMixedNeoHookeanElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityResidual_MixedNeoHookeanPLInitial)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return MixedElasticityResidual(ctx, Q, f1_MixedNeoHookeanPLInitial, g0_MixedNeoHookeanPLInitial, !!NUM_COMPONENTS_STATE_MixedNeoHookeanPLInitial,
                                 !!NUM_COMPONENTS_STORED_MixedNeoHookeanPLInitial, NUM_ACTIVE_FIELD_EVAL_MODES_MixedNeoHookeanPLInitial, in, out);
}

/**
  @brief Evaluate Jacobian for mixed neo-Hookean perturbed Lagrange-multiplier hyperelasticity in initial configuration

  @param[in]   ctx  QFunction context, holding `RatelMixedNeoHookeanElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityJacobian_MixedNeoHookeanPLInitial)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return MixedElasticityJacobian(ctx, Q, df1_MixedNeoHookeanPLInitial, dg0_MixedNeoHookeanPLInitial, !!NUM_COMPONENTS_STATE_MixedNeoHookeanPLInitial,
                                 !!NUM_COMPONENTS_STORED_MixedNeoHookeanPLInitial, NUM_ACTIVE_FIELD_EVAL_MODES_MixedNeoHookeanPLInitial, in, out);
}

/**
  @brief Wrapper to compute `P` for mixed neo-Hookean perturbed Lagrange-multiplier hyperelasticity in initial configuration for platen BCs

  @param[in]   ctx                   QFunction context, holding `RatelMixedNeoHookeanElasticityParams`
  @param[in]   Q                     Number of quadrature points
  @param[in]   i                     Current quadrature point
  @param[in]   in                    Input arrays
                                       0 - volumetric qdata
                                       1 - gradient of u with respect to reference coordinates
                                       2 - pressure
  @param[out]  out                   Output arrays
                                       0 - stored values: grad_u and p
  @param[out]  dXdx                  Coordinate transformation
  @param[out]  f1                    `f1 = P`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int f1_MixedNeoHookeanPLInitial_Platen(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in,
                                                             CeedScalar *const *out, CeedScalar dXdx[3][3], CeedScalar f1[3][3]) {
  CeedScalar Jm1;

  return f1_MixedNeoHookeanPLInitial(ctx, Q, i, in, out, dXdx, f1, &Jm1);
}

/**
  @brief Compute linearization of `P` for mixed neo-Hookean perturbed Lagrange-multiplier hyperelasticity in initial configuration

  @param[in]   ctx                   QFunction context, holding `RatelMixedNeoHookeanElasticityParams`
  @param[in]   Q                     Number of quadrature points
  @param[in]   i                     Current quadrature point
  @param[in]   in                    Input arrays
                                       0 - volumetric qdata
                                       1 - stored gradient of u and p
                                       2 - gradient of incremental change to u with respect to reference coordinates
                                       3 - dp
  @param[out]  out                   Output arrays, unused
  @param[out]  dXdx                  Coordinate transformation
  @param[out]  df1                   `df1 = dP`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int df1_MixedNeoHookeanPLInitial_Platen(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in,
                                                              CeedScalar *const *out, CeedScalar dXdx[3][3], CeedScalar df1[3][3]) {
  CeedScalar Jm1, Cinv_contract_dE;

  return df1_MixedNeoHookeanPLInitial(ctx, Q, i, in, out, dXdx, df1, &Jm1, &Cinv_contract_dE);
}

/**
  @brief Compute platen residual for neo-Hookean perturbed Lagrange-multiplier hyperelasticity in initial configuration

  @param[in]   ctx  QFunction context, holding `BCPlatenData`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(PlatenBCsResidual_MixedNeoHookeanPLInitial)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return PlatenBCs(ctx, Q, f1_MixedNeoHookeanPLInitial_Platen, !!NUM_COMPONENTS_STATE_MixedNeoHookeanPLInitial,
                   !!NUM_COMPONENTS_STORED_MixedNeoHookeanPLInitial, NUM_ACTIVE_FIELD_EVAL_MODES_MixedNeoHookeanPLInitial, in, out);
}

/**
  @brief Evaluate platen Jacobian for neo-Hookean perturbed Lagrange-multiplier hyperelasticity in initial configuration

  @param[in]   ctx  QFunction context, holding `BCPlatenData`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(PlatenBCsJacobian_MixedNeoHookeanPLInitial)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return PlatenBCs_Jacobian(ctx, Q, df1_MixedNeoHookeanPLInitial_Platen, !!NUM_COMPONENTS_STATE_MixedNeoHookeanPLInitial,
                            !!NUM_COMPONENTS_STORED_MixedNeoHookeanPLInitial, NUM_ACTIVE_FIELD_EVAL_MODES_MixedNeoHookeanPLInitial, in, out);
}

/**
  @brief Compute displacement block for pMG preconditioner for mixed neo-Hookean perturbed Lagrange-multiplier hyperelasticity in initial
configuration

  @param[in]   ctx  QFunction context, holding `RatelMixedNeoHookeanElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      0 - qdata
                      1 - gradient of incremental change of u with respect to reference coordinates
  @param[out]  out  Output arrays
                      0 - action of QFunction for displacement field

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityPC_uu_MixedNeoHookeanPLInitial)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*stored_values)      = in[1];
  const CeedScalar(*dug)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[2];

  // Outputs
  CeedScalar(*ddvdX)[3][CEED_Q_VLA] = (CeedScalar(*)[3][CEED_Q_VLA])out[0];

  // Context
  const RatelMixedNeoHookeanElasticityParams *context        = (RatelMixedNeoHookeanElasticityParams *)ctx;
  const CeedScalar                            mu             = context->mu;
  const CeedScalar                            two_mu         = context->two_mu;
  const CeedScalar                            bulk_primal_pc = context->bulk_primal_pc;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar grad_du[3][3], grad_u[3][3], E_sym[6], C_inv_sym[6], S_sym[6], S[3][3], dS_sym[6], dS[3][3], Cinv_contract_dE, p, U, J_dUdJ, J2_d2UdJ2,
        df1_uu[3][3];
    // -- Qdata
    const CeedScalar wdetJ = q_data[0][i];
    CeedScalar       dXdx[3][3];
    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Read spatial derivatives of increment u; d(du)/dX = d(du)/dX
    CeedScalar ddudX[3][3];
    RatelGradUnpack(Q, i, dug, ddudX);

    // Compute grad_du = ddu/dX * dX/dx
    // X is ref coordinate [-1,1]^3; x is physical coordinate
    RatelMatMatMult(1.0, ddudX, dXdx, grad_du);
    // Deformation Gradient : F = I + grad_u
    RatelStoredValuesUnpack(Q, i, 0, 9, stored_values, (CeedScalar *)grad_u);
    const CeedScalar F[3][3] = {
        {grad_u[0][0] + 1, grad_u[0][1],     grad_u[0][2]    },
        {grad_u[1][0],     grad_u[1][1] + 1, grad_u[1][2]    },
        {grad_u[2][0],     grad_u[2][1],     grad_u[2][2] + 1}
    };

    // J - 1
    const CeedScalar Jm1 = RatelMatDetAM1(grad_u);

    // Compute E, C^{-1}, S
    RatelGreenLagrangeStrain(grad_u, E_sym);
    RatelCInverse(E_sym, Jm1, C_inv_sym);
    RatelStoredValuesUnpack(Q, i, 9, 1, stored_values, &p);
    VolumetricFunctionAndDerivatives_PL(Jm1, &U, &J_dUdJ, &J2_d2UdJ2);
    RatelSecondKirchhoffStress_MixedNeoHookeanPL(U, J_dUdJ, bulk_primal_pc, two_mu, p, Jm1, C_inv_sym, E_sym, S_sym);
    RatelSymmetricMatUnpack(S_sym, S);

    // dS = dS/dE:dE; note we should set dp = 0.
    RatelSecondKirchhoffStress_MixedNeoHookeanPL_fwd(U, J_dUdJ, J2_d2UdJ2, bulk_primal_pc, mu, p, 0.0, Jm1, F, grad_du, C_inv_sym, E_sym, dS_sym,
                                                     &Cinv_contract_dE);
    RatelSymmetricMatUnpack(dS_sym, dS);

    // df1_uu = dF*S + F*dS(du, dp = 0)
    RatelMatMatMultPlusMatMatMult(grad_du, S, F, dS, df1_uu);

    // (grad(v), df1_uu)
    RatelMatMatTransposeMultAtQuadraturePoint(Q, i, wdetJ, dXdx, df1_uu, ddvdX);
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute pressure block for pMG preconditioner for mixed neo-Hookean perturbed Lagrange-multiplier hyperelasticity in initial configuration

  @param[in]   ctx  QFunction context, holding `RatelMixedNeoHookeanElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      0 - qdata
                      1 - incremental change of p
  @param[out]  out  Output arrays
                      0 - action of QFunction for pressure field

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityPC_pp_MixedNeoHookeanPLInitial)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*dp)                 = in[2];

  // Outputs
  CeedScalar(*dq) = out[0];

  // Context
  const RatelMixedNeoHookeanElasticityParams *context        = (RatelMixedNeoHookeanElasticityParams *)ctx;
  const CeedScalar                            mu             = context->mu;
  const CeedScalar                            bulk           = context->bulk;
  const CeedScalar                            bulk_primal_pc = context->bulk_primal_pc;
  const CeedInt                               sign_pp        = context->sign_pp;
  const CeedScalar                            inv_bulk_pc    = (bulk == 0.) ? 0. : 1. / (bulk - bulk_primal_pc);

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // -- Qdata
    const CeedScalar wdetJ = q_data[0][i];

    // (q, -dp/(bulk - bulk_primal_pc))
    dq[i] = sign_pp * (inv_bulk_pc + 1 / mu) * dp[i] * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
