/// @file
/// Ratel mixed volumetric common stress with perturbed Lagrange-multiplier method at finite strain QFunction source
#pragma once

#include <ceed/types.h>

#include "../../models/mixed-neo-hookean.h"       // IWYU pragma: export
#include "../utils.h"                             // IWYU pragma: export
#include "elasticity-volumetric-energy-common.h"  // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

#define FLOPS_Tau_vol_mixed_PL (FLOPS_U + FLOPS_J_dUdJ + 3)
#define FLOPS_S_vol_mixed_PL (FLOPS_U + FLOPS_J_dUdJ + 3 + FLOPS_ScalarMatMultSymmetric)
#define FLOPS_dS_vol_mixed_PL (FLOPS_U + FLOPS_J_dUdJ + FLOPS_J2_d2UdJ2 + 13 + FLOPS_MatMatAddSymmetric + 12)
#define FLOPS_FdSFTranspose_vol_mixed_PL (FLOPS_U + FLOPS_J_dUdJ + FLOPS_J2_d2UdJ2 + 13 + FLOPS_ScalarMatMultSymmetric + 4)

/**
  @brief Compute volumetric Kirchoff tau for mixed perturbed Lagrange-multiplier hyperelasticity.

 `tau_vol = [bulk_primal * U - p ] * J dU/dJ * I`

  @param[in]   U            U(J)
  @param[in]   J_dUdJ       J dU/dJ
  @param[in]   bulk_primal  Primal bulk modulus
  @param[in]   p            Pressure
  @param[out]  tau_vol_sym  Volumetric Kirchoff tau

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelVolumetricKirchhoffTau_Mixed_PL(CeedScalar U, CeedScalar J_dUdJ, CeedScalar bulk_primal, CeedScalar p,
                                                               CeedScalar *tau_vol_sym) {
  // -- [bulk_primal * U - p ] * J dU/dJ
  *tau_vol_sym = (bulk_primal * U - p) * J_dUdJ;
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute volumetric part of second Kirchoff stress for mixed with perturbed Lagrange-multiplier hyperelasticity.

 `S = [bulk_primal * U - p ] * J dU/dJ * C_inv`

  @param[in]   U             U(J)
  @param[in]   J_dUdJ        J dU/dJ
  @param[in]   bulk_primal   Primal bulk modulus
  @param[in]   p             Lagrange multiplier
  @param[in]   C_inv_sym     Inverse of right Cauchy-Green tensor, in symmetric representation
  @param[out]  S_vol_sym     Volumetric second Kirchoff stress, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelVolumetricSecondKirchhoffStress_Mixed_PL(CeedScalar U, CeedScalar J_dUdJ, CeedScalar bulk_primal, CeedScalar p,
                                                                        const CeedScalar C_inv_sym[6], CeedScalar S_vol_sym[6]) {
  // -- [bulk_primal * U - p ] * J dU/dJ
  const CeedScalar coeff_1 = (bulk_primal * U - p) * J_dUdJ;

  // S_vol = coeff_1 * C_inv
  RatelScalarMatMultSymmetric(coeff_1, C_inv_sym, S_vol_sym);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute derivative of volumetric second Kirchoff stress mixed with perturbed Lagrange-multiplier hyperelasticity.

 `dS_vol = [(bulk_primal (J dU/dJ)^2 + (bulk_primal * U - p) * (J^2 * d2U/dJ2 + J dU/dJ)) C_inv:dE - dp * J dU/dJ] C_inv + [bulk_primal * U - p] J
dU/dJ * dC_inv`

  @param[in]   U                 U(J)
  @param[in]   J_dUdJ            J dU/dJ
  @param[in]   J2_d2UdJ2         J^2 d^2U/dJ^2
  @param[in]   bulk_primal       Primal bulk modulus
  @param[in]   p                 Pressure
  @param[in]   dp                Increment of Pressure
  @param[in]   Cinv_contract_dE  `C_inv : dE`
  @param[in]   C_inv_sym         Inverse of right Cauchy-Green tensor, in symmetric representation
  @param[in]   dC_inv_sym        Derivative of C^{-1}, in symmetric representation
  @param[out]  dS_vol_sym        Derivative of primal second Kirchoff stress, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelVolumetricSecondKirchhoffStress_Mixed_PL_fwd(CeedScalar U, CeedScalar J_dUdJ, CeedScalar J2_d2UdJ2,
                                                                            CeedScalar bulk_primal, CeedScalar p, CeedScalar dp,
                                                                            CeedScalar Cinv_contract_dE, const CeedScalar C_inv_sym[6],
                                                                            const CeedScalar dC_inv_sym[6], CeedScalar dS_vol_sym[6]) {
  const CeedScalar coeff_1 = (bulk_primal * J_dUdJ * J_dUdJ + (bulk_primal * U - p) * (J2_d2UdJ2 + J_dUdJ)) * Cinv_contract_dE - dp * J_dUdJ;
  const CeedScalar coeff_2 = (bulk_primal * U - p) * J_dUdJ;

  // dS_vol = coeff_1 C_inv + coeff_2 dC_inv
  RatelMatMatAddSymmetric(coeff_1, C_inv_sym, coeff_2, dC_inv_sym, dS_vol_sym);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute F*dS_vol*F^T for mixed with perturbed Lagrange-multiplier in current configuration.

  @param[in]   U                       U(J)
  @param[in]   J_dUdJ                  J dU/dJ
  @param[in]   J2_d2UdJ2               J^2 d^2U/dJ^2
  @param[in]   bulk_primal             Primal bulk modulus
  @param[in]   p                       Pressure
  @param[in]   dp                      Increment of Pressure
  @param[in]   trace_depsilon          `trace(depsilon)`
  @param[in]   depsilon_sym            depsilon = (grad_du + grad_du^T)/2
  @param[out]  FdSFTranspose_vol_sym   F*dS_vol*F^T needed for computing df1 in current configuration

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelComputeFdSFTransposeVolumetric_Mixed_PL(CeedScalar U, CeedScalar J_dUdJ, CeedScalar J2_d2UdJ2, CeedScalar bulk_primal,
                                                                       CeedScalar p, CeedScalar dp, CeedScalar trace_depsilon,
                                                                       const CeedScalar depsilon_sym[6], CeedScalar FdSFTranspose_vol_sym[6]) {
  const CeedScalar coeff_1 = (bulk_primal * J_dUdJ * J_dUdJ + (bulk_primal * U - p) * (J2_d2UdJ2 + J_dUdJ)) * trace_depsilon - dp * J_dUdJ;
  const CeedScalar coeff_2 = (bulk_primal * U - p) * J_dUdJ;

  // F*dS_vol*F^T = -2 * coeff_2 * depsilon_sym
  RatelScalarMatMultSymmetric(-2. * coeff_2, depsilon_sym, FdSFTranspose_vol_sym);

  // Add coeff_1 * I to FdSFTranspose_vol_sym
  FdSFTranspose_vol_sym[0] += coeff_1;
  FdSFTranspose_vol_sym[1] += coeff_1;
  FdSFTranspose_vol_sym[2] += coeff_1;
  return CEED_ERROR_SUCCESS;
}

/// @}
