/// @file
/// Ratel mixed neo-Hookean perturbed Lagrange-multiplier hyperelasticity at finite strain common QFunction source
#pragma once

#include <ceed/types.h>
#ifndef CEED_RUNNING_JIT_PASS
#include <math.h>
#endif

#include "../../models/mixed-neo-hookean.h"                  // IWYU pragma: export
#include "../utils.h"                                        // IWYU pragma: export
#include "elasticity-isochoric-stress-neo-hookean-common.h"  // IWYU pragma: export
#include "elasticity-mixed-PL-volumetric-stress-common.h"
#include "elasticity-volumetric-energy-common.h"  // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

#define NUM_COMPONENTS_DIAGNOSTIC_MixedNeoHookeanPL 16

#define FLOPS_Tau_MixedNeoHookeanPL (FLOPS_Tau_vol_mixed_PL + FLOPS_Tau_iso_NeoHookean + 3)
#define FLOPS_S_MixedNeoHookeanPL (FLOPS_S_iso_NeoHookean + FLOPS_S_vol_mixed_PL)
#define FLOPS_dS_MixedNeoHookeanPL (1 + FLOPS_dS_vol_mixed_PL + FLOPS_dS_iso_NeoHookean + FLOPS_MatMatMatAddSymmetric)
#define FLOPS_FdSFTranspose_MixedNeoHookeanPL (FLOPS_FdSFTranspose_iso_NeoHookean + FLOPS_FdSFTranspose_vol_mixed_PL + FLOPS_MatMatAddSymmetric)

/**
  @brief Compute Kirchoff tau for mixed neo-Hookean perturbed Lagrange-multiplier hyperelasticity.

 `tau = [bulk_primal * U - p ] * J dU/dJ * I + 2 mu J^{-2/3} e_dev`

  @param[in]   U            U(J)
  @param[in]   J_dUdJ       J dU/dJ
  @param[in]   bulk_primal  Primal bulk modulus
  @param[in]   two_mu       Two times the shear modulus
  @param[in]   p            Lagrange multiplier
  @param[in]   J_pow        J^{-2/3}
  @param[in]   e_sym        Green Euler strain, in symmetric representation
  @param[out]  tau_sym      Kirchoff tau, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelKirchhoffTau_MixedNeoHookeanPL(CeedScalar U, CeedScalar J_dUdJ, CeedScalar bulk_primal, CeedScalar two_mu,
                                                              CeedScalar p, CeedScalar J_pow, const CeedScalar e_sym[6], CeedScalar tau_sym[6]) {
  CeedScalar tau_vol_sym;

  // -- [bulk_primal * U - p ] * J dU/dJ
  RatelVolumetricKirchhoffTau_Mixed_PL(U, J_dUdJ, bulk_primal, p, &tau_vol_sym);

  // -- 2 mu J^{-2/3} e_dev
  RatelIsochoricKirchhoffTau_NeoHookean(two_mu, J_pow, e_sym, tau_sym);

  tau_sym[0] += tau_vol_sym;
  tau_sym[1] += tau_vol_sym;
  tau_sym[2] += tau_vol_sym;
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute second Kirchoff stress for mixed neo-Hookean perturbed Lagrange-multiplier hyperelasticity.

 `S = S_vol + S_iso`

  @param[in]   U             U(J)
  @param[in]   J_dUdJ        J dU/dJ
  @param[in]   bulk_primal   Primal bulk modulus
  @param[in]   two_mu        Two times the shear modulus
  @param[in]   p             Lagrange multiplier
  @param[in]   Jm1           Determinant of deformation gradient - 1.
  @param[in]   C_inv_sym     Inverse of right Cauchy-Green tensor, in symmetric representation
  @param[in]   E_sym         Green Lagrange strain, in symmetric representation
  @param[out]  S_sym         Second Kirchoff stress, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelSecondKirchhoffStress_MixedNeoHookeanPL(CeedScalar U, CeedScalar J_dUdJ, CeedScalar bulk_primal, CeedScalar two_mu,
                                                                       CeedScalar p, CeedScalar Jm1, const CeedScalar C_inv_sym[6],
                                                                       const CeedScalar E_sym[6], CeedScalar S_sym[6]) {
  CeedScalar       S_iso_sym[6], S_vol_sym[6];
  const CeedScalar J = Jm1 + 1.;

  // Compute S_iso
  RatelIsochoricSecondKirchhoffStress_NeoHookean(two_mu, J, C_inv_sym, E_sym, S_iso_sym);

  // Compute S_vol
  RatelVolumetricSecondKirchhoffStress_Mixed_PL(U, J_dUdJ, bulk_primal, p, C_inv_sym, S_vol_sym);

  // S = S_vol + S_iso
  RatelMatMatAddSymmetric(1.0, S_vol_sym, 1.0, S_iso_sym, S_sym);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute derivative of second Kirchoff stress mixed neo-Hookean perturbed Lagrange-multiplier hyperelasticity.

 `dS = dS_iso + dS_vol`

  @param[in]   U                 U(J)
  @param[in]   J_dUdJ            J dU/dJ
  @param[in]   J2_d2UdJ2         J^2 d^2U/dJ^2
  @param[in]   bulk_primal       Primal bulk modulus
  @param[in]   mu                Shear modulus
  @param[in]   p                 Pressure
  @param[in]   dp                Increment of Pressure
  @param[in]   Jm1               Determinant of deformation gradient - 1
  @param[in]   F                 Deformation gradient
  @param[in]   grad_du           Gradient of incremental change in u
  @param[in]   C_inv_sym         Inverse of right Cauchy-Green tensor, in symmetric representation
  @param[in]   E_sym             Green Lagrange strain, in symmetric representation
  @param[out]  dS_sym            Derivative of second Kirchoff stress, in symmetric representation
  @param[out]  Cinv_contract_dE  `C_inv : dE`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelSecondKirchhoffStress_MixedNeoHookeanPL_fwd(CeedScalar U, CeedScalar J_dUdJ, CeedScalar J2_d2UdJ2,
                                                                           CeedScalar bulk_primal, CeedScalar mu, CeedScalar p, CeedScalar dp,
                                                                           CeedScalar Jm1, const CeedScalar F[3][3], const CeedScalar grad_du[3][3],
                                                                           const CeedScalar C_inv_sym[6], const CeedScalar E_sym[6],
                                                                           CeedScalar dS_sym[6], CeedScalar *Cinv_contract_dE) {
  CeedScalar dC_inv_sym[6], dS_vol_sym[6], dS_iso_sym[6];

  // dS_iso
  RatelIsochoricSecondKirchhoffStress_NeoHookean_fwd(mu, Jm1 + 1., F, grad_du, C_inv_sym, E_sym, dC_inv_sym, dS_iso_sym, Cinv_contract_dE);

  // dS_vol
  RatelVolumetricSecondKirchhoffStress_Mixed_PL_fwd(U, J_dUdJ, J2_d2UdJ2, bulk_primal, p, dp, *Cinv_contract_dE, C_inv_sym, dC_inv_sym, dS_vol_sym);

  // dS = dS_iso + dS_vol;
  RatelMatMatAddSymmetric(1, dS_vol_sym, 1.0, dS_iso_sym, dS_sym);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute F*dS*F^T for mixed neo-Hookean hyperelasticity in current configuration.

  @param[in]   U               U(J)
  @param[in]   J_dUdJ          J dU/dJ
  @param[in]   J2_d2UdJ2       J^2 d^2U/dJ^2
  @param[in]   bulk_primal     Primal bulk modulus
  @param[in]   mu              Shear modulus
  @param[in]   p               Pressure
  @param[in]   dp              Increment of Pressure
  @param[in]   J_pow           J^{-2/3}
  @param[in]   grad_du         Gradient of incremental change in u
  @param[in]   e_sym           Green Euler strain, in symmetric representation
  @param[out]  FdSFTranspose   F*dS*F^T needed for computing df1 in current configuration
  @param[out]  trace_depsilon  `trace(depsilon)`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelComputeFdSFTranspose_MixedNeoHookeanPL(CeedScalar U, CeedScalar J_dUdJ, CeedScalar J2_d2UdJ2, CeedScalar bulk_primal,
                                                                      CeedScalar mu, CeedScalar p, CeedScalar dp, CeedScalar J_pow,
                                                                      const CeedScalar grad_du[3][3], const CeedScalar e_sym[6],
                                                                      CeedScalar FdSFTranspose[3][3], CeedScalar *trace_depsilon) {
  CeedScalar depsilon_sym[6], FdSFTranspose_vol_sym[6], FdSFTranspose_iso_sym[6], FdSFTranspose_sym[6];

  RatelComputeFdSFTransposeIsochoric_NeoHookean(mu, J_pow, grad_du, e_sym, depsilon_sym, FdSFTranspose_iso_sym, trace_depsilon);
  RatelComputeFdSFTransposeVolumetric_Mixed_PL(U, J_dUdJ, J2_d2UdJ2, bulk_primal, p, dp, *trace_depsilon, depsilon_sym, FdSFTranspose_vol_sym);

  // F*dS*F^T = F*dS_vol*F^T + F*dS_iso*F^T
  RatelMatMatAddSymmetric(1.0, FdSFTranspose_vol_sym, 1.0, FdSFTranspose_iso_sym, FdSFTranspose_sym);
  RatelSymmetricMatUnpack(FdSFTranspose_sym, FdSFTranspose);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute mixed potential energy for mixed neo-Hookean model.

 `Pi(u,p) = 0.5 mu (I1_bar - 3) - p U + 0.5 * bulk_primal * U^2 - 0.5 p^2 / (bulk - bulk_primal)`

  Note that strain energy function is `psi = 0.5 mu (I1_bar - 3) + bulk / 2 * U(J)^2`

  @param[in]   U              U(J)
  @param[in]   bulk           Bulk modulus
  @param[in]   bulk_primal    Primal bulk modulus
  @param[in]   mu             Shear modulus
  @param[in]   p              Pressure
  @param[in]   Jm1            Determinant of deformation gradient - 1
  @param[in]   trace_strain   Trace of Green Lagrange or Green euler strain tensor (E or e)
  @param[out]  mixed_energy   Energy for mixed Neo-Hookean model

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelStrainEnergy_MixedNeoHookeanPL(CeedScalar U, CeedScalar bulk, CeedScalar bulk_primal, CeedScalar mu, CeedScalar p,
                                                              CeedScalar Jm1, CeedScalar trace_strain, CeedScalar *mixed_energy) {
  const CeedScalar J  = Jm1 + 1.;
  const CeedScalar I1 = 2 * trace_strain + 3., J_pow = pow(J, -2. / 3.);
  const CeedScalar I1_bar = J_pow * I1;

  // Mixed energy potential Pi(u,p) for mixed Neo-Hookean PL
  *mixed_energy = (0.5 * mu * (I1_bar - 3.) - p * U + 0.5 * bulk_primal * U * U - 0.5 * p * p / (bulk - bulk_primal));
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute strain energy for mixed neo-Hookean perturbed Lagrange-multiplier hyperelasticity

  @param[in]   ctx  QFunction context, holding `RatelMixedNeoHookeanElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      0 - qdata
                      1 - gradient of u with respect to reference coordinates
  @param[out]  out  Output arrays
                      0 - strain energy

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(StrainEnergy_MixedNeoHookeanPL)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];
  const CeedScalar(*p)                  = in[2];

  // Outputs
  CeedScalar(*energy) = (CeedScalar(*))out[0];

  // Context
  const RatelMixedNeoHookeanElasticityParams *context     = (RatelMixedNeoHookeanElasticityParams *)ctx;
  const CeedScalar                            bulk        = context->bulk;
  const CeedScalar                            mu          = context->mu;
  const CeedScalar                            bulk_primal = context->bulk_primal;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar dXdx[3][3], grad_u[3][3], E_sym[6], U, mixed_energy;

    // Qdata
    const CeedScalar wdetJ = q_data[0][i];

    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Read spatial derivatives of u; du/dX
    CeedScalar dudX[3][3];
    RatelGradUnpack(Q, i, ug, dudX);

    // Compute grad_u = du/dX * dX/dx
    // X is ref coordinate [-1,1]^3; x is physical coordinate
    RatelMatMatMult(1.0, dudX, dXdx, grad_u);

    // E - Euler strain tensor
    RatelGreenLagrangeStrain(grad_u, E_sym);
    const CeedScalar Jm1  = RatelMatDetAM1(grad_u);
    const CeedScalar tr_E = RatelMatTraceSymmetric(E_sym);

    VolumetricFunctionAndDerivatives_PL(Jm1, &U, NULL, NULL);
    RatelStrainEnergy_MixedNeoHookeanPL(U, bulk, bulk_primal, mu, p[i], Jm1, tr_E, &mixed_energy);
    energy[i] = mixed_energy * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute projected diagnostic values for mixed neo-Hookean perturbed Lagrange-multiplier hyperelasticity

  @param[in]   ctx  QFunction context, holding `RatelMixedNeoHookeanElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      0 - qdata
                      1 - u
                      2 - gradient of u with respect to reference coordinates
                      3 - p
  @param[out]  out  Output arrays
                      0 - projected diagnostic values

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(Diagnostic_MixedNeoHookeanPL)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[1];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[2];
  const CeedScalar(*p)                  = in[3];

  // Outputs
  CeedScalar(*diagnostic)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Context
  const RatelMixedNeoHookeanElasticityParams *context     = (RatelMixedNeoHookeanElasticityParams *)ctx;
  const CeedScalar                            mu          = context->mu;
  const CeedScalar                            bulk        = context->bulk;
  const CeedScalar                            two_mu      = context->two_mu;
  const CeedScalar                            bulk_primal = context->bulk_primal;
  const CeedScalar                            rho         = context->common_parameters[RATEL_COMMON_PARAMETER_RHO];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar dXdx[3][3], grad_u[3][3], e_sym[6], tau_sym[6], sigma_sym[6], sigma_dev_sym[6], U, J_dUdJ, mixed_energy;

    // Qdata
    const CeedScalar wdetJ = q_data[0][i];

    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Read spatial derivatives of u; du/dX
    CeedScalar dudX[3][3];
    RatelGradUnpack(Q, i, ug, dudX);

    // Compute grad_u = du/dX * dX/dx
    // X is ref coordinate [-1,1]^3; x is physical coordinate
    RatelMatMatMult(1.0, dudX, dXdx, grad_u);

    // Compute J-1 and logJ
    const CeedScalar Jm1   = RatelMatDetAM1(grad_u);
    const CeedScalar J     = Jm1 + 1.0;
    const CeedScalar J_pow = pow(J, -2. / 3.);

    RatelGreenEulerStrain(grad_u, e_sym);
    VolumetricFunctionAndDerivatives_PL(Jm1, &U, &J_dUdJ, NULL);
    RatelKirchhoffTau_MixedNeoHookeanPL(U, J_dUdJ, bulk_primal, two_mu, p[i], J_pow, e_sym, tau_sym);
    for (CeedInt j = 0; j < 6; j++) sigma_sym[j] = tau_sym[j] / J;

    // Displacement
    diagnostic[0][i] = u[0][i];
    diagnostic[1][i] = u[1][i];
    diagnostic[2][i] = u[2][i];

    // Cauchy stress tensor
    diagnostic[3][i] = sigma_sym[0];
    diagnostic[4][i] = sigma_sym[5];
    diagnostic[5][i] = sigma_sym[4];
    diagnostic[6][i] = sigma_sym[1];
    diagnostic[7][i] = sigma_sym[3];
    diagnostic[8][i] = sigma_sym[2];

    // Hydrostatic pressure; p_h = -trace(sigma) / 3
    diagnostic[9][i] = -RatelMatTraceSymmetric(sigma_sym) / 3.;

    // Strain tensor invariants
    const CeedScalar trace_e  = RatelMatTraceSymmetric(e_sym);
    const CeedScalar trace_e2 = RatelMatMatContractSymmetric(1.0, e_sym, e_sym);

    diagnostic[10][i] = trace_e;
    diagnostic[11][i] = trace_e2;

    diagnostic[12][i] = J;

    RatelStrainEnergy_MixedNeoHookeanPL(U, bulk, bulk_primal, mu, p[i], Jm1, trace_e, &mixed_energy);

    // Strain energy
    diagnostic[13][i] = mixed_energy;

    // Compute von-Mises stress
    // -- Compute the the deviatoric part of Cauchy stress: sigma_dev = sigma - trace(sigma)/3
    const CeedScalar trace_sigma = RatelMatTraceSymmetric(sigma_sym);

    RatelMatDeviatoricSymmetric(trace_sigma, sigma_sym, sigma_dev_sym);

    // -- sigma_dev:sigma_dev
    const CeedScalar sigma_dev_contract = RatelMatMatContractSymmetric(1.0, sigma_dev_sym, sigma_dev_sym);

    // -- Compute von-Mises stress: sigma_von = sqrt(3/2 sigma_dev:sigma_dev)
    diagnostic[14][i] = sqrt(3. * sigma_dev_contract / 2.);

    // Compute mass density values

    // -- Set to 0 as default (rho should be 0.0 as defualt anyways)
    diagnostic[15][i] = 0.0;

    // -- if mass density is requested then compute mass density: rho/J
    if (rho > 0.0) diagnostic[15][i] = rho / diagnostic[12][i];

    // Quadrature weight
    for (CeedInt j = 0; j < NUM_COMPONENTS_DIAGNOSTIC_MixedNeoHookeanPL; j++) diagnostic[j][i] *= wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
