/// @file
/// Ratel linear elasticity with damage phase field
#include <ceed/types.h>

#include "../../models/elasticity-damage.h"  // IWYU pragma: export
#include "../boundaries/platen-nitsche.h"    // IWYU pragma: export
#include "../utils.h"                        // IWYU pragma: export

#define NUM_COMPONENTS_DIAGNOSTIC_Elasticity_Damage_Dual 2
#define NUM_COMPONENTS_DIAGNOSTIC_Elasticity_Damage 19
#define NUM_COMPONENTS_STORED_Elasticity_Damage 8  // 1 damage + 6 strain + 1 Psi_plus_history_flag
#define NUM_COMPONENTS_STATE_Elasticity_Damage 1   // 1 Psi_plus
#define NUM_ACTIVE_FIELD_EVAL_MODES_Elasticity_Damage 2
#define NUM_U_t_FIELD_EVAL_MODES_Elasticity_Damage 1

/// @addtogroup RatelMaterials
/// @{

/**
  @brief Compute Psi_plus for linear elasticity with damage

  @param[in]   mu         shear modulus
  @param[in]   bulk       bulk modulus
  @param[in]   trace_e    trace strain tensor
  @param[in]   e_dev_sym  deviatoric strain tensor in symmetric representation
  @param[out]  Psi_plus   Psi^+

  @return Computed Psi_plus for linear elasticity
**/
CEED_QFUNCTION_HELPER int PsiPlus_Linear(CeedScalar mu, CeedScalar bulk, CeedScalar trace_e, const CeedScalar e_dev_sym[6], CeedScalar *Psi_plus) {
  // Compute Psi_dev = mu*(e_dev : e_dev)
  const CeedScalar Psi_dev = RatelMatMatContractSymmetric(mu, e_dev_sym, e_dev_sym);

  // Compute Psi_vol = 0.5*K*tr(e)^2
  const CeedScalar Psi_vol = 0.5 * bulk * (trace_e * trace_e);

  // Compute Psi_plus
  *Psi_plus = Psi_dev + (trace_e > 0) * Psi_vol;
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_PsiPlus_Linear (FLOPS_MatMatContractSymmetric + 5)

/**
  @brief Compute Linearization of Psi_plus for linear elasticity with damage

  @param[in]   mu        Shear modulus
  @param[in]   bulk      Bulk modulus
  @param[in]   trace_e   trace strain tensor
  @param[in]   e_dev_sym deviatoric strain tensor in symmetric representation
  @param[in]   de_sym    Linear strain tensor increment in symmetric representation
  @param[out]  dPsi_plus dPsi^+

  @return Computed linearization of Psi_plus for linear elasticity
**/
CEED_QFUNCTION_HELPER int PsiPlus_Linear_fwd(CeedScalar mu, CeedScalar bulk, CeedScalar trace_e, const CeedScalar e_dev_sym[6],
                                             const CeedScalar de_sym[6], CeedScalar *dPsi_plus) {
  CeedScalar sigma_sym[6];
  CeedScalar sigma_vol_diag = (trace_e > 0) * bulk * trace_e;

  // Compute deviatoric stress
  RatelScalarMatMultSymmetric(2. * mu, e_dev_sym, sigma_sym);
  for (CeedInt i = 0; i < 3; i++) sigma_sym[i] += sigma_vol_diag;

  // Compute dPsi_plus = sigma : de
  *dPsi_plus = RatelMatMatContractSymmetric(1., sigma_sym, de_sym);
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_PsiPlus_Linear_fwd (3 + FLOPS_ScalarMatMultSymmetric + 3 + FLOPS_MatMatContractSymmetric)

/**
  @brief Compute `sigma_degr` for linear damage elasticity

  `sigma_degr = g(phi) * sigma_vol + g(phi) * sigma_dev, if trace_e >= 0`
  `sigma_degr = sigma_vol + g(phi) * sigma_dev, if trace_e < 0`

  @param[in]   mu              Shear modulus
  @param[in]   bulk            Bulk modulus
  @param[in]   eta             Residual stiffness
  @param[in]   phi             Damage field
  @param[in]   trace_e         trace strain tensor
  @param[in]   e_dev_sym       deviatoric strain tensor in symmetric representation
  @param[out]  sigma_degr_sym  Degraded stress in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int ComputeDegradedStress_Linear(CeedScalar mu, CeedScalar bulk, CeedScalar eta, CeedScalar phi, CeedScalar trace_e,
                                                       const CeedScalar e_dev_sym[6], CeedScalar sigma_degr_sym[6]) {
  // Compute g(phi) = (1 - eta)(1 - phi)^2 + eta
  const bool       trace_e_positive = trace_e > 0;
  const CeedScalar one_minus_phi    = 1 - phi;
  const CeedScalar g = one_minus_phi * one_minus_phi * (1 - eta) + eta, sigma_vol_diag = (trace_e_positive ? g : 1.) * bulk * trace_e;

  // Compute deviatoric part of degraded stress
  RatelScalarMatMultSymmetric(2. * mu * g, e_dev_sym, sigma_degr_sym);

  // Add volumetric part in-place (and only diagonal values) to save memory and flops
  for (CeedInt i = 0; i < 3; i++) sigma_degr_sym[i] += sigma_vol_diag;
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_DegradedStress_Linear (9 + FLOPS_ScalarMatMultSymmetric + 3)

/**
  @brief Compute linearization of `sigma_degr` for linear damage elasticity

  `dsigma_degr = g(phi) * dsigma_vol + g(phi) * dsigma_dev + dg * sigma_vol + dg * sigma_dev, if trace_e >= 0`
  `dsigma_degr = dsigma_vol + g(phi) * dsigma_dev + dg * sigma_dev, if trace_e < 0`

  @param[in]   mu               Shear modulus
  @param[in]   bulk             Bulk modulus
  @param[in]   eta              Residual stiffness
  @param[in]   use_offdiagonal  Flag to use off-diagonal terms
  @param[in]   phi              Damage field
  @param[in]   trace_e          trace strain tensor
  @param[in]   dphi             Increment of damage field
  @param[in]   trace_de         Trace of incremental linear strain
  @param[in]   e_dev_sym        deviatoric strain tensor in symmetric representation
  @param[in]   de_dev_sym       deviatoric strain tensor increment in symmetric representation
  @param[out]  dsigma_degr_sym  Linearization of degraded stress in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int ComputeDegradedStress_Linear_fwd(CeedScalar mu, CeedScalar bulk, CeedScalar eta, bool use_offdiagonal, CeedScalar phi,
                                                           CeedScalar trace_e, CeedScalar dphi, CeedScalar trace_de, const CeedScalar e_dev_sym[6],
                                                           const CeedScalar de_dev_sym[6], CeedScalar dsigma_degr_sym[6]) {
  // Compute g(phi) = (1 - eta)(1 - phi)^2 + eta
  const bool       trace_e_positive = trace_e > 0;
  const CeedScalar one_minus_phi    = 1 - phi;
  const CeedScalar g = one_minus_phi * one_minus_phi * (1 - eta) + eta, dsigma_vol_diag = (trace_e_positive ? g : 1.) * bulk * trace_de;

  // Compute deviatoric part of degraded stress
  RatelScalarMatMultSymmetric(2. * mu * g, de_dev_sym, dsigma_degr_sym);

  // Add volumetric part in-place (and only diagonal values) to save memory and flops
  for (CeedInt i = 0; i < 3; i++) dsigma_degr_sym[i] += dsigma_vol_diag;

  if (use_offdiagonal) {
    // Compute dSigma_degr/dphi = dg/dphi * dphi * Sigma_degr (if offdiagonal term are considered)
    const CeedScalar dg_dphi        = -2 * one_minus_phi * (1 - eta);
    const CeedScalar sigma_vol_diag = (trace_e > 0) * bulk * trace_e;
    CeedScalar       sigma_degr_sym[6];

    RatelScalarMatMultSymmetric(2. * mu, e_dev_sym, sigma_degr_sym);
    // In-place to save memory
    // Add sigma_vol_sym[6] = trace_e > 0 ? {bulk * trace_e, bulk * trace_e, bulk * trace_e, 0., 0., 0.} : {0.}
    for (CeedInt i = 0; i < 3; i++) sigma_degr_sym[i] += sigma_vol_diag;
    RatelMatMatAddSymmetric(1., dsigma_degr_sym, dg_dphi * dphi, sigma_degr_sym, dsigma_degr_sym);
  }
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_DegradedStress_Linear_fwd (9 + FLOPS_ScalarMatMultSymmetric + 3 + 6 + FLOPS_ScalarMatMultSymmetric + 3 + FLOPS_MatMatAddSymmetric + 6)

/**
  @brief Compute u_t term of damage residual

  @param[in]   ctx  QFunction context, holding `RatelElasticityDamageParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - volumetric qdata
                      - 2 - u_t (four components: 3 displacement components, 1 scalar damage field)
  @param[out]  out  Output arrays
                      - 2 - action on u_t

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityDamageResidual_ut_Linear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u_t)[CEED_Q_VLA]    = (const CeedScalar(*)[CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*v_t)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Context Elasticity+Damage
  const RatelElasticityDamageParams *context        = (RatelElasticityDamageParams *)ctx;
  const CeedScalar                   xi             = context->damage_viscosity;
  const CeedScalar                   damage_scaling = context->damage_scaling;

  // Quadrature point loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    const CeedScalar wdetJ = q_data[0][i];

    // Update values
    v_t[0][i] = 0;
    v_t[1][i] = 0;
    v_t[2][i] = 0;
    v_t[3][i] = damage_scaling * xi * u_t[3][i] * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute residual for linear elasticity with damage

  @param[in]   ctx  QFunction context, holding `RatelElasticityDamageParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - volumetric qdata
                      - 1 - Psi_plus and damage state
                      - 2 - u (four components: 3 displacement components, 1 scalar damage field)
                      - 3 - u_g gradient of u with respect to reference coordinates
  @param[out]  out  Output arrays
                      - 0 - Updated Psi_plus and damage state
                      - 1 - stored vector
                      - 2 - action on u
                      - 3 - action on u_g

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityDamageResidual_Linear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*state)              = in[1];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[2];
  const CeedScalar(*u_g)[4][CEED_Q_VLA] = (const CeedScalar(*)[4][CEED_Q_VLA])in[3];

  // Outputs
  CeedScalar(*current_state)       = out[0];
  CeedScalar(*stored)              = out[1];
  CeedScalar(*v)[CEED_Q_VLA]       = (CeedScalar(*)[CEED_Q_VLA])out[2];
  CeedScalar(*dvdX)[4][CEED_Q_VLA] = (CeedScalar(*)[4][CEED_Q_VLA])out[3];

  // Context Elasticity+Damage
  const RatelElasticityDamageParams *context        = (RatelElasticityDamageParams *)ctx;
  const CeedScalar                   mu             = context->mu;
  const CeedScalar                   bulk           = context->bulk;
  const CeedScalar                   Gc             = context->fracture_toughness;
  const CeedScalar                   l0             = context->characteristic_length;
  const CeedScalar                   eta            = context->residual_stiffness;
  const CeedScalar                   damage_scaling = context->damage_scaling;
  const bool                         use_AT1        = context->use_AT1;

  const CeedScalar Psi_plus_critical = use_AT1 ? 3 * Gc / (8 * 2 * l0) : 0;
  const CeedScalar c0                = use_AT1 ? 8. / 3. : 2.;

  // Quadrature point loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar Psi_plus_history_flag, H, Psi_plus;
    CeedScalar grad_phi[3], e_sym[6], e_dev_sym[6], sigma_degr_sym[6], sigma_degr[3][3], dXdx[3][3], grad_u[3][3];

    // dX/dx from Q_data
    const CeedScalar wdetJ = q_data[0][i];
    RatelQdataUnpack(Q, i, q_data, dXdx);

    //----- Displacement problem
    // Read spatial derivatives of u; du/dX
    const CeedScalar dudX[3][3] = {
        {u_g[0][0][i], u_g[1][0][i], u_g[2][0][i]},
        {u_g[0][1][i], u_g[1][1][i], u_g[2][1][i]},
        {u_g[0][2][i], u_g[1][2][i], u_g[2][2][i]}
    };

    // Compute grad_u = du/dX * dX/dx
    // X is ref coordinate [-1,1]^3; x is physical coordinate
    RatelMatMatMult(1.0, dudX, dXdx, grad_u);

    // Compute strain e
    RatelLinearStrain(grad_u, e_sym);

    // Compute deviatoric strain ed = e - ev
    const CeedScalar trace_e = RatelMatTraceSymmetric(e_sym);
    RatelMatDeviatoricSymmetric(trace_e, e_sym, e_dev_sym);

    // Read damage (forth component)
    const CeedScalar phi = u[3][i];

    // Compute (grad(v), sigma_degr)
    ComputeDegradedStress_Linear(mu, bulk, eta, phi, trace_e, e_dev_sym, sigma_degr_sym);
    RatelSymmetricMatUnpack(sigma_degr_sym, sigma_degr);
    for (CeedInt j = 0; j < 3; j++) {
      for (CeedInt k = 0; k < 3; k++) {
        dvdX[j][k][i] = 0;
        for (CeedInt m = 0; m < 3; m++) {
          dvdX[j][k][i] += wdetJ * dXdx[j][m] * sigma_degr[k][m];
        }
      }
    }

    //----- Damage problem
    // Compute Psi_plus
    PsiPlus_Linear(mu, bulk, trace_e, e_dev_sym, &Psi_plus);

    // Treat Psi_plus as a history variable and ensure that increases monotonically for crack irreversibility
    const CeedScalar Psi_plus_old = state[i];
    Psi_plus_history_flag         = Psi_plus > Psi_plus_old;
    Psi_plus                      = RatelMax(Psi_plus, Psi_plus_old);
    Psi_plus                      = RatelMax(Psi_plus_critical, Psi_plus);

    // Spatial derivative of phi
    const CeedScalar phi_g[3] = {u_g[0][3][i], u_g[1][3][i], u_g[2][3][i]};

    // Compute grad_phi = dX/dx^T dphi/dX
    RatelMatTransposeVecMult(1.0, dXdx, phi_g, grad_phi);
    const CeedScalar dg_dphi = -2 * (1 - eta) * (1 - phi);

    // If AT2, substitute appropiate value for alpha (hence dalpha)
    const CeedScalar dalpha_dphi = use_AT1 ? 1. : 2. * phi;

    // Compute H and update v values
    H       = dg_dphi * Psi_plus + (Gc / (c0 * l0)) * dalpha_dphi;
    v[0][i] = 0;
    v[1][i] = 0;
    v[2][i] = 0;
    v[3][i] = damage_scaling * H * wdetJ;

    // Update fourth component of dvdX via (grad(q), 2 Gc l0 / c0 grad(phi))
    for (CeedInt j = 0; j < 3; j++) {
      dvdX[j][3][i] = damage_scaling * (grad_phi[0] * dXdx[j][0] + grad_phi[1] * dXdx[j][1] + grad_phi[2] * dXdx[j][2]) * 2. * Gc * l0 * wdetJ / c0;
    }

    // Save updated values for state fields
    RatelStoredValuesPack(Q, i, 0, 1, &Psi_plus, current_state);

    // Store values
    RatelStoredValuesPack(Q, i, 0, 1, &phi, stored);
    RatelStoredValuesPack(Q, i, 1, 6, e_sym, stored);
    RatelStoredValuesPack(Q, i, 7, 1, &Psi_plus_history_flag, stored);
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute jacobian for linear elasticity with damage

  @param[in]   ctx  QFunction context, holding `RatelElasticityDamageParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - volumetric qdata
                      - 1 - Current state values
                      - 2 - Stored values
                      - 3 - du (four components: 3 displacement components, 1 damage field)
                      - 4 - du_g gradient of du with respect to reference coordinates
  @param[out]  out  Output arrays
                      - 0 - action on du
                      - 1 - action on du_g

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityDamageJacobian_Linear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA]  = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*current_state)       = in[1];
  const CeedScalar(*stored)              = in[2];
  const CeedScalar(*du)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[3];
  const CeedScalar(*du_g)[4][CEED_Q_VLA] = (const CeedScalar(*)[4][CEED_Q_VLA])in[4];

  // Outputs
  CeedScalar(*dv)[CEED_Q_VLA]       = (CeedScalar(*)[CEED_Q_VLA])out[0];
  CeedScalar(*ddvdX)[4][CEED_Q_VLA] = (CeedScalar(*)[4][CEED_Q_VLA])out[1];

  // Context Elasticity+Damage
  const RatelElasticityDamageParams *context         = (RatelElasticityDamageParams *)ctx;
  const CeedScalar                   mu              = context->mu;
  const CeedScalar                   bulk            = context->bulk;
  const CeedScalar                   Gc              = context->fracture_toughness;
  const CeedScalar                   l0              = context->characteristic_length;
  const CeedScalar                   eta             = context->residual_stiffness;
  const CeedScalar                   damage_scaling  = context->damage_scaling;
  const CeedScalar                   xi              = context->damage_viscosity;
  const bool                         use_AT1         = context->use_AT1;
  const bool                         use_offdiagonal = context->use_offdiagonal;
  const CeedScalar                   shift_v         = context->common_parameters[1];

  // Set d2alpha_dphi2 and c0 based on AT1 or AT2
  const CeedScalar c0            = use_AT1 ? 8. / 3. : 2.;
  const CeedScalar d2alpha_dphi2 = use_AT1 ? 0. : 2.;
  const CeedScalar d2g_dphi2     = 2 * (1 - eta);

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar Psi_plus_history_flag, dPsi_plus, dH;
    CeedScalar Psi_plus, phi, grad_dphi[3], e_sym[6], de_sym[6], e_dev_sym[6], de_dev_sym[6];
    CeedScalar dXdx[3][3], grad_du[3][3], dsigma_degr_sym[6], dsigma_degr[3][3];

    // Read spatial derivatives of increment u; d(du)/dX = d(du)/dX
    const CeedScalar ddudX[3][3] = {
        {du_g[0][0][i], du_g[1][0][i], du_g[2][0][i]},
        {du_g[0][1][i], du_g[1][1][i], du_g[2][1][i]},
        {du_g[0][2][i], du_g[1][2][i], du_g[2][2][i]}
    };

    // Extract Qdata
    const CeedScalar wdetJ = q_data[0][i];
    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Compute grad_du = ddu/dX * dX/dx
    // X is ref coordinate [-1,1]^3; x is physical coordinate
    RatelMatMatMult(1.0, ddudX, dXdx, grad_du);

    // Compute de = 1/2 (grad_du + (grad_du)^T)
    RatelLinearStrain_fwd(grad_du, de_sym);

    // Compute deviatoric strain increment d(e_d)
    const CeedScalar trace_de = RatelMatTraceSymmetric(de_sym);
    RatelMatDeviatoricSymmetric(trace_de, de_sym, de_dev_sym);

    // Unpack strain tensor and phi
    RatelStoredValuesUnpack(Q, i, 1, 6, stored, e_sym);
    const CeedScalar trace_e = RatelMatTraceSymmetric(e_sym);
    RatelStoredValuesUnpack(Q, i, 0, 1, stored, &phi);

    // Retrieve dphi and e_dev
    const CeedScalar dphi = du[3][i];
    RatelMatDeviatoricSymmetric(trace_e, e_sym, e_dev_sym);

    // Compute dSigma_degr
    ComputeDegradedStress_Linear_fwd(mu, bulk, eta, use_offdiagonal, phi, trace_e, dphi, trace_de, e_dev_sym, de_dev_sym, dsigma_degr_sym);
    RatelSymmetricMatUnpack(dsigma_degr_sym, dsigma_degr);
    for (CeedInt j = 0; j < 3; j++) {
      for (CeedInt k = 0; k < 3; k++) {
        ddvdX[j][k][i] = 0;
        for (CeedInt m = 0; m < 3; m++) {
          ddvdX[j][k][i] += wdetJ * dXdx[j][m] * dsigma_degr[k][m];
        }
      }
    }

    //--Compute l0*Gc*(grad(dphi), grad(q))
    // Spatial derivative of phi
    const CeedScalar dphi_g[3] = {du_g[0][3][i], du_g[1][3][i], du_g[2][3][i]};

    // grad_dphi = dX/dx^T * d(dphi)/dX
    RatelMatTransposeVecMult(1.0, dXdx, dphi_g, grad_dphi);

    // Retrieve Psi_plus_history_flag (Psi_plus_history_flag=0, if Psi_plus is "unloading")
    RatelStoredValuesUnpack(Q, i, 7, 1, stored, &Psi_plus_history_flag);
    // Unpack state fields
    RatelStoredValuesUnpack(Q, i, 0, 1, current_state, &Psi_plus);

    const CeedScalar dg_dphi = -2 * (1 - eta) * (1 - phi);

    // Compute d_phi_t
    const CeedScalar dphi_t = shift_v * dphi;

    // Compute dPsi_plus
    const bool add_offdiag = !(Psi_plus_history_flag == 0. || !use_offdiagonal);
    PsiPlus_Linear_fwd(mu, bulk, trace_e, e_dev_sym, de_sym, &dPsi_plus);
    dPsi_plus = add_offdiag * dPsi_plus;

    // dH and dv values
    dH       = d2g_dphi2 * dphi * Psi_plus + dg_dphi * dPsi_plus + (Gc / (c0 * l0)) * d2alpha_dphi2 * dphi + xi * dphi_t;
    dv[0][i] = 0;
    dv[1][i] = 0;
    dv[2][i] = 0;
    dv[3][i] = damage_scaling * dH * wdetJ;

    // Update fourth component of dvdX via (grad(dq), 2 Gc l0 / c0 grad(dphi))
    const CeedScalar scale = damage_scaling * 2. * Gc * l0 * wdetJ / c0;
    for (CeedInt j = 0; j < 3; j++) {
      ddvdX[j][3][i] = scale * (grad_dphi[0] * dXdx[j][0] + grad_dphi[1] * dXdx[j][1] + grad_dphi[2] * dXdx[j][2]);
    }
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_JACOBIAN_Elasticity_Damage                                                                                                 \
  (FLOPS_MatMatMult + FLOPS_LinearStrain_fwd + 2 * FLOPS_MatTrace + 2 * FLOPS_MatDeviatoricSymmetric + FLOPS_DegradedStress_Linear_fwd + \
   FLOPS_MatMatMult + 9 + FLOPS_MatVecMult + 4 + FLOPS_PsiPlus_Linear_fwd + 18 + 3 * 6)

/**
  @brief Compute projected diagnostic values for linear elasticity with damage

  @param[in]   ctx  QFunction context holding RatelElasticityDamageParams
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 2 - u (four components: 3 displacement components, 1 scalar damage field)
                      - 3 - gradient of u with respect to reference coordinates
  @param[out]  out  Output arrays
                      - 0 - projected diagnostic values

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityDamageDiagnostic_Linear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[2];
  const CeedScalar(*ug)[4][CEED_Q_VLA]  = (const CeedScalar(*)[4][CEED_Q_VLA])in[3];

  // Outputs
  CeedScalar(*diagnostic)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Context
  const RatelElasticityDamageParams *context = (RatelElasticityDamageParams *)ctx;
  const CeedScalar                   mu      = context->mu;
  const CeedScalar                   bulk    = context->bulk;
  const CeedScalar                   eta     = context->residual_stiffness;
  const CeedScalar                   rho     = context->common_parameters[RATEL_COMMON_PARAMETER_RHO];
  const CeedScalar                   Gc      = context->fracture_toughness;
  const CeedScalar                   l0      = context->characteristic_length;
  const bool                         use_AT1 = context->use_AT1;

  // Set c0 based on AT1 or AT2
  const CeedScalar c0 = use_AT1 ? 8. / 3. : 2.;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar grad_phi[3], e_sym[6], e_dev_sym[6], dXdx[3][3], grad_u[3][3], sigma_degr_sym[6], sigma_dev_degr_sym[6], Psi_plus;

    // Read spatial derivatives of u
    const CeedScalar dudX[3][3] = {
        {ug[0][0][i], ug[1][0][i], ug[2][0][i]},
        {ug[0][1][i], ug[1][1][i], ug[2][1][i]},
        {ug[0][2][i], ug[1][2][i], ug[2][2][i]}
    };
    // -- Qdata
    const CeedScalar wdetJ = q_data[0][i];
    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Compute grad_u
    RatelMatMatMult(1.0, dudX, dXdx, grad_u);

    // Compute strain e
    RatelLinearStrain(grad_u, e_sym);

    // Compute deviatoric strain ed = e - ev
    const CeedScalar trace_e = RatelMatTraceSymmetric(e_sym);
    RatelMatDeviatoricSymmetric(trace_e, e_sym, e_dev_sym);

    // Read damage (forth component)
    const CeedScalar phi = u[3][i];

    // Compute sigma_degr
    ComputeDegradedStress_Linear(mu, bulk, eta, phi, trace_e, e_dev_sym, sigma_degr_sym);

    // Compute volumetric and deviatoric strain energy
    const CeedScalar Psi_vol = 0.5 * bulk * trace_e * trace_e;
    const CeedScalar Psi_dev = mu * RatelMatMatContractSymmetric(1.0, e_dev_sym, e_dev_sym);

    // Compute Psi_plus
    const CeedScalar g        = (1 - phi) * (1 - phi) * (1 - eta) + eta;
    const CeedScalar phi_g[3] = {ug[0][3][i], ug[1][3][i], ug[2][3][i]};
    const CeedScalar alpha    = use_AT1 ? phi : phi * phi;

    // Compute grad_phi = dX/dx^T dphi/dX
    RatelMatTransposeVecMult(1.0, dXdx, phi_g, grad_phi);
    const CeedScalar norm_grad_phi2 = RatelDot3(grad_phi, grad_phi);

    // Compute psi_plus and psi_minus
    PsiPlus_Linear(mu, bulk, trace_e, e_dev_sym, &Psi_plus);
    const CeedScalar Psi_minus = (trace_e > 0) ? 0 : Psi_vol;

    // Strain energy W_bar(u, phi)
    const CeedScalar strain_energy = g * Psi_plus + Psi_minus + Gc * (alpha + l0 * l0 * norm_grad_phi2) / (c0 * l0);

    //--Store diagnostics
    // Displacement
    diagnostic[0][i] = u[0][i];
    diagnostic[1][i] = u[1][i];
    diagnostic[2][i] = u[2][i];

    // Cauchy stress tensor
    diagnostic[3][i] = sigma_degr_sym[0];
    diagnostic[4][i] = sigma_degr_sym[1];
    diagnostic[5][i] = sigma_degr_sym[2];
    diagnostic[6][i] = sigma_degr_sym[3];
    diagnostic[7][i] = sigma_degr_sym[4];
    diagnostic[8][i] = sigma_degr_sym[5];

    // Pressure
    diagnostic[9][i] = -bulk * trace_e;

    // Strain tensor invariants
    diagnostic[10][i] = trace_e;

    const CeedScalar trace_e2 = RatelMatMatContractSymmetric(1.0, e_sym, e_sym);

    diagnostic[11][i] = trace_e2;
    diagnostic[12][i] = 1 + trace_e;

    diagnostic[13][i] = strain_energy;

    // von Mises stress
    const CeedScalar trace_sigma_degr = RatelMatTraceSymmetric(sigma_degr_sym);
    RatelMatDeviatoricSymmetric(trace_sigma_degr, sigma_degr_sym, sigma_dev_degr_sym);
    const CeedScalar sigma_dev_degr_contract = RatelMatMatContractSymmetric(1.0, sigma_dev_degr_sym, sigma_dev_degr_sym);

    diagnostic[14][i] = sqrt(3 * sigma_dev_degr_contract / 2);

    // Compute mass density values

    // -- Set to 0 as default (rho should be 0.0 as defualt anyways)
    diagnostic[15][i] = 0.0;

    // -- if mass density is requested then compute mass density: rho/J
    if (rho > 0.0) diagnostic[15][i] = rho / diagnostic[12][i];

    // Additional diagnostic quantities for damage model
    // Damage
    diagnostic[16][i] = phi;

    // Psi_vol (undegraded)
    diagnostic[17][i] = Psi_vol;

    // Psi_dev (undegraded)
    diagnostic[18][i] = Psi_dev;

    for (CeedInt j = 0; j < NUM_COMPONENTS_DIAGNOSTIC_Elasticity_Damage; j++) diagnostic[j][i] *= wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute (degraded) strain energy for linear elasticity+damage

  @param[in]   ctx  QFunction context holding RatelElasticityDamageParams
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 2 - u (4 components)
                      - 3 - gradient of u with respect to reference coordinates
  @param[out]  out  Output arrays
                      - 0 - (degraded) strain energy

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityDamageStrainEnergy_Linear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[2];
  const CeedScalar(*ug)[4][CEED_Q_VLA]  = (const CeedScalar(*)[4][CEED_Q_VLA])in[3];

  // Outputs
  CeedScalar(*degr_energy) = (CeedScalar(*))out[0];

  // Context
  const RatelElasticityDamageParams *context = (RatelElasticityDamageParams *)ctx;
  const CeedScalar                   mu      = context->mu;
  const CeedScalar                   bulk    = context->bulk;
  const CeedScalar                   eta     = context->residual_stiffness;
  const CeedScalar                   Gc      = context->fracture_toughness;
  const CeedScalar                   l0      = context->characteristic_length;
  const bool                         use_AT1 = context->use_AT1;

  // Set c0 based on AT1 or AT2
  const CeedScalar c0 = use_AT1 ? 8. / 3. : 2.;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar grad_phi[3], e_sym[6], e_dev_sym[6], dXdx[3][3], grad_u[3][3], Psi_plus;

    // Read spatial derivatives of u
    const CeedScalar dudX[3][3] = {
        {ug[0][0][i], ug[1][0][i], ug[2][0][i]},
        {ug[0][1][i], ug[1][1][i], ug[2][1][i]},
        {ug[0][2][i], ug[1][2][i], ug[2][2][i]}
    };
    // -- Qdata
    const CeedScalar wdetJ = q_data[0][i];
    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Compute grad_u
    RatelMatMatMult(1.0, dudX, dXdx, grad_u);

    // Compute strain e
    RatelLinearStrain(grad_u, e_sym);

    // Compute deviatoric strain ed = e - ev
    const CeedScalar trace_e = RatelMatTraceSymmetric(e_sym);
    RatelMatDeviatoricSymmetric(trace_e, e_sym, e_dev_sym);

    // Compute Psi
    const CeedScalar phi      = u[3][i];
    const CeedScalar g        = (1 - phi) * (1 - phi) * (1 - eta) + eta;
    const CeedScalar phi_g[3] = {ug[0][3][i], ug[1][3][i], ug[2][3][i]};
    const CeedScalar alpha    = use_AT1 ? phi : phi * phi;
    const CeedScalar Psi_vol  = 0.5 * bulk * trace_e * trace_e;

    // Compute grad_phi = dX/dx^T dphi/dX
    RatelMatTransposeVecMult(1.0, dXdx, phi_g, grad_phi);
    const CeedScalar norm_grad_phi2 = RatelDot3(grad_phi, grad_phi);

    // Compute psi_plus and psi_minus
    PsiPlus_Linear(mu, bulk, trace_e, e_dev_sym, &Psi_plus);
    const CeedScalar Psi_minus = (trace_e > 0) ? 0 : Psi_vol;

    // Strain energy W_bar(u, phi)
    const CeedScalar strain_energy = g * Psi_plus + Psi_minus + Gc * (alpha + l0 * l0 * norm_grad_phi2) / (c0 * l0);

    degr_energy[i] = strain_energy * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute dual space diagnostic values for linear elasticity with damage

  @param[in]   ctx  QFunction context, unused
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - volumetric qdata
  @param[out]  out  Output array
                      - 0 - nodal volume

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityDamageDualDiagnostic_Linear)(void *ctx, const CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[4][CEED_Q_VLA]  = (const CeedScalar(*)[4][CEED_Q_VLA])in[2];

  // Outputs
  CeedScalar(*v)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Context
  const CeedScalar *context = (const CeedScalar *)ctx;
  const CeedScalar  rho_0   = context[RATEL_COMMON_PARAMETER_RHO];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar e_sym[6], dXdx[3][3], grad_u[3][3];

    // Read spatial derivatives of u
    const CeedScalar dudX[3][3] = {
        {ug[0][0][i], ug[1][0][i], ug[2][0][i]},
        {ug[0][1][i], ug[1][1][i], ug[2][1][i]},
        {ug[0][2][i], ug[1][2][i], ug[2][2][i]}
    };
    // -- Qdata
    const CeedScalar wdetJ = q_data[0][i];
    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Compute grad_u
    RatelMatMatMult(1.0, dudX, dXdx, grad_u);

    // Compute strain e
    RatelLinearStrain(grad_u, e_sym);

    // Compute trace(e)
    const CeedScalar trace_e = RatelMatTraceSymmetric(e_sym);

    // Compute approximate volume fraction J = 1 + trace(e)
    const CeedScalar J = 1 + trace_e;

    // Nodal volume
    v[0][i] = 1.0 * wdetJ;

    // Nodal density: rho_0 = rho / J
    v[1][i] = rho_0 / J * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute platens residual for linear elasticity with damage

  @param[in]   ctx          QFunction context, holding `RatelElasticityDamageParams`
  @param[in]   Q            Number of quadrature points
  @param[in]   i            Index
  @param[in]   in           Input arrays
                              - 0 - volumetric qdata
                              - 2 - u (four components: 3 displacement components, 1 scalar damage field)
                              - 3 - u_g gradient of u with respect to reference coordinates
  @param[out]  out          Output arrays
                              - 1 - stored vector
                              - 2 - initializing v
  @param[out]  dXdx         dXdx
  @param[out]  sigma_degr   Degraded stress tensor

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int ElasticityDamagePlatenResidual_Linear(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in,
                                                                CeedScalar *const *out, CeedScalar dXdx[3][3], CeedScalar sigma_degr[3][3]) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[2];
  const CeedScalar(*u_g)[4][CEED_Q_VLA] = (const CeedScalar(*)[4][CEED_Q_VLA])in[3];

  // Outputs
  CeedScalar(*state_out)     = out[0];
  CeedScalar(*stored)        = out[1];
  CeedScalar(*v)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[2];

  // Context
  const RatelElasticityDamageParams *context = (RatelElasticityDamageParams *)ctx;
  const CeedScalar                   mu      = context->mu;
  const CeedScalar                   bulk    = context->bulk;
  const CeedScalar                   eta     = context->residual_stiffness;

  CeedScalar e_sym[6], e_dev_sym[6], sigma_degr_sym[6], grad_u[3][3];

  // dX/dx from Q_data
  RatelQdataUnpack(Q, i, q_data, dXdx);

  //----- Displacement problem
  // Spatial derivatives of u
  const CeedScalar du[3][3] = {
      {u_g[0][0][i], u_g[1][0][i], u_g[2][0][i]},
      {u_g[0][1][i], u_g[1][1][i], u_g[2][1][i]},
      {u_g[0][2][i], u_g[1][2][i], u_g[2][2][i]}
  };

  // Compute grad_u = du*dX/dx
  RatelMatMatMult(1.0, du, dXdx, grad_u);

  // Compute strain e
  RatelLinearStrain(grad_u, e_sym);

  // Compute deviatoric strain ed = e - ev
  const CeedScalar trace_e = RatelMatTraceSymmetric(e_sym);
  RatelMatDeviatoricSymmetric(trace_e, e_sym, e_dev_sym);

  // Read damage (forth component)
  const CeedScalar phi = u[3][i];

  // Compute (grad(v), sigma_degr)
  ComputeDegradedStress_Linear(mu, bulk, eta, phi, trace_e, e_dev_sym, sigma_degr_sym);
  RatelSymmetricMatUnpack(sigma_degr_sym, sigma_degr);

  //----- Update stored
  RatelStoredValuesPack(Q, i, 0, 1, &phi, stored);
  RatelStoredValuesPack(Q, i, 1, 6, (CeedScalar *)e_sym, stored);
  CeedScalar value_for_init = 0;

  RatelStoredValuesPack(Q, i, 7, 1, &value_for_init, stored);

  // Initialize state variable and damage component to zero
  state_out[i] = value_for_init;
  v[3][i]      = 0;
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute platens jacobian for linear elasticity with damage

  @param[in]   ctx          QFunction context, holding `RatelElasticityDamageParams`
  @param[in]   Q            Number of quadrature points
  @param[in]   i            Index
  @param[in]   in           Input arrays
                              - 0 - volumetric qdata
                              - 2 - Stored values
                              - 3 - du (four components: 3 displacement components, 1 damage field)
                              - 4 - du_g gradient of du with respect to reference coordinates
  @param[out]  out          Output arrays
                              - 0 - initializing dv
  @param[out]  dXdx         dXdx
  @param[out]  dsigma_degr  Linearization of degraded stress tensor

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int ElasticityDamagePlatenJacobian_Linear(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in,
                                                                CeedScalar *const *out, CeedScalar dXdx[3][3], CeedScalar dsigma_degr[3][3]) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA]  = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*stored)              = in[2];
  const CeedScalar(*du)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[3];
  const CeedScalar(*du_g)[4][CEED_Q_VLA] = (const CeedScalar(*)[4][CEED_Q_VLA])in[4];

  // Outputs
  CeedScalar(*dv)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  CeedScalar phi, e_sym[6], de_sym[6], e_dev_sym[6], de_dev_sym[6], dsigma_degr_sym[6], grad_du[3][3];

  // Context
  const RatelElasticityDamageParams *context         = (RatelElasticityDamageParams *)ctx;
  const CeedScalar                   mu              = context->mu;
  const CeedScalar                   bulk            = context->bulk;
  const CeedScalar                   eta             = context->residual_stiffness;
  const bool                         use_offdiagonal = context->use_offdiagonal;

  //--Compute dsigma_term = (1 - phi)^2 * (bulk * dev + mu * ded)
  // Read derivatives of delta du
  const CeedScalar ddudX[3][3] = {
      {du_g[0][0][i], du_g[1][0][i], du_g[2][0][i]},
      {du_g[0][1][i], du_g[1][1][i], du_g[2][1][i]},
      {du_g[0][2][i], du_g[1][2][i], du_g[2][2][i]}
  };

  // Extract Qdata
  RatelQdataUnpack(Q, i, q_data, dXdx);

  // Compute grad_du = ddu*dXdx
  RatelMatMatMult(1.0, ddudX, dXdx, grad_du);

  // Compute de = 1/2 (grad_du + (grad_du)^T)
  RatelLinearStrain_fwd(grad_du, de_sym);

  // Compute deviatoric strain increment d(e_d)
  const CeedScalar trace_de = RatelMatTraceSymmetric(de_sym);
  RatelMatDeviatoricSymmetric(trace_de, de_sym, de_dev_sym);

  // Unpack strain tensor
  RatelStoredValuesUnpack(Q, i, 1, 6, stored, e_sym);
  const CeedScalar trace_e = RatelMatTraceSymmetric(e_sym);

  // Compute sigma_term
  RatelStoredValuesUnpack(Q, i, 0, 1, stored, &phi);

  //--Compute dphi_term = -2*(1-phi)*dphi*sigma
  // Compute Deviatoric Strain ed = e - ev
  RatelMatDeviatoricSymmetric(trace_e, e_sym, e_dev_sym);

  // Retrieve dphi
  const CeedScalar dphi = du[3][i];

  // Compute dSigma_degr
  ComputeDegradedStress_Linear_fwd(mu, bulk, eta, use_offdiagonal, phi, trace_e, dphi, trace_de, e_dev_sym, de_dev_sym, dsigma_degr_sym);
  RatelSymmetricMatUnpack(dsigma_degr_sym, dsigma_degr);

  // Initialize damage component to zero
  dv[3][i] = 0;
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_JACOBIAN_Elasticity_Damage_Platen \
  (FLOPS_MatMatMult + FLOPS_LinearStrain_fwd + 2 * FLOPS_MatTrace + 2 * FLOPS_MatDeviatoricSymmetric + FLOPS_DegradedStress_Linear_fwd)

/**
  @brief Compute platen residual for linear elasticity+damage

  @param[in]   ctx  QFunction context, holding `RatelBCPlatenParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(PlatenBCsResidualElasticityDamage_Linear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return PlatenBCs(ctx, Q, ElasticityDamagePlatenResidual_Linear, !!NUM_COMPONENTS_STATE_Elasticity_Damage, !!NUM_COMPONENTS_STORED_Elasticity_Damage,
                   NUM_ACTIVE_FIELD_EVAL_MODES_Elasticity_Damage, in, out);
}

/**
  @brief Evaluate platen Jacobian for linear elasticity+damage

  @param[in]   ctx  QFunction context, holding `RatelBCPlatenParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(PlatenBCsJacobianElasticityDamage_Linear)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return PlatenBCs_Jacobian(ctx, Q, ElasticityDamagePlatenJacobian_Linear, !!NUM_COMPONENTS_STATE_Elasticity_Damage,
                            !!NUM_COMPONENTS_STORED_Elasticity_Damage, NUM_ACTIVE_FIELD_EVAL_MODES_Elasticity_Damage, in, out);
}

/// @}
