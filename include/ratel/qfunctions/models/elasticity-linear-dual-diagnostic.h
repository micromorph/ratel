/// @file
/// Ratel common dual space diagnostic values for elasticity
#include <ceed/types.h>

#include "../../models/common-parameters.h"  // IWYU pragma: export
#include "../utils.h"                        // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

#define NUM_COMPONENTS_ELASTICITY_LINEAR_DIAGNOSTIC_Dual 2

/**
  @brief Compute dual space diagnostic values

  @param[in]   ctx  QFunction context, unused
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - volumetric qdata
  @param[out]  out  Output array
                      - 0 - nodal volume

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ElasticityDualDiagnostic_Linear)(void *ctx, const CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[2];

  // Outputs
  CeedScalar(*v)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Context
  const CeedScalar *context = (const CeedScalar *)ctx;
  const CeedScalar  rho_0   = context[RATEL_COMMON_PARAMETER_RHO];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar e_sym[6], dXdx[3][3], grad_u[3][3];

    // -- Qdata
    const CeedScalar wdetJ = q_data[0][i];
    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Read spatial derivatives of u
    const CeedScalar dudX[3][3] = {
        {ug[0][0][i], ug[1][0][i], ug[2][0][i]},
        {ug[0][1][i], ug[1][1][i], ug[2][1][i]},
        {ug[0][2][i], ug[1][2][i], ug[2][2][i]}
    };

    // Compute grad_u
    RatelMatMatMult(1.0, dudX, dXdx, grad_u);

    // Compute strain e
    RatelLinearStrain(grad_u, e_sym);

    // Compute trace(e)
    const CeedScalar trace_e = RatelMatTraceSymmetric(e_sym);

    // Compute approximate volume fraction J = 1 + trace(e)
    const CeedScalar J = 1 + trace_e;

    // Nodal volume
    v[0][i] = 1.0 * wdetJ;

    // Nodal density: rho_0 = rho / J
    v[1][i] = rho_0 / J * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
