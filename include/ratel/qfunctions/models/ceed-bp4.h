/// @file
/// Ratel CEED BP4 QFunction source
#include <ceed/types.h>

#include "../utils.h"  // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

/**
  @brief Compute the residual for the CEED vector BP problem

  @param[in]   ctx  QFunction context, unused
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - volumetric qdata
                      - 1 - gradient of u
  @param[out]  out  Output array
                      - 0 - action of the QFunction

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(Residual_CEED_BP4)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*vg)[3][CEED_Q_VLA] = (CeedScalar(*)[3][CEED_Q_VLA])out[0];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Read spatial derivatives of u; du/dX
    CeedScalar dudX[3][3];
    RatelGradUnpack(Q, i, ug, dudX);

    // Read q_data (dXdxdXdx_T symmetric matrix)
    const CeedScalar dXdxdXdx_T[3][3] = {
        {q_data[1][i], q_data[6][i], q_data[5][i]},
        {q_data[6][i], q_data[2][i], q_data[4][i]},
        {q_data[5][i], q_data[4][i], q_data[3][i]}
    };

    for (CeedInt k = 0; k < 3; k++) {    // k = component
      for (CeedInt j = 0; j < 3; j++) {  // j = direction of vg
        vg[j][k][i] = (dudX[k][0] * dXdxdXdx_T[0][j] + dudX[k][1] * dXdxdXdx_T[1][j] + dudX[k][2] * dXdxdXdx_T[2][j]);
      }
    }
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute the Jacobian for the CEED vector BP problem

  @param[in]   ctx  QFunction context, unused
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - volumetric qdata
                      - 1 - gradient of du
  @param[out]  out  Output array
                      - 0 - action of the QFunction

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(Jacobian_CEED_BP4)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*dug)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*dvg)[3][CEED_Q_VLA] = (CeedScalar(*)[3][CEED_Q_VLA])out[0];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Read spatial derivatives of increment u; d(du)/dX = d(du)/dX
    CeedScalar ddudX[3][3];
    RatelGradUnpack(Q, i, dug, ddudX);

    // Read q_data (dXdxdXdx_T symmetric matrix)
    const CeedScalar dXdxdXdx_T[3][3] = {
        {q_data[1][i], q_data[6][i], q_data[5][i]},
        {q_data[6][i], q_data[2][i], q_data[4][i]},
        {q_data[5][i], q_data[4][i], q_data[3][i]}
    };

    for (CeedInt k = 0; k < 3; k++) {    // k = component
      for (CeedInt j = 0; j < 3; j++) {  // j = direction of vg
        dvg[j][k][i] = (ddudX[k][0] * dXdxdXdx_T[0][j] + ddudX[k][1] * dXdxdXdx_T[1][j] + ddudX[k][2] * dXdxdXdx_T[2][j]);
      }
    }
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute the diagnostic values for the CEED vector BP problem

  @param[in]   ctx  QFunction context, unused
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - volumetric qdata
                      - 1 - u
  @param[out]  out  Output array
                      - 0 - diagnostic values

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(Diagnostic_CEED_BP4)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*diagnostic)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Qdata
    const CeedScalar wdetJ = q_data[0][i];

    // Displacement
    diagnostic[0][i] = u[0][i];
    diagnostic[1][i] = u[1][i];
    diagnostic[2][i] = u[2][i];

    for (CeedInt j = 0; j < 3; j++) diagnostic[j][i] *= wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
