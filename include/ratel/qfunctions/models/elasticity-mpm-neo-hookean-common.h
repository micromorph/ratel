/// @file
/// Ratel neo-Hookean hyperelasticity at finite strain with MPM common QFunction source
#pragma once

#include <ceed/types.h>

#include "../../models/elasticity-linear.h"  // IWYU pragma: export
#include "../../models/neo-hookean.h"        // IWYU pragma: export
#include "../utils.h"                        // IWYU pragma: export
#include "elasticity-neo-hookean-common.h"   // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

#define NUM_COMPONENTS_DIAGNOSTIC_MPM_NeoHookean 16

CEED_QFUNCTION(SetPointFields_MPM_NeoHookean)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Outputs
  CeedScalar *rho  = out[0];
  CeedScalar *mu   = out[1];
  CeedScalar *bulk = out[2];

  // Context
  const RatelNeoHookeanElasticityParams *context = (RatelNeoHookeanElasticityParams *)ctx;

  // Quadrature Point Loop
  for (CeedInt i = 0; i < Q; i++) {
    rho[i]  = context->common_parameters[RATEL_COMMON_PARAMETER_RHO];
    mu[i]   = context->mu;
    bulk[i] = context->bulk;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute strain energy for neo-Hookean hyperelasticity

  @param[in]   ctx  QFunction context, holding `RatelNeoHookeanElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - gradient of u with respect to reference coordinates
  @param[out]  out  Output arrays
                      - 0 - strain energy

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(StrainEnergy_MPM_NeoHookean)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];
  const CeedScalar *mu                  = in[3];
  const CeedScalar *bulk                = in[4];

  // Outputs
  CeedScalar(*energy) = (CeedScalar(*))out[0];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar dXdx[3][3], grad_u[3][3], e_sym[6], V, strain_energy;

    // Material properties
    const CeedScalar lambda = bulk[i] - 2 * mu[i] / 3.;

    // Qdata
    const CeedScalar wdetJ = q_data[0][i];

    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Read spatial derivatives of u; du/dX
    CeedScalar dudX[3][3];
    RatelGradUnpack(Q, i, ug, dudX);

    // Compute grad_u = du/dX * dX/dx
    // X is ref coordinate [-1,1]^3; x is physical coordinate
    RatelMatMatMult(1.0, dudX, dXdx, grad_u);

    // e - Euler strain tensor
    RatelGreenEulerStrain(grad_u, e_sym);
    const CeedScalar trace_e = RatelMatTraceSymmetric(e_sym);
    const CeedScalar Jm1     = RatelMatDetAM1(grad_u);

    VolumetricFunctionAndDerivatives(Jm1, &V, NULL, NULL);
    RatelStrainEnergy_NeoHookean(V, lambda, mu[i], Jm1, trace_e, &strain_energy);

    // Strain energy psi(e) for compressible Neo-Hookean
    energy[i] = strain_energy * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
  (void)StrainEnergy_NeoHookean;
}

/**
  @brief Compute projected diagnostic values for neo-Hookean hyperelasticity

  @param[in]   ctx  QFunction context, holding `RatelNeoHookeanElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - u
                      - 2 - gradient of u with respect to reference coordinates
  @param[out]  out  Output arrays
                      - 0 - projected diagnostic values

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(Diagnostic_MPM_NeoHookean)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[1];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[2];
  const CeedScalar *rho                 = in[3];
  const CeedScalar *mu                  = in[4];
  const CeedScalar *bulk                = in[5];

  // Outputs
  CeedScalar(*diagnostic)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar dXdx[3][3], grad_u[3][3], e_sym[6], tau_sym[6], sigma_sym[6], sigma_dev_sym[6], V, J_dVdJ, strain_energy;

    // Material properties
    const CeedScalar lambda = bulk[i] - 2 * mu[i] / 3.;
    const CeedScalar two_mu = mu[i] * 2;

    // Qdata
    const CeedScalar wdetJ = q_data[0][i];

    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Read spatial derivatives of u; du/dX
    CeedScalar dudX[3][3];
    RatelGradUnpack(Q, i, ug, dudX);

    // Compute grad_u = du/dX * dX/dx
    // X is ref coordinate [-1,1]^3; x is physical coordinate
    RatelMatMatMult(1.0, dudX, dXdx, grad_u);

    // Compute J-1 and logJ
    const CeedScalar Jm1 = RatelMatDetAM1(grad_u);
    const CeedScalar J   = Jm1 + 1.0;

    RatelGreenEulerStrain(grad_u, e_sym);
    VolumetricFunctionAndDerivatives(Jm1, &V, &J_dVdJ, NULL);
    RatelKirchhoffTau_NeoHookean(J_dVdJ, lambda, two_mu, e_sym, tau_sym);
    for (CeedInt j = 0; j < 6; j++) sigma_sym[j] = tau_sym[j] / J;

    // Displacement
    diagnostic[0][i] = u[0][i];
    diagnostic[1][i] = u[1][i];
    diagnostic[2][i] = u[2][i];

    // Cauchy stress tensor
    diagnostic[3][i] = sigma_sym[0];
    diagnostic[4][i] = sigma_sym[5];
    diagnostic[5][i] = sigma_sym[4];
    diagnostic[6][i] = sigma_sym[1];
    diagnostic[7][i] = sigma_sym[3];
    diagnostic[8][i] = sigma_sym[2];

    // Hydrostatic pressure; p_h = -trace(sigma) / 3
    diagnostic[9][i] = -RatelMatTraceSymmetric(sigma_sym) / 3.;

    // Strain tensor invariants
    const CeedScalar trace_e  = RatelMatTraceSymmetric(e_sym);
    const CeedScalar trace_e2 = RatelMatMatContractSymmetric(1.0, e_sym, e_sym);

    diagnostic[10][i] = trace_e;
    diagnostic[11][i] = trace_e2;

    diagnostic[12][i] = J;

    RatelStrainEnergy_NeoHookean(V, lambda, mu[i], Jm1, trace_e, &strain_energy);

    // Strain energy
    diagnostic[13][i] = strain_energy;

    // Compute von-Mises stress
    // -- Compute the the deviatoric part of Cauchy stress: sigma_dev = sigma - trace(sigma)/3
    const CeedScalar trace_sigma = RatelMatTraceSymmetric(sigma_sym);

    RatelMatDeviatoricSymmetric(trace_sigma, sigma_sym, sigma_dev_sym);

    // -- sigma_dev:sigma_dev
    const CeedScalar sigma_dev_contract = RatelMatMatContractSymmetric(1.0, sigma_dev_sym, sigma_dev_sym);

    // -- Compute von-Mises stress: sigma_von = sqrt(3/2 sigma_dev:sigma_dev)
    diagnostic[14][i] = sqrt(3. * sigma_dev_contract / 2.);

    // Compute mass density values

    // -- Set to 0 as default (rho should be 0.0 as defualt anyways)
    diagnostic[15][i] = 0.0;

    // -- if mass density is requested then compute mass density: rho/J
    if (rho[i] > 0.0) diagnostic[15][i] = rho[i] / diagnostic[12][i];

    for (CeedInt j = 0; j < NUM_COMPONENTS_DIAGNOSTIC_MPM_NeoHookean; j++) diagnostic[j][i] *= wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
  (void)Diagnostic_NeoHookean;
}

/// @}
