/// @file
/// Common diagnostic components for elasticity QFunctions
#pragma once

#include <ceed/types.h>

#include "../../models/common-parameters.h"       // IWYU pragma: export
#include "../utils.h"                             // IWYU pragma: export
#include "elasticity-volumetric-energy-common.h"  // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

typedef int (*RatelComputeDiagnosticStress)(void *, const CeedScalar, const CeedScalar, const CeedScalar, CeedScalar[3][3], CeedScalar[6],
                                            CeedScalar *, CeedScalar *, CeedScalar *);

#define NUM_COMPONENTS_DIAGNOSTIC_Elasticity 16

/**
  @brief Compute elasticity diagnostic evaluation

  @param[in]   ctx                        QFunction context, holding common parameters and model parameters
  @param[in]   Q                          Number of quadrature points
  @param[in]   compute_diagnostic_stress  Function to compute diagnostic
  @param[in]   in                         Input arrays
                                            - 0 - qdata
                                            - 1 - u
                                            - 2 - gradient of u with respect to reference coordinates
  @param[out]  out                        Output arrays
                                            - 0 - projected diagnostic values

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int ElasticityDiagnostic(void *ctx, CeedInt Q, RatelComputeDiagnosticStress compute_diagnostic_stress,
                                               const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[1];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[2];

  // Outputs
  CeedScalar(*diagnostic)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Context
  const CeedScalar *context = (CeedScalar *)ctx;
  const CeedScalar  rho     = context[RATEL_COMMON_PARAMETER_RHO];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar dXdx[3][3], dudX[3][3], grad_u[3][3], sigma_sym[6], sigma_dev_sym[6], V, J_dVdJ, strain_energy, trace_e, trace_e2;

    // Qdata
    const CeedScalar wdetJ = q_data[0][i];
    RatelQdataUnpack(Q, i, q_data, dXdx);

    // Read spatial derivatives of u; du/dX
    RatelGradUnpack(Q, i, ug, dudX);

    // Compute grad_u = du/dX * dX/dx
    // X is ref coordinate [-1,1]^3; x is physical coordinate
    RatelMatMatMult(1.0, dudX, dXdx, grad_u);

    // Compute J-1 and volumetric energy and its first derivative
    const CeedScalar Jm1 = RatelMatDetAM1(grad_u);
    VolumetricFunctionAndDerivatives(Jm1, &V, &J_dVdJ, NULL);

    compute_diagnostic_stress(ctx, Jm1, V, J_dVdJ, grad_u, sigma_sym, &strain_energy, &trace_e, &trace_e2);

    // Displacement
    diagnostic[0][i] = u[0][i];
    diagnostic[1][i] = u[1][i];
    diagnostic[2][i] = u[2][i];

    // Cauchy stress tensor
    diagnostic[3][i] = sigma_sym[0];
    diagnostic[4][i] = sigma_sym[5];
    diagnostic[5][i] = sigma_sym[4];
    diagnostic[6][i] = sigma_sym[1];
    diagnostic[7][i] = sigma_sym[3];
    diagnostic[8][i] = sigma_sym[2];

    // Hydrostatic pressure; p_h = -trace(sigma) / 3
    diagnostic[9][i] = -RatelMatTraceSymmetric(sigma_sym) / 3.;

    // Strain tensor invariants
    diagnostic[10][i] = trace_e;
    diagnostic[11][i] = trace_e2;

    diagnostic[12][i] = Jm1 + 1.;

    // Strain energy
    diagnostic[13][i] = strain_energy;

    // Compute von-Mises stress
    // -- Compute the the deviatoric part of Cauchy stress: sigma_dev = sigma - trace(sigma)/3
    const CeedScalar trace_sigma = RatelMatTraceSymmetric(sigma_sym);

    RatelMatDeviatoricSymmetric(trace_sigma, sigma_sym, sigma_dev_sym);

    // -- sigma_dev:sigma_dev
    const CeedScalar sigma_dev_contract = RatelMatMatContractSymmetric(1.0, sigma_dev_sym, sigma_dev_sym);

    // -- Compute von-Mises stress: sigma_von = sqrt(3/2 sigma_dev:sigma_dev)
    diagnostic[14][i] = sqrt(3. * sigma_dev_contract / 2.);

    // Compute mass density values
    // -- Set to 0 as default (rho should be 0.0 as defualt anyways)
    diagnostic[15][i] = 0.0;

    // -- If mass density is requested then compute mass density: rho/J
    if (rho > 0.0) diagnostic[15][i] = rho / diagnostic[12][i];

    // Quadrature weight
    for (CeedInt j = 0; j < NUM_COMPONENTS_DIAGNOSTIC_Elasticity; j++) diagnostic[j][i] *= wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
