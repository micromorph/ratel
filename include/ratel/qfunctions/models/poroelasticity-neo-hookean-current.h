/// @file
/// Ratel Neo-Hookean poroelasticity  QFunction source
#include <ceed/types.h>
#ifndef CEED_RUNNING_JIT_PASS
#include <math.h>
#endif

#include "../../models/poroelasticity-neo-hookean.h"  // IWYU pragma: export
#include "../utils.h"                                 // IWYU pragma: export
#include "poroelasticity-common.h"                    // IWYU pragma: export
#include "poroelasticity-neo-hookean-common.h"        // IWYU pragma: export

/// @addtogroup RatelMaterials
/// @{

#define NUM_COMPONENTS_STATE_PoroElasticityNeoHookeanCurrent 0
#define NUM_COMPONENTS_STORED_PoroElasticityNeoHookeanCurrent 29
#define NUM_COMPONENTS_STORED_PoroElasticityNeoHookeanCurrent_ut 2
#define NUM_ACTIVE_FIELD_EVAL_MODES_PoroElasticityNeoHookeanCurrent_u 1
#define NUM_ACTIVE_FIELD_EVAL_MODES_PoroElasticityNeoHookeanCurrent_p 2
#define NUM_ACTIVE_FIELD_EVAL_MODES_PoroElasticityNeoHookeanCurrent \
  (NUM_ACTIVE_FIELD_EVAL_MODES_PoroElasticityNeoHookeanCurrent_u + NUM_ACTIVE_FIELD_EVAL_MODES_PoroElasticityNeoHookeanCurrent_p)
#define NUM_U_t_FIELD_EVAL_MODES_PoroElasticityNeoHookeanCurrent_u 1
#define NUM_U_t_FIELD_EVAL_MODES_PoroElasticityNeoHookeanCurrent_p 1

// FLOPS_grad_du = FLOPS_MatMatMult
#define FLOPS_df1_PoroElasticityNeoHookeanCurrent                                                                                                  \
  (2 * FLOPS_MatMatMult + FLOPS_J_dVdJ + FLOPS_J2_d2VdJ2 + 1 + FLOPS_Tau_PoroElasticityNeoHookean + FLOPS_FdSFTranspose_PoroElasticityNeoHookean + \
   FLOPS_MatMatAdd)
#define FLOPS_dg0_PoroElasticityNeoHookeanCurrent \
  (4 + FLOPS_MatTransposeVecMult + FLOPS_MatMatMult + FLOPS_VecVecSubtract + 2 * FLOPS_Dot3 + FLOPS_MatTrace + 22)
#define FLOPS_dg1_PoroElasticityNeoHookeanCurrent \
  (19 + 2 * FLOPS_PorosityFunction + FLOPS_PorosityFunctionDerivative + FLOPS_MatTransposeVecMult + FLOPS_MatVecMult + FLOPS_VecVecVecAdd + 9)
#define FLOPS_JACOBIAN_PoroElasticityNeoHookeanCurrent                                                       \
  (FLOPS_df1_PoroElasticityNeoHookeanCurrent + FLOPS_dXdxwdetJ + FLOPS_dg0_PoroElasticityNeoHookeanCurrent + \
   FLOPS_dg1_PoroElasticityNeoHookeanCurrent + FLOPS_MatVecMult + 3 + 1)
#define FLOPS_JACOBIAN_Block_uu_PoroElasticityNeoHookeanCurrent                                                                                    \
  (2 * FLOPS_MatMatMult + FLOPS_J_dVdJ + FLOPS_J2_d2VdJ2 + 1 + FLOPS_Tau_PoroElasticityNeoHookean + FLOPS_FdSFTranspose_PoroElasticityNeoHookean + \
   FLOPS_MatMatAdd)
#define FLOPS_JACOBIAN_Block_pp_PoroElasticityNeoHookeanCurrent \
  (20 + 2 * FLOPS_PorosityFunction + FLOPS_PorosityFunctionDerivative + FLOPS_MatTransposeVecMult + FLOPS_Dot3 + FLOPS_MatVecMult)

/**
  @brief Compute `tau` for Neo-Hookean poroelasticity

  @param[in]   ctx      QFunction context, holding `RatelNeoHookeanPoroElasticityParams`
  @param[in]   Q        Number of quadrature points
  @param[in]   i        Current quadrature point
  @param[in]   in       Input arrays
                          - 0 - volumetric qdata
                          - 1 - gradient of u with respect to reference coordinates
                          - 2 - p
  @param[out]  out      Output arrays
                          - 0 - stored values u
  @param[out]  dXdx     Coordinate transformation
  @param[out]  f1       `f1 = tau = tau_effective - (J B p I)`
  @param[out]  Jm1      Determinant of deformation gradient - 1 needed for computing g0 and g1 functions

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int f1_PoroElasticityNeoHookeanCurrent(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in,
                                                             CeedScalar *const *out, CeedScalar dXdx[3][3], CeedScalar f1[3][3], CeedScalar *Jm1) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*ug)[3][CEED_Q_VLA]  = (const CeedScalar(*)[3][CEED_Q_VLA])in[1];
  const CeedScalar(*p)                  = in[2];

  // Outputs
  CeedScalar(*stored_values_u) = out[0];

  // Context
  const RatelNeoHookeanPoroElasticityParams *context  = (RatelNeoHookeanPoroElasticityParams *)ctx;
  const CeedScalar                           lambda_d = context->lambda_d;
  const CeedScalar                           B        = context->B;
  const CeedScalar                           two_mu_d = context->two_mu_d;

  CeedScalar dXdx_initial[3][3], grad_u_initial[3][3], grad_u[3][3], e_sym[6], tau_sym[6], F_inv[3][3], J_dVdJ;

  // -- Qdata
  // dXdx_initial = dX/dx_initial
  // X is natural coordinate sys OR Reference [-1, 1]^dim
  // x_initial is initial config coordinate system
  RatelQdataUnpack(Q, i, q_data, dXdx_initial);

  // Read spatial derivatives of u; du/dX
  CeedScalar dudX[3][3];
  RatelGradUnpack(Q, i, ug, dudX);

  // X is natural coordinate sys OR Reference system
  // x_initial is initial config coordinate system
  // grad_u_initial =du/dx_initial= du/dX * dX/dx_initial
  RatelMatMatMult(1.0, dudX, dXdx_initial, grad_u_initial);

  // Compute the Deformation Gradient : F = I + grad_u_initial
  const CeedScalar F[3][3] = {
      {grad_u_initial[0][0] + 1, grad_u_initial[0][1],     grad_u_initial[0][2]    },
      {grad_u_initial[1][0],     grad_u_initial[1][1] + 1, grad_u_initial[1][2]    },
      {grad_u_initial[2][0],     grad_u_initial[2][1],     grad_u_initial[2][2] + 1}
  };

  // J - 1
  *Jm1               = RatelMatDetAM1(grad_u_initial);
  const CeedScalar J = *Jm1 + 1.;

  // Compute e, tau
  RatelGreenEulerStrain(grad_u_initial, e_sym);
  VolumetricFunctionAndDerivatives(*Jm1, NULL, &J_dVdJ, NULL);
  RatelKirchhoffTau_PoroElasticityNeoHookean(J, J_dVdJ, lambda_d, two_mu_d, B, p[i], e_sym, tau_sym);
  RatelSymmetricMatUnpack(tau_sym, f1);

  // x is current config coordinate system
  // dXdx = dX/dx = dX/dx_initial * F^{-1}, grad_u = grad_u_initial * F^{-1}
  // Note that F^{-1} = dx_initial/dx
  RatelMatInverse(F, J, F_inv);
  RatelMatMatMult(1.0, dXdx_initial, F_inv, dXdx);
  RatelMatMatMult(1.0, grad_u_initial, F_inv, grad_u);

  // Store values
  RatelStoredValuesPack(Q, i, 0, 9, (CeedScalar *)dXdx, stored_values_u);
  RatelStoredValuesPack(Q, i, 9, 6, (CeedScalar *)e_sym, stored_values_u);
  RatelStoredValuesPack(Q, i, 15, 1, Jm1, stored_values_u);
  RatelStoredValuesPack(Q, i, 16, 1, &p[i], stored_values_u);
  RatelStoredValuesPack(Q, i, 17, 9, (CeedScalar *)grad_u, stored_values_u);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute `g1` for Neo-Hookean poroelasticity

  @param[in]   ctx       QFunction context, holding `RatelNeoHookeanPoroElasticityParams`
  @param[in]   Q         Number of quadrature points
  @param[in]   i         Current quadrature point
  @param[in]   in        Input arrays
                           - 3 - gradient of p with respect to reference coordinates
  @param[in]   dXdx      Coordinate transformation
  @param[in]   Jm1       Determinant of deformation gradient - 1 computed in f1 function
  @param[out]  out       Output arrays
                           - 0 - stored values u
  @param[out]  grad_p    Gradient of pressure in current configuration
  @param[out]  g1        `g1 = varkappa grad_p * J`
  @param[out]  varkappa  Permeability coefficient, needed in g0 function

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int g1_PoroElasticityNeoHookeanCurrent(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in,
                                                             CeedScalar dXdx[3][3], const CeedScalar Jm1, CeedScalar *const *out,
                                                             CeedScalar grad_p[3], CeedScalar g1[3], CeedScalar *varkappa) {
  // Inputs
  const CeedScalar(*pg)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[3];

  // Outputs
  CeedScalar(*stored_values_u) = out[0];

  // Context
  const RatelNeoHookeanPoroElasticityParams *context    = (RatelNeoHookeanPoroElasticityParams *)ctx;
  const CeedScalar                           varkappa_0 = context->varkappa_0;
  const CeedScalar                           eta_f      = context->eta_f;
  const CeedScalar                           phi_0      = context->phi_0;

  CeedScalar F0, F;

  // Read spatial derivatives of p; dp/dX
  const CeedScalar dpdX[3] = {pg[0][i], pg[1][i], pg[2][i]};

  // Compute porosity and permeability coefficient
  const CeedScalar J      = Jm1 + 1.;
  const CeedScalar phi_0s = 1 - phi_0;
  const CeedScalar phi    = 1 - phi_0s / J;
  PorosityFunctionAndDerivative(phi_0, &F0, NULL);
  PorosityFunctionAndDerivative(phi, &F, NULL);
  *varkappa = varkappa_0 * F / (eta_f * F0);

  // g1 = (varkappa_0 * F(phi)/ eta_f * F(phi_0)) * grad_p * J= varkappa * dXdx^T * dpdX * J
  RatelMatTransposeVecMult(1., dXdx, dpdX, grad_p);
  RatelScalarVecMult(*varkappa * J, grad_p, g1);

  // Store values
  RatelStoredValuesPack(Q, i, 26, 3, (CeedScalar *)grad_p, stored_values_u);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute `g0` for Neo-Hookean poroelasticity

  `g0 = (phi * p_t / bulk_f + div(u_t) - varkappa * grad_p \cdot grad_p / bulk_f) * J`

  the time derivative part of above expression is given in residual_ut Qfucntion

  @param[in]   ctx               QFunction context, holding `RatelNeoHookeanPoroElasticityParams`
  @param[in]   Q                 Number of quadrature points
  @param[in]   i                 Current quadrature point
  @param[in]   in                Input arrays, unused
  @param[in]   grad_p            Gradient of pressure in current configuration computed in g1 function
  @param[in]   Jm1               Determinant of deformation gradient - 1 computed in f1 function
  @param[in]   varkappa          Permeability coefficient computed in g1 function
  @param[out]  out               Output arrays, unused
  @param[out]  g0               `g0 = - (varkappa * grad_p \cdot grad_p / bulk_f) * J`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int g0_PoroElasticityNeoHookeanCurrent(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in,
                                                             CeedScalar grad_p[3], const CeedScalar Jm1, const CeedScalar varkappa,
                                                             CeedScalar *const *out, CeedScalar *g0) {
  // Context
  const RatelNeoHookeanPoroElasticityParams *context = (RatelNeoHookeanPoroElasticityParams *)ctx;
  const CeedScalar                           bulk_f  = context->bulk_f;

  const CeedScalar J = Jm1 + 1.;
  *g0                = -varkappa * RatelDot3(grad_p, grad_p) * J / bulk_f;
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute `g0 = (phi * p_t / bulk_f + div(u_t)) * J` for Neo-Hookean poroelasticity

  @param[in]   ctx  QFunction context, holding `RatelNeoHookeanPoroElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - volumetric qdata
                      - 1 - stored values u; computed in residual_u
                      - 2 - gradient of u_t with respect to reference coordinates
                      - 3 - p_t
  @param[out]  out  Output arrays
                      - 0 - stored values ut
                      - 1 - action on displacement field (it is 0)
                      - 2 - action on pressure field (q, g0) where `g0 = (phi * p_t / bulk_f + div(u_t)) * J`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(PoroElasticityResidual_NeoHookeanCurrent_ut)(void *ctx, const CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA]  = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*stored_values_u)     = in[1];
  const CeedScalar(*ug_t)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[2];
  const CeedScalar(*p_t)                 = in[3];

  // Outputs
  CeedScalar(*stored_values_ut)     = out[0];
  CeedScalar(*dvtdX)[3][CEED_Q_VLA] = (CeedScalar(*)[3][CEED_Q_VLA])out[1];
  CeedScalar(*g0)                   = out[2];

  // Context
  const RatelNeoHookeanPoroElasticityParams *context = (RatelNeoHookeanPoroElasticityParams *)ctx;
  const CeedScalar                           phi_0   = context->phi_0;
  const CeedScalar                           bulk_f  = context->bulk_f;

  // Quadrature point loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    const CeedScalar wdetJ = q_data[0][i];

    // // Retrieve dXdx, read spatial derivatives of ut; dut/dX
    CeedScalar dutdX[3][3], dXdx[3][3], grad_ut[3][3], Jm1;
    RatelStoredValuesUnpack(Q, i, 0, 9, stored_values_u, (CeedScalar *)dXdx);
    RatelGradUnpack(Q, i, ug_t, dutdX);

    // Compute grad_ut = dut/dX * dX/dx
    // X is ref coordinate [-1,1]^3; x is current coordinate
    RatelMatMatMult(1.0, dutdX, dXdx, grad_ut);
    // Compute div(u_t) = trace(grad_ut)
    CeedScalar trace_grad_ut = RatelMatTrace(grad_ut);

    // Compute porosity and g0 = [phi * pt / bulk_f + div(ut)] * J
    RatelStoredValuesUnpack(Q, i, 15, 1, stored_values_u, &Jm1);
    const CeedScalar J      = Jm1 + 1.;
    const CeedScalar phi_0s = 1 - phi_0;
    const CeedScalar phi    = 1 - phi_0s / J;
    g0[i]                   = (phi * p_t[i] / bulk_f + trace_grad_ut) * J * wdetJ;

    // Set dvtdX = 0
    for (CeedInt j = 0; j < 3; j++) {
      for (CeedInt k = 0; k < 3; k++) {
        dvtdX[j][k][i] = 0;
      }
    }
    // Store values
    RatelStoredValuesPack(Q, i, 0, 1, &p_t[i], stored_values_ut);
    RatelStoredValuesPack(Q, i, 1, 1, &trace_grad_ut, stored_values_ut);
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute linearization of `f1` for Neo-Hookean poroelasticity

  @param[in]   ctx               QFunction context, holding `RatelNeoHookeanPoroElasticityParams`
  @param[in]   Q                 Number of quadrature points
  @param[in]   i                 Current quadrature point
  @param[in]   in                Input arrays
                                   - 0 - volumetric qdata
                                   - 1 - stored values u
                                   - 3 - gradient of incremental change to u with respect to reference coordinates
                                   - 4 - dp
  @param[out]  out               Output arrays, unused
  @param[out]  dXdx              Coordinate transformation
  @param[out]  grad_du           Gradient of incremental change in u
  @param[out]  depsilon          depsilon = (grad_du + grad_du^T)/2
  @param[out]  df1               `df1 = dtau - tau * grad_du^T`
  @param[out]  Jm1               Determinant of deformation gradient - 1 needed for computing dg0 function
  @param[out]  trace_depsilon    `trace(depsilon)` needed for computing dg0 function

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int df1_PoroElasticityNeoHookeanCurrent(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in,
                                                              CeedScalar *const *out, CeedScalar dXdx[3][3], CeedScalar grad_du[3][3],
                                                              CeedScalar depsilon[3][3], CeedScalar df1[3][3], CeedScalar *Jm1,
                                                              CeedScalar *trace_depsilon) {
  // Inputs
  const CeedScalar(*stored_values_u)    = in[1];
  const CeedScalar(*dug)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[3];
  const CeedScalar(*dp)                 = in[4];

  // Context
  const RatelNeoHookeanPoroElasticityParams *context  = (RatelNeoHookeanPoroElasticityParams *)ctx;
  const CeedScalar                           lambda_d = context->lambda_d;
  const CeedScalar                           B        = context->B;
  const CeedScalar                           two_mu_d = context->two_mu_d;
  const CeedScalar                           mu_d     = context->mu_d;

  CeedScalar e_sym[6], tau_sym[6], tau[3][3], grad_du_tau[3][3], FdSFTranspose[3][3], p, J_dVdJ, J2_d2VdJ2;

  // Read spatial derivatives of increment u; d(du)/dX = d(du)/dX
  CeedScalar ddudX[3][3];
  RatelGradUnpack(Q, i, dug, ddudX);

  // Retrieve dXdx
  RatelStoredValuesUnpack(Q, i, 0, 9, stored_values_u, (CeedScalar *)dXdx);

  // Compute grad_du = ddu/dX * dX/dx
  // X is ref coordinate [-1,1]^3; x is physical coordinate in current configuration
  RatelMatMatMult(1.0, ddudX, dXdx, grad_du);

  // Retrieve e_sym, J1, J^{-2/3} and p
  RatelStoredValuesUnpack(Q, i, 9, 6, stored_values_u, e_sym);
  RatelStoredValuesUnpack(Q, i, 15, 1, stored_values_u, Jm1);
  RatelStoredValuesUnpack(Q, i, 16, 1, stored_values_u, &p);
  VolumetricFunctionAndDerivatives(*Jm1, NULL, &J_dVdJ, &J2_d2VdJ2);
  const CeedScalar J = *Jm1 + 1.;
  RatelKirchhoffTau_PoroElasticityNeoHookean(J, J_dVdJ, lambda_d, two_mu_d, B, p, e_sym, tau_sym);
  RatelSymmetricMatUnpack(tau_sym, tau);

  // Compute grad_du_tau = grad_du*tau
  RatelMatMatMult(1.0, grad_du, tau, grad_du_tau);

  // Compute F*dS*F^T
  RatelComputeFdSFTranspose_PoroElasticityNeoHookean(J, J_dVdJ, J2_d2VdJ2, lambda_d, mu_d, B, p, dp[i], grad_du, depsilon, FdSFTranspose,
                                                     trace_depsilon);

  // df1 = dtau - tau * grad_du^T
  //     = grad_du*tau + F*dS*F^T
  RatelMatMatAdd(1.0, grad_du_tau, 1., FdSFTranspose, df1);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute linearization of `g1` for Neo-Hookean poroelasticity

  @param[in]   ctx               QFunction context, holding `RatelNeoHookeanPoroElasticityParams`
  @param[in]   Q                 Number of quadrature points
  @param[in]   i                 Current quadrature point
  @param[in]   in                Input arrays
                                   - 1 - stored values u
                                   - 5 - gradient of incremental change to p with respect to reference coordinates
  @param[in]   dXdx              Coordinate transformation
  @param[in]   depsilon          depsilon = (grad_du + grad_du^T)/2
  @param[in]   Jm1               Determinant of deformation gradient - 1 computed in df1 function
  @param[in]   trace_depsilon    `trace(depsilon)` computed in df1 function
  @param[out]  out               Output arrays, unused
  @param[out]  grad_dp           Gradient of incremental change in p, needed in dg0 function
  @param[out]  dg1               `dg1 = d(varkappa grad_dp)`
  @param[out]  varkappa          Permeability coefficient, needed in dg0 function
  @param[out]  varkappa_fwd      Linearization of permeability coefficient, needed in dg0 function

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int dg1_PoroElasticityNeoHookeanCurrent(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in,
                                                              CeedScalar dXdx[3][3], CeedScalar depsilon[3][3], const CeedScalar Jm1,
                                                              const CeedScalar trace_depsilon, CeedScalar *const *out, CeedScalar grad_dp[3],
                                                              CeedScalar dg1[3], CeedScalar *varkappa, CeedScalar *varkappa_fwd) {
  // Inputs
  const CeedScalar(*stored_values_u) = in[1];
  const CeedScalar(*dpg)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[5];

  // Context
  const RatelNeoHookeanPoroElasticityParams *context    = (RatelNeoHookeanPoroElasticityParams *)ctx;
  const CeedScalar                           varkappa_0 = context->varkappa_0;
  const CeedScalar                           eta_f      = context->eta_f;
  const CeedScalar                           phi_0      = context->phi_0;

  CeedScalar F, F0, dFdphi, grad_p[3], depsilon_grad_p[3];

  // Retrieve grad_p
  RatelStoredValuesUnpack(Q, i, 26, 3, stored_values_u, (CeedScalar *)grad_p);

  // Compute porosity, permeability coefficient and its linearization
  const CeedScalar J      = Jm1 + 1.;
  const CeedScalar phi_0s = 1 - phi_0;
  const CeedScalar phi    = 1 - phi_0s / J;
  PorosityFunctionAndDerivative(phi_0, &F0, NULL);
  PorosityFunctionAndDerivative(phi, &F, &dFdphi);
  *varkappa                = varkappa_0 * F / (eta_f * F0);
  const CeedScalar phi_fwd = phi_0s * trace_depsilon / J;
  *varkappa_fwd            = varkappa_0 * dFdphi * phi_fwd / (eta_f * F0);

  // Read spatial derivatives of p; d(dp)/dX, and compute grad_dp
  const CeedScalar ddpdX[3] = {dpg[0][i], dpg[1][i], dpg[2][i]};
  RatelMatTransposeVecMult(1., dXdx, ddpdX, grad_dp);
  RatelMatVecMult(1., depsilon, grad_p, depsilon_grad_p);

  const CeedScalar coeff_1 = *varkappa_fwd * J + *varkappa * J * trace_depsilon;
  const CeedScalar coeff_2 = *varkappa * J;

  RatelVecVecVecAdd(coeff_1, grad_p, coeff_2, grad_dp, -2 * coeff_2, depsilon_grad_p, dg1);
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute linearization of `g0` for Neo-Hookean poroelasticity

  @param[in]   ctx               QFunction context, holding `RatelNeoHookeanPoroElasticityParams`
  @param[in]   Q                 Number of quadrature points
  @param[in]   i                 Current quadrature point
  @param[in]   in                Input arrays
                                   - 1 - stored values u
                                   - 2 - stored values ut
                                   - 4 - dp
  @param[in]   grad_du           Gradient of incremental change in u, computed in df1 function
  @param[in]   grad_dp           Gradient of incremental change in p, computed in dg1 function
  @param[in]   Jm1               Determinant of deformation gradient - 1, computed in df1 function
  @param[in]   trace_depsilon    `trace(depsilon)`, computed in df1 function
  @param[in]   varkappa          Permeability coefficient, computed in dg1 function
  @param[in]   varkappa_fwd      Linearization of permeability coefficient, computed in dg1 function
  @param[out]  out               Output arrays, unused
  @param[out]  dg0               `dg0`

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int dg0_PoroElasticityNeoHookeanCurrent(void *ctx, const CeedInt Q, const CeedInt i, const CeedScalar *const *in,
                                                              CeedScalar grad_du[3][3], CeedScalar grad_dp[3], const CeedScalar Jm1,
                                                              const CeedScalar trace_depsilon, const CeedScalar varkappa,
                                                              const CeedScalar varkappa_fwd, CeedScalar *const *out, CeedScalar *dg0) {
  // Inputs
  const CeedScalar(*stored_values_u)  = in[1];
  const CeedScalar(*stored_values_ut) = in[2];
  const CeedScalar(*dp)               = in[4];

  // Context
  const RatelNeoHookeanPoroElasticityParams *context = (RatelNeoHookeanPoroElasticityParams *)ctx;
  const CeedScalar                           shift_v = context->common_parameters[1];
  const CeedScalar                           phi_0   = context->phi_0;
  const CeedScalar                           bulk_f  = context->bulk_f;

  CeedScalar p_t, trace_grad_ut, grad_p[3], grad_du_T_grad_p[3], grad_u[3][3], grad_u_grad_du[3][3], grad_p_fwd[3];

  // Retrieve stored values u and ut
  RatelStoredValuesUnpack(Q, i, 17, 9, stored_values_u, (CeedScalar *)grad_u);
  RatelStoredValuesUnpack(Q, i, 26, 3, stored_values_u, (CeedScalar *)grad_p);
  RatelStoredValuesUnpack(Q, i, 0, 1, stored_values_ut, &p_t);
  RatelStoredValuesUnpack(Q, i, 1, 1, stored_values_ut, &trace_grad_ut);

  // Compute permeability coefficient
  const CeedScalar J      = Jm1 + 1.;
  const CeedScalar phi_0s = 1 - phi_0;
  const CeedScalar phi    = 1 - phi_0s / J;

  // Read spatial derivatives of p; d(dp)/dX and compute grad_dp
  RatelMatTransposeVecMult(1., grad_du, grad_p, grad_du_T_grad_p);
  RatelMatMatMult(1.0, grad_u, grad_du, grad_u_grad_du);
  RatelVecVecSubtract(grad_dp, grad_du_T_grad_p, grad_p_fwd);
  const CeedScalar grad_p_dot_grad_p = RatelDot3(grad_p, grad_p);

  const CeedScalar L         = phi * p_t / bulk_f + trace_grad_ut - varkappa * grad_p_dot_grad_p / bulk_f;
  const CeedScalar div_u_fwd = trace_depsilon - RatelMatTrace(grad_u_grad_du);
  const CeedScalar L_fwd     = shift_v * (phi * dp[i] / bulk_f + div_u_fwd) - varkappa_fwd * grad_p_dot_grad_p / bulk_f -
                           2. * varkappa * RatelDot3(grad_p, grad_p_fwd) / bulk_f;
  *dg0 = L_fwd * J + L * J * trace_depsilon;
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute residual for Neo-Hookean poroelasticity

  @param[in]   ctx  QFunction context, holding `RatelNeoHookeanPoroElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(PoroElasticityResidual_NeoHookeanCurrent)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return PoroElasticityResidual(ctx, Q, f1_PoroElasticityNeoHookeanCurrent, g1_PoroElasticityNeoHookeanCurrent, g0_PoroElasticityNeoHookeanCurrent,
                                !!NUM_COMPONENTS_STATE_PoroElasticityNeoHookeanCurrent, !!NUM_COMPONENTS_STORED_PoroElasticityNeoHookeanCurrent,
                                NUM_ACTIVE_FIELD_EVAL_MODES_PoroElasticityNeoHookeanCurrent, in, out);
}

/**
  @brief Evaluate Jacobian for Neo-Hookean poroelasticity

  @param[in]   ctx  QFunction context, holding `RatelNeoHookeanPoroElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
  @param[out]  out  Output arrays

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(PoroElasticityJacobian_NeoHookeanCurrent)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  return PoroElasticityJacobian(ctx, Q, df1_PoroElasticityNeoHookeanCurrent, dg1_PoroElasticityNeoHookeanCurrent, dg0_PoroElasticityNeoHookeanCurrent,
                                !!NUM_COMPONENTS_STATE_PoroElasticityNeoHookeanCurrent, !!NUM_COMPONENTS_STORED_PoroElasticityNeoHookeanCurrent,
                                NUM_ACTIVE_FIELD_EVAL_MODES_PoroElasticityNeoHookeanCurrent, in, out);
}

/**
  @brief Compute displacement block for pMG preconditioner for Neo-Hookean poroelasticity

  @param[in]   ctx  QFunction context, holding `RatelNeoHookeanPoroElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - stored values u
                      - 3 - gradient of incremental change of u with respect to reference coordinates
  @param[out]  out  Output arrays
                      - 0 - action of QFunction for displacement field

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(PoroElasticityPC_uu_NeoHookeanCurrent)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*stored_values_u)    = in[1];
  const CeedScalar(*dug)[3][CEED_Q_VLA] = (const CeedScalar(*)[3][CEED_Q_VLA])in[3];

  // Outputs
  CeedScalar(*ddvdX)[3][CEED_Q_VLA] = (CeedScalar(*)[3][CEED_Q_VLA])out[0];

  // Context
  const RatelNeoHookeanPoroElasticityParams *context  = (RatelNeoHookeanPoroElasticityParams *)ctx;
  const CeedScalar                           lambda_d = context->lambda_d;
  const CeedScalar                           two_mu_d = context->two_mu_d;
  const CeedScalar                           mu_d     = context->mu_d;
  const CeedScalar                           B        = context->B;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar ddudX[3][3], dXdx[3][3], grad_du[3][3], e_sym[6], tau_sym[6], tau[3][3], grad_du_tau[3][3], depsilon[3][3], FdSFTranspose[3][3],
        df1_uu[3][3], Jm1, p, J_dVdJ, J2_d2VdJ2, trace_depsilon;

    // -- Qdata
    const CeedScalar wdetJ = q_data[0][i];

    // Read spatial derivatives of increment u; d(du)/dX = d(du)/dX
    RatelGradUnpack(Q, i, dug, ddudX);

    // Retrieve dXdx
    RatelStoredValuesUnpack(Q, i, 0, 9, stored_values_u, (CeedScalar *)dXdx);

    // Compute grad_du = ddu/dX * dX/dx
    // X is ref coordinate [-1,1]^3; x is physical coordinate in current configuration
    RatelMatMatMult(1.0, ddudX, dXdx, grad_du);

    // Retrieve e_sym, J1, J^{-2/3} and p
    RatelStoredValuesUnpack(Q, i, 9, 6, stored_values_u, e_sym);
    RatelStoredValuesUnpack(Q, i, 15, 1, stored_values_u, &Jm1);
    RatelStoredValuesUnpack(Q, i, 16, 1, stored_values_u, &p);
    VolumetricFunctionAndDerivatives(Jm1, NULL, &J_dVdJ, &J2_d2VdJ2);
    const CeedScalar J = Jm1 + 1.;
    RatelKirchhoffTau_PoroElasticityNeoHookean(J, J_dVdJ, lambda_d, two_mu_d, B, p, e_sym, tau_sym);
    RatelSymmetricMatUnpack(tau_sym, tau);

    // Compute grad_du_tau = grad_du*tau
    RatelMatMatMult(1.0, grad_du, tau, grad_du_tau);

    // Compute F*dS*F^T
    RatelComputeFdSFTranspose_PoroElasticityNeoHookean(J, J_dVdJ, J2_d2VdJ2, lambda_d, mu_d, B, p, 0.0, grad_du, depsilon, FdSFTranspose,
                                                       &trace_depsilon);

    // df1 = dtau - tau * grad_du^T
    //     = grad_du*tau + F*dS*F^T
    RatelMatMatAdd(1.0, grad_du_tau, 1., FdSFTranspose, df1_uu);

    // (grad(v), dsigma_pc)
    RatelMatMatTransposeMultAtQuadraturePoint(Q, i, wdetJ, dXdx, df1_uu, ddvdX);
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute pressure block for pMG preconditioner for Neo-Hookean poroelasticity

  @param[in]   ctx  QFunction context, holding `RatelNeoHookeanPoroElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - stored values u
                      - 3 - incremental change of p
                      - 4 - gradient of p
  @param[out]  out  Output arrays
                      - 0 - action of QFunction for pressure field (dg0)
                      - 1 - action of QFunction for pressure field (dg1)

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(PoroElasticityPC_pp_NeoHookeanCurrent)(void *ctx, CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*stored_values_u)    = in[1];
  const CeedScalar(*dp)                 = in[3];
  const CeedScalar(*dpg)[CEED_Q_VLA]    = (const CeedScalar(*)[CEED_Q_VLA])in[4];

  // Outputs
  CeedScalar(*dq)               = out[0];
  CeedScalar(*dqdX)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[1];

  // Context
  const RatelNeoHookeanPoroElasticityParams *context    = (RatelNeoHookeanPoroElasticityParams *)ctx;
  const CeedScalar                           varkappa_0 = context->varkappa_0;
  const CeedScalar                           bulk_f     = context->bulk_f;
  const CeedScalar                           phi_0      = context->phi_0;
  const CeedScalar                           shift_v    = context->common_parameters[1];
  const CeedInt                              sign_pp    = context->sign_pp;
  const CeedScalar                           eta_f      = context->eta_f;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    CeedScalar Jm1, F0, F, dFdphi, grad_p[3], grad_dp[3], dXdx[3][3];

    // -- Qdata
    const CeedScalar wdetJ = q_data[0][i];

    // Retrieve stored values u and ut
    RatelStoredValuesUnpack(Q, i, 0, 9, stored_values_u, (CeedScalar *)dXdx);
    RatelStoredValuesUnpack(Q, i, 15, 1, stored_values_u, &Jm1);
    RatelStoredValuesUnpack(Q, i, 26, 3, stored_values_u, (CeedScalar *)grad_p);

    // Compute permeability coefficient
    const CeedScalar J      = Jm1 + 1.;
    const CeedScalar phi_0s = 1 - phi_0;
    const CeedScalar phi    = 1 - phi_0s / J;
    PorosityFunctionAndDerivative(phi_0, &F0, NULL);
    PorosityFunctionAndDerivative(phi, &F, &dFdphi);
    const CeedScalar varkappa = varkappa_0 * F / (eta_f * F0);

    // Read spatial derivatives of p; d(dp)/dX, and compute grad_dp
    const CeedScalar ddpdX[3] = {dpg[0][i], dpg[1][i], dpg[2][i]};
    RatelMatTransposeVecMult(1., dXdx, ddpdX, grad_dp);

    const CeedScalar dg0_pp = (shift_v * phi * dp[i] / bulk_f - 2. * varkappa * RatelDot3(grad_p, grad_dp) / bulk_f) * J;
    dq[i]                   = sign_pp * dg0_pp * wdetJ;

    // (q, dqdX)
    RatelMatVecMultAtQuadraturePoint(Q, i, varkappa * J * sign_pp * wdetJ, dXdx, grad_dp, dqdX);
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
