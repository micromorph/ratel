/// @file
/// Ratel mass operator QFunction source
#include <ceed/types.h>

#include "utils.h"  // IWYU pragma: export

/// @addtogroup RatelInternal
/// @{

/**
  @brief Apply mass operator

  @param[in]   ctx  QFunction context, holding the number of components
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - volumetric qdata
                      - 1 - u
  @param[out]  out  Output array
                      - 0 - v

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(Mass)(void *ctx, const CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*v)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  const CeedInt num_comp = *(CeedInt *)ctx;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Mass operator
    RatelScaledMassApplyAtQuadraturePoint(Q, i, num_comp, q_data[0][i], (const CeedScalar *)u, (CeedScalar *)v);
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
