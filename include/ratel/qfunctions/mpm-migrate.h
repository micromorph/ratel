/// @file
/// Ratel point migration operator QFunction source
#include <ceed/types.h>

/// @addtogroup RatelInternal
/// @{

/**
  @brief Apply mass operator

  @param[in]   ctx  QFunction context, not used
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - delta u
  @param[out]  out  Output array
                      - 0 - point positions (x)

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(RatelMigratePoints)(void *ctx, const CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*delta_u)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];

  // Outputs
  CeedScalar(*x)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Only add first 3 components of delta_u to x (needed for 4 component damage models)
    for (CeedInt j = 0; j < 3; j++) x[j][i] = delta_u[j][i];
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
