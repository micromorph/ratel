/// @file
/// Ratel CEED scalar BPs manufactured solution QFunction source
#pragma once

#include <ceed/types.h>
#ifndef CEED_RUNNING_JIT_PASS
#include <math.h>
#endif

#include "../../models/mms-ceed-bps.h"  // IWYU pragma: export
#include "../utils.h"                   // IWYU pragma: export

/// @addtogroup RatelInternal
/// @{

/**
  @brief Compute true solution for scalar BPs problems

  @param[in]   ctx              QFunction context, holding `RatelMMSCEEDBPsParams`
  @param[in]   coords           Coordinate array
  @param[out]  true_solution    True solution

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelScalarBPsMMSTrueSolution(void *ctx, const CeedScalar coords[3], CeedScalar *true_solution) {
  // Scaling
  RatelMMSCEEDBPsParams *context       = (RatelMMSCEEDBPsParams *)ctx;
  const CeedScalar       weierstrass_a = context->weierstrass_a;
  const CeedScalar       weierstrass_b = context->weierstrass_b;
  const CeedInt          weierstrass_n = context->weierstrass_n;
  const CeedScalar       time          = context->time;

  // Coordinates
  const CeedScalar x = coords[0], y = coords[1], z = coords[2];

  // True solution
  CeedScalar weierstrass_function = 0.0;
  CeedScalar pow_weierstrass_a = 1.0, pow_weierstrass_b = 1.0;

  for (CeedInt k = 0; k < weierstrass_n; k++) {
    weierstrass_function += pow_weierstrass_a * cos(pow_weierstrass_b * RATEL_PI_DOUBLE * x) * cos(pow_weierstrass_b * RATEL_PI_DOUBLE * y) *
                            cos(pow_weierstrass_b * RATEL_PI_DOUBLE * z);
    pow_weierstrass_a *= weierstrass_a;
    pow_weierstrass_b *= weierstrass_b;
  }
  *true_solution = time * weierstrass_function;
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute forcing term for Poisson problems

  @param[in]   ctx              QFunction context, holding `RatelMMSCEEDBPsParams`
  @param[in]   coords           Coordinate array
  @param[out]  poisson_forcing  Forcing term needed for Poisson problem

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelScalarBPsPoissonForcing(void *ctx, const CeedScalar coords[3], CeedScalar *poisson_forcing) {
  // Scaling
  RatelMMSCEEDBPsParams *context       = (RatelMMSCEEDBPsParams *)ctx;
  const CeedScalar       weierstrass_a = context->weierstrass_a;
  const CeedScalar       weierstrass_b = context->weierstrass_b;
  const CeedInt          weierstrass_n = context->weierstrass_n;

  /// Coordinates
  const CeedScalar x = coords[0], y = coords[1], z = coords[2];

  // Forcing term
  CeedScalar forcing           = 0.0;
  CeedScalar pow_weierstrass_a = 1.0, pow_weierstrass_b = 1.0;

  for (CeedInt k = 0; k < weierstrass_n; k++) {
    forcing += pow_weierstrass_a * pow_weierstrass_b * pow_weierstrass_b * cos(pow_weierstrass_b * RATEL_PI_DOUBLE * x) *
               cos(pow_weierstrass_b * RATEL_PI_DOUBLE * y) * cos(pow_weierstrass_b * RATEL_PI_DOUBLE * z);
    pow_weierstrass_a *= weierstrass_a;
    pow_weierstrass_b *= weierstrass_b;
  }
  *poisson_forcing = -3 * RATEL_PI_DOUBLE * RATEL_PI_DOUBLE * forcing;
  return CEED_ERROR_SUCCESS;
}

/// @}
