/// @file
/// Ratel linear elasticity manufactured solution QFunction source
#pragma once

#include <ceed/types.h>
#ifndef CEED_RUNNING_JIT_PASS
#include <math.h>
#endif

#include "../../models/elasticity-linear.h"      // IWYU pragma: export
#include "../../models/mms-elasticity-linear.h"  // IWYU pragma: export
#include "../utils.h"                            // IWYU pragma: export

/// @addtogroup RatelInternal
/// @{

/**
  @brief Compute true solution for linear elasticity MMS

  @details Uses an MMS adapted from doi:10.1016/j.compstruc.2019.106175

  @param[in]   ctx            QFunction context, holding `RatelMMSLinearElasticityParams`
  @param[in]   coords         Coordinate array
  @param[out]  true_solution  True solution

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelLinearElasticityMMSTrueSolution(void *ctx, const CeedScalar coords[3], CeedScalar true_solution[3]) {
  // Scaling
  RatelMMSLinearElasticityParams *context = (RatelMMSLinearElasticityParams *)ctx;
  const CeedScalar                A0      = context->A0;
  const CeedScalar                shift   = context->shift;
  const CeedScalar                scale   = context->scale;
  const CeedScalar                time    = context->time;

  // Setup
  const CeedScalar x = coords[0], y = coords[1], z = coords[2];

  // True solution
  true_solution[0] = time * A0 * sin(scale * x - shift) * sin(scale * y - shift) * sin(scale * z - shift);
  true_solution[1] = 2.0 * true_solution[0];
  true_solution[2] = 3.0 * true_solution[0];
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute forcing term for linear elasticity MMS

  @details Uses an MMS adapted from doi:10.1016/j.compstruc.2019.106175

  @param[in]   ctx          QFunction context, holding `RatelMMSLinearElasticityParams`
  @param[in]   coords       Coordinate array
  @param[out]  mms_forcing  Forcing term

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelLinearElasticityMMSForcing(void *ctx, const CeedScalar coords[3], CeedScalar mms_forcing[3]) {
  // Context
  RatelMMSLinearElasticityParams    *context  = (RatelMMSLinearElasticityParams *)ctx;
  const CeedScalar                   A0       = context->A0;
  const CeedScalar                   shift    = context->shift;
  const CeedScalar                   scale    = context->scale;
  const RatelLinearElasticityParams *material = (RatelLinearElasticityParams *)context->material;
  const CeedScalar                   E        = material->E;
  const CeedScalar                   nu       = material->nu;
  const CeedScalar                   mu       = E / (2 * (1 + nu));
  const CeedScalar                   lambda   = 2 * nu * mu / (1 - 2 * nu);

  // Setup
  const CeedScalar x = coords[0], y = coords[1], z = coords[2];

  // True solution
  CeedScalar true_solution[3] = {0., 0., 0.};

  RatelLinearElasticityMMSTrueSolution(ctx, coords, true_solution);

  // Forcing term
  const CeedScalar n_pi_sq = scale * scale;
  const CeedScalar ue1     = true_solution[0];
  const CeedScalar ue1_11 = -n_pi_sq * ue1, ue1_22 = -n_pi_sq * ue1, ue1_33 = -n_pi_sq * ue1;
  const CeedScalar ue1_12 = A0 * n_pi_sq * cos(scale * x - shift) * cos(scale * y - shift) * sin(scale * z - shift),
                   ue1_13 = A0 * n_pi_sq * cos(scale * x - shift) * sin(scale * y - shift) * cos(scale * z - shift),
                   ue1_23 = A0 * n_pi_sq * sin(scale * x - shift) * cos(scale * y - shift) * cos(scale * z - shift);

  // mu*(ue1_11 + ue1_22 + ue1_33) + (mu+lambda)*(ue1_11 + ue2_21 + ue3_31) + f1 = 0
  const CeedScalar ue2_21 = 2 * ue1_12;
  const CeedScalar ue3_31 = 3 * ue1_13;
  mms_forcing[0]          = -mu * (ue1_11 + ue1_22 + ue1_33) - (mu + lambda) * (ue1_11 + ue2_21 + ue3_31);

  // mu*(ue2_11 + ue2_22 + ue2_33) + (mu+lambda)*(ue1_12 + ue2_22 + ue3_32) + f2 = 0
  const CeedScalar ue2_11 = 2 * ue1_11, ue2_22 = 2 * ue1_22, ue2_33 = 2 * ue1_33;
  const CeedScalar ue3_32 = 3 * ue1_23;
  mms_forcing[1]          = -mu * (ue2_11 + ue2_22 + ue2_33) - (mu + lambda) * (ue1_12 + ue2_22 + ue3_32);

  // mu*(ue3_11 + ue3_22 + ue3_33) + (mu+lambda)*(ue1_13 + ue2_23 + ue3_33) + f3 = 0
  const CeedScalar ue3_11 = 3 * ue1_11, ue3_22 = 3 * ue1_22, ue3_33 = 3 * ue1_33;
  const CeedScalar ue2_23 = 2 * ue1_23;
  mms_forcing[2]          = -mu * (ue3_11 + ue3_22 + ue3_33) - (mu + lambda) * (ue1_13 + ue2_23 + ue3_33);
  return CEED_ERROR_SUCCESS;
}

/// @}
