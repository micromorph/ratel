/// @file
/// Ratel linear poroelasticity manufactured solution QFunction source
#pragma once

#include <ceed/types.h>
#ifndef CEED_RUNNING_JIT_PASS
#include <math.h>
#endif

#include "../../models/mms-poroelasticity-linear.h"  // IWYU pragma: export
#include "../../models/poroelasticity-linear.h"      // IWYU pragma: export
#include "../utils.h"                                // IWYU pragma: export

/// @addtogroup RatelInternal
/// @{

/**
  @brief Compute true solution for linear poroelasticity MMS

  @details Uses an MMS adapted from doi:10.1016/j.compstruc.2019.106175

  @param[in]   ctx              QFunction context, holding `RatelMMSPoroElasticityLinearParams`
  @param[in]   coords           Coordinate array
  @param[out]  true_solution_u  True solution, displacement
  @param[out]  true_solution_p  True solution, pressure

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelPoroElasticityMMSTrueSolution(void *ctx, const CeedScalar coords[3], CeedScalar true_solution_u[3],
                                                             CeedScalar *true_solution_p) {
  // Context
  // Scaling

  RatelMMSPoroElasticityLinearParams *context = (RatelMMSPoroElasticityLinearParams *)ctx;
  const CeedScalar                    A0      = context->A0;
  const CeedScalar                    shift   = context->shift;
  const CeedScalar                    scale   = context->scale;
  const CeedScalar                    time    = context->time;

  // Setup
  const CeedScalar x = coords[0], y = coords[1], z = coords[2];

  // True solution, displacement
  true_solution_u[0] = A0 * sin(scale * x - shift) * sin(scale * y - shift) * sin(scale * z - shift) * sin(time);
  true_solution_u[1] = 2.0 * true_solution_u[0];
  true_solution_u[2] = 3.0 * true_solution_u[0];

  // True solution, pressure
  if (true_solution_p) *true_solution_p = 4.0 * true_solution_u[0];
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute displacement forcing term for linear poroelasticity MMS

  @details Uses an MMS adapted from doi:10.1016/j.compstruc.2019.106175

  @param[in]   ctx            QFunction context, holding `RatelMMSPoroElasticityLinearParams`
  @param[in]   coords         Coordinate array
  @param[out]  mms_forcing_u  Forcing term for displacement

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelPoroElasticityMMSForcing(void *ctx, const CeedScalar coords[3], CeedScalar mms_forcing_u[3]) {
  // Context
  // Scaling
  RatelMMSPoroElasticityLinearParams    *context  = (RatelMMSPoroElasticityLinearParams *)ctx;
  const CeedScalar                       A0       = context->A0;
  const CeedScalar                       shift    = context->shift;
  const CeedScalar                       scale    = context->scale;
  const CeedScalar                       time     = context->time;
  const RatelLinearPoroElasticityParams *material = (RatelLinearPoroElasticityParams *)context->material;
  const CeedScalar                       lambda_d = material->lambda_d;
  const CeedScalar                       mu_d     = material->mu_d;
  const CeedScalar                       B        = material->B;

  // // Setup true solution
  const CeedScalar x = coords[0], y = coords[1], z = coords[2];
  CeedScalar       true_solution_u[3] = {0., 0., 0.};

  RatelPoroElasticityMMSTrueSolution(ctx, coords, true_solution_u, NULL);
  const CeedScalar ue1    = true_solution_u[0];  // ue2 = 2 * ue1, ue3 = 3 * ue1;
  // Forcing term for displacement
  // mu*(ue1_11 + ue1_22 + ue1_33) + (mu + lambda)*(ue1_11 + ue2_21 + ue3_31) - B*p_1 + f1 = 0
  const CeedScalar ue1_11 = -scale * scale * ue1, ue1_22 = ue1_11, ue1_33 = ue1_11;
  const CeedScalar ue1_12 = A0 * scale * scale * cos(scale * x - shift) * cos(scale * y - shift) * sin(scale * z - shift) * sin(time),
                   ue1_13 = A0 * scale * scale * cos(scale * x - shift) * sin(scale * y - shift) * cos(scale * z - shift) * sin(time),
                   ue1_23 = A0 * scale * scale * sin(scale * x - shift) * cos(scale * y - shift) * cos(scale * z - shift) * sin(time);
  const CeedScalar ue2_21 = 2 * ue1_12;
  const CeedScalar ue3_31 = 3 * ue1_13;
  const CeedScalar pe_1   = 4. * A0 * scale * cos(scale * x - shift) * sin(scale * y - shift) * sin(scale * z - shift) * sin(time);
  mms_forcing_u[0]        = -mu_d * (ue1_11 + ue1_22 + ue1_33) - (mu_d + lambda_d) * (ue1_11 + ue2_21 + ue3_31) + B * pe_1;

  // mu*(ue2_11 + ue2_22 + ue2_33) + (mu + lambda)*(ue1_12 + ue2_22 + ue3_32) - B*p_2 + f2 = 0
  const CeedScalar ue2_11 = 2 * ue1_11, ue2_22 = 2 * ue1_22, ue2_33 = 2 * ue1_33;
  const CeedScalar ue3_32 = 3 * ue1_23;
  const CeedScalar pe_2   = 4. * A0 * scale * sin(scale * x - shift) * cos(scale * y - shift) * sin(scale * z - shift) * sin(time);
  mms_forcing_u[1]        = -mu_d * (ue2_11 + ue2_22 + ue2_33) - (mu_d + lambda_d) * (ue1_12 + ue2_22 + ue3_32) + B * pe_2;

  // mu*(ue3_11 + ue3_22 + ue3_33) + (mu + lambda)*(ue1_13 + ue2_23 + ue3_33) - B*p_3 + f3 = 0
  const CeedScalar ue3_11 = 3 * ue1_11, ue3_22 = 3 * ue1_22, ue3_33 = 3 * ue1_33;
  const CeedScalar ue2_23 = 2 * ue1_23;
  const CeedScalar pe_3   = 4. * A0 * scale * sin(scale * x - shift) * sin(scale * y - shift) * cos(scale * z - shift) * sin(time);
  mms_forcing_u[2]        = -mu_d * (ue3_11 + ue3_22 + ue3_33) - (mu_d + lambda_d) * (ue1_13 + ue2_23 + ue3_33) + B * pe_3;
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute pressure forcing term for linear poroelasticity MMS

  @details Uses an MMS adapted from doi:10.1016/j.compstruc.2019.106175

  @param[in]   ctx            QFunction context, holding `RatelMMSPoroElasticityLinearParams`
  @param[in]   coords         Coordinate array
  @param[out]  mms_forcing_p  Forcing term for pressure

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelPoroElasticityMMSGamma(void *ctx, const CeedScalar coords[3], CeedScalar *mms_forcing_p) {
  // Context
  // Scaling
  RatelMMSPoroElasticityLinearParams    *context    = (RatelMMSPoroElasticityLinearParams *)ctx;
  const CeedScalar                       A0         = context->A0;
  const CeedScalar                       shift      = context->shift;
  const CeedScalar                       scale      = context->scale;
  const CeedScalar                       time       = context->time;
  const RatelLinearPoroElasticityParams *material   = (RatelLinearPoroElasticityParams *)context->material;
  const CeedScalar                       B          = material->B;
  const CeedScalar                       eta_f      = material->eta_f;
  const CeedScalar                       varkappa_0 = material->varkappa_0;
  const CeedScalar                       M          = material->M;

  // // Setup true solution
  const CeedScalar x = coords[0], y = coords[1], z = coords[2];
  CeedScalar       true_solution_u[3] = {0., 0., 0.}, true_solution_p = 0.;

  RatelPoroElasticityMMSTrueSolution(ctx, coords, true_solution_u, &true_solution_p);

  // Forcing term for pressure
  // gamma_t = pe_t / M + B * div(ue_t) + div(q) = pe_t / M + B * div(ue_t) - varkappa_0 * \nabla^2 (pe) / eta_f
  const CeedScalar pe_11 = -scale * scale * true_solution_p, pe_22 = pe_11, pe_33 = pe_11;
  const CeedScalar pe_t   = 4. * A0 * scale * sin(scale * x - shift) * sin(scale * y - shift) * sin(scale * z - shift) * cos(time);
  const CeedScalar ue1_1t = A0 * scale * cos(scale * x - shift) * sin(scale * y - shift) * sin(scale * z - shift) * cos(time),
                   ue1_2t = A0 * scale * sin(scale * x - shift) * cos(scale * y - shift) * sin(scale * z - shift) * cos(time),
                   ue1_3t = A0 * scale * sin(scale * x - shift) * sin(scale * y - shift) * cos(scale * z - shift) * cos(time);
  const CeedScalar ue2_2t = 2 * ue1_2t, ue3_3t = 3 * ue1_3t;
  *mms_forcing_p = pe_t / M + B * (ue1_1t + ue2_2t + ue3_3t) - varkappa_0 * (pe_11 + pe_22 + pe_33) / eta_f;
  return CEED_ERROR_SUCCESS;
}
/// @}
