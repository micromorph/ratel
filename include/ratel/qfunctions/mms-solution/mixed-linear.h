/// @file
/// Ratel mixed linear elasticity manufactured solution QFunction source
#pragma once

#include <ceed/types.h>
#ifndef CEED_RUNNING_JIT_PASS
#include <math.h>
#endif

#include "../../models/elasticity-mixed-linear.h"      // IWYU pragma: export
#include "../../models/mms-elasticity-mixed-linear.h"  // IWYU pragma: export
#include "../utils.h"                                  // IWYU pragma: export

/// @addtogroup RatelInternal
/// @{

/**
  @brief Compute true solution for mixed linear elasticity MMS

  @details Uses an MMS adapted from doi:10.1016/j.compstruc.2019.106175

  @param[in]   ctx              QFunction context, holding `RatelMMSMixedLinearElasticityParams`
  @param[in]   coords           Coordinate array
  @param[out]  true_solution_u  True solution, displacement
  @param[out]  true_solution_p  True solution, pressure

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelMixedLinearElasticityMMSTrueSolution(void *ctx, const CeedScalar coords[3], CeedScalar true_solution_u[3],
                                                                    CeedScalar *true_solution_p) {
  // Context
  // Scaling
  RatelMMSMixedLinearElasticityParams    *context     = (RatelMMSMixedLinearElasticityParams *)ctx;
  const CeedScalar                        A0          = context->A0;
  const CeedScalar                        shift       = context->shift;
  const CeedScalar                        scale       = context->scale;
  const CeedScalar                        time        = context->time;
  const RatelMixedLinearElasticityParams *material    = (RatelMixedLinearElasticityParams *)context->material;
  const CeedScalar                        E           = material->E;
  const CeedScalar                        nu          = material->nu;
  const CeedScalar                        nu_primal   = material->nu_primal;
  const CeedScalar                        mu          = E / (2 * (1 + nu));
  const CeedScalar                        two_mu      = 2 * mu;
  const CeedScalar                        bulk        = two_mu * (1 + nu) / (3. * (1 - 2 * nu));
  const CeedScalar                        bulk_primal = two_mu * (1 + nu_primal) / (3. * (1 - 2 * nu_primal));

  // Setup
  const CeedScalar x = coords[0], y = coords[1], z = coords[2];

  // True solution, displacement
  true_solution_u[0] = time * A0 * sin(scale * x - shift) * sin(scale * y - shift) * sin(scale * z - shift);
  true_solution_u[1] = 2.0 * true_solution_u[0];
  true_solution_u[2] = 3.0 * true_solution_u[0];

  // True solution, pressure
  if (true_solution_p) {
    const CeedScalar ue1_1 = A0 * scale * cos(scale * x - shift) * sin(scale * y - shift) * sin(scale * z - shift),
                     ue1_2 = A0 * scale * sin(scale * x - shift) * cos(scale * y - shift) * sin(scale * z - shift),
                     ue1_3 = A0 * scale * sin(scale * x - shift) * sin(scale * y - shift) * cos(scale * z - shift);
    const CeedScalar ue2_2 = 2 * ue1_2, ue3_3 = 3 * ue1_3;

    *true_solution_p = -time * (bulk - bulk_primal) * (ue1_1 + ue2_2 + ue3_3);
  }
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute forcing term for mixed linear elasticity MMS

  @details Uses an MMS adapted from doi:10.1016/j.compstruc.2019.106175

  @param[in]   ctx            QFunction context, holding `RatelMMSMixedLinearElasticityParams`
  @param[in]   coords         Coordinate array
  @param[out]  mms_forcing_u  Forcing term for displacement

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelMixedLinearElasticityMMSForcing(void *ctx, const CeedScalar coords[3], CeedScalar mms_forcing_u[3]) {
  // Context
  // Scaling
  RatelMMSMixedLinearElasticityParams    *context  = (RatelMMSMixedLinearElasticityParams *)ctx;
  const CeedScalar                        A0       = context->A0;
  const CeedScalar                        shift    = context->shift;
  const CeedScalar                        scale    = context->scale;
  const RatelMixedLinearElasticityParams *material = (RatelMixedLinearElasticityParams *)context->material;
  const CeedScalar                        E        = material->E;
  const CeedScalar                        nu       = material->nu;
  const CeedScalar                        mu       = E / (2 * (1 + nu));
  const CeedScalar                        lambda   = 2 * nu * mu / (1 - 2 * nu);

  // Setup true solution
  const CeedScalar x = coords[0], y = coords[1], z = coords[2];
  CeedScalar       true_solution_u[3] = {0., 0., 0.};

  RatelMixedLinearElasticityMMSTrueSolution(ctx, coords, true_solution_u, NULL);
  const CeedScalar ue1    = true_solution_u[0];  // ue2 = 2 * ue1, ue3 = 3 * ue1;
  // Forcing term
  // mu*(ue1_11 + ue1_22 + ue1_33) + (mu + lambda)*(ue1_11 + ue2_21 + ue3_31) + f1 = 0
  const CeedScalar ue1_11 = -scale * scale * ue1, ue1_22 = ue1_11, ue1_33 = ue1_11;
  const CeedScalar ue1_12 = A0 * scale * scale * cos(scale * x - shift) * cos(scale * y - shift) * sin(scale * z - shift),
                   ue1_13 = A0 * scale * scale * cos(scale * x - shift) * sin(scale * y - shift) * cos(scale * z - shift),
                   ue1_23 = A0 * scale * scale * sin(scale * x - shift) * cos(scale * y - shift) * cos(scale * z - shift);
  const CeedScalar ue2_21 = 2 * ue1_12;
  const CeedScalar ue3_31 = 3 * ue1_13;
  mms_forcing_u[0]        = -mu * (ue1_11 + ue1_22 + ue1_33) - (mu + lambda) * (ue1_11 + ue2_21 + ue3_31);

  // mu*(ue2_11 + ue2_22 + ue2_33) + (mu + lambda)*(ue1_12 + ue2_22 + ue3_32) + f2 = 0
  const CeedScalar ue2_11 = 2 * ue1_11, ue2_22 = 2 * ue1_22, ue2_33 = 2 * ue1_33;
  const CeedScalar ue3_32 = 3 * ue1_23;
  mms_forcing_u[1]        = -mu * (ue2_11 + ue2_22 + ue2_33) - (mu + lambda) * (ue1_12 + ue2_22 + ue3_32);

  // mu*(ue3_11 + ue3_22 + ue3_33) + (mu + lambda)*(ue1_13 + ue2_23 + ue3_33) + f3 = 0
  const CeedScalar ue3_11 = 3 * ue1_11, ue3_22 = 3 * ue1_22, ue3_33 = 3 * ue1_33;
  const CeedScalar ue2_23 = 2 * ue1_23;
  mms_forcing_u[2]        = -mu * (ue3_11 + ue3_22 + ue3_33) - (mu + lambda) * (ue1_13 + ue2_23 + ue3_33);
  return CEED_ERROR_SUCCESS;
}

/// @}
