/// @file
/// Ratel volumetric forcing term for CEED BP3 manufactured solution
#include <ceed/types.h>

#include "../mms-solution/ceed-scalar-bps.h"  // IWYU pragma: export

/// @addtogroup RatelInternal
/// @{

/**
  @brief Compute forcing term for CEED scalar BP3 MMS

  @param[in]   ctx  QFunction context, holding `RatelMMSCEEDBPsParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - quadrature point coordinates
  @param[out]  out  Output array
                      - 0 - forcing term

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(MMSForce_CEED_BP3)(void *ctx, const CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*coords)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*force) = (CeedScalar(*))out[0];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Setup
    const CeedScalar wdetJ = q_data[0][i];
    const CeedScalar X[3]  = {coords[0][i], coords[1][i], coords[2][i]};
    CeedScalar       poisson_forcing;
    RatelScalarBPsPoissonForcing(ctx, X, &poisson_forcing);

    force[i] = wdetJ * poisson_forcing;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
