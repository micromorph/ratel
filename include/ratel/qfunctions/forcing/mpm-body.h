/// @file
/// Ratel user-specified volumetric body force QFunction source
#include <ceed/types.h>

#include "../../models/forcing.h"  // IWYU pragma: export
#include "../boundaries/flexible-loading.h"
#include "../utils.h"

/// @addtogroup RatelInternal
/// @{

/**
  @brief Compute forcing term with user-specified body force and density defined at points

  @param[in]   ctx  QFunction context, holding `RatelForcingBodyParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - state data
                      - 2 - density
  @param[out]  out  Output array
                      - 0 - forcing term

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(MPMBodyForce)(void *ctx, const CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar *rho                 = in[1];

  // Outputs
  CeedScalar(*force)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Context
  const RatelForcingBodyParams *context         = (RatelForcingBodyParams *)ctx;
  const CeedInt                 prev_time_index = RatelBCInterpGetPreviousKnotIndex(context->time, context->num_times, context->times);
  CeedScalar                    a[3]            = {0.};

  RatelBCInterpolate(context->interpolation_type, context->time, prev_time_index, context->num_times, context->times, 3, context->acceleration, a);

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Setup
    const CeedScalar wdetJ = q_data[0][i];

    // Forcing function
    for (CeedInt j = 0; j < 3; j++) {
      force[j][i] = -rho[i] * wdetJ * a[j];
    }
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
