/// @file
/// Ratel volumetric forcing term for linear poroelasticity manufactured solution
#include <ceed/types.h>

#include "../mms-solution/poroelastic-linear.h"  // IWYU pragma: export

/// @addtogroup RatelInternal
/// @{

/**
  @brief Compute forcing term for linear poroelasticity MMS

  @param[in]   ctx  QFunction context, holding `RatelMMSPoroElasticityLinearParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - quadrature point coordinates
  @param[out]  out  Output array
                      - 0 - forcing term, displacement field
                      - 1 - forcing term, pressure field

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(MMSForce_PoroElasticityLinear)(void *ctx, const CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*coords)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*force_u)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];
  CeedScalar(*force_p)             = out[1];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Setup
    const CeedScalar X[3]             = {coords[0][i], coords[1][i], coords[2][i]};
    CeedScalar       wdetJ            = q_data[0][i];
    CeedScalar       mms_forcing_u[3] = {0., 0., 0.};
    CeedScalar       mms_forcing_p    = 0.;

    // Compute displacement forcing
    RatelPoroElasticityMMSForcing(ctx, X, mms_forcing_u);
    // Compute pressure forcing
    RatelPoroElasticityMMSGamma(ctx, X, &mms_forcing_p);

    // Forcing function for displacement
    // -- Component 1
    force_u[0][i] = -mms_forcing_u[0] * wdetJ;
    // -- Component 2
    force_u[1][i] = -mms_forcing_u[1] * wdetJ;
    // -- Component 3
    force_u[2][i] = -mms_forcing_u[2] * wdetJ;

    // Forcing function for pressure
    force_p[i] = -mms_forcing_p * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
