/// @file
/// Ratel volumetric forcing term for mixed linear elasticity manufactured solution
#include <ceed/types.h>

#include "../mms-solution/mixed-linear.h"  // IWYU pragma: export

/// @addtogroup RatelInternal
/// @{

/**
  @brief Compute forcing term for mixed linear elasticity MMS

  @param[in]   ctx  QFunction context, holding `RatelMMSMixedLinearElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - quadrature point coordinates
  @param[out]  out  Output array
                      - 0 - forcing term, displacement field

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(MMSForce_MixedLinear)(void *ctx, const CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*coords)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[1];

  // Outputs
  CeedScalar(*force_u)[CEED_Q_VLA] = (CeedScalar(*)[CEED_Q_VLA])out[0];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Setup
    const CeedScalar X[3]             = {coords[0][i], coords[1][i], coords[2][i]};
    CeedScalar       wdetJ            = q_data[0][i];
    CeedScalar       mms_forcing_u[3] = {0., 0., 0.};

    RatelMixedLinearElasticityMMSForcing(ctx, X, mms_forcing_u);
    // Forcing function
    // -- Component 1
    force_u[0][i] = -mms_forcing_u[0] * wdetJ;
    // -- Component 2
    force_u[1][i] = -mms_forcing_u[1] * wdetJ;
    // -- Component 3
    force_u[2][i] = -mms_forcing_u[2] * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute external energy cause by forcing term for mixed linear elasticity MMS

  @param[in]   ctx  QFunction context, holding `RatelMMSMixedLinearElasticityParams`
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - qdata
                      - 1 - quadrature point coordinates
                      - 2 - displacement u
  @param[out]  out  Output array
                      - 0 - energy due to mms force

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(MMSForceEnergy_MixedLinear)(void *ctx, const CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];
  const CeedScalar(*coords)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[1];
  const CeedScalar(*u)[CEED_Q_VLA]      = (const CeedScalar(*)[CEED_Q_VLA])in[2];

  // Outputs
  CeedScalar(*force_energy) = (CeedScalar(*))out[0];

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Setup
    const CeedScalar X[3]             = {coords[0][i], coords[1][i], coords[2][i]};
    CeedScalar       wdetJ            = q_data[0][i];
    CeedScalar       mms_forcing_u[3] = {0., 0., 0.}, u_dot_f = 0.0;

    RatelMixedLinearElasticityMMSForcing(ctx, X, mms_forcing_u);

    for (CeedInt j = 0; j < 3; j++) {
      u_dot_f += u[j][i] * mms_forcing_u[j];
    }
    force_energy[i] = -u_dot_f * wdetJ;
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
