/// @file
/// Ratel mass operator QFunction source
#include <ceed/types.h>

#include "../models/scaled-mass.h"  // IWYU pragma: export
#include "utils.h"                  // IWYU pragma: export

/// @addtogroup RatelInternal
/// @{

/**
  @brief Apply scaled mass operator

  @param[in]   ctx  QFunction context, holding `RatelScaledMassParams` struct
  @param[in]   Q    Number of quadrature points
  @param[in]   in   Input arrays
                      - 0 - volumetric qdata
                      - 1 - u
  @param[out]  out  Output array
                      - 0 - v

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION(ScaledMass)(void *ctx, const CeedInt Q, const CeedScalar *const *in, CeedScalar *const *out) {
  // Inputs
  const CeedScalar(*q_data)[CEED_Q_VLA] = (const CeedScalar(*)[CEED_Q_VLA])in[0];

  const RatelScaledMassParams *context = (RatelScaledMassParams *)ctx;

  // Quadrature Point Loop
  CeedPragmaSIMD for (CeedInt i = 0; i < Q; i++) {
    // Scaled mass operator
    for (CeedInt j = 0; j < 1; j++) {
      RatelScaledMassApplyAtQuadraturePoint(Q, i, context->field_sizes[j], context->rho * q_data[0][i], in[j + 1], out[j]);
    }
  }  // End of Quadrature Point Loop
  return CEED_ERROR_SUCCESS;
}

/// @}
