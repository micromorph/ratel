/// @file
/// Ratel utility helpers QFunction source
#pragma once

#include <ceed/types.h>
#ifndef CEED_RUNNING_JIT_PASS
#include <math.h>
#include <stdbool.h>
#endif

/// @addtogroup RatelInternal
/// @{

#define RATEL_PI_DOUBLE 3.14159265358979323846
#define RATEL_EPSILON_DOUBLE 1e-16

/**
  @brief Generic max function

  @param[in]  a  First value
  @param[in]  b  Second value

  @return Maximum of both values
**/
#define RatelMax(a, b) ((a) > (b) ? (a) : (b))

/**
  @brief Generic min function

  @param[in]  a  First value
  @param[in]  b  Second value

  @return Minimum of both values
**/
#define RatelMin(a, b) ((a) < (b) ? (a) : (b))

/**
  @brief Check if a value is in an interval.

  @param[in]  value  Value to check
  @param[in]  min    Minimum value of the interval
  @param[in]  max    Maximum value of the interval
**/
#define RatelValueInInterval(value, min, max) ((value) >= (min) && (value) <= (max))

/**
  @brief Check if a point is in a bounding box.

  @param[in]  x    Point to check
  @param[in]  min  Minimum values of the bounding box
  @param[in]  max  Maximum values of the bounding box
**/
CEED_QFUNCTION_HELPER bool RatelCoordinatesInBoundingBox(const CeedScalar x[3], const CeedScalar min[3], const CeedScalar max[3]) {
  return RatelValueInInterval(x[0], min[0], max[0]) && RatelValueInInterval(x[1], min[1], max[1]) && RatelValueInInterval(x[2], min[2], max[2]);
}

// Apply dXdx^T and weight; 9 is flop of scalar multiplication wdetJ
#define FLOPS_dXdxwdetJ (FLOPS_MatMatMult + 9)
#define FLOPS_ScaledMass 9

/**
  @brief Series approximation of `log1p()`

  `log1p()` is not vectorized in libc.
  The series expansion up to the sixth term is accurate to 1e-10 relative error in the range `2/3 < J < 3/2`.
  This is accurate enough for many hyperelastic applications, but perhaps not for materials like foams that may encounter volumetric strains on the
order of `1/5 < J < 5`.

  @param[in]  x  Scalar value

  @return A scalar value: `log1p(x)`
**/
CEED_QFUNCTION_HELPER CeedScalar RatelLog1pSeries(CeedScalar x) {
  CeedScalar sum = 0;
  CeedScalar y   = x / (2. + x);
  CeedScalar y2  = y * y;

  sum += y;
  for (CeedInt i = 0; i < 5; i++) {
    y *= y2;
    sum += y / (2 * i + 3);
  }
  return 2 * sum;
}
#define FLOPS_Log1pSeries 30

/**
  @brief Series approximation of `expm1()`

  @param[in]  x  Scalar value

  @return A scalar value: `expm1(x)`
**/
CEED_QFUNCTION_HELPER CeedScalar RatelExpm1Series(CeedScalar x) {
  // max error 1.94289029309402e-16 on [0, 1/4]
  const CeedScalar a_1 = 0.9999999999999573;
  const CeedScalar a_2 = 0.5000000000052572;
  const CeedScalar a_3 = 0.16666666643305858;
  const CeedScalar a_4 = 0.04166667167982326;
  const CeedScalar a_5 = 0.008333274157993953;
  const CeedScalar a_6 = 0.0013892934750217851;
  const CeedScalar a_7 = 0.0001968199740818832;
  const CeedScalar a_8 = 2.812789332715902e-05;
  return x < 0.25 ? x * (a_1 + x * (a_2 + x * (a_3 + x * (a_4 + x * (a_5 + x * (a_6 + x * (a_7 + x * a_8))))))) : exp(x) - 1;
}
#define FLOPS_Expm1Series 15

/**
  @brief Approximation of atan(x) with max error 1.95815585968262e-14 on [-1,1]

  Computed via Remez algorithm with 31 degree polynomial using github.com/DKenefake/OptimalPoly

  @param[in]  x  Input value

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER CeedScalar RatelAtanSeries(CeedScalar x) {
  // Series approximation of `atan()` with max error 1.95815585968262e-14 on [-1,1]

  const CeedScalar a_1  = 0.9999999999993525;
  const CeedScalar a_3  = -0.33333333321533365;
  const CeedScalar a_5  = 0.19999999355144213;
  const CeedScalar a_7  = -0.14285697629655103;
  const CeedScalar a_9  = 0.11110863734873448;
  const CeedScalar a_11 = -0.09088554595195618;
  const CeedScalar a_13 = 0.07676939640679648;
  const CeedScalar a_15 = -0.06594663814817052;
  const CeedScalar a_17 = 0.05632170111970379;
  const CeedScalar a_19 = -0.046021855121716235;
  const CeedScalar a_21 = 0.03405390225731899;
  const CeedScalar a_23 = -0.021369751660910184;
  const CeedScalar a_25 = 0.010577355252604226;
  const CeedScalar a_27 = -0.003785491301089091;
  const CeedScalar a_29 = 0.0008586183815558662;
  const CeedScalar a_31 = -9.184922434344886e-05;
  const CeedScalar x_sq = x * x;

  // Evaluate polynomial using Horner's method
  // clang-format off
  return x * (a_1 + x_sq * (a_3 + x_sq * (a_5 + x_sq * (a_7 + x_sq * (a_9 + x_sq * (a_11 + x_sq * (a_13 + x_sq * (a_15 + x_sq * (a_17 +
    x_sq * (a_19 + x_sq * (a_21 + x_sq * (a_23 + x_sq * (a_25 + x_sq * (a_27 + x_sq * (a_29 + x_sq * a_31)))))))))))))));
  // clang-format on
}
// Technically, the number of flops is 17 with FMA, but we assume FMA takes 2 flops
#define FLOPS_AtanSeries (15 * 2 + 2)

/**
  @brief Compute sign of scalar value

  @param[in]  x  Scalar value

  @return An integer: -1 if `x` is negative and 1 if `x` is positive
**/
CEED_QFUNCTION_HELPER CeedInt RatelSign(CeedScalar x) { return (x > 0) - (x < 0); }

/**
  @brief Pack stored values at quadrature point

  @param[in]   Q         Number of quadrature points
  @param[in]   i         Current quadrature point
  @param[in]   start     Starting index to store components
  @param[in]   num_comp  Number of components to store
  @param[in]   local     Local values for quadrature point i
  @param[out]  stored    Stored values

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelStoredValuesPack(CeedInt Q, CeedInt i, CeedInt start, CeedInt num_comp, const CeedScalar *local, CeedScalar *stored) {
  for (CeedInt j = 0; j < num_comp; j++) stored[(start + j) * Q + i] = local[j];
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Unpack stored values at quadrature point

  @param[in]   Q         Number of quadrature points
  @param[in]   i         Current quadrature point
  @param[in]   start     Starting index to store components
  @param[in]   num_comp  Number of components to store
  @param[in]   stored    Stored values
  @param[out]  local     Local values for quadrature point i

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelStoredValuesUnpack(CeedInt Q, CeedInt i, CeedInt start, CeedInt num_comp, const CeedScalar *stored,
                                                  CeedScalar *local) {
  for (CeedInt j = 0; j < num_comp; j++) local[j] = stored[(start + j) * Q + i];
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute dot product of two length 3 vectors

  @param[in]  a  First input vector
  @param[in]  b  Second input vector

  @return A scalar value: `a . b`
**/
CEED_QFUNCTION_HELPER CeedScalar RatelDot3(const CeedScalar a[3], const CeedScalar b[3]) { return a[0] * b[0] + a[1] * b[1] + a[2] * b[2]; }
#define FLOPS_Dot3 5

/**
  @brief Compute norm of a length 3 vector

  @param[in]  a  Input vector

  @return A scalar value: `|a|`
**/
CEED_QFUNCTION_HELPER CeedScalar RatelNorm3(const CeedScalar a[3]) { return sqrt(RatelDot3(a, a)); }
#define FLOPS_Norm3 6

/**
  @brief Compute `b = alpha a` for a length 3 vector

  @param[in]   alpha  Scaling factor
  @param[in]   a      Input vector
  @param[out]  b      Output vector

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelScalarVecMult(CeedScalar alpha, const CeedScalar a[3], CeedScalar b[3]) {
  b[0] = alpha * a[0];
  b[1] = alpha * a[1];
  b[2] = alpha * a[2];
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_ScalarVecMult 3

/**
  @brief Compute `d = alpha * a + beta * b + gamma * c` for three length 3 vectors

  @param[in]   alpha  First scaling factor
  @param[in]   a      First input vector
  @param[in]   beta   Second scaling factor
  @param[in]   b      Second input vector
  @param[in]   gamma  Third scaling factor
  @param[in]   c      Third input vector
  @param[out]  d      Output vector

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelVecVecVecAdd(CeedScalar alpha, const CeedScalar a[3], CeedScalar beta, const CeedScalar b[3], CeedScalar gamma,
                                            const CeedScalar c[3], CeedScalar d[3]) {
  for (CeedInt k = 0; k < 3; k++) {
    d[k] = alpha * a[k] + beta * b[k] + gamma * c[k];
  }
  return CEED_ERROR_SUCCESS;
}
// Scalar multiplication alpha/beta/gamma is not considered. You need to add 3 (per scalar) FLOPs when alpha/beta/gamma != 1.0
#define FLOPS_VecVecVecAdd 6

/**
  @brief Compute `c = a - b` for two length 3 vectors

  @param[in]   a  First input vector
  @param[in]   b  Second input vector
  @param[out]  c  Output vector

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelVecVecSubtract(const CeedScalar a[3], const CeedScalar b[3], CeedScalar c[3]) {
  c[0] = a[0] - b[0];
  c[1] = a[1] - b[1];
  c[2] = a[2] - b[2];
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_VecVecSubtract 3

/**
  @brief Compute `c = a x b` for two length 3 vectors

  @param[in]   a  First input vector
  @param[in]   b  Second input vector
  @param[out]  c  Output vector

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelVecVecCross(const CeedScalar a[3], const CeedScalar b[3], CeedScalar c[3]) {
  c[0] = a[1] * b[2] - a[2] * b[1];
  c[1] = a[2] * b[0] - a[0] * b[2];
  c[2] = a[0] * b[1] - a[1] * b[0];
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_VecVecCross 9

/**
  @brief Compute outer product of a vector length 3 with it self results in symmetric matrix `A_sym = a*a^T`

  @param[in]   a      Input vector
  @param[out]  A_sym  Symmetric matrix

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelVecOuterMult(const CeedScalar a[3], CeedScalar A_sym[6]) {
  const CeedInt ind_j[6] = {0, 1, 2, 1, 0, 0}, ind_k[6] = {0, 1, 2, 2, 2, 1};

  for (CeedInt m = 0; m < 6; m++) {
    A_sym[m] = 0.;
    A_sym[m] += a[ind_j[m]] * a[ind_k[m]];
  }
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_VecOuterMult 12

/**
  @brief Compute outer product of two vectors of length 3 results in a matrix `A = a*b^T`

  @param[in]   a  Input vector
  @param[in]   b  Input vector
  @param[out]  A  Matrix

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelVecVecOuterMult(const CeedScalar a[3], const CeedScalar b[3], CeedScalar A[3][3]) {
  for (CeedInt i = 0; i < 3; i++) {
    for (CeedInt j = 0; j < 3; j++) {
      A[i][j] = 0.;
      A[i][j] += a[i] * b[j];
    }
  }
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_VecVecOuterMult 18

/**
  @brief Compute `B_sym = alpha A_sym`

  @param[in]   alpha  Scaling factor
  @param[in]   A_sym  Input 3x3 matrix, in symmetric representation
  @param[out]  B_sym  Output 3x3 matrix, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelScalarMatMultSymmetric(CeedScalar alpha, const CeedScalar A_sym[6], CeedScalar B_sym[6]) {
  for (CeedInt i = 0; i < 6; i++) B_sym[i] = alpha * A_sym[i];
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_ScalarMatMultSymmetric 6

/**
  @brief Compute `b = alpha A x` for a length 3 vector and a 3x3 matrix

  @param[in]   alpha  Scaling factor
  @param[in]   A      Input matrix
  @param[in]   x      Input vector
  @param[out]  b      Output vector

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelMatVecMult(CeedScalar alpha, const CeedScalar A[3][3], const CeedScalar x[3], CeedScalar b[3]) {
  for (CeedInt j = 0; j < 3; j++) {
    b[j] = 0;
    for (CeedInt m = 0; m < 3; m++) {
      b[j] += alpha * A[j][m] * x[m];
    }
  }
  return CEED_ERROR_SUCCESS;
}
// Scalar multiplication alpha is not considered. You need to add 3 FLOPs when alpha != 1.0
#define FLOPS_MatVecMult 18

/**
  @brief Compute `b = alpha A^T x` for a length 3 vector and a 3x3 matrix

  @param[in]   alpha  Scaling factor
  @param[in]   A      Input matrix
  @param[in]   x      Input vector
  @param[out]  b      Output vector

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelMatTransposeVecMult(CeedScalar alpha, const CeedScalar A[3][3], const CeedScalar x[3], CeedScalar b[3]) {
  for (CeedInt j = 0; j < 3; j++) {
    b[j] = 0;
    for (CeedInt m = 0; m < 3; m++) {
      b[j] += alpha * A[m][j] * x[m];
    }
  }
  return CEED_ERROR_SUCCESS;
}
// Scalar multiplication alpha is not considered. You need to add 3 FLOPs when alpha != 1.0
#define FLOPS_MatTransposeVecMult 18

/**
  @brief Pack symmetric 3x3 matrix from full to symmetric representation.

  Matrix is packed as

  \f$ \begin{vmatrix} s_0 & s_5 & s_4 \\
                      s_5 & s_1 & s_3 \\
                      s_4 & s_3 & s_2 \end{vmatrix} \f$

  where \f$s_0, s_1, \dots \f$ are the indices of the vector in symmetric representation.

  @param[in]   full  Input full 3x3 matrix
  @param[out]  sym   Output matrix as length 6 vector, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelSymmetricMatPack(const CeedScalar full[3][3], CeedScalar sym[6]) {
  sym[0] = full[0][0];
  sym[1] = full[1][1];
  sym[2] = full[2][2];
  sym[3] = full[1][2];
  sym[4] = full[0][2];
  sym[5] = full[0][1];
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Unpack symmetric 3x3 matrix from symmetric to full representation.

  Matrix is packed as

  \f$ \begin{vmatrix} s_0 & s_5 & s_4 \\
                      s_5 & s_1 & s_3 \\
                      s_4 & s_3 & s_2 \end{vmatrix} \f$

  where \f$s_0, s_1, \dots \f$ are the indices of the vector in symmetric representation.

  @param[in]   sym   Input matrix as length 6 vector, in symmetric representation
  @param[out]  full  Output full 3x3 matrix

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelSymmetricMatUnpack(const CeedScalar sym[6], CeedScalar full[3][3]) {
  full[0][0] = sym[0];
  full[0][1] = sym[5];
  full[0][2] = sym[4];
  full[1][0] = sym[5];
  full[1][1] = sym[1];
  full[1][2] = sym[3];
  full[2][0] = sym[4];
  full[2][1] = sym[3];
  full[2][2] = sym[2];
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Unpack symmetric 3x3 matrix from symmetric to full representation

  Matrix is packed as

  \f$ \begin{vmatrix} s_0 & s_5 & s_4 \\
                      s_8 & s_1 & s_3 \\
                      s_7 & s_6 & s_2 \end{vmatrix} \f$

  where \f$s_0, s_1, \dots \f$ are the indices of the vector.

  @param[in]   a       Input matrix as length 9 vector
  @param[out]  A_full  Output full 3x3 matrix

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelMatUnpack(const CeedScalar a[9], CeedScalar A_full[3][3]) {
  A_full[0][0] = a[0];
  A_full[0][1] = a[5];
  A_full[0][2] = a[4];
  A_full[1][0] = a[8];
  A_full[1][1] = a[1];
  A_full[1][2] = a[3];
  A_full[2][0] = a[7];
  A_full[2][1] = a[6];
  A_full[2][2] = a[2];
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Pack 3x3 matrix from full to length 9 vector.

  @param[in]   A_full  Input full 3x3 matrix
  @param[out]  a       Output length 9 vector

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelMatPack(const CeedScalar A_full[3][3], CeedScalar a[9]) {
  a[0] = A_full[0][0];
  a[5] = A_full[0][1];
  a[4] = A_full[0][2];
  a[8] = A_full[1][0];
  a[1] = A_full[1][1];
  a[3] = A_full[1][2];
  a[7] = A_full[2][0];
  a[6] = A_full[2][1];
  a[2] = A_full[2][2];
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Unpack symmetric tensor into full tensor via Kelvin-Mandel normalization

  Matrix is packed as

  \f$ \begin{vmatrix} s_0 & s_5/ \sqrt{2} & s_4/ \sqrt{2} \\
                      s_5/ \sqrt{2} & s_1 & s_3/ \sqrt{2} \\
                      s_4/ \sqrt{2} & s_3/ \sqrt{2} & s_2/ \sqrt{2} \end{vmatrix} \f$

  where \f$s_0, s_1, \dots \f$ are the indices of the vector in symmetric representation.

  @param[in]   sym   Input matrix as length 6 vector, in symmetric representation
  @param[out]  full  Output full 3x3 matrix

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelKelvinMandelMatUnpack(const CeedScalar sym[6], CeedScalar full[3][3]) {
  const CeedScalar weight = 1 / sqrt(2.);
  full[0][0]              = sym[0];
  full[1][1]              = sym[1];
  full[2][2]              = sym[2];
  full[2][1] = full[1][2] = weight * sym[3];
  full[2][0] = full[0][2] = weight * sym[4];
  full[1][0] = full[0][1] = weight * sym[5];
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Pack full tensor into symmetric tensor of length 6 via Kelvin-Mandel normalization

  Matrix is packed as

  \f$ \begin{vmatrix} s_0 & s_5/ \sqrt{2} & s_4/ \sqrt{2} \\
                      s_5/ \sqrt{2} & s_1 & s_3/ \sqrt{2} \\
                      s_4/ \sqrt{2} & s_3/ \sqrt{2} & s_2/ \sqrt{2} \end{vmatrix} \f$

  where \f$s_0, s_1, \dots \f$ are the indices of the vector in symmetric representation.

  @param[in]   full  Input full 3x3 matrix
  @param[out]  sym   Output matrix as length 6 vector, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelKelvinMandelMatPack(const CeedScalar full[3][3], CeedScalar sym[6]) {
  const CeedScalar weight = sqrt(2.);
  sym[0]                  = full[0][0];
  sym[1]                  = full[1][1];
  sym[2]                  = full[2][2];
  sym[3]                  = full[2][1] * weight;
  sym[4]                  = full[2][0] * weight;
  sym[5]                  = full[1][0] * weight;
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute the trace of a 3x3 matrix

  @param[in]  A  Input 3x3 matrix

  @return A scalar value: `trace(A)`
**/
CEED_QFUNCTION_HELPER CeedScalar RatelMatTrace(const CeedScalar A[3][3]) { return A[0][0] + A[1][1] + A[2][2]; }
#define FLOPS_MatTrace 2

/**
  @brief Compute the trace of a 3x3 matrix

  @param[in]  A_sym  Input 3x3 matrix, in symmetric representation

  @return A scalar value: `trace(A)`
**/
CEED_QFUNCTION_HELPER CeedScalar RatelMatTraceSymmetric(const CeedScalar A_sym[6]) { return A_sym[0] + A_sym[1] + A_sym[2]; }

/**
  @brief Compute `C = alpha A + beta B` for 3x3 matrices

  @param[in]   alpha  First scaling factor
  @param[in]   A      First input matrix
  @param[in]   beta   Second scaling factor
  @param[in]   B      Second input matrix
  @param[out]  C      Output matrix

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelMatMatAdd(CeedScalar alpha, const CeedScalar A[3][3], CeedScalar beta, const CeedScalar B[3][3], CeedScalar C[3][3]) {
  for (CeedInt j = 0; j < 3; j++) {
    for (CeedInt k = 0; k < 3; k++) {
      C[j][k] = alpha * A[j][k] + beta * B[j][k];
    }
  }
  return CEED_ERROR_SUCCESS;
}
// Scalar multiplication alpha/beta is not considered. You need to add 9 (per scalar) FLOPs when alpha/beta != 1.0
#define FLOPS_MatMatAdd 9

/**
  @brief Copy B = A for 3x3 matrices

  @param[in]   A      Input matrix
  @param[out]  B      Output matrix

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelMatCopy(const CeedScalar A[3][3], CeedScalar B[3][3]) {
  for (CeedInt j = 0; j < 3; j++) {
    for (CeedInt k = 0; k < 3; k++) {
      B[j][k] = A[j][k];
    }
  }
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_MatCopy 0

/**
  @brief Compute `C = alpha A + beta B` for 3x3 matrices, in symmetric representation

  @param[in]   alpha  First scaling factor
  @param[in]   A_sym  First input matrix, in symmetric representation
  @param[in]   beta   Second scaling factor
  @param[in]   B_sym  Second input matrix, in symmetric representation
  @param[out]  C_sym  Output matrix, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelMatMatAddSymmetric(CeedScalar alpha, const CeedScalar A_sym[6], CeedScalar beta, const CeedScalar B_sym[6],
                                                  CeedScalar C_sym[6]) {
  for (CeedInt j = 0; j < 6; j++) {
    C_sym[j] = alpha * A_sym[j] + beta * B_sym[j];
  }
  return CEED_ERROR_SUCCESS;
}
// Scalar multiplication alpha/beta is not considered. You need to add 6 (per scalar) FLOPs when alpha/beta != 1.0
#define FLOPS_MatMatAddSymmetric 6

/**
  @brief Compute `D = alpha A + beta B + gamma C` for 3x3 matrices, in symmetric representation

  @param[in]   alpha  First scaling factor
  @param[in]   A_sym  First input matrix, in symmetric representation
  @param[in]   beta   Second scaling factor
  @param[in]   B_sym  Second input matrix, in symmetric representation
  @param[in]   gamma  Third scaling factor
  @param[in]   C_sym  Third input matrix, in symmetric representation
  @param[out]  D_sym  Output matrix, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelMatMatMatAddSymmetric(CeedScalar alpha, const CeedScalar A_sym[6], CeedScalar beta, const CeedScalar B_sym[6],
                                                     CeedScalar gamma, const CeedScalar C_sym[6], CeedScalar D_sym[6]) {
  for (CeedInt j = 0; j < 6; j++) {
    D_sym[j] = alpha * A_sym[j] + beta * B_sym[j] + gamma * C_sym[j];
  }
  return CEED_ERROR_SUCCESS;
}
// Scalar multiplication alpha/beta/gamma is not considered. You need to add 6 (per scalar) FLOPs when alpha/beta/gamma != 1.0
#define FLOPS_MatMatMatAddSymmetric 12

/**
  @brief Compute `D = alpha A + beta B + gamma C` for 3x3 matrices

  @param[in]   alpha  First scaling factor
  @param[in]   A      First input matrix
  @param[in]   beta   Second scaling factor
  @param[in]   B      Second input matrix
  @param[in]   gamma  Third scaling factor
  @param[in]   C      Third input matrix
  @param[out]  D      Output matrix

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelMatMatMatAdd(CeedScalar alpha, const CeedScalar A[3][3], CeedScalar beta, const CeedScalar B[3][3], CeedScalar gamma,
                                            const CeedScalar C[3][3], CeedScalar D[3][3]) {
  for (CeedInt j = 0; j < 3; j++) {
    for (CeedInt k = 0; k < 3; k++) {
      D[j][k] = alpha * A[j][k] + beta * B[j][k] + gamma * C[j][k];
    }
  }
  return CEED_ERROR_SUCCESS;
}
// Scalar multiplication alpha/beta/gamma is not considered. You need to add 9 (per scalar) FLOPs when alpha/beta/gamma != 1.0
#define FLOPS_MatMatMatAdd (9 * 2)

/**
  @brief Compute deviatoric part of symmetric matrix; `A_sym_dev = A_sym - (1/3) trace(A) I` for 3x3 matrix, in symmetric representation

  @param[in]   trace_A    Trace of input matrix
  @param[in]   A_sym      Input matrix, in symmetric representation
  @param[out]  A_sym_dev  Deviatoric part of input matrix, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelMatDeviatoricSymmetric(CeedScalar trace_A, const CeedScalar A_sym[6], CeedScalar A_sym_dev[6]) {
  for (CeedInt j = 0; j < 6; j++) {
    A_sym_dev[j] = A_sym[j] - (j < 3) * (trace_A / 3.);
  }
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_MatDeviatoricSymmetric 6

/**
  @brief Compute `C = alpha A * B` for 3x3 matrices

  @param[in]   alpha  Scaling factor
  @param[in]   A      First input matrix
  @param[in]   B      Second input matrix
  @param[out]  C      Output matrix

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelMatMatMult(CeedScalar alpha, const CeedScalar A[3][3], const CeedScalar B[3][3], CeedScalar C[3][3]) {
  for (CeedInt j = 0; j < 3; j++) {
    for (CeedInt k = 0; k < 3; k++) {
      C[j][k] = 0;
      for (CeedInt m = 0; m < 3; m++) {
        C[j][k] += alpha * A[j][m] * B[m][k];
      }
    }
  }
  return CEED_ERROR_SUCCESS;
}
// Scalar multiplication alpha is not considered. You need to add 9 FLOPs when alpha != 1.0
#define FLOPS_MatMatMult 54

/**
  @brief Compute `C = alpha A^T * B` for 3x3 matrices

  @param[in]   alpha  Scaling factor
  @param[in]   A      First input matrix
  @param[in]   B      Second input matrix
  @param[out]  C      Output matrix

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelMatTransposeMatMult(CeedScalar alpha, const CeedScalar A[3][3], const CeedScalar B[3][3], CeedScalar C[3][3]) {
  for (CeedInt j = 0; j < 3; j++) {
    for (CeedInt k = 0; k < 3; k++) {
      C[j][k] = 0;
      for (CeedInt m = 0; m < 3; m++) {
        C[j][k] += alpha * A[m][j] * B[m][k];
      }
    }
  }
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute `C = alpha A * B^T` for 3x3 matrices

  @param[in]   alpha  Scaling factor
  @param[in]   A      First input matrix
  @param[in]   B      Second input matrix
  @param[out]  C      Output matrix

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelMatMatTransposeMult(CeedScalar alpha, const CeedScalar A[3][3], const CeedScalar B[3][3], CeedScalar C[3][3]) {
  for (CeedInt j = 0; j < 3; j++) {
    for (CeedInt k = 0; k < 3; k++) {
      C[j][k] = 0;
      for (CeedInt m = 0; m < 3; m++) {
        C[j][k] += alpha * A[j][m] * B[k][m];
      }
    }
  }
  return CEED_ERROR_SUCCESS;
}
// Scalar multiplication alpha is not considered. You need to add 9 FLOPs when alpha != 1.0
#define FLOPS_MatMatTransposeMult 54

/**
  @brief Compute `C = alpha A^T * B^T` for 3x3 matrices

  @param[in]   alpha  Scaling factor
  @param[in]   A      First input matrix
  @param[in]   B      Second input matrix
  @param[out]  C      Output matrix

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelMatTransposeMatTransposeMult(CeedScalar alpha, const CeedScalar A[3][3], const CeedScalar B[3][3],
                                                            CeedScalar C[3][3]) {
  for (CeedInt j = 0; j < 3; j++) {
    for (CeedInt k = 0; k < 3; k++) {
      C[j][k] = 0;
      for (CeedInt m = 0; m < 3; m++) {
        C[j][k] += alpha * A[m][j] * B[k][m];
      }
    }
  }
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute `E = A * B + C * D` for 3x3 matrices

  @param[in]   A  First input matrix
  @param[in]   B  Second input matrix
  @param[in]   C  Third input matrix
  @param[in]   D  Fourth input matrix
  @param[out]  E  Output matrix

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelMatMatMultPlusMatMatMult(const CeedScalar A[3][3], const CeedScalar B[3][3], const CeedScalar C[3][3],
                                                        const CeedScalar D[3][3], CeedScalar E[3][3]) {
  for (CeedInt j = 0; j < 3; j++) {
    for (CeedInt k = 0; k < 3; k++) {
      E[j][k] = 0;
      for (CeedInt m = 0; m < 3; m++) {
        E[j][k] += A[j][m] * B[m][k] + C[j][m] * D[m][k];
      }
    }
  }
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_MatMatMultPlusMatMatMult 108

/**
  @brief Compute `c = alpha A : B` for 3x3 matrices

  @param[in]   alpha  Scaling factor
  @param[in]   A      First input matrix
  @param[in]   B      Second input matrix

  @return A scalar value: `alpha A : B`
**/
CEED_QFUNCTION_HELPER CeedScalar RatelMatMatContract(CeedScalar alpha, const CeedScalar A[3][3], const CeedScalar B[3][3]) {
  CeedScalar c = 0;

  for (CeedInt j = 0; j < 3; j++) {
    for (CeedInt k = 0; k < 3; k++) {
      c += A[j][k] * B[j][k];
    }
  }
  return alpha * c;
}

/**
  @brief Compute `c = alpha A_sym : B_sym` for 3x3 matrices, in symmetric representation

  @param[in]   alpha  Scaling factor
  @param[in]   A_sym  First input matrix, in symmetric representation
  @param[in]   B_sym  Second input matrix, in symmetric representation

  @return A scalar value: `alpha A_sym : B_sym`
**/
CEED_QFUNCTION_HELPER CeedScalar RatelMatMatContractSymmetric(CeedScalar alpha, const CeedScalar A_sym[6], const CeedScalar B_sym[6]) {
  CeedScalar c = 0;

  for (CeedInt j = 0; j < 6; j++) {
    c += A_sym[j] * B_sym[j] * (1 + (j >= 3));
  }
  return alpha * c;
}
// Scalar multiplication alpha is not considered. You need to add 1 FLOPs when alpha != 1.0
#define FLOPS_MatMatContractSymmetric 15

/**
  @brief Compute `det(A)` for a 3x3 matrix

  @param[in]  A  Input matrix

  @return A scalar value: `det(A)`
**/
CEED_QFUNCTION_HELPER CeedScalar RatelMatDetA(const CeedScalar A[3][3]) {
  const CeedScalar B11 = A[1][1] * A[2][2] - A[1][2] * A[2][1];
  const CeedScalar B12 = A[0][2] * A[2][1] - A[0][1] * A[2][2];
  const CeedScalar B13 = A[0][1] * A[1][2] - A[0][2] * A[1][1];

  return A[0][0] * B11 + A[1][0] * B12 + A[2][0] * B13;
}
#define FLOPS_MatDetA 14

/**
  @brief Compute `det(A) - 1` for a 3x3 matrix

  @param[in]  A  Input matrix

  @return A scalar value: `det(A) - 1`
**/
CEED_QFUNCTION_HELPER CeedScalar RatelMatDetAM1(const CeedScalar A[3][3]) {
  return A[0][0] * (A[1][1] * A[2][2] - A[1][2] * A[2][1]) +         /* *NOPAD* */
         A[0][1] * (A[1][2] * A[2][0] - A[1][0] * A[2][2]) +         /* *NOPAD* */
         A[0][2] * (A[1][0] * A[2][1] - A[2][0] * A[1][1]) +         /* *NOPAD* */
         A[0][0] + A[1][1] + A[2][2] +                               /* *NOPAD* */
         A[0][0] * A[1][1] + A[0][0] * A[2][2] + A[1][1] * A[2][2] - /* *NOPAD* */
         A[0][1] * A[1][0] - A[0][2] * A[2][0] - A[1][2] * A[2][1];  /* *NOPAD* */
}
#define FLOPS_MatDetAM1 29

/**
  @brief Compute `det(A_sym) - 1` for a 3x3 matrix, in symmetric representation

  @param[in]  A_sym  Input matrix, in symmetric representation

  @return A scalar value: `det(A_sym) - 1`
**/
CEED_QFUNCTION_HELPER CeedScalar RatelMatDetAM1Symmetric(const CeedScalar A_sym[6]) {
  return A_sym[0] * (A_sym[1] * A_sym[2] - A_sym[3] * A_sym[3]) +          /* *NOPAD* */
         A_sym[5] * (A_sym[3] * A_sym[4] - A_sym[5] * A_sym[2]) +          /* *NOPAD* */
         A_sym[4] * (A_sym[5] * A_sym[3] - A_sym[4] * A_sym[1]) +          /* *NOPAD* */
         A_sym[0] + A_sym[1] + A_sym[2] +                                  /* *NOPAD* */
         A_sym[0] * A_sym[1] + A_sym[0] * A_sym[2] + A_sym[1] * A_sym[2] - /* *NOPAD* */
         A_sym[5] * A_sym[5] - A_sym[4] * A_sym[4] - A_sym[3] * A_sym[3];  /* *NOPAD* */
}

/**
  @brief Compute `C_sym = alpha A_sym * B_sym` for 3x3 matrices, in symmetric representation

  Note `C_sym` will not be symmetric in general.
  Only use this function when `A_sym` and `B_sym` have same eigenvectors.

  @param[in]   alpha  Scaling factor
  @param[in]   A_sym  First input matrix, in symmetric representation
  @param[in]   B_sym  Second input matrix, in symmetric representation
  @param[out]  C_sym  Output matrix, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelMatMatMultSymmetric(CeedScalar alpha, const CeedScalar A_sym[6], const CeedScalar B_sym[6], CeedScalar C_sym[6]) {
  CeedScalar    A[3][3], B[3][3];
  const CeedInt ind_j[6] = {0, 1, 2, 1, 0, 0}, ind_k[6] = {0, 1, 2, 2, 2, 1};

  RatelSymmetricMatUnpack(A_sym, A);
  RatelSymmetricMatUnpack(B_sym, B);
  for (CeedInt m = 0; m < 6; m++) {
    C_sym[m] = 0.;
    for (CeedInt n = 0; n < 3; n++) {
      C_sym[m] += A[ind_j[m]][n] * B[n][ind_k[m]];
    }
    C_sym[m] *= alpha;
  }
  return CEED_ERROR_SUCCESS;
}
// Scalar multiplication alpha is not considered. You need to add 6 FLOPs when alpha != 1.0
#define FLOPS_MatMatMultSymmetric 36

/**
  @brief Compute the Frobenius norm `||A|| = sqrt(sum_ij |a_ij|^2)` for a symmetric 3x3 matrix

  @param[in]  A  Input matrix

  @return A scalar value: `||A||`
**/
CEED_QFUNCTION_HELPER CeedScalar RatelMatNorm(const CeedScalar A[3][3]) {
  CeedScalar sum_abs = 0.;

  for (CeedInt i = 0; i < 3; i++) {
    for (CeedInt j = 0; j < 3; j++) {
      sum_abs += A[i][j] * A[i][j];
    }
  }
  return sqrt(sum_abs);
}
#define FLOPS_MatNorm 19

/**
  @brief Compute `C_sym = A_sym * B_sym * A_sym` for 3x3 matrices, in symmetric representation

  @param[in]   alpha  Scaling factor
  @param[in]   A_sym  First input matrix, in symmetric representation
  @param[in]   B_sym  Second input matrix, in symmetric representation
  @param[out]  C_sym  Output matrix, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelMatMatMatMultSymmetric(CeedScalar alpha, const CeedScalar A_sym[6], const CeedScalar B_sym[6], CeedScalar C_sym[6]) {
  CeedScalar    A[3][3], B[3][3], BA[3][3];
  const CeedInt ind_j[6] = {0, 1, 2, 1, 0, 0}, ind_k[6] = {0, 1, 2, 2, 2, 1};

  RatelSymmetricMatUnpack(A_sym, A);
  RatelSymmetricMatUnpack(B_sym, B);

  // B*A; which is not symmetric in general
  RatelMatMatMult(1.0, B, A, BA);

  // C = A*B*A which is symmetric
  for (CeedInt m = 0; m < 6; m++) {
    C_sym[m] = 0.0;
    for (CeedInt n = 0; n < 3; n++) {
      C_sym[m] += A[ind_j[m]][n] * BA[n][ind_k[m]];
    }
    C_sym[m] *= alpha;
  }
  return CEED_ERROR_SUCCESS;
}
// Scalar multiplication alpha is not considered. You need to add 6 FLOPs when alpha != 1.0
#define FLOPS_MatMatMatMultSymmetric (FLOPS_MatMatMult + 18)

/**
  @brief Compute `A^-1` for a 3x3 matrix

  @param[in]   A      Input matrix
  @param[in]   det_A  Determinant of A
  @param[out]  A_inv  Output matrix inverse

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelMatInverse(const CeedScalar A[3][3], CeedScalar det_A, CeedScalar A_inv[3][3]) {
  CeedScalar B[3][3] = {
      {A[1][1] * A[2][2] - A[1][2] * A[2][1], A[0][2] * A[2][1] - A[0][1] * A[2][2], A[0][1] * A[1][2] - A[0][2] * A[1][1]},
      {A[1][2] * A[2][0] - A[1][0] * A[2][2], A[0][0] * A[2][2] - A[0][2] * A[2][0], A[0][2] * A[1][0] - A[0][0] * A[1][2]},
      {A[1][0] * A[2][1] - A[1][1] * A[2][0], A[0][1] * A[2][0] - A[0][0] * A[2][1], A[0][0] * A[1][1] - A[0][1] * A[1][0]},
  };

  for (CeedInt i = 0; i < 3; i++) {
    for (CeedInt j = 0; j < 3; j++) {
      A_inv[i][j] = B[i][j] / (det_A);
    }
  }
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_MatInverse 36

/**
  @brief Compute `A^-1` for a symmetric 3x3 matrix

  @param[in]   A      Input matrix
  @param[in]   det_A  Determinant of A
  @param[out]  A_inv  Output matrix inverse, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelMatInverseSymmetric(const CeedScalar A[3][3], CeedScalar det_A, CeedScalar A_inv[6]) {
  CeedScalar B[6] = {
      A[1][1] * A[2][2] - A[1][2] * A[2][1], /* *NOPAD* */
      A[0][0] * A[2][2] - A[0][2] * A[2][0], /* *NOPAD* */
      A[0][0] * A[1][1] - A[0][1] * A[1][0], /* *NOPAD* */
      A[0][2] * A[1][0] - A[0][0] * A[1][2], /* *NOPAD* */
      A[0][1] * A[1][2] - A[0][2] * A[1][1], /* *NOPAD* */
      A[0][2] * A[2][1] - A[0][1] * A[2][2]  /* *NOPAD* */
  };

  for (CeedInt m = 0; m < 6; m++) {
    A_inv[m] = B[m] / (det_A);
  }
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_MatInverseSymmetric 24

/**
  @brief Apply mass matrix for all components at a quadrature point

  @param[in]   Q         Number of quadrature points
  @param[in]   i         Current quadrature point
  @param[in]   num_comp  Number of components in u
  @param[in]   alpha     Scaling factor
  @param[in]   u         Input array
  @param[out]  v         Output array

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelScaledMassApplyAtQuadraturePoint(CeedInt Q, CeedInt i, CeedInt num_comp, CeedScalar alpha, const CeedScalar *u,
                                                                CeedScalar *v) {
  for (CeedInt c = 0; c < num_comp; c++) v[c * Q + i] = alpha * u[c * Q + i];
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute inverse of right Cauchy-Green tensor

  @param[in]   E_sym      Strain tensor, in symmetric representation
  @param[in]   Jm1        Determinant of deformation gradient(F) - 1; J = det(F)
  @param[out]  C_inv_sym  Inverse of right Cauchy-Green tensor, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelCInverse(const CeedScalar E_sym[6], CeedScalar Jm1, CeedScalar C_inv_sym[6]) {
  CeedScalar E[3][3];

  RatelSymmetricMatUnpack(E_sym, E);
  // C : right Cauchy-Green tensor
  // C = I + 2E
  const CeedScalar C[3][3] = {
      {2 * E[0][0] + 1, 2 * E[0][1],     2 * E[0][2]    },
      {2 * E[0][1],     2 * E[1][1] + 1, 2 * E[1][2]    },
      {2 * E[0][2],     2 * E[1][2],     2 * E[2][2] + 1}
  };

  // Compute C^(-1) : C-Inverse
  const CeedScalar J    = Jm1 + 1.;
  const CeedScalar detC = J * J;

  return RatelMatInverseSymmetric(C, detC, C_inv_sym);
}
#define FLOPS_CInverse (14 + FLOPS_MatInverseSymmetric)

/**
  @brief Compute `dC_inv_sym = -2 C_inv_sym * dE_sym * C_inv_sym`

  @param[in]   C_inv_sym   Inverse of right Cauchy-Green tensor, in symmetric representation
  @param[in]   dE_sym      Incremental strain energy tensor, in symmetric representation
  @param[out]  dC_inv_sym  Inverse of incremental right Cauchy-Green tensor, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelCInverse_fwd(const CeedScalar C_inv_sym[6], const CeedScalar dE_sym[6], CeedScalar dC_inv_sym[6]) {
  return RatelMatMatMatMultSymmetric(-2.0, C_inv_sym, dE_sym, dC_inv_sym);
}
#define FLOPS_CInverse_fwd (FLOPS_MatMatMatMultSymmetric + 6)

/**
  @brief Compute linear strain `e = (grad_u + grad_u^T) / 2`

  @param[in]   grad_u  Gradient of u
  @param[out]  e_sym   Linear strain, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelLinearStrain(const CeedScalar grad_u[3][3], CeedScalar e_sym[6]) {
  e_sym[0] = grad_u[0][0];
  e_sym[1] = grad_u[1][1];
  e_sym[2] = grad_u[2][2];
  e_sym[3] = (grad_u[1][2] + grad_u[2][1]) / 2.;
  e_sym[4] = (grad_u[0][2] + grad_u[2][0]) / 2.;
  e_sym[5] = (grad_u[0][1] + grad_u[1][0]) / 2.;
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_LinearStrain 6

/**
  @brief Compute incremental linear strain `de = (grad_du + grad_du^T) / 2`

  @param[in]   grad_du  Gradient of incremental change to u
  @param[out]  de_sym   Incremental linear strain, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelLinearStrain_fwd(const CeedScalar grad_du[3][3], CeedScalar de_sym[6]) {
  de_sym[0] = grad_du[0][0];
  de_sym[1] = grad_du[1][1];
  de_sym[2] = grad_du[2][2];
  de_sym[3] = (grad_du[1][2] + grad_du[2][1]) / 2.;
  de_sym[4] = (grad_du[0][2] + grad_du[2][0]) / 2.;
  de_sym[5] = (grad_du[0][1] + grad_du[1][0]) / 2.;
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_LinearStrain_fwd 6

/**
  @brief Compute Green Euler strain `e = (b - I) / 2 = (grad_u + grad_u^T + grad_u * grad_u^T) / 2` for current configuration

  @param[in]   grad_u  Gradient of u
  @param[out]  e_sym   Green Euler strain, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelGreenEulerStrain(const CeedScalar grad_u[3][3], CeedScalar e_sym[6]) {
  const CeedInt ind_j[6] = {0, 1, 2, 1, 0, 0}, ind_k[6] = {0, 1, 2, 2, 2, 1};

  RatelLinearStrain(grad_u, e_sym);
  // Add (grad_u * grad_u^T)/2 term to the linear part of e_sym
  for (CeedInt m = 0; m < 6; m++) {
    for (CeedInt n = 0; n < 3; n++) {
      e_sym[m] += (grad_u[ind_j[m]][n] * grad_u[ind_k[m]][n]) * 0.5;
    }
  }
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_GreenEulerStrain (FLOPS_LinearStrain + 54)

/**
  @brief Compute incremental Green Euler strain `de = db / 2 = (grad_du b + b grad_du^T) / 2` for current configuration.
         `grad_du = d(du)/dx` where `x` is physical coordinate in current configuration.

  @param[in]   grad_du  Gradient of incremental change to u
  @param[in]   b        Left Cauchy-Green deformation tensor
  @param[out]  de_sym   Incremental Green Euler strain, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelGreenEulerStrain_fwd(const CeedScalar grad_du[3][3], const CeedScalar b[3][3], CeedScalar de_sym[6]) {
  const CeedInt ind_j[6] = {0, 1, 2, 1, 0, 0}, ind_k[6] = {0, 1, 2, 2, 2, 1};

  for (CeedInt m = 0; m < 6; m++) {
    de_sym[m] = 0;
    for (CeedInt n = 0; n < 3; n++) {
      de_sym[m] += (grad_du[ind_j[m]][n] * b[n][ind_k[m]] + b[ind_j[m]][n] * grad_du[ind_k[m]][n]) / 2.;
    }
  }
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_GreenEulerStrain_fwd 90

/**
  @brief Compute Green Lagrange strain `E = (C - I)/2 = (grad_u + grad_u^T + grad_u^T * grad_u)/2` for initial configuration

  @param[in]   grad_u  Gradient of u
  @param[out]  E_sym   Green Lagrange strain, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelGreenLagrangeStrain(const CeedScalar grad_u[3][3], CeedScalar E_sym[6]) {
  const CeedInt ind_j[6] = {0, 1, 2, 1, 0, 0}, ind_k[6] = {0, 1, 2, 2, 2, 1};

  RatelLinearStrain(grad_u, E_sym);
  // Add (grad_u^T * grad_u)/2 term to the linear part of E_sym
  for (CeedInt m = 0; m < 6; m++) {
    for (CeedInt n = 0; n < 3; n++) {
      E_sym[m] += (grad_u[n][ind_j[m]] * grad_u[n][ind_k[m]]) * 0.5;
    }
  }
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_GreenLagrangeStrain (FLOPS_LinearStrain + 54)

/**
  @brief Compute incremental Green Lagrange strain `dE = (grad_du^T * F + F^T * grad_du) / 2` for initial configuration

  @param[in]   grad_du  Gradient of incremental change to u
  @param[in]   F        Deformation gradient
  @param[out]  dE_sym   Incremental Green Lagrange strain, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelGreenLagrangeStrain_fwd(const CeedScalar grad_du[3][3], const CeedScalar F[3][3], CeedScalar dE_sym[6]) {
  const CeedInt ind_j[6] = {0, 1, 2, 1, 0, 0}, ind_k[6] = {0, 1, 2, 2, 2, 1};

  for (CeedInt m = 0; m < 6; m++) {
    dE_sym[m] = 0;
    for (CeedInt n = 0; n < 3; n++) {
      dE_sym[m] += (grad_du[n][ind_j[m]] * F[n][ind_k[m]] + F[n][ind_j[m]] * grad_du[n][ind_k[m]]) / 2.;
    }
  }
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_GreenLagrangeStrain_fwd 90

/**
  @brief Compute linear stress `sigma = lambda*trace(e)I + 2*mu*e`

  @param[in]   two_mu     Shear modulus multiplied by 2
  @param[in]   lambda     Lame parameter
  @param[in]   trace_e    Trace of linear strain
  @param[in]   e_sym      Linear strain, in symmetric representation
  @param[out]  sigma_sym  Linear stress, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelLinearStress(CeedScalar two_mu, CeedScalar lambda, CeedScalar trace_e, const CeedScalar e_sym[6],
                                            CeedScalar sigma_sym[6]) {
  sigma_sym[0] = lambda * trace_e + two_mu * e_sym[0];
  sigma_sym[1] = lambda * trace_e + two_mu * e_sym[1];
  sigma_sym[2] = lambda * trace_e + two_mu * e_sym[2];
  sigma_sym[3] = two_mu * e_sym[3];
  sigma_sym[4] = two_mu * e_sym[4];
  sigma_sym[5] = two_mu * e_sym[5];
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_LinearStress 12

/**
  @brief Compute linear stress sigma eigenvalues

  @param[in]   two_mu             Shear modulus multiplied by 2
  @param[in]   lambda             Lame parameter
  @param[in]   trace_e            Trace of linear strain
  @param[in]   e_eigenvalues      Linear strain eigenvalues
  @param[out]  sigma_eigenvalues  Linear stress eigenvalues

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelLinearStressEigenvalues(const CeedScalar two_mu, const CeedScalar lambda, const CeedScalar trace_e,
                                                       const CeedScalar e_eigenvalues[3], CeedScalar sigma_eigenvalues[3]) {
  for (CeedInt j = 0; j < 3; j++) {
    sigma_eigenvalues[j] = lambda * trace_e + two_mu * e_eigenvalues[j];
  }
  return CEED_ERROR_SUCCESS;
}

/**
  @brief Compute incremental linear stress `dsigma = lambda*trace(de)I + 2*mu*de`

  @param[in]   two_mu      Shear modulus multiplied by 2
  @param[in]   lambda      Lame parameter
  @param[in]   trace_de    Trace of incremental linear strain
  @param[in]   de_sym      Linear strain incremental, in symmetric representation
  @param[out]  dsigma_sym  Linear stress incremental, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelLinearStress_fwd(CeedScalar two_mu, CeedScalar lambda, CeedScalar trace_de, const CeedScalar de_sym[6],
                                                CeedScalar dsigma_sym[6]) {
  dsigma_sym[0] = lambda * trace_de + two_mu * de_sym[0];
  dsigma_sym[1] = lambda * trace_de + two_mu * de_sym[1];
  dsigma_sym[2] = lambda * trace_de + two_mu * de_sym[2];
  dsigma_sym[3] = two_mu * de_sym[3];
  dsigma_sym[4] = two_mu * de_sym[4];
  dsigma_sym[5] = two_mu * de_sym[5];
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_LinearStress_fwd 12

/**
  @brief Compute mixed linear stress `sigma = (bulk_primal * tr(e) - p) * I + 2 * mu * e_dev`

  @param[in]   two_mu       Shear modulus multiplied by 2
  @param[in]   bulk_primal  Primal bulk modulus
  @param[in]   trace_e      Trace of linear strain
  @param[in]   p            Pressure
  @param[in]   e_dev_sym    Deviatoric linear strain, in symmetric representation
  @param[out]  sigma_sym    Mixed linear stress, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelMixedLinearStress(CeedScalar two_mu, CeedScalar bulk_primal, CeedScalar trace_e, CeedScalar p,
                                                 const CeedScalar e_dev_sym[6], CeedScalar sigma_sym[6]) {
  sigma_sym[0] = bulk_primal * trace_e - p + two_mu * e_dev_sym[0];
  sigma_sym[1] = bulk_primal * trace_e - p + two_mu * e_dev_sym[1];
  sigma_sym[2] = bulk_primal * trace_e - p + two_mu * e_dev_sym[2];
  sigma_sym[3] = two_mu * e_dev_sym[3];
  sigma_sym[4] = two_mu * e_dev_sym[4];
  sigma_sym[5] = two_mu * e_dev_sym[5];
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_MixedLinearStress 15

/**
  @brief Compute incremental mixed linear stress `dsigma = (bulk_primal * tr(de) - dp) * I + 2 * mu * de_dev`

  @param[in]   two_mu        Shear modulus multiplied by 2
  @param[in]   bulk_primal   Primal bulk modulus
  @param[in]   trace_de      Trace of incremental linear strain
  @param[in]   dp            Pressure incremental
  @param[in]   de_dev_sym    Deviatoric linear strain incremental, in symmetric representation
  @param[out]  dsigma_sym    Mixed linear stress incremental, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelMixedLinearStress_fwd(CeedScalar two_mu, CeedScalar bulk_primal, CeedScalar trace_de, CeedScalar dp,
                                                     const CeedScalar de_dev_sym[6], CeedScalar dsigma_sym[6]) {
  dsigma_sym[0] = bulk_primal * trace_de - dp + two_mu * de_dev_sym[0];
  dsigma_sym[1] = bulk_primal * trace_de - dp + two_mu * de_dev_sym[1];
  dsigma_sym[2] = bulk_primal * trace_de - dp + two_mu * de_dev_sym[2];
  dsigma_sym[3] = two_mu * de_dev_sym[3];
  dsigma_sym[4] = two_mu * de_dev_sym[4];
  dsigma_sym[5] = two_mu * de_dev_sym[5];
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_MixedLinearStress_fwd 15

/**
  @brief Compute poroelastic linear stress `sigma = ((lambda_d * trace_e * I) - (B * p * I) + (2_mu_d * e_sym)`

  @param[in]   two_mu_d     Shear modulus (drained) multiplied by 2
  @param[in]   lambda_d     First Lame parameter
  @param[in]   B            Biot's coefficient
  @param[in]   trace_e      Trace of linear strain
  @param[in]   p            Pressure
  @param[in]   e_sym        Poroelastic linear strain, in symmetric representation
  @param[out]  sigma_sym    Poroelastic linear stress, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelPoroElasticityLinearStress(CeedScalar two_mu_d, CeedScalar lambda_d, CeedScalar B, CeedScalar trace_e, CeedScalar p,
                                                          const CeedScalar e_sym[6], CeedScalar sigma_sym[6]) {
  sigma_sym[0] = lambda_d * trace_e - B * p + two_mu_d * e_sym[0];
  sigma_sym[1] = lambda_d * trace_e - B * p + two_mu_d * e_sym[1];
  sigma_sym[2] = lambda_d * trace_e - B * p + two_mu_d * e_sym[2];
  sigma_sym[3] = two_mu_d * e_sym[3];
  sigma_sym[4] = two_mu_d * e_sym[4];
  sigma_sym[5] = two_mu_d * e_sym[5];
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_PoroElasticityLinearStress 18

/**
  @brief Compute incremental poroelastic linear stress `dsigma = (lambda_d trace_de I) - (B dp I) + (2_mu_d de_sym)`

  @param[in]   two_mu_d      Shear modulus (drained) multiplied by 2
  @param[in]   lambda_d      First Lame parameter
  @param[in]   B             Biot's coefficient
  @param[in]   trace_de      Trace of incremental linear strain
  @param[in]   dp            Pressure incremental
  @param[in]   de_sym        linear strain incremental, in symmetric representation //Deviatoric ?
  @param[out]  dsigma_sym    Mixed linear stress incremental, in symmetric representation

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelPoroElasticityLinearStress_fwd(CeedScalar two_mu_d, CeedScalar lambda_d, CeedScalar B, CeedScalar trace_de,
                                                              CeedScalar dp, const CeedScalar de_sym[6], CeedScalar dsigma_sym[6]) {
  dsigma_sym[0] = lambda_d * trace_de - B * dp + two_mu_d * de_sym[0];
  dsigma_sym[1] = lambda_d * trace_de - B * dp + two_mu_d * de_sym[1];
  dsigma_sym[2] = lambda_d * trace_de - B * dp + two_mu_d * de_sym[2];
  dsigma_sym[3] = two_mu_d * de_sym[3];
  dsigma_sym[4] = two_mu_d * de_sym[4];
  dsigma_sym[5] = two_mu_d * de_sym[5];
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_PoroElasticityLinearStress_fwd 18

/**
  @brief Compute right handed orthonormal set `{w, u, v}` for length 3 unit vectors.

  Ref https://www.geometrictools.com/GTE/Mathematics/SymmetricEigensolver3x3.h

  @param[in]   w  Input vector, unit length
  @param[out]  u  First output vector
  @param[out]  v  Second output vector

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelOrthogonalComplement(const CeedScalar w[3], CeedScalar u[3], CeedScalar v[3]) {
  CeedScalar inv_length;

  // The vector w is guaranteed to be unit-length, so there is no need to worry about division by zero
  if (fabs(w[0]) > fabs(w[1])) {
    inv_length = 1. / sqrt(w[0] * w[0] + w[2] * w[2]);
    u[0]       = -w[2] * inv_length;
    u[1]       = 0.0;
    u[2]       = w[0] * inv_length;
  } else {
    inv_length = 1. / sqrt(w[1] * w[1] + w[2] * w[2]);
    u[0]       = 0.0;
    u[1]       = w[2] * inv_length;
    u[2]       = -w[1] * inv_length;
  }

  RatelVecVecCross(w, u, v);
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_OrthogonalComplement (7 + FLOPS_VecVecCross)

/**
  @brief Compute unit length eigenvector for the first eigenvalue of 3x3 symmetric matrix `A_sym`.

  Ref https://www.geometrictools.com/GTE/Mathematics/SymmetricEigensolver3x3.h

  @param[in]   A_sym    Symmetric matrix
  @param[in]   e_val_0  First eigenvalue
  @param[out]  e_vec_0  First eigenvector

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelComputeEigenvector0(const CeedScalar A_sym[6], CeedScalar e_val_0, CeedScalar e_vec_0[3]) {
  CeedInt    i_max = 0;
  CeedScalar d_max;
  // rows of A - e_val_0 * I
  CeedScalar row_0[3] = {A_sym[0] - e_val_0, A_sym[5], A_sym[4]};
  CeedScalar row_1[3] = {A_sym[5], A_sym[1] - e_val_0, A_sym[3]};
  CeedScalar row_2[3] = {A_sym[4], A_sym[3], A_sym[2] - e_val_0};
  CeedScalar r0_x_r1[3], r0_x_r2[3], r1_x_r2[3], d_0, d_1, d_2;

  RatelVecVecCross(row_0, row_1, r0_x_r1);
  RatelVecVecCross(row_0, row_2, r0_x_r2);
  RatelVecVecCross(row_1, row_2, r1_x_r2);

  d_0   = RatelDot3(r0_x_r1, r0_x_r1);
  d_1   = RatelDot3(r0_x_r2, r0_x_r2);
  d_2   = RatelDot3(r1_x_r2, r1_x_r2);
  d_max = d_0;

  if (d_1 > d_max) {
    d_max = d_1;
    i_max = 1;
  }
  if (d_2 > d_max) {
    i_max = 2;
  }

  if (i_max == 0) RatelScalarVecMult(1.0 / sqrt(d_0), r0_x_r1, e_vec_0);
  else if (i_max == 1) RatelScalarVecMult(1.0 / sqrt(d_1), r0_x_r2, e_vec_0);
  else RatelScalarVecMult(1.0 / sqrt(d_2), r1_x_r2, e_vec_0);
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_ComputeEigenvector0 (4 + 3 * FLOPS_VecVecCross + 3 * FLOPS_Dot3 + FLOPS_ScalarVecMult)

/**
  @brief Compute unit length eigenvector for the second eigenvalue of 3x3 symmetric matrix `A_sym`.

  Ref https://www.geometrictools.com/GTE/Mathematics/SymmetricEigensolver3x3.h

  @param[in]   A_sym    Symmetric matrix
  @param[in]   e_vec_0  First eigenvector
  @param[in]   e_val_1  Second eigenvalue
  @param[out]  e_vec_1  Second eigenvector

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelComputeEigenvector1(const CeedScalar A_sym[6], const CeedScalar e_vec_0[3], CeedScalar e_val_1,
                                                   CeedScalar e_vec_1[3]) {
  CeedScalar u[3], v[3], A[3][3], Au[3], Av[3];
  CeedScalar d_00, d_11, m_00, m_01, m_11, max_abs_comp, mu[3], mv[3];

  RatelOrthogonalComplement(e_vec_0, u, v);
  RatelSymmetricMatUnpack(A_sym, A);
  RatelMatVecMult(1.0, A, u, Au);
  RatelMatVecMult(1.0, A, v, Av);

  d_00 = RatelDot3(u, Au);
  m_01 = RatelDot3(u, Av);
  d_11 = RatelDot3(v, Av);
  m_00 = d_00 - e_val_1;
  m_11 = d_11 - e_val_1;

  const CeedScalar abs_m_00 = fabs(m_00), abs_m_01 = fabs(m_01), abs_m_11 = fabs(m_11);

  if (abs_m_00 >= abs_m_11) {
    max_abs_comp = RatelMax(abs_m_00, abs_m_01);
    if (max_abs_comp > 0.) {
      if (abs_m_00 >= abs_m_01) {
        m_01 /= m_00;
        m_00 = 1. / sqrt(1 + m_01 * m_01);
        m_01 *= m_00;
      } else {
        m_00 /= m_01;
        m_01 = 1. / sqrt(1 + m_00 * m_00);
        m_00 *= m_01;
      }

      RatelScalarVecMult(m_01, u, mu);
      RatelScalarVecMult(m_00, v, mv);
      RatelVecVecSubtract(mu, mv, e_vec_1);
    } else {
      e_vec_1[0] = u[0];
      e_vec_1[1] = u[1];
      e_vec_1[2] = u[2];
    }
  } else {
    max_abs_comp = RatelMax(abs_m_11, abs_m_01);
    if (max_abs_comp > 0.) {
      if (abs_m_11 >= abs_m_01) {
        m_01 /= m_11;
        m_11 = 1. / sqrt(1 + m_01 * m_01);
        m_01 *= m_11;
      } else {
        m_11 /= m_01;
        m_01 = 1. / sqrt(1 + m_11 * m_11);
        m_11 *= m_01;
      }

      RatelScalarVecMult(m_11, u, mu);
      RatelScalarVecMult(m_01, v, mv);
      RatelVecVecSubtract(mu, mv, e_vec_1);
    } else {
      e_vec_1[0] = u[0];
      e_vec_1[1] = u[1];
      e_vec_1[2] = u[2];
    }
  }
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_ComputeEigenvector1 \
  (FLOPS_OrthogonalComplement + 2 * FLOPS_MatVecMult + 3 * FLOPS_Dot3 + 2 * FLOPS_ScalarVecMult + FLOPS_VecVecSubtract + 8)

/**
  @brief Compute eigenvalues/vectors of 3x3 symmetric matrix `A_sym`.

  Ref https://onlinelibrary.wiley.com/doi/10.1002/nme.7153?af=R

  The rows of `e_vecs` are the eigenvectors for each eigenvalue.

  @param[in]   A_sym   Symmetric matrix
  @param[out]  e_vals  Array of eigenvalues
  @param[out]  e_vecs  Array of eigenvectors

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelMatComputeEigensystemSymmetric(const CeedScalar A_sym[6], CeedScalar e_vals[3], CeedScalar e_vecs[3][3]) {
  CeedScalar I1, J2, s, b_00, b_11, b_22, off_diag;

  I1       = RatelMatTraceSymmetric(A_sym);
  b_00     = (A_sym[0] - A_sym[1]) * (A_sym[0] - A_sym[1]);
  b_11     = (A_sym[1] - A_sym[2]) * (A_sym[1] - A_sym[2]);
  b_22     = (A_sym[2] - A_sym[0]) * (A_sym[2] - A_sym[0]);
  J2       = (b_00 + b_11 + b_22) / 6. + (A_sym[3] * A_sym[3] + A_sym[4] * A_sym[4] + A_sym[5] * A_sym[5]);
  s        = sqrt(J2 / 3.);
  off_diag = A_sym[5] * A_sym[5] + A_sym[4] * A_sym[4] + A_sym[3] * A_sym[3];

  // zero matrix
  if (s < RATEL_EPSILON_DOUBLE && I1 == 0.) {
    e_vals[0] = 0.;
    e_vals[1] = 0.;
    e_vals[2] = 0.;
    if (e_vecs) {
      e_vecs[0][0] = 1., e_vecs[0][1] = 0., e_vecs[0][2] = 0.;
      e_vecs[1][0] = 0., e_vecs[1][1] = 1., e_vecs[1][2] = 0.;
      e_vecs[2][0] = 0., e_vecs[2][1] = 0., e_vecs[2][2] = 1.;
    }
    return CEED_ERROR_SUCCESS;
  }

  // non-zero matrix
  if (off_diag > 0.) {
    CeedScalar B[3][3], C[3][3], S_sym[6], S2_sym[6], T_sym[6], B_sym[6], C_sym[6];
    CeedScalar d, sign_j;

    // S = A - I1/3 * I
    RatelMatDeviatoricSymmetric(I1, A_sym, S_sym);
    RatelMatMatMultSymmetric(1.0, S_sym, S_sym, S2_sym);
    // T = S*S - (2*J2/3) * I
    RatelMatDeviatoricSymmetric(2 * J2, S2_sym, T_sym);
    // B = T - s*S
    RatelMatMatAddSymmetric(1.0, T_sym, -s, S_sym, B_sym);
    // C = T + s*S
    RatelMatMatAddSymmetric(1.0, T_sym, s, S_sym, C_sym);
    RatelSymmetricMatUnpack(B_sym, B);
    RatelSymmetricMatUnpack(C_sym, C);
    const CeedScalar norm_B = RatelMatNorm(B);
    const CeedScalar norm_C = RatelMatNorm(C);

    d      = norm_B / norm_C;
    sign_j = RatelSign(1. - d);
    if (sign_j * (1. - d) < RATEL_EPSILON_DOUBLE) {
      e_vals[0] = -sqrt(J2) + I1 / 3.;
      e_vals[1] = 0. + I1 / 3.;
      e_vals[2] = sqrt(J2) + I1 / 3.;
    } else {
      CeedScalar theta, cd, sd;

      theta     = 2 * RatelAtanSeries(sign_j > 0 ? d : (1.0 / d)) / 3.;
      cd        = sign_j * s * cos(theta);
      sd        = sqrt(J2) * sin(theta);
      e_vals[0] = -cd - sd + I1 / 3.;
      e_vals[1] = -cd + sd + I1 / 3.;
      e_vals[2] = 2 * cd + I1 / 3.;
    }
    if (e_vecs) {
      RatelComputeEigenvector0(A_sym, e_vals[2], e_vecs[2]);
      RatelComputeEigenvector1(A_sym, e_vecs[2], e_vals[1], e_vecs[1]);
      RatelVecVecCross(e_vecs[1], e_vecs[2], e_vecs[0]);
    }
  } else {  // A_sym is diagonal
    e_vals[0] = A_sym[0];
    e_vals[1] = A_sym[1];
    e_vals[2] = A_sym[2];
    if (e_vecs) {
      e_vecs[0][0] = 1., e_vecs[0][1] = 0., e_vecs[0][2] = 0.;
      e_vecs[1][0] = 0., e_vecs[1][1] = 1., e_vecs[1][2] = 0.;
      e_vecs[2][0] = 0., e_vecs[2][1] = 0., e_vecs[2][2] = 1.;
    }
  }
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_MatComputeEigenValueSymmetric                                                                                                          \
  (FLOPS_MatTrace + 20 + 2 * FLOPS_MatDeviatoricSymmetric + FLOPS_MatMatMultSymmetric + 2 * FLOPS_MatMatAddSymmetric + 12 + 2 * FLOPS_MatNorm + 20 + \
   FLOPS_AtanSeries)
#define FLOPS_MatComputeEigenVectorSymmetric (FLOPS_ComputeEigenvector0 + FLOPS_ComputeEigenvector1 + FLOPS_VecVecCross)
#define FLOPS_MatComputeEigensystemSymmetric (FLOPS_MatComputeEigenValueSymmetric + FLOPS_MatComputeEigenVectorSymmetric)

/**
  @brief Compute eigenvalue's increment `de_vals[i] = (dA*e_vecs[i], e_vecs[i])` for Newton linearization

  @param[in]   dA_sym   Increment of symmetric matrix A
  @param[in]   e_vecs   Eigenvectors of A_sym
  @param[out]  de_vals  Increment eigenvalues

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelEigenValue_fwd(const CeedScalar dA_sym[6], const CeedScalar e_vecs[3][3], CeedScalar de_vals[3]) {
  CeedScalar dA[3][3];

  RatelSymmetricMatUnpack(dA_sym, dA);
  for (CeedInt i = 0; i < 3; i++) {
    CeedScalar dA_N[3];

    RatelMatVecMult(1.0, dA, e_vecs[i], dA_N);
    de_vals[i] = RatelDot3(dA_N, e_vecs[i]);
  }
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_EigenValue_fwd (3 * FLOPS_MatVecMult + 3 * FLOPS_Dot3)

/**
  @brief Compute eigenvector's increment ` de_vec[i] = sum_{j!=i} (dA*e_vecs[i], e_vecs[j]) * e_vecs[j] / (e_vals[i] - e_vals[j])` for Newton
         linearization

  @param[in]   dA_sym   Increment of symmetric matrix A
  @param[in]   e_vals   Eigenvalues of A_sym
  @param[in]   e_vecs   Eigenvectors of A_sym
  @param[in]   i        ith de_vec that we want to return
  @param[out]  de_vec   Increment of ith eigenvector

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelEigenVector_fwd(const CeedScalar dA_sym[6], const CeedScalar e_vals[3], const CeedScalar e_vecs[3][3], CeedInt i,
                                               CeedScalar de_vec[3]) {
  CeedScalar dA[3][3];

  RatelSymmetricMatUnpack(dA_sym, dA);
  de_vec[0] = 0., de_vec[1] = 0., de_vec[2] = 0.;
  for (CeedInt j = 0; j < 3; j++) {
    if (i != j) {
      CeedScalar dA_N[3], N_dA_N, e_vals_subtract;

      RatelMatVecMult(1.0, dA, e_vecs[i], dA_N);     // dA*e_vecs[i]
      N_dA_N          = RatelDot3(dA_N, e_vecs[j]);  // (dA*e_vecs[i], e_vecs[j])
      e_vals_subtract = e_vals[i] - e_vals[j];
      for (CeedInt k = 0; k < 3; k++) {
        de_vec[k] += N_dA_N * e_vecs[j][k] / e_vals_subtract;
      }
    }
  }
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_EigenVector_fwd (2 * FLOPS_MatVecMult + 2 * FLOPS_Dot3 + 20)

/**
  @brief Compute principal stretch `pr[i] = sqrt(1 + e_vals[i])` where e_vals are eigenvalue of Green-Lagrange strain
         tensor E (in initial configuration) or Green-Euler strain tensor e (in current configuration)

  @param[in]   e_vals   Array of eigenvalues of E or e
  @param[out]  pr_str   Array of principal stretch

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelPrincipalStretch(const CeedScalar e_vals[3], CeedScalar pr_str[3]) {
  for (CeedInt i = 0; i < 3; i++) {
    pr_str[i] = sqrt(1 + 2 * e_vals[i]);
  }
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_PrincipalStretch (9)

/**
  @brief Compute principal stretch increment `dpr_str[i] = (dA*e_vecs[i], e_vecs[i]) / pr[i]` for Newton linearization

  @param[in]   dA_sym   Increment of Green-Lagrange dE (in initial configuration) or Green-Euler de (in current configuration)
  @param[in]   e_vecs   Array of eigenvectors of A_sym
  @param[in]   pr_str   Array of principal stretch
  @param[out]  dpr_str  Array of increment principal stretch

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelPrincipalStretch_fwd(const CeedScalar dA_sym[6], const CeedScalar e_vecs[3][3], const CeedScalar pr_str[3],
                                                    CeedScalar dpr_str[3]) {
  CeedScalar de_vals[3];

  RatelEigenValue_fwd(dA_sym, e_vecs, de_vals);
  for (CeedInt i = 0; i < 3; i++) {
    dpr_str[i] = de_vals[i] / pr_str[i];
  }
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_PrincipalStretch_fwd (FLOPS_EigenValue_fwd + 3)

/**
  @brief Compute outer product of eigenvector `outer_mult[i][6] = (N[i] * N[i]^T)` for i=0:2

  @param[in]   e_vals      Input eigenvalues
  @param[in]   e_vecs      Input eigenvectors 3x3
  @param[out]  outer_mult  Matrix of size 3x6

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelEigenVectorOuterMult(const CeedScalar e_vals[3], const CeedScalar e_vecs[3][3], CeedScalar outer_mult[3][6]) {
  if ((e_vals[0] == e_vals[1]) && (e_vals[0] == e_vals[2]) && (e_vals[1] == e_vals[2])) {
    // Case 1: e_vals[0] = e_vals[1] = e_vals[2] ==> sum_{i=0:3} N[i] N[i]^T = I
    for (CeedInt i = 0; i < 3; i++) {
      for (CeedInt j = 0; j < 6; j++) {
        outer_mult[i][j] = 0.;
        if (i == j) outer_mult[i][j] = 1.;
      }
    }
  } else if ((e_vals[0] == e_vals[1]) && fabs(e_vals[1] - e_vals[2]) > 0.) {
    // Case 2: e_vals[0] = e_vals[1] != e_vals[2]
    RatelVecOuterMult(e_vecs[2], outer_mult[2]);  // N[2] N[2]^T
    // Set N[0] N[0]^T OR N[1] N[1]^T to 0., doesn't matter which one and compute the other one
    for (CeedInt j = 0; j < 6; j++) {
      outer_mult[1][j] = 0.;
      outer_mult[0][j] = (j < 3) - outer_mult[2][j];
    }
  } else if ((e_vals[1] == e_vals[2]) && fabs(e_vals[1] - e_vals[0]) > 0.) {
    // Case 3: e_vals[0] != e_vals[1] = e_vals[2]
    RatelVecOuterMult(e_vecs[0], outer_mult[0]);  // N[0] N[0]^T
    // Set N[2] N[2]^T OR N[1] N[1]^T to 0., doesn't matter which one and compute the other one
    for (CeedInt j = 0; j < 6; j++) {
      outer_mult[1][j] = 0.;
      outer_mult[2][j] = (j < 3) - outer_mult[0][j];
    }
  } else {
    // Case 4: e_vals[0] != e_vals[1] != e_vals[2]
    for (CeedInt i = 0; i < 3; i++) RatelVecOuterMult(e_vecs[i], outer_mult[i]);  // N[i] N[i]^T
  }
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_EigenVectorOuterMult (3 * FLOPS_VecOuterMult)

/**
  @brief Compute outer product increment of eigenvector `Outer_mult_fwd[i][6] = d(N[i] * N[i]^T)` for Newton linearization

  @param[in]   dA_sym         Increment of symmetric matrix A
  @param[in]   e_vecs         Eigenvectors of A
  @param[in]   e_vals         Eigenvalues of A
  @param[out]  outer_mult_fwd Matrix of size 3x6

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelEigenVectorOuterMult_fwd(const CeedScalar dA_sym[6], const CeedScalar e_vals[3], const CeedScalar e_vecs[3][3],
                                                        CeedScalar outer_mult_fwd[3][6]) {
  if ((e_vals[0] == e_vals[1]) && (e_vals[0] == e_vals[2]) && (e_vals[1] == e_vals[2])) {
    for (CeedInt i = 0; i < 3; i++) {
      for (CeedInt j = 0; j < 6; j++) {
        outer_mult_fwd[i][j] = 0.;
      }
    }
  } else if ((e_vals[0] == e_vals[1]) && fabs(e_vals[1] - e_vals[2]) > 0.) {
    // Case 2: e_vals[0] = e_vals[1] != e_vals[2]
    CeedScalar B[3][3], C[3][3], D[3][3], de_vec[3];

    RatelEigenVector_fwd(dA_sym, e_vals, e_vecs, 2, de_vec);  // dN[2]
    RatelVecVecOuterMult(de_vec, e_vecs[2], B);               // dN[2] * N[2]^T
    RatelVecVecOuterMult(e_vecs[2], de_vec, C);               // N[2] * dN[2]^T
    RatelMatMatAdd(1., B, 1., C, D);                          // dN[2] * N[2]^T + N[2] * dN[2]^T
    RatelSymmetricMatPack(D, outer_mult_fwd[2]);
    // Set d(N[0] N[0]^T) OR d(N[1] N[1]^T) to 0., doesn't matter which one and compute the other one
    for (CeedInt j = 0; j < 6; j++) {
      outer_mult_fwd[1][j] = 0.;
      outer_mult_fwd[0][j] = -outer_mult_fwd[2][j];
    }
  } else if ((e_vals[1] == e_vals[2]) && fabs(e_vals[1] - e_vals[0]) > 0.) {
    // Case 3: e_vals[0] != e_vals[1] = e_vals[2]
    CeedScalar B[3][3], C[3][3], D[3][3], de_vec[3];

    RatelEigenVector_fwd(dA_sym, e_vals, e_vecs, 0, de_vec);  // dN[0]
    RatelVecVecOuterMult(de_vec, e_vecs[0], B);               // dN[0] * N[0]^T
    RatelVecVecOuterMult(e_vecs[0], de_vec, C);               // N[0] * dN[0]^T
    RatelMatMatAdd(1., B, 1., C, D);                          // dN[0] * N[0]^T + N[0] * dN[0]^T
    RatelSymmetricMatPack(D, outer_mult_fwd[0]);
    // Set d(N[2] N[2]^T) OR d(N[1] N[1]^T) to 0., doesn't matter which one and compute the other one
    for (CeedInt j = 0; j < 6; j++) {
      outer_mult_fwd[1][j] = 0.;
      outer_mult_fwd[2][j] = -outer_mult_fwd[0][j];
    }
  } else {
    // Case 4: e_vals[0] != e_vals[1] != e_vals[2]
    for (CeedInt i = 0; i < 3; i++) {
      CeedScalar B[3][3], C[3][3], D[3][3], de_vec[3];

      RatelEigenVector_fwd(dA_sym, e_vals, e_vecs, i, de_vec);  // dN[i]
      RatelVecVecOuterMult(de_vec, e_vecs[i], B);               // dN[i] * N[i]^T
      RatelVecVecOuterMult(e_vecs[i], de_vec, C);               // N[i] * dN[i]^T
      RatelMatMatAdd(1., B, 1., C, D);                          // dN[i] * N[i]^T + N[i] * dN[i]^T
      RatelSymmetricMatPack(D, outer_mult_fwd[i]);
    }
  }
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_EigenVectorOuterMult_fwd (3 * FLOPS_EigenVector_fwd + 6 * FLOPS_VecVecOuterMult + 3 * FLOPS_MatMatAdd)

/**
  @brief Compute signum function of vector, defined by `sgn(x) = ||x|| > 0 ? x / ||x|| : 0`

  @param[in]   x     Input vector
  @param[out]  y     Output vector, `y = sgn(x)`
  @param[out]  norm  Norm of input vector

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelVecSignum(const CeedScalar x[3], CeedScalar y[3], CeedScalar *norm) {
  *norm                     = RatelNorm3(x);
  const CeedScalar inv_norm = *norm > CEED_EPSILON ? 1. / *norm : 0.;
  for (CeedInt i = 0; i < 3; i++) {
    y[i] = x[i] * inv_norm;
  }
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_VecSignum (3 + 1 + FLOPS_Norm3)

/**
  @brief Compute increment of signum of vector `d(sgn(x))/dx = ||x|| > 0 ? (I - (x/||x||)⊗(x/||x||)) / ||x|| * dx : 0`

  @param[in]   x     Input vector
  @param[in]   dx    Increment of input vector
  @param[out]  y     Output vector, d = sgn(x)
  @param[out]  dy    Increment of output vector, dy = d(sgn(x))/dx
  @param[out]  norm  Norm of input vector

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelVecSignum_fwd(const CeedScalar x[3], const CeedScalar dx[3], CeedScalar y[3], CeedScalar dy[3], CeedScalar *norm) {
  RatelVecSignum(x, y, norm);
  const CeedScalar inv_norm     = *norm > CEED_EPSILON ? 1. / *norm : 0.;
  const CeedScalar dx_dot_sgn_x = RatelDot3(dx, y);
  for (CeedInt i = 0; i < 3; i++) {
    // Compute d(sgn(x)) = (I - sgn(x) ⊗ sgn(x)) * dx / ||x||
    dy[i] = inv_norm * (dx[i] - dx_dot_sgn_x * y[i]);
  }
  return CEED_ERROR_SUCCESS;
}
#define FLOPS_VecSignum_fwd (3 * 3 + 1 + FLOPS_VecSignum + FLOPS_Dot3)

/**
  @brief Compute symmetric 3x3 matrix as A_sym = sum_{i=1}^{3} f(lambda_i) Q_i Q_i^T

  @param[in]   eigenvalues  A_sym eigenvalues
  @param[in]   n_outer3x6   eigenvectors outer product
  @param[out]  A_sym        A_sym

  @return An error code: 0 - success, otherwise - failure
**/
CEED_QFUNCTION_HELPER int RatelMatFromEigensystemSymmetric(const CeedScalar eigenvalues[3], const CeedScalar n_outer3x6[3][6], CeedScalar A_sym[6]) {
  CeedScalar aux[6];

  // sum_{i=1}^{3} lambda_i Q_i Q_i^T
  RatelMatMatAddSymmetric(eigenvalues[0], n_outer3x6[0], eigenvalues[1], n_outer3x6[1], aux);
  RatelMatMatAddSymmetric(eigenvalues[2], n_outer3x6[2], 1, aux, A_sym);

  return CEED_ERROR_SUCCESS;
}
#define FLOPS_MatFromEigensystemSymmetric (2 * FLOPS_MatMatAddSymmetric)

/// @}
