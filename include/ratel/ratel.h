#pragma once

#include <petscts.h>

#include "types.h"  // IWYU pragma: export
// IWYU pragma: private, include <ratel.h>

#if PETSC_VERSION_LT(3, 22, 0)
#error "PETSc latest main branch or PETSc v3.22 is required"
#endif

/// @defgroup RatelCore       Core Ratel library
/// @defgroup RatelSolvers    Solver methods
/// @defgroup RatelMaterials  Material models
/// @defgroup RatelBoundary   Boundary conditions
/// @defgroup RatelInternal   Internal functions

/*
  RATEL_EXTERN is used in this header to denote all publicly visible symbols.

  No other file should declare publicly visible symbols, thus it should never be used outside ratel.h.
 */
#if defined(__clang_analyzer__)
#define RATEL_EXTERN extern
#elif defined(__cplusplus)
#define RATEL_EXTERN extern "C"
#else
#define RATEL_EXTERN extern
#endif

// Ratel library version numbering
/// Ratel library major version number
/// @ingroup RatelCore
#define RATEL_VERSION_MAJOR 0
/// Ratel library minor version number
/// @ingroup RatelCore
#define RATEL_VERSION_MINOR 3
/// Ratel library patch version number
/// @ingroup RatelCore
#define RATEL_VERSION_PATCH 0
/// Flag indicating library is a release version
/// @ingroup RatelCore
#define RATEL_VERSION_RELEASE false

/// Compile-time check that the the current library version is at least as recent as the specified version.
/// This macro is typically used in
/// @code
/// #if RATEL_VERSION_GE(0, 1, 0)
///   code path that needs at least 0.1.0
/// #else
///   fallback code for older versions
/// #endif
/// @endcode
///
/// A non-release version always compares as positive infinity.
///
/// @param major   Major version
/// @param minor   Minor version
/// @param patch   Patch (subminor) version
///
/// @ingroup RatelCore
/// @sa RatelGetVersion()
#define RATEL_VERSION_GE(major, minor, patch) \
  (!RATEL_VERSION_RELEASE ||                  \
   (RATEL_VERSION_MAJOR > major ||            \
    (RATEL_VERSION_MAJOR == major && (RATEL_VERSION_MINOR > minor || (RATEL_VERSION_MINOR == minor && RATEL_VERSION_PATCH >= patch)))))

RATEL_EXTERN PetscErrorCode RatelGetVersion(int *major, int *minor, int *patch, PetscBool *release);

/// String names for enum pretty printing and command line options
RATEL_EXTERN const char *const RatelSolverTypes[];
RATEL_EXTERN const char *const RatelMethodTypes[];
RATEL_EXTERN const char *const RatelMethodTypesCL[];
RATEL_EXTERN const char *const RatelForcingTypes[];
RATEL_EXTERN const char *const RatelForcingTypesCL[];
RATEL_EXTERN const char *const RatelBoundaryTypes[];
RATEL_EXTERN const char *const RatelPlatenTypes[];
RATEL_EXTERN const char *const RatelPlatenTypesCL[];
RATEL_EXTERN const char *const RatelFrictionTypes[];
RATEL_EXTERN const char *const RatelFrictionTypesCL[];
RATEL_EXTERN const char *const RatelBCInterpolationTypes[];
RATEL_EXTERN const char *const RatelBCInterpolationTypesCL[];
RATEL_EXTERN const char *const RatelInitialConditionTypes[];
RATEL_EXTERN const char *const RatelPMutigridCoarseningTypes[];
RATEL_EXTERN const char *const RatelPMultigridCoarseningTypesCL[];
RATEL_EXTERN const char *const RatelPointLocationTypesCL[];
RATEL_EXTERN const char *const RatelDirectionTypesCL[];

/// Common diagnostic component names
RATEL_EXTERN const char *const RatelElasticityDualDiagnosticComponentNames[];

/// Context for internal unit scaling
/// @struct RatelUnits
/// @ingroup RatelCore
typedef struct RatelUnits_private *RatelUnits;

/// Ratel context for MPM, stores points `CeedElemRestriction` for a material region
/// @struct RatelMPMContext
/// @ingroup RatelMaterials
typedef struct RatelMPMContext_private *RatelMPMContext;

/// Library context created by RatelInit()
/// @struct Ratel
/// @ingroup RatelCore
typedef struct Ratel_private *Ratel;

/// Library context management
RATEL_EXTERN PetscErrorCode RatelInit(MPI_Comm comm, Ratel *ratel);
RATEL_EXTERN PetscErrorCode RatelView(Ratel ratel, PetscViewer viewer);
RATEL_EXTERN PetscErrorCode RatelDestroy(Ratel *ratel);

/// User DM setup
RATEL_EXTERN PetscErrorCode RatelDMCreate(Ratel ratel, RatelSolverType solver_type, DM *dm);
RATEL_EXTERN PetscErrorCode RatelGetSolutionMeshDM(Ratel ratel, DM *dm);

/// Solver management
RATEL_EXTERN PetscErrorCode RatelTSSetup(Ratel ratel, TS ts);
RATEL_EXTERN PetscErrorCode RatelTSSetFromOptions(Ratel ratel, TS ts);
RATEL_EXTERN PetscErrorCode RatelSNESSetup(Ratel ratel, SNES snes);
RATEL_EXTERN PetscErrorCode RatelTSSetupInitialCondition(Ratel ratel, TS ts, Vec U);
RATEL_EXTERN PetscErrorCode RatelSNESSetupInitialCondition(Ratel ratel, SNES snes, Vec U);
RATEL_EXTERN PetscErrorCode RatelTSCheckpointFinalSolutionFromOptions(Ratel ratel, TS ts, Vec U);
RATEL_EXTERN PetscErrorCode RatelSNESCheckpointFinalSolutionFromOptions(Ratel ratel, SNES snes, Vec U);
RATEL_EXTERN PetscErrorCode RatelSetNonSPD(Ratel ratel);

/// Diagnostic quantities
RATEL_EXTERN PetscErrorCode RatelHasMMS(Ratel ratel, PetscBool *has_mms);
RATEL_EXTERN PetscErrorCode RatelIsCeedBP(Ratel ratel, PetscBool *is_ceed_bp);
RATEL_EXTERN PetscErrorCode RatelComputeMMSL2Error(Ratel ratel, Vec U, PetscScalar time, PetscInt *num_fields, PetscScalar **l2_error);
RATEL_EXTERN PetscErrorCode RatelGetExpectedStrainEnergy(Ratel ratel, PetscBool *has_expected, PetscScalar *expected_strain_energy);
RATEL_EXTERN PetscErrorCode RatelGetExpectedMaxDisplacement(Ratel ratel, PetscInt *num_components, PetscBool has_expected[],
                                                            PetscScalar expected_max_displacement[]);
RATEL_EXTERN PetscErrorCode RatelGetExpectedFaceSurfaceForce(Ratel ratel, const char *face, PetscInt *num_components, PetscBool has_expected[],
                                                             PetscScalar expected_surface_force[]);
RATEL_EXTERN PetscErrorCode RatelGetExpectedFaceCentroid(Ratel ratel, const char *face, PetscInt *num_components, PetscBool has_expected[],
                                                         PetscScalar expected_centroid[]);
RATEL_EXTERN PetscErrorCode RatelGetExpectedMaxDiagnosticQuantities(Ratel ratel, PetscInt *num_expected, PetscScalar expected_max_values[]);
RATEL_EXTERN PetscErrorCode RatelComputeStrainEnergyError(Ratel ratel, PetscScalar strain_energy, PetscBool *has_expected_strain_energy,
                                                          PetscScalar *strain_energy_error);
RATEL_EXTERN PetscErrorCode RatelComputeMaxDisplacementError(Ratel ratel, PetscInt num_components, const PetscScalar max_displacement[],
                                                             PetscBool *has_any_expected, PetscScalar max_displacement_error[]);
RATEL_EXTERN PetscErrorCode RatelComputeFaceForceErrors(Ratel ratel, const char *face, PetscInt num_components, const PetscScalar centroid[],
                                                        const PetscScalar surface_force[], PetscBool *has_any_expected_centroid,
                                                        PetscBool *has_any_expected_surface_force, PetscScalar centroid_error[],
                                                        PetscScalar surface_force_error[]);
RATEL_EXTERN PetscErrorCode RatelComputeStrainEnergy(Ratel ratel, Vec U, PetscReal time, PetscScalar *strain_energy);
RATEL_EXTERN PetscErrorCode RatelComputeMaxVectorValues(Ratel ratel, DM dm, Vec U, PetscReal time, PetscInt *num_fields, char ***field_names,
                                                        PetscInt **num_components, PetscScalar max_values[]);
PETSC_EXTERN PetscErrorCode RatelComputeMaxSolutionValues(Ratel ratel, Vec U, PetscReal time, PetscInt *num_fields, char ***field_names,
                                                          PetscInt **num_components, PetscScalar max_values[]);
RATEL_EXTERN PetscErrorCode RatelGetSurfaceForceFaces(Ratel ratel, PetscInt *num_faces, const char **faces[]);
RATEL_EXTERN PetscErrorCode RatelComputeSurfaceForcesCellToFace(Ratel ratel, Vec U, PetscReal time, PetscInt *num_components,
                                                                PetscScalar **surface_forces);
RATEL_EXTERN PetscErrorCode RatelComputeSurfaceForces(Ratel ratel, Vec U, PetscReal time, PetscInt *num_components, PetscScalar **surface_forces);
RATEL_EXTERN PetscErrorCode RatelComputeSurfaceCentroids(Ratel ratel, Vec U, PetscReal time, PetscInt *num_components,
                                                         PetscScalar **surface_centroids);
RATEL_EXTERN PetscErrorCode RatelComputeMaxDiagnosticQuantities(Ratel ratel, Vec U, PetscScalar time, PetscInt *num_diagnostic_components,
                                                                const char *diagnostic_component_names[], PetscScalar max_diagnostic_values[]);
RATEL_EXTERN PetscErrorCode RatelComputeVolume(Ratel ratel, Vec U, PetscReal time, PetscScalar *volume);
RATEL_EXTERN PetscErrorCode RatelGetDiagnosticQuantities(Ratel ratel, Vec U, PetscReal time, Vec *D);
RATEL_EXTERN PetscErrorCode RatelRestoreDiagnosticQuantities(Ratel ratel, Vec *D);

RATEL_EXTERN PetscErrorCode RatelViewMMSL2ErrorFromOptions(Ratel ratel, PetscScalar time, Vec U);
RATEL_EXTERN PetscErrorCode RatelViewMaxSolutionValuesErrorFromOptions(Ratel ratel, PetscScalar time, Vec U);
RATEL_EXTERN PetscErrorCode RatelViewStrainEnergyErrorFromOptions(Ratel ratel, PetscScalar time, Vec U);
RATEL_EXTERN PetscErrorCode RatelViewSurfaceForceAndCentroidErrorFromOptions(Ratel ratel, PetscScalar time, Vec U);
RATEL_EXTERN PetscErrorCode RatelViewMaxDiagnosticQuantitiesErrorByNameFromOptions(Ratel ratel, PetscReal time, Vec U);
RATEL_EXTERN PetscErrorCode RatelViewDiagnosticQuantitiesFromOptions(Ratel ratel, PetscReal time, Vec U);
RATEL_EXTERN PetscErrorCode RatelViewVolumeErrorFromOptions(Ratel ratel, PetscReal time, Vec U);

// MPM Utilities
RATEL_EXTERN PetscErrorCode RatelMPMMeshToSwarm(Ratel ratel, Vec U, PetscReal time, PetscBool displace_coords);
RATEL_EXTERN PetscErrorCode RatelMPMSwarmToMesh(Ratel ratel, Vec U);
RATEL_EXTERN PetscErrorCode RatelDMSwarmViewFromOptions(Ratel ratel, DM dm_swarm, const char option[]);
