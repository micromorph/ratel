/// @file
/// Ratel linear poroelastic material parameters
#pragma once

#include <ceed/types.h>

#include "common-parameters.h"  // IWYU pragma: export

/// PoroElastic model data
/// @ingroup RatelMaterials
typedef struct {
  /// Common properties
  CeedScalar common_parameters[RATEL_NUMBER_COMMON_PARAMETERS];
  /// To change the sign in pp block pc
  CeedInt sign_pp;

  /// Lame parameter - user
  CeedScalar lambda_d;
  /// Shear modulus  - user
  CeedScalar mu_d;
  /// Shear modulus multiplied by 2
  CeedScalar two_mu_d;

  /// Bulk drained  - user
  CeedScalar bulk_d;
  /// Bulk modulus fluid  - user
  CeedScalar bulk_f;
  /// Bulk solid  - user
  CeedScalar bulk_s;

  /// Initial porosity  - user
  CeedScalar phi_0;
  /// Fluid viscosity - user
  CeedScalar eta_f;
  /// Intrinsic permeability - user
  CeedScalar varkappa_0;  // varkappa_0 = varkappa* eta_f

  /// Biot's coefficient
  CeedScalar B;
  /// Biot's modulus
  CeedScalar M;
} RatelLinearPoroElasticityParams;
