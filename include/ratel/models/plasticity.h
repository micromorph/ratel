/// @file
/// Ratel plastiticy material parameters
#pragma once

#include <ceed/types.h>

#include "common-parameters.h"  // IWYU pragma: export
#include "elasticity-linear.h"  // IWYU pragma: export

/// Plasticity model data
/// @ingroup RatelMaterials
typedef struct {
  /// Initial yield stress sigma_0
  CeedScalar yield_stress;
  /// Parameter for linear hardening component
  CeedScalar linear_hardening;
  /// Saturation stress for Voce hardening component
  CeedScalar saturation_stress;
  /// Exponential decay rate for Voce hardening component
  CeedScalar hardening_decay;
} RatelPlasticityParams;

/// Plasticity model data
/// @ingroup RatelMaterials
typedef struct {
  /// Elastic model parameters
  RatelLinearElasticityParams elasticity_params;
  /// Plastic model parameters
  RatelPlasticityParams plasticity_params;
} RatelElastoPlasticityParams;
