/// @file
/// Ratel linear elastic material parameters
#pragma once

#include <ceed/types.h>

#include "common-parameters.h"  // IWYU pragma: export

/// Elastic model data
/// @ingroup RatelMaterials
typedef struct {
  /// Common properties
  CeedScalar common_parameters[RATEL_NUMBER_COMMON_PARAMETERS];
  /// Poisson's ratio
  CeedScalar nu;
  /// Young's modulus
  CeedScalar E;
  /// First Lame parameter
  CeedScalar lambda;
  /// Second Lame parameter (shear modulus)
  CeedScalar mu;
  /// Second Lame parameter multiplied by 2
  CeedScalar two_mu;
  /// bulk modulus
  CeedScalar bulk;
} RatelLinearElasticityParams;

/// Point fields for linear elasticity
/// @struct RatelLinearElasticityPointFields
/// @ingroup RatelMaterials
typedef struct {
  /// Second Lame parameter (shear modulus)
  CeedScalar mu;
  /// bulk modulus
  CeedScalar bulk;
} RatelLinearElasticityPointFields;
