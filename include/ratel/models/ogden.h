/// @file
/// Ratel Ogden material parameters
#pragma once

#include <ceed/types.h>

#include "common-parameters.h"  // IWYU pragma: export

#define RATEL_NUMBER_OGDEN_PARAMETERS 10

/// Elastic model data
/// @ingroup RatelMaterials
typedef struct {
  /// Common properties
  CeedScalar common_parameters[RATEL_NUMBER_COMMON_PARAMETERS];
  /// Poisson's ratio
  CeedScalar nu;
  /// Ogden's first parameters
  CeedScalar m[RATEL_NUMBER_OGDEN_PARAMETERS];
  /// Ogden's second parameters
  CeedScalar alpha[RATEL_NUMBER_OGDEN_PARAMETERS];
  /// First Lame parameter
  CeedScalar lambda;
  /// Second Lame parameter (shear modulus)
  CeedScalar mu;
  /// Number of Ogden's parameters
  CeedInt num_ogden_parameters;
  /// bulk modulus
  CeedScalar bulk;
  /// Young's modulus
  CeedScalar E;
} RatelOgdenElasticityParams;
