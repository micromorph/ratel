/// @file
/// Ratel elasticity+damage parameters
#pragma once

#include <ceed/types.h>

#include "common-parameters.h"

/// Elastic+damage model data
/// @ingroup RatelMaterials
typedef struct {
  /// Common properties
  CeedScalar common_parameters[RATEL_NUMBER_COMMON_PARAMETERS];
  // (isotropic) Elasticity specific properties
  /// Poisson's ratio
  CeedScalar nu;
  /// Young's Modulus
  CeedScalar E;
  // Phase-field fracture specific properties
  /// Critical energy release rate
  CeedScalar fracture_toughness;
  /// Length-scale parameter
  CeedScalar characteristic_length;
  /// Residual stiffness parameter
  CeedScalar residual_stiffness;
  /// Scaling factor for damage residual
  CeedScalar damage_scaling;
  /// Damage viscosity
  CeedScalar damage_viscosity;
  /// Flag for switching on/off offdiagonal terms of the Jacobian
  bool use_offdiagonal;
  /// AT1 model type (true-AT1, false AT2, should be changed to char)
  bool use_AT1;
  /// Second Lame parameter (shear modulus)
  CeedScalar mu;
  /// bulk modulus
  CeedScalar bulk;
  /// time step
  CeedScalar dt;
} RatelElasticityDamageParams;

/// Point fields for elasticity+damage
/// @struct RatelElasticityDamagePointFields
/// @ingroup RatelMaterials
typedef struct {
  // Phase-field fracture specific properties
  /// Critical energy release rate
  CeedScalar fracture_toughness;
  /// Length-scale parameter
  CeedScalar characteristic_length;
  /// Residual stiffness parameter
  CeedScalar residual_stiffness;
  /// Scaling factor for damage residual
  CeedScalar damage_scaling;
  /// Damage viscosity
  CeedScalar damage_viscosity;
} RatelElasticityDamagePointFields;
