/// @file
/// Ratel friction parameters data structures
#pragma once

#include <ceed/types.h>

#include "../types.h"

/// @addtogroup RatelBoundary
/// @{

/// @brief Friction parameters for all friction models, some of which may be unused in a specific model
typedef struct {
  /// Friction model
  RatelFrictionType model;
  /// Static coefficient of friction
  CeedScalar static_coefficient;
  /// Kinetic coefficient of friction, assumed to equal the static coefficient for some models
  CeedScalar kinetic_coefficient;
  /// Viscous damping coefficient, Ns/m
  CeedScalar viscous_coefficient;
  /// Tolerance velocity, m/s
  CeedScalar tolerance_velocity;
} RatelFrictionParams;

/// @}
