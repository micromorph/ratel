/// @file
/// Ratel mixed linear elasticity MMS parameters
#pragma once

#include <ceed/types.h>

#include "mms-elasticity-linear.h"  // IWYU pragma: export

/// Mixed linear elasticity MMS parameters
/// @struct RatelMMSMixedLinearElasticityParams
/// @ingroup RatelMaterials
typedef RatelMMSLinearElasticityParams RatelMMSMixedLinearElasticityParams;
