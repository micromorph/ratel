/// @file
/// Ratel boundary condition data structures
#pragma once

#include <ceed/types.h>

#include "../types.h"      // IWYU pragma: export
#include "bounding-box.h"  // IWYU pragma: export

/// @addtogroup RatelBoundary
/// @{

/// Maximum interpolation nodes for boundary conditions
#define RATEL_MAX_BC_INTERP_POINTS 256

/// Clamp boundary condition context
typedef struct {
  /// Translation vectors
  CeedScalar translation[4 * RATEL_MAX_BC_INTERP_POINTS];
  /// Rotation axes
  CeedScalar rotation_axis[4 * RATEL_MAX_BC_INTERP_POINTS];
  /// Rotation polynomial coefficients
  CeedScalar rotation_polynomial[2 * RATEL_MAX_BC_INTERP_POINTS];
  /// Transition times
  CeedScalar times[RATEL_MAX_BC_INTERP_POINTS];
  /// Number of transition times
  CeedInt num_times;
  /// Type of interpolation between points
  RatelBCInterpolationType interpolation_type;
  /// Number of components in field
  CeedInt num_comp;
  /// Current solver time
  CeedScalar time;
  /// Current solver time step
  CeedScalar dt;
} RatelBCClampParams;

/// Slip boundary condition context
typedef struct {
  /// Translation vectors
  CeedScalar translation[4 * RATEL_MAX_BC_INTERP_POINTS];
  /// Transition times
  CeedScalar times[RATEL_MAX_BC_INTERP_POINTS];
  /// Number of transition times
  CeedInt num_times;
  /// Type of interpolation between points
  RatelBCInterpolationType interpolation_type;
  /// Components to constrain
  CeedInt components[4];
  /// Number of constrained components
  CeedInt num_comp_slip;
  /// Number of components in field
  CeedInt num_comp;
  /// Current solver time
  CeedScalar time;
  /// Current solver time step
  CeedScalar dt;
} RatelBCSlipParams;

/// Traction boundary condition context
typedef struct {
  /// Traction vector
  CeedScalar direction[3 * RATEL_MAX_BC_INTERP_POINTS];
  /// Transition times
  CeedScalar times[RATEL_MAX_BC_INTERP_POINTS];
  /// Number of transition times
  CeedInt num_times;
  /// Type of interpolation between points
  RatelBCInterpolationType interpolation_type;
  /// Current solver time
  CeedScalar time;
} RatelBCTractionParams;

/// Pressure boundary condition context
typedef struct {
  /// Pressure
  CeedScalar pressure[RATEL_MAX_BC_INTERP_POINTS];
  /// Transition times
  CeedScalar times[RATEL_MAX_BC_INTERP_POINTS];
  /// Number of transition times
  CeedInt num_times;
  /// Type of interpolation between points
  RatelBCInterpolationType interpolation_type;
  /// Current solver time
  CeedScalar time;
} RatelBCPressureParams;

/// @}
