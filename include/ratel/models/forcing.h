/// @file
/// Ratel forcing function data structures
#pragma once

#include <ceed/types.h>

#include "../types.h"  // IWYU pragma: export

/// @addtogroup RatelInternal
/// @{

/// Maximum interpolation points for user-specified forcing function
#define RATEL_MAX_FORCE_INTERP_POINTS 16

/// User-specified forcing context
typedef struct {
  /// Density
  CeedScalar rho;
  /// Acceleration vector
  CeedScalar acceleration[3 * RATEL_MAX_FORCE_INTERP_POINTS];
  /// Transition times
  CeedScalar times[RATEL_MAX_FORCE_INTERP_POINTS];
  /// Number of transition times
  CeedInt num_times;
  /// Type of interpolation between points
  RatelBCInterpolationType interpolation_type;
  /// Current solver time
  CeedScalar time;
} RatelForcingBodyParams;

/// @}
