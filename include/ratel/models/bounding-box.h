/// @file
/// Ratel bounding box data structures
#pragma once

#include <ceed/types.h>

/// @addtogroup RatelBoundary
/// @{

typedef struct {
  CeedScalar min[3];
  CeedScalar max[3];
} RatelBoundingBoxParams;

/// @} RatelBoundary
