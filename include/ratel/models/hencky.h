/// @file
/// Ratel Hencky material parameters
#pragma once

#include <ceed/types.h>

#include "common-parameters.h"  // IWYU pragma: export
#include "elasticity-linear.h"  // IWYU pragma: export

/// Hencky model data
/// @struct RatelHenckyElasticityParams
/// @ingroup RatelMaterials
typedef RatelLinearElasticityParams RatelHenckyElasticityParams;

/// Point fields for Hencky
/// @struct RatelHenckyElasticityPointFields
/// @ingroup RatelMaterials
typedef RatelLinearElasticityPointFields RatelHenckyElasticityPointFields;
