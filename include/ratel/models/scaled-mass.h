/// @file
/// Ratel scaled mass matrix parameters
#pragma once

#include <ceed/types.h>

#include "common-parameters.h"  // IWYU pragma: export

/// Scaled mass context
/// @ingroup RatelInternal
typedef struct {
  /// Scaling parameter
  CeedScalar rho;
  /// Number of fields
  CeedInt num_fields;
  /// Number of components in each field
  CeedInt field_sizes[RATEL_MAX_FIELDS];
} RatelScaledMassParams;
