/// @file
/// Ratel Neo-Hookean poroelasticity material parameters
#pragma once

#include <ceed/types.h>

#include "common-parameters.h"      // IWYU pragma: export
#include "poroelasticity-linear.h"  // IWYU pragma: export

/// Neo-Hookean poroelasticity model data
/// @struct RatelNeoHookeanPoroElasticityParams
/// @ingroup RatelMaterials
typedef RatelLinearPoroElasticityParams RatelNeoHookeanPoroElasticityParams;
