/// @file
/// Ratel Mooney-Rivlin material parameters
#pragma once

#include <ceed/types.h>

#include "common-parameters.h"  // IWYU pragma: export

/// Mooney-Rivlin model data
/// @ingroup RatelMaterials
typedef struct {
  /// Common properties
  CeedScalar common_parameters[RATEL_NUMBER_COMMON_PARAMETERS];
  /// First Mooney-Rivlin property
  CeedScalar mu_1;
  /// Second Mooney-Rivlin property
  CeedScalar mu_2;
  /// Poisson's ratio
  CeedScalar nu;
  /// First Lame parameter
  CeedScalar lambda;
  /// Second Lame parameter (shear modulus)
  CeedScalar mu;
  /// Second Mooney-Rivlin property multiplied by 2
  CeedScalar two_mu_2;
  /// bulk modulus
  CeedScalar bulk;
} RatelMooneyRivlinElasticityParams;
