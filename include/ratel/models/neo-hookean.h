/// @file
/// Ratel Neo-Hookean material parameters
#pragma once

#include <ceed/types.h>

#include "common-parameters.h"  // IWYU pragma: export
#include "elasticity-linear.h"  // IWYU pragma: export

/// Neo-Hookean model data
/// @struct RatelNeoHookeanElasticityParams
/// @ingroup RatelMaterials
typedef RatelLinearElasticityParams RatelNeoHookeanElasticityParams;

/// Point fields for Neo-Hookean
/// @struct RatelNeoHookeanElasticityPointFields
/// @ingroup RatelMaterials
typedef RatelLinearElasticityPointFields RatelNeoHookeanElasticityPointFields;
