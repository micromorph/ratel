/// @file
/// Ratel CEED BPs MMS parameters
#pragma once

#include <ceed/types.h>

/// CEED BPs MMS parameters
/// @ingroup RatelMaterials
typedef struct {
  /// First Weierstrass parameter
  CeedScalar weierstrass_a;
  /// Second Weierstrass parameter
  CeedScalar weierstrass_b;
  /// Number of terms in Weierstrass series
  CeedInt weierstrass_n;
  /// Current solver time
  CeedScalar time;
} RatelMMSCEEDBPsParams;
