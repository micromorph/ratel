/// @file
/// Ratel platen boundary parameters
#pragma once

#include <ceed/types.h>

#include "../qfunctions/boundaries/flexible-loading.h"
#include "boundary.h"           // IWYU pragma: export
#include "common-parameters.h"  // IWYU pragma: export
#include "friction.h"           // IWYU pragma: export

/// Common Platen BC data
/// @ingroup RatelBoundary
typedef struct {
  /// Exterior normal for platen
  CeedScalar normal[3];
  /// Center of the platen
  CeedScalar center[3];
  /// Displacement of the platen along normal vector
  CeedScalar distance[RATEL_MAX_BC_INTERP_POINTS];
  /// Transition times
  CeedScalar times[RATEL_MAX_BC_INTERP_POINTS];
  /// Number of transition times
  CeedInt num_times;
  /// Type of interpolation between points
  RatelBCInterpolationType interpolation_type;
  /// Nitsche's method parameter
  CeedScalar gamma;
  /// Friction parameters
  RatelFrictionParams friction;
  /// DM face id
  CeedInt face_id;
  /// DM face orientation
  CeedInt face_domain_value;
  /// Index for platen name lookup
  CeedInt name_index;
} RatelBCPlatenParamsCommon;

/// Platen BC data, including material data
/// @ingroup RatelBoundary
typedef struct {
  /// Common platen data
  RatelBCPlatenParamsCommon platen;
  /// Current solver time
  CeedScalar time;
  /// Material data
  CeedScalar material[RATEL_MAX_MATERIAL_SIZE];
} RatelBCPlatenParams;
