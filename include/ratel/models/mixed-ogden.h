/// @file
/// Ratel mixed linear elastic material parameters
#pragma once

#include <ceed/types.h>

#include "common-parameters.h"  // IWYU pragma: export

#define RATEL_NUMBER_OGDEN_PARAMETERS 10

/// Elastic model data
/// @ingroup RatelMaterials
typedef struct {
  /// Common properties
  CeedScalar common_parameters[RATEL_NUMBER_COMMON_PARAMETERS];
  /// Poisson's ratio
  CeedScalar nu;
  /// Ogden's first parameters
  CeedScalar m[RATEL_NUMBER_OGDEN_PARAMETERS];
  /// Ogden's second parameters
  CeedScalar alpha[RATEL_NUMBER_OGDEN_PARAMETERS];
  /// First Lame parameter
  CeedScalar lambda;
  /// Second Lame parameter (shear modulus)
  CeedScalar mu;
  /// Number of Ogden's parameters
  CeedInt num_ogden_parameters;
  /// bulk modulus
  CeedScalar bulk;
  /// Young's modulus
  CeedScalar E;
  /// To change full strain to deviatoric in mixed formulation
  CeedScalar nu_primal;
  /// To change full strain to deviatoric in uu block pc
  CeedScalar nu_primal_pc;
  /// To change the sign in pp block pc
  CeedInt sign_pp;
  /// Primal bulk modulus
  CeedScalar bulk_primal;
  /// Primal bulk modulus for block pc
  CeedScalar bulk_primal_pc;
} RatelMixedOgdenElasticityParams;
