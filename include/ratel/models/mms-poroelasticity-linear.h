/// @file
/// Ratel linear poroelasticity MMS parameters
#pragma once

#include <ceed/types.h>

#include "common-parameters.h"  // IWYU pragma: export

/// Linear elasticity MMS parameters
/// @ingroup RatelMaterials
typedef struct {
  /// Solution scaling factor
  CeedScalar A0;
  /// Sin shift factor
  CeedScalar shift;
  /// Sin scaling factor
  CeedScalar scale;
  /// Current solver time
  CeedScalar time;
  /// Material data
  CeedScalar material[RATEL_MAX_MATERIAL_SIZE];
} RatelMMSPoroElasticityLinearParams;
