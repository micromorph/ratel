/// @file
/// Ratel common material parameters
#pragma once

/// Number of common model parameters
/// @ingroup RatelMaterials
#define RATEL_NUMBER_COMMON_PARAMETERS 3
/// Index of common parameter `rho`
/// @ingroup RatelMaterials
#define RATEL_COMMON_PARAMETER_RHO 0
/// Index of common parameter shift_v
/// @ingroup RatelMaterials
#define RATEL_COMMON_PARAMETER_SHIFT_V 1
/// Index of common parameter shift_a
/// @ingroup RatelMaterials
#define RATEL_COMMON_PARAMETER_SHIFT_A 2

/// Maximum supported number of fields
/// @ingroup RatelMaterials
#define RATEL_MAX_FIELDS 16

/// Maximum material context size
/// @ingroup RatelMaterials
#define RATEL_MAX_MATERIAL_SIZE 128
