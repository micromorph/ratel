/// @file
/// Ratel mixed Neo-Hookean material parameters
#pragma once

#include <ceed/types.h>

#include "common-parameters.h"        // IWYU pragma: export
#include "elasticity-mixed-linear.h"  // IWYU pragma: export

/// Neo-Hookean model data
/// @struct RatelNeoHookeanElasticityParams
/// @ingroup RatelMaterials
typedef RatelMixedLinearElasticityParams RatelMixedNeoHookeanElasticityParams;
