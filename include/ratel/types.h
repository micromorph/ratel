#pragma once

/// Specify solver type
/// @ingroup RatelCore
typedef enum {
  /// Static solver, to be solved with a PETSc SNES
  RATEL_SOLVER_STATIC      = 0,
  /// Quasistatic solver, to be solved with a PETSc TS
  RATEL_SOLVER_QUASISTATIC = 1,
  /// Fully dynamic solver, to be solved with a PETSc TS
  RATEL_SOLVER_DYNAMIC     = 2,
} RatelSolverType;

/// Specify numerical method
/// @ingroup RatelCore
typedef enum {
  /// FEM method
  RATEL_METHOD_FEM = 0,
  /// MPM method
  RATEL_METHOD_MPM = 1,
} RatelMethodType;

/// Specify forcing term
/// @ingroup RatelCore
typedef enum {
  /// No forcing term
  RATEL_FORCING_NONE = 0,
  /// User-specified body force with interpolation
  RATEL_FORCING_BODY = 1,
  /// Forcing for linear elasticity manufactured solution
  RATEL_FORCING_MMS  = 2,
} RatelForcingType;

/// Specify boundary condition
/// @ingroup RatelBoundary
typedef enum {
  /// Dirichlet clamped boundary
  RATEL_BOUNDARY_CLAMP    = 0,
  /// Dirichlet MMS boundary
  RATEL_BOUNDARY_MMS      = 1,
  /// Neumann traction boundary,
  RATEL_BOUNDARY_TRACTION = 2,
  /// Platen boundary integral
  RATEL_BOUNDARY_PLATEN   = 3,
} RatelBoundaryType;

/// Specify friction model
/// @ingroup RatelBoundary
typedef enum {
  /// No friction
  RATEL_FRICTION_NONE      = 0,
  /// Coulomb friction model
  RATEL_FRICTION_COULOMB   = 1,
  /// Threlfall friction model
  RATEL_FRICTION_THRELFALL = 2,
} RatelFrictionType;

/// Specify interpolation type for flexible boundary condition
/// @ingroup RatelBoundary
typedef enum {
  /// No interpolation, piecewise constant
  RATEL_BC_INTERP_NONE   = 0,
  /// Linear interpolation
  RATEL_BC_INTERP_LINEAR = 1,
} RatelBCInterpolationType;

/// Specify initial condition
/// @ingroup RatelCore
typedef enum {
  /// Initial condition zero vector
  RATEL_INITIAL_CONDITION_ZERO     = 0,
  /// Initial condition given by binary file
  RATEL_INITIAL_CONDITION_CONTINUE = 1,
} RatelInitialConditionType;

/// Specify p-multigrid coarsening strategy
/// @ingroup RatelCore
typedef enum {
  /// P-multigrid, coarsen logarithmically, decreasing basis order to next lowest power of 2
  RATEL_P_MULTIGRID_COARSENING_LOGARITHMIC = 0,
  /// P-multigrid, coarsen uniformly, decreasing basis order by 1
  RATEL_P_MULTIGRID_COARSENING_UNIFORM     = 1,
  /// P-multigrid, user defined coarsening strategy
  RATEL_P_MULTIGRID_COARSENING_USER        = 2,
} RatelPMultigridCoarseningType;

/// Specify platen type
/// @ingroup RatelBoundary
typedef enum {
  RATEL_PLATEN_NITSCHE = 0,
  RATEL_PLATEN_PENALTY = 1,
} RatelPlatenType;

/// Specify initial point locations for MPM
/// @ingroup RatelCore
typedef enum {
  /// Initial point locations at Gauss quadrature points in each cell
  RATEL_POINT_LOCATION_GAUSS       = 0,
  /// Initial point locations uniformly spaced in each cell
  RATEL_POINT_LOCATION_UNIFORM     = 1,
  /// Initial point locations randomly distributed in each cell
  RATEL_POINT_LOCATION_CELL_RANDOM = 2,
} RatelPointLocationType;

/// Specify cartesian coordinate direction
/// @ingroup RatelCore
typedef enum {
  /// X coordinate direction
  RATEL_DIRECTION_X = 0,
  /// Y coordinate direction
  RATEL_DIRECTION_Y = 1,
  /// Z coordinate direction
  RATEL_DIRECTION_Z = 2,
} RatelDirectionType;

/// Diagnostic Sub-DM indices
/// @ingroup RatelCore
typedef enum {
  /// Sub-DM index for projected diagnostic quantities
  RATEL_DIAGNOSTIC_PROJECTED = 0,
  /// Sub-DM index for dual (nodal) diagnostic quantities
  RATEL_DIAGNOSTIC_DUAL      = 1,
} RatelDiagnosticType;
