#pragma once

#include <ceed.h>
#include <petscdm.h>
#include <ratel-impl.h>
#include <ratel.h>
#include <ratel/models/forcing.h>

// Material
RATEL_INTERN PetscErrorCode RatelMaterialCreate(Ratel ratel, const char *material_name, RatelMaterial *material);
RATEL_INTERN PetscErrorCode RatelMaterialView(RatelMaterial material, PetscViewer viewer);
RATEL_INTERN PetscErrorCode RatelModelDataView(Ratel ratel, RatelModelData model_data, CeedQFunctionContext context, PetscViewer viewer);
RATEL_INTERN PetscErrorCode RatelModelParameterDataView(Ratel ratel, RatelModelParameterData param_data, CeedQFunctionContext context,
                                                        PetscViewer viewer);
RATEL_INTERN PetscErrorCode RatelMaterialDestroy(RatelMaterial *material);
RATEL_INTERN PetscErrorCode RatelMaterialGetActiveFieldSizes(RatelMaterial material, CeedInt *num_active_fields, const CeedInt **active_field_sizes);
RATEL_INTERN PetscErrorCode RatelMaterialGetPointFields(RatelMaterial material, CeedInt *num_point_fields, const CeedInt **point_field_sizes,
                                                        const char ***point_field_names);
RATEL_INTERN PetscErrorCode RatelMaterialGetNumDiagnosticComponents(RatelMaterial material, CeedInt *num_components_projected,
                                                                    CeedInt *num_components_dual);
RATEL_INTERN PetscErrorCode RatelMaterialGetNumStateComponents(RatelMaterial material, CeedInt *num_comp_state);
RATEL_INTERN PetscErrorCode RatelMaterialGetActiveFieldNames(RatelMaterial material, const char ***active_field_names,
                                                             const char ***active_component_names);
RATEL_INTERN PetscErrorCode RatelMaterialGetDiagnosticComponentNames(RatelMaterial material, const char ***projected_component_names,
                                                                     const char ***dual_component_names);
RATEL_INTERN PetscErrorCode RatelMaterialGetVolumeLabelName(RatelMaterial material, const char **label_name);
RATEL_INTERN PetscErrorCode RatelMaterialGetVolumeLabelValues(RatelMaterial material, PetscInt *num_label_values, PetscInt **label_values);
RATEL_INTERN PetscErrorCode RatelMaterialSetVolumeLabelValue(RatelMaterial material, PetscInt label_value);
RATEL_INTERN PetscErrorCode RatelMaterialGetForcingType(RatelMaterial material, RatelForcingType *forcing_type);
RATEL_INTERN PetscErrorCode RatelMaterialGetMaterialName(RatelMaterial material, const char **material_name);
RATEL_INTERN PetscErrorCode RatelMaterialGetModelName(RatelMaterial material, const char **model_name);
RATEL_INTERN PetscErrorCode RatelMaterialGetSurfaceGradientLabelName(RatelMaterial material, PetscInt dm_face, const char **label_name);
RATEL_INTERN PetscErrorCode RatelMaterialGetSurfaceGradientDiagnosticLabelName(RatelMaterial material, PetscInt dm_face, const char **label_name);
RATEL_INTERN PetscErrorCode RatelMaterialGetSurfaceGradientOperatorFaceLabelAndValue(RatelMaterial material, CeedOperator op,
                                                                                     DMLabel *face_domain_label, PetscInt *face_domain_value);
RATEL_INTERN PetscErrorCode RatelMaterialGetInitialRandomScaling(RatelMaterial material, PetscScalar *initial_random_scaling);
RATEL_INTERN PetscErrorCode RatelMaterialHasMMS(RatelMaterial material, PetscBool *has_mms);

// Utilities
RATEL_INTERN PetscErrorCode RatelMaterialGetCLPrefix(RatelMaterial material, char **cl_prefix);
RATEL_INTERN PetscErrorCode RatelMaterialGetCLMessage(RatelMaterial material, char **cl_message);
RATEL_INTERN PetscErrorCode RatelMaterialSetOperatorName(RatelMaterial material, const char *base_name, CeedOperator op);

// Geometry
RATEL_INTERN PetscErrorCode RatelMaterialGetSolutionData(RatelMaterial material, CeedOperator op_residual_u, CeedInt *num_active_fields,
                                                         CeedElemRestriction **restrictions, CeedBasis **bases);
RATEL_INTERN PetscErrorCode RatelMaterialGetStoredDataU(RatelMaterial material, CeedOperator op_residual_u, CeedElemRestriction *restriction,
                                                        CeedVector *values);
RATEL_INTERN PetscErrorCode RatelMaterialGetStoredDataUt(RatelMaterial material, CeedOperator op_residual_ut, CeedElemRestriction *restriction,
                                                         CeedVector *values);
RATEL_INTERN PetscErrorCode RatelMaterialGetInitialStateData(RatelMaterial material, CeedOperator op_residual_u, CeedElemRestriction *restriction,
                                                             CeedVector *values);
RATEL_INTERN PetscErrorCode RatelMaterialGetStateData(RatelMaterial material, CeedOperator op_residual_u, CeedElemRestriction *restriction,
                                                      CeedVector *values);
RATEL_INTERN PetscErrorCode RatelMaterialGetPointData(RatelMaterial material, CeedOperator op_residual_u, CeedInt *num_point_fields,
                                                      CeedElemRestriction **restrictions, CeedVector **values);
RATEL_INTERN PetscErrorCode RatelMaterialGetVolumeQData(RatelMaterial material, CeedElemRestriction *restriction, CeedVector *q_data);
RATEL_INTERN PetscErrorCode RatelMaterialGetMeshVolumeQData(RatelMaterial material, CeedElemRestriction *restriction, CeedVector *q_data);
RATEL_INTERN PetscErrorCode RatelMaterialGetSurfaceGradientQData(RatelMaterial material, PetscInt dm_face, PetscInt orientation,
                                                                 CeedElemRestriction *restriction, CeedVector *q_data);
RATEL_INTERN PetscErrorCode RatelMaterialGetSurfaceGradientDiagnosticQData(RatelMaterial material, DM dm, PetscInt dm_face, PetscInt orientation,
                                                                           CeedElemRestriction *restriction, CeedVector *q_data);
// Residual and Jacobian
RATEL_INTERN PetscErrorCode RatelMaterialSetupResidualSuboperators(RatelMaterial material, CeedOperator op_residual_u, CeedOperator op_residual_ut,
                                                                   CeedOperator op_residual_utt);
RATEL_INTERN PetscErrorCode RatelMaterialSetupJacobianSuboperator(RatelMaterial material, CeedOperator op_residual_u, CeedOperator op_residual_ut,
                                                                  CeedOperator op_jacobian);
RATEL_INTERN PetscErrorCode RatelMaterialSetupJacobianBlockSuboperator(RatelMaterial material, DM dm, PetscInt field, CeedOperator op_jacobian_block);
RATEL_INTERN PetscErrorCode RatelMaterialForcingBodyDataFromOptions(RatelMaterial material, RatelForcingBodyParams *params_forcing);
RATEL_INTERN PetscErrorCode RatelMaterialSetupForcingSuboperator(RatelMaterial material, CeedOperator op_residual_u);
RATEL_INTERN PetscErrorCode RatelMaterialSetupMultigridLevel(RatelMaterial material, DM dm_level, CeedVector m_loc, CeedOperator op_jacobian_fine,
                                                             CeedOperator op_jacobian_coarse, CeedOperator op_prolong, CeedOperator op_restrict);
RATEL_INTERN PetscErrorCode RatelMaterialSetJacobianSmootherContext(RatelMaterial material, PetscBool set_or_unset, CeedOperator op_jacobian,
                                                                    PetscBool *was_set);
RATEL_INTERN PetscErrorCode RatelMaterialAcceptState(RatelMaterial material, CeedOperator op_residual);

// BCs
RATEL_INTERN PetscErrorCode RatelMaterialSetBoundaryJacobianMultigridInfo(RatelMaterial material, CeedOperator op_jacobian,
                                                                          PetscInt old_num_sub_operators, PetscInt new_num_sub_operators,
                                                                          RatelMaterialSetupMultigridLevelFunction setup_multigrid_level);

// Platen BC
RATEL_INTERN PetscErrorCode RatelMaterialSetupPlatenSuboperators(RatelMaterial material, CeedVector u_dot_loc, CeedOperator op_residual_u,
                                                                 CeedOperator op_jacobian);

// Pressure BC
RATEL_INTERN PetscErrorCode RatelMaterialSetupPressureSuboperators(RatelMaterial material, CeedOperator op_residual_u, CeedOperator op_jacobian);

// Diagnostic output
RATEL_INTERN PetscErrorCode RatelMaterialSetupStrainEnergySuboperator(RatelMaterial material, DM dm_energy, CeedOperator op_strain_energy);
RATEL_INTERN PetscErrorCode RatelMaterialSetupForcingEnergySuboperator(RatelMaterial material, DM dm_energy, CeedOperator op_external_energy);
RATEL_INTERN PetscErrorCode RatelMaterialSetupDiagnosticSuboperators(RatelMaterial material, DM dm_projected_diagnostic, DM dm_dual_diagnostic,
                                                                     CeedOperator op_mass_diagnostic, CeedOperator op_projected_diagnostic,
                                                                     CeedOperator op_dual_diagnostic, CeedOperator op_dual_nodal_scale);
RATEL_INTERN PetscErrorCode RatelMaterialSetupSurfaceForceCellToFaceSuboperators(RatelMaterial material, DM dm_surface_force,
                                                                                 CeedOperator *ops_surface_force);
RATEL_INTERN PetscErrorCode RatelMaterialSetupMMSErrorSuboperator(RatelMaterial material, CeedOperator op_mms_error);
