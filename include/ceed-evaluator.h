#pragma once

#include <ceed.h>
#include <petsc-ceed.h>
#include <petscdm.h>

/// libCEED evaluator object for applying composite `CeedOperator` on a `DM`
/// @ingroup RatelInternal
typedef struct _p_CeedEvaluator *CeedEvaluator;

PETSC_CEED_INTERN PetscErrorCode CeedEvaluatorCreate(DM dm_x, DM dm_y, CeedOperator op, CeedEvaluator *evaluator);
PETSC_CEED_INTERN PetscErrorCode CeedEvaluatorCreateWithSameInputs(CeedEvaluator evaluator_orig, DM dm_y, CeedOperator op, CeedEvaluator *evaluator);
PETSC_CEED_INTERN PetscErrorCode CeedEvaluatorView(CeedEvaluator evaluator, PetscViewer viewer);
PETSC_CEED_INTERN PetscErrorCode CeedEvaluatorDestroy(CeedEvaluator *evaluator);

PETSC_CEED_INTERN PetscErrorCode CeedEvaluatorSetContextDouble(CeedEvaluator evaluator, const char *name, double value);
PETSC_CEED_INTERN PetscErrorCode CeedEvaluatorGetContextDouble(CeedEvaluator evaluator, const char *name, double *value);
PETSC_CEED_INTERN PetscErrorCode CeedEvaluatorUpdateTimeAndBoundaryValues(CeedEvaluator evaluator, PetscReal time);
PETSC_CEED_INTERN PetscErrorCode CeedEvaluatorUpdateTimeDtAndBoundaryValues(CeedEvaluator evaluator, PetscReal time, PetscReal dt);
PETSC_CEED_INTERN PetscErrorCode CeedEvaluatorSetTime(CeedEvaluator evaluator, PetscReal time);
PETSC_CEED_INTERN PetscErrorCode CeedEvaluatorGetTime(CeedEvaluator evaluator, PetscReal *time);
PETSC_CEED_INTERN PetscErrorCode CeedEvaluatorSetDt(CeedEvaluator evaluator, PetscReal dt);
PETSC_CEED_INTERN PetscErrorCode CeedEvaluatorGetDt(CeedEvaluator evaluator, PetscReal *dt);

PETSC_CEED_EXTERN PetscErrorCode CeedEvaluatorHasField(CeedEvaluator evaluator, const char *name, PetscBool *has_field);

PETSC_CEED_EXTERN PetscErrorCode CeedEvaluatorGetDMs(CeedEvaluator evaluator, DM *dm_x, DM *dm_y);
PETSC_CEED_EXTERN PetscErrorCode CeedEvaluatorRestoreDMs(CeedEvaluator evaluator, DM *dm_x, DM *dm_y);

PETSC_CEED_EXTERN PetscErrorCode CeedEvaluatorSetLocalVectors(CeedEvaluator evaluator, Vec X_loc, Vec X_dot_loc, Vec Y_loc);
PETSC_CEED_EXTERN PetscErrorCode CeedEvaluatorGetLocalVectors(CeedEvaluator evaluator, Vec *X_loc, Vec *X_dot_loc, Vec *Y_loc);
PETSC_CEED_EXTERN PetscErrorCode CeedEvaluatorRestoreLocalVectors(CeedEvaluator evaluator, Vec *X_loc, Vec *X_dot_loc, Vec *Y_loc);
PETSC_CEED_EXTERN PetscErrorCode CeedEvaluatorSetLocalCeedVectors(CeedEvaluator evaluator, CeedVector x_loc, CeedVector x_dot_loc, CeedVector y_loc);
PETSC_CEED_EXTERN PetscErrorCode CeedEvaluatorGetLocalCeedVectors(CeedEvaluator evaluator, CeedVector *x_loc, CeedVector *x_dot_loc,
                                                                  CeedVector *y_loc);
PETSC_CEED_EXTERN PetscErrorCode CeedEvaluatorRestoreLocalCeedVectors(CeedEvaluator evaluator, CeedVector *x_loc, CeedVector *x_dot_loc,
                                                                      CeedVector *y_loc);

PETSC_CEED_EXTERN PetscErrorCode CeedEvaluatorApplyGlobalToGlobal(CeedEvaluator evaluator, Vec X, Vec Y);
PETSC_CEED_EXTERN PetscErrorCode CeedEvaluatorApplyAddGlobalToGlobal(CeedEvaluator evaluator, Vec X, Vec Y);
PETSC_CEED_EXTERN PetscErrorCode CeedEvaluatorApplyVelocityGlobalToGlobal(CeedEvaluator evaluator, Vec X, Vec X_dot, Vec Y);
PETSC_CEED_EXTERN PetscErrorCode CeedEvaluatorApplyGlobalToLocal(CeedEvaluator evaluator, Vec X, Vec Y_loc);
PETSC_CEED_EXTERN PetscErrorCode CeedEvaluatorApplyLocalToLocal(CeedEvaluator evaluator, Vec X_loc, Vec Y_loc);
PETSC_CEED_EXTERN PetscErrorCode CeedEvaluatorApplyAddLocalToLocal(CeedEvaluator evaluator, Vec X_loc, Vec Y_loc);

PETSC_CEED_EXTERN PetscErrorCode CeedEvaluatorGetCeedOperator(CeedEvaluator evaluator, CeedOperator *op);
PETSC_CEED_EXTERN PetscErrorCode CeedEvaluatorRestoreCeedOperator(CeedEvaluator evaluator, CeedOperator *op);

PETSC_CEED_EXTERN PetscErrorCode CeedEvaluatorSetLogEvent(CeedEvaluator evaluator, PetscLogEvent log_event);
PETSC_CEED_EXTERN PetscErrorCode CeedEvaluatorGetLogEvent(CeedEvaluator evaluator, PetscLogEvent *log_event);
PETSC_CEED_EXTERN PetscErrorCode CeedEvaluatorSetCeedOperatorLogEvent(CeedEvaluator evaluator, PetscLogEvent log_event);
PETSC_CEED_EXTERN PetscErrorCode CeedEvaluatorGetCeedOperatorLogEvent(CeedEvaluator evaluator, PetscLogEvent *log_event);
