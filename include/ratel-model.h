#pragma once

#include <ceed.h>
#include <petscsys.h>
#include <ratel-impl.h>
#include <ratel.h>

// Model parameter options
#define RATEL_MODEL_REGISTER(material_create_functions, model_cl_argument, model_postfix) \
  PetscCall(RatelDebug(ratel, "---- Registration of model \"%s\"", model_cl_argument));   \
  PetscCall(PetscFunctionListAdd(material_create_functions, model_cl_argument, RatelMaterialCreate_##model_postfix));

RATEL_INTERN PetscErrorCode RatelRegisterModels(Ratel ratel, PetscFunctionList *material_create_functions);
RATEL_INTERN PetscErrorCode RatelModelDataVerifyRelativePath(Ratel ratel, RatelModelData model_data);
RATEL_INTERN PetscErrorCode RatelModelParameterDataGetDefaultValue_Scalar(Ratel ratel, RatelModelParameterData data, const char *name,
                                                                          CeedScalar *value);
RATEL_INTERN PetscErrorCode RatelModelParameterDataGetDefaultValue_Int(Ratel ratel, RatelModelParameterData data, const char *name, CeedInt *value);
RATEL_INTERN PetscErrorCode RatelModelParameterDataGetDefaultValue_Bool(Ratel ratel, RatelModelParameterData data, const char *name,
                                                                        PetscBool *value);
RATEL_INTERN PetscErrorCode RatelModelParameterDataRegisterContextFields(Ratel ratel, RatelModelParameterData parameters, CeedQFunctionContext ctx);

// Model parameter options
// The RATEL_MODEL_PARAMS macro creates the declarations for the 2 functions each RatelMaterialParams is required to create.
#define RATEL_MODEL_PARAMS(params_postfix)                                                                              \
  RATEL_INTERN PetscErrorCode RatelMaterialParamsContextCreate_##params_postfix(RatelMaterial, CeedQFunctionContext *); \
  RATEL_INTERN PetscErrorCode RatelMaterialParamsSmootherDataSetup_##params_postfix(RatelMaterial);

RATEL_MODEL_PARAMS(NeoHookean)
RATEL_MODEL_PARAMS(MooneyRivlin)
RATEL_MODEL_PARAMS(Plasticity)
RATEL_MODEL_PARAMS(Ogden)
RATEL_MODEL_PARAMS(ElasticityDamage)
RATEL_MODEL_PARAMS(MixedNeoHookean)
RATEL_MODEL_PARAMS(MixedOgden)
RATEL_MODEL_PARAMS(PoroElasticityLinear)
RATEL_MODEL_PARAMS(PoroElasticityNeoHookean)

// MMS parameter options
// The RATEL_MMSL_PARAMS macro creates the declarations for the function each MMS is required to create.
#define RATEL_MMS_PARAMS(mms_postfix) RATEL_INTERN PetscErrorCode RatelMMSParamsContextCreate_##mms_postfix(RatelMaterial, CeedQFunctionContext *);

RATEL_MMS_PARAMS(CEED_BPs)
RATEL_MMS_PARAMS(Elasticity_Linear)
RATEL_MMS_PARAMS(Elasticity_MixedLinear)
RATEL_MMS_PARAMS(PoroElasticityLinear)
