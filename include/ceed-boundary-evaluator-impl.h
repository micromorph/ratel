#pragma once

#include <ceed.h>
#include <petsc-ceed.h>
#include <petsc/private/petscimpl.h>
#include <petscdm.h>

typedef struct _CeedBoundaryEvaluatorOps *CeedBoundaryEvaluatorOps;
struct _CeedBoundaryEvaluatorOps {};

struct _p_CeedBoundaryEvaluator {
  PETSCHEADER(struct _CeedBoundaryEvaluatorOps);
  Ceed                  ceed;
  PetscLogEvent         log_event, log_event_ceed;
  CeedVector            u_loc;
  CeedContextFieldLabel time_field, dt_field;
  CeedOperator          op_dirichlet;
  CeedScalar            time; /* Cached time value */
  CeedScalar            dt;   /* Cached time step value */
};

PETSC_CEED_INTERN PetscErrorCode DMPlexInsertBoundaryValues_Ceed(DM dm, PetscBool insert_essential, Vec U_loc, PetscReal time, Vec face_geometry_FVM,
                                                                 Vec cell_geometry_FVM, Vec grad_FVM);
