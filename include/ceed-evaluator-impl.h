#pragma once

#include <ceed.h>
#include <petsc-ceed.h>
#include <petsc/private/petscimpl.h>
#include <petscdm.h>

typedef struct _CeedEvaluatorOps *CeedEvaluatorOps;
struct _CeedEvaluatorOps {};

struct _p_CeedEvaluator {
  PETSCHEADER(struct _CeedEvaluatorOps);
  Ceed          ceed;
  PetscLogEvent log_event, log_event_ceed;
  Vec           X_loc, X_dot_loc, Y_loc;
  DM            dm_x, dm_y;
  CeedVector    x_loc, x_dot_loc, y_loc;
  CeedOperator  op;
  CeedScalar    time; /* Cached time value */
  CeedScalar    dt;   /* Cached time step value */
};
