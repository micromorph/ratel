#pragma once

#include <ratel-impl.h>
#include <ratel.h>

RATEL_INTERN PetscErrorCode RatelProcessCommandLineOptions(Ratel ratel);
