#pragma once

#include <petscdm.h>
#include <ratel-impl.h>
#include <ratel.h>
#include <ratel/models/boundary.h>
#include <ratel/models/friction.h>
#include <ratel/models/platen.h>

RATEL_INTERN PetscErrorCode RatelCreateBCLabel(DM dm, const char name[]);
RATEL_INTERN PetscErrorCode RatelDMAddBoundariesDirichlet(Ratel ratel, DM dm);
RATEL_INTERN PetscErrorCode RatelDMAddBoundariesSlip(Ratel ratel, DM dm);
RATEL_INTERN PetscErrorCode RatelBoundarySlipDataFromOptions(Ratel ratel, PetscInt i, PetscInt j, RatelBCSlipParams *params_slip);
RATEL_INTERN PetscErrorCode RatelCeedAddBoundariesDirichletSlip(Ratel ratel, DM dm, PetscBool incremental, CeedOperator op_dirichlet);
RATEL_INTERN PetscErrorCode RatelBoundaryClampDataFromOptions(Ratel ratel, PetscInt i, PetscInt j, RatelBCClampParams *params_clamp);
RATEL_INTERN PetscErrorCode RatelCeedAddBoundariesDirichletClamp(Ratel ratel, DM dm, PetscBool incremental, CeedOperator op_dirichlet);
RATEL_INTERN PetscErrorCode RatelCeedAddBoundariesDirichletMMS(Ratel ratel, DM dm, CeedOperator op_dirichlet);
RATEL_INTERN PetscErrorCode RatelBoundaryTractionDataFromOptions(Ratel ratel, PetscInt i, RatelBCTractionParams *params_traction);
RATEL_INTERN PetscErrorCode RatelBoundaryPressureDataFromOptions(Ratel ratel, PetscInt i, RatelBCPressureParams *params_pressure);
RATEL_INTERN PetscErrorCode RatelCeedSetupSurfaceQData(Ratel ratel, DM dm, const char *label_name, PetscInt label_value,
                                                       CeedElemRestriction *restriction, CeedVector *q_data);
RATEL_INTERN PetscErrorCode RatelCeedAddBoundariesNeumann(Ratel ratel, DM dm, CeedOperator op_residual);
RATEL_INTERN PetscErrorCode RatelSetupTractionEnergySuboperator(Ratel ratel, DM dm_energy, CeedOperator op_external_energy);

RATEL_INTERN PetscErrorCode RatelFrictionParamsView(Ratel ratel, const RatelFrictionParams *params_friction, PetscViewer viewer);
RATEL_INTERN PetscErrorCode RatelFrictionParamsFromOptions(Ratel ratel, const char option_prefix[], RatelFrictionParams *params_friction);
RATEL_INTERN PetscErrorCode RatelBoundaryPlatenParamsCommonView(Ratel ratel, const RatelBCPlatenParamsCommon *params_platen, PetscViewer viewer);
RATEL_INTERN PetscErrorCode RatelBoundaryPlatenParamsCommonFromOptions(Ratel ratel, const char options_prefix[],
                                                                       RatelBCPlatenParamsCommon *params_platen);
RATEL_INTERN PetscErrorCode RatelMaterialCeedAddBoundariesPlatenNitsche(RatelMaterial material, CeedVector u_dot_loc, CeedOperator op_residual,
                                                                        CeedOperator op_jacobian);
RATEL_INTERN PetscErrorCode RatelCeedAddBoundariesPlatenPenalty(Ratel ratel, DM dm, CeedVector u_dot_loc, CeedOperator op_residual,
                                                                CeedOperator op_jacobian);
RATEL_INTERN PetscErrorCode RatelMaterialSetupPlatenNitscheJacobianMultigridLevel(RatelMaterial material, DM dm_level, CeedVector m_loc,
                                                                                  CeedOperator sub_op_jacobian_fine, CeedOperator op_jacobian_coarse,
                                                                                  CeedOperator op_prolong, CeedOperator op_restrict);
RATEL_INTERN PetscErrorCode RatelMaterialSetupPlatenPenaltyJacobianMultigridLevel(RatelMaterial material, DM dm_level, CeedVector m_loc,
                                                                                  CeedOperator sub_op_jacobian_fine, CeedOperator op_jacobian_coarse,
                                                                                  CeedOperator op_prolong, CeedOperator op_restrict);
RATEL_INTERN PetscErrorCode RatelSetupSurfaceForceCentroids(Ratel ratel, DM dm);
RATEL_INTERN PetscErrorCode RatelBoundarySetupSurfaceDisplacementSuboperators(Ratel ratel, DM dm_surface_displacement,
                                                                              CeedOperator ops_surface_displacement[]);
RATEL_INTERN PetscErrorCode RatelCeedAddBoundariesPressure(Ratel ratel, DM dm, CeedOperator op_residual, CeedOperator op_jacobian);
RATEL_INTERN PetscErrorCode RatelSetupPressureJacobianMultigridLevel(Ratel ratel, DM dm_level, CeedVector m_loc, CeedOperator sub_op_jacobian_fine,
                                                                     CeedOperator op_jacobian_coarse);
RATEL_INTERN PetscErrorCode RatelBoundingBoxParamsFromOptions(Ratel ratel, const char option_prefix[], const char name[],
                                                              RatelBoundingBoxParams *params_bounding_box);
RATEL_INTERN PetscErrorCode RatelFaceLabelValueFromOptions(Ratel ratel, const char option_prefix[], const char name[], PetscInt *label_value);
