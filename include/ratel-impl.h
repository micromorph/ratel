#pragma once

#ifdef __clang_analyzer__
#define RATEL_INTERN
#else
#define RATEL_INTERN RATEL_EXTERN __attribute__((visibility("hidden")))
#endif

#include <ceed-boundary-evaluator.h>
#include <ceed-evaluator.h>
#include <ceed.h>
#include <ceed/backend.h>
#include <mat-ceed.h>
#include <petsc-ceed.h>
#include <petscksp.h>
#include <ratel.h>
#include <ratel/models/common-parameters.h>
#include <ratel/models/elasticity-linear.h>

RATEL_INTERN const char *RatelJitSourceRootDefault;

#define RatelQFunctionRelativePath(absolute_path) strstr(absolute_path, "ratel/qfunctions")

#if !(CEED_VERSION_GE(0, 12, 1))
#error "libCEED latest main branch or libCEED v0.12.1 is required"
#endif

/**
  @brief Calls a libCEED function and then checks the resulting error code.
  If the error code is non-zero, then a PETSc error is set with the libCEED error message.
**/
/// @ingroup RatelInternal
#define RatelCallCeed(ratel, ...) PetscCallCeed((ratel->ceed), __VA_ARGS__)

#define RATEL_MAX_VIEWERS 16
#define RATEL_MAX_MATERIALS 16
#define RATEL_MAX_BOUNDARY_FACES 16
#define RATEL_MAX_MULTIGRID_LEVELS 16
#define RATEL_MAX_MATERIAL_LABEL_VALUES 128
#define RATEL_MAX_MATERIAL_PARAMETERS 64
#define RATEL_CHECKPOINT_VERSION_INT32 0XCEEDF32
#define RATEL_CHECKPOINT_VERSION_INT64 0XCEEDF64
#define RATEL_UNSET_INITIAL_TIME -42.0
#define RATEL_PCPMG "pmg"

/// Colors for debug logging,
typedef enum {
  /// Success color
  RATEL_DEBUG_COLOR_SUCCESS = CEED_DEBUG_COLOR_SUCCESS,
  /// Warning color
  RATEL_DEBUG_COLOR_WARNING = CEED_DEBUG_COLOR_WARNING,
  /// Error color
  RATEL_DEBUG_COLOR_ERROR   = CEED_DEBUG_COLOR_ERROR,
  /// Use native terminal coloring
  RATEL_DEBUG_COLOR_NONE    = CEED_DEBUG_COLOR_NONE,
} RatelDebugColor;

/// Forward RatelDebug* declarations & macros
RATEL_INTERN PetscErrorCode RatelDebugImpl256(MPI_Comm comm, PetscBool is_debug, PetscBool all_ranks, PetscInt comm_rank, const unsigned char,
                                              const char *, ...);
#define RatelDebug256(ratel, color, ...) RatelDebugImpl256((ratel)->comm, (ratel)->is_debug, PETSC_FALSE, (ratel)->comm_rank, color, __VA_ARGS__)
#define RatelDebug(ratel, ...) RatelDebug256(ratel, (unsigned char)RATEL_DEBUG_COLOR_NONE, __VA_ARGS__)
#define RatelDebugEnv256(color, ...) \
  RatelDebugImpl256(PETSC_COMM_WORLD, (!!getenv("RATEL_DEBUG") || !!getenv("DEBUG") || !!getenv("DBG")), PETSC_FALSE, -1, color, __VA_ARGS__)
#define RatelDebugEnv(...) RatelDebugEnv256((unsigned char)RATEL_DEBUG_COLOR_NONE, __VA_ARGS__)
#define RatelParallelDebug256(ratel, color, ...) \
  RatelDebugImpl256((ratel)->comm, (ratel)->is_debug, PETSC_TRUE, (ratel)->comm_rank, color, __VA_ARGS__)
#define RatelParallelDebug(ratel, ...) RatelParallelDebug256(ratel, (unsigned char)RATEL_DEBUG_COLOR_NONE, __VA_ARGS__)
#define RatelPrintf256(ratel, color, ...) RatelDebugImpl256((ratel)->comm, PETSC_TRUE, PETSC_FALSE, (ratel)->comm_rank, color, __VA_ARGS__)

/// Default value option
#define RATEL_DECIDE -42

/// Units
struct RatelUnits_private {
  /// Fundamental units
  PetscScalar meter;
  PetscScalar kilogram;
  PetscScalar second;
  /// Derived unit
  PetscScalar Pascal;
  PetscScalar JoulePerSquareMeter;
};

/// Function signature for BC function to set on DMPlex face
/// @ingroup RatelBoundary
typedef PetscErrorCode RatelBCFunction(PetscInt, PetscReal, const PetscReal *, PetscInt, PetscScalar *, void *);

typedef union {
  CeedInt    i;
  CeedScalar s;
  bool       b;
} RatelDefaultParameterValue;

/// Information about a single material model parameter
/// @ingroup RatelMaterials
typedef struct {
  /// Name of parameter, used for PetscOptions, e.g. "nu"
  const char *name;
  /// Description of parameter, used for PetscOptions
  const char *description;
  /// SI units expected by the parameter, e.g. "Pa"
  const char *units;
  /// Expected domain of values, e.g. "0 <= nu < 0.5"
  const char *restrictions;
  /// Is parameter required?
  PetscBool is_required;
  /// Should parameter be hidden in `RatelModelParameterDataView`?
  PetscBool is_hidden;
  /// Default value
  RatelDefaultParameterValue default_value;
  /// Offset into context
  CeedSize offset;
  /// Number of components
  CeedInt num_components;
  /// Type of parameter
  CeedContextFieldType type;
} RatelModelParameter;

/// Material model parameter information for printing
/// @struct RatelModelParameterData
/// @ingroup RatelMaterials
typedef struct RatelModelParameterData_private *RatelModelParameterData;
struct RatelModelParameterData_private {
  /// List of model parameters
  RatelModelParameter parameters[RATEL_MAX_MATERIAL_PARAMETERS];
  /// Number of model parameters
  const CeedInt num_parameters;
  /// DOI or link to reference for material model
  const char *reference;
};

/// Material model data
/// @struct RatelModelData
/// @ingroup RatelMaterials
typedef struct RatelModelData_private *RatelModelData;
struct RatelModelData_private {
  const char       *name;
  const char       *command_line_option;
  CeedQFunctionUser setup_q_data_volume, setup_q_data_surface, setup_q_data_surface_grad, residual_u, residual_ut, residual_utt, jacobian,
      jacobian_block[RATEL_MAX_FIELDS], strain_energy, projected_diagnostic, dual_diagnostic, mms_boundary[RATEL_MAX_FIELDS], mms_forcing, mms_error,
      mms_forcing_energy, platen_residual_u, platen_jacobian, setup_q_data_volume_mpm, set_point_fields, update_volume_mpm,
      surface_force_cell_to_face;
  const char *setup_q_data_volume_loc, *setup_q_data_surface_loc, *setup_q_data_surface_grad_loc, *residual_u_loc, *residual_ut_loc,
      *residual_utt_loc, *jacobian_loc, *jacobian_block_loc[RATEL_MAX_FIELDS], *strain_energy_loc, *projected_diagnostic_loc, *dual_diagnostic_loc,
      *mms_boundary_loc[RATEL_MAX_FIELDS], *mms_forcing_loc, *mms_error_loc, *mms_forcing_energy_loc, *platen_residual_u_loc, *platen_jacobian_loc,
      *setup_q_data_volume_mpm_loc, *set_point_fields_loc, *update_volume_mpm_loc, *surface_force_cell_to_face_loc;
  CeedInt num_active_fields, num_point_fields, num_comp_stored_u, num_comp_stored_ut, num_comp_stored_platen, num_comp_state,
      num_comp_projected_diagnostic, num_comp_dual_diagnostic, q_data_volume_size, q_data_surface_size, q_data_surface_grad_size, num_forcing;
  const char *const  *active_field_names, **point_field_names;
  const char *const  *active_component_names;
  const CeedInt      *active_field_sizes, *active_field_num_eval_modes, *ut_field_num_eval_modes, *utt_field_num_eval_modes, *point_field_sizes;
  const CeedEvalMode *active_field_eval_modes, *ut_field_eval_modes, *utt_field_eval_modes;
  const char *const  *projected_diagnostic_component_names;
  const char *const  *dual_diagnostic_component_names;
  CeedQuadMode        quadrature_mode;
  PetscScalar         initial_random_scaling;
  PetscBool           has_ut_term, pass_stored_u_to_ut;
  PetscLogDouble      flops_qf_jacobian_u, flops_qf_jacobian_ut, flops_qf_jacobian_utt, flops_qf_jacobian_platen,
      flops_qf_jacobian_block[RATEL_MAX_FIELDS];
  RatelModelParameterData param_data, mms_param_data;
};

/// Material region
/// @struct RatelMaterial
/// @ingroup RatelMaterials
typedef struct RatelMaterial_private *RatelMaterial;

/// @brief Material model multigrid level setup function
/// @ingroup RatelMaterials
typedef PetscErrorCode (*RatelMaterialSetupMultigridLevelFunction)(RatelMaterial material, DM dm_level, CeedVector m_loc, CeedOperator sub_op_fine,
                                                                   CeedOperator op_coarse, CeedOperator op_prolong, CeedOperator op_restrict);
struct RatelMaterial_private {
  Ratel       ratel;
  const char *name;
  /// Mesh
  PetscInt  num_volume_label_values;
  PetscInt *volume_label_values;
  char     *volume_label_name;
  PetscInt  num_face_orientations;
  PetscInt  num_surface_grad_dm_face_ids, num_surface_grad_diagnostic_dm_face_ids;
  PetscInt *surface_grad_dm_face_ids, *surface_grad_diagnostic_dm_face_ids;
  char    **surface_grad_label_names, **surface_grad_diagnostic_label_names;
  /// Model
  RatelModelData         model_data;
  CeedQFunctionContext   ctx_params, ctx_mms_params;
  PetscInt               num_params_smoother_values;
  CeedScalar            *ctx_params_values, *ctx_params_smoother_values;
  char                 **ctx_params_label_names;
  CeedContextFieldLabel *ctx_params_labels;
  /// MPM cached data
  RatelMPMContext mpm;
  PetscBool       has_point_fields_initialized;
  /// Forcing
  RatelForcingType forcing_type;
  /// Qdata
  CeedVector          q_data_volume, **q_data_surface_grad_diagnostic, **q_data_surface_grad;
  CeedElemRestriction restriction_q_data_volume, **restrictions_q_data_surface_grad_diagnostic, **restrictions_q_data_surface_grad;
  /// Operators
  PetscInt *residual_indices, num_residual_indices, *residual_ut_indices, num_residual_ut_indices, *jacobian_indices, num_jacobian_indices;
  /// Methods
  /// -- Geometry
  PetscErrorCode (*SetupVolumeQData)(RatelMaterial, const char *, PetscInt, CeedElemRestriction *, CeedVector *);
  PetscErrorCode (*SetupSurfaceGradientQData)(RatelMaterial, DM, const char *, PetscInt, CeedElemRestriction *, CeedVector *);
  /// -- Residual and Jacobian operators
  PetscErrorCode (*SetupResidualSuboperators)(RatelMaterial, CeedOperator, CeedOperator, CeedOperator);
  PetscErrorCode (*SetupJacobianSuboperator)(RatelMaterial, CeedOperator, CeedOperator, CeedOperator);
  PetscErrorCode (*SetupJacobianBlockSuboperator)(RatelMaterial, DM, PetscInt, CeedOperator);
  RatelMaterialSetupMultigridLevelFunction *RatelMaterialSetupMultigridLevels;
  /// -- Output operators
  PetscErrorCode (*SetupStrainEnergySuboperator)(RatelMaterial, DM, CeedOperator);
  PetscErrorCode (*SetupDiagnosticSuboperators)(RatelMaterial, DM, DM, CeedOperator, CeedOperator, CeedOperator, CeedOperator);
  PetscErrorCode (*SetupSurfaceForceCellToFaceSuboperators)(RatelMaterial, DM, CeedOperator *);
  PetscErrorCode (*SetupMMSErrorSuboperator)(RatelMaterial, CeedOperator);
};

/// Ratel viewer information for monitor routines
/// @struct RatelViewer
/// @ingroup RatelInternal
typedef struct RatelViewer_private *RatelViewer;
struct RatelViewer_private {
  Ratel             ratel;
  PetscViewer       viewer, aux_viewer;
  PetscViewerFormat viewer_format;
  PetscViewerType   viewer_type;
  PetscInt          interval;
  char             *monitor_file_name, *monitor_file_extension;
  PetscBool         is_monitor_vtk;
  PetscBool         is_header_written;
  PetscBool         is_footer_written;
  PetscErrorCode (*WriteHeader)(RatelViewer, void *);
  PetscErrorCode (*WriteFooter)(RatelViewer);
};

/// Ratel context for wrapping a collection of `RatelViewer` objects
/// @struct RatelViewers
/// @ingroup RatelInternal
typedef struct RatelViewers_private *RatelViewers;
struct RatelViewers_private {
  Ratel        ratel;
  PetscInt     num_viewers;
  RatelViewer *viewers;
};

/// Ratel context for checkpoints
/// @struct RatelCheckpointCtx
/// @ingroup RatelInternal
typedef struct RatelCheckpointCtx_private *RatelCheckpointCtx;
struct RatelCheckpointCtx_private {
  PetscBool  write_toggle, toggle_state;
  PetscInt   interval;
  RatelUnits units;
  char      *file_name;
};

/// Ratel data stored in checkpoint file
/// @struct RatelCheckpointData
/// @ingroup RatelInternal
typedef struct RatelCheckpointData_private *RatelCheckpointData;
struct RatelCheckpointData_private {
  PetscInt32  version;
  PetscInt    step;
  PetscReal   time;
  PetscViewer viewer;
};

/// Ratel context for MPM options
/// @struct RatelMPMOptions
/// @ingroup RatelInternal
typedef struct RatelMPMOptions_private *RatelMPMOptions;
struct RatelMPMOptions_private {
  RatelPointLocationType point_location_type;
  PetscInt               num_points, num_points_per_cell;
};

/// Ratel context for MPM, stores points `CeedElemRestriction` for a material region
struct RatelMPMContext_private {
  Ratel               ratel;
  CeedVector          x_ref_points, q_data_points;
  CeedElemRestriction restriction_x_points, restriction_q_data_points;
};

/// Ratel private context
struct Ratel_private {
  MPI_Comm        comm;
  PetscInt        comm_rank;
  RatelSolverType solver_type;
  RatelMethodType method_type;
  PetscBool       is_debug;
  PetscBool       is_ceed_backend_gpu;
  PetscBool       is_spd;
  PetscBool       use_objective;
  PetscBool       is_rank_0_debug;
  PetscBool       is_ts_steprestart;
  /// Ceed
  char *ceed_resource;
  Ceed  ceed;
  /// Point field data
  CeedVector    *point_fields;
  CeedInt        num_point_fields;
  const CeedInt *point_field_sizes;
  const char   **point_field_names;
  /// Caching state
  PetscInt mesh_remap_state;
  /// PETSc
  DM       dm_solution, dm_diagnostic, *sub_dms_diagnostic;
  PetscInt num_sub_dms_diagnostic;
  IS      *is_sub_dms_diagnostic;
  KSP      ksp_diagnostic_projection;
  Mat      mat_jacobian, mat_jacobian_pre;
  /// Solver and diagnostic operators
  PetscBool     are_snes_mats_set;
  CeedEvaluator evaluator_residual_u, evaluator_residual_ut, evaluator_residual_utt, evaluator_strain_energy, evaluator_external_energy,
      evaluator_projected_diagnostic, evaluator_dual_diagnostic, evaluator_dual_nodal_scale, evaluator_surface_force, evaluator_mms_error,
      evaluators_surface_displacement[RATEL_MAX_BOUNDARY_FACES], evaluators_surface_force_face[RATEL_MAX_BOUNDARY_FACES],
      evaluators_surface_force_cell_to_face[RATEL_MAX_BOUNDARY_FACES];
  CeedInt num_jacobian_multiplicity_skip_indices, *jacobian_multiplicity_skip_indices, first_jacobian_pressure_index;
  /// Materials
  PetscFunctionList material_create_functions;
  PetscInt          num_materials, num_active_fields, fine_grid_orders[RATEL_MAX_FIELDS];
  RatelMaterial    *materials;
  char             *material_volume_label_name;
  RatelUnits        material_units;
  PetscBool         has_mixed_u_ut_term, has_ut_term;
  /// Initial condition
  RatelInitialConditionType initial_condition_type;
  char                     *continue_file_name;
  /// Mesh options
  PetscInt highest_fine_order, diagnostic_order, diagnostic_geometry_order;
  PetscInt q_extra, q_extra_surface_force;
  /// Boundaries
  PetscInt              bc_clamp_count[RATEL_MAX_FIELDS];
  PetscInt              bc_clamp_nonzero_count;
  PetscInt              bc_clamp_faces[RATEL_MAX_FIELDS][RATEL_MAX_BOUNDARY_FACES];
  PetscInt              bc_mms_count[RATEL_MAX_FIELDS];
  PetscInt              bc_mms_faces[RATEL_MAX_FIELDS][RATEL_MAX_BOUNDARY_FACES];
  PetscInt              bc_slip_count[RATEL_MAX_FIELDS];
  PetscInt              bc_slip_faces[RATEL_MAX_FIELDS][RATEL_MAX_BOUNDARY_FACES];
  PetscInt              bc_slip_components_count[RATEL_MAX_FIELDS][RATEL_MAX_BOUNDARY_FACES];
  PetscInt              bc_slip_components[RATEL_MAX_FIELDS][RATEL_MAX_BOUNDARY_FACES][4];
  PetscInt              bc_traction_count;
  PetscInt              bc_traction_faces[RATEL_MAX_BOUNDARY_FACES];
  PetscInt              bc_pressure_count;
  PetscInt              bc_pressure_faces[RATEL_MAX_BOUNDARY_FACES];
  PetscInt              bc_platen_count;
  RatelPlatenType       bc_platen_types[RATEL_MAX_BOUNDARY_FACES];
  PetscInt              bc_platen_label_values[RATEL_MAX_BOUNDARY_FACES];
  PetscInt              surface_force_face_count;
  char                 *surface_force_face_names[RATEL_MAX_BOUNDARY_FACES];
  PetscInt              surface_force_face_label_values[RATEL_MAX_BOUNDARY_FACES];
  char                 *bc_platen_names[RATEL_MAX_BOUNDARY_FACES];
  PetscScalar           initial_surface_area[RATEL_MAX_BOUNDARY_FACES];
  PetscScalar           initial_surface_centroid[RATEL_MAX_BOUNDARY_FACES][3];
  CeedBoundaryEvaluator boundary_evaluator_u, boundary_evaluator_visualization;
  /// Testing
  PetscBool   has_mms;
  PetscBool   has_expected_strain_energy;
  PetscScalar expected_strain_energy;
  PetscBool   has_expected_max_displacement[3];
  PetscScalar expected_max_displacement[3];
  PetscBool   has_expected_surface_force[RATEL_MAX_BOUNDARY_FACES][3];
  PetscScalar expected_surface_force[RATEL_MAX_BOUNDARY_FACES][3];
  PetscBool   has_expected_centroid[RATEL_MAX_BOUNDARY_FACES][3];
  PetscScalar expected_centroid[RATEL_MAX_BOUNDARY_FACES][3];
};

extern PetscClassId  RATEL_CLASSID;
extern PetscLogStage RATEL_Setup;
extern PetscLogEvent RATEL_DMSetupByOrder, RATEL_DMSetupSolver, RATEL_Diagnostics, RATEL_Diagnostics_CeedOp, RATEL_Residual, RATEL_Residual_CeedOp,
    RATEL_Jacobian, RATEL_Jacobian_CeedOp, RATEL_Prolong[RATEL_MAX_MULTIGRID_LEVELS], RATEL_Prolong_CeedOp[RATEL_MAX_MULTIGRID_LEVELS],
    RATEL_Restrict_CeedOp[RATEL_MAX_MULTIGRID_LEVELS], RATEL_Restrict[RATEL_MAX_MULTIGRID_LEVELS];
