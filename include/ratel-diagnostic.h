#pragma once

#include <petscvec.h>
#include <ratel-impl.h>
#include <ratel.h>

// Operator contexts
RATEL_INTERN PetscErrorCode RatelSetupStrainEnergyEvaluator(Ratel ratel, CeedEvaluator *evaluator_strain_energy);
RATEL_INTERN PetscErrorCode RatelSetupExternalEnergyEvaluator(Ratel ratel, CeedEvaluator *evaluator_external_energy);
RATEL_INTERN PetscErrorCode RatelSetupDiagnosticEvaluators(Ratel ratel, KSP *ksp_diagnostic_projection, CeedEvaluator *evaluator_projected_diagnostic,
                                                           CeedEvaluator *evaluator_dual_diagnostic, CeedEvaluator *evaluator_dual_nodal_scale);
RATEL_INTERN PetscErrorCode RatelSetupMMSErrorEvaluator(Ratel ratel, CeedEvaluator *evaluator_mms_error);
RATEL_INTERN PetscErrorCode RatelSetupSurfaceDisplacementEvaluators(Ratel ratel, CeedEvaluator evaluators_surface_displacement[]);
RATEL_INTERN PetscErrorCode RatelSetupSurfaceForceEvaluator(Ratel ratel, CeedEvaluator *evaluator_surface_force,
                                                            CeedEvaluator evaluators_surface_force_face[]);
RATEL_INTERN PetscErrorCode RatelSetupSurfaceForceCellToFaceEvaluators(Ratel ratel, CeedEvaluator evaluators_surface_force_cell_to_face[]);

// Computation routines
RATEL_INTERN PetscErrorCode RatelComputeDiagnosticQuantities_Internal(Ratel ratel, Vec U, PetscReal time, Vec D);
RATEL_INTERN PetscErrorCode RatelComputeSurfaceCentroids_Internal(Ratel ratel, Vec U, PetscReal time, PetscScalar *surface_centroids);
RATEL_INTERN PetscErrorCode RatelComputeSurfaceForces_Internal(Ratel ratel, Vec U, PetscReal time, PetscScalar *surface_forces);
RATEL_INTERN PetscErrorCode RatelComputeSurfaceForcesCellToFace_Internal(Ratel ratel, Vec U, PetscReal time, PetscScalar *surface_forces);
