#pragma once

#include <ceed.h>
#include <petscdmswarm.h>
#include <petscsys.h>
#include <ratel-impl.h>
#include <ratel.h>

RATEL_INTERN const char DMSwarmPICField_volume[7];

RATEL_INTERN PetscErrorCode RatelMPMOptionsCreateFromOptions(Ratel ratel, RatelMPMOptions *options);
RATEL_INTERN PetscErrorCode RatelMaterialCreate_MPM(Ratel ratel, RatelMaterial material);

// Material MPM context
RATEL_INTERN PetscErrorCode RatelMaterialGetMPMContext(RatelMaterial material, RatelMPMContext *mpm);

// MPM context functions
RATEL_INTERN PetscErrorCode RatelMPMContextCreate(Ratel ratel, CeedVector x_ref_points, CeedElemRestriction restriction_x_points,
                                                  RatelMPMContext *mpm);
RATEL_INTERN PetscErrorCode RatelMPMContextDestroy(RatelMPMContext *mpm);
RATEL_INTERN PetscErrorCode RatelMPMContextGetPoints(RatelMPMContext mpm, CeedVector *x_ref_points);
RATEL_INTERN PetscErrorCode RatelMPMContextGetElemRestrictionPoints(RatelMPMContext mpm, CeedElemRestriction *restriction_x_points);
RATEL_INTERN PetscErrorCode RatelMPMContextGetNumPointsLocal(RatelMPMContext mpm, CeedSize *num_points_local);
RATEL_INTERN PetscErrorCode RatelMPMContextCeedElemRestrictionCreateAtPoints(RatelMPMContext mpm, CeedInt num_comp, CeedElemRestriction *restriction);
RATEL_INTERN PetscErrorCode RatelMPMContextCeedOperatorSetPoints(RatelMPMContext mpm, CeedOperator op);

// DM setup
RATEL_INTERN PetscErrorCode RatelSolutionDMSetup_MPM(Ratel ratel, DM *dm_solution);
RATEL_INTERN PetscErrorCode RatelDMSetupByOrder_MPM(Ratel ratel, RatelMPMOptions mpm_options, CeedInt num_fields, const CeedInt *field_sizes,
                                                    const char **field_names, DM dm);

// MPM Utilities
RATEL_INTERN PetscErrorCode RatelMPMMigrate(Ratel ratel, Vec U, PetscReal time);
RATEL_INTERN PetscErrorCode RatelMPMSetPointFields(Ratel ratel);

// Geometry
RATEL_INTERN PetscErrorCode RatelMaterialSetupVolumeQData_MPM(RatelMaterial material, const char *label_name, PetscInt label_value,
                                                              CeedElemRestriction *restriction, CeedVector *q_data);

// Operators
// -- Material properties
RATEL_INTERN PetscErrorCode RatelMaterialSetupSetPointFieldsSuboperator_MPM(RatelMaterial material, CeedOperator op_set_point_fields);
RATEL_INTERN PetscErrorCode RatelMaterialSetPointFieldsVectors_MPM(RatelMaterial material);

// -- Residual and Jacobian
RATEL_INTERN PetscErrorCode RatelMaterialSetupResidualSuboperators_MPM(RatelMaterial material, CeedOperator op_residual_u,
                                                                       CeedOperator op_residual_ut, CeedOperator op_residual_utt);
RATEL_INTERN PetscErrorCode RatelMaterialSetupJacobianSuboperator_MPM(RatelMaterial material, CeedOperator op_residual_u, CeedOperator op_residual_ut,
                                                                      CeedOperator op_jacobian);
RATEL_INTERN PetscErrorCode RatelMaterialSetupForcingSuboperator_MPM(RatelMaterial material, CeedOperator op_residual);
RATEL_INTERN PetscErrorCode RatelMaterialSetupForcingEnergySuboperator_MPM(RatelMaterial material, DM dm_energy, CeedOperator op_external_energy);

// -- Block preconditioner for mixed-FEM
RATEL_INTERN PetscErrorCode RatelMaterialSetupJacobianBlockSuboperator_MPM(RatelMaterial material, DM dm, PetscInt field,
                                                                           CeedOperator op_jacobian_block);

// -- Output
RATEL_INTERN PetscErrorCode RatelMaterialSetupMMSErrorSuboperator_MPM(RatelMaterial material, CeedOperator op_mms_error);

// -- Projection
RATEL_INTERN PetscErrorCode RatelMaterialSetupMeshToSwarmSuboperators_MPM(RatelMaterial material, CeedInt field_index, CeedOperator op_mesh_to_swarm);
RATEL_INTERN PetscErrorCode RatelMaterialSetupSwarmToMeshSuboperators_MPM(RatelMaterial material, CeedOperator op_swarm_to_mesh,
                                                                          CeedOperator op_mass);

// -- Diagnostic
RATEL_INTERN PetscErrorCode RatelMaterialSetupStrainEnergySuboperator_MPM(RatelMaterial material, DM dm_energy, CeedOperator op_strain_energy);
RATEL_INTERN PetscErrorCode RatelMaterialSetupDiagnosticSuboperators_MPM(RatelMaterial material, DM dm_projected_diagnostic, DM dm_dual_diagnostic,
                                                                         CeedOperator op_mass_diagnostic, CeedOperator op_projected_diagnostic,
                                                                         CeedOperator op_dual_diagnostic, CeedOperator op_dual_nodal_scale);
