#pragma once

#include <petscsys.h>
#include <ratel-impl.h>
#include <ratel.h>

/// Monitor routine signature
/// @ingroup RatelInternal
typedef PetscErrorCode              RatelTSMonitorFunction(TS, PetscInt, PetscReal, Vec, void *);
RATEL_EXTERN RatelTSMonitorFunction RatelTSMonitorStrainEnergy;
RATEL_EXTERN RatelTSMonitorFunction RatelTSMonitorSurfaceForceCellToFace;
RATEL_EXTERN RatelTSMonitorFunction RatelTSMonitorSurfaceForce;
RATEL_EXTERN RatelTSMonitorFunction RatelTSMonitorSurfaceForcePerFace;
RATEL_EXTERN RatelTSMonitorFunction RatelTSMonitorDiagnosticQuantities;
RATEL_EXTERN RatelTSMonitorFunction RatelTSMonitorCheckpoint;
RATEL_EXTERN RatelTSMonitorFunction RatelTSMonitorSwarm;
RATEL_EXTERN RatelTSMonitorFunction RatelTSMonitorSwarmSolution;

// RatelCheckpointData
RATEL_INTERN PetscErrorCode RatelCheckpointDataRead(Ratel ratel, const char filename[], RatelCheckpointData *data);
RATEL_INTERN PetscErrorCode RatelCheckpointDataDestroy(RatelCheckpointData *data);

// RatelCheckpointCtx
RATEL_INTERN PetscErrorCode RatelCheckpointCtxCreateFromOptions(Ratel ratel, const char opt[], const char text[], RatelCheckpointCtx *ctx,
                                                                PetscBool *set);
RATEL_INTERN PetscErrorCode RatelCheckpointCtxCreate(Ratel ratel, PetscInt interval, PetscBool toggle, const char file_name[],
                                                     RatelCheckpointCtx *ctx);
RATEL_INTERN PetscErrorCode RatelCheckpointCtxDestroy(RatelCheckpointCtx *ctx);

// RatelViewer
RATEL_INTERN PetscErrorCode RatelViewerCreateFromOptions(Ratel ratel, const char[], const char[], RatelViewer *monitor_viewer, PetscBool *set);
RATEL_INTERN PetscErrorCode RatelViewerListCreateFromOptions(Ratel ratel, const char opt[], const char text[], RatelViewer monitor_viewers[],
                                                             PetscInt *num_viewers, PetscBool *set);
RATEL_INTERN PetscErrorCode RatelViewerCreate(Ratel ratel, PetscViewer viewer, PetscViewerFormat viewer_format, PetscInt interval,
                                              RatelViewer *monitor_viewer);
RATEL_INTERN PetscErrorCode RatelViewerDestroy(RatelViewer *monitor_viewer);
RATEL_INTERN PetscErrorCode RatelViewerWriteHeader(RatelViewer viewer, void *ctx);
RATEL_INTERN PetscErrorCode RatelViewerWriteFooter(RatelViewer viewer);
PETSC_INTERN PetscErrorCode RatelViewerGetSequenceFilename(RatelViewer ratel_viewer, PetscInt steps, PetscSizeT filename_size, char filename[]);
RATEL_INTERN PetscErrorCode RatelViewerShouldWrite(RatelViewer viewer, TS ts, PetscInt steps, PetscBool *should_write);

// RatelViewers
RATEL_INTERN PetscErrorCode RatelViewersCreateFromOptions(Ratel ratel, const char opt[], const char text[], PetscInt num_viewers, const char **names,
                                                          RatelViewers *viewer_list, PetscBool *set);
RATEL_INTERN PetscErrorCode RatelViewersCreate(Ratel ratel, PetscViewer viewer, PetscViewerFormat viewer_format, PetscInt interval,
                                               PetscInt num_viewers, const char **names, RatelViewers *viewer_list);
RATEL_INTERN PetscErrorCode RatelViewersDestroy(RatelViewers *viewer_list);
