#pragma once

#include <ceed.h>
#include <petscts.h>
#include <ratel-impl.h>
#include <ratel.h>

// Preconditioning
RATEL_INTERN PetscErrorCode RatelCreateSubmatrix(Mat A, IS is_row, IS is_col, MatReuse reuse_submatrix, Mat *A_sub);

// Solver callbacks
RATEL_INTERN PetscErrorCode RatelSNESFormResidual(SNES snes, Vec X, Vec Y, void *ctx_residual_u);
RATEL_INTERN PetscErrorCode RatelSNESFormJacobian(SNES snes, Vec X, Mat J, Mat J_pre, void *ctx_jacobian);
RATEL_INTERN PetscErrorCode RatelTSFormIResidual(TS ts, PetscReal time, Vec X, Vec X_t, Vec Y, void *ctx_residual_ut);
RATEL_INTERN PetscErrorCode RatelTSFormIJacobian(TS ts, PetscReal time, Vec X, Vec X_t, PetscReal v, Mat J, Mat J_pre, void *ctx_jacobian);
RATEL_INTERN PetscErrorCode RatelTSFormI2Residual(TS ts, PetscReal time, Vec X, Vec X_t, Vec X_tt, Vec Y, void *ctx_residual_utt);
RATEL_INTERN PetscErrorCode RatelTSFormI2Jacobian(TS ts, PetscReal time, Vec X, Vec X_t, Vec X_tt, PetscReal v, PetscReal a, Mat J, Mat J_pre,
                                                  void *ctx_jacobian);
RATEL_INTERN PetscErrorCode RatelTSPreStep(TS ts);
RATEL_INTERN PetscErrorCode RatelTSPreStage(TS ts, PetscScalar stage_time);
RATEL_INTERN PetscErrorCode RatelTSPostEvaluate(TS ts);
RATEL_INTERN PetscErrorCode RatelTSPostStep(TS ts);
RATEL_INTERN PetscErrorCode RatelSNESFormObjective(SNES snes, Vec X, PetscReal *merit, void *ctx);
