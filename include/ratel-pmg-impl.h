#pragma once

#include <ratel-impl.h>
#include <ratel.h>

/// Ratel pMG context, does rely upon Ratel DM setup
/// @struct RatelPMGContext
/// @ingroup RatelInternal
typedef struct RatelPMGContext_private *RatelPMGContext;
struct RatelPMGContext_private {
  Ratel                         ratel;
  PC                            pc_mg;
  PetscObjectState              fine_mat_state;
  RatelPMultigridCoarseningType p_multigrid_coarsening_type;
  PetscInt                      num_multigrid_levels, coarse_grid_order, multigrid_level_orders[RATEL_MAX_MULTIGRID_LEVELS];
  Mat                          *mats_operator, mat_operator_coarse, *mats_prolong_restrict;
};

RATEL_INTERN PetscErrorCode RatelPCPMGCreate(PC pc);
RATEL_INTERN PetscErrorCode RatelPCSetUp_PMG(PC pc);
RATEL_INTERN PetscErrorCode RatelPCView_PMG(PC pc, PetscViewer viewer);
RATEL_INTERN PetscErrorCode RatelPCApply_PMG(PC pc, Vec X_in, Vec X_out);
RATEL_INTERN PetscErrorCode RatelPCApplyTranspose_PMG(PC pc, Vec X_in, Vec X_out);
RATEL_INTERN PetscErrorCode RatelPCMatApply_PMG(PC pc, Mat X_in, Mat X_out);
RATEL_INTERN PetscErrorCode RatelPCDestroy_PMG(PC pc);
RATEL_INTERN PetscErrorCode RatelPCPMGContextDestroy(RatelPMGContext pmg);
