#pragma once

#include <ceed.h>
#include <petsc-ceed.h>
#include <petscdm.h>

/// libCEED evaluator object for applying composite `CeedOperator` on a `DM` to populate essential boundary values
/// @ingroup RatelBoundary
typedef struct _p_CeedBoundaryEvaluator *CeedBoundaryEvaluator;

PETSC_CEED_INTERN PetscErrorCode CeedBoundaryEvaluatorCreate(MPI_Comm comm, CeedOperator op_dirichlet, CeedBoundaryEvaluator *evaluator);
PETSC_CEED_INTERN PetscErrorCode CeedBoundaryEvaluatorView(CeedBoundaryEvaluator evaluator, PetscViewer viewer);
PETSC_CEED_INTERN PetscErrorCode CeedBoundaryEvaluatorDestroy(CeedBoundaryEvaluator *evaluator);

PETSC_CEED_INTERN PetscErrorCode CeedBoundaryEvaluatorSetTime(CeedBoundaryEvaluator evaluator, PetscReal time);
PETSC_CEED_INTERN PetscErrorCode CeedBoundaryEvaluatorGetTime(CeedBoundaryEvaluator evaluator, PetscReal *time);

PETSC_CEED_INTERN PetscErrorCode CeedBoundaryEvaluatorSetDt(CeedBoundaryEvaluator evaluator, PetscReal dt);
PETSC_CEED_INTERN PetscErrorCode CeedBoundaryEvaluatorGetDt(CeedBoundaryEvaluator evaluator, PetscReal *dt);

PETSC_CEED_EXTERN PetscErrorCode CeedBoundaryEvaluatorSetLogEvent(CeedBoundaryEvaluator evaluator, PetscLogEvent log_event);
PETSC_CEED_EXTERN PetscErrorCode CeedBoundaryEvaluatorGetLogEvent(CeedBoundaryEvaluator evaluator, PetscLogEvent *log_event);
PETSC_CEED_EXTERN PetscErrorCode CeedBoundaryEvaluatorSetCeedOperatorLogEvent(CeedBoundaryEvaluator evaluator, PetscLogEvent log_event);
PETSC_CEED_EXTERN PetscErrorCode CeedBoundaryEvaluatorGetCeedOperatorLogEvent(CeedBoundaryEvaluator evaluator, PetscLogEvent *log_event);

PETSC_CEED_INTERN PetscErrorCode DMPlexSetCeedBoundaryEvaluator(DM dm, CeedBoundaryEvaluator evaluator);
PETSC_CEED_INTERN PetscErrorCode DMPlexRemoveCeedBoundaryEvaluator(DM dm);
PETSC_CEED_INTERN PetscErrorCode DMPlexGetCeedBoundaryEvaluator(DM dm, CeedBoundaryEvaluator *evaluator);
