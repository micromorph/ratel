/// @file
/// Test MPM BPs

//TESTARGS(name="BP1, MPM") -quiet -ceed {ceed_resource} -options_file tests/ymls/ceed-bps-mpm.yml -model ceed-bp1 -mms_l2_atol 2e-2
//TESTARGS(name="BP2, MPM") -quiet -ceed {ceed_resource} -options_file tests/ymls/ceed-bps-mpm.yml -model ceed-bp2 -mms_l2_atol 5e-2
//TESTARGS(name="BP3, MPM") -quiet -ceed {ceed_resource} -options_file tests/ymls/ceed-bps-mpm.yml -model ceed-bp3 -mms_l2_atol 3e-2
//TESTARGS(name="BP4, MPM") -quiet -ceed {ceed_resource} -options_file tests/ymls/ceed-bps-mpm.yml -model ceed-bp4 -mms_l2_atol 9e-2

const char help[] = "Ratel - test case 050\n";

#include <ceed.h>
#include <petsc.h>
#include <ratel.h>
#include <stddef.h>

int main(int argc, char **argv) {
  MPI_Comm     comm;
  Ratel        ratel;
  SNES         snes;
  DM           dm_swarm, dm_mesh;
  Vec          U;
  PetscInt     num_fields;
  PetscScalar  tol = 2.8e-4;
  PetscScalar *l2_error;
  PetscBool    quiet = PETSC_FALSE;

  PetscCall(PetscInitialize(&argc, &argv, NULL, help));
  comm = PETSC_COMM_WORLD;
  PetscOptionsBegin(comm, NULL, "Ratel - test case 050", NULL);
  PetscCall(PetscOptionsBool("-quiet", "Suppress summary outputs", NULL, quiet, &quiet, NULL));
  PetscCall(PetscOptionsScalar("-mms_l2_atol", "L2 error tolerance", NULL, tol, &tol, NULL));
  PetscOptionsEnd();

  // Initialize Ratel context
  PetscCall(PetscOptionsInsertString(NULL, "-pc_type jacobi"));
  PetscCall(RatelInit(comm, &ratel));

  // Create DM
  PetscCall(RatelDMCreate(ratel, RATEL_SOLVER_STATIC, &dm_swarm));
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_mesh));

  // Create KSP
  PetscCall(SNESCreate(comm, &snes));
  PetscCall(RatelSNESSetup(ratel, snes));

  // Solution vector
  PetscCall(DMCreateGlobalVector(dm_mesh, &U));

  // Solve
  PetscCall(VecSet(U, 0.0));
  PetscCall(SNESSolve(snes, NULL, U));

  // Verify
  PetscCall(RatelComputeMMSL2Error(ratel, U, 1.0, &num_fields, &l2_error));
  // LCOV_EXCL_START
  if (PetscAbsReal(l2_error[0]) > tol) PetscCall(PetscPrintf(comm, "Error: L2 norm, field 0 = %0.5e\n", l2_error[0]));
  else if (!quiet) PetscCall(PetscPrintf(comm, "L2 norm, field 0 = %0.5e\n", l2_error[0]));
  for (PetscInt i = 1; i < num_fields; i++) {
    if (PetscAbsReal(l2_error[i]) > 10 * tol) PetscCall(PetscPrintf(comm, "Error: L2 norm, field %" PetscInt_FMT " = %0.5e\n", i, l2_error[i]));
    else if (!quiet) PetscCall(PetscPrintf(comm, "L2 norm, field %" PetscInt_FMT " = %0.5e\n", i, l2_error[i]));
  }
  PetscCall(VecViewFromOptions(U, NULL, "-vec_view"));
  // LCOV_EXCL_STOP

  // Cleanup
  PetscCall(PetscFree(l2_error));
  PetscCall(VecDestroy(&U));
  PetscCall(SNESDestroy(&snes));
  PetscCall(DMDestroy(&dm_mesh));
  PetscCall(DMDestroy(&dm_swarm));
  PetscCall(RatelDestroy(&ratel));

  return PetscFinalize();
}
