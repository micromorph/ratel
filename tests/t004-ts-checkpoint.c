/// @file
/// Test Ratel TS Checkpoint

//TESTARGS(name="ts-checkpoint")         -ceed {ceed_resource} -options_file tests/ymls/t004-ts-checkpoint.yml -ts_monitor_surface_force ascii:t004-ts-checkpoint-surface-forces_{ceed_resource}.csv -ts_monitor_checkpoint t004-ts-checkpoint_{ceed_resource} -ts_monitor_strain_energy ascii:t004-ts-checkpoint-strain-energy_{ceed_resource}.csv
//TESTARGS(name="ts-checkpoint-deleted") -ceed {ceed_resource} -remove_old_csv -options_file tests/ymls/t004-ts-checkpoint.yml -ts_monitor_surface_force ascii:t004-ts-checkpoint-surface-forces-deleted_{ceed_resource}.csv -ts_monitor_checkpoint t004-ts-checkpoint-deleted_{ceed_resource} -ts_monitor_strain_energy ascii:t004-ts-checkpoint-strain-energy-deleted_{ceed_resource}.csv

const char help[] = "Ratel - test case 004\n";

#include <ceed.h>
#include <petsc.h>
#include <petscoptions.h>
#include <ratel.h>
#include <stddef.h>
#include <unistd.h>

int main(int argc, char **argv) {
  MPI_Comm comm;

  PetscCall(PetscInitialize(&argc, &argv, NULL, help));
  comm = PETSC_COMM_WORLD;

  // Round 1: go from t=0.0 to t=0.5
  {
    // -- Initialize Ratel context
    Ratel ratel;
    TS    ts;
    DM    dm;
    Vec   U;
    PetscCall(RatelInit(comm, &ratel));

    // -- Create DM
    PetscCall(RatelDMCreate(ratel, RATEL_SOLVER_QUASISTATIC, &dm));

    // -- Create TS
    PetscCall(TSCreate(comm, &ts));
    PetscCall(RatelTSSetup(ratel, ts));
    PetscCall(TSSetTimeStep(ts, 0.1));
    PetscCall(TSSetMaxTime(ts, 0.5));
    PetscCall(TSSetExactFinalTime(ts, TS_EXACTFINALTIME_MATCHSTEP));

    // -- Print Ratel context
    PetscCall(RatelView(ratel, PETSC_VIEWER_STDOUT_WORLD));

    // -- Solution vector
    PetscCall(DMCreateGlobalVector(dm, &U));

    // -- Solve
    PetscCall(VecSet(U, 0.0));
    PetscCall(TSSolve(ts, U));

    // -- Cleanup
    PetscCall(TSDestroy(&ts));
    PetscCall(DMDestroy(&dm));
    PetscCall(VecDestroy(&U));
    PetscCall(RatelDestroy(&ratel));
  }

  {
    char      *ceed_resource = NULL, continue_file[PETSC_MAX_PATH_LEN], filename[PETSC_MAX_PATH_LEN];
    PetscInt   max_strings   = 1;
    PetscSizeT len;
    PetscBool  flg;
    PetscBool  remove_old_csv = PETSC_FALSE;

    PetscOptionsBegin(comm, NULL, "Ratel options", NULL);
    PetscCall(PetscOptionsStringArray("-ceed", "CEED resource specifier", NULL, &ceed_resource, &max_strings, &flg));
    PetscCall(PetscOptionsBool("-remove_old_csv", "Should the old CSV file be removed", NULL, remove_old_csv, &remove_old_csv, NULL));
    PetscOptionsEnd();

    PetscCall(PetscStrlen(ceed_resource, &len));

    for (PetscInt i = 0; i < len; i++) {
      if (ceed_resource[i] == '/') ceed_resource[i] = '-';
    }
    PetscCall(PetscSNPrintf(continue_file, PETSC_MAX_PATH_LEN, "-continue_file t004-ts-checkpoint%s_%s-2.bin", remove_old_csv ? "-deleted" : "",
                            flg ? ceed_resource : ""));
    PetscCall(PetscOptionsInsertString(NULL, continue_file));

    if (remove_old_csv) {
      PetscCall(PetscSNPrintf(filename, PETSC_MAX_PATH_LEN, "t004-ts-checkpoint-surface-forces-deleted_%s.csv", ceed_resource));
      unlink(filename);
      PetscCall(PetscSNPrintf(filename, PETSC_MAX_PATH_LEN, "t004-ts-checkpoint-strain-energy-deleted_%s.csv", ceed_resource));
      unlink(filename);
    }

    // Cleanup
    PetscFree(ceed_resource);
  }

  // Round 1: go from t=0.2 to t=1.0
  {
    // -- Initialize Ratel context
    Ratel ratel;
    TS    ts;
    DM    dm;
    Vec   U;

    PetscCall(RatelInit(comm, &ratel));

    // -- Create DM
    PetscCall(RatelDMCreate(ratel, RATEL_SOLVER_QUASISTATIC, &dm));

    // -- Create TS
    PetscCall(TSCreate(comm, &ts));
    PetscCall(RatelTSSetup(ratel, ts));
    PetscCall(TSSetTimeStep(ts, 0.1));
    PetscCall(TSSetMaxTime(ts, 1.0));
    PetscCall(TSSetExactFinalTime(ts, TS_EXACTFINALTIME_MATCHSTEP));

    // -- Print Ratel context
    PetscCall(RatelView(ratel, PETSC_VIEWER_STDOUT_WORLD));

    // -- Solution vector
    PetscCall(DMCreateGlobalVector(dm, &U));

    // -- Solve
    PetscCall(RatelTSSetupInitialCondition(ratel, ts, U));
    PetscCall(TSSolve(ts, U));

    // -- Cleanup
    PetscCall(TSDestroy(&ts));
    PetscCall(DMDestroy(&dm));
    PetscCall(VecDestroy(&U));
    PetscCall(RatelDestroy(&ratel));
  }

  return PetscFinalize();
}
