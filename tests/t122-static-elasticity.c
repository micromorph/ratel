/// @file
/// Test Ratel diagnostic face forces

//TESTARGS(name="linear, face forces")       -ceed {ceed_resource} -options_file tests/ymls/elasticity-linear-face-forces.yml -compute_cell_to_face
//TESTARGS(name="mixed linear, face forces") -ceed {ceed_resource} -options_file tests/ymls/elasticity-mixed-linear-face-forces-pcjacobi.yml

const char help[] = "Ratel - test case 122\n";

#include <petsc.h>
#include <ratel.h>
#include <stddef.h>

int main(int argc, char **argv) {
  MPI_Comm     comm;
  Ratel        ratel;
  SNES         snes;
  DM           dm;
  Vec          U;
  PetscBool    compute_cell_to_face = PETSC_FALSE;
  PetscScalar *surface_forces = NULL, *surface_forces_cell_to_face = NULL;
  PetscInt     num_faces, num_comp_u;
  const char **faces;

  PetscCall(PetscInitialize(&argc, &argv, NULL, help));
  comm = PETSC_COMM_WORLD;

  // Initialize Ratel context
  PetscCall(RatelInit(comm, &ratel));

  // Create DM
  PetscCall(RatelDMCreate(ratel, RATEL_SOLVER_STATIC, &dm));

  // Create SNES
  PetscCall(SNESCreate(comm, &snes));
  PetscCall(RatelSNESSetup(ratel, snes));

  // Solution vector
  PetscCall(DMCreateGlobalVector(dm, &U));

  // Solve
  PetscCall(VecSet(U, 0.0));
  PetscCall(SNESSolve(snes, NULL, U));

  // Check for cell to face  flag
  PetscOptionsBegin(comm, NULL, "Ratel testing option", NULL);
  PetscCall(PetscOptionsBool("-compute_cell_to_face", NULL, NULL, compute_cell_to_face, &compute_cell_to_face, NULL));
  PetscOptionsEnd();

  // Verify
  PetscCall(RatelGetSurfaceForceFaces(ratel, &num_faces, &faces));
  PetscCall(RatelComputeSurfaceForces(ratel, U, 1.0, &num_comp_u, &surface_forces));
  if (compute_cell_to_face) {
    PetscCall(RatelComputeSurfaceForcesCellToFace(ratel, U, 1.0, &num_comp_u, &surface_forces_cell_to_face));
  }
  for (PetscInt i = 0; i < num_faces; i++) {
    char        option_name[PETSC_MAX_OPTION_NAME];
    PetscInt    max_n = 3;
    PetscScalar applied_traction[max_n];

    PetscOptionsBegin(comm, NULL, "", NULL);
    PetscCall(PetscSNPrintf(option_name, sizeof(option_name), "-bc_traction_%s", faces[i]));
    PetscCall(PetscOptionsScalarArray(option_name, "Traction vector for constrained face", NULL, applied_traction, &max_n, NULL));
    PetscOptionsEnd();

    for (PetscInt j = 0; j < max_n; j++) {
      if (PetscAbsReal(applied_traction[j] + surface_forces[i * max_n + j]) > 5e-4) {
        // LCOV_EXCL_START
        PetscCall(PetscPrintf(comm, "Error: face %s, surface force component %" PetscInt_FMT " = %0.5e != %0.5e\n", faces[i], j,
                              surface_forces[i * max_n + j], applied_traction[j]));
        // LCOV_EXCL_STOP
      }
    }
    if (compute_cell_to_face) {
      for (PetscInt j = 0; j < max_n; j++) {
        if (PetscAbsReal(applied_traction[j] + surface_forces_cell_to_face[i * max_n + j]) > 5e-4) {
          // LCOV_EXCL_START
          PetscCall(PetscPrintf(comm, "Error: face %s, surface force (cell-to-face) component %" PetscInt_FMT " = %0.5e != %0.5e\n", faces[i], j,
                                surface_forces_cell_to_face[i * max_n + j], applied_traction[j]));
          // LCOV_EXCL_STOP
        }
      }
    }
  }

  // Cleanup
  PetscCall(PetscFree(surface_forces));
  PetscCall(PetscFree(surface_forces_cell_to_face));
  PetscCall(SNESDestroy(&snes));
  PetscCall(DMDestroy(&dm));
  PetscCall(VecDestroy(&U));
  PetscCall(RatelDestroy(&ratel));

  return PetscFinalize();
}
