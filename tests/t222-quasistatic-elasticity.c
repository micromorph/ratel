/// @file
/// Test TS monitor for Ratel

//TESTARGS(name="monitor per-face surface force, stdout") -ceed {ceed_resource} -options_file tests/ymls/t222-quasistatic-elasticity.yml -ts_monitor_surface_force_per_face
//TESTARGS(name="monitor per-face surface force, csv")    -ceed {ceed_resource} -options_file tests/ymls/t222-quasistatic-elasticity.yml
//TESTARGS(name="monitor swarm, xmf",only="cpu")          -ceed {ceed_resource} -options_file tests/ymls/t222-quasistatic-elasticity-mpm.yml

const char help[] = "Ratel - test case 222\n";

#include <petsc.h>
#include <ratel.h>
#include <stddef.h>

int main(int argc, char **argv) {
  MPI_Comm    comm;
  Ratel       ratel;
  TS          ts;
  DM          dm, dm_solution;
  Vec         U;
  PetscScalar strain_energy = 0.0, expected_strain_energy = 0.0;
  PetscBool   has_expected_strain_energy;

  PetscCall(PetscInitialize(&argc, &argv, NULL, help));
  comm = PETSC_COMM_WORLD;

  // Initialize Ratel context
  PetscCall(RatelInit(comm, &ratel));

  // Create DM
  PetscCall(RatelDMCreate(ratel, RATEL_SOLVER_QUASISTATIC, &dm));
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));

  // Create TS
  PetscCall(TSCreate(comm, &ts));
  PetscCall(RatelTSSetup(ratel, ts));
  PetscCall(TSSetTimeStep(ts, 0.5));
  PetscCall(TSSetMaxTime(ts, 1.0));

  // Solution vector
  PetscCall(DMCreateGlobalVector(dm_solution, &U));

  // Solve
  PetscCall(VecSet(U, 0.0));
  PetscCall(TSSolve(ts, U));

  // Verify
  PetscCall(RatelGetExpectedStrainEnergy(ratel, &has_expected_strain_energy, &expected_strain_energy));
  PetscCall(RatelComputeStrainEnergy(ratel, U, 1.0, &strain_energy));
  if (PetscAbsReal(strain_energy - expected_strain_energy) > 6e-3) {
    // LCOV_EXCL_START
    PetscCall(PetscPrintf(comm, "Error: strain energy error = %0.5e\n", PetscAbsReal(strain_energy - expected_strain_energy)));
    // LCOV_EXCL_STOP
  }

  // Cleanup
  PetscCall(TSDestroy(&ts));
  PetscCall(DMDestroy(&dm));
  PetscCall(DMDestroy(&dm_solution));
  PetscCall(VecDestroy(&U));
  PetscCall(RatelDestroy(&ratel));

  return PetscFinalize();
}
