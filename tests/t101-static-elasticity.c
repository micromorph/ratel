/// @file
/// Test Ratel DM initialization and destruction

//TESTARGS(name="Neo-Hookean")                     -ceed {ceed_resource} -options_file tests/ymls/elasticity-neo-hookean-initial.yml
//TESTARGS(name="Neo-Hookean, low-order")          -ceed {ceed_resource} -options_file tests/ymls/elasticity-neo-hookean-initial-low-order.yml
//TESTARGS(name="Neo-Hookean, Dirichlet boundary") -ceed {ceed_resource} -options_file tests/ymls/elasticity-neo-hookean-current-dirichlet-boundary.yml
//TESTARGS(name="Mooney-Rivlin")                   -ceed {ceed_resource} -options_file tests/ymls/elasticity-mooney-rivlin-initial.yml

const char help[] = "Ratel - test case 101\n";

#include <petsc.h>
#include <ratel.h>
#include <stddef.h>

int main(int argc, char **argv) {
  MPI_Comm comm;
  Ratel    ratel;
  DM       dm;

  PetscCall(PetscInitialize(&argc, &argv, NULL, help));
  comm = PETSC_COMM_WORLD;

  // Initialize Ratel context
  PetscCall(RatelInit(comm, &ratel));

  // Create DM
  PetscCall(RatelDMCreate(ratel, RATEL_SOLVER_STATIC, &dm));

  // Cleanup
  PetscCall(DMDestroy(&dm));
  PetscCall(RatelDestroy(&ratel));

  return PetscFinalize();
}
