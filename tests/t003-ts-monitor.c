/// @file
/// Test Ratel TS Monitor

//TESTARGS(name="ts-monitor")     -ceed {ceed_resource} -options_file tests/ymls/t003-ts-monitor.yml
//TESTARGS(name="ts-monitor-mpm") -ceed {ceed_resource} -options_file tests/ymls/t003-ts-monitor-mpm.yml

const char help[] = "Ratel - test case 003\n";

#include <petsc.h>
#include <ratel.h>
#include <stddef.h>

int main(int argc, char **argv) {
  MPI_Comm comm;
  Ratel    ratel;
  DM       dm;
  TS       ts;

  PetscCall(PetscInitialize(&argc, &argv, NULL, help));
  comm = PETSC_COMM_WORLD;

  // Initialize Ratel context
  PetscCall(RatelInit(comm, &ratel));

  // View
  PetscCall(RatelView(ratel, PETSC_VIEWER_STDOUT_WORLD));

  // Create DM
  PetscCall(RatelDMCreate(ratel, RATEL_SOLVER_QUASISTATIC, &dm));

  // Create TS
  PetscCall(TSCreate(comm, &ts));
  PetscCall(RatelTSSetup(ratel, ts));

  // Cleanup
  PetscCall(TSDestroy(&ts));
  PetscCall(DMDestroy(&dm));
  PetscCall(RatelDestroy(&ratel));

  return PetscFinalize();
}
