# Ratel Tests

This page provides a brief description of the tests for the Ratel library.

The tests are organized as follows:

0\. Ratel Context Tests\
1\. SNES Tests\
    0\. DM creation and setup tests\
    1\. SNESSolve with PCJacobi tests\
    2\. SNESSolve with PCpMG tests\
2\. TS Tests\
    1\. TSSolve with PCJacobi tests\
    2\. TSSolve with PCpMG tests

Tests ending in an even number use linear models and tests ending in an odd number use non-linear models.
