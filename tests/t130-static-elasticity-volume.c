/// @file
/// Test Ratel view

//TESTARGS(name="linear, volume, initial")                         -ceed {ceed_resource} -quiet -options_file tests/ymls/elasticity-linear-volume.yml
//TESTARGS(name="linear, volume, initial, order 2")                -ceed {ceed_resource} -quiet -options_file tests/ymls/elasticity-linear-volume.yml -diagnostic_order 2
//TESTARGS(name="linear, volume, initial, simplex")                -ceed {ceed_resource} -quiet -options_file tests/ymls/elasticity-linear-volume.yml -dm_plex_simplex 1
//TESTARGS(name="linear, volume, initial, simplex, order 2")       -ceed {ceed_resource} -quiet -options_file tests/ymls/elasticity-linear-volume.yml -dm_plex_simplex 1  -diagnostic_order 2
//TESTARGS(name="linear, volume, initial, cylinder")               -ceed {ceed_resource} -quiet -options_file tests/ymls/elasticity-linear-volume-cylinder.yml
//TESTARGS(name="linear, volume, initial, sphere, simplex")        -ceed {ceed_resource} -quiet -options_file tests/ymls/elasticity-linear-volume-sphere.yml
//TESTARGS(name="linear damage, volume, initial, sphere, simplex") -ceed {ceed_resource} -quiet -options_file tests/ymls/elasticity-linear-damage-volume-sphere.yml

const char help[] = "Ratel - test case 001\n";

#include <petsc.h>
#include <ratel.h>
#include <stddef.h>

int main(int argc, char **argv) {
  MPI_Comm comm;
  Ratel    ratel;
  DM       dm;
  Vec      U;

  PetscCall(PetscInitialize(&argc, &argv, NULL, help));
  comm = PETSC_COMM_WORLD;

  // Initialize Ratel context
  PetscCall(PetscOptionsInsertString(NULL, "-pc_type jacobi"));
  PetscCall(RatelInit(comm, &ratel));

  // Create DM
  PetscCall(RatelDMCreate(ratel, RATEL_SOLVER_STATIC, &dm));

  // Solution vector
  PetscCall(DMCreateGlobalVector(dm, &U));
  PetscCall(VecSet(U, 0.0));

  // Verify
  PetscCall(RatelViewVolumeErrorFromOptions(ratel, 1.0, U));
  PetscCall(RatelViewDiagnosticQuantitiesFromOptions(ratel, 1.0, U));

  // Cleanup
  PetscCall(DMDestroy(&dm));
  PetscCall(VecDestroy(&U));
  PetscCall(RatelDestroy(&ratel));

  return PetscFinalize();
}
