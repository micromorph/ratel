/// @file
/// Test Ratel TS Monitor

//TESTARGS(name="Quasistatic, ts-monitor")     -ceed {ceed_resource} -options_file tests/ymls/t003-ts-monitor.yml
//TESTARGS(name="Quasistatic, ts-monitor-mpm") -ceed {ceed_resource} -options_file tests/ymls/t003-ts-monitor-mpm.yml

const char help[] = "Ratel - test case 203 - ts_monitor\n";

#include <petsc.h>
#include <ratel.h>
#include <stddef.h>

int main(int argc, char **argv) {
  MPI_Comm comm;
  Ratel    ratel;
  DM       dm, dm_solution;
  TS       ts;
  Vec      U;

  PetscCall(PetscInitialize(&argc, &argv, NULL, help));
  comm = PETSC_COMM_WORLD;

  // Initialize Ratel context
  PetscCall(PetscOptionsInsertString(NULL, "-pc_type jacobi"));
  PetscCall(RatelInit(comm, &ratel));

  // Create DM
  PetscCall(RatelDMCreate(ratel, RATEL_SOLVER_QUASISTATIC, &dm));
  PetscCall(RatelGetSolutionMeshDM(ratel, &dm_solution));

  // Create TS
  PetscCall(TSCreate(comm, &ts));
  PetscCall(RatelTSSetup(ratel, ts));
  PetscCall(TSSetMaxTime(ts, 1.0));

  // Solution vector
  PetscCall(DMCreateGlobalVector(dm_solution, &U));

  // Solve
  PetscCall(VecSet(U, 0.0));
  PetscCall(TSSolve(ts, U));

  // Verify
  PetscCall(RatelViewStrainEnergyErrorFromOptions(ratel, 1.0, U));

  // Cleanup
  PetscCall(TSDestroy(&ts));
  PetscCall(DMDestroy(&dm));
  PetscCall(DMDestroy(&dm_solution));
  PetscCall(VecDestroy(&U));
  PetscCall(RatelDestroy(&ratel));

  return PetscFinalize();
}
