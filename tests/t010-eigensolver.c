/// @file
/// Test eigensolver

//TESTARGS(name="direct eigensolver")

const char help[] = "Ratel - test case 010\n";

#include <ceed.h>
#include <petsc.h>
#include <ratel.h>
#include <stddef.h>

#include "../include/ratel/qfunctions/utils.h"

PetscErrorCode TestEigensystem(MPI_Comm comm, const CeedScalar A_sym[6], CeedScalar e_vals[3], CeedScalar e_vecs[3][3]);

int main(int argc, char **argv) {
  MPI_Comm comm;

  PetscCall(PetscInitialize(&argc, &argv, NULL, help));
  comm = PETSC_COMM_WORLD;

  // Zero matrix
  const CeedScalar E0_sym[6] = {0., 0., 0., 0., 0., 0.};
  CeedScalar       e0_vals[3], e0_vecs[3][3];
  RatelMatComputeEigensystemSymmetric(E0_sym, e0_vals, e0_vecs);
  PetscCall(TestEigensystem(comm, E0_sym, e0_vals, e0_vecs));

  // Diagonal matrix
  const CeedScalar Ed_sym1[6] = {-1., 0., -1.0001, 0., 0., 0.};
  CeedScalar       ed_vals1[3], ed_vecs1[3][3];
  RatelMatComputeEigensystemSymmetric(Ed_sym1, ed_vals1, ed_vecs1);
  PetscCall(TestEigensystem(comm, Ed_sym1, ed_vals1, ed_vecs1));

  // Diagonal matrix with equal entries
  const CeedScalar Ed_sym2[6] = {1., 1., 1., 0., 0., 0.};
  CeedScalar       ed_vals2[3], ed_vecs2[3][3];
  RatelMatComputeEigensystemSymmetric(Ed_sym2, ed_vals2, ed_vecs2);
  PetscCall(TestEigensystem(comm, Ed_sym2, ed_vals2, ed_vecs2));

  // Matrix with one eigenvalue close to 0
  const CeedScalar E_sym[6] = {1.6420402029352883,  -0.0000000000000002, -0.0000000000000002,
                               -0.0000000000000002, 0.3823790007724444,  0.3823790007724444};
  CeedScalar       e_vals[3], e_vecs[3][3];
  RatelMatComputeEigensystemSymmetric(E_sym, e_vals, e_vecs);
  PetscCall(TestEigensystem(comm, E_sym, e_vals, e_vecs));

  // Matrix with two eigenvalues close to 0
  const CeedScalar E2_sym[6] = {-0.2141177854895470, -0.2141177800305910, -0.2141177874323701,
                                -0.2141177834585880, -0.2141177837932527, -0.2141177783336952};
  CeedScalar       e2_vals[3], e2_vecs[3][3];
  RatelMatComputeEigensystemSymmetric(E2_sym, e2_vals, e2_vecs);
  PetscCall(TestEigensystem(comm, E2_sym, e2_vals, e2_vecs));

  // Matrix with large diagonal value and small off diagonal
  const CeedScalar E3_sym[6] = {100.0, 101.0, 102.0, 0., 0.1, 0.};
  CeedScalar       e3_vals[3], e3_vecs[3][3];
  RatelMatComputeEigensystemSymmetric(E3_sym, e3_vals, e3_vecs);
  PetscCall(TestEigensystem(comm, E3_sym, e3_vals, e3_vecs));

  return PetscFinalize();
}

// -----------------------------------------------------------------------------
// Test computed eigenvalues and eigenvectors of a symmetric matrix A_sym
// -----------------------------------------------------------------------------
PetscErrorCode TestEigensystem(MPI_Comm comm, const CeedScalar A_sym[6], CeedScalar e_vals[3], CeedScalar e_vecs[3][3]) {
  PetscFunctionBeginUser;
  for (PetscInt i = 0; i < 3; i++) {
    CeedScalar       B[3][3], v[3];
    const CeedScalar B_sym[6] = {A_sym[0] - e_vals[i], A_sym[1] - e_vals[i], A_sym[2] - e_vals[i], A_sym[3], A_sym[4], A_sym[5]};

    RatelSymmetricMatUnpack(B_sym, B);
    RatelMatVecMult(1.0, B, e_vecs[i], v);

    for (PetscInt j = 0; j < 3; j++) {
      if (fabs(v[j]) > 500 * RATEL_EPSILON_DOUBLE || isnan(v[j])) {
        // LCOV_EXCL_START
        PetscCall(
            PetscPrintf(comm, "v = (A - e_vals[%" PetscInt_FMT "] I) u[%" PetscInt_FMT "] = 0\nv[%" PetscInt_FMT "] = %12.16f\n", i, i, j, v[j]));
        PetscCall(PetscPrintf(comm, "  A:\n"));
        PetscCall(PetscPrintf(comm, "    %020.16f %020.16f %020.16f\n", A_sym[0], A_sym[5], A_sym[4]));
        PetscCall(PetscPrintf(comm, "    %020.16f %020.16f %020.16f\n", A_sym[5], A_sym[1], A_sym[3]));
        PetscCall(PetscPrintf(comm, "    %020.16f %020.16f %020.16f\n", A_sym[4], A_sym[3], A_sym[2]));
        PetscCall(PetscPrintf(comm, "  Eigenvalue:\n"));
        PetscCall(PetscPrintf(comm, "    %020.16f\n", e_vals[j]));
        PetscCall(PetscPrintf(comm, "  Eigenvector:\n"));
        PetscCall(PetscPrintf(comm, "    %020.16f %020.16f %020.16f\n", e_vecs[0][j], e_vecs[1][j], e_vecs[2][j]));
        // LCOV_EXCL_STOP
      }
    }
  }
  PetscFunctionReturn(PETSC_SUCCESS);
}
