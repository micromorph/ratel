/// @file
/// Test TS create and solve with PCJacobi from Ratel DM

//TESTARGS(name="Neo-Hookean")                                    -ceed {ceed_resource} -options_file tests/ymls/elasticity-neo-hookean-initial.yml
//TESTARGS(name="Neo-Hookean, low-order")                         -ceed {ceed_resource} -options_file tests/ymls/elasticity-neo-hookean-initial-low-order.yml
//TESTARGS(name="Neo-Hookean initial Enzyme-AD",only="ad-enzyme") -ceed {ceed_resource} -options_file tests/ymls/elasticity-neo-hookean-initial-ad-enzyme.yml
//TESTARGS(name="Neo-Hookean current Enzyme-AD",only="ad-enzyme") -ceed {ceed_resource} -options_file tests/ymls/elasticity-neo-hookean-current-ad-enzyme.yml
//TESTARGS(name="Neo-Hookean initial ADOL-C",only="ad-adolc")     -ceed {ceed_resource} -options_file tests/ymls/elasticity-neo-hookean-initial-ad-adolc.yml
//TESTARGS(name="Neo-Hookean current ADOL-C",only="ad-adolc")     -ceed {ceed_resource} -options_file tests/ymls/elasticity-neo-hookean-current-ad-adolc.yml
//TESTARGS(name="Neo-Hookean, Dirichlet boundary")                -ceed {ceed_resource} -options_file tests/ymls/elasticity-neo-hookean-current-dirichlet-boundary.yml
//TESTARGS(name="Mooney-Rivlin")                                  -ceed {ceed_resource} -options_file tests/ymls/elasticity-mooney-rivlin-initial.yml
//TESTARGS(name="Mixed Neo-Hookean, PCJacobi")                    -ceed {ceed_resource} -options_file tests/ymls/elasticity-mixed-neo-hookean-initial-pcjacobi.yml

const char help[] = "Ratel - test case 211\n";

#include <petsc.h>
#include <ratel.h>
#include <stddef.h>

int main(int argc, char **argv) {
  MPI_Comm    comm;
  Ratel       ratel;
  TS          ts;
  DM          dm;
  Vec         U;
  PetscScalar strain_energy = 0.0, expected_strain_energy = 0.0;
  PetscBool   has_expected_strain_energy;

  PetscCall(PetscInitialize(&argc, &argv, NULL, help));
  comm = PETSC_COMM_WORLD;

  // Initialize Ratel context
  PetscCall(PetscOptionsInsertString(NULL, "-pc_type jacobi"));
  PetscCall(RatelInit(comm, &ratel));

  // Create DM
  PetscCall(RatelDMCreate(ratel, RATEL_SOLVER_QUASISTATIC, &dm));

  // Create TS
  PetscCall(TSCreate(comm, &ts));
  PetscCall(RatelTSSetup(ratel, ts));
  PetscCall(TSSetTimeStep(ts, 0.5));
  PetscCall(TSSetMaxTime(ts, 1.0));

  // Solution vector
  PetscCall(DMCreateGlobalVector(dm, &U));

  // Solve
  PetscCall(VecSet(U, 0.0));
  PetscCall(TSSolve(ts, U));

  // Verify
  PetscCall(RatelGetExpectedStrainEnergy(ratel, &has_expected_strain_energy, &expected_strain_energy));
  PetscCall(RatelComputeStrainEnergy(ratel, U, 1.0, &strain_energy));
  if (PetscAbsReal(strain_energy - expected_strain_energy) > 6e-3) {
    // LCOV_EXCL_START
    PetscCall(PetscPrintf(comm, "Error: strain energy error = %0.5e\n", PetscAbsReal(strain_energy - expected_strain_energy)));
    // LCOV_EXCL_STOP
  }

  // Cleanup
  PetscCall(TSDestroy(&ts));
  PetscCall(DMDestroy(&dm));
  PetscCall(VecDestroy(&U));
  PetscCall(RatelDestroy(&ratel));

  return PetscFinalize();
}
