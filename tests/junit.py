#!/usr/bin/env python3
from junit_common import *


def create_argparser() -> argparse.ArgumentParser:
    """Creates argument parser to read command line arguments

    Returns:
        argparse.ArgumentParser: Created `ArgumentParser`
    """
    parser = argparse.ArgumentParser('Test runner with JUnit and TAP output')
    parser.add_argument(
        '-c',
        '--ceed-backends',
        type=str,
        nargs='*',
        default=['/cpu/self'],
        help='libCEED backend')
    parser.add_argument('-e', '--enzyme-lib', type=str, default='',
                        help='path to Enzyme library, if used to build Ratel')
    parser.add_argument('-a', '--with-adolc', type=int, default=None,
                        help='1, if used to build Ratel')
    parser.add_argument(
        '-m',
        '--mode',
        type=RunMode,
        action=CaseInsensitiveEnumAction,
        help='Output mode, junit or tap',
        default=RunMode.JUNIT)
    parser.add_argument('-n', '--nproc', type=int, default=1, help='number of MPI processes')
    parser.add_argument('-o', '--output', help='Output file to write test', default=None)
    parser.add_argument('-b', '--junit-batch', type=str, default='', help='Name of JUnit batch for output file')
    parser.add_argument('-p', '--petsc-arch', type=str, default='', help='PETSc arch name used to build Ratel')
    parser.add_argument('-np', '--pool-size', type=int, default=1, help='Number of test cases to run in parallel')
    parser.add_argument('test', help='Test executable', nargs='?')

    return parser


class RatelSuiteSpec(SuiteSpec):
    def __init__(self, enzyme_lib: str, with_adolc: int, petsc_arch: str, cgns_tol: float):
        """Initialize `RatelSuiteSpec`

        Args:
            enzyme_lib (str): Path to Enzyme library used to build Ratel
            with_adolc (int): 1, if ADOL-C is used to build Ratel
            petsc_arch (str): PETSc arch name used to build Ratel
        """
        self.petsc_arch: str = petsc_arch
        self.has_enzyme: bool = enzyme_lib and Path(enzyme_lib).is_file()
        self.has_adolc: bool = with_adolc and (with_adolc == 1)
        self.has_cgnsdiff: bool = has_cgnsdiff()
        self.cgns_tol: float = cgns_tol

    def get_source_path(self, test: str) -> Path:
        """Compute path to test source file

        Args:
            test (str): Name of test

        Returns:
            Path: Path to source file
        """
        if 'convergence' in test:
            return Path('examples') / test
        elif test.startswith('ex'):
            return (Path('examples') / test).with_suffix('.c')
        else:
            return (Path('tests') / test).with_suffix('.c')

    def get_run_path(self, test: str) -> Path:
        """Compute path to built test executable file

        Args:
            test (str): Name of test

        Returns:
            Path: Path to test executable
        """
        if 'convergence' in test:
            return Path('examples') / test
        else:
            return Path('build') / test

    def get_output_path(self, test: str, output_file: str) -> Path:
        """Compute path to expected output file

        Args:
            test (str): Name of test
            output_file (str): File name of output file

        Returns:
            Path: Path to expected output file
        """
        if test.startswith('ex'):
            return Path('examples') / 'output' / output_file
        else:
            return Path('tests') / 'output' / output_file

    def check_pre_skip(self, test: str, spec: TestSpec, resource: str, nproc: int) -> Optional[str]:
        """Check if a test case should be skipped prior to running, returning the reason for skipping

        Args:
            test (str): Name of test
            spec (TestSpec): Test case specification
            resource (str): libCEED backend
            nproc (int): Number of MPI processes to use when running test case

        Returns:
            Optional[str]: Skip reason, or `None` if test case should not be skipped
        """
        for condition in spec.only:
            if (condition == 'cpu') and ('gpu' in resource):
                return 'CPU-only test with GPU backend'
            if (condition == 'gpu') and ('cpu' in resource):
                return 'GPU-only test with CPU backend'
            if (condition == 'serial') and (int(nproc) > 1):
                return 'serial test with nproc > 1'
            if (condition == 'parallel') and (int(nproc) == 1):
                return 'parallel test with nproc = 1'
            if (condition == 'ad-enzyme') and (not self.has_enzyme):
                return 'Enzyme-AD test with ENZYME_LIB unset'
            if (condition == 'ad-enzyme') and ('gpu' in resource):
                return 'Enzyme-AD test with GPU backend'
            if (condition == 'ad-adolc') and (not self.has_adolc):
                return 'ADOL-C test with WITH_ADOLC not set to 1'
            if (condition == 'ad-adolc') and ('gpu' in resource):
                return 'ADOL-C test with GPU backend'
            if (condition == 'cgnsdiff') and ('serial' in self.petsc_arch):
                return 'cgns viewer not installed in PETSc arch'
            if (condition == 'cgnsdiff') and (not self.has_cgnsdiff):
                return 'cgnsdiff not installed'
        return ''

    def check_post_skip(self, test: str, spec: TestSpec, resource: str, stderr: str) -> Optional[str]:
        """Check if a test case should be allowed to fail, based on its stderr output

        Args:
            test (str): Name of test
            spec (TestSpec): Test case specification
            resource (str): libCEED backend
            stderr (str): Standard error output from test case execution

        Returns:
            Optional[str]: Skip reason, or `None` if unexpected error
        """
        if 'Backend does not implement' in stderr:
            return f'libCEED backend {resource} does not implement all methods for {test}, {spec.name}'
        elif 'PETSC ERROR: Model data for' in stderr:
            return f'Material model dependencies not installed for {test}, {spec.name}'
        elif 'You may need to add --download-ctetgen or --download-tetgen' in stderr:
            return f'Tet mesh generator not installed for {test}, {spec.name}'
        return None

    def post_test_hook(self, test: str, spec: TestSpec) -> None:
        """Clean up output after t003

        Args:
            test (str): Name of test
            spec (TestSpec): Test case specification
        """
        if test[:4] == 't003':
            # remove generated files
            for ext in ['bin', 'info', 'vtu', 'series', 'csv', 'pbin', 'xmf', 'txt']:
                files = Path.cwd().glob(f't003-*.{ext}')
                for f in files:
                    f.unlink(missing_ok=True)
        if test[:4] == 't004':
            # remove generated files
            for ext in ['bin', 'info', 'vtu', 'series']:
                files = Path.cwd().glob(f't004-*.{ext}')
                for f in files:
                    f.unlink(missing_ok=True)
            files = Path.cwd().glob(f't004-*-surface-force-face*.csv')
            for f in files:
                f.unlink(missing_ok=True)
        if test[:4] == 't203':
            # remove generated files
            for ext in ['bin', 'info', 'vtu', 'series', 'csv', 'pbin', 'xmf', 'txt']:
                files = Path.cwd().glob(f't003-*.{ext}')
                for f in files:
                    f.unlink(missing_ok=True)
        if test[:4] == 't222':
            # remove generated files
            for ext in ['csv', 'pbin', 'xmf']:
                files = Path.cwd().glob(f't222-*.{ext}')
                for f in files:
                    f.unlink(missing_ok=True)

    def check_allowed_stdout(self, test: str) -> bool:
        """Check whether a test is allowed to print console output

        Args:
            test (str): Name of test

        Returns:
            bool: True if the test is allowed to print console output
        """
        return test[:4] in ['t001', 't002', 't003', 't004', 't203', 't222']


# main
if __name__ == '__main__':
    args = create_argparser().parse_args()

    # run tests
    result: TestSuite = run_tests(
        args.test,
        args.ceed_backends,
        args.mode,
        args.nproc,
        RatelSuiteSpec(
            args.enzyme_lib,
            args.with_adolc,
            args.petsc_arch,
            1.0e-10),
        args.pool_size)

    # write output and check for failures
    if args.mode is RunMode.JUNIT:
        write_junit_xml(result, args.output, args.junit_batch)
        if has_failures(result):
            sys.exit(1)
