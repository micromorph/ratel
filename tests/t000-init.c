/// @file
/// Test Ratel object initialization and destruction

//TESTARGS(name="initalize, FEM") -ceed {ceed_resource} -options_file tests/ymls/t000-init.yml -method fem
//TESTARGS(name="initalize, MPM") -ceed {ceed_resource} -options_file tests/ymls/t000-init.yml -method mpm

const char help[] = "Ratel - test case 000\n";

#include <petsc.h>
#include <ratel.h>
#include <stddef.h>

int main(int argc, char **argv) {
  MPI_Comm comm;
  Ratel    ratel;

  PetscCall(PetscInitialize(&argc, &argv, NULL, help));
  comm = PETSC_COMM_WORLD;

  // Initialize and destroy Ratel context
  PetscCall(RatelInit(comm, &ratel));
  PetscCall(RatelDestroy(&ratel));

  return PetscFinalize();
}
