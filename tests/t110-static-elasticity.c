/// @file
/// Test SNES create and solve with PCJacobi from Ratel DM

//TESTARGS(name="linear, hex")                   -ceed {ceed_resource} -options_file tests/ymls/elasticity-linear.yml
//TESTARGS(name="linear, simplex")               -ceed {ceed_resource} -options_file tests/ymls/elasticity-linear-simplex.yml
//TESTARGS(name="mixed linear, PCJacobi")        -ceed {ceed_resource} -options_file tests/ymls/elasticity-mixed-linear-pcjacobi.yml
//TESTARGS(name="linear, quadratic coordinates") -ceed {ceed_resource} -options_file tests/ymls/elasticity-linear-quad-coords.yml
//TESTARGS(name="linear, cubic coordinates")     -ceed {ceed_resource} -options_file tests/ymls/elasticity-linear-cubic-coords.yml

const char help[] = "Ratel - test case 110\n";

#include <petsc.h>
#include <ratel.h>
#include <stddef.h>

int main(int argc, char **argv) {
  MPI_Comm     comm;
  Ratel        ratel;
  SNES         snes;
  DM           dm;
  Vec          U;
  PetscInt     num_fields;
  PetscScalar *l2_error;

  PetscCall(PetscInitialize(&argc, &argv, NULL, help));
  comm = PETSC_COMM_WORLD;

  // Initialize Ratel context
  PetscCall(PetscOptionsInsertString(NULL, "-pc_type jacobi"));
  PetscCall(RatelInit(comm, &ratel));

  // Create DM
  PetscCall(RatelDMCreate(ratel, RATEL_SOLVER_STATIC, &dm));

  // Create SNES
  PetscCall(SNESCreate(comm, &snes));
  PetscCall(RatelSNESSetup(ratel, snes));

  // Solution vector
  PetscCall(DMCreateGlobalVector(dm, &U));

  // Solve
  PetscCall(VecSet(U, 0.0));
  PetscCall(SNESSolve(snes, NULL, U));

  // Verify
  PetscCall(RatelComputeMMSL2Error(ratel, U, 1.0, &num_fields, &l2_error));
  if (PetscAbsReal(l2_error[0]) > 2.8e-4) PetscCall(PetscPrintf(comm, "Error: L2 norm, field 0 = %0.5e\n", l2_error[0]));
  for (PetscInt i = 1; i < num_fields; i++) {
    if (PetscAbsReal(l2_error[i]) > 2.8e-3) PetscCall(PetscPrintf(comm, "Error: L2 norm, field %" PetscInt_FMT " = %0.5e\n", i, l2_error[i]));
  }

  // Cleanup
  PetscCall(PetscFree(l2_error));
  PetscCall(SNESDestroy(&snes));
  PetscCall(DMDestroy(&dm));
  PetscCall(VecDestroy(&U));
  PetscCall(RatelDestroy(&ratel));

  return PetscFinalize();
}
