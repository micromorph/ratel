/// @file
/// Test Ratel DM initialization and destruction

//TESTARGS(name="linear, hex")                   -ceed {ceed_resource} -options_file tests/ymls/elasticity-linear.yml
//TESTARGS(name="linear, simplex")               -ceed {ceed_resource} -options_file tests/ymls/elasticity-linear-simplex.yml
//TESTARGS(name="linear, quadratic coordinates") -ceed {ceed_resource} -options_file tests/ymls/elasticity-linear-quad-coords.yml
//TESTARGS(name="linear, cubic coordinates")     -ceed {ceed_resource} -options_file tests/ymls/elasticity-linear-cubic-coords.yml

const char help[] = "Ratel - test case 100\n";

#include <petsc.h>
#include <ratel.h>
#include <stddef.h>

int main(int argc, char **argv) {
  MPI_Comm comm;
  Ratel    ratel;
  DM       dm;

  PetscCall(PetscInitialize(&argc, &argv, NULL, help));
  comm = PETSC_COMM_WORLD;

  // Initialize Ratel context
  PetscCall(RatelInit(comm, &ratel));

  // Create DM
  PetscCall(RatelDMCreate(ratel, RATEL_SOLVER_STATIC, &dm));

  // Cleanup
  PetscCall(DMDestroy(&dm));
  PetscCall(RatelDestroy(&ratel));

  return PetscFinalize();
}
