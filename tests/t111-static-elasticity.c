/// @file
/// Test SNES create and solve from Ratel DM

//TESTARGS(name="Neo-Hookean")                     -ceed {ceed_resource} -options_file tests/ymls/elasticity-neo-hookean-initial.yml
//TESTARGS(name="Neo-Hookean, low-order")          -ceed {ceed_resource} -options_file tests/ymls/elasticity-neo-hookean-initial-low-order.yml
//TESTARGS(name="Neo-Hookean, low-order shell")    -ceed {ceed_resource} -options_file tests/ymls/elasticity-neo-hookean-initial-low-order-shell.yml
//TESTARGS(name="Neo-Hookean, Dirichlet boundary") -ceed {ceed_resource} -options_file tests/ymls/elasticity-neo-hookean-current-dirichlet-boundary.yml
//TESTARGS(name="Mooney-Rivlin")                   -ceed {ceed_resource} -options_file tests/ymls/elasticity-mooney-rivlin-initial.yml

const char help[] = "Ratel - test case 111\n";

#include <petsc.h>
#include <ratel.h>
#include <stddef.h>

int main(int argc, char **argv) {
  MPI_Comm    comm;
  Ratel       ratel;
  SNES        snes;
  DM          dm;
  Vec         U;
  PetscScalar strain_energy = 0.0, expected_strain_energy = 0.0;
  PetscBool   has_expected_strain_energy;

  PetscCall(PetscInitialize(&argc, &argv, NULL, help));
  comm = PETSC_COMM_WORLD;

  // Initialize Ratel context
  PetscCall(PetscOptionsInsertString(NULL, "-pc_type jacobi"));
  PetscCall(RatelInit(comm, &ratel));

  // Create DM
  PetscCall(RatelDMCreate(ratel, RATEL_SOLVER_STATIC, &dm));

  // Create SNES
  PetscCall(SNESCreate(comm, &snes));
  PetscCall(RatelSNESSetup(ratel, snes));

  // Solution vector
  PetscCall(DMCreateGlobalVector(dm, &U));

  // Solve
  PetscCall(VecSet(U, 0.0));
  PetscCall(SNESSolve(snes, NULL, U));

  // Verify
  PetscCall(RatelGetExpectedStrainEnergy(ratel, &has_expected_strain_energy, &expected_strain_energy));
  PetscCall(RatelComputeStrainEnergy(ratel, U, 1.0, &strain_energy));
  if (PetscAbsReal(strain_energy - expected_strain_energy) > 6e-3) {
    // LCOV_EXCL_START
    PetscCall(PetscPrintf(comm, "Error: strain energy error = %0.05e\n", PetscAbsReal(strain_energy - expected_strain_energy)));
    // LCOV_EXCL_STOP
  }

  // Cleanup
  PetscCall(SNESDestroy(&snes));
  PetscCall(DMDestroy(&dm));
  PetscCall(VecDestroy(&U));
  PetscCall(RatelDestroy(&ratel));

  return PetscFinalize();
}
