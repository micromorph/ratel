/// @file
/// Test Ratel view

//TESTARGS(name="view FEM, 3 component models") -ceed {ceed_resource} -options_file tests/ymls/t001-view-3-comp.yml -model fem
//TESTARGS(name="view FEM, 4 component models") -ceed {ceed_resource} -options_file tests/ymls/t001-view-4-comp.yml -model fem
//TESTARGS(name="view MPM, 3 component models") -ceed {ceed_resource} -options_file tests/ymls/t001-view-3-comp.yml -model mpm

const char help[] = "Ratel - test case 001\n";

#include <petsc.h>
#include <ratel.h>
#include <stddef.h>

int main(int argc, char **argv) {
  MPI_Comm comm;
  Ratel    ratel;

  PetscCall(PetscInitialize(&argc, &argv, NULL, help));
  comm = PETSC_COMM_WORLD;

  // Initialize Ratel context
  PetscCall(RatelInit(comm, &ratel));

  // View
  PetscCall(RatelView(ratel, PETSC_VIEWER_STDOUT_WORLD));

  // Cleanup
  PetscCall(RatelDestroy(&ratel));

  return PetscFinalize();
}
