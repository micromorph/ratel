/// @file
/// Test Ratel view with DM setup

//TESTARGS(name="view, FEM") -ceed {ceed_resource} -options_file tests/ymls/t002-view.yml -method fem
//TESTARGS(name="view, MPM") -ceed {ceed_resource} -options_file tests/ymls/t002-view.yml -method mpm -mpm_point_location_type cell_random

const char help[] = "Ratel - test case 002\n";

#include <petsc.h>
#include <ratel.h>
#include <stddef.h>

int main(int argc, char **argv) {
  MPI_Comm comm;
  Ratel    ratel;
  DM       dm;

  PetscCall(PetscInitialize(&argc, &argv, NULL, help));
  comm = PETSC_COMM_WORLD;

  // Initialize Ratel context
  PetscCall(RatelInit(comm, &ratel));

  // Create DM
  PetscCall(RatelDMCreate(ratel, RATEL_SOLVER_STATIC, &dm));

  // View
  PetscCall(RatelView(ratel, PETSC_VIEWER_STDOUT_WORLD));

  // Cleanup
  PetscCall(DMDestroy(&dm));
  PetscCall(RatelDestroy(&ratel));

  return PetscFinalize();
}
